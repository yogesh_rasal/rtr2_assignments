/*

*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include "GL/glew.h"
#include <GL/gl.h>
#include <GL/glx.h>
#include "vmath.h"


using namespace std;
using namespace vmath;

FILE* gpFile = NULL;

GLenum Result;
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

//For Shaders
GLuint gVertex_Shader_Object;
GLuint gFragment_Shader_Object;
GLuint gShader_Program_Object;

GLint iShaderCompileStatus = 0;
GLint iInfoLogLength = 0;
GLchar *szInfoLog = NULL;
GLint iProgrmLinkStatus = 0;

GLuint mvpUniform;
mat4 Perspective_Projection_Matrix;
mat4 View_Matrix;


GLsizei shaderCount, shaderNumber;

//imp

GLuint Vao_pyramid;
GLuint Vbo_pyramid_position;
GLuint Vbo_pyramid_normal;

//for Lights
bool bLighting = false;
bool bAnimate = false;
GLfloat AngleTri = 0.0f;

//12 Uniforms : per Light
GLuint Model_Uniform;
GLuint View_Uniform;
GLuint Projection_Uniform;

GLuint LD_Uniform, LA_Uniform, LS_Uniform;
GLuint KD_Uniform, KA_Uniform, KS_Uniform;

GLuint Material_Shininess;

GLuint Light_Position_Uniform;
GLuint LisPressed_Uniform;

//Red

GLuint LD_Uniform_Red, LA_Uniform_Red, LS_Uniform_Red;
GLuint KD_Uniform_Red, KA_Uniform_Red, KS_Uniform_Red;

GLuint Light_Position_Uniform_Red;

//ENUM
enum 
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

//4 Globals
typedef GLXContext (*glXCreateContextAttribsARBProc)
                    (Display*, 
                     GLXFBConfig, 
                     GLXContext, 
                     Bool, 
                     const int*);

glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL; //imp

GLXContext gGLXContext;

GLXFBConfig gGLXFBConfig;

//func
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void initialize(void);
    void display(void);
    void resize(int,int);
    void update(void);
    void uninitialize(void);
  

int main(void)
{
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

    bool bDone = false;

    gpFile = fopen("Log.txt", "w");
  
    CreateWindow();

    initialize();

    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
             XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                break;
               
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
      
                        case XK_F:
                        case XK_f:
                            if(bFullscreen == false)
                            {
                                ToggleFullscreen();
                                bFullscreen = true;
                            }
                            else
                            {
                                ToggleFullscreen();
                                bFullscreen = false;
                            }
                            break;

                            case XK_L:
                        case XK_l:
                         if(bLighting == false)
                            bLighting = true;
                         else if(bLighting == true)
                          bLighting = false;
                         break;
      

                        default:
                        break;
                    }
                break;
      
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                        break;
                        case 2:
                        break;
                        case 3:
                        break;
                        default:
                        break;
                    }
                break;
      
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth,winHeight);
                break;
      
                case Expose:
                break;
      
                case DestroyNotify:
                break;
      
                case 33:
                    uninitialize();
                    exit(0);
                break;
      
                default:
                break;
            }
        }
        
        update(); //2

        display(); //most imp
    }
   	uninitialize(); //safe
    return(0);
}


void CreateWindow(void)
{

    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    //new
    GLXFBConfig *pGLXFBConfigs = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumFBConfigs=0;

    
    static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		GLX_STENCIL_SIZE,8,
		GLX_DOUBLEBUFFER,True,
		None
        }; 
	
    //imp
   gpDisplay = XOpenDisplay(NULL);
    
  if(gpDisplay == NULL)
    {
        printf("ERROR: Unable To Open X Display.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    //most imp : get Best Context
    //1
    pGLXFBConfigs = glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);
	
    if(pGLXFBConfigs==NULL)
	{
		printf( "Failed To Get Valid Framebuffer Config.\n");
		uninitialize();
		exit(1);
	}
	
    printf("%d Matching FB Configs Found.\n",iNumFBConfigs);
	
    int bestFramebufferconfig = -1;
    int worstFramebufferConfig = -1;
    int bestNumberOfSamples = -1;
    int worstNumberOfSamples = 999;
	
    for(int i=0;i<iNumFBConfigs;i++)
	{
        //3rd way to get visual
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
		
        if(pTempXVisualInfo)
		{
			int sampleBuffer,samples;

            // get no of sample Buffers from each fb
        	glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
		
        	glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLES,&samples);
		
        	printf("Matching Framebuffer Config=%d \n",i);
		
            // Like sorting
			if(bestFramebufferconfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferconfig=i;
				bestNumberOfSamples=samples;
			}
			
            if( worstFramebufferConfig < 0 || (!sampleBuffer) || samples < worstNumberOfSamples)
			{
				worstFramebufferConfig=i;
			    worstNumberOfSamples=samples;
			}
		}
		XFree(pTempXVisualInfo);
	}
	
    //Get Best Visual
    bestGLXFBConfig = pGLXFBConfigs[bestFramebufferconfig];

	gGLXFBConfig = bestGLXFBConfig;
	
	XFree(pGLXFBConfigs);
	
	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
    
    if(gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable To Get XVisualInfo.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
        RootWindow(gpDisplay,gpXVisualInfo->screen),
        gpXVisualInfo->visual,
        AllocNone);

    gColormap = winAttribs.colormap;
    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);  
           
    winAttribs.event_mask = ExposureMask| VisibilityChangeMask | ButtonPressMask | KeyPressMask 
                            | PointerMotionMask | StructureNotifyMask;
    
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap ; 

    gWindow = XCreateWindow(gpDisplay,
                RootWindow(gpDisplay,gpXVisualInfo->screen),
                0,0,giWindowWidth,
                giWindowHeight,
                0,
                gpXVisualInfo->depth,
                InputOutput,
                gpXVisualInfo->visual,
                styleMask,&winAttribs);

        if(!gWindow)
        {
            printf("ERROR: Failed To Create Window.\n Exitting Now...\n");
            uninitialize();
            exit(1);
        }

    XStoreName(gpDisplay,gWindow,"XWindows : Two Lights on Pyramid ");
    
    Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
    XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

    XMapWindow(gpDisplay,gWindow);         // imp
}


//Best
void ToggleFullscreen(void)
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 :1;
    fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev); 
}

//imp
void initialize(void)
{

    //new 
    GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB,3,
		GLX_CONTEXT_MINOR_VERSION_ARB,1,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None }; 		
	
    //Take & get fun pointer
    glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

    if(glXCreateContextAttribsARB == NULL)
    {
        printf("exit from ARB");
        uninitialize();
        exit(1);
    }

    gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);

	if(!gGLXContext) 
	{
		GLint attribs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			0 }; 
		
        printf("Failed To Create GLX context. \n");

		gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
	}
	
    else 
	{
		printf("OpenGL Context is Created.\n");
	}
	
	
	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Non HW Context Obtained \n");
	}
	else
	{
		printf("HW Context Obtained \n" );
	}
	
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	
    Result = glewInit();
    fprintf(gpFile, " Shader : %d",Result);


	//Depth
	
	Perspective_Projection_Matrix = mat4::identity();


    	//For Shaders : create 
   gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

    const GLchar *vertexShaderSourceCode =
        "#version 330 core" 		\
        "\n" 						\
        "in vec4 vPosition;"						\
		"in vec3 vNormal;"							\
		"out vec3 T_Norm;"							\
		"out vec3 Light_Direction;"					\
		"out vec3 Light_Direction_R;"				\
		"out vec3 Viewer_Vector;"					\
		"\n"										\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform int u_LKeyPressed;"				\
		"uniform vec4 u_Light_Position;"			\
		"uniform vec4 u_Light_Position_R;"			\
		"\n"										\
		"void main(void)"							\
		"{"											\
		"if(u_LKeyPressed == 1)"					\
		"{"											\
		"vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"	\
		"T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"			\
		"Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"	\
		"Light_Direction_R = vec3(u_Light_Position_R) - (Eye_Coordinates.xyz);"	\
		"Viewer_Vector = vec3(-Eye_Coordinates.xyz);"	\
		"}"		\
		"gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"	\
		"}" ;



    glShaderSource(gVertex_Shader_Object,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    glCompileShader(gVertex_Shader_Object);
    
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char* szInfoLog = NULL;

    glGetShaderiv(gVertex_Shader_Object,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    
	if(iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gVertex_Shader_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        
		if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertex_Shader_Object,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }    

    //fragment shader
    gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar * gFragmentShaderSourceCode = 
        "#version 330 core"			\
        "\n"						\
 		"in vec3 T_Norm;"			\
		"in vec3 Light_Direction;"	\
		"in vec3 Light_Direction_R;"	\
		"in vec3 Viewer_Vector;"	\
		"out vec4 FragColor;"		\
		"\n"						\
		"uniform vec3 u_LA;"				\
		"uniform vec3 u_LD;"				\
		"uniform vec3 u_LS;"				\
		"uniform vec3 u_KA;"				\
		"uniform vec3 u_KD;"				\
		"uniform vec3 u_KS;"				\
		"uniform vec3 u_LA_R;"				\
		"uniform vec3 u_LD_R;"				\
		"uniform vec3 u_LS_R;"				\
		"uniform vec3 u_KA_R;"				\
		"uniform vec3 u_KD_R;"				\
		"uniform vec3 u_KS_R;"				\
		"uniform float u_Shininess;"		\
		"uniform int u_LKeyPressed;"		\
		"uniform vec4 u_Light_Position;"	\
		"uniform vec4 u_Light_Position_R;"	\
		"\n"								\
		"void main(void)"					\
		"{"						\
		"if(u_LKeyPressed == 1)"\
		"{"						\
		"vec3 Normalized_View_Vector = normalize(Viewer_Vector);"	\
		"vec3 Normalized_Light_Direction = normalize(Light_Direction);"\
		"vec3 Normalized_Light_Direction_R = normalize(Light_Direction_R);"\
		"vec3 Normalized_TNorm = normalize(T_Norm);"\
		"float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0);"	\
		"vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm);"\
		"float TN_Dot_LD_R = max(dot(Normalized_Light_Direction_R,Normalized_TNorm), 0.0);"	\
		"vec3 Reflection_Vector_R = reflect(-Normalized_Light_Direction_R, Normalized_TNorm);"\
		"vec3 Ambient = vec3(u_LA * u_KA);"	\
		"vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);"	\
		"vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess));" \
		"vec3 Ambient_R = vec3(u_LA_R * u_KA_R);"	\
		"vec3 Diffuse_R = vec3(u_LD_R * u_KD_R * TN_Dot_LD_R);"	\
		"vec3 Specular_R = vec3(u_LS_R * u_KS_R * pow(max(dot(Reflection_Vector_R, Normalized_View_Vector), 0.0), u_Shininess));" \
		"vec3 Phong_ADS_Light = Ambient + Diffuse+ Specular;"	\
		"vec3 Phong_ADS_Light_R = Ambient_R + Diffuse_R+ Specular_R;"	\
		"FragColor = vec4(Phong_ADS_Light+Phong_ADS_Light_R,1.0);" \
		"}"							\
		"else"						\
		"{"							\
		"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		"}"							\
		"}" ;
       

    glShaderSource(gFragment_Shader_Object,1,(const GLchar **)&gFragmentShaderSourceCode,NULL);
    glCompileShader(gFragment_Shader_Object);

    glGetShaderiv(gFragment_Shader_Object,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    
	if(iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gFragment_Shader_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        
		if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragment_Shader_Object,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s\n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }

    //shader program

    gShader_Program_Object = glCreateProgram();
    
    glAttachShader(gShader_Program_Object,gVertex_Shader_Object);
    glAttachShader(gShader_Program_Object,gFragment_Shader_Object);

    //bind in variables before linking
    glBindAttribLocation(gShader_Program_Object,AMC_ATTRIBUTE_POSITION,"vPosition");
    glBindAttribLocation(gShader_Program_Object, AMC_ATTRIBUTE_NORMAL, "vNormal");

    glLinkProgram(gShader_Program_Object);
    GLint iShaderProgramLinkStatus = 0;
    
	glGetProgramiv(gShader_Program_Object,GL_LINK_STATUS,&iShaderProgramLinkStatus);
    
	if(iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShader_Program_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShader_Program_Object,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Shader Program Link Log: %s\n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }

    //get uniform location
    mvpUniform = glGetUniformLocation(gShader_Program_Object,"u_model_matrix");
	Projection_Uniform = glGetUniformLocation(gShader_Program_Object, "u_projection_matrix");
	View_Uniform = glGetUniformLocation(gShader_Program_Object, "u_view_matrix");

	LD_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LD");
	KD_Uniform = glGetUniformLocation(gShader_Program_Object, "u_KD");

	LA_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LA");
	KA_Uniform = glGetUniformLocation(gShader_Program_Object, "u_KA");

	LS_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LS");
	KS_Uniform = glGetUniformLocation(gShader_Program_Object, "u_KS");

	LisPressed_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LKeyPressed");
	Light_Position_Uniform = glGetUniformLocation(gShader_Program_Object, "u_Light_Position");
	Material_Shininess = glGetUniformLocation(gShader_Program_Object, "u_Shininess");
	    
	//Red Light
	
	mvpUniform = glGetUniformLocation(gShader_Program_Object, "u_model_matrix");
	Projection_Uniform = glGetUniformLocation(gShader_Program_Object, "u_projection_matrix");
	View_Uniform = glGetUniformLocation(gShader_Program_Object, "u_view_matrix");

	LD_Uniform_Red = glGetUniformLocation(gShader_Program_Object, "u_LD_R");
	KD_Uniform_Red = glGetUniformLocation(gShader_Program_Object, "u_KD_R");

	LA_Uniform_Red = glGetUniformLocation(gShader_Program_Object, "u_LA_R");
	KA_Uniform_Red = glGetUniformLocation(gShader_Program_Object, "u_KA_R");

	LS_Uniform_Red = glGetUniformLocation(gShader_Program_Object, "u_LS_R");
	KS_Uniform_Red = glGetUniformLocation(gShader_Program_Object, "u_KS_R");

	LisPressed_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LKeyPressed");
	Light_Position_Uniform_Red = glGetUniformLocation(gShader_Program_Object, "u_Light_Position_R");
	Material_Shininess = glGetUniformLocation(gShader_Program_Object, "u_Shininess");



	//pyramid
	const GLfloat pyramidVertices[] =
	{
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f
	};

	glGenVertexArrays(1, &Vao_pyramid);
	glBindVertexArray(Vao_pyramid);

	//for vertices
	glGenBuffers(1, &Vbo_pyramid_position);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_pyramid_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	const GLfloat pyramid_normals[] =
	{
		0.0f,0.447214f,0.894427f, //1
		0.0f,0.447214f,0.894427f, //2
		0.0f,0.447214f,0.894427f, //3
		0.894427f,0.447214f,0.0f, //4
		0.894427f,0.447214f,0.0f, //5
		0.894427f,0.447214f,0.0f, //6
		0.0f,0.447214f,-0.894427f, //7
		0.0f,0.447214f,-0.894427f, //8
		0.0f,0.447214f,-0.894427f, //9
		-0.894427f,0.447214f,0.0f, //10
		-0.894427f,0.447214f,0.0f, //11
		-0.894427f,0.447214f,0.0f //12
	};

	glGenBuffers(1, &Vbo_pyramid_normal);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_pyramid_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramid_normals), pyramid_normals, GL_STATIC_DRAW);

	// normal vbo Binding

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	
	//Depth 
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
    Perspective_Projection_Matrix = mat4::identity();
    
    resize(giWindowWidth,giWindowHeight); //stray
}


void resize(int Width,int Height)
{
   if (Height == 0)
        Height = 1;
    
    glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
  
   	Perspective_Projection_Matrix = perspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);

}


void display(void)
{
 	//imp
	mat4 Model_Matrix;
	mat4 View_Matrix;
	mat4 Projection_Matrix;
	mat4 Translation_Matrix;
	mat4 Rotation_Matrix;
	mat4 ModelView_Projection_Matrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShader_Program_Object);

	//code : 9 steps

	//2 : I[]
	Model_Matrix = mat4::identity();
    View_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	Rotation_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//4 : Do Transforms & Mat mul
	Translation_Matrix = translate(0.0f, 0.0f, -5.0f);

	Rotation_Matrix = rotate(AngleTri, 0.0f, 1.0f, 0.0f);

	Model_Matrix = Translation_Matrix * Rotation_Matrix;

	ModelView_Projection_Matrix = Perspective_Projection_Matrix * View_Matrix * Model_Matrix;


	// 5	: send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, Model_Matrix);
	glUniformMatrix4fv(Projection_Uniform, 1, GL_FALSE, Perspective_Projection_Matrix);
	glUniformMatrix4fv(View_Uniform, 1, GL_FALSE, View_Matrix);
	
    //imp
    	if (bLighting == true)
	{
		glUniform1i(LisPressed_Uniform, 1);

		glUniform3f(LD_Uniform, 0.0f, 0.0f, 1.0f);
		glUniform3f(KD_Uniform, 1.0f, 1.0f, 1.0f);

		glUniform3f(LS_Uniform, 0.0f, 0.0f, 1.0f);
		glUniform3f(KS_Uniform, 1.0f, 1.0f, 1.0f);
		
		glUniform3f(LA_Uniform, 0.0f, 0.0f, 0.0f);
		glUniform3f(KA_Uniform, 0.25f, 0.25f, 0.25f);
		
		glUniform1f(Material_Shininess, 128.0f);
		
		//imp : out to in Light
		
		glUniform4f(Light_Position_Uniform, 5.0f, 0.0f, 0.0f, 1.0f);

		//2nd Light

			glUniform3f(LD_Uniform_Red, 1.0f, 0.0f, 0.0f);
			glUniform3f(KD_Uniform_Red, 1.0f, 1.0f, 1.0f);

			glUniform3f(LS_Uniform_Red, 1.0f, 0.0f, 0.0f);
			glUniform3f(KS_Uniform_Red, 1.0f, 1.0f, 1.0f);

			glUniform3f(LA_Uniform_Red, 0.0f, 0.0f, 0.0f);
			glUniform3f(KA_Uniform_Red, 0.25f, 0.25f, 0.25f);

			glUniform1f(Material_Shininess, 128.0f);

			glUniform4f(Light_Position_Uniform_Red, -5.0f, 0.0f, 0.0f, 1.0f);


	}


	else
	{
		glUniform1i(LisPressed_Uniform, 0);
	}

	// 6 : bind to vao
	glBindVertexArray(Vao_pyramid);


	//8 : draw scene
	glDrawArrays(GL_TRIANGLES, 0, 12);



	//9 : Unbind vao    
	glBindVertexArray(0);

	glUseProgram(0);

    glXSwapBuffers(gpDisplay,gWindow);
}


//new
void update(void)
{
    	AngleTri += 0.2f;

	if (AngleTri >= 360.0f)
		AngleTri -= 360.0f;

}


void uninitialize(void)
{
    GLXContext currentGLXContext;
    currentGLXContext =glXGetCurrentContext();

    	//for Shaders
	if (Vbo_pyramid_position)
	{
		glDeleteBuffers(1, &Vbo_pyramid_position);
		Vbo_pyramid_position = 0;
	}

    if (Vbo_pyramid_normal)
	{
		glDeleteBuffers(1, &Vbo_pyramid_normal);
		Vbo_pyramid_normal = 0;
	}

	if (Vao_pyramid)
	{
		glDeleteVertexArrays(1, &Vao_pyramid);
		Vao_pyramid = 0;
	}

	glUseProgram(gShader_Program_Object);
	glDetachShader(gShader_Program_Object, gFragment_Shader_Object);
	glDetachShader(gShader_Program_Object, gVertex_Shader_Object);

	glDeleteShader(gFragment_Shader_Object);
	gFragment_Shader_Object = 0;

	glDeleteShader(gVertex_Shader_Object);
	gVertex_Shader_Object = 0;

	glDeleteProgram(gShader_Program_Object);
	glUseProgram(0);


    if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,0,0);
    }

    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay,gGLXContext);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay,gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }

    if (gpFile)
    {
        fprintf(gpFile, "Log File successfully Closed.\n");
        fclose(gpFile);
        gpFile = NULL;
    }

}

