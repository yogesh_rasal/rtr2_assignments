/*
    Deathly Hallows
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include "GL/glew.h"
#include <GL/gl.h>
#include <GL/glx.h>
#include "vmath.h"

using namespace std;
using namespace vmath;

FILE* gpFile = NULL;
#define SLICES 20
#define NUM_POINTS 1000

GLenum Result;
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;

int giWindowWidth = 800;
int giWindowHeight = 600;

//For Shaders
GLuint gVertex_Shader_Object;
GLuint gFragment_Shader_Object;
GLuint gShader_Program_Object;

GLint iShaderCompileStatus = 0;
GLint iInfoLogLength = 0;
GLchar *szInfoLog = NULL;
GLint iProgrmLinkStatus = 0;

//2d
GLuint vao_Triangle,vao_Rectagle;
GLuint vbo_Position_Triangle;
GLuint vbo_Position_Rectangle;
GLuint vbo_Color_Triangle;
GLuint vbo_Color_Rectangle;

//for Graph
GLuint vao_XAxis, vao_YAxis; 
GLuint vbo_XPosition,vbo_XColor;
GLuint vbo_YPosition, vbo_YColor;
GLuint vao_Lines, vao_HLines;
GLuint vbo_PositionHLines, vbo_PositionVLines;
GLuint vbo_ColorHLines, vbo_ColorVLines;

//for circle
GLuint vao_Circle, vbo_Position_Circle, vbo_Color_Circle;


GLuint mvpUniform;
mat4 Perspective_Projection_Matrix;

GLfloat AngleTri = 0.0f, AngleRect = 0.0f;

//ENUM
enum 
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

//4 Globals
typedef GLXContext (*glXCreateContextAttribsARBProc)
                    (Display*, 
                     GLXFBConfig, 
                     GLXContext, 
                     Bool, 
                     const int*);

glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL; //imp

GLXContext gGLXContext;

GLXFBConfig gGLXFBConfig;

//func
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void initialize(void);
    void display(void);
    void resize(int,int);
    void update(void);
    void uninitialize(void);
    
    //new
    void showGraph();

int main(void)
{
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

    bool bDone = false;

    gpFile = fopen("Log.txt", "w");
  
    CreateWindow();

    initialize();

    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
             XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                break;
               
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
      
                        case XK_F:
                        case XK_f:
                            if(bFullscreen == false)
                            {
                                ToggleFullscreen();
                                bFullscreen = true;
                            }
                            else
                            {
                                ToggleFullscreen();
                                bFullscreen = false;
                            }
                            break;
      
                        default:
                        break;
                    }
                break;
      
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                        break;
                        case 2:
                        break;
                        case 3:
                        break;
                        default:
                        break;
                    }
                break;
      
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth,winHeight);
                break;
      
                case Expose:
                break;
      
                case DestroyNotify:
                break;
      
                case 33:
                    uninitialize();
                    exit(0);
                break;
      
                default:
                break;
            }
        }
        
        update(); //2

        display(); //most imp
    }
   	uninitialize(); //safe
    return(0);
}


void CreateWindow(void)
{

    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    //new
    GLXFBConfig *pGLXFBConfigs = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumFBConfigs=0;

    
    static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		GLX_STENCIL_SIZE,8,
		GLX_DOUBLEBUFFER,True,
		None
        }; 
	
    //imp
   gpDisplay = XOpenDisplay(NULL);
    
  if(gpDisplay == NULL)
    {
        printf("ERROR: Unable To Open X Display.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    //most imp : get Best Context
    //1
    pGLXFBConfigs = glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);
	
    if(pGLXFBConfigs==NULL)
	{
		printf( "Failed To Get Valid Framebuffer Config.\n");
		uninitialize();
		exit(1);
	}
	
    printf("%d Matching FB Configs Found.\n",iNumFBConfigs);
	
    int bestFramebufferconfig = -1;
    int worstFramebufferConfig = -1;
    int bestNumberOfSamples = -1;
    int worstNumberOfSamples = 999;
	
    for(int i=0;i<iNumFBConfigs;i++)
	{
        //3rd way to get visual
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
		
        if(pTempXVisualInfo)
		{
			int sampleBuffer,samples;

            // get no of sample Buffers from each fb
        	glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
		
        	glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLES,&samples);
		
        	printf("Matching Framebuffer Config=%d \n",i);
		
            // Like sorting
			if(bestFramebufferconfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferconfig=i;
				bestNumberOfSamples=samples;
			}
			
            if( worstFramebufferConfig < 0 || (!sampleBuffer) || samples < worstNumberOfSamples)
			{
				worstFramebufferConfig=i;
			    worstNumberOfSamples=samples;
			}
		}
		XFree(pTempXVisualInfo);
	}
	
    //Get Best Visual
    bestGLXFBConfig = pGLXFBConfigs[bestFramebufferconfig];

	gGLXFBConfig = bestGLXFBConfig;
	
	XFree(pGLXFBConfigs);
	
	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
    
    if(gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable To Get XVisualInfo.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
        RootWindow(gpDisplay,gpXVisualInfo->screen),
        gpXVisualInfo->visual,
        AllocNone);

    gColormap = winAttribs.colormap;
    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);  
           
    winAttribs.event_mask = ExposureMask| VisibilityChangeMask | ButtonPressMask | KeyPressMask 
                            | PointerMotionMask | StructureNotifyMask;
    
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap ; 

    gWindow = XCreateWindow(gpDisplay,
                RootWindow(gpDisplay,gpXVisualInfo->screen),
                0,0,giWindowWidth,
                giWindowHeight,
                0,
                gpXVisualInfo->depth,
                InputOutput,
                gpXVisualInfo->visual,
                styleMask,&winAttribs);

        if(!gWindow)
        {
            printf("ERROR: Failed To Create Window.\n Exitting Now...\n");
            uninitialize();
            exit(1);
        }

    XStoreName(gpDisplay,gWindow,"XWindows : Deathly Hallows");
    
    Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
    XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

    XMapWindow(gpDisplay,gWindow);         // imp
}


//Best
void ToggleFullscreen(void)
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 :1;
    fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev); 
}

//imp
void initialize(void)
{

    //new 
    GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB,3,
		GLX_CONTEXT_MINOR_VERSION_ARB,1,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None }; 		
	
    //Take & get fun pointer
    glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

    if(glXCreateContextAttribsARB == NULL)
    {
        printf("exit from ARB");
        uninitialize();
        exit(1);
    }

    gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);

	if(!gGLXContext) 
	{
		GLint attribs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			0 }; 
		
        printf("Failed To Create GLX context. \n");

		gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
	}
	
    else 
	{
		printf("OpenGL Context is Created.\n");
	}
	
	
	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Non HW Context Obtained \n");
	}
	else
	{
		printf("HW Context Obtained \n" );
	}
	
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	
    Result = glewInit();
    fprintf(gpFile, " Shader : %d",Result);


	//Depth
	
	Perspective_Projection_Matrix = mat4::identity();


    //for Ortho
    	//For Shaders : create 
   gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

    const GLchar *vertexShaderSourceCode =
        "#version 330 core" 		                \
        "\n" 						                \
        "in vec4 vPosition;"						\
		"in vec4 vColor;"							\
		"out vec4 Out_Color;"						\
		"uniform mat4 u_mvp_matrix;"				\
		"void main(void)"							\
		"{"											\
		"gl_Position = u_mvp_matrix * vPosition;"	\
		"Out_Color = vColor;"						\
		"}" ;


    glShaderSource(gVertex_Shader_Object,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    glCompileShader(gVertex_Shader_Object);
    
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char* szInfoLog = NULL;

    glGetShaderiv(gVertex_Shader_Object,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    
	if(iShaderCompiledStatus ==GL_FALSE)
    {
        glGetShaderiv(gVertex_Shader_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        
		if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertex_Shader_Object,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }    

    //fragment shader
    gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar * gFragmentShaderSourceCode = 
        "#version 330 core"		 \
        "\n"					 \
        "in vec4 Out_Color;"	 \
		"out vec4 FragColor;"	 \
		"void main(void)"		 \
		" { "					 \
		"FragColor = Out_Color;" \
		" } " ;


    glShaderSource(gFragment_Shader_Object,1,(const GLchar **)&gFragmentShaderSourceCode,NULL);
    glCompileShader(gFragment_Shader_Object);

    glGetShaderiv(gFragment_Shader_Object,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    
	if(iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gFragment_Shader_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        
		if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragment_Shader_Object,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s\n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }

    //shader program

    gShader_Program_Object = glCreateProgram();
    glAttachShader(gShader_Program_Object,gVertex_Shader_Object);
    glAttachShader(gShader_Program_Object,gFragment_Shader_Object);

    //bind in variables before linking
    glBindAttribLocation(gShader_Program_Object,AMC_ATTRIBUTE_POSITION,"vPosition");


    glLinkProgram(gShader_Program_Object);
    GLint iShaderProgramLinkStatus = 0;
    
	glGetProgramiv(gShader_Program_Object,GL_LINK_STATUS,&iShaderProgramLinkStatus);
    
	if(iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShader_Program_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShader_Program_Object,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Shader Program Link Log: %s\n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }

    //get uniform location
    mvpUniform = glGetUniformLocation(gShader_Program_Object,"u_mvp_matrix");

    	//use Triangle vertices
	const GLfloat Triangle_Vertices[] = { 0.0f,1.0f,0.0f,
										-1.0f,-1.0f,0.0f,
										1.0f,-1.0f,0.0f,
										0.0f,1.0f,0.0f,
										1.0f,-1.0f,0.0f,
									   -1.0f,-1.0f,0.0f };

	const GLfloat Triangle_Colors[] = { 1.0f,1.0f,1.0f,
										1.0f,1.0f,1.0f,
										1.0f,1.0f,1.0f,
										1.0f,1.0f,1.0f,
										1.0f,1.0f,1.0f,
										1.0f,1.0f,1.0f };


	//create vao for Triangle
	glGenVertexArrays(1, &vao_Triangle);

	//Binding
	glBindVertexArray(vao_Triangle);

	//Buffer
	glGenBuffers(1, &vbo_Position_Triangle);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_Position_Triangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Triangle_Vertices),
				Triangle_Vertices,GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Color
	//Buffer
	glGenBuffers(1, &vbo_Color_Triangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Triangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Triangle_Colors),
		Triangle_Colors, GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
    //vao
	glBindVertexArray(0);


    //for Circle : 
    //Circle
	GLfloat fPerimeter;
	GLfloat fRadius;

	GLfloat CircleVertices[NUM_POINTS][3];
	GLfloat CircleColor[NUM_POINTS][3];

	//Code
	fPerimeter = (GLfloat)sqrt(((1.0f)*(1.0f)) + ((1.0f)*(1.0f))); //imp
	fRadius = fPerimeter / 2;

	for (int i = 0; i < NUM_POINTS; i++)
	{
		GLfloat angle = (GLfloat)(2 * M_PI * i) / NUM_POINTS; //2 Pie R
		CircleVertices[i][0] = (GLfloat)cos(angle) * fRadius; 
		CircleVertices[i][1] = (GLfloat)sin(angle) * fRadius;
		CircleVertices[i][2] = 0.0f;

		//yellow
		CircleColor[i][0] = 1.0f;
		CircleColor[i][1] = 1.0f;
		CircleColor[i][2] = 1.0f;
	}

    	// Create vao , Bind 
	glGenVertexArrays(1, &vao_Circle);
	glBindVertexArray(vao_Circle);

    //VBO - Gen , Bind , Bufferdata 
	glGenBuffers(1, &vbo_Position_Circle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Circle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(CircleVertices), CircleVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	// Unbind vbo
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Bind vboColor
	glGenBuffers(1, &vbo_Color_Circle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Circle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(CircleColor), CircleColor, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	// Unbind vbo
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Unbind VAO
	glBindVertexArray(0);

//*** Elder Wand
	GLfloat AxisColor[] = { 1.0f,1.0f,1.0f,
							1.0f,1.0f,1.0f };
	
	GLfloat YAxis[] = { 0.0f, 1.0f, 0.0f,
						0.0f, -1.0f, 0.0f };


 //Y-Axis
	
	// Create vao
	
	glGenVertexArrays(1, &vao_YAxis);
	glBindVertexArray(vao_YAxis);

	glGenBuffers(1, &vbo_YPosition);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_YPosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*6, YAxis, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	// Unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Bind color
	glGenBuffers(1, &vbo_YColor);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_YColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(AxisColor), AxisColor, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	// Unbind color
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Unbind vao
	glBindVertexArray(0);


	//Depth 
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
    resize(giWindowWidth,giWindowHeight); //stray
}


void resize(int Width,int Height)
{
   if (Height == 0)
        Height = 1;
    
    glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
  
   	Perspective_Projection_Matrix = perspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);

}



void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //1

   	mat4 ModelView_Matrix;
	mat4 ModelView_Projection_Matrix;
    //new
   	mat4 Translation_Matrix;
	mat4 Rotation_Matrix;

	glUseProgram(gShader_Program_Object);

	//code : 9 steps

	// Wand
	// 2 : I[]
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();

	//  Transformation
	ModelView_Matrix = translate(0.0f, 0.0f, -3.42f);

	//  Matrix Multiplication
	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;

	//5 :  Send matrices to shader 
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	// Bind vao
	glBindVertexArray(vao_YAxis);

	glLineWidth(3.0f);
	glDrawArrays(GL_LINES, 0, 2);

	// Unbind vao
	glBindVertexArray(0);



//Circle
//2 : I[]
	Rotation_Matrix = mat4::identity();
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//3 : Transforms
	Translation_Matrix = translate(0.0f, -0.45f, -4.0f);

	//4 : Do Mat mul
	ModelView_Matrix = Translation_Matrix * Rotation_Matrix;

	//Sequence is imp
	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;


	// 5 : send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	//Bind
	glBindVertexArray(vao_Circle);

	//draw
	glLineWidth(2.0f);
	glDrawArrays(GL_LINE_LOOP, 0, NUM_POINTS);

	// Unbind vao
	glBindVertexArray(0);


//Triangle
	//2 : I[]
	Rotation_Matrix = mat4::identity();
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//3 : Transforms

	Translation_Matrix = translate(0.0f, 0.0f,-3.42f);

	//4 : Do Mat mul

	ModelView_Matrix = Translation_Matrix * Rotation_Matrix;

	//new
	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;


	// 5 : send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	glBindVertexArray(vao_Triangle);

	//8 : draw scene
	glLineWidth(3.0f);
	glDrawArrays(GL_LINES, 0, 6);

	//9 : Unbind vao
	glBindVertexArray(0);


	glUseProgram(0); //SPO

	//printf("\n  Unbind VAO Buffer");
    glXSwapBuffers(gpDisplay,gWindow);
}


//new
void update(void)
{
    	
}


void uninitialize(void)
{
    GLXContext currentGLXContext;
    currentGLXContext =glXGetCurrentContext();

		//for Shaders
	if (vbo_Position_Triangle)
	{
		glDeleteBuffers(1, &vbo_Position_Triangle);
		vbo_Position_Triangle = 0;
	}

	if (vao_Triangle)
	{
		glDeleteVertexArrays(1, &vao_Triangle);
		vao_Triangle = 0;
	}

  	// Tri
	if (vbo_Position_Triangle)
	{
		glDeleteBuffers(1, &vbo_Position_Triangle);
		vbo_Position_Rectangle = 0;
	}

	if (vao_Triangle)
	{
		glDeleteVertexArrays(1, &vao_Triangle);
		vao_Triangle = 0;
	}


	//vao_Circle, vbo_Position_Circle, vbo_Color_Circle, vao_XAxis, vao_YAxis; 
	// vbo_XPosition, vbo_XColor, vbo_YPosition, vbo_YColor;

	if (vbo_Position_Circle)
	{
		glDeleteBuffers(1, &vbo_Position_Circle);
		vbo_Position_Circle = 0;

	}

	if (vbo_Color_Circle)
	{
		glDeleteBuffers(1, &vbo_Color_Circle);
		vbo_Color_Circle = 0;

	}
	
	if (vbo_XPosition)
	{
		glDeleteBuffers(1, &vbo_XPosition);
		vbo_XPosition = 0;

	}

	if (vbo_YPosition)
	{
		glDeleteBuffers(1, &vbo_YPosition);
		vbo_YPosition = 0;

	}

	if (vbo_XColor)
	{
		glDeleteBuffers(1, &vbo_XColor);
		vbo_XColor = 0;

	}

	if (vbo_YColor)
	{
		glDeleteBuffers(1, &vbo_YColor);
		vbo_YColor = 0;

	}


	if (vao_Circle)
	{
		glDeleteVertexArrays(1, &vao_Circle);
		vao_Circle = 0;
	}

	if (vao_XAxis)
	{
		glDeleteVertexArrays(1, &vao_XAxis);
		vao_XAxis = 0;
	}

	if (vao_YAxis)
	{
		glDeleteVertexArrays(1, &vao_YAxis);
		vao_YAxis = 0;
	}


	glUseProgram(gShader_Program_Object);
	glDetachShader(gShader_Program_Object, gFragment_Shader_Object);
	glDetachShader(gShader_Program_Object, gVertex_Shader_Object);

	glDeleteShader(gFragment_Shader_Object);
	gFragment_Shader_Object = 0;

	glDeleteShader(gVertex_Shader_Object);
	gVertex_Shader_Object = 0;

	glDeleteProgram(gShader_Program_Object);
	glUseProgram(0);


    if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,0,0);
    }

    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay,gGLXContext);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay,gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }

    if (gpFile)
    {
        fprintf(gpFile, "Log File successfully Closed.\n");
        fclose(gpFile);
        gpFile = NULL;
    }

}

