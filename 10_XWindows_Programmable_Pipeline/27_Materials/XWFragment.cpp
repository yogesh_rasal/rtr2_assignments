/*
    Materials
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include "GL/glew.h"
#include <GL/gl.h>
#include <GL/glx.h>
#include "vmath.h"
#include "Sphere.h"

using namespace std;
using namespace vmath;

FILE* gpFile = NULL;

GLenum Result;
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

//For Shaders
GLuint gVertex_Shader_Object;
GLuint gFragment_Shader_Object;
GLuint gShader_Program_Object;

GLint iShaderCompileStatus = 0;
GLint iInfoLogLength = 0;
GLchar *szInfoLog = NULL;
GLint iProgrmLinkStatus = 0;

GLuint mvpUniform;
mat4 Perspective_Projection_Matrix;
mat4 View_Matrix;

bool bLighting = false;
GLuint gVao_sphere, gVbo_sphere_position, gVbo_sphere_normal, gVbo_sphere_element;
GLuint gNumElements, gNumVertices;
GLsizei shaderCount, shaderNumber;

//for sphere
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

//imp
GLfloat Angle = 0.0f;
bool XPressed,YPressed , ZPressed;
int winWidth = giWindowWidth;
int winHeight = giWindowHeight;


//12 Uniforms
GLuint Model_Uniform;
GLuint View_Uniform;
GLuint Projection_Uniform;

GLuint LD_Uniform, LA_Uniform, LS_Uniform;
GLuint KD_Uniform, KA_Uniform, KS_Uniform;

GLuint u_Material_Shininess;
GLuint Light_Position_Uniform;
GLuint LisPressed_Uniform;

//ENUM
enum 
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

//4 Globals
typedef GLXContext (*glXCreateContextAttribsARBProc)
                    (Display*, 
                     GLXFBConfig, 
                     GLXContext, 
                     Bool, 
                     const int*);

glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL; //imp

GLXContext gGLXContext;

GLXFBConfig gGLXFBConfig;

//func
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void initialize(void);
    void display(void);
    void resize(int,int);
    void update(void);
    void uninitialize(void);
  

int main(void)
{
    bool bDone = false;

    gpFile = fopen("Log.txt", "w");
  
    CreateWindow();

    initialize();

    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
             XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                break;
               
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
      
                        case XK_F:
                        case XK_f:
                            if(bFullscreen == false)
                            {
                                ToggleFullscreen();
                                bFullscreen = true;
                            }
                            else
                            {
                                ToggleFullscreen();
                                bFullscreen = false;
                            }
                            break;
      
                        case XK_L:
                        case XK_l:
                         if(bLighting == false)
                            bLighting = true;
                         else if(bLighting == true)
                          bLighting = false;
                         break;

                        case XK_x: 
                        case XK_X: 
                            XPressed = true;
                            YPressed = false;
                            ZPressed = false;
                            break;
                    
                        case XK_y:
                        case XK_Y: 
                            YPressed = true;
                            ZPressed = false;
                            XPressed = false;
                            break;

                        case XK_z:
                        case XK_Z: 
                            ZPressed = true;
                            XPressed = false;
                            YPressed = false;
                            break;

                        default:
                        break;
                    }
                break;
      
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                        break;
                        case 2:
                        break;
                        case 3:
                        break;
                        default:
                        break;
                    }
                break;
      
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth,winHeight);
                    break;
      
                case Expose:
                break;
      
                case DestroyNotify:
                break;
      
                case 33:
                    uninitialize();
                    exit(0);
                break;
      
                default:
                break;
            }
        }
        
        update(); //2

        display(); //most imp
    }
   	uninitialize(); //safe
    return(0);
}


void CreateWindow(void)
{

    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    //new
    GLXFBConfig *pGLXFBConfigs = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumFBConfigs = 0;

    
    static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		GLX_STENCIL_SIZE,8,
		GLX_DOUBLEBUFFER,True,
		None
        }; 
	
    //imp
   gpDisplay = XOpenDisplay(NULL);
    
  if(gpDisplay == NULL)
    {
        printf("ERROR: Unable To Open X Display.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    //most imp : get Best Context
    //1
    pGLXFBConfigs = glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);
	
    if(pGLXFBConfigs==NULL)
	{
		printf( "Failed To Get Valid Framebuffer Config.\n");
		uninitialize();
		exit(1);
	}
	
    printf("%d Matching FB Configs Found.\n",iNumFBConfigs);
	
    int bestFramebufferconfig = -1;
    int worstFramebufferConfig = -1;
    int bestNumberOfSamples = -1;
    int worstNumberOfSamples = 999;
	
    for(int i=0;i<iNumFBConfigs;i++)
	{
        //3rd way to get visual
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
		
        if(pTempXVisualInfo)
		{
			int sampleBuffer,samples;

            // get no of sample Buffers from each fb
        	glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
		
        	glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLES,&samples);
		
        	printf("Matching Framebuffer Config=%d \n",i);
		
            // Like sorting
			if(bestFramebufferconfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferconfig=i;
				bestNumberOfSamples=samples;
			}
			
            if( worstFramebufferConfig < 0 || (!sampleBuffer) || samples < worstNumberOfSamples)
			{
				worstFramebufferConfig=i;
			    worstNumberOfSamples=samples;
			}
		}
		XFree(pTempXVisualInfo);
	}
	
    //Get Best Visual
    bestGLXFBConfig = pGLXFBConfigs[bestFramebufferconfig];

	gGLXFBConfig = bestGLXFBConfig;
	
	XFree(pGLXFBConfigs);
	
	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
    
    if(gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable To Get XVisualInfo.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
        RootWindow(gpDisplay,gpXVisualInfo->screen),
        gpXVisualInfo->visual,
        AllocNone);

    gColormap = winAttribs.colormap;
    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);  
           
    winAttribs.event_mask = ExposureMask| VisibilityChangeMask | ButtonPressMask | KeyPressMask 
                            | PointerMotionMask | StructureNotifyMask;
    
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap ; 

    gWindow = XCreateWindow(gpDisplay,
                RootWindow(gpDisplay,gpXVisualInfo->screen),
                0,0,giWindowWidth,
                giWindowHeight,
                0,
                gpXVisualInfo->depth,
                InputOutput,
                gpXVisualInfo->visual,
                styleMask,&winAttribs);

        if(!gWindow)
        {
            printf("ERROR: Failed To Create Window.\n Exitting Now...\n");
            uninitialize();
            exit(1);
        }

    XStoreName(gpDisplay,gWindow,"XWindows : Materials Lighting");
    
    Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
    XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

    XMapWindow(gpDisplay,gWindow);         // imp
}


//Best
void ToggleFullscreen(void)
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 :1;
    fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev); 
}

//imp
void initialize(void)
{

    //new 
    GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB,3,
		GLX_CONTEXT_MINOR_VERSION_ARB,1,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None }; 		
	
    //Take & get fun pointer
    glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

    if(glXCreateContextAttribsARB == NULL)
    {
        printf("exit from ARB");
        uninitialize();
        exit(1);
    }

    gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);

	if(!gGLXContext) 
	{
		GLint attribs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			0 }; 
		
        printf("Failed To Create GLX context. \n");

		gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
	}
	
    else 
	{
		printf("OpenGL Context is Created.\n");
	}
	
	
	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Non HW Context Obtained \n");
	}
	else
	{
		printf("HW Context Obtained \n" );
	}
	
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	
    Result = glewInit();
    fprintf(gpFile, " Shader : %d",Result);


	//Depth
	
	Perspective_Projection_Matrix = mat4::identity();


    //for Ortho
    	//For Shaders : create 
   gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

    const GLchar *vertexShaderSourceCode =
        "#version 330 core" 		\
        "\n" 						\
		"in vec4 vPosition;"						\
		"in vec3 vNormal;"							\
		"out vec3 T_Norm;"							\
		"out vec3 Light_Direction;"					\
		"out vec3 Viewer_Vector;"					\
		"\n"										\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform vec3 u_LA;"						\
		"uniform vec3 u_LD;"						\
		"uniform vec3 u_LS;"						\
		"uniform vec3 u_KA;"						\
		"uniform vec3 u_KD;"						\
		"uniform vec3 u_KS;"						\
		"uniform float u_Shininess;"				\
		"uniform int u_LKeyPressed;"				\
		"uniform vec4 u_Light_Position;"			\
		"\n"										\
		"void main(void)"							\
		"{"											\
		"if(u_LKeyPressed == 1)"					\
		"{"											\
		"vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"	\
		"T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"			\
		"Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"	\
		"Viewer_Vector = vec3(-Eye_Coordinates.xyz);"\
		"}"		\
		"gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"	\
		"}" ;



    //compile
    glShaderSource(gVertex_Shader_Object,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    glCompileShader(gVertex_Shader_Object);
    
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char* szInfoLog = NULL;

    glGetShaderiv(gVertex_Shader_Object,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    
	if(iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gVertex_Shader_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        
		if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertex_Shader_Object,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }    

    //fragment shader
    gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar * gFragmentShaderSourceCode = 
        "#version 330 core"			\
        "\n"						\
		"in vec3 T_Norm;"			\
		"in vec3 Light_Direction;"	\
		"in vec3 Viewer_Vector;"	\
		"out vec4 FragColor;"		\
		"\n"						\
		"uniform vec3 u_LA;"						\
		"uniform vec3 u_LD;"						\
		"uniform vec3 u_LS;"						\
		"uniform vec3 u_KA;"						\
		"uniform vec3 u_KD;"						\
		"uniform vec3 u_KS;"						\
		"uniform float u_Shininess;"				\
		"uniform int u_LKeyPressed;"				\
		"uniform vec4 u_Light_Position;"			\
		"\n"										\
		"void main(void)"		\
		"{"						\
		"if(u_LKeyPressed == 1)"\
		"{"						\
		"vec3 Normalized_View_Vector = normalize(Viewer_Vector);"	\
		"vec3 Normalized_Light_Direction = normalize(Light_Direction);"\
		"vec3 Normalized_TNorm = normalize(T_Norm);"\
		"float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0);"	\
		"vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm);"\
		"vec3 Ambient = vec3(u_LA * u_KA);"	\
		"vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);"	\
		"vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess));" \
		"vec3 Phong_ADS_Light = Ambient + Diffuse+ Specular;"	\
		"FragColor = vec4(Phong_ADS_Light,1.0);" \
		"}"							\
		"else"						\
		"{"							\
		"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		"}"							\
		"}" ;



    glShaderSource(gFragment_Shader_Object,1,(const GLchar **)&gFragmentShaderSourceCode,NULL);
    glCompileShader(gFragment_Shader_Object);

    glGetShaderiv(gFragment_Shader_Object,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    
	if(iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gFragment_Shader_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        
		if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragment_Shader_Object,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s\n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }

    //shader program

    gShader_Program_Object = glCreateProgram();
    glAttachShader(gShader_Program_Object,gVertex_Shader_Object);
    glAttachShader(gShader_Program_Object,gFragment_Shader_Object);

    //bind in variables before linking
    glBindAttribLocation(gShader_Program_Object,AMC_ATTRIBUTE_POSITION,"vPosition");
    glBindAttribLocation(gShader_Program_Object, AMC_ATTRIBUTE_NORMAL, "vNormal");

    glLinkProgram(gShader_Program_Object);
    GLint iShaderProgramLinkStatus = 0;
    
	glGetProgramiv(gShader_Program_Object,GL_LINK_STATUS,&iShaderProgramLinkStatus);
    
	if(iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShader_Program_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShader_Program_Object,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Shader Program Link Log: %s\n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }

    //get uniform location
    mvpUniform = glGetUniformLocation(gShader_Program_Object,"u_model_matrix");
	Projection_Uniform = glGetUniformLocation(gShader_Program_Object, "u_projection_matrix");
	View_Uniform = glGetUniformLocation(gShader_Program_Object, "u_view_matrix");

	LD_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LD");
	KD_Uniform = glGetUniformLocation(gShader_Program_Object, "u_KD");

	LA_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LA");
	KA_Uniform = glGetUniformLocation(gShader_Program_Object, "u_KA");

	LS_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LS");
	KS_Uniform = glGetUniformLocation(gShader_Program_Object, "u_KS");

	LisPressed_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LKeyPressed");
	Light_Position_Uniform = glGetUniformLocation(gShader_Program_Object, "u_Light_Position");
	u_Material_Shininess = glGetUniformLocation(gShader_Program_Object, "u_Shininess");
	

    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	//drawing
	// vao
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);  

	// position vbo 
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	
	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    
    glBindVertexArray(0);    

	//Depth 
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
    Perspective_Projection_Matrix = mat4::identity();
    
    resize(giWindowWidth,giWindowHeight); //stray
}


void resize(int Width,int Height)
{
   if (Height == 0)
        Height = 1;
    
    glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
  
   	Perspective_Projection_Matrix = perspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);

}


void display(void)
{
    	//1
	mat4 Model_Matrix;
	mat4 View_Matrix;
	mat4 Projection_Matrix;
	mat4 Translation_Matrix;
	mat4 Rotation_Matrix;
	mat4 ModelView_Projection_Matrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShader_Program_Object);

	//code : 9 steps

	//2 : I[]
	Model_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	View_Matrix = mat4::identity();
	Rotation_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//3: Transform
	Translation_Matrix = translate(0.1f, 0.0f, -3.0f);

    //4 : Do Mat mul
	Model_Matrix = Translation_Matrix * Rotation_Matrix;



	// 5	: send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, Model_Matrix);
	
	glUniformMatrix4fv(View_Uniform, 1, GL_FALSE, View_Matrix);
	
	if (bLighting == true)
	{
		glUniform1i(LisPressed_Uniform, 1);
	
    	glUniform3f(LD_Uniform, 1.0f, 1.0f, 1.0f);

		glUniform3f(LS_Uniform, 1.0f, 1.0f, 1.0f);
		
		glUniform3f(LA_Uniform, 0.25f, 0.25f, 0.25f);
		
	//imp : out to in Light

        if(XPressed)
		glUniform4f(Light_Position_Uniform, 0.0f,sinf(Angle)*3 ,cosf(Angle)*3 , 1.0f);

        if(YPressed)
        glUniform4f(Light_Position_Uniform,sinf(Angle)*3 , 0.0f,cosf(Angle)*3 , 1.0f);

        if(ZPressed)
        glUniform4f(Light_Position_Uniform,sinf(Angle)*3 ,cosf(Angle)*3 , 0.0f, 1.0f);
	}

	else
	{
		glUniform1i(LisPressed_Uniform, 0);
	}

    //PPM
    glUniformMatrix4fv(Projection_Uniform, 1, GL_FALSE, Perspective_Projection_Matrix);
    
    //24 Sph

	GLfloat  Material_Ambient[72]; // {0.0215f,0.1745f,0.0215f,1.0f};
	GLfloat  Material_Diffuse[72]; // {0.0215f,0.1745f,0.0215f,1.0f};
	GLfloat  Material_Specular[72]; //  {1.0f,1.0f,1.0f,1.0f};
	GLfloat  Material_Shininess[24]; // {128.0f};
	

	//24 Spheres
// 1 : Emerald
	Material_Ambient[0] = 0.0215f;
	Material_Ambient[1] = 0.1745f;
	Material_Ambient[2] = 0.0215f;

	Material_Diffuse[0] = 0.0215f;
	Material_Diffuse[1] = 0.1745f;
	Material_Diffuse[2] = 0.0215f;
    //
    
	Material_Specular[0] = 0.633f;
	Material_Specular[1] = 0.727811f;
	Material_Specular[2] = 0.633f;

	Material_Shininess[0] = 0.6f * 128.0f;


// 2 = Jade
	Material_Ambient[3] = 0.135f;
	Material_Ambient[4] = 0.2225f;
	Material_Ambient[5] = 0.1575f;
	//
	///

	Material_Diffuse[3] = 0.54f;
	Material_Diffuse[4] = 0.89f;
	Material_Diffuse[5] = 0.63f;
    //
	//
    
	Material_Specular[3] = 0.316228f;
	Material_Specular[4] = 0.316228f;
	Material_Specular[5] = 0.316228f;
	//

	Material_Shininess[1] = 0.1f * 128.0f;


// 3 = obisidian
	Material_Ambient[6] = 0.05375f;
	Material_Ambient[7] = 0.05f;
	Material_Ambient[8] = 0.06625f;
	//

	Material_Diffuse[6] = 0.18275f;
	Material_Diffuse[7] = 0.17f;
	Material_Diffuse[8] = 0.22525f;
    //
	//
    
	Material_Specular[6] = 0.332741f;
	Material_Specular[7] = 0.328634f;
	Material_Specular[8] = 0.346435f;
	//
    //

	Material_Shininess[2] = 0.3f * 128.0f;
    //   

// 4= Pearl
	Material_Ambient[9] = 0.25f;
	Material_Ambient[10] = 0.20725f;
	Material_Ambient[11] = 0.20725f;
	//
	///

	Material_Diffuse[9] = 1.0f;
	Material_Diffuse[10] = 0.829f;
	Material_Diffuse[11] = 0.829f;
    //
	//
    
	Material_Specular[9] = 0.296648f;
	Material_Specular[10] = 0.296648f;
	Material_Specular[11] = 0.296648f;
	//
    //

	Material_Shininess[3] = 0.088f * 128.0f;
    //

// 5 = Ruby
	Material_Ambient[12] = 0.1745f;
	Material_Ambient[13] = 0.01175f;
	Material_Ambient[14] = 0.01175f;
	//
	///

	Material_Diffuse[12] = 0.61424f;
	Material_Diffuse[13] = 0.04136f;
	Material_Diffuse[14] = 0.04136f;
    //
    
	Material_Specular[12] = 0.727811f;
	Material_Specular[13] = 0.626959f;
	Material_Specular[14] = 0.626959f;
	//

	Material_Shininess[4] = 0.6f * 128.0f;
    //

// 6 = Turquoise
	Material_Ambient[15] = 0.1f;
	Material_Ambient[16] = 0.18275f;
	Material_Ambient[17] = 0.1745f;
	//
	///

	Material_Diffuse[15] = 0.396f;
	Material_Diffuse[16] = 0.74151f;
	Material_Diffuse[17] = 0.69102f;
    //
	//
    
	Material_Specular[15] = 0.297524f;
	Material_Specular[16] = 0.30829f;
	Material_Specular[17] = 0.306678f;
	//
    //

	Material_Shininess[5] = 0.1f * 128.0f;
    //
  

// Metals   ---------------------

// 1 : Brass
	Material_Ambient[18] = 0.349412f;
	Material_Ambient[19] = 0.30829f;
	Material_Ambient[20] = 0.306678f;
	///

	Material_Diffuse[18] = 0.780392f;
	Material_Diffuse[19] = 0.223529f;
	Material_Diffuse[20] = 0.027451f;
    //
	//
    
	Material_Specular[18] = 0.992157f;
	Material_Specular[19] = 0.941176f;
	Material_Specular[20] = 0.807843f;
	//
    //

	Material_Shininess[6] = 0.21794872f * 128.0f;
    //

// 2 = Bronze
	Material_Ambient[21] = 0.2125f;
	Material_Ambient[22] = 0.1275f;
	Material_Ambient[23] = 0.054f;
	//
	///

	Material_Diffuse[21] = 0.714f;
	Material_Diffuse[22] = 0.4284f;
	Material_Diffuse[23] = 0.18144f;
    //
	//
    
	Material_Specular[21] = 0.393548f;
	Material_Specular[22] = 0.271906f;
	Material_Specular[23] = 0.166721f;
	//
    //

	Material_Shininess[7] = 0.2f * 128.0f;
    //

// 3 = Chrome
	Material_Ambient[24] = 0.25f;
	Material_Ambient[25] = 0.25f;
	Material_Ambient[26] = 0.25f;
	//
	///

	Material_Diffuse[24] = 0.4f;
	Material_Diffuse[25] = 0.4f;
	Material_Diffuse[26] = 0.4f;
    //
	//
    
	Material_Specular[24] = 0.774597f;
	Material_Specular[25] = 0.774597f;
	Material_Specular[26] = 0.774597f;
	//
    //

	Material_Shininess[8] = 0.6f * 128.0f;
    //

	
// 4= copper
	Material_Ambient[27] = 0.19125f;
	Material_Ambient[28] = 0.0735f;
	Material_Ambient[29] = 0.02025f;
	//
	///

	Material_Diffuse[27] = 0.7038f;
	Material_Diffuse[28] = 0.27048f;
	Material_Diffuse[29] = 0.0828f;
    //
	//
    
	Material_Specular[27] = 0.25677f;
	Material_Specular[28] = 0.137622f;
	Material_Specular[29] = 0.086014f;
	//
    //

	Material_Shininess[9] = 0.1f * 128.0f;


// 5 = Gold
	Material_Ambient[30] = 0.24725f;
	Material_Ambient[31] = 0.1995f;
	Material_Ambient[32] = 0.0745f;
	//
	///

	Material_Diffuse[30] = 0.7517f;
	Material_Diffuse[31] = 0.6065f;
	Material_Diffuse[32] = 0.2265f;
    //
    
	Material_Specular[30] = 0.6283f;
	Material_Specular[31] = 0.55580f;
	Material_Specular[32] = 0.36606f;
	//

	Material_Shininess[10] = 0.4f * 128.0f;
    //

// 6 = Silver
	Material_Ambient[33] = 0.19225f;
	Material_Ambient[34] = 0.19225f;
	Material_Ambient[35] = 0.19225f;
	//

	Material_Diffuse[33] = 0.5075f;
	Material_Diffuse[34] = 0.5075f;
	Material_Diffuse[35] = 0.5075f;
    //
	//
    
	Material_Specular[33] = 0.50828f;
	Material_Specular[34] = 0.50828f;
	Material_Specular[35] = 0.50828f;
	//
    //

	Material_Shininess[11] = 0.4f * 128.0f;
    //


// Plastic ---------------------
// 1 - Black
	Material_Ambient[36] = 0.0f;
	Material_Ambient[37] = 0.0f;
	Material_Ambient[38] = 0.0f;
	//
	///

	Material_Diffuse[36] = 0.01f;
	Material_Diffuse[37] = 0.01f;
	Material_Diffuse[38] = 0.01f;
    //
	//
    
	Material_Specular[36] = 0.5f;
	Material_Specular[37] = 0.5f;
	Material_Specular[38] = 0.5f;
	//
    //

	Material_Shininess[12] = 0.25f * 128.0f;
    //


// 2 - Cyan
	Material_Ambient[39] = 0.0f;
	Material_Ambient[40] = 0.1f;
	Material_Ambient[41] = 0.06f;
	//
	///

	Material_Diffuse[39] = 0.0f;
	Material_Diffuse[40] = 0.5098039f;
	Material_Diffuse[41] = 0.5098039f;
    //
	//
    
	Material_Specular[39] = 0.0f;
	Material_Specular[40] = 0.501960f;
	Material_Specular[41] = 0.501960f;
	//

	Material_Shininess[13] = 0.25f * 128.0f;
    //

//3 - Green
	Material_Ambient[42] = 0.0f;
	Material_Ambient[43] = 0.1f;
	Material_Ambient[44] = 0.06f;
	//
	///

	Material_Diffuse[42] = 0.1f;
	Material_Diffuse[43] = 0.35f;
	Material_Diffuse[44] = 0.1f;
    //
	//
    
	Material_Specular[42] = 0.45f;
	Material_Specular[43] = 0.55f;
	Material_Specular[44] = 0.45f;
	//
    //

	Material_Shininess[14] = 0.25f * 128.0f;
    //


// Red

	Material_Ambient[45] = 0.0f;
	Material_Ambient[46] = 0.0f;
	Material_Ambient[47] = 0.0f;
	//
	///

	Material_Diffuse[45] = 0.5f;
	Material_Diffuse[46] = 0.0f;
	Material_Diffuse[47] = 0.0f;
    //
	//
    
	Material_Specular[45] = 0.7f;
	Material_Specular[46] = 0.6f;
	Material_Specular[47] = 0.6f;
	//
    //

	Material_Shininess[15] = 0.25f * 128.0f;


// White
	Material_Ambient[48] = 0.0f;
	Material_Ambient[49] = 0.0f;
	Material_Ambient[50] = 0.0f;
	//
	///

	Material_Diffuse[48] = 0.55f;
	Material_Diffuse[49] = 0.55f;
	Material_Diffuse[50] = 0.55f;
    //
	//
    
	Material_Specular[48] = 0.7f;
	Material_Specular[49] = 0.7f;
	Material_Specular[50] = 0.7f;
	//
    //

	Material_Shininess[16] = 0.25f * 128.0f;
    //


// Yellow
	Material_Ambient[51] = 0.0f;
	Material_Ambient[52] = 0.0f;
	Material_Ambient[53] = 0.0f;
	//
	///

	Material_Diffuse[51] = 0.5f;
	Material_Diffuse[52] = 0.5f;
	Material_Diffuse[53] = 0.0f;
    
	Material_Specular[51] = 0.6f;
	Material_Specular[52] = 0.6f;
	Material_Specular[53] = 0.5f;

	Material_Shininess[17] = 0.25f * 128.0f;


// -------- Rubber
// 1 - Black
	Material_Ambient[54] = 0.02f;
	Material_Ambient[55] = 0.02f;
	Material_Ambient[56] = 0.02f;
	//
	///

	Material_Diffuse[54] = 0.01f;
	Material_Diffuse[55] = 0.01f;
	Material_Diffuse[56] = 0.01f;
    //
	//
    
	Material_Specular[54] = 0.4f;
	Material_Specular[55] = 0.4f;
	Material_Specular[56] = 0.4f;
	//
    //

	Material_Shininess[18] = 0.07813f * 128.0f;
    //


// 2 - Cyan
	Material_Ambient[57] = 0.0f;
	Material_Ambient[58] = 0.05f;
	Material_Ambient[59] = 0.05f;
	//
	///

	Material_Diffuse[57] = 0.4f;
	Material_Diffuse[58] = 0.5098039f;
	Material_Diffuse[59] = 0.5098039f;
    //
	//
    
	Material_Specular[57] = 0.0f;
	Material_Specular[58] = 0.501960f;
	Material_Specular[59] = 0.501960f;
	//
    //

	Material_Shininess[19] = 0.07813f * 128.0f;
    //

	//
	
//3 - Green
	Material_Ambient[60] = 0.0f;
	Material_Ambient[61] = 0.1f;
	Material_Ambient[62] = 0.06f;
	//
	///

	Material_Diffuse[60] = 0.1f;
	Material_Diffuse[61] = 0.35f;
	Material_Diffuse[62] = 0.1f;
    //
	//
    
	Material_Specular[60] = 0.45f;
	Material_Specular[61] = 0.55f;
	Material_Specular[62] = 0.45f;
	//
    //

	Material_Shininess[20] = 0.07813f * 128.0f;
    //

// 4- Red

	Material_Ambient[63] = 0.0f;
	Material_Ambient[64] = 0.0f;
	Material_Ambient[65] = 0.0f;
	//
	///

	Material_Diffuse[63] = 0.5f;
	Material_Diffuse[64] = 0.0f;
	Material_Diffuse[65] = 0.0f;
    //
	//
    
	Material_Specular[63] = 0.7f;
	Material_Specular[64] = 0.6f;
	Material_Specular[65] = 0.6f;
	//
    //

	Material_Shininess[21] = 0.07813f * 128.0f;
    //

// 5- White
	Material_Ambient[66] = 0.0f;
	Material_Ambient[67] = 0.0f;
	Material_Ambient[68] = 0.0f;
	//
	///

	Material_Diffuse[66] = 0.55f;
	Material_Diffuse[67] = 0.55f;
	Material_Diffuse[68] = 0.55f;
    //
	//
    
	Material_Specular[66] = 0.7f;
	Material_Specular[67] = 0.7f;
	Material_Specular[68] = 0.7f;
	//
    //

	Material_Shininess[22] = 0.07813f * 128.0f;

// 6- Yellow
	Material_Ambient[69] = 0.0f;
	Material_Ambient[70] = 0.0f;
	Material_Ambient[71] = 0.0f;

	Material_Diffuse[69] = 0.5f;
	Material_Diffuse[70] = 0.5f;
	Material_Diffuse[71] = 0.0f;
    
	Material_Specular[69] = 0.6f;
	Material_Specular[70] = 0.6f;
	Material_Specular[71] = 0.5f;

	Material_Shininess[23] = 0.07813f * 128.0f;


	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	//  glDrawArrays() 
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);

	//imp 
                
	int width = (GLint)(winWidth / 6);
	int height = (GLint)(winHeight / 4);

  	int i,j;

  // material : i*3+j
  for(i = 0; i <= 5; i++) //x
   {
    for(j = 0; j <= 3; j++) //y
	{
		//1
		glUniform1f(u_Material_Shininess,Material_Shininess[i*j]);
		
		glUniform3f(KS_Uniform,Material_Specular[3*i+j],Material_Specular[3*i+j+1],Material_Specular[3*i+j+2]);		
		glUniform3f(KD_Uniform,Material_Diffuse[3*i+j],Material_Diffuse[3*i+j+1],Material_Diffuse[3*i+j+2]);
		glUniform3f(KA_Uniform,Material_Ambient[3*i+j],Material_Ambient[3*i+j+1],Material_Ambient[3*i+j+2]);

		//2
		glViewport(width*i, height*j, width, height);	

		Perspective_Projection_Matrix = perspective(45.0f, (GLfloat)(width) / (GLfloat)(height), 0.1f, 100.0f);

		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	}

 }


	// *** unbind vao ***
	glBindVertexArray(0);

	//Common
	glUseProgram(0);

    glXSwapBuffers(gpDisplay,gWindow);
}


//new
void update(void)
{
    Angle += 0.01f;

	if (Angle >= 360.0f)
		Angle -= 360.0f;
}


void uninitialize(void)
{
    GLXContext currentGLXContext;
    currentGLXContext =glXGetCurrentContext();

    	//for Shaders
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

    	if (gVbo_sphere_normal)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}


	glUseProgram(gShader_Program_Object);
	glDetachShader(gShader_Program_Object, gFragment_Shader_Object);
	glDetachShader(gShader_Program_Object, gVertex_Shader_Object);

	glDeleteShader(gFragment_Shader_Object);
	gFragment_Shader_Object = 0;

	glDeleteShader(gVertex_Shader_Object);
	gVertex_Shader_Object = 0;

	glDeleteProgram(gShader_Program_Object);
	glUseProgram(0);


    if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,0,0);
    }

    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay,gGLXContext);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay,gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }

    if (gpFile)
    {
        fprintf(gpFile, "Log File successfully Closed.\n");
        fclose(gpFile);
        gpFile = NULL;
    }

}

