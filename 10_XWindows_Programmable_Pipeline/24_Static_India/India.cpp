/*
    Triangle
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include "GL/glew.h"
#include <GL/gl.h>
#include <GL/glx.h>
#include "vmath.h"

using namespace std;
using namespace vmath;

FILE* gpFile = NULL;

GLenum Result;
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

//For Shaders
GLuint gVertex_Shader_Object;
GLuint gFragment_Shader_Object;
GLuint gShader_Program_Object;

GLint iShaderCompileStatus = 0;
GLint iInfoLogLength = 0;
GLchar *szInfoLog = NULL;
GLint iProgrmLinkStatus = 0;

//2d
//Vao
GLuint vao_I , vbo_IP,vbo_IC;

GLuint vao_N , vbo_NP,vbo_NC;

GLuint vao_D , vbo_DP,vbo_DC;

GLuint vao_A , vbo_AP,vbo_AC;

GLuint mvpUniform;
mat4 Perspective_Projection_Matrix;


//ENUM
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

//4 Globals
typedef GLXContext (*glXCreateContextAttribsARBProc)
                    (Display*, 
                     GLXFBConfig, 
                     GLXContext, 
                     Bool, 
                     const int*);

glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL; //imp

GLXContext gGLXContext;

GLXFBConfig gGLXFBConfig;

//func
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void initialize(void);
    void display(void);
    void resize(int,int);
    void update(void);
    void uninitialize(void);
  

int main(void)
{
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

    bool bDone = false;

    gpFile = fopen("Log.txt", "w");
  
    CreateWindow();

    initialize();

    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
             XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                break;
               
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
      
                        case XK_F:
                        case XK_f:
                            if(bFullscreen == false)
                            {
                                ToggleFullscreen();
                                bFullscreen = true;
                            }
                            else
                            {
                                ToggleFullscreen();
                                bFullscreen = false;
                            }
                            break;
      
                        default:
                        break;
                    }
                break;
      
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                        break;
                        case 2:
                        break;
                        case 3:
                        break;
                        default:
                        break;
                    }
                break;
      
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth,winHeight);
                break;
      
                case Expose:
                break;
      
                case DestroyNotify:
                break;
      
                case 33:
                    uninitialize();
                    exit(0);
                break;
      
                default:
                break;
            }
        }
        
        update(); //2

        display(); //most imp
    }
   	uninitialize(); //safe
    return(0);
}


void CreateWindow(void)
{

    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    //new
    GLXFBConfig *pGLXFBConfigs = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumFBConfigs=0;

    
    static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		GLX_STENCIL_SIZE,8,
		GLX_DOUBLEBUFFER,True,
		None
        }; 
	
    //imp
   gpDisplay = XOpenDisplay(NULL);
    
  if(gpDisplay == NULL)
    {
        printf("ERROR: Unable To Open X Display.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    //most imp : get Best Context
    //1
    pGLXFBConfigs = glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);
	
    if(pGLXFBConfigs==NULL)
	{
		printf( "Failed To Get Valid Framebuffer Config.\n");
		uninitialize();
		exit(1);
	}
	
    printf("%d Matching FB Configs Found.\n",iNumFBConfigs);
	
    int bestFramebufferconfig = -1;
    int worstFramebufferConfig = -1;
    int bestNumberOfSamples = -1;
    int worstNumberOfSamples = 999;
	
    for(int i=0;i<iNumFBConfigs;i++)
	{
        //3rd way to get visual
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
		
        if(pTempXVisualInfo)
		{
			int sampleBuffer,samples;

            // get no of sample Buffers from each fb
        	glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
		
        	glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLES,&samples);
		
        	printf("Matching Framebuffer Config=%d \n",i);
		
            // Like sorting
			if(bestFramebufferconfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferconfig=i;
				bestNumberOfSamples=samples;
			}
			
            if( worstFramebufferConfig < 0 || (!sampleBuffer) || samples < worstNumberOfSamples)
			{
				worstFramebufferConfig=i;
			    worstNumberOfSamples=samples;
			}
		}
		XFree(pTempXVisualInfo);
	}
	
    //Get Best Visual
    bestGLXFBConfig = pGLXFBConfigs[bestFramebufferconfig];

	gGLXFBConfig = bestGLXFBConfig;
	
	XFree(pGLXFBConfigs);
	
	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
    
    if(gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable To Get XVisualInfo.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
        RootWindow(gpDisplay,gpXVisualInfo->screen),
        gpXVisualInfo->visual,
        AllocNone);

    gColormap = winAttribs.colormap;
    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);  
           
    winAttribs.event_mask = ExposureMask| VisibilityChangeMask | ButtonPressMask | KeyPressMask 
                            | PointerMotionMask | StructureNotifyMask;
    
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap ; 

    gWindow = XCreateWindow(gpDisplay,
                RootWindow(gpDisplay,gpXVisualInfo->screen),
                0,0,giWindowWidth,
                giWindowHeight,
                0,
                gpXVisualInfo->depth,
                InputOutput,
                gpXVisualInfo->visual,
                styleMask,&winAttribs);

        if(!gWindow)
        {
            printf("ERROR: Failed To Create Window.\n Exitting Now...\n");
            uninitialize();
            exit(1);
        }

    XStoreName(gpDisplay,gWindow,"XWindows Static India");
    
    Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
    XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

    XMapWindow(gpDisplay,gWindow);         // imp
}


//Best
void ToggleFullscreen(void)
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 :1;
    fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev); 
}

//imp
void initialize(void)
{

    //new 
    GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB,3,
		GLX_CONTEXT_MINOR_VERSION_ARB,1,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None }; 		
	
    //Take & get fun pointer
    glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

    if(glXCreateContextAttribsARB == NULL)
    {
        printf("exit from ARB");
        uninitialize();
        exit(1);
    }

    gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);

	if(!gGLXContext) 
	{
		GLint attribs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			0 }; 
		
        printf("Failed To Create GLX context. \n");

		gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
	}
	
    else 
	{
		printf("OpenGL Context is Created.\n");
	}
	
	
	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Non HW Context Obtained \n");
	}
	else
	{
		printf("HW Context Obtained \n" );
	}
	
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	
    Result = glewInit();
    fprintf(gpFile, " Shader : %d",Result);


	//Depth
	
	Perspective_Projection_Matrix = mat4::identity();


    //for Ortho
    	//For Shaders : create 
   gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

    const GLchar *vertexShaderSourceCode =
        "#version 330 core" 		\
        "\n" 						\
        "in vec4 vPosition;"						\
		"in vec4 vColor;"							\
		"out vec4 Out_Color;"						\
		"uniform mat4 u_mvp_matrix;"				\
		"void main(void)"							\
		"{"											\
		"gl_Position = u_mvp_matrix * vPosition;"	\
		"Out_Color = vColor;"						\
		"}" ;


    glShaderSource(gVertex_Shader_Object,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    glCompileShader(gVertex_Shader_Object);
    
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char* szInfoLog = NULL;

    glGetShaderiv(gVertex_Shader_Object,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    
	if(iShaderCompiledStatus ==GL_FALSE)
    {
        glGetShaderiv(gVertex_Shader_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        
		if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertex_Shader_Object,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }    

    //fragment shader
    gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar * gFragmentShaderSourceCode = 
    "#version 330 core"		 \
    "\n"					 \
    "in vec4 Out_Color;"	 \
    "out vec4 FragColor;"	 \
    "void main(void)"		 \
    " { "					 \
    "FragColor = Out_Color;" \
    " } " ;


    glShaderSource(gFragment_Shader_Object,1,(const GLchar **)&gFragmentShaderSourceCode,NULL);
    glCompileShader(gFragment_Shader_Object);

    glGetShaderiv(gFragment_Shader_Object,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    
	if(iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gFragment_Shader_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        
		if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragment_Shader_Object,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s\n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }

    //shader program

    gShader_Program_Object = glCreateProgram();
    glAttachShader(gShader_Program_Object,gVertex_Shader_Object);
    glAttachShader(gShader_Program_Object,gFragment_Shader_Object);

    //bind in variables before linking
    glBindAttribLocation(gShader_Program_Object,AMC_ATTRIBUTE_POSITION,"vPosition");
    glBindAttribLocation(gShader_Program_Object,AMC_ATTRIBUTE_COLOR,"vColor");



    glLinkProgram(gShader_Program_Object);
    GLint iShaderProgramLinkStatus = 0;
    
	glGetProgramiv(gShader_Program_Object,GL_LINK_STATUS,&iShaderProgramLinkStatus);
    
	if(iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShader_Program_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShader_Program_Object,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Shader Program Link Log: %s\n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }

    //get uniform location
    mvpUniform = glGetUniformLocation(gShader_Program_Object,"u_mvp_matrix");



	//Depth 
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
// N
	const GLfloat N_Vertices[] = 	
	{ 
		//I
		 0.3f, 0.5f, 0.0f,
		 0.2f, 0.5f, 0.0f,
		 0.2f, -0.5f, 0.0f,
		 0.3f, -0.5f, 0.0f,

		 -0.2f, 0.5f, 0.0f,
		 -0.3f, 0.5f, 0.0f,
		 -0.3f, -0.5f, 0.0f,
		 -0.2f, -0.5f, 0.0f,

		//I
		 -0.1f, 0.5f, 0.0f,
		 -0.22f, 0.5f, 0.0f,
		 0.1f, -0.5f, 0.0f,
		 0.22f, -0.5f, 0.0f

	};

	const GLfloat N_Colors[] = 
	{ 	1.0f,0.5f,0.0f,
		1.0f,1.0f,1.0f,
		1.0f,1.0f,1.0f,
		0.0f,1.0f,0.0f ,

		1.0f,0.5f,0.0f,
		1.0f,1.0f,1.0f,
		1.0f,1.0f,1.0f,
		0.0f,1.0f,0.0f,

		1.0f,0.5f,0.0f,
		1.0f,1.0f,1.0f,
		1.0f,1.0f,1.0f,
		0.0f,1.0f,0.0f 

	};

	//create vao
	glGenVertexArrays(1, &vao_N);

	//Binding
	glBindVertexArray(vao_N);

//Pos
	glGenBuffers(1, &vbo_NP);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_NP);

	glBufferData(GL_ARRAY_BUFFER, sizeof(N_Vertices),
				N_Vertices,GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

//Color
	//Buffer
	glGenBuffers(1, &vbo_NC);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_NC);

	glBufferData(GL_ARRAY_BUFFER, sizeof(N_Colors),
				N_Colors,GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//vao
	glBindVertexArray(0);



//	I
const GLfloat I_Vertices[] = 
	{ 
		0.05f, 0.5f, 0.0f,
		-0.05f, 0.5f, 0.0f,
		-0.05f, -0.5f, 0.0f,
		0.05f, -0.5f, 0.0f
	 };

	const GLfloat I_Colors[] = 
	{ 	1.0f,0.5f,0.0f,
		1.0f,1.0f,1.0f,
		1.0f,1.0f,1.0f,
		0.0f,1.0f,0.0f 
	};

	//create vao
	glGenVertexArrays(1, &vao_I);

	//Binding
	glBindVertexArray(vao_I);

//Pos
	glGenBuffers(1, &vbo_IP);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_IP);

	glBufferData(GL_ARRAY_BUFFER, sizeof(I_Vertices),
				I_Vertices,GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

//Color
	//Buffer
	glGenBuffers(1, &vbo_IC);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_IC);

	glBufferData(GL_ARRAY_BUFFER, sizeof(I_Colors),
				I_Colors,GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//vao
	glBindVertexArray(0);


//D :  vao_D , vbo_DP,vbo_DC

const GLfloat D_Vertices[] = 
	{ 
		// I
		-0.3f, 0.5f, 0.0f,
		-0.2f, 0.5f, 0.0f,
		-0.2f, -0.5f, 0.0f,
		-0.3f, -0.5f, 0.0f,

		// D
		-0.2f, 0.4f, 0.0f,
		-0.2f, 0.5f, 0.0f,
		0.0f, 0.4f, 0.0f,
		0.0f, 0.5f, 0.0f,

		0.15f, 0.3f, 0.0f,
		0.25f, 0.36f, 0.0f,
		0.15f, -0.3f, 0.0f ,
		0.25f, -0.36f, 0.0f ,

		0.0f, -0.4f, 0.0f ,
		0.0f, -0.5f, 0.0f ,
		-0.2f, -0.4f, 0.0f ,
		-0.2f, -0.5f, 0.0f
	 };

  const GLfloat D_Colors[] = 
	{ 	1.0f,0.5f,0.0f,
		1.0f,1.0f,1.0f,
		1.0f,1.0f,1.0f,
		0.0f,1.0f,0.0f,

		1.0f,0.5f,0.0f,
		1.0f,0.5f,0.0f,
		1.0f,0.5f,0.0f,
		1.0f,0.5f,0.0f,

		1.0f,1.0f,1.0f,
		1.0f,1.0f,1.0f,
		1.0f,1.0f,1.0f,
		1.0f,1.0f,1.0f,

		0.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,0.0f,0.0f //for look
	};

	//create vao
	glGenVertexArrays(1, &vao_D);

	//Binding
	glBindVertexArray(vao_D);

//Pos
	glGenBuffers(1, &vbo_DP);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_DP);

	glBufferData(GL_ARRAY_BUFFER, sizeof(D_Vertices),
				D_Vertices,GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

//Color
	//Buffer
	glGenBuffers(1, &vbo_DC);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_DC);

	glBufferData(GL_ARRAY_BUFFER, sizeof(D_Colors),
				D_Colors,GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//vao
	glBindVertexArray(0);


//	A  vao_A , vbo_AP,vbo_AC;

	const GLfloat A_Vertices[] =
	{
		//strips
		 1.32f, 0.3f, 0.0f,
		 1.05f, 0.3f, 0.0f,
	     1.05f, 0.2f, 0.0f,
		 1.32f, 0.2f, 0.0f,

		 //W
		1.32f, 0.26f, 0.0f,
		1.05f, 0.26f, 0.0f,
		1.05f, 0.24f, 0.0f,
		1.32f, 0.24f, 0.0f,


		// A 4
		 1.05f, 0.5f, 0.0f,
		 0.5f,  0.6f, 0.0f,
	     0.5f, -0.5f, 0.0f,
		 1.05f, -0.6f, 0.0f
	};

	const GLfloat A_Colors[] =
	{	
		//4
		1.0f, 0.5f, 0.0f,
		1.0f, 0.5f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		// W
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		
		//4
	    1.0f,0.5f,0.0f,
		1.0f,1.0f,1.0f,
		1.0f,1.0f,1.0f,
		0.0f,1.0f,0.0f 
	};


//create vao
	glGenVertexArrays(1, &vao_A);

	glBindVertexArray(vao_A);

//Pos
	glGenBuffers(1, &vbo_AP);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_AP);

	glBufferData(GL_ARRAY_BUFFER, sizeof(A_Vertices), A_Vertices,GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

//Color
	//Buffer
	glGenBuffers(1, &vbo_AC);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_AC);

	glBufferData(GL_ARRAY_BUFFER, sizeof(A_Colors),	A_Colors,GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//vao
	glBindVertexArray(0);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
    resize(giWindowWidth,giWindowHeight); //stray
}


void resize(int Width,int Height)
{
   if (Height == 0)
        Height = 1;
    
    glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
  
   	Perspective_Projection_Matrix = perspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);

}


void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //1

   	mat4 ModelView_Matrix;
	mat4 ModelView_Projection_Matrix;
    
	glUseProgram(gShader_Program_Object);

    // I 
	//2 : I[]
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();

	//3 : Transforms
	ModelView_Matrix = translate(-2.5f, 0.0f, -5.0f);

	//4 : Do Mat mul
	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;

	// 5 : send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	// 6 : bind to vao
	glBindVertexArray(vao_I);

	//8 : draw scene
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	//9 : Unbind vao
	glBindVertexArray(0);


//	N
//2 : I[]
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();

	//3 : Transforms
	ModelView_Matrix = translate(-1.2f, 0.0f, -5.0f);

	//4 : Do Mat mul
	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;

	// 5 : send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	// 6 : bind to vao
	glBindVertexArray(vao_N);

	//8 : draw scene
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);

	//9 : Unbind vao
	glBindVertexArray(0);


//	D vao_D
//2 : I[]
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();

	//3 : Transforms
	ModelView_Matrix = translate(0.2f, 0.0f, -5.0f);

	//4 : Do Mat mul
	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;

	// 5 : send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	// 6 : bind to vao
	glBindVertexArray(vao_D);

	//8 : draw scene : Imp

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_STRIP, 4,12);

	//9 : Unbind vao
	glBindVertexArray(0);


// I	I
//2 : I[]
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();

	//3 : Transforms
	ModelView_Matrix = translate(1.4f, 0.0f, -5.0f);

	//4 : Do Mat mul
	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;

	// 5 : send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	// 6 : bind to vao
	glBindVertexArray(vao_I);

	//8 : draw scene
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	//9 : Unbind vao
	glBindVertexArray(0);


// A : Vao_A
	mat4 scaleA , rotateA;

// 1
	scaleA = mat4::identity();
	rotateA = mat4::identity();
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();

	//3 : Transforms
	ModelView_Matrix = translate(1.25f, -0.25f, -5.4f);

	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	glBindVertexArray(vao_A);

	//A
	glDrawArrays(GL_TRIANGLE_FAN, 0,4);
	glDrawArrays(GL_TRIANGLE_FAN, 4,4);

	glBindVertexArray(0);

//2 
	scaleA = mat4::identity();
	rotateA = mat4::identity();
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();

	//3 : Transforms
	ModelView_Matrix = translate(2.0f, -0.2f, -5.4f);
    rotateA = rotate(45.0f,1.0f,1.0f,0.0f);
	scaleA = scale(0.65f,0.95f,1.0f);

	ModelView_Matrix *= scaleA * rotateA;

	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	glBindVertexArray(vao_A);

	//A
	glDrawArrays(GL_TRIANGLE_FAN, 8,4);

	glBindVertexArray(0);


// Reverse
//3
	scaleA = mat4::identity();
	rotateA = mat4::identity();
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();

	//3 : Transforms
	ModelView_Matrix = translate(2.72f, 0.3f, -5.4f);
	rotateA = rotate(310.0f,1.0f,-1.0f,0.0f);
	scaleA = scale(0.0f,0.9f,0.40f);

	ModelView_Matrix *= scaleA * rotateA;

	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	glBindVertexArray(vao_A);

	//A
	glDrawArrays(GL_TRIANGLE_FAN,8,4);

	glBindVertexArray(0);

	glUseProgram(0);

    glXSwapBuffers(gpDisplay,gWindow);
}


//new
void update(void)
{

}


void uninitialize(void)
{
    GLXContext currentGLXContext;
    currentGLXContext =glXGetCurrentContext();

		//for Shaders

	glUseProgram(gShader_Program_Object);
	glDetachShader(gShader_Program_Object, gFragment_Shader_Object);
	glDetachShader(gShader_Program_Object, gVertex_Shader_Object);

	glDeleteShader(gFragment_Shader_Object);
	gFragment_Shader_Object = 0;

	glDeleteShader(gVertex_Shader_Object);
	gVertex_Shader_Object = 0;

	glDeleteProgram(gShader_Program_Object);
	glUseProgram(0);


    if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,0,0);
    }

    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay,gGLXContext);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay,gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }

    if (gpFile)
    {
        fprintf(gpFile, "Log File successfully Closed.\n");
        fclose(gpFile);
        gpFile = NULL;
    }

}

