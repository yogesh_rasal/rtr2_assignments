/*
    Smiley
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include "GL/glew.h"
#include <GL/gl.h>
#include <GL/glx.h>
#include "vmath.h"
#include <SOIL/SOIL.h>

using namespace std;
using namespace vmath;

FILE* gpFile = NULL;

GLenum Result;
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

//For Shaders
GLuint gVertex_Shader_Object;
GLuint gFragment_Shader_Object;
GLuint gShader_Program_Object;

GLint iShaderCompileStatus = 0;
GLint iInfoLogLength = 0;
GLchar *szInfoLog = NULL;
GLint iProgrmLinkStatus = 0;


//Shaders
GLuint vao_Rectagle;
GLuint vbo_Position_Rectangle;
GLuint vbo_Texture;
GLuint Texture_sampler_uniform;

GLuint Texture_SMILEY;

int u_PressedKey,PressedKey;

GLuint mvpUniform;
mat4 Perspective_Projection_Matrix;

//ENUM
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

//4 Globals
typedef GLXContext (*glXCreateContextAttribsARBProc)
                    (Display*, 
                     GLXFBConfig, 
                     GLXContext, 
                     Bool, 
                     const int*);

glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL; //imp

GLXContext gGLXContext;

GLXFBConfig gGLXFBConfig;

//func
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void initialize(void);
    void display(void);
    void resize(int,int);
    void update(void);
    void uninitialize(void);
    GLuint LoadTexture(const char *,GLuint*);  

int main(void)
{
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

    bool bDone = false;

    gpFile = fopen("Log.txt", "w");
  
    fprintf(gpFile,"\n Open Log");

    CreateWindow();

    initialize();

    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
             XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                break;
               
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
      
                        case XK_F:
                        case XK_f:
                            if(bFullscreen == false)
                            {
                                ToggleFullscreen();
                                bFullscreen = true;
                            }
                            else
                            {
                                ToggleFullscreen();
                                bFullscreen = false;
                            }
                            break;

                        case XK_0:
                            PressedKey = 0;
                            break;

                        case XK_1:
                            PressedKey = 1;
                            break;

                        case XK_2:
                            PressedKey = 2;
                            break;

                        case XK_3:
                            PressedKey = 3;
                            break;

                        case XK_4:
                            PressedKey = 4;
                            break;

                        default:
                        break;
                    }
                break;
      
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                        break;
                        case 2:
                        break;
                        case 3:
                        break;
                        default:
                        break;
                    }
                break;
      
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth,winHeight);
                break;
      
                case Expose:
                break;
      
                case DestroyNotify:
                break;
      
                case 33:
                    uninitialize();
                    exit(0);
                break;
      
                default:
                break;
            }
        }
        
        update(); //2

        display(); //most imp
    }
   	uninitialize(); //safe
    return(0);
}


void CreateWindow(void)
{

    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    //new
    GLXFBConfig *pGLXFBConfigs = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumFBConfigs=0;

    
    static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		GLX_STENCIL_SIZE,8,
		GLX_DOUBLEBUFFER,True,
		None
        }; 
	
    //imp
   gpDisplay = XOpenDisplay(NULL);
    
  if(gpDisplay == NULL)
    {
        printf("ERROR: Unable To Open X Display.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    //most imp : get Best Context
    //1
    pGLXFBConfigs = glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);
	
    if(pGLXFBConfigs==NULL)
	{
		printf( "Failed To Get Valid Framebuffer Config.\n");
		uninitialize();
		exit(1);
	}
	
    printf("%d Matching FB Configs Found.\n",iNumFBConfigs);
	
    int bestFramebufferconfig = -1;
    int worstFramebufferConfig = -1;
    int bestNumberOfSamples = -1;
    int worstNumberOfSamples = 999;
	
    for(int i=0;i<iNumFBConfigs;i++)
	{
        //3rd way to get visual
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
		
        if(pTempXVisualInfo)
		{
			int sampleBuffer,samples;

            // get no of sample Buffers from each fb
        	glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
		
        	glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLES,&samples);
		
        	printf("Matching Framebuffer Config=%d \n",i);
		
            // Like sorting
			if(bestFramebufferconfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferconfig=i;
				bestNumberOfSamples=samples;
			}
			
            if( worstFramebufferConfig < 0 || (!sampleBuffer) || samples < worstNumberOfSamples)
			{
				worstFramebufferConfig=i;
			    worstNumberOfSamples=samples;
			}
		}
		XFree(pTempXVisualInfo);
	}
	
    //Get Best Visual
    bestGLXFBConfig = pGLXFBConfigs[bestFramebufferconfig];

	gGLXFBConfig = bestGLXFBConfig;
	
	XFree(pGLXFBConfigs);
	
	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
    
    if(gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable To Get XVisualInfo.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
        RootWindow(gpDisplay,gpXVisualInfo->screen),
        gpXVisualInfo->visual,
        AllocNone);

    gColormap = winAttribs.colormap;
    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);  
           
    winAttribs.event_mask = ExposureMask| VisibilityChangeMask | ButtonPressMask | KeyPressMask 
                            | PointerMotionMask | StructureNotifyMask;
    
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap ; 

    gWindow = XCreateWindow(gpDisplay,
                RootWindow(gpDisplay,gpXVisualInfo->screen),
                0,0,giWindowWidth,
                giWindowHeight,
                0,
                gpXVisualInfo->depth,
                InputOutput,
                gpXVisualInfo->visual,
                styleMask,&winAttribs);

        if(!gWindow)
        {
            printf("ERROR: Failed To Create Window.\n Exitting Now...\n");
            uninitialize();
            exit(1);
        }

    XStoreName(gpDisplay,gWindow,"Tweaked Smiley : XWindows");
    
    Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
    XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

    XMapWindow(gpDisplay,gWindow);         // imp
}


//Best
void ToggleFullscreen(void)
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 :1;
    fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev); 
}

//imp
void initialize(void)
{
    //new 
    GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB,3,
		GLX_CONTEXT_MINOR_VERSION_ARB,1,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None }; 		
	
    //Take & get fun pointer
    glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

    if(glXCreateContextAttribsARB == NULL)
    {
        printf("exit from ARB");
        uninitialize();
        exit(1);
    }

    gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);

	if(!gGLXContext) 
	{
		GLint attribs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			0 }; 
		
        printf("Failed To Create GLX context. \n");

		gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
	}
	
    else 
	{
		printf("OpenGL Context is Created.\n");
	}
	
	
	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Non HW Context Obtained \n");
	}
	else
	{
		printf("HW Context Obtained \n" );
	}
	
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	
    Result = glewInit();
    fprintf(gpFile, " Shader : %d",Result);


	//Depth
	Perspective_Projection_Matrix = mat4::identity();

   //For Shaders : create 
    gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

    const GLchar *vertexShaderSourceCode =
        "#version 330 core" 		    \
        "\n" 						    \
   	    "in vec4 vPosition;"			\
		"in vec2 vTexture_Coord;"		\
		"uniform mat4 u_mvp_matrix;"	\
		"out vec2 out_texture_coord;"	\
		"\n"							\
		"void main (void)"				\
		"{"								\
		"gl_Position = u_mvp_matrix*vPosition;"	\
		"out_texture_coord = vTexture_Coord;"	\
		"}";

    glShaderSource(gVertex_Shader_Object,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    glCompileShader(gVertex_Shader_Object);
    
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char* szInfoLog = NULL;

    glGetShaderiv(gVertex_Shader_Object,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    
	if(iShaderCompiledStatus ==GL_FALSE)
    {
        glGetShaderiv(gVertex_Shader_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        
		if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertex_Shader_Object,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }    

    //fragment shader
    gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar * gFragmentShaderSourceCode = 
        "#version 330 core"			         \
        "\n"						         \
		"in vec2 out_texture_coord;"				\
		"out vec4 FragColor;"						\
		"uniform sampler2D u_texture_sampler;"		\
		"uniform int u_Keypressed ;"				\
		"\n"										\
		"void main (void)"							\
		"{ "		                                \
		"if(u_Keypressed == 0)"						\
		"{"									        \
		"FragColor = vec4(1.0,1.0,1.0,1.0);"        \
		"}"                                         \
		"else"                                      \
		"{"                                         \
		"FragColor = texture(u_texture_sampler,out_texture_coord);" \
		"}"                                          \
		"}";

    glShaderSource(gFragment_Shader_Object,1,(const GLchar **)&gFragmentShaderSourceCode,NULL);
    
    glCompileShader(gFragment_Shader_Object);

    glGetShaderiv(gFragment_Shader_Object,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    
	if(iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gFragment_Shader_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        
		if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragment_Shader_Object,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s\n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }

    //shader program

    gShader_Program_Object = glCreateProgram();
    glAttachShader(gShader_Program_Object,gVertex_Shader_Object);
    glAttachShader(gShader_Program_Object,gFragment_Shader_Object);

    //bind all attribs in variables before linking
    glBindAttribLocation(gShader_Program_Object,AMC_ATTRIBUTE_POSITION,"vPosition");

    glBindAttribLocation(gShader_Program_Object,AMC_ATTRIBUTE_TEXCOORD,"vTexture_Coord");

 
    //link
    glLinkProgram(gShader_Program_Object);
 
    GLint iShaderProgramLinkStatus = 0;
    
	glGetProgramiv(gShader_Program_Object,GL_LINK_STATUS,&iShaderProgramLinkStatus);
    
	if(iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShader_Program_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShader_Program_Object,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "Shader Program Link Log: %s\n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }

    //get uniform location
    mvpUniform = glGetUniformLocation(gShader_Program_Object,"u_mvp_matrix");

    //imp
   	Texture_sampler_uniform = glGetUniformLocation( gShader_Program_Object, "u_texture_sampler");

    u_PressedKey = glGetUniformLocation(gShader_Program_Object, "u_Keypressed");

	//**********
	const GLfloat Rectangle_Vertices[] = { 1.0f,1.0f,0.0f,
										-1.0f,1.0f,0.0f,
										-1.0f,-1.0f,0.0f,
										 1.0f,-1.0f,0.0f };

	const GLfloat TexCoords[]= {
		                        1.0f,1.0f,
		                        0.0f,1.0f,
		                        0.0f,0.0f,  
		                        1.0f,0.0f
	                           };

	//create vao for Rectangle
	glGenVertexArrays(1, &vao_Rectagle);

	//Binding
	glBindVertexArray(vao_Rectagle);

	//Buffer
	glGenBuffers(1, &vbo_Position_Rectangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Rectangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Rectangle_Vertices),
				 Rectangle_Vertices, GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Binding
	glGenBuffers(1, &vbo_Texture);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Texture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords), TexCoords, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); //vao


	//Depth 
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    LoadTexture("Smiley.bmp",&Texture_SMILEY);	   
    printf("\n %d Image is Loaded \n",Texture_SMILEY);
    glEnable(GL_TEXTURE_2D);	
	
    resize(giWindowWidth,giWindowHeight); //stray
}


//imp
GLuint LoadTexture(const char *path, GLuint *Texture)
{	
	int imgwidth;
	int imgheight;
	unsigned char *imageData = NULL;


    //impTexture
	imageData = SOIL_load_image(path,&imgwidth,&imgheight,0,SOIL_LOAD_RGB);
	
	glPixelStorei(GL_UNPACK_ALIGNMENT,4);
	glGenTextures(1, Texture);
    glBindTexture(GL_TEXTURE_2D,*Texture);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

	glTexImage2D(GL_TEXTURE_2D,	0, GL_RGB, imgwidth, imgwidth, 	0, GL_BGR, GL_UNSIGNED_BYTE, imageData);

	glGenerateMipmap(GL_TEXTURE_2D); 

//    gluBuild2DMipmaps(GL_TEXTURE_2D,3,imgwidth,imgheight,GL_RGBA,GL_UNSIGNED_BYTE,imageData); //old
    
    //new
    glBindTexture(GL_TEXTURE_2D, 0);
	SOIL_free_image_data(imageData);

    return(*Texture);
}


void resize(int Width,int Height)
{
   if (Height == 0)
        Height = 1;
    
    glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);

   	Perspective_Projection_Matrix = perspective(45.0f, (GLfloat)Width /(GLfloat)Height, 0.1f, 100.0f);

}


void display(void)
{
    	//1
	mat4 ModelView_Matrix;
	mat4 ModelView_Projection_Matrix;
	mat4 Translation_Matrix;
	mat4 Rotation_Matrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShader_Program_Object);

    glEnable(GL_TEXTURE_2D);	

	//Rectangle
	//2 : I[]
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	Rotation_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//4 : Do Mat mul
	Translation_Matrix = translate(0.0f, 0.0f, -6.0f);

	ModelView_Matrix = Translation_Matrix * Rotation_Matrix; //same as I[]

	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;

	// 5 : send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	//7 : texture bind code
    glActiveTexture(GL_TEXTURE0);
    
    	//new : Keypress
	GLfloat squareTexture[8];

	if(PressedKey == 0)
	{
		squareTexture[0] = 1.0f;
		squareTexture[1] = 1.0f;
		squareTexture[2] = 0.0f;
		squareTexture[3] = 1.0f;
		squareTexture[4] = 0.0f;
		squareTexture[5] = 0.0f;
		squareTexture[6] = 1.0f;
		squareTexture[7] = 0.0f;
	}
	
	else if(PressedKey == 1)
	{
		squareTexture[0] = 0.5f;
		squareTexture[1] = 0.5f;
		squareTexture[2] = 0.0f;
		squareTexture[3] = 0.5f;
		squareTexture[4] = 0.0f;
		squareTexture[5] = 0.0f;
		squareTexture[6] = 0.5f;
		squareTexture[7] = 0.0f;
	}
	
	else if(PressedKey == 2)
	{
		squareTexture[0] = 1.0f;
		squareTexture[1] = 1.0f;
		squareTexture[2] = 0.0f;
		squareTexture[3] = 1.0f;
		squareTexture[4] = 0.0f;
		squareTexture[5] = 0.0f;
		squareTexture[6] = 1.0f;
		squareTexture[7] = 0.0f;
	}
	
	else if(PressedKey == 3)
	{
		squareTexture[0] = 2.0f;
		squareTexture[1] = 2.0f;
		squareTexture[2] = 0.0f;
		squareTexture[3] = 2.0f;
		squareTexture[4] = 0.0f;
		squareTexture[5] = 0.0f;
		squareTexture[6] = 2.0f;
		squareTexture[7] = 0.0f;
	}
	else if(PressedKey == 4)
	{
		squareTexture[0] = 0.5f;
		squareTexture[1] = 0.5f;
		squareTexture[2] = 0.5f;
		squareTexture[3] = 0.5f;
		squareTexture[4] = 0.5f;
		squareTexture[5] = 0.5f;
		squareTexture[6] = 0.5f;
		squareTexture[7] = 0.5f;
	}

	glBindVertexArray(vao_Rectagle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Texture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareTexture), squareTexture, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, Texture_SMILEY);
	glUniform1i(Texture_sampler_uniform, 0);

	//texture bind code
	if(PressedKey == 0)
	{
		glUniform1i(u_PressedKey, 0);
	}

	else
	{
		glUniform1i(u_PressedKey, 1);
	}
	

    //Draw
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	glBindVertexArray(0);


    glUseProgram(0);

    glXSwapBuffers(gpDisplay,gWindow);
}


//new
void update(void)
{
    
}


void uninitialize(void)
{
    GLXContext currentGLXContext;
    currentGLXContext = glXGetCurrentContext();

		//for Shaders
	if (vbo_Position_Rectangle)
	{
		glDeleteBuffers(1, &vbo_Position_Rectangle);
		vbo_Position_Rectangle = 0;
	}

	if (vao_Rectagle)
	{
		glDeleteVertexArrays(1, &vao_Rectagle);
		vao_Rectagle = 0;
	}

	glUseProgram(gShader_Program_Object);
	glDetachShader(gShader_Program_Object, gFragment_Shader_Object);
	glDetachShader(gShader_Program_Object, gVertex_Shader_Object);

	glDeleteShader(gFragment_Shader_Object);
	gFragment_Shader_Object = 0;

	glDeleteShader(gVertex_Shader_Object);
	gVertex_Shader_Object = 0;

	glDeleteProgram(gShader_Program_Object);
	glUseProgram(0);


    if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,0,0);
    }

    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay,gGLXContext);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay,gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }

    if (gpFile)
    {
        fprintf(gpFile, "Log File successfully Closed.\n");
        fclose(gpFile);
        gpFile = NULL;
    }

}

