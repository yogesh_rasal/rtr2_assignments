//
//  GLESView.m
//
//  Copyright © 2020 AstroMediComp. All rights reserved.

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "GLESView.h"
#import "vmath.h"

using namespace vmath;

enum {
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};


#define SLICES 20
#define NUM_POINTS 1000

void showGraph();

//for Graph
GLuint vao_XAxis, vao_YAxis;
GLuint vbo_XPosition,vbo_XColor;
GLuint vbo_YPosition, vbo_YColor;
GLuint vao_Lines, vao_HLines;
GLuint vbo_PositionHLines, vbo_PositionVLines;
GLuint vbo_ColorHLines, vbo_ColorVLines;


@implementation GLESView
{
    
    EAGLContext *eaglContext;
    
    GLuint frameBuffer;
    GLuint colorBuffer;
    GLuint depthBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    // Shader Variables
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gProgramShaderObject;

    GLuint mvpUniform;
    mat4 Perspective_Projection_Matrix;

    //Vao
    GLuint vao_I , vbo_IP,vbo_IC;
    
    GLuint vao_N , vbo_NP,vbo_NC;
    
    GLuint vao_D , vbo_DP,vbo_DC;
    
    GLuint vao_A , vbo_AP,vbo_AC;

      
}


-(id) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    
    if(self) {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil) {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext: eaglContext];
        
        //Frame Buffer! Thing!
        glGenFramebuffers(1, &frameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
        //COlor Buffer!
        glGenRenderbuffers(1, &colorBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorBuffer);
        
        //Depth Buffer
        GLint backWidth;
        GLint backHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backHeight);
        
        glGenRenderbuffers(1, &depthBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backWidth, backHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            printf("FAILED :: TO create FBO");
            glDeleteRenderbuffers(1, &depthBuffer);
            glDeleteRenderbuffers(1, &colorBuffer);
            glDeleteFramebuffers(1, &frameBuffer);
            
            return(nil);
        }
        else {
            printf("FBO Created \n");
        }
        
        printf("GLES RENDERER : %s\n GLES VERSION : %s\n GLSL Version : %s \n",
               glGetString(GL_RENDERER),
               glGetString(GL_VERSION),
               glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        isAnimating = NO;
        animationFrameInterval = 60;
        
        [self initialize];
        
        //Gesture SetUP
        [self addGestures];
        
    }
    else {
        printf("Failed To Acquire the Instance \n");
    }
    
    return(self);
}

+(Class) layerClass {
    return ([CAEAGLLayer class]);
}

//imp
-(void) addGestures
{
    
    UITapGestureRecognizer *singleTapGestureRecognizer =
    [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector (onSingleTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    // this will allow to differentiate
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    // swipe
    UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    // long press
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    
    
}




-(void) initialize
{
    GLint iShaderCompileStatus = 0;
    GLint iShaderInfoLogLen = 0;
    GLchar* szInfoLog = NULL;
    
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // Vertex Shader
    const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;"                        \
        "in vec4 vColor;"                            \
        "out vec4 Out_Color;"                        \
        "uniform mat4 u_mvp_matrix;"                \
        "void main(void)"                            \
        "{"                                           \
        "gl_Position = u_mvp_matrix * vPosition;"    \
        "Out_Color = vColor;"                        \
        "}" ;



    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    
    //Compile
    glCompileShader(gVertexShaderObject);
    
    //Error Checking.
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE) {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        
        if(iShaderInfoLogLen > 0) {
            szInfoLog = (GLchar*)malloc(iShaderInfoLogLen);
            
            if(szInfoLog != NULL) {
                GLsizei written;
                
                glGetShaderInfoLog(gVertexShaderObject,
                                   iShaderInfoLogLen,
                                   &written,
                                   szInfoLog);
                
                printf("Vertex Shader Failed:\n %s\n", szInfoLog);
                
                free(szInfoLog);
                [self release];
            }
        }
    }
    else {
        printf("Vertex Shader Compiled..\n");
    }
    
    //Fragment Shader
    iShaderCompileStatus = 0;
    iShaderInfoLogLen = 0;
    szInfoLog = NULL;
    
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar *fragmentShaderSourceCode =
    "#version 300 es" \
    "\n"\
    "precision highp float;"
    "in vec4 Out_Color;"    \
    "out vec4 FragColor;"    \
    "void main(void)"        \
    " { "                    \
    "FragColor = Out_Color;" \
    " } " ;

    
    glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    
    glCompileShader(gFragmentShaderObject);
    
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        
        if(iShaderInfoLogLen > 0) {
            szInfoLog = (GLchar*)malloc(iShaderInfoLogLen);
            if(szInfoLog != NULL) {
                GLint written;
                
                glGetShaderInfoLog(gFragmentShaderObject,
                                   iShaderInfoLogLen,
                                   &written,
                                   szInfoLog);
                
                printf("Fragment Shader Failed:\n %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    else {
        printf("Fragement Shader Compiled..\n");
    }
    
//3
    GLint iProgLinkStatus = 0;
    GLint iProgLogLen = 0;
    GLchar* szProgLog = NULL;

    gProgramShaderObject = glCreateProgram();
    
    //Attach VS to Shader Prog
    glAttachShader(gProgramShaderObject, gVertexShaderObject);
    
    //Attach FS to Shader Prog
    glAttachShader(gProgramShaderObject, gFragmentShaderObject);
    
    //Prelink
    glBindAttribLocation(gProgramShaderObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gProgramShaderObject, AMC_ATTRIBUTE_COLOR, "vColor");

    
    
    //Link
    glLinkProgram(gProgramShaderObject);
    
    glGetProgramiv(gProgramShaderObject, GL_LINK_STATUS, &iProgLinkStatus);
    
    if(iProgLinkStatus == GL_FALSE) {
        glGetProgramiv(gProgramShaderObject, GL_INFO_LOG_LENGTH, &iProgLogLen);
        
        if(iProgLogLen > 0) {
            szProgLog = (GLchar*)malloc(iProgLogLen);
            
            if(szProgLog != NULL) {
                GLint written;
                
                glGetProgramInfoLog(gProgramShaderObject, iProgLogLen, &written, szProgLog);
                
                printf("Program Link Failed :\n %s\n",szProgLog);
                
                [self release];
            }
        }
    }
    else {
        printf("Program Linked Successfully..\n");
    }
    
    //POST LINKING
    mvpUniform = glGetUniformLocation(gProgramShaderObject, "u_mvp_matrix");
   
        
    //shapes

    //N
    const GLfloat N_Vertices[] =
        {
            //I
             0.3f, 0.5f, 0.0f,
             0.2f, 0.5f, 0.0f,
             0.2f, -0.5f, 0.0f,
             0.3f, -0.5f, 0.0f,
    
             -0.2f, 0.5f, 0.0f,
             -0.3f, 0.5f, 0.0f,
             -0.3f, -0.5f, 0.0f,
             -0.2f, -0.5f, 0.0f,
    
            //I
             -0.1f, 0.5f, 0.0f,
             -0.22f, 0.5f, 0.0f,
             0.1f, -0.5f, 0.0f,
             0.22f, -0.5f, 0.0f
    
        };
    
        const GLfloat N_Colors[] =
        {     1.0f,0.5f,0.0f,
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
            0.0f,1.0f,0.0f ,
    
            1.0f,0.5f,0.0f,
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
            0.0f,1.0f,0.0f,
    
            1.0f,0.5f,0.0f,
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
            0.0f,1.0f,0.0f
    
        };
    
        //create vao
        glGenVertexArrays(1, &vao_N);
    
        //Binding
        glBindVertexArray(vao_N);
    
    //Pos
        glGenBuffers(1, &vbo_NP);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_NP);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(N_Vertices),
                    N_Vertices,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        //Set Position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Color
        //Buffer
        glGenBuffers(1, &vbo_NC);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_NC);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(N_Colors),
                    N_Colors,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        //Set Position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //vao
        glBindVertexArray(0);
    
    
    
    //    I
    const GLfloat I_Vertices[] =
        {
            0.05f, 0.5f, 0.0f,
            -0.05f, 0.5f, 0.0f,
            -0.05f, -0.5f, 0.0f,
            0.05f, -0.5f, 0.0f
         };
    
        const GLfloat I_Colors[] =
        {
            1.0f,0.5f,0.0f,
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
            0.0f,1.0f,0.0f
        };
    
        //create vao
        glGenVertexArrays(1, &vao_I);
    
        //Binding
        glBindVertexArray(vao_I);
    
    //Pos
        glGenBuffers(1, &vbo_IP);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_IP);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(I_Vertices),
                    I_Vertices,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        //Set Position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Color
        //Buffer
        glGenBuffers(1, &vbo_IC);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_IC);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(I_Colors),
                    I_Colors,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        //Set Position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //vao
        glBindVertexArray(0);
    
    
    //D :  vao_D , vbo_DP,vbo_DC
    
    const GLfloat D_Vertices[] =
        {
                  -0.3f, 0.5f, 0.0f,
                   -0.2f, 0.5f, 0.0f,
                   -0.2f, -0.5f, 0.0f,
                   -0.3f, -0.5f, 0.0f,
           
                   // D
                   -0.2f, 0.4f, 0.0f,
                   -0.2f, 0.5f, 0.0f,
                   0.0f, 0.4f, 0.0f,
                   0.0f, 0.5f, 0.0f,
           
                   0.15f, 0.3f, 0.0f,
                   0.25f, 0.36f, 0.0f,
                   0.15f, -0.3f, 0.0f ,
                   0.25f, -0.36f, 0.0f ,
           
                   0.0f, -0.4f, 0.0f ,
                   0.0f, -0.5f, 0.0f ,
                   -0.2f, -0.4f, 0.0f ,
                   -0.2f, -0.5f, 0.0f
        
        };
    
      const GLfloat D_Colors[] =
        {
            1.0f,0.5f,0.0f,
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
            0.0f,1.0f,0.0f,
    
            1.0f,0.5f,0.0f,
            1.0f,0.5f,0.0f,
            1.0f,0.5f,0.0f,
            1.0f,0.5f,0.0f,
    
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
    
            0.0f,1.0f,0.0f,
            0.0f,1.0f,0.0f,
            0.0f,1.0f,0.0f,
            0.0f,0.0f,0.0f //for look
        };
    
        //create vao
        glGenVertexArrays(1, &vao_D);
    
        //Binding
        glBindVertexArray(vao_D);
    
    //Pos
        glGenBuffers(1, &vbo_DP);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_DP);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(D_Vertices),
                    D_Vertices,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        //Set Position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Color
        //Buffer
        glGenBuffers(1, &vbo_DC);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_DC);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(D_Colors),
                    D_Colors,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        //Set Position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //vao
        glBindVertexArray(0);
    
    
    //    A  vao_A , vbo_AP,vbo_AC;
    
        const GLfloat A_Vertices[] =
        {
                // for A
                     1.0f, 0.5f, 0.0f,
                     0.95f, 0.5f, 0.0f,
                     0.0f, -0.5f, 0.0f,
                     0.25f, -0.5f, 0.0f,
                     
                     //
                     1.15f, 0.5f, 0.0f,
                     1.0f, 0.5f, 0.0f,
                     1.05f, -0.5f, 0.0f,
                     1.35f, -0.5f, 0.0f,
            
                    //strips
                     1.1f, 0.1f, 0.0f,
                     0.6f, 0.1f, 0.0f,
                     0.6f, 0.0f, 0.0f,
                     1.1f, 0.0f, 0.0f,
            
                     //W
                    1.1f, 0.06f, 0.0f,
                    0.6f, 0.06f, 0.0f,
                    0.6f, 0.04f, 0.0f,
                    1.1f, 0.04f, 0.0f
            

        };
    
        const GLfloat A_Colors[] =
        {
                    1.0f,0.5f,0.0f,
                    1.0f,1.0f,1.0f,
                    1.0f,1.0f,1.0f,
                    0.0f,1.0f,0.0f,
            
                    //
                    1.0f,0.5f,0.0f,
                    1.0f,1.0f,1.0f,
                    1.0f,1.0f,1.0f,
                    0.0f,1.0f,0.0f,
            
                    
                    //RG
                    1.0f, 0.5f, 0.0f,
                    1.0f, 0.5f, 0.0f,
                    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,
            
                    // W
                    1.0f, 1.0f, 1.0f,
                    1.0f, 1.0f, 1.0f,
                    1.0f, 1.0f, 1.0f,
                    1.0f, 1.0f, 1.0f
        };
    
    
    //create vao
        glGenVertexArrays(1, &vao_A);
    
        glBindVertexArray(vao_A);
    
    //Pos
        glGenBuffers(1, &vbo_AP);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_AP);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(A_Vertices), A_Vertices,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        //Set Position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Color
        //Buffer
        glGenBuffers(1, &vbo_AC);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_AC);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(A_Colors),    A_Colors,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //vao
        glBindVertexArray(0);
    
   
    //depth
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);


    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);  //imp

    
    Perspective_Projection_Matrix = mat4::identity();
}




-(void) drawRect:(CGRect)rect
{
    
}


-(BOOL) acceptsFirstResponder{
    return(YES);
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    //TO Start Our Code
}

-(void) onSingleTap:(UITapGestureRecognizer *) event {
    
}

-(void) onDoubleTap:(UITapGestureRecognizer *) event
{
    
    printf("\n in Double Tap ..");
    
}

-(void) onSwipe:(UISwipeGestureRecognizer *) event {
    [self release];
    exit(0);
}

-(void) onLongPress:(UILongPressGestureRecognizer *) event
{
    
}



-(void) layoutSubviews
{
    
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable: (CAEAGLLayer *)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    //Depth BUffer Regenration!
    glGenRenderbuffers(1, &depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        printf("Failed to Update FBO");
        glCheckFramebufferStatus(GL_FRAMEBUFFER);
    }
    else {
        printf("FBO Updated Successfully \n");
    }
    
    
    if(height == 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    GLfloat Width = (GLfloat)width;
    GLfloat Height = (GLfloat)height;
    
    Perspective_Projection_Matrix = perspective(45.0f, Width/Height, 0.1f, 100.0f);
    
    [self drawView: nil];
}



//Display
-(void) drawView: (id) sender
{
    [EAGLContext setCurrentContext:eaglContext];
    
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
        

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        
        glUseProgram(gProgramShaderObject);

        //
            mat4 ModelView_Matrix,ModelView_Projection_Matrix;
            
        // I
            //2 : I[]
            ModelView_Matrix = mat4::identity();
            ModelView_Projection_Matrix = mat4::identity();
        
            //3 : Transforms
            ModelView_Matrix = translate(-2.5f, 0.0f, -5.0f);
        
            //4 : Do Mat mul
            ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;
        
            // 5 : send it to shader
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);
        
            // 6 : bind to vao
            glBindVertexArray(vao_I);
        
            //8 : draw scene
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        
            //9 : Unbind vao
            glBindVertexArray(0);
        
        
        //    N
        //2 : I[]
            ModelView_Matrix = mat4::identity();
            ModelView_Projection_Matrix = mat4::identity();
        
            //3 : Transforms
            ModelView_Matrix = translate(-1.2f, 0.0f, -5.0f);
        
            //4 : Do Mat mul
            ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;
        
            // 5 : send it to shader
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);
        
            // 6 : bind to vao
            glBindVertexArray(vao_N);
        
            //8 : draw scene
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
            glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
        
            //9 : Unbind vao
            glBindVertexArray(0);
        
        
        //    D vao_D
        //2 : I[]
            ModelView_Matrix = mat4::identity();
            ModelView_Projection_Matrix = mat4::identity();
        
            //3 : Transforms
            ModelView_Matrix = translate(0.2f, 0.0f, -5.0f);
        
            //4 : Do Mat mul
            ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;
        
            // 5 : send it to shader
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);
        
            // 6 : bind to vao
            glBindVertexArray(vao_D);
        
            //8 : draw scene : Imp
        
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            // glDrawArrays(GL_QUAD_STRIP, 4,12);
        
            glDrawArrays(GL_TRIANGLE_STRIP, 4,12);

            
            
            //9 : Unbind vao
            glBindVertexArray(0);
        
        
        // I    I
        //2 : I[]
            ModelView_Matrix = mat4::identity();
            ModelView_Projection_Matrix = mat4::identity();
        
            //3 : Transforms
            ModelView_Matrix = translate(1.4f, 0.0f, -5.0f);
        
            //4 : Do Mat mul
            ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;
        
            // 5 : send it to shader
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);
        
            // 6 : bind to vao
            glBindVertexArray(vao_I);
        
            //8 : draw scene
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        
            //9 : Unbind vao
            glBindVertexArray(0);
        
        
        // A : Vao_A
            mat4 scaleA , rotateA;
        
        // 1
            scaleA = mat4::identity();
            rotateA = mat4::identity();
            ModelView_Matrix = mat4::identity();
            ModelView_Projection_Matrix = mat4::identity();
        
            //3 : Transforms
            ModelView_Matrix = translate(2.3f,0.0f,-5.6f);
        
            ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;
        
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);
        
            glBindVertexArray(vao_A);
        
            //A
            glDrawArrays(GL_TRIANGLE_FAN, 0,4); //QUADS
            
            glDrawArrays(GL_TRIANGLE_FAN, 4,4);

            glDrawArrays(GL_TRIANGLE_FAN, 8,4);
            
            glDrawArrays(GL_TRIANGLE_FAN, 12,4);
            
            glBindVertexArray(0);
        
   
        glUseProgram(0);
        
        glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
        [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void) startAnimation //update
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond: animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}


-(void) stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

-(void) dealloc
{
    
    //Safe Release
    if(gProgramShaderObject) {
        GLsizei iShaderCnt = 0;
        GLsizei iShaderNo = 0;
        
        glUseProgram(gProgramShaderObject);
        
        glGetProgramiv(gProgramShaderObject,
                       GL_ATTACHED_SHADERS,
                       &iShaderCnt);
        
        GLuint *pShaders = (GLuint*)malloc(iShaderCnt * sizeof(GLuint));
        
        if(pShaders) {
            glGetAttachedShaders(gProgramShaderObject,
                                 iShaderCnt,
                                 &iShaderCnt,
                                 pShaders);
            
            for(iShaderNo = 0; iShaderNo < iShaderCnt; iShaderNo++) {
                
                glDetachShader(gProgramShaderObject, pShaders[iShaderNo]);
                
                pShaders[iShaderNo] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gProgramShaderObject);
        gProgramShaderObject = 0;
        glUseProgram(0);
    }
    
    if(depthBuffer) {
        glDeleteRenderbuffers(1, &depthBuffer);
        depthBuffer = 0;
    }
    if(colorBuffer){
        glDeleteRenderbuffers(1, &colorBuffer);
        colorBuffer = 0;
    }
    
    if(frameBuffer) {
        glDeleteFramebuffers(1, &frameBuffer);
        frameBuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext) {
        [EAGLContext setCurrentContext:nil];
    }
    
    if(eaglContext) {
        eaglContext = nil;
    }
    
    [super dealloc];
}



@end



