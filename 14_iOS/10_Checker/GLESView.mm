//
//  GLESView.m
//
//  Copyright © 2020 AstroMediComp. All rights reserved.

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "GLESView.h"
#import "vmath.h"

using namespace vmath;

enum {
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};

#define CHECK_IMG_WIDTH  64
#define CHECK_IMG_HEIGHT  64

GLubyte CheckImage[CHECK_IMG_WIDTH][CHECK_IMG_HEIGHT][4];


@implementation GLESView
{
    
    EAGLContext *eaglContext;
    
    GLuint frameBuffer;
    GLuint colorBuffer;
    GLuint depthBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    // Shader Variables
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gProgramShaderObject;
    
    GLuint mvpUniform;
    mat4 perspectiveProjectionMatrix;

    GLuint samplerUniform;
    
    GLuint vao_Rectangle;
    GLuint vbo_Position_Rectangle;
    GLuint vbo_Texture;
       
    //for Texture
    GLuint Texture_CheckImg;
       
      
}


-(id) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    
    if(self) {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil) {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext: eaglContext];
        
        //Frame Buffer! Thing!
        glGenFramebuffers(1, &frameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
        //COlor Buffer!
        glGenRenderbuffers(1, &colorBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorBuffer);
        
        //Depth Buffer
        GLint backWidth;
        GLint backHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backHeight);
        
        glGenRenderbuffers(1, &depthBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backWidth, backHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            printf("FAILED :: TO create FBO");
            glDeleteRenderbuffers(1, &depthBuffer);
            glDeleteRenderbuffers(1, &colorBuffer);
            glDeleteFramebuffers(1, &frameBuffer);
            
            return(nil);
        }
        else {
            printf("FBO Created \n");
        }
        
        printf("GLES RENDERER : %s\n GLES VERSION : %s\n GLSL Version : %s \n",
               glGetString(GL_RENDERER),
               glGetString(GL_VERSION),
               glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        isAnimating = NO;
        animationFrameInterval = 60;
        
        [self initialize];
        
        //Gesture SetUP
        [self addGestures];
        
    }
    else {
        printf("Failed To Acquire the Instance \n");
    }
    
    return(self);
}

+(Class) layerClass {
    return ([CAEAGLLayer class]);
}

//imp
-(void) addGestures
{
    
    UITapGestureRecognizer *singleTapGestureRecognizer =
    [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector (onSingleTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    // this will allow to differentiate
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    // swipe
    UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    // long press
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    
    
}




-(void) initialize
{
    GLint iShaderCompileStatus = 0;
    GLint iShaderInfoLogLen = 0;
    GLchar* szInfoLog = NULL;
    
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // Vertex Shader
    const GLchar *vertexShaderSourceCode =
    "#version 300 es" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec2 vTexCoord;" \
    "out vec2 out_texCoord;" \
    "uniform mat4 u_mvp_matrix;" \
    "void main(void)" \
    "{" \
    "gl_Position = u_mvp_matrix * vPosition;" \
    "out_texCoord = vTexCoord;" \
    "}";

    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    
    //Compile
    glCompileShader(gVertexShaderObject);
    
    //Error Checking.
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE) {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        
        if(iShaderInfoLogLen > 0) {
            szInfoLog = (GLchar*)malloc(iShaderInfoLogLen);
            
            if(szInfoLog != NULL) {
                GLsizei written;
                
                glGetShaderInfoLog(gVertexShaderObject,
                                   iShaderInfoLogLen,
                                   &written,
                                   szInfoLog);
                
                printf("Vertex Shader Failed:\n %s\n", szInfoLog);
                
                free(szInfoLog);
                [self release];
            }
        }
    }
    else {
        printf("Vertex Shader Compiled..\n");
    }
    
    //Fragment Shader
    iShaderCompileStatus = 0;
    iShaderInfoLogLen = 0;
    szInfoLog = NULL;
    
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar *fragmentShaderSourceCode =
    "#version 300 es" \
    "\n" \
    "precision highp float;"
    "in vec2 out_texCoord;" \
    "uniform sampler2D u_sampler;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "vec3 temp = vec3(texture(u_sampler, out_texCoord));"
    "FragColor = vec4(temp, 1.0f);" \
    "}";
    
    glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    
    glCompileShader(gFragmentShaderObject);
    
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        
        if(iShaderInfoLogLen > 0) {
            szInfoLog = (GLchar*)malloc(iShaderInfoLogLen);
            if(szInfoLog != NULL) {
                GLint written;
                
                glGetShaderInfoLog(gFragmentShaderObject,
                                   iShaderInfoLogLen,
                                   &written,
                                   szInfoLog);
                
                printf("Fragment Shader Failed:\n %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    else {
        printf("Fragement Shader Compiled..\n");
    }
    
//3
    GLint iProgLinkStatus = 0;
    GLint iProgLogLen = 0;
    GLchar* szProgLog = NULL;

    gProgramShaderObject = glCreateProgram();
    
    //Attach VS to Shader Prog
    glAttachShader(gProgramShaderObject, gVertexShaderObject);
    
    //Attach FS to Shader Prog
    glAttachShader(gProgramShaderObject, gFragmentShaderObject);
    
    //Prelink
    glBindAttribLocation(gProgramShaderObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gProgramShaderObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
    
    
    //Link
    glLinkProgram(gProgramShaderObject);
    
    glGetProgramiv(gProgramShaderObject, GL_LINK_STATUS, &iProgLinkStatus);
    
    if(iProgLinkStatus == GL_FALSE) {
        glGetProgramiv(gProgramShaderObject, GL_INFO_LOG_LENGTH, &iProgLogLen);
        
        if(iProgLogLen > 0) {
            szProgLog = (GLchar*)malloc(iProgLogLen);
            
            if(szProgLog != NULL) {
                GLint written;
                
                glGetProgramInfoLog(gProgramShaderObject, iProgLogLen, &written, szProgLog);
                
                printf("Program Link Failed :\n %s\n",szProgLog);
                
                [self release];
            }
        }
    }
    else {
        printf("Program Linked Successfully..\n");
    }
    
    //POST LINKING
    mvpUniform = glGetUniformLocation(gProgramShaderObject, "u_mvp_matrix");
    
    samplerUniform = glGetUniformLocation(gProgramShaderObject, "u_sampler");
    
 
    //Rectangle
    //**********
    
    //create vao for Rectangle
    glGenVertexArrays(1, &vao_Rectangle);

    //Binding
    glBindVertexArray(vao_Rectangle);

    //Buffer
    glGenBuffers(1, &vbo_Position_Rectangle);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Rectangle);

    // 3 = axis, 4 = vertices, 4 = size of type(float)
    glBufferData(GL_ARRAY_BUFFER, 3*4*4, NULL, GL_DYNAMIC_DRAW);

    //most imp
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Set Position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);


//New
    const GLfloat TexCoords[]=
    {
        1.0f,1.0f,
        0.0f,1.0f,
        0.0f,0.0f,
        1.0f,0.0f
    };

    glGenBuffers(1, &vbo_Texture);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Texture);
    glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords),TexCoords, GL_STATIC_DRAW);

    glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
    

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);

   
    //depth
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    [self loadTextureFromBMPFile];
    
    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);  //imp

    
    perspectiveProjectionMatrix = mat4::identity();
}


-(void) drawRect:(CGRect)rect
{
    
}


-(BOOL) acceptsFirstResponder{
    return(YES);
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    //TO Start Our Code
}

-(void) onSingleTap:(UITapGestureRecognizer *) event {
    
}

-(void) onDoubleTap:(UITapGestureRecognizer *) event
{
    
    printf("\n in Double Tap ..");
    
}

-(void) onSwipe:(UISwipeGestureRecognizer *) event {
    [self release];
    exit(0);
}

-(void) onLongPress:(UILongPressGestureRecognizer *) event {
    
}



-(void) layoutSubviews
{
    
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable: (CAEAGLLayer *)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    //Depth BUffer Regenration!
    glGenRenderbuffers(1, &depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        printf("Failed to Update FBO");
        glCheckFramebufferStatus(GL_FRAMEBUFFER);
    }
    else {
        printf("FBO Updated Successfully \n");
    }
    
    
    if(height == 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    GLfloat Width = (GLfloat)width;
    GLfloat Height = (GLfloat)height;
    
    perspectiveProjectionMatrix = perspective(45.0f, Width/Height, 0.1f, 100.0f);
    
    [self drawView: nil];
}


//VIMP
//Maths Texture
 void MakeCheckImage()
    {
        int i,j,c;

        for(i=0; i < CHECK_IMG_HEIGHT;i++)
        {
            for(j=0; j < CHECK_IMG_WIDTH; j++)
            {
                //Most imp
                c = (((i&0x8) == 0) ^ ((j&0x8) == 0))*255;

                CheckImage[i][j][0] = (GLubyte) c;   // R
                CheckImage[i][j][1] = (GLubyte) c;   //G
                CheckImage[i][j][2] = (GLubyte) c;   //B
                CheckImage[i][j][3] = (GLubyte) 255; //A

            }

        }

    }    //Texture Loading


//VIMP

-(void)loadTextureFromBMPFile
{

    //try
    MakeCheckImage();

    
    glGenTextures(1, &Texture_CheckImg);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    glBindTexture(GL_TEXTURE_2D, Texture_CheckImg);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, CHECK_IMG_WIDTH, CHECK_IMG_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, CheckImage); //pixels

    glGenerateMipmap(GL_TEXTURE_2D);
    
    glBindTexture(GL_TEXTURE_2D, 0);
}




//Display
-(void) drawView: (id) sender
{
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(gProgramShaderObject);
    
    
    mat4 modelViewMatrix;
    mat4 modelviewProjMatrix;
    mat4 translateMatrix;
    mat4 Rotation_Matrix;
    
        //I
    translateMatrix = mat4::identity();
    modelViewMatrix = mat4::identity();
    modelviewProjMatrix = mat4::identity();
    Rotation_Matrix = mat4::identity();
    
    
    translateMatrix = translate(0.0f, 0.0f, -5.0f);
    
    modelViewMatrix = translateMatrix * Rotation_Matrix;
    
    modelviewProjMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelviewProjMatrix);
   
    // texture bind code
      
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D , Texture_CheckImg);
      glUniform1i(samplerUniform, 0);

      
      //new
      GLfloat RectVertices1[12] =
      {
          -2.0f, -1.0f, 0.0f,
          -2.0f, 1.0f, 0.0f,
           0.0f, 1.0f, 0.0f,
           0.0f, -1.0f, 0.0f
      };

      GLfloat RectVertices2[12] =
      {
          1.0f, -1.0f, 0.0f,
          1.0f, 1.0f, 0.0f,
          2.41421f, 1.0f, -1.41421f,
          2.41421f, -1.0f, -1.41421f
      };

      //1st
      glBindVertexArray(vao_Rectangle);
      glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Rectangle);
      glBufferData(GL_ARRAY_BUFFER, sizeof(RectVertices1), RectVertices1, GL_STATIC_DRAW);
      glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
      glBindVertexArray(0);

      //2nd
      glBindVertexArray(vao_Rectangle);
      glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Rectangle);
      glBufferData(GL_ARRAY_BUFFER, sizeof(RectVertices2), RectVertices2, GL_STATIC_DRAW);
      glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
      
      
      // Unbind
    glBindVertexArray(0);

    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void) startAnimation //update
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond: animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}


-(void) stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

-(void) dealloc
{
    
    //Uninitialize
    
    //Vao & vbo
    vbo_Position_Rectangle = NULL;
    vao_Rectangle = NULL;
    
    
    //Safe Release
    if(gProgramShaderObject) {
        GLsizei iShaderCnt = 0;
        GLsizei iShaderNo = 0;
        
        glUseProgram(gProgramShaderObject);
        
        glGetProgramiv(gProgramShaderObject,
                       GL_ATTACHED_SHADERS,
                       &iShaderCnt);
        
        GLuint *pShaders = (GLuint*)malloc(iShaderCnt * sizeof(GLuint));
        
        if(pShaders) {
            glGetAttachedShaders(gProgramShaderObject,
                                 iShaderCnt,
                                 &iShaderCnt,
                                 pShaders);
            
            for(iShaderNo = 0; iShaderNo < iShaderCnt; iShaderNo++) {
                
                glDetachShader(gProgramShaderObject, pShaders[iShaderNo]);
                
                pShaders[iShaderNo] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gProgramShaderObject);
        gProgramShaderObject = 0;
        glUseProgram(0);
    }
    
    if(depthBuffer) {
        glDeleteRenderbuffers(1, &depthBuffer);
        depthBuffer = 0;
    }
    if(colorBuffer){
        glDeleteRenderbuffers(1, &colorBuffer);
        colorBuffer = 0;
    }
    
    if(frameBuffer) {
        glDeleteFramebuffers(1, &frameBuffer);
        frameBuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext) {
        [EAGLContext setCurrentContext:nil];
    }
    
    if(eaglContext) {
        eaglContext = nil;
    }
    
    [super dealloc];
}



@end



