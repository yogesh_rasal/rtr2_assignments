//
//  AppDelegate.h
//  iOSHello
//
//  Created by Kartik Ghangare on 19/12/19.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

