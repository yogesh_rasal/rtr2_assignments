//
//  MyView.m
//  iOSHello
//
//  Created by YSR on 19/12/19.
//

#import <Foundation/Foundation.h>
#import "MyView.h"

@implementation MyView
{
    NSString *centralText;
    
}

-(id)initWithFrame:(CGRect)frameRect
{
    self= [super initWithFrame:frameRect];
    
    if(self)
    {
        //init
        // set BKColor
        [self setBackgroundColor:[UIColor whiteColor]];
        
        centralText = @"Hello iOS !!!";
        
        //Gestures
        // Tap
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector (onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired : 1];
        
        [singleTapGestureRecognizer setNumberOfTouchesRequired : 1];
        
        [singleTapGestureRecognizer setDelegate : self];
        
        //Register
        [self addGestureRecognizer : singleTapGestureRecognizer];
    
    //Double
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector (onDoubleTap:)];
        
        [doubleTapGestureRecognizer setNumberOfTapsRequired : 2]; //imp
        
        [doubleTapGestureRecognizer setNumberOfTouchesRequired : 1];
        
        [doubleTapGestureRecognizer setDelegate : self];
        
        //Register
        [self addGestureRecognizer : doubleTapGestureRecognizer];
        
    //Diff
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    //swipe
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    
        //Register
        [self addGestureRecognizer : swipeGestureRecognizer];
        
      //Long press
        UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:) ];
                                                             
        //Register
        [self addGestureRecognizer : longPressRecognizer];
        
    
    }
    return(self);
}

//drawRect
-(void)drawRect:(CGRect)rect
{
    //BK
    UIColor *fill= [UIColor blackColor];
    [fill set];
    UIRectFill(rect);
    
    //KVC
    NSDictionary *dictForTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys : [UIFont fontWithName:@"Helvetica" size:24], NSFontAttributeName,[UIColor greenColor],
                  NSForegroundColorAttributeName,nil
    ];
    
    CGSize textSize = [centralText sizeWithAttributes:dictForTextAttributes];
    
    CGPoint point;

    point.x = (rect.size.width/2)-(textSize.width/2);

    point.y = (rect.size.height/2)-(textSize.height/2);

    [centralText drawAtPoint:point withAttributes:dictForTextAttributes];


}

//FirstResponder
-(BOOL)acceptsFirstResponder
{
    return YES;
    
}

-(void)touchesBegan : (NSSet *)touches withEvent:(UIEvent *)event
{
    
    //common : [self setNeedsDisplay];
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    centralText = @"Single Tap Event";
    [self setNeedsDisplay];
    
}


-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    centralText = @"Double Tap";
    [self setNeedsDisplay];
    
}

-(void)onLongPress:(UITapGestureRecognizer *)gr
{
    centralText = @"Long Tap";
    [self setNeedsDisplay];
    
}


-(void)onSwipe:(UITapGestureRecognizer *)gr
{
    [self release];
    exit(0);
}


- (void)dealloc
{
    [super dealloc];
    
}

@end
