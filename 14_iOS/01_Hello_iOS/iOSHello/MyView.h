//
//  MyView.h
//  iOSHello
//
//  Created by  YSR on 19/12/19.
//

#ifndef MyView_h
#define MyView_h

#import <UIKit/UIKit.h>

@interface MyView : UIView <UIGestureRecognizerDelegate>

@end

#endif /* MyView_h */
