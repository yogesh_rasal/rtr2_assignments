//
//  GLESView.m
//
//  Copyright © 2020 AstroMediComp. All rights reserved.

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "GLESView.h"
#import "vmath.h"
#import "YSR_Sphere.h"

using namespace vmath;

float AngleRect = 0.0f;


@implementation GLESView
{
    
    EAGLContext *eaglContext;
    
    GLuint frameBuffer;
    GLuint colorBuffer;
    GLuint depthBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    // Shader Variables
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gProgramShaderObject;

    //New
    GLuint Model_View_Uniform;
    GLuint Projection_Uniform;
    GLuint LD_Uniform;
    GLuint KD_Uniform;
    GLuint Light_Position_Uniform;
    GLuint LisPressed_Uniform;

    
    bool bisLPressed;
    bool bLighting;
    
    GLuint mvpUniform;
    mat4 perspectiveProjectionMatrix;

    
    GLuint Vao_cube;
    GLuint Vbo_cube_position;
    GLuint Vbo_cube_normal;


}


-(id) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    
    if(self) {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil) {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext: eaglContext];
        
        //Frame Buffer! Thing!
        glGenFramebuffers(1, &frameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
        //COlor Buffer!
        glGenRenderbuffers(1, &colorBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorBuffer);
        
        //Depth Buffer
        GLint backWidth;
        GLint backHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backHeight);
        
        glGenRenderbuffers(1, &depthBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backWidth, backHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            printf("FAILED :: TO create FBO");
            glDeleteRenderbuffers(1, &depthBuffer);
            glDeleteRenderbuffers(1, &colorBuffer);
            glDeleteFramebuffers(1, &frameBuffer);
            
            return(nil);
        }
        else {
            printf("FBO Created \n");
        }
        
        printf("GLES RENDERER : %s\n GLES VERSION : %s\n GLSL Version : %s \n",
               glGetString(GL_RENDERER),
               glGetString(GL_VERSION),
               glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        isAnimating = NO;
        animationFrameInterval = 60;
        
        [self initialize];
        
        //Gesture SetUP
        [self addGestures];
        
    }
    else {
        printf("Failed To Acquire the Instance \n");
    }
    
    return(self);
}

+(Class) layerClass {
    return ([CAEAGLLayer class]);
}



-(void) initialize
{
    GLint iShaderCompileStatus = 0;
    GLint iShaderInfoLogLen = 0;
    GLchar* szInfoLog = NULL;
    
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // Vertex Shader
    const GLchar *vertexShaderSourceCode =
    "#version 300 es" \
    "\n" \
    "precision highp float;"
    "precision highp int;"
    "in vec4 vPosition;"                        \
    "in vec3 vNormal;"                            \
    "out vec3 diffuse_Color;"                    \
    "\n"                                        \
    "uniform mat4 u_mv_matrix;"                    \
    "uniform mat4 u_projection_matrix;"            \
    "uniform int u_LKeyPressed;"                \
    "uniform vec3 u_LD;"                        \
    "uniform vec3 u_KD;"                        \
    "uniform vec4 u_Light_Position;"            \
    "void main(void)"                            \
    "{"                                            \
    "if(u_LKeyPressed == 1)"                    \
    "{"                                            \
    "vec4 Eye_Coordinates = u_mv_matrix * vPosition;"    \
    "mat3 Normal_Matrix = mat3(transpose(inverse(u_mv_matrix)));"    \
    "vec3 T_Norm = normalize(Normal_Matrix * vNormal);"    \
    "vec3 S = vec3(u_Light_Position) - (Eye_Coordinates.xyz);" \
    "diffuse_Color = u_LD * u_KD * dot(S,T_Norm);"  \
    "}"                                            \
    "gl_Position = u_projection_matrix * u_mv_matrix * vPosition;"    \
    "}" ;


    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    
    //Compile
    glCompileShader(gVertexShaderObject);
    
    //Error Checking.
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE) {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        
        if(iShaderInfoLogLen > 0) {
            szInfoLog = (GLchar*)malloc(iShaderInfoLogLen);
            
            if(szInfoLog != NULL) {
                GLsizei written;
                
                glGetShaderInfoLog(gVertexShaderObject,
                                   iShaderInfoLogLen,
                                   &written,
                                   szInfoLog);
                
                printf("Vertex Shader Failed:\n %s\n", szInfoLog);
                
                free(szInfoLog);
                [self release];
            }
        }
    }
    else {
        printf("Vertex Shader Compiled..\n");
    }
    
    //Fragment Shader
    iShaderCompileStatus = 0;
    iShaderInfoLogLen = 0;
    szInfoLog = NULL;
    
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar *fragmentShaderSourceCode =
    "#version 300 es" \
    "\n" \
    "precision highp float;"
    "precision highp int;"
    "in vec3 diffuse_Color;"    \
    "out vec4 FragColor;"        \
    "uniform int u_LKeyPressed;"\
    "\n"                        \
    "void main(void)"            \
    "{"                            \
    "if(u_LKeyPressed == 1)"    \
    "{"                            \
    "FragColor = vec4(diffuse_Color,1.0);"        \
    "}"                            \
    "else"                        \
    "{"\
    "FragColor = vec4(1.0,1.0,1.0,1.0);" \
    "}"\
    "}";
    
    
    glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    
    glCompileShader(gFragmentShaderObject);
    
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        
        if(iShaderInfoLogLen > 0) {
            szInfoLog = (GLchar*)malloc(iShaderInfoLogLen);
            if(szInfoLog != NULL) {
                GLint written;
                
                glGetShaderInfoLog(gFragmentShaderObject,
                                   iShaderInfoLogLen,
                                   &written,
                                   szInfoLog);
                
                printf("Fragment Shader Failed:\n %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    else {
        printf("Fragement Shader Compiled..\n");
    }
    
//3
    GLint iProgLinkStatus = 0;
    GLint iProgLogLen = 0;
    GLchar* szProgLog = NULL;

    gProgramShaderObject = glCreateProgram();
    
    //Attach VS to Shader Prog
    glAttachShader(gProgramShaderObject, gVertexShaderObject);
    
    //Attach FS to Shader Prog
    glAttachShader(gProgramShaderObject, gFragmentShaderObject);
    
    //Prelink
    glBindAttribLocation(gProgramShaderObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gProgramShaderObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
    
    //Link
    glLinkProgram(gProgramShaderObject);
    
    glGetProgramiv(gProgramShaderObject, GL_LINK_STATUS, &iProgLinkStatus);
    
    if(iProgLinkStatus == GL_FALSE) {
        glGetProgramiv(gProgramShaderObject, GL_INFO_LOG_LENGTH, &iProgLogLen);
        
        if(iProgLogLen > 0) {
            szProgLog = (GLchar*)malloc(iProgLogLen);
            
            if(szProgLog != NULL) {
                GLint written;
                
                glGetProgramInfoLog(gProgramShaderObject, iProgLogLen, &written, szProgLog);
                
                printf("Program Link Failed :\n %s\n",szProgLog);
                
                [self release];
            }
        }
    }
    else {
        printf("Program Linked Successfully..\n");
    }
    
    //POST LINKING
    mvpUniform = glGetUniformLocation(gProgramShaderObject,"u_mv_matrix");
    
    Projection_Uniform = glGetUniformLocation(gProgramShaderObject, "u_projection_matrix");
    
    KD_Uniform = glGetUniformLocation(gProgramShaderObject , "u_KD");
    
    Light_Position_Uniform = glGetUniformLocation(gProgramShaderObject , "u_Light_Position");
    
    LD_Uniform = glGetUniformLocation(gProgramShaderObject , "u_LD");
    
    LisPressed_Uniform = glGetUniformLocation( gProgramShaderObject , "u_LKeyPressed");

                                      
    //cube
      const GLfloat cubeVertices[] =
      {
          1.0f, 1.0f, 1.0f,
          -1.0f, 1.0f, 1.0f,
          -1.0f, -1.0f, 1.0f,
          1.0f, -1.0f, 1.0f,
  
          1.0f, 1.0f, -1.0f,
          1.0f, 1.0f, 1.0f,
          1.0f, -1.0f, 1.0f,
          1.0f, -1.0f, -1.0f,
          
          -1.0f, 1.0f, -1.0f,
          1.0f, 1.0f, -1.0f,
          1.0f, -1.0f, -1.0f,
          -1.0f, -1.0f, -1.0f,
          
          -1.0f, 1.0f, 1.0f,
          -1.0f, 1.0f, -1.0f,
          -1.0f, -1.0f, -1.0f,
          -1.0f, -1.0f, 1.0f,
          
          -1.0f, 1.0f, 1.0f,
          1.0f, 1.0f, 1.0f,
          1.0f, 1.0f, -1.0f,
          -1.0f, 1.0f, -1.0f,
          
          1.0f, -1.0f, 1.0f,
          -1.0f, -1.0f, 1.0f,
          -1.0f, -1.0f, -1.0f,
          1.0f, -1.0f, -1.0f
      };
  
      glGenVertexArrays(1, &Vao_cube);
      glBindVertexArray(Vao_cube);
  
      glGenBuffers(1, &Vbo_cube_position);
      glBindBuffer(GL_ARRAY_BUFFER, Vbo_cube_position);
      glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
  
      glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  
      glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
      
      glBindBuffer(GL_ARRAY_BUFFER, 0);
      
      //Lights - AMC_ATTRIBUTE_NORMAL
      const GLfloat CubeNormals[] =
      {
          0.0f,0.0f,1.0f,
          0.0f,0.0f,1.0f,
          0.0f,0.0f,1.0f,
          0.0f,0.0f,1.0f,
          
          1.0f,0.0f,0.0f,
          1.0f,0.0f,0.0f,
          1.0f,0.0f,0.0f,
          1.0f,0.0f,0.0f,
          
          0.0f,0.0f,-1.0f,
          0.0f,0.0f,-1.0f,
          0.0f,0.0f,-1.0f,
          0.0f,0.0f,-1.0f,
          
          -1.0f,0.0f,0.0f,
          -1.0f,0.0f,0.0f,
          -1.0f,0.0f,0.0f,
          -1.0f,0.0f,0.0f,
          
          0.0f,1.0f,0.0f,
          0.0f,1.0f,0.0f,
          0.0f,1.0f,0.0f,
          0.0f,1.0f,0.0f,
          
          0.0f,-1.0f,0.0f,
          0.0f,-1.0f,0.0f,
          0.0f,-1.0f,0.0f,
          0.0f,-1.0f,0.0f
      };
  
      //imp
      glGenBuffers(1, &Vbo_cube_normal);
      glBindBuffer(GL_ARRAY_BUFFER, Vbo_cube_normal);
      glBufferData(GL_ARRAY_BUFFER, sizeof(CubeNormals), CubeNormals, GL_STATIC_DRAW);
  
      glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  
      glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
  
      glBindBuffer(GL_ARRAY_BUFFER, 0);
  
      //common
      glBindVertexArray(0); //vao

      

    //depth
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    perspectiveProjectionMatrix = mat4::identity();
}


-(void) drawRect:(CGRect)rect {
    
}


-(void) layoutSubviews
{
    
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable: (CAEAGLLayer *)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    //Depth BUffer Regenration!
    glGenRenderbuffers(1, &depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        printf("Failed to Update FBO");
        glCheckFramebufferStatus(GL_FRAMEBUFFER);
    }
    else {
        printf("FBO Updated Successfully \n");
    }
    
    
    if(height == 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    GLfloat Width = (GLfloat)width;
    GLfloat Height = (GLfloat)height;
    
    perspectiveProjectionMatrix = perspective(45.0f, Width/Height, 0.1f, 100.0f);
    
    [self drawView: nil];
}


//VIMP

-(GLuint)loadTextureFromBMPFile:(NSString *)texFileName :(NSString *)extension
{
    NSString *textureFileNameWithPath = [[NSBundle mainBundle] pathForResource:texFileName ofType:extension];
    
    UIImage *bmpImage = [[UIImage alloc] initWithContentsOfFile:textureFileNameWithPath];
    
    if(!bmpImage)
    {
        NSLog(@"Can't find %@", textureFileNameWithPath);
        return(0);
    }
    
    CGImageRef cgImage = bmpImage.CGImage; //try
    
    int bmpWidth = (int)CGImageGetWidth(cgImage);
    int bmpHeight = (int)CGImageGetHeight(cgImage);
    
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    
    void* pixels = (void *)CFDataGetBytePtr(imageData);
    
    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 bmpWidth,
                 bmpHeight,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels); // imp
    
    glGenerateMipmap(GL_TEXTURE_2D); // new addition
    
    CFRelease(imageData);

    return(bmpTexture);

}



//Display
-(void) drawView: (id) sender
{
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    
    mat4 modelViewMatrix;
    mat4 modelviewProjMatrix;
    mat4 translateMatrix;
    mat4 Rotation_Matrix;

    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );
 
    glUseProgram(gProgramShaderObject);
        
    
    translateMatrix = mat4::identity();
    modelViewMatrix = mat4::identity();
    Rotation_Matrix = mat4::identity();
    
    
    translateMatrix = translate(0.0f, 0.0f, -7.0f);
    
    Rotation_Matrix = vmath::rotate(AngleRect, 0.0f , 1.0f, 0.0f);
    
    modelViewMatrix = translateMatrix * Rotation_Matrix;
    
    
    //uniform
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewMatrix);
     
    glUniformMatrix4fv(Projection_Uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    
    
     //imp
     if (bLighting == true)
         {
             glUniform1i(LisPressed_Uniform, 1);
             glUniform3f(LD_Uniform, 1.0f, 1.0f, 1.0f);
             glUniform3f(KD_Uniform, 0.4f, 0.2f, 0.2f);
     
             //imp : out to in Light
             glUniform4f(Light_Position_Uniform, 0.0f, 0.0f, 2.0f, 1.0f);
         }
     
         else
         {
             glUniform1i(LisPressed_Uniform, 0);
         }

    
     // 6 : bind to vao
     glBindVertexArray(Vao_cube);

     //8 : draw scene

     glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
     glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
     glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
     glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
     glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
     glDrawArrays(GL_TRIANGLE_FAN, 20, 4);


         //9 : Unbind vao
     glBindVertexArray(NULL);
     
     //common
     glUseProgram(0);

     AngleRect += 0.5f;
    
    if (AngleRect >= 360.0f)
    AngleRect -= 360.0f;
    
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void) startAnimation //update
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond: animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}


-(void) stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}


-(BOOL) acceptsFirstResponder
{
    return(YES);
}


//imp

-(void) addGestures
{
    
    UITapGestureRecognizer *singleTapGestureRecognizer =
    [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector (onSingleTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    // this will allow to differentiate
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    // swipe
    UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    // long press
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    
    
}


-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    //TO Start Our Code
}

-(void) onSingleTap:(UITapGestureRecognizer *) event {
    
}



-(void) onSwipe:(UISwipeGestureRecognizer *) event {
    [self release];
    exit(0);
}

-(void) onLongPress:(UILongPressGestureRecognizer *) event
{
    
}



-(void) onDoubleTap:(UITapGestureRecognizer *) event
{

    if (!bLighting)
    {
        bLighting = true;
        bisLPressed = true;
    }
    else
    {
        bLighting = false;
        bisLPressed = false;
    }
    
}



-(void) dealloc
{
    //Uninitialize

    
    //Safe Release
    if(gProgramShaderObject) {
        GLsizei iShaderCnt = 0;
        GLsizei iShaderNo = 0;
        
        glUseProgram(gProgramShaderObject);
        
        glGetProgramiv(gProgramShaderObject,
                       GL_ATTACHED_SHADERS,
                       &iShaderCnt);
        
        GLuint *pShaders = (GLuint*)malloc(iShaderCnt * sizeof(GLuint));
        
        if(pShaders) {
            glGetAttachedShaders(gProgramShaderObject,
                                 iShaderCnt,
                                 &iShaderCnt,
                                 pShaders);
            
            for(iShaderNo = 0; iShaderNo < iShaderCnt; iShaderNo++) {
                
                glDetachShader(gProgramShaderObject, pShaders[iShaderNo]);
                
                pShaders[iShaderNo] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gProgramShaderObject);
        gProgramShaderObject = 0;
        glUseProgram(0);
    }
    
    if(depthBuffer) {
        glDeleteRenderbuffers(1, &depthBuffer);
        depthBuffer = 0;
    }
    if(colorBuffer){
        glDeleteRenderbuffers(1, &colorBuffer);
        colorBuffer = 0;
    }
    
    if(frameBuffer) {
        glDeleteFramebuffers(1, &frameBuffer);
        frameBuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext) {
        [EAGLContext setCurrentContext:nil];
    }
    
    if(eaglContext) {
        eaglContext = nil;
    }
    
    [super dealloc];
}



@end



