//
//  GLESView.m
//
//  Copyright © 2020 AstroMediComp. All rights reserved.

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "GLESView.h"
#import "vmath.h"
#import "YSR_Sphere.h"

using namespace vmath;


@implementation GLESView
{
    
    EAGLContext *eaglContext;
    
    GLuint frameBuffer;
    GLuint colorBuffer;
    GLuint depthBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    // Shader Variables
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gProgramShaderObject;

   //12 Uniforms
    GLfloat AngleTri;
    
    //Blue
    GLuint LD_Uniform, LA_Uniform, LS_Uniform;
    GLuint KD_Uniform, KA_Uniform, KS_Uniform;
    
    GLuint Material_Shininess;
    GLuint Light_Position_Uniform;
    GLuint LisPressed_Uniform;
    
    //Red
    GLuint LD_Uniform_Red, LA_Uniform_Red, LS_Uniform_Red;
    GLuint KD_Uniform_Red, KA_Uniform_Red, KS_Uniform_Red;
    GLuint Light_Position_Uniform_Red;
   
    
    bool bisLPressed;
    bool bLighting;
    
    GLuint mvpUniform, Projection_Uniform, View_Uniform;
    
    mat4 perspectiveProjectionMatrix;

    GLuint Vao_pyramid;
    GLuint Vbo_pyramid_position;
    GLuint Vbo_pyramid_normal;

      
    
}


-(id) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    
    if(self) {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil) {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext: eaglContext];
        
        //Frame Buffer! Thing!
        glGenFramebuffers(1, &frameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
        //COlor Buffer!
        glGenRenderbuffers(1, &colorBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorBuffer);
        
        //Depth Buffer
        GLint backWidth;
        GLint backHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backHeight);
        
        glGenRenderbuffers(1, &depthBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backWidth, backHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            printf("FAILED :: TO create FBO");
            glDeleteRenderbuffers(1, &depthBuffer);
            glDeleteRenderbuffers(1, &colorBuffer);
            glDeleteFramebuffers(1, &frameBuffer);
            
            return(nil);
        }
        else {
            printf("FBO Created \n");
        }
        
        printf("GLES RENDERER : %s\n GLES VERSION : %s\n GLSL Version : %s \n",
               glGetString(GL_RENDERER),
               glGetString(GL_VERSION),
               glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        isAnimating = NO;
        animationFrameInterval = 60;
        
        [self initialize];
        
        //Gesture SetUP
        [self addGestures];
        
    }
    else {
        printf("Failed To Acquire the Instance \n");
    }
    
    return(self);
}

+(Class) layerClass {
    return ([CAEAGLLayer class]);
}


-(void) initialize
{
    GLint iShaderCompileStatus = 0;
    GLint iShaderInfoLogLen = 0;
    GLchar* szInfoLog = NULL;
    
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // Vertex Shader
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;"
        "precision highp int;"
        "in vec4 vPosition;"                        \
        "in vec3 vNormal;"                            \
        "out vec3 T_Norm;"                            \
        "out vec3 Light_Direction;"                    \
        "out vec3 Light_Direction_R;"                \
        "out vec3 Light_Direction_G;"                \
        "out vec3 Viewer_Vector;"                    \
        "\n"                                        \
        "uniform mat4 u_model_matrix;"                \
        "uniform mat4 u_view_matrix;"                \
        "uniform mat4 u_projection_matrix;"            \
        "uniform int u_LKeyPressed;"                \
        "uniform vec4 u_Light_Position;"            \
        "uniform vec4 u_Light_Position_R;"            \
        "uniform vec4 u_Light_Position_G;"            \
        "\n"                                        \
        "void main(void)"                            \
        "{"                                            \
        "if(u_LKeyPressed == 1)"                    \
        "{"                                            \
        "vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"    \
        "T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"            \
        "Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"    \
        "Light_Direction_R = vec3(u_Light_Position_R) - (Eye_Coordinates.xyz);"    \
        "Light_Direction_G = vec3(u_Light_Position_G) - (Eye_Coordinates.xyz);"    \
        "Viewer_Vector = vec3(-Eye_Coordinates.xyz);"    \
        "}"        \
        "gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"    \
        "}" ;

  

    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    
    //Compile
    glCompileShader(gVertexShaderObject);
    
    //Error Checking.
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE) {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        
        if(iShaderInfoLogLen > 0) {
            szInfoLog = (GLchar*)malloc(iShaderInfoLogLen);
            
            if(szInfoLog != NULL) {
                GLsizei written;
                
                glGetShaderInfoLog(gVertexShaderObject,
                                   iShaderInfoLogLen,
                                   &written,
                                   szInfoLog);
                
                printf("Vertex Shader Failed:\n %s\n", szInfoLog);
                
                free(szInfoLog);
                [self release];
            }
        }
    }
    else {
        printf("Vertex Shader Compiled..\n");
    }
    
    //Fragment Shader
    iShaderCompileStatus = 0;
    iShaderInfoLogLen = 0;
    szInfoLog = NULL;
    
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;"
        "precision highp int;"
        "in vec3 T_Norm;"            \
        "in vec3 Light_Direction;"    \
        "in vec3 Light_Direction_R;"    \
        "in vec3 Light_Direction_G;"    \
        "in vec3 Viewer_Vector;"    \
        "out vec4 FragColor;"        \
        "\n"                        \
        "uniform vec3 u_LA;"                \
        "uniform vec3 u_LD;"                \
        "uniform vec3 u_LS;"                \
        "uniform vec3 u_KA;"                \
        "uniform vec3 u_KD;"                \
        "uniform vec3 u_KS;"                \
        "uniform vec3 u_LA_R;"                \
        "uniform vec3 u_LD_R;"                \
        "uniform vec3 u_LS_R;"                \
        "uniform vec3 u_KA_R;"                \
        "uniform vec3 u_KD_R;"                \
        "uniform vec3 u_KS_R;"                \
        "uniform float u_Shininess;"        \
        "uniform int u_LKeyPressed;"        \
        "uniform vec4 u_Light_Position;"    \
        "uniform vec4 u_Light_Position_R;"    \
        "\n"                                \
        "void main(void)"                    \
        "{"                        \
        "if(u_LKeyPressed == 1)"\
        "{"                        \
        "vec3 Normalized_View_Vector = normalize(Viewer_Vector);"    \
        "vec3 Normalized_Light_Direction = normalize(Light_Direction);"\
        "vec3 Normalized_Light_Direction_R = normalize(Light_Direction_R);"\
        "\n"\
        "vec3 Normalized_TNorm = normalize(T_Norm);"\
        "float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0);"    \
        "vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm);"\
        "float TN_Dot_LD_R = max(dot(Normalized_Light_Direction_R,Normalized_TNorm), 0.0);"    \
        "vec3 Reflection_Vector_R = reflect(-Normalized_Light_Direction_R, Normalized_TNorm);"\
        "\n" \
        "vec3 Ambient = vec3(u_LA * u_KA);"    \
        "vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);"    \
        "vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess));" \
        "vec3 Ambient_R = vec3(u_LA_R * u_KA_R);"    \
        "vec3 Diffuse_R = vec3(u_LD_R * u_KD_R * TN_Dot_LD_R);"    \
        "vec3 Specular_R = vec3(u_LS_R * u_KS_R * pow(max(dot(Reflection_Vector_R, Normalized_View_Vector), 0.0), u_Shininess));" \
        "vec3 Phong_ADS_Light = Ambient + Diffuse + Specular;"    \
        "vec3 Phong_ADS_Light_R = Ambient_R + Diffuse_R+ Specular_R;"    \
        "FragColor = vec4(Phong_ADS_Light+Phong_ADS_Light_R,1.0);" \
        "}"                            \
        "else"                        \
        "{"                            \
        "FragColor = vec4(1.0,1.0,1.0,1.0);" \
        "}"                            \
        "}" ;


    
    
    glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    
    glCompileShader(gFragmentShaderObject);
    
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        
        if(iShaderInfoLogLen > 0) {
            szInfoLog = (GLchar*)malloc(iShaderInfoLogLen);
            if(szInfoLog != NULL) {
                GLint written;
                
                glGetShaderInfoLog(gFragmentShaderObject,
                                   iShaderInfoLogLen,
                                   &written,
                                   szInfoLog);
                
                printf("Fragment Shader Failed:\n %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    else {
        printf("Fragement Shader Compiled..\n");
    }
    
//3
    GLint iProgLinkStatus = 0;
    GLint iProgLogLen = 0;
    GLchar* szProgLog = NULL;

    gProgramShaderObject = glCreateProgram();
    
    //Attach VS to Shader Prog
    glAttachShader(gProgramShaderObject, gVertexShaderObject);
    
    //Attach FS to Shader Prog
    glAttachShader(gProgramShaderObject, gFragmentShaderObject);
    
    //Prelink
    glBindAttribLocation(gProgramShaderObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gProgramShaderObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
    
    //Link
    glLinkProgram(gProgramShaderObject);
    
    glGetProgramiv(gProgramShaderObject, GL_LINK_STATUS, &iProgLinkStatus);
    
    if(iProgLinkStatus == GL_FALSE) {
        glGetProgramiv(gProgramShaderObject, GL_INFO_LOG_LENGTH, &iProgLogLen);
        
        if(iProgLogLen > 0) {
            szProgLog = (GLchar*)malloc(iProgLogLen);
            
            if(szProgLog != NULL) {
                GLint written;
                
                glGetProgramInfoLog(gProgramShaderObject, iProgLogLen, &written, szProgLog);
                
                printf("Program Link Failed :\n %s\n",szProgLog);
                
                [self release];
            }
        }
    }
    else {
        printf("Program Linked Successfully..\n");
    }
    
    //POST LINKING
    mvpUniform = glGetUniformLocation(gProgramShaderObject,"u_model_matrix");

    View_Uniform = glGetUniformLocation(gProgramShaderObject, "u_view_matrix");
       
    Projection_Uniform = glGetUniformLocation(gProgramShaderObject, "u_projection_matrix");
    
    KD_Uniform = glGetUniformLocation(gProgramShaderObject , "u_KD");
    
    Light_Position_Uniform = glGetUniformLocation(gProgramShaderObject , "u_Light_Position");
    
    LD_Uniform = glGetUniformLocation(gProgramShaderObject , "u_LD");
    
    LisPressed_Uniform = glGetUniformLocation( gProgramShaderObject , "u_LKeyPressed");
    
    LA_Uniform = glGetUniformLocation(gProgramShaderObject , "u_LA");
    KA_Uniform = glGetUniformLocation(gProgramShaderObject, "u_KA");

    LS_Uniform = glGetUniformLocation(gProgramShaderObject, "u_LS");
    KS_Uniform = glGetUniformLocation(gProgramShaderObject, "u_KS");

    Material_Shininess = glGetUniformLocation(gProgramShaderObject, "u_Shininess");

    //Red

    LD_Uniform_Red = glGetUniformLocation(gProgramShaderObject, "u_LD_R");
    KD_Uniform_Red = glGetUniformLocation(gProgramShaderObject, "u_KD_R");

    LA_Uniform_Red = glGetUniformLocation(gProgramShaderObject, "u_LA_R");
    KA_Uniform_Red = glGetUniformLocation(gProgramShaderObject, "u_KA_R");

    LS_Uniform_Red = glGetUniformLocation(gProgramShaderObject, "u_LS_R");
    KS_Uniform_Red = glGetUniformLocation(gProgramShaderObject, "u_KS_R");

    LisPressed_Uniform = glGetUniformLocation(gProgramShaderObject, "u_LKeyPressed");
    
    Light_Position_Uniform_Red = glGetUniformLocation(gProgramShaderObject, "u_Light_Position_R");

    //imp
    //pyramid
    const GLfloat pyramidVertices[] =
         {
               0.0f, 1.0f, 0.0f,
               -1.0f, -1.0f, 1.0f,
               1.0f, -1.0f, 1.0f,
               0.0f, 1.0f, 0.0f,
               1.0f, -1.0f, 1.0f,
               1.0f, -1.0f, -1.0f,
               0.0f, 1.0f, 0.0f,
               1.0f, -1.0f, -1.0f,
               -1.0f, -1.0f, -1.0f,
               0.0f, 1.0f, 0.0f,
               -1.0f, -1.0f, -1.0f,
               -1.0f, -1.0f, 1.0f
        };
   
       glGenVertexArrays(1, &Vao_pyramid);
       glBindVertexArray(Vao_pyramid);
   
       //for vertices
       glGenBuffers(1, &Vbo_pyramid_position);
       glBindBuffer(GL_ARRAY_BUFFER, Vbo_pyramid_position);
       glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
   
       glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
   
       glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
   
       glBindBuffer(GL_ARRAY_BUFFER, 0);
   
       // normal vbo
       const GLfloat pyramid_normals[] =
       {
           0.0f,0.447214f,0.894427f, //1
           0.0f,0.447214f,0.894427f, //2
           0.0f,0.447214f,0.894427f, //3
           0.894427f,0.447214f,0.0f, //4
           0.894427f,0.447214f,0.0f, //5
           0.894427f,0.447214f,0.0f, //6
           0.0f,0.447214f,-0.894427f, //7
           0.0f,0.447214f,-0.894427f, //8
           0.0f,0.447214f,-0.894427f, //9
           -0.894427f,0.447214f,0.0f, //10
           -0.894427f,0.447214f,0.0f, //11
           -0.894427f,0.447214f,0.0f //12
       };
   
       glGenBuffers(1, &Vbo_pyramid_normal);
       glBindBuffer(GL_ARRAY_BUFFER, Vbo_pyramid_normal);
       glBufferData(GL_ARRAY_BUFFER, sizeof(pyramid_normals), pyramid_normals, GL_STATIC_DRAW);
   
       glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
   
       glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
   
       glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);


 
    //depth
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    AngleTri = 0.0f;

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    perspectiveProjectionMatrix = mat4::identity();
}


-(void) drawRect:(CGRect)rect {
    
}

//imp
-(void) addGestures
{
    
    UITapGestureRecognizer *singleTapGestureRecognizer =
    [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector (onSingleTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:1];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    // this will allow to differentiate
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    // swipe
    UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    // long press
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    
    
}



-(void) layoutSubviews
{
    
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable: (CAEAGLLayer *)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    //Depth BUffer Regenration!
    glGenRenderbuffers(1, &depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        printf("Failed to Update FBO");
        glCheckFramebufferStatus(GL_FRAMEBUFFER);
    }
    else {
        printf("FBO Updated Successfully \n");
    }
    
    
    if(height == 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    GLfloat Width = (GLfloat)width;
    GLfloat Height = (GLfloat)height;
    
    perspectiveProjectionMatrix = perspective(45.0f, Width/Height, 0.1f, 100.0f);
    
    [self drawView: nil];
}


//VIMP

-(GLuint)loadTextureFromBMPFile:(NSString *)texFileName :(NSString *)extension
{
    NSString *textureFileNameWithPath = [[NSBundle mainBundle] pathForResource:texFileName ofType:extension];
    
    UIImage *bmpImage = [[UIImage alloc] initWithContentsOfFile:textureFileNameWithPath];
    
    if(!bmpImage)
    {
        NSLog(@"Can't find %@", textureFileNameWithPath);
        return(0);
    }
    
    CGImageRef cgImage = bmpImage.CGImage; //try
    
    int bmpWidth = (int)CGImageGetWidth(cgImage);
    int bmpHeight = (int)CGImageGetHeight(cgImage);
    
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    
    void* pixels = (void *)CFDataGetBytePtr(imageData);
    
    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 bmpWidth,
                 bmpHeight,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels); // imp
    
    glGenerateMipmap(GL_TEXTURE_2D); // new addition
    
    CFRelease(imageData);

    return(bmpTexture);

}



//Display
-(void) drawView: (id) sender
{
        [EAGLContext setCurrentContext:eaglContext];
        
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
        
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );
        bLighting = true;
        glUseProgram(gProgramShaderObject);
        
    
       mat4 Model_Matrix;
       mat4 View_Matrix;
       mat4 Projection_Matrix;
       mat4 Translation_Matrix;
       mat4 Rotation_Matrix;
       mat4 ModelView_Projection_Matrix;
       
       //2 : I[]
       Model_Matrix = mat4::identity();
       ModelView_Projection_Matrix = mat4::identity();
       View_Matrix = mat4::identity();
       Rotation_Matrix = mat4::identity();
       Translation_Matrix = mat4::identity();

       //4 : Do Mat mul
       Translation_Matrix = translate(0.0f, 0.0f, -6.0f);
    
       Rotation_Matrix = rotate(AngleTri, 0.0f, 1.0f, 0.0f);

       Model_Matrix = Translation_Matrix * Rotation_Matrix;


       // 5    : send it to shader

       glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, Model_Matrix);
       
       glUniformMatrix4fv(Projection_Uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
       
       glUniformMatrix4fv(View_Uniform, 1, GL_FALSE, View_Matrix);
       
       if (bLighting == true)
             {
                 glUniform1i(LisPressed_Uniform, 1);
         
                 glUniform3f(LD_Uniform, 0.0f, 0.0f, 1.0f);
                 glUniform3f(KD_Uniform, 1.0f, 1.0f, 1.0f);
         
                 glUniform3f(LS_Uniform, 1.0f, 1.0f, 1.0f);
                 glUniform3f(KS_Uniform, 1.0f, 1.0f, 1.0f);
                 
                 glUniform3f(LA_Uniform, 0.0f, 0.0f, 0.0f);
                 glUniform3f(KA_Uniform, 0.25f, 0.25f, 0.25f);
                 
                 glUniform1f(Material_Shininess, 128.0f);
                 
                 //imp : out to in + Light
                 glUniform4f(Light_Position_Uniform,5.0f,0.0f,-5.0f,1.0f);
         
                 //2nd Light
         
                 glUniform3f(LD_Uniform_Red, 1.0f, 0.0f, 0.0f);
                 glUniform3f(KD_Uniform_Red, 1.0f, 1.0f, 1.0f);
         
                 glUniform3f(LS_Uniform_Red, 1.0f, 1.0f, 1.0f);
                 glUniform3f(KS_Uniform_Red, 1.0f, 1.0f, 1.0f);
         
                 glUniform3f(LA_Uniform_Red, 0.0f, 0.0f, 0.0f);
                 glUniform3f(KA_Uniform_Red, 0.25f, 0.25f, 0.25f);
         
                 glUniform4f(Light_Position_Uniform_Red,-5.0f,0.0f,-5.0f,1.0f);
         
         
             }

       else
       {
           glUniform1i(LisPressed_Uniform, 0);
       }

    //Scene
    // 6 : bind to vao
    glBindVertexArray(Vao_pyramid);


    //8 : draw scene
    glDrawArrays(GL_TRIANGLES, 0, 12);

    // *** unbind vao ***
    glBindVertexArray(0);

    
    AngleTri += 0.8f;
      
          if (AngleTri >= 360.0f)
              AngleTri -= 359.0f;
      
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}


-(void) startAnimation //update
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond: animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}


-(void) stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}


-(BOOL) acceptsFirstResponder{
    return(YES);
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    //TO Start Our Code
}

-(void) onSingleTap:(UITapGestureRecognizer *) event
{
    if (!bLighting)
    {
        bLighting = true;
        bisLPressed = true;
    }
    else
    {
        bLighting = false;
        bisLPressed = false;
    }
    
}

-(void) onDoubleTap:(UITapGestureRecognizer *) event
{

    
    
}

-(void) onSwipe:(UISwipeGestureRecognizer *) event {
    [self release];
    exit(0);
}

-(void) onLongPress:(UILongPressGestureRecognizer *) event {
    
}

-(void) dealloc
{
    //Uninitialize

/*
    //Vao & vbo
    
    if(vbo_rectangle) {
        glDeleteBuffers(1, &vbo_rectangle);
        vbo_rectangle = 0;
    }
    if(vao_rectangle) {
        glDeleteVertexArrays(1, &vao_rectangle);
        vao_rectangle = 0;
    }
*/
    
    //Safe Release
    if(gProgramShaderObject) {
        GLsizei iShaderCnt = 0;
        GLsizei iShaderNo = 0;
        
        glUseProgram(gProgramShaderObject);
        
        glGetProgramiv(gProgramShaderObject,
                       GL_ATTACHED_SHADERS,
                       &iShaderCnt);
        
        GLuint *pShaders = (GLuint*)malloc(iShaderCnt * sizeof(GLuint));
        
        if(pShaders) {
            glGetAttachedShaders(gProgramShaderObject,
                                 iShaderCnt,
                                 &iShaderCnt,
                                 pShaders);
            
            for(iShaderNo = 0; iShaderNo < iShaderCnt; iShaderNo++) {
                
                glDetachShader(gProgramShaderObject, pShaders[iShaderNo]);
                
                pShaders[iShaderNo] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gProgramShaderObject);
        gProgramShaderObject = 0;
        glUseProgram(0);
    }
    
    if(depthBuffer) {
        glDeleteRenderbuffers(1, &depthBuffer);
        depthBuffer = 0;
    }
    if(colorBuffer){
        glDeleteRenderbuffers(1, &colorBuffer);
        colorBuffer = 0;
    }
    
    if(frameBuffer) {
        glDeleteFramebuffers(1, &frameBuffer);
        frameBuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext) {
        [EAGLContext setCurrentContext:nil];
    }
    
    if(eaglContext) {
        eaglContext = nil;
    }
    
    [super dealloc];
}



@end



