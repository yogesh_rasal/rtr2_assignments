//
//  GLESView.m
//
//  Copyright © 2020 AstroMediComp. All rights reserved.

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "GLESView.h"
#import "vmath.h"

using namespace vmath;

enum {
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};


#define SLICES 20
#define NUM_POINTS 1000

@implementation GLESView
{
    
    EAGLContext *eaglContext;
    
    GLuint frameBuffer;
    GLuint colorBuffer;
    GLuint depthBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    // Shader Variables
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gProgramShaderObject;

    GLuint mvpUniform;
    GLuint samplerUniform;
    mat4 PerspectiveProjectionMatrix;

    //new
    GLuint vao_Triangle,vao_Rectagle;
    GLuint vbo_Position_Triangle;
    GLuint vbo_Position_Rectangle;
    GLuint vbo_Color_Triangle;
    GLuint vbo_Color_Rectangle;


    //for circle
    GLuint vao_Circle, vbo_Position_Circle, vbo_Color_Circle;

      
}


-(id) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    
    if(self) {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil) {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext: eaglContext];
        
        //Frame Buffer! Thing!
        glGenFramebuffers(1, &frameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
        //COlor Buffer!
        glGenRenderbuffers(1, &colorBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorBuffer);
        
        //Depth Buffer
        GLint backWidth;
        GLint backHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backHeight);
        
        glGenRenderbuffers(1, &depthBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backWidth, backHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            printf("FAILED :: TO create FBO");
            glDeleteRenderbuffers(1, &depthBuffer);
            glDeleteRenderbuffers(1, &colorBuffer);
            glDeleteFramebuffers(1, &frameBuffer);
            
            return(nil);
        }
        else {
            printf("FBO Created \n");
        }
        
        printf("GLES RENDERER : %s\n GLES VERSION : %s\n GLSL Version : %s \n",
               glGetString(GL_RENDERER),
               glGetString(GL_VERSION),
               glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        isAnimating = NO;
        animationFrameInterval = 60;
        
        [self initialize];
        
        //Gesture SetUP
        [self addGestures];
        
    }
    else {
        printf("Failed To Acquire the Instance \n");
    }
    
    return(self);
}

+(Class) layerClass {
    return ([CAEAGLLayer class]);
}

//imp
-(void) addGestures
{
    
    UITapGestureRecognizer *singleTapGestureRecognizer =
    [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector (onSingleTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    // this will allow to differentiate
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    // swipe
    UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    // long press
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    
    
}




-(void) initialize
{
    GLint iShaderCompileStatus = 0;
    GLint iShaderInfoLogLen = 0;
    GLchar* szInfoLog = NULL;
    
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // Vertex Shader
    const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;"                        \
        "in vec4 vColor;"                            \
        "out vec4 Out_Color;"                        \
        "uniform mat4 u_mvp_matrix;"                \
        "void main(void)"                            \
        "{"                                           \
        "gl_Position = u_mvp_matrix * vPosition;"    \
        "Out_Color = vColor;"                        \
        "}" ;



    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    
    //Compile
    glCompileShader(gVertexShaderObject);
    
    //Error Checking.
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE) {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        
        if(iShaderInfoLogLen > 0) {
            szInfoLog = (GLchar*)malloc(iShaderInfoLogLen);
            
            if(szInfoLog != NULL) {
                GLsizei written;
                
                glGetShaderInfoLog(gVertexShaderObject,
                                   iShaderInfoLogLen,
                                   &written,
                                   szInfoLog);
                
                printf("Vertex Shader Failed:\n %s\n", szInfoLog);
                
                free(szInfoLog);
                [self release];
            }
        }
    }
    else {
        printf("Vertex Shader Compiled..\n");
    }
    
    //Fragment Shader
    iShaderCompileStatus = 0;
    iShaderInfoLogLen = 0;
    szInfoLog = NULL;
    
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar *fragmentShaderSourceCode =
    "#version 300 es" \
    "\n"\
    "precision highp float;"
    "in vec4 Out_Color;"    \
    "out vec4 FragColor;"    \
    "void main(void)"        \
    " { "                    \
    "FragColor = Out_Color;" \
    " } " ;

    
    glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    
    glCompileShader(gFragmentShaderObject);
    
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        
        if(iShaderInfoLogLen > 0) {
            szInfoLog = (GLchar*)malloc(iShaderInfoLogLen);
            if(szInfoLog != NULL) {
                GLint written;
                
                glGetShaderInfoLog(gFragmentShaderObject,
                                   iShaderInfoLogLen,
                                   &written,
                                   szInfoLog);
                
                printf("Fragment Shader Failed:\n %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    else {
        printf("Fragement Shader Compiled..\n");
    }
    
//3
    GLint iProgLinkStatus = 0;
    GLint iProgLogLen = 0;
    GLchar* szProgLog = NULL;

    gProgramShaderObject = glCreateProgram();
    
    //Attach VS to Shader Prog
    glAttachShader(gProgramShaderObject, gVertexShaderObject);
    
    //Attach FS to Shader Prog
    glAttachShader(gProgramShaderObject, gFragmentShaderObject);
    
    //Prelink
    glBindAttribLocation(gProgramShaderObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gProgramShaderObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
    
    
    //Link
    glLinkProgram(gProgramShaderObject);
    
    glGetProgramiv(gProgramShaderObject, GL_LINK_STATUS, &iProgLinkStatus);
    
    if(iProgLinkStatus == GL_FALSE) {
        glGetProgramiv(gProgramShaderObject, GL_INFO_LOG_LENGTH, &iProgLogLen);
        
        if(iProgLogLen > 0) {
            szProgLog = (GLchar*)malloc(iProgLogLen);
            
            if(szProgLog != NULL) {
                GLint written;
                
                glGetProgramInfoLog(gProgramShaderObject, iProgLogLen, &written, szProgLog);
                
                printf("Program Link Failed :\n %s\n",szProgLog);
                
                [self release];
            }
        }
    }
    else {
        printf("Program Linked Successfully..\n");
    }
    
    //POST LINKING
    mvpUniform = glGetUniformLocation(gProgramShaderObject, "u_mvp_matrix");
   
        //shapes


	//use Triangle vertices
	const GLfloat Triangle_Vertices[] =
    {   0.0f,1.0f,0.0f,
        -1.0f,-1.0f,0.0f,
        
        1.0f,-1.0f,0.0f,
        0.0f,1.0f,0.0f,
        
        1.0f,-1.0f,0.0f,
       -1.0f,-1.0f,0.0f

    };

	const GLfloat Triangle_Colors[] =
    {
          1.0f,1.0f,1.0f,
          1.0f,1.0f,1.0f,
         
          1.0f,1.0f,1.0f,
          1.0f,1.0f,1.0f,
        
          1.0f,1.0f,1.0f,
          1.0f,1.0f,1.0f,
          
    };

	//create vao for Triangle
	glGenVertexArrays(1, &vao_Triangle);

	//Binding
	glBindVertexArray(vao_Triangle);

	//Buffer
	glGenBuffers(1, &vbo_Position_Triangle);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_Position_Triangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Triangle_Vertices),Triangle_Vertices,GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	//vao

	//Color
	//Buffer
	glGenBuffers(1, &vbo_Color_Triangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Triangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Triangle_Colors),
		Triangle_Colors, GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	glBindVertexArray(0); //vao

	//**********
	const GLfloat Rectangle_Vertices[] =
    {
		0.0f,-1.0f,0.0f,
		0.0f,1.0f,0.0f
	};

	const GLfloat Rectangle_Colors[] =
    {
		1.0f,0.50f,0.50f,
		1.0f,0.50f,0.50f,
        
    };

	//create vao for Rectangle
	glGenVertexArrays(1, &vao_Rectagle);

	//Binding
	glBindVertexArray(vao_Rectagle);

	//Buffer
	glGenBuffers(1, &vbo_Position_Rectangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Rectangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Rectangle_Vertices),Rectangle_Vertices, GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Color

	glGenBuffers(1, &vbo_Color_Rectangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Rectangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Rectangle_Colors),	Rectangle_Colors, GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); //vao
	

    //Circle
    GLfloat fPerimeter;
    GLfloat fRadius;

    GLfloat CircleVertices[NUM_POINTS][3];
    GLfloat CircleColor[NUM_POINTS][3];

    //Code
    fPerimeter = (GLfloat)sqrt(((1.0f)*(1.0f)) + ((1.0f)*(1.0f))); //imp
    fRadius = fPerimeter / 2;

    for (int i = 0; i < NUM_POINTS; i++)
    {
        GLfloat angle = (GLfloat)(2 * M_PI * i) / NUM_POINTS; //2 Pie R
        CircleVertices[i][0] = (GLfloat)cos(angle) * fRadius;
        CircleVertices[i][1] = (GLfloat)sin(angle) * fRadius;
        CircleVertices[i][2] = 0.0f;

        //yellow
        CircleColor[i][0] = 1.0f;
        CircleColor[i][1] = 1.0f;
        CircleColor[i][2] = 0.0f;
    }


    // Create vao
    glGenVertexArrays(1, &vao_Circle);
    glBindVertexArray(vao_Circle);

    glGenBuffers(1, &vbo_Position_Circle);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Circle);

    glBufferData(GL_ARRAY_BUFFER, sizeof(CircleVertices), CircleVertices, GL_STATIC_DRAW);

    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    // Unbind vbo
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Bind vboColor
    glGenBuffers(1, &vbo_Color_Circle);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Circle);

    glBufferData(GL_ARRAY_BUFFER, sizeof(CircleColor), CircleColor, GL_STATIC_DRAW);

    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

    // Unbind vbo
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Unbind
    glBindVertexArray(0);

   
    //depth
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);


    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);  //imp

    
    PerspectiveProjectionMatrix = mat4::identity();
}


-(void) drawRect:(CGRect)rect
{
    
}


-(BOOL) acceptsFirstResponder{
    return(YES);
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    //TO Start Our Code
}

-(void) onSingleTap:(UITapGestureRecognizer *) event {
    
}

-(void) onDoubleTap:(UITapGestureRecognizer *) event
{
    
    printf("\n in Double Tap ..");
    
}

-(void) onSwipe:(UISwipeGestureRecognizer *) event {
    [self release];
    exit(0);
}

-(void) onLongPress:(UILongPressGestureRecognizer *) event
{
    
}



-(void) layoutSubviews
{
    
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable: (CAEAGLLayer *)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    //Depth BUffer Regenration!
    glGenRenderbuffers(1, &depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        printf("Failed to Update FBO");
        glCheckFramebufferStatus(GL_FRAMEBUFFER);
    }
    else {
        printf("FBO Updated Successfully \n");
    }
    
    
    if(height == 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    GLfloat Width = (GLfloat)width;
    GLfloat Height = (GLfloat)height;
    
    PerspectiveProjectionMatrix = perspective(45.0f, Width/Height, 0.1f, 100.0f);
    
    [self drawView: nil];
}



//Display
-(void) drawView: (id) sender
{
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(gProgramShaderObject);
    
       //code : 9 steps
        //1
    mat4 ModelView_Matrix;
    mat4 ModelView_Projection_Matrix;
    mat4 Translation_Matrix;
    mat4 Rotation_Matrix;

    //Circle
    //2 : I[]
    Rotation_Matrix = mat4::identity();
    ModelView_Matrix = mat4::identity();
    ModelView_Projection_Matrix = mat4::identity();
    Translation_Matrix = mat4::identity();

    //3 : Transforms
    Translation_Matrix = translate(0.0f, -0.45f, -3.89f);

    //4 : Do Mat mul
    ModelView_Matrix = Translation_Matrix * Rotation_Matrix;

    //Sequence is imp
    ModelView_Projection_Matrix = PerspectiveProjectionMatrix * ModelView_Matrix;


    // 5 : send it to shader
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

    //Bind
    glBindVertexArray(vao_Circle);

    //draw
    glLineWidth(2.0f);
    glDrawArrays(GL_LINE_LOOP, 0, NUM_POINTS);

    // Unbind vao
    glBindVertexArray(0);

    
    //Rectangle
    //2 : I[]
    ModelView_Matrix = mat4::identity();
    ModelView_Projection_Matrix = mat4::identity();
    Rotation_Matrix = mat4::identity();
    Translation_Matrix = mat4::identity();

    //4 : Do Mat mul
    Translation_Matrix = translate(0.0f, 0.0f, -3.40f);

    ModelView_Matrix = Translation_Matrix * Rotation_Matrix;

    ModelView_Projection_Matrix = PerspectiveProjectionMatrix * ModelView_Matrix;


    // 5 : send it to shader
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

    // 6 : bind to vao
    glBindVertexArray(vao_Rectagle);

    //8 : draw scene
    glDrawArrays(GL_LINES, 0, 2);

    //9 : Unbind vao
    glBindVertexArray(0);

    
//Triangle
    //2 : I[]
    Rotation_Matrix = mat4::identity();
    ModelView_Matrix = mat4::identity();
    ModelView_Projection_Matrix = mat4::identity();
    Translation_Matrix = mat4::identity();

    //3 : Transforms

    Translation_Matrix = translate(0.0f, 0.0f, -3.32f);

    //4 : Do Mat mul

    ModelView_Matrix = Translation_Matrix * Rotation_Matrix;

    //new
    ModelView_Projection_Matrix = PerspectiveProjectionMatrix * ModelView_Matrix;


    // 5 : send it to shader
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

    glBindVertexArray(vao_Triangle);

    //8 : draw scene
    glLineWidth(3.0f);
    glDrawArrays(GL_LINES, 0, 6);

    //9 : Unbind vao
    glBindVertexArray(0);
    
    
   
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void) startAnimation //update
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond: animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}


-(void) stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

-(void) dealloc
{
    
    //Safe Release
    if(gProgramShaderObject) {
        GLsizei iShaderCnt = 0;
        GLsizei iShaderNo = 0;
        
        glUseProgram(gProgramShaderObject);
        
        glGetProgramiv(gProgramShaderObject,
                       GL_ATTACHED_SHADERS,
                       &iShaderCnt);
        
        GLuint *pShaders = (GLuint*)malloc(iShaderCnt * sizeof(GLuint));
        
        if(pShaders) {
            glGetAttachedShaders(gProgramShaderObject,
                                 iShaderCnt,
                                 &iShaderCnt,
                                 pShaders);
            
            for(iShaderNo = 0; iShaderNo < iShaderCnt; iShaderNo++) {
                
                glDetachShader(gProgramShaderObject, pShaders[iShaderNo]);
                
                pShaders[iShaderNo] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gProgramShaderObject);
        gProgramShaderObject = 0;
        glUseProgram(0);
    }
    
    if(depthBuffer) {
        glDeleteRenderbuffers(1, &depthBuffer);
        depthBuffer = 0;
    }
    if(colorBuffer){
        glDeleteRenderbuffers(1, &colorBuffer);
        colorBuffer = 0;
    }
    
    if(frameBuffer) {
        glDeleteFramebuffers(1, &frameBuffer);
        frameBuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext) {
        [EAGLContext setCurrentContext:nil];
    }
    
    if(eaglContext) {
        eaglContext = nil;
    }
    
    [super dealloc];
}



@end



