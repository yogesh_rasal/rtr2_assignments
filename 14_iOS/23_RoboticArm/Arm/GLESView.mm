//
//  GLESView.m
//
//  Copyright © 2020 AstroMediComp. All rights reserved.

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "GLESView.h"
#import "vmath.h"
#import "YSR_Sphere.h"

using namespace vmath;

int Shoulder = 0;
int Elbow = 0;

@implementation GLESView
{
    
    EAGLContext *eaglContext;
    
    GLuint frameBuffer;
    GLuint colorBuffer;
    GLuint depthBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    // Shader Variables
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gProgramShaderObject;
    
    GLuint mvpUniform;
    mat4 perspectiveProjectionMatrix;

    GLuint samplerUniform;

    //new
    GLuint gVao_sphere,gVbo_sphere_texture;
    GLuint gVbo_sphere_position;
    GLuint gVbo_sphere_normal;
    GLuint gVbo_sphere_element;
    
    int gNumVertices , gNumElements;

    
    //new
    GLuint Texture_Pyramid,Texture_Cube;
    GLuint Texture_Moon;

    

}


-(id) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    
    if(self) {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil) {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext: eaglContext];
        
        //Frame Buffer! Thing!
        glGenFramebuffers(1, &frameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
        //COlor Buffer!
        glGenRenderbuffers(1, &colorBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorBuffer);
        
        //Depth Buffer
        GLint backWidth;
        GLint backHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backHeight);
        
        glGenRenderbuffers(1, &depthBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backWidth, backHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            printf("FAILED :: TO create FBO");
            glDeleteRenderbuffers(1, &depthBuffer);
            glDeleteRenderbuffers(1, &colorBuffer);
            glDeleteFramebuffers(1, &frameBuffer);
            
            return(nil);
        }
        else {
            printf("FBO Created \n");
        }
        
        printf("GLES RENDERER : %s\n GLES VERSION : %s\n GLSL Version : %s \n",
               glGetString(GL_RENDERER),
               glGetString(GL_VERSION),
               glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        isAnimating = NO;
        animationFrameInterval = 60;
        
        [self initialize];
        
        //Gesture SetUP
        [self addGestures];
        
    }
    else {
        printf("Failed To Acquire the Instance \n");
    }
    
    return(self);
}


+(Class) layerClass
{
    return ([CAEAGLLayer class]);
}



//imp

-(void) addGestures
{
    
    UITapGestureRecognizer *singleTapGestureRecognizer =
    [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector (onSingleTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    // this will allow to differentiate
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    // swipe
    UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    // long press
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    
    
}


-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    //TO Start Our Code
}

-(void) onSingleTap:(UITapGestureRecognizer *) event
{
    printf("Single Tap..");
    Elbow =(int) (Elbow - 3.0f) % 360;
    

}

-(void) onDoubleTap:(UITapGestureRecognizer *) event
{
    
    printf("\n in Double Tap ..");
    Elbow =(int) (Elbow + 3.0f) % 360;
    
     
}

-(void) onSwipe:(UISwipeGestureRecognizer *) event
{
    [self release];
    exit(0);
}

-(void) onLongPress:(UILongPressGestureRecognizer *) event
{
    Shoulder = (int)(Shoulder + 3.0f) % 360;

}


//Imp
-(void) initialize
{
    GLint iShaderCompileStatus = 0;
    GLint iShaderInfoLogLen = 0;
    GLchar* szInfoLog = NULL;
    
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // Vertex Shader
    const GLchar *vertexShaderSourceCode =
    "#version 300 es" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec2 vTexCoord;" \
    "out vec2 out_texCoord;" \
    "uniform mat4 u_mvp_matrix;" \
    "void main(void)" \
    "{" \
    "gl_Position = u_mvp_matrix * vPosition;" \
    "out_texCoord = vTexCoord;" \
    "}";

    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    
    //Compile
    glCompileShader(gVertexShaderObject);
    
    //Error Checking.
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE) {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        
        if(iShaderInfoLogLen > 0) {
            szInfoLog = (GLchar*)malloc(iShaderInfoLogLen);
            
            if(szInfoLog != NULL) {
                GLsizei written;
                
                glGetShaderInfoLog(gVertexShaderObject,
                                   iShaderInfoLogLen,
                                   &written,
                                   szInfoLog);
                
                printf("Vertex Shader Failed:\n %s\n", szInfoLog);
                
                free(szInfoLog);
                [self release];
            }
        }
    }
    else {
        printf("Vertex Shader Compiled..\n");
    }
    
    //Fragment Shader
    iShaderCompileStatus = 0;
    iShaderInfoLogLen = 0;
    szInfoLog = NULL;
    
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar *fragmentShaderSourceCode =
    "#version 300 es" \
    "\n" \
    "precision highp float;"
    "in vec2 out_texCoord;" \
    "uniform sampler2D u_sampler;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "vec3 temp = vec3(texture(u_sampler, out_texCoord));"
    "FragColor = vec4(temp, 1.0f);" \
    "}";
    
    glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    
    glCompileShader(gFragmentShaderObject);
    
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        
        if(iShaderInfoLogLen > 0) {
            szInfoLog = (GLchar*)malloc(iShaderInfoLogLen);
            if(szInfoLog != NULL) {
                GLint written;
                
                glGetShaderInfoLog(gFragmentShaderObject,
                                   iShaderInfoLogLen,
                                   &written,
                                   szInfoLog);
                
                printf("Fragment Shader Failed:\n %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    else {
        printf("Fragement Shader Compiled..\n");
    }
    
//3
    GLint iProgLinkStatus = 0;
    GLint iProgLogLen = 0;
    GLchar* szProgLog = NULL;

    gProgramShaderObject = glCreateProgram();
    
    //Attach VS to Shader Prog
    glAttachShader(gProgramShaderObject, gVertexShaderObject);
    
    //Attach FS to Shader Prog
    glAttachShader(gProgramShaderObject, gFragmentShaderObject);
    
    //Prelink
    glBindAttribLocation(gProgramShaderObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gProgramShaderObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
    
    
    //Link
    glLinkProgram(gProgramShaderObject);
    
    glGetProgramiv(gProgramShaderObject, GL_LINK_STATUS, &iProgLinkStatus);
    
    if(iProgLinkStatus == GL_FALSE) {
        glGetProgramiv(gProgramShaderObject, GL_INFO_LOG_LENGTH, &iProgLogLen);
        
        if(iProgLogLen > 0) {
            szProgLog = (GLchar*)malloc(iProgLogLen);
            
            if(szProgLog != NULL) {
                GLint written;
                
                glGetProgramInfoLog(gProgramShaderObject, iProgLogLen, &written, szProgLog);
                
                printf("Program Link Failed :\n %s\n",szProgLog);
                
                [self release];
            }
        }
    }
    else {
        printf("Program Linked Successfully..\n");
    }
    
    //POST LINKING
    mvpUniform = glGetUniformLocation(gProgramShaderObject, "u_mvp_matrix");
    
    samplerUniform = glGetUniformLocation(gProgramShaderObject, "u_sampler");
    
    //Texture_Pyramid = [self loadTextureFromBMPFile:"Stone.bmp"];
    
    //Texture_Cube = [self loadTextureFromBMPFile:"Vijay_Kundali.bmp"];
    Texture_Pyramid = [self loadTextureFromBMPFile:@"Stone" :@"bmp"];

    
    
    
    //Shapes
   
    makeSphere(2.0, 30,30);
    
    // Cube
    
    
    
    //Tex
    const GLfloat cubeTexCoord[] =
                {
                     0.0f, 1.0f,
                     0.0f, 0.0f,
                     1.0f, 0.0f,
                     1.0f, 1.0f,
                     1.0f, 1.0f,
                     0.0f, 1.0f,
                     0.0f, 0.0f,
                     1.0f, 0.0f,
                     0.0f, 0.0f,
                     1.0f, 0.0f,
                     1.0f, 1.0f,
                     0.0f, 1.0f,
                     1.0f, 0.0f,
                     1.0f, 1.0f,
                     0.0f, 1.0f,
                     0.0f, 0.0f,
                     1.0f, 0.0f,
                     1.0f, 1.0f,
                     0.0f, 1.0f,
                     0.0f, 0.0f,
                     0.0f, 0.0f,
                     1.0f, 0.0f,
                     1.0f, 1.0f,
                     0.0f, 1.0f
                };
  
        glGenBuffers(1, &gVbo_sphere_texture);
    
        glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_texture);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexCoord), cubeTexCoord, GL_STATIC_DRAW);
    
        glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    
        glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
    
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
    
    //depth
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    
    glClearColor(0.0f, 0.0f, 0.1f, 1.0f);
    
    perspectiveProjectionMatrix = mat4::identity();
}


-(void) drawRect:(CGRect)rect {
    
}


-(void) layoutSubviews
{
    
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable: (CAEAGLLayer *)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    //Depth BUffer Regenration!
    glGenRenderbuffers(1, &depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        printf("Failed to Update FBO");
        glCheckFramebufferStatus(GL_FRAMEBUFFER);
    }
    else {
        printf("FBO Updated Successfully \n");
    }
    
    
    if(height == 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    GLfloat Width = (GLfloat)width;
    GLfloat Height = (GLfloat)height;
    
    perspectiveProjectionMatrix = perspective(45.0f, Width/Height, 0.1f, 100.0f);
    
    [self drawView: nil];
}


//VIMP

-(GLuint)loadTextureFromBMPFile:(NSString *)texFileName :(NSString *)extension
{
    NSString *textureFileNameWithPath = [[NSBundle mainBundle] pathForResource:texFileName ofType:extension];
    
    UIImage *bmpImage = [[UIImage alloc] initWithContentsOfFile:textureFileNameWithPath];
    
    if(!bmpImage)
    {
        NSLog(@"Can't find %@", textureFileNameWithPath);
        return(0);
    }
    
    CGImageRef cgImage = bmpImage.CGImage; //try
    
    int bmpWidth = (int)CGImageGetWidth(cgImage);
    int bmpHeight = (int)CGImageGetHeight(cgImage);
    
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    
    void* pixels = (void *)CFDataGetBytePtr(imageData);
    
    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 bmpWidth,
                 bmpHeight,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels); // imp
    
    glGenerateMipmap(GL_TEXTURE_2D); // new addition
    
    CFRelease(imageData);

    return(bmpTexture);

}



//Display
-(void) drawView: (id) sender
{
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    
   
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );
    
    glUseProgram(gProgramShaderObject);
    
        //1
    mat4 Model_Matrix;
    mat4 ModelView_Matrix;
    mat4 ModelView_Projection_Matrix;
    mat4 Translation_Matrix;
    mat4 Rotation_Matrix;

    //Arm
    //2 : I[]
    Model_Matrix = mat4::identity();
    ModelView_Matrix = mat4::identity();
    ModelView_Projection_Matrix = mat4::identity();
    Rotation_Matrix = mat4::identity();
    Translation_Matrix = mat4::identity();

    //Transforms : RTS

    ModelView_Matrix = translate(0.0f, 0.0f, -16.5f);

    ModelView_Matrix *= rotate((GLfloat)Shoulder, 0.0f, 0.0f, 1.0f);

    ModelView_Matrix *= translate(1.0f, 0.0f, 0.0f);

    Model_Matrix = ModelView_Matrix; //Very Imp call to save Old Matrix

    ModelView_Matrix *= scale(2.0f, 0.5f, 1.0f); //Longer Arm
    
    
    ModelView_Projection_Matrix = perspectiveProjectionMatrix * ModelView_Matrix;
    
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

    
    
    //7 . ABU
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, Texture_Pyramid);
    glUniform1i(samplerUniform, 0);

    draw();

    
//2nd : Elbow
    
    ModelView_Matrix = Model_Matrix; // Before Scale

    ModelView_Matrix *= translate(2.5f, 0.0f, 0.0f);

    ModelView_Matrix *= rotate((GLfloat)Elbow, 0.0f, 0.0f, 1.0f); // Rotate around Hand

    ModelView_Matrix *= translate(1.6f, 0.0f, 0.0f); //elong ARM

    ModelView_Matrix *= scale(2.0f, 0.5f, 1.0f); //After Scale : VIMP
    
    ModelView_Projection_Matrix = perspectiveProjectionMatrix * ModelView_Matrix;

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

    // *** bind vao ***

    //7 . ABU
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, Texture_Pyramid);
    glUniform1i(samplerUniform, 0);

    draw();


    //Common
    glUseProgram(0);


    glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void) startAnimation //update
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond: animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}


-(void) stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}


-(BOOL) acceptsFirstResponder
{
    return(YES);
}


-(void) dealloc
{
    
    //Uninitialize
    
    
    //Safe Release
    if(gProgramShaderObject) {
        GLsizei iShaderCnt = 0;
        GLsizei iShaderNo = 0;
        
        glUseProgram(gProgramShaderObject);
        
        glGetProgramiv(gProgramShaderObject,
                       GL_ATTACHED_SHADERS,
                       &iShaderCnt);
        
        GLuint *pShaders = (GLuint*)malloc(iShaderCnt * sizeof(GLuint));
        
        if(pShaders) {
            glGetAttachedShaders(gProgramShaderObject,
                                 iShaderCnt,
                                 &iShaderCnt,
                                 pShaders);
            
            for(iShaderNo = 0; iShaderNo < iShaderCnt; iShaderNo++) {
                
                glDetachShader(gProgramShaderObject, pShaders[iShaderNo]);
                
                pShaders[iShaderNo] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gProgramShaderObject);
        gProgramShaderObject = 0;
        glUseProgram(0);
    }
    
    if(depthBuffer) {
        glDeleteRenderbuffers(1, &depthBuffer);
        depthBuffer = 0;
    }
    if(colorBuffer){
        glDeleteRenderbuffers(1, &colorBuffer);
        colorBuffer = 0;
    }
    
    if(frameBuffer) {
        glDeleteFramebuffers(1, &frameBuffer);
        frameBuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext) {
        [EAGLContext setCurrentContext:nil];
    }
    
    if(eaglContext) {
        eaglContext = nil;
    }
    
    [super dealloc];
}



@end



