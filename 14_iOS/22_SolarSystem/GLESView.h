//
//  GLESView.h
//
//  Copyright © 2020 AstroMediComp. All rights reserved.


#import <UIKit/UIKit.h>

@interface GLESView : UIView <UIGestureRecognizerDelegate>

-(void) startAnimation;
-(void) stopAnimation;

@end
