//
//  GLESView.m
//
//  Copyright © 2020 AstroMediComp. All rights reserved.

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "GLESView.h"
#import "vmath.h"
#import "YSR_Sphere.h"

using namespace vmath;

static int bisLPressed;
int winWidth,winHeight;

@implementation GLESView
{
    
    EAGLContext *eaglContext;
    
    GLuint frameBuffer;
    GLuint colorBuffer;
    GLuint depthBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    // Shader Variables
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gProgramShaderObject;

   //12 Uniforms
    GLfloat AngleTri;
    
    //Blue
    GLuint LD_Uniform, LA_Uniform, LS_Uniform;
    GLuint KD_Uniform, KA_Uniform, KS_Uniform;
    
    GLuint u_Material_Shininess;
    GLuint Light_Position_Uniform;
    GLuint LisPressed_Uniform;
    
    int bLighting,flag;
    
    GLuint mvpUniform, Projection_Uniform, View_Uniform;
    mat4 perspectiveProjectionMatrix;

    
}


-(id) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    
    if(self) {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil) {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext: eaglContext];
        
        //Frame Buffer! Thing!
        glGenFramebuffers(1, &frameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
        //COlor Buffer!
        glGenRenderbuffers(1, &colorBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorBuffer);
        
        //Depth Buffer
        GLint backWidth;
        GLint backHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backHeight);
        
        glGenRenderbuffers(1, &depthBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backWidth, backHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            printf("FAILED :: TO create FBO");
            glDeleteRenderbuffers(1, &depthBuffer);
            glDeleteRenderbuffers(1, &colorBuffer);
            glDeleteFramebuffers(1, &frameBuffer);
            
            return(nil);
        }
        else {
            printf("FBO Created \n");
        }
        
        printf("GLES RENDERER : %s\n GLES VERSION : %s\n GLSL Version : %s \n",
               glGetString(GL_RENDERER),
               glGetString(GL_VERSION),
               glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        isAnimating = NO;
        animationFrameInterval = 60;
        
        [self initialize];
        
        //Gesture SetUP
        [self addGestures];
        
    }
    else {
        printf("Failed To Acquire the Instance \n");
    }
    
    return(self);
}

+(Class) layerClass {
    return ([CAEAGLLayer class]);
}


-(void) initialize
{
    GLint iShaderCompileStatus = 0;
    GLint iShaderInfoLogLen = 0;
    GLchar* szInfoLog = NULL;
    
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // Vertex Shader
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;"
        "precision lowp int;"
        "in vec4 vPosition;"                        \
        "in vec3 vNormal;"                            \
        "out vec3 T_Norm;"                            \
        "out vec3 Light_Direction;"                    \
        "out vec3 Viewer_Vector;"                    \
        "\n"                                        \
        "uniform mat4 u_model_matrix;"                \
        "uniform mat4 u_view_matrix;"                \
        "uniform mat4 u_projection_matrix;"            \
        "uniform int u_LKeyPressed;"                \
        "uniform vec4 u_Light_Position;"            \
        "\n"                                        \
        "void main(void)"                            \
        "{"                                            \
        "if(u_LKeyPressed == 1)"                    \
        "{"                                            \
        "vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"    \
        "T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"            \
        "Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"    \
        "Viewer_Vector = vec3(-Eye_Coordinates.xyz);"    \
        "}"        \
        "gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"    \
        "}" ;

  

    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    
    //Compile
    glCompileShader(gVertexShaderObject);
    
    //Error Checking.
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE) {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        
        if(iShaderInfoLogLen > 0) {
            szInfoLog = (GLchar*)malloc(iShaderInfoLogLen);
            
            if(szInfoLog != NULL) {
                GLsizei written;
                
                glGetShaderInfoLog(gVertexShaderObject,
                                   iShaderInfoLogLen,
                                   &written,
                                   szInfoLog);
                
                printf("Vertex Shader Failed:\n %s\n", szInfoLog);
                
                free(szInfoLog);
                [self release];
            }
        }
    }
    else {
        printf("Vertex Shader Compiled..\n");
    }
    
    //Fragment Shader
    iShaderCompileStatus = 0;
    iShaderInfoLogLen = 0;
    szInfoLog = NULL;
    
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;"
        "precision lowp int;"
        "in vec3 T_Norm;"            \
        "in vec3 Light_Direction;"    \
        "in vec3 Viewer_Vector;"    \
        "out vec4 FragColor;"        \
        "\n"                        \
        "uniform vec3 u_LA;"                \
        "uniform vec3 u_LD;"                \
        "uniform vec3 u_LS;"                \
        "uniform vec3 u_KA;"                \
        "uniform vec3 u_KD;"                \
        "uniform vec3 u_KS;"                \
        "uniform float u_Shininess;"        \
        "uniform int u_LKeyPressed;"        \
        "uniform vec4 u_Light_Position;"    \
        "\n"                                \
        "void main(void)"                    \
        "{"                        \
        "if(u_LKeyPressed == 1)"\
        "{"                        \
        "vec3 Normalized_View_Vector = normalize(Viewer_Vector);"    \
        "vec3 Normalized_Light_Direction = normalize(Light_Direction);"\
        "\n"\
        "vec3 Normalized_TNorm = normalize(T_Norm);"\
        "float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0);"    \
        "vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm);"\
        "\n" \
        "vec3 Ambient = vec3(u_LA * u_KA);"    \
        "vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);"    \
        "vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess));" \
        "\n" \
        "vec3 Phong_ADS_Light = Ambient + Diffuse + Specular;"    \
        "FragColor = vec4(Phong_ADS_Light,1.0);" \
        "}"                            \
        "else"                        \
        "{"                            \
        "FragColor = vec4(1.0,1.0,1.0,1.0);" \
        "}"                            \
        "}" ;


    
    
    glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    
    glCompileShader(gFragmentShaderObject);
    
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        
        if(iShaderInfoLogLen > 0) {
            szInfoLog = (GLchar*)malloc(iShaderInfoLogLen);
            if(szInfoLog != NULL) {
                GLint written;
                
                glGetShaderInfoLog(gFragmentShaderObject,
                                   iShaderInfoLogLen,
                                   &written,
                                   szInfoLog);
                
                printf("Fragment Shader Failed:\n %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    else {
        printf("Fragement Shader Compiled..\n");
    }
    
//3
    GLint iProgLinkStatus = 0;
    GLint iProgLogLen = 0;
    GLchar* szProgLog = NULL;

    gProgramShaderObject = glCreateProgram();
    
    //Attach VS to Shader Prog
    glAttachShader(gProgramShaderObject, gVertexShaderObject);
    
    //Attach FS to Shader Prog
    glAttachShader(gProgramShaderObject, gFragmentShaderObject);
    
    //Prelink
    glBindAttribLocation(gProgramShaderObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gProgramShaderObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
    
    //Link
    glLinkProgram(gProgramShaderObject);
    
    glGetProgramiv(gProgramShaderObject, GL_LINK_STATUS, &iProgLinkStatus);
    
    if(iProgLinkStatus == GL_FALSE) {
        glGetProgramiv(gProgramShaderObject, GL_INFO_LOG_LENGTH, &iProgLogLen);
        
        if(iProgLogLen > 0) {
            szProgLog = (GLchar*)malloc(iProgLogLen);
            
            if(szProgLog != NULL) {
                GLint written;
                
                glGetProgramInfoLog(gProgramShaderObject, iProgLogLen, &written, szProgLog);
                
                printf("Program Link Failed :\n %s\n",szProgLog);
                
                [self release];
            }
        }
    }
    else {
        printf("Program Linked Successfully..\n");
    }
    
    //POST LINKING
    mvpUniform = glGetUniformLocation(gProgramShaderObject,"u_model_matrix");

    View_Uniform = glGetUniformLocation(gProgramShaderObject, "u_view_matrix");
       
    Projection_Uniform = glGetUniformLocation(gProgramShaderObject, "u_projection_matrix");
    
    KD_Uniform = glGetUniformLocation(gProgramShaderObject , "u_KD");
    
    Light_Position_Uniform = glGetUniformLocation(gProgramShaderObject , "u_Light_Position");
    
    LD_Uniform = glGetUniformLocation(gProgramShaderObject , "u_LD");
    
    LisPressed_Uniform = glGetUniformLocation( gProgramShaderObject , "u_LKeyPressed");
    
    LA_Uniform = glGetUniformLocation(gProgramShaderObject , "u_LA");
    KA_Uniform = glGetUniformLocation(gProgramShaderObject, "u_KA");

    LS_Uniform = glGetUniformLocation(gProgramShaderObject, "u_LS");
    KS_Uniform = glGetUniformLocation(gProgramShaderObject, "u_KS");

    u_Material_Shininess = glGetUniformLocation(gProgramShaderObject, "u_Shininess");



    
    //sphere
    makeSphere(2.0,30,30);
    
    //depth
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    AngleTri = 0.0f;
    flag = 0;
    bisLPressed = 0;

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    perspectiveProjectionMatrix = mat4::identity();
}


-(void) drawRect:(CGRect)rect {
    
}

//imp
-(void) addGestures
{
    
    UITapGestureRecognizer *singleTapGestureRecognizer =
    [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector (onSingleTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    // this will allow to differentiate
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    // swipe
    UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    // long press
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    
    
}



-(void) layoutSubviews
{
    
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable: (CAEAGLLayer *)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    //Depth BUffer Regenration!
    glGenRenderbuffers(1, &depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        printf("Failed to Update FBO");
        glCheckFramebufferStatus(GL_FRAMEBUFFER);
    }
    else {
        printf("FBO Updated Successfully \n");
    }
    
    
    if(height == 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    GLfloat Width = (GLfloat)width;
    GLfloat Height = (GLfloat)height;
    
    winWidth = width;
    winHeight = height;
    
    perspectiveProjectionMatrix = perspective(45.0f, Width/Height, 0.1f, 100.0f);
    
    [self drawView: nil];
}


//VIMP

-(GLuint)loadTextureFromBMPFile:(NSString *)texFileName :(NSString *)extension
{
    NSString *textureFileNameWithPath = [[NSBundle mainBundle] pathForResource:texFileName ofType:extension];
    
    UIImage *bmpImage = [[UIImage alloc] initWithContentsOfFile:textureFileNameWithPath];
    
    if(!bmpImage)
    {
        NSLog(@"Can't find %@", textureFileNameWithPath);
        return(0);
    }
    
    CGImageRef cgImage = bmpImage.CGImage; //try
    
    int bmpWidth = (int)CGImageGetWidth(cgImage);
    int bmpHeight = (int)CGImageGetHeight(cgImage);
    
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    
    void* pixels = (void *)CFDataGetBytePtr(imageData);
    
    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 bmpWidth,
                 bmpHeight,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels); // imp
    
    glGenerateMipmap(GL_TEXTURE_2D); // new addition
    
    CFRelease(imageData);

    return(bmpTexture);

}

-(BOOL) acceptsFirstResponder
{
    return(YES);
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    //TO Start Our Code
}

-(void) onSingleTap:(UITapGestureRecognizer *) event
{
    printf("\n Single Tap.. \n");

   
}


-(void) onDoubleTap:(UITapGestureRecognizer *) event
{
    printf("\n in Double Tap");
    
    if (bisLPressed == 0)
    {
        bisLPressed = 1;
    }
    else if(flag > 4)
    {
        bisLPressed = 0;
        flag = 0;
    }
    
    if(flag == 4)
        flag = 1;
    
    flag++;
}

-(void) onSwipe:(UISwipeGestureRecognizer *) event {
    [self release];
    exit(0);
}

-(void) onLongPress:(UILongPressGestureRecognizer *) event
{
    printf("\n Long Press");
    
    
    
}



//Display
-(void) drawView: (id) sender
{
        [EAGLContext setCurrentContext:eaglContext];
        
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
        
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );
        bLighting = true;
        glUseProgram(gProgramShaderObject);
        
    
       mat4 Model_Matrix;
       mat4 View_Matrix;
       mat4 Projection_Matrix;
       mat4 Translation_Matrix;
       mat4 Rotation_Matrix;
       mat4 ModelView_Projection_Matrix;
       
       //2 : I[]
       Model_Matrix = mat4::identity();
       ModelView_Projection_Matrix = mat4::identity();
       View_Matrix = mat4::identity();
       Rotation_Matrix = mat4::identity();
       Translation_Matrix = mat4::identity();

       //4 : Do Mat mul
       Translation_Matrix = translate(0.0f, 0.0f, -6.5f);

       Model_Matrix = Translation_Matrix * Rotation_Matrix;


       // 5    : send it to shader

       glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, Model_Matrix);
       
       glUniformMatrix4fv(Projection_Uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
       
       glUniformMatrix4fv(View_Uniform, 1, GL_FALSE, View_Matrix);
       
       if (bisLPressed == 1)
             {
                 glUniform1i(LisPressed_Uniform, 1);
         
                 glUniform3f(LD_Uniform, 1.0f, 1.0f, 1.0f);
                 
                 glUniform3f(LS_Uniform, 1.0f, 1.0f, 1.0f);
                 
                 glUniform3f(LA_Uniform, 0.0f, 0.0f, 0.0f);
                                  
                 //imp : out to in + Light
                 if(flag == 1)
                 glUniform4f(Light_Position_Uniform,0.0f,sinf(AngleTri)*8 ,cosf(AngleTri)*8 , 1.0f);
                 
                 if(flag == 3)
                 glUniform4f(Light_Position_Uniform,sinf(AngleTri)*8 ,cosf(AngleTri)*8 , 0.0f, 1.0f);
         
                 if(flag == 2)
                 glUniform4f(Light_Position_Uniform,sinf(AngleTri)*8,  0.0f, cosf(AngleTri)*8, 1.0f);
                 
         
             }

       else
       {
           glUniform1i(LisPressed_Uniform, 0);
       }

    //very imp
    
        
    AngleTri += 0.2f;
      
          if (AngleTri >= 360.0f)
              AngleTri -= 359.0f;
      
    //24 Sph

        GLfloat  Material_Ambient[72]; //
        GLfloat  Material_Diffuse[72]; //
        GLfloat  Material_Specular[72]; //
        GLfloat  Material_Shininess[24]; //
        

        //24 Spheres
    // 1 : Emerald
        Material_Ambient[0] = 0.0215f;
        Material_Ambient[1] = 0.1745f;
        Material_Ambient[2] = 0.0215f;

        Material_Diffuse[0] = 0.0215f;
        Material_Diffuse[1] = 0.1745f;
        Material_Diffuse[2] = 0.0215f;
        //
        
        Material_Specular[0] = 0.633f;
        Material_Specular[1] = 0.727811f;
        Material_Specular[2] = 0.633f;

        Material_Shininess[0] = 0.6f * 128.0f;


    // 2 = Jade
        Material_Ambient[3] = 0.135f;
        Material_Ambient[4] = 0.2225f;
        Material_Ambient[5] = 0.1575f;
        //
        ///

        Material_Diffuse[3] = 0.54f;
        Material_Diffuse[4] = 0.89f;
        Material_Diffuse[5] = 0.63f;
        //
        //
        
        Material_Specular[3] = 0.316228f;
        Material_Specular[4] = 0.316228f;
        Material_Specular[5] = 0.316228f;
        //

        Material_Shininess[1] = 0.1f * 128.0f;


    // 3 = obisidian
        Material_Ambient[6] = 0.05375f;
        Material_Ambient[7] = 0.05f;
        Material_Ambient[8] = 0.06625f;
        //

        Material_Diffuse[6] = 0.18275f;
        Material_Diffuse[7] = 0.17f;
        Material_Diffuse[8] = 0.22525f;
        //
        //
        
        Material_Specular[6] = 0.332741f;
        Material_Specular[7] = 0.328634f;
        Material_Specular[8] = 0.346435f;
        //
        //

        Material_Shininess[2] = 0.3f * 128.0f;
        //

    // 4= Pearl
        Material_Ambient[9] = 0.25f;
        Material_Ambient[10] = 0.20725f;
        Material_Ambient[11] = 0.20725f;
        //
        ///

        Material_Diffuse[9] = 1.0f;
        Material_Diffuse[10] = 0.829f;
        Material_Diffuse[11] = 0.829f;
        //
        //
        
        Material_Specular[9] = 0.296648f;
        Material_Specular[10] = 0.296648f;
        Material_Specular[11] = 0.296648f;
        //
        //

        Material_Shininess[3] = 0.088f * 128.0f;
        //

    // 5 = Ruby
        Material_Ambient[12] = 0.1745f;
        Material_Ambient[13] = 0.01175f;
        Material_Ambient[14] = 0.01175f;
        //
        ///

        Material_Diffuse[12] = 0.61424f;
        Material_Diffuse[13] = 0.04136f;
        Material_Diffuse[14] = 0.04136f;
        //
        
        Material_Specular[12] = 0.727811f;
        Material_Specular[13] = 0.626959f;
        Material_Specular[14] = 0.626959f;
        //

        Material_Shininess[4] = 0.6f * 128.0f;
        //

    // 6 = Turquoise
        Material_Ambient[15] = 0.1f;
        Material_Ambient[16] = 0.18275f;
        Material_Ambient[17] = 0.1745f;
        //
        ///

        Material_Diffuse[15] = 0.396f;
        Material_Diffuse[16] = 0.74151f;
        Material_Diffuse[17] = 0.69102f;
        //
        //
        
        Material_Specular[15] = 0.297524f;
        Material_Specular[16] = 0.30829f;
        Material_Specular[17] = 0.306678f;
        //
        //

        Material_Shininess[5] = 0.1f * 128.0f;
        //
      

    // Metals   ---------------------

    // 1 : Brass
        Material_Ambient[18] = 0.349412f;
        Material_Ambient[19] = 0.30829f;
        Material_Ambient[20] = 0.306678f;
        ///

        Material_Diffuse[18] = 0.780392f;
        Material_Diffuse[19] = 0.223529f;
        Material_Diffuse[20] = 0.027451f;
        //
        //
        
        Material_Specular[18] = 0.992157f;
        Material_Specular[19] = 0.941176f;
        Material_Specular[20] = 0.807843f;
        //
        //

        Material_Shininess[6] = 0.21794872f * 128.0f;
        //

    // 2 = Bronze
        Material_Ambient[21] = 0.2125f;
        Material_Ambient[22] = 0.1275f;
        Material_Ambient[23] = 0.054f;
        //
        ///

        Material_Diffuse[21] = 0.714f;
        Material_Diffuse[22] = 0.4284f;
        Material_Diffuse[23] = 0.18144f;
        //
        //
        
        Material_Specular[21] = 0.393548f;
        Material_Specular[22] = 0.271906f;
        Material_Specular[23] = 0.166721f;
        //
        //

        Material_Shininess[7] = 0.2f * 128.0f;
        //

    // 3 = Chrome
        Material_Ambient[24] = 0.25f;
        Material_Ambient[25] = 0.25f;
        Material_Ambient[26] = 0.25f;
        //
        ///

        Material_Diffuse[24] = 0.4f;
        Material_Diffuse[25] = 0.4f;
        Material_Diffuse[26] = 0.4f;
        //
        //
        
        Material_Specular[24] = 0.774597f;
        Material_Specular[25] = 0.774597f;
        Material_Specular[26] = 0.774597f;
        //
        //

        Material_Shininess[8] = 0.6f * 128.0f;
        //

        
    // 4= copper
        Material_Ambient[27] = 0.19125f;
        Material_Ambient[28] = 0.0735f;
        Material_Ambient[29] = 0.02025f;
        //
        ///

        Material_Diffuse[27] = 0.7038f;
        Material_Diffuse[28] = 0.27048f;
        Material_Diffuse[29] = 0.0828f;
        //
        //
        
        Material_Specular[27] = 0.25677f;
        Material_Specular[28] = 0.137622f;
        Material_Specular[29] = 0.086014f;
        //
        //

        Material_Shininess[9] = 0.1f * 128.0f;


    // 5 = Gold
        Material_Ambient[30] = 0.24725f;
        Material_Ambient[31] = 0.1995f;
        Material_Ambient[32] = 0.0745f;
        //
        ///

        Material_Diffuse[30] = 0.7517f;
        Material_Diffuse[31] = 0.6065f;
        Material_Diffuse[32] = 0.2265f;
        //
        
        Material_Specular[30] = 0.6283f;
        Material_Specular[31] = 0.55580f;
        Material_Specular[32] = 0.36606f;
        //

        Material_Shininess[10] = 0.4f * 128.0f;
        //

    // 6 = Silver
        Material_Ambient[33] = 0.19225f;
        Material_Ambient[34] = 0.19225f;
        Material_Ambient[35] = 0.19225f;
        //

        Material_Diffuse[33] = 0.5075f;
        Material_Diffuse[34] = 0.5075f;
        Material_Diffuse[35] = 0.5075f;
        //
        //
        
        Material_Specular[33] = 0.50828f;
        Material_Specular[34] = 0.50828f;
        Material_Specular[35] = 0.50828f;
        //
        //

        Material_Shininess[11] = 0.4f * 128.0f;
        //


    // Plastic ---------------------
    // 1 - Black
        Material_Ambient[36] = 0.0f;
        Material_Ambient[37] = 0.0f;
        Material_Ambient[38] = 0.0f;
        //
        ///

        Material_Diffuse[36] = 0.01f;
        Material_Diffuse[37] = 0.01f;
        Material_Diffuse[38] = 0.01f;
        //
        //
        
        Material_Specular[36] = 0.5f;
        Material_Specular[37] = 0.5f;
        Material_Specular[38] = 0.5f;
        //
        //

        Material_Shininess[12] = 0.25f * 128.0f;
        //


    // 2 - Cyan
        Material_Ambient[39] = 0.0f;
        Material_Ambient[40] = 0.1f;
        Material_Ambient[41] = 0.06f;
        //
        ///

        Material_Diffuse[39] = 0.0f;
        Material_Diffuse[40] = 0.5098039f;
        Material_Diffuse[41] = 0.5098039f;
        //
        //
        
        Material_Specular[39] = 0.0f;
        Material_Specular[40] = 0.501960f;
        Material_Specular[41] = 0.501960f;
        //

        Material_Shininess[13] = 0.25f * 128.0f;
        //

    //3 - Green
        Material_Ambient[42] = 0.0f;
        Material_Ambient[43] = 0.1f;
        Material_Ambient[44] = 0.06f;
        //
        ///

        Material_Diffuse[42] = 0.1f;
        Material_Diffuse[43] = 0.35f;
        Material_Diffuse[44] = 0.1f;
        //
        //
        
        Material_Specular[42] = 0.45f;
        Material_Specular[43] = 0.55f;
        Material_Specular[44] = 0.45f;
        //
        //

        Material_Shininess[14] = 0.25f * 128.0f;
        //


    // Red

        Material_Ambient[45] = 0.0f;
        Material_Ambient[46] = 0.0f;
        Material_Ambient[47] = 0.0f;
        //
        ///

        Material_Diffuse[45] = 0.5f;
        Material_Diffuse[46] = 0.0f;
        Material_Diffuse[47] = 0.0f;
        //
        //
        
        Material_Specular[45] = 0.7f;
        Material_Specular[46] = 0.6f;
        Material_Specular[47] = 0.6f;
        //
        //

        Material_Shininess[15] = 0.25f * 128.0f;


    // White
        Material_Ambient[48] = 0.0f;
        Material_Ambient[49] = 0.0f;
        Material_Ambient[50] = 0.0f;
        //
        ///

        Material_Diffuse[48] = 0.55f;
        Material_Diffuse[49] = 0.55f;
        Material_Diffuse[50] = 0.55f;
        //
        //
        
        Material_Specular[48] = 0.7f;
        Material_Specular[49] = 0.7f;
        Material_Specular[50] = 0.7f;
        //
        //

        Material_Shininess[16] = 0.25f * 128.0f;
        //


    // Yellow
        Material_Ambient[51] = 0.0f;
        Material_Ambient[52] = 0.0f;
        Material_Ambient[53] = 0.0f;
        //
        ///

        Material_Diffuse[51] = 0.5f;
        Material_Diffuse[52] = 0.5f;
        Material_Diffuse[53] = 0.0f;
        
        Material_Specular[51] = 0.6f;
        Material_Specular[52] = 0.6f;
        Material_Specular[53] = 0.5f;

        Material_Shininess[17] = 0.25f * 128.0f;


    // -------- Rubber
    // 1 - Black
        Material_Ambient[54] = 0.02f;
        Material_Ambient[55] = 0.02f;
        Material_Ambient[56] = 0.02f;
        //
        ///

        Material_Diffuse[54] = 0.01f;
        Material_Diffuse[55] = 0.01f;
        Material_Diffuse[56] = 0.01f;
        //
        //
        
        Material_Specular[54] = 0.4f;
        Material_Specular[55] = 0.4f;
        Material_Specular[56] = 0.4f;
        //
        //

        Material_Shininess[18] = 0.07813f * 128.0f;
        //


    // 2 - Cyan
        Material_Ambient[57] = 0.0f;
        Material_Ambient[58] = 0.05f;
        Material_Ambient[59] = 0.05f;
        //
        ///

        Material_Diffuse[57] = 0.4f;
        Material_Diffuse[58] = 0.5098039f;
        Material_Diffuse[59] = 0.5098039f;
        //
        //
        
        Material_Specular[57] = 0.0f;
        Material_Specular[58] = 0.501960f;
        Material_Specular[59] = 0.501960f;
        //
        //

        Material_Shininess[19] = 0.07813f * 128.0f;
        //

        //
        
    //3 - Green
        Material_Ambient[60] = 0.0f;
        Material_Ambient[61] = 0.1f;
        Material_Ambient[62] = 0.06f;
        //
        ///

        Material_Diffuse[60] = 0.1f;
        Material_Diffuse[61] = 0.35f;
        Material_Diffuse[62] = 0.1f;
        //
        //
        
        Material_Specular[60] = 0.45f;
        Material_Specular[61] = 0.55f;
        Material_Specular[62] = 0.45f;
        //
        //

        Material_Shininess[20] = 0.07813f * 128.0f;
        //

    // 4- Red

        Material_Ambient[63] = 0.0f;
        Material_Ambient[64] = 0.0f;
        Material_Ambient[65] = 0.0f;
        //
        ///

        Material_Diffuse[63] = 0.5f;
        Material_Diffuse[64] = 0.0f;
        Material_Diffuse[65] = 0.0f;
        //
        //
        
        Material_Specular[63] = 0.7f;
        Material_Specular[64] = 0.6f;
        Material_Specular[65] = 0.6f;
        //
        //

        Material_Shininess[21] = 0.07813f * 128.0f;
        //

    // 5- White
        Material_Ambient[66] = 0.0f;
        Material_Ambient[67] = 0.0f;
        Material_Ambient[68] = 0.0f;
        //
        ///

        Material_Diffuse[66] = 0.55f;
        Material_Diffuse[67] = 0.55f;
        Material_Diffuse[68] = 0.55f;
        //
        //
        
        Material_Specular[66] = 0.7f;
        Material_Specular[67] = 0.7f;
        Material_Specular[68] = 0.7f;
        //
        //

        Material_Shininess[22] = 0.07813f * 128.0f;

    // 6- Yellow
        Material_Ambient[69] = 0.0f;
        Material_Ambient[70] = 0.0f;
        Material_Ambient[71] = 0.0f;

        Material_Diffuse[69] = 0.5f;
        Material_Diffuse[70] = 0.5f;
        Material_Diffuse[71] = 0.0f;
        
        Material_Specular[69] = 0.6f;
        Material_Specular[70] = 0.6f;
        Material_Specular[71] = 0.5f;

        Material_Shininess[23] = 0.07813f * 128.0f;


        // ---------- Set
        int width = (GLint)(winWidth / 6);
        int height = (GLint)(winHeight / 4);

        int i,j;

      // material : i*3+j
      for(i = 0; i <= 5; i++) //x
       {
        for(j = 0; j <= 3; j++) //y
        {
            //1
            glUniform1f(u_Material_Shininess,Material_Shininess[i+j]);
            
        glUniform3f(KS_Uniform,Material_Specular[3*i+j],Material_Specular[3*i+j+1],Material_Specular[3*i+j+2]);
        glUniform3f(KD_Uniform,Material_Diffuse[3*i+j],Material_Diffuse[3*i+j+1],Material_Diffuse[3*i+j+2]);
        glUniform3f(KA_Uniform,Material_Ambient[3*i+j],Material_Ambient[3*i+j+1],Material_Ambient[3*i+j+2]);

            //2
            glViewport(width*i, height*j, width, height);

            perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)(width) / (GLfloat)(height), 0.1f, 100.0f);
            
            //imp
            draw();

        }

     }

    
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}



-(void) startAnimation //update
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond: animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}


-(void) stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}


-(void) dealloc
{
    //Uninitialize

/*
    //Vao & vbo
    
    if(vbo_rectangle) {
        glDeleteBuffers(1, &vbo_rectangle);
        vbo_rectangle = 0;
    }
    if(vao_rectangle) {
        glDeleteVertexArrays(1, &vao_rectangle);
        vao_rectangle = 0;
    }
*/
    
    //Safe Release
    if(gProgramShaderObject) {
        GLsizei iShaderCnt = 0;
        GLsizei iShaderNo = 0;
        
        glUseProgram(gProgramShaderObject);
        
        glGetProgramiv(gProgramShaderObject,
                       GL_ATTACHED_SHADERS,
                       &iShaderCnt);
        
        GLuint *pShaders = (GLuint*)malloc(iShaderCnt * sizeof(GLuint));
        
        if(pShaders) {
            glGetAttachedShaders(gProgramShaderObject,
                                 iShaderCnt,
                                 &iShaderCnt,
                                 pShaders);
            
            for(iShaderNo = 0; iShaderNo < iShaderCnt; iShaderNo++) {
                
                glDetachShader(gProgramShaderObject, pShaders[iShaderNo]);
                
                pShaders[iShaderNo] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gProgramShaderObject);
        gProgramShaderObject = 0;
        glUseProgram(0);
    }
    
    if(depthBuffer) {
        glDeleteRenderbuffers(1, &depthBuffer);
        depthBuffer = 0;
    }
    if(colorBuffer){
        glDeleteRenderbuffers(1, &colorBuffer);
        colorBuffer = 0;
    }
    
    if(frameBuffer) {
        glDeleteFramebuffers(1, &frameBuffer);
        frameBuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext) {
        [EAGLContext setCurrentContext:nil];
    }
    
    if(eaglContext) {
        eaglContext = nil;
    }
    
    [super dealloc];
}



@end



