//
//  GLESView.m
//
//  Copyright © 2020 AstroMediComp. All rights reserved.

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "GLESView.h"
#import "vmath.h"
#import "YSR_Sphere.h"

using namespace vmath;

//global
static int bisLPressed,flag;

// Shader Variables
GLuint gVertex_Shader_Object;
GLuint gFragment_Shader_Object;

GLuint gShader_Program_Object_Vert;
GLuint gShader_Program_Object_Frag;


@implementation GLESView
{
    
    EAGLContext *eaglContext;
    
    GLuint frameBuffer;
    GLuint colorBuffer;
    GLuint depthBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint Model_Uniform;
    GLuint View_Uniform, View_Uniform_Frag;
    GLuint Projection_Uniform, Projection_Uniform_Frag;
    
    
   //12 Uniforms
    GLuint LD_Uniform, LA_Uniform, LS_Uniform;
    GLuint KD_Uniform, KA_Uniform, KS_Uniform;
    
    GLuint Material_Shininess;
    GLuint Light_Position_Uniform;
    
    //Fragment
    GLuint mUniform_Frag;
    GLuint LD_Uniform_Frag, LA_Uniform_Frag, LS_Uniform_Frag;
    GLuint KD_Uniform_Frag, KA_Uniform_Frag, KS_Uniform_Frag;
    
    GLuint Material_Shininess_Frag;
    GLuint Light_Position_Uniform_Frag;
    GLuint LisPressed_Uniform , LisPressed_Uniform_Frag;

    
    bool bLighting;

    mat4 perspectiveProjectionMatrix;

    
}


-(id) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    
    if(self) {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil) {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext: eaglContext];
        
        //Frame Buffer! Thing!
        glGenFramebuffers(1, &frameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
        //COlor Buffer!
        glGenRenderbuffers(1, &colorBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorBuffer);
        
        //Depth Buffer
        GLint backWidth;
        GLint backHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backHeight);
        
        glGenRenderbuffers(1, &depthBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backWidth, backHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            printf("FAILED :: TO create FBO");
            glDeleteRenderbuffers(1, &depthBuffer);
            glDeleteRenderbuffers(1, &colorBuffer);
            glDeleteFramebuffers(1, &frameBuffer);
            
            return(nil);
        }
        else {
            printf("FBO Created \n");
        }
        
        printf("GLES RENDERER : %s\n GLES VERSION : %s\n GLSL Version : %s \n",
               glGetString(GL_RENDERER),
               glGetString(GL_VERSION),
               glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        isAnimating = NO;
        animationFrameInterval = 60;
        
        [self initialize];
        
        //Gesture SetUP
        [self addGestures];
        
    }
    else {
        printf("Failed To Acquire the Instance \n");
    }
    
    return(self);
}

+(Class) layerClass {
    return ([CAEAGLLayer class]);
}


//new
- (void) PerVertex
{
    //code
    /*
    Add 6 Uniforms for Light
    */

    //For Shaders : create
    gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

    //Define VS code
    GLchar *VertexShaderCode =
        "#version 300 es"                            \
        "\n"                                        \
        "precision highp float;"
        "precision lowp int;"
        "in vec4 vPosition;"                        \
        "in vec3 vNormal;"                           \
        "out vec3 Phong_ADS_Light;"                  \
        "\n"                                        \
        "uniform mat4 u_model_matrix;"              \
        "uniform mat4 u_view_matrix;"               \
        "uniform mat4 u_projection_matrix;"         \
        "uniform vec3 u_LA;"                        \
        "uniform vec3 u_LD;"                        \
        "uniform vec3 u_LS;"                        \
        "uniform vec3 u_KA;"                        \
        "uniform vec3 u_KD;"                        \
        "uniform vec3 u_KS;"                        \
        "uniform float u_Shininess;"                \
        "uniform int u_LKeyPressed;"                \
        "uniform vec4 u_Light_Position;"            \
        "\n"                                        \
        "void main(void)"                            \
        "{"                                            \
        "if(u_LKeyPressed == 1)"                    \
        "{"                                            \
        "vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"        \
        "vec3 T_Norm = normalize(mat3( u_view_matrix * u_model_matrix)* vNormal);"    \
        "vec3 Light_Direction = normalize(vec3(u_Light_Position) - (Eye_Coordinates.xyz));"    \
        "float TN_Dot_LD = max(dot(Light_Direction,T_Norm), 0.0);"\
        "vec3 Reflecion_Vector = reflect(-Light_Direction, T_Norm);"\
        "vec3 Viewer_Vector = normalize(-Eye_Coordinates.xyz);"        \
        "vec3 Ambient = vec3(u_LA * u_KA);"                \
        "vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);" \
        "vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflecion_Vector, Viewer_Vector), 0.0), u_Shininess));" \
        "Phong_ADS_Light = Ambient + Diffuse + Specular;" \
        "}"        \
        "else"    \
        "{"        \
        "Phong_ADS_Light = vec3(1.0,1.0,1.0);"    \
        "}"        \
        "gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"    \
        "}";

    //Specify code to Obj
    glShaderSource(gVertex_Shader_Object,
        1,
        (const GLchar**)&VertexShaderCode,
        NULL);

    
    // Error checking
    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    char *szInfoLog = NULL;
    GLint iProgrmLinkStatus = 0;


    //Compile the shader
    glCompileShader(gVertex_Shader_Object);

    //Error Code for VS
    glGetShaderiv(gVertex_Shader_Object,
        GL_COMPILE_STATUS,
        &iShaderCompileStatus);


    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertex_Shader_Object,
            GL_INFO_LOG_LENGTH,
            &iInfoLogLength);

        if (iInfoLogLength > 0) //Error found
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei Written;
                glGetShaderInfoLog(gVertex_Shader_Object, iInfoLogLength, &Written, szInfoLog);

                printf("\n %s Failed in Vertex Shader", szInfoLog);
                free(szInfoLog);
                szInfoLog = NULL;
                
                [self release];

                exit(0);
            }
        }
    }


    
    //2 - FS
    gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

    //Define FS code
    GLchar *FragmentShaderCode =
        "#version 300 es"            \
        "\n"                        \
        "precision highp float;"
        "precision lowp int;"
        "in vec3 Phong_ADS_Light;"    \
        "out vec4 FragColor;"        \
        "uniform int u_LKeyPressed;"\
        "\n"                        \
        "void main(void)"            \
        "{"                            \
        "if(u_LKeyPressed == 1)"    \
        "{"                            \
        "FragColor = vec4(Phong_ADS_Light,1.0);"        \
        "}"                            \
        "else"                        \
        "{"                            \
        "FragColor = vec4(1.0,1.0,1.0,1.0);" \
        "}"                            \
        "}";

    //Specify code to Obj
    glShaderSource(gFragment_Shader_Object, 1,
        (GLchar**)&FragmentShaderCode, NULL);

    //Compile the shader
    glCompileShader(gFragment_Shader_Object);


    //Error check for FS
    glGetShaderiv(gFragment_Shader_Object,
        GL_COMPILE_STATUS,
        &iShaderCompileStatus);

    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragment_Shader_Object,
            GL_INFO_LOG_LENGTH,
            &iInfoLogLength);

        if (iInfoLogLength > 0) //Error found
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei Written;
                glGetShaderInfoLog(gFragment_Shader_Object, iInfoLogLength, &Written, szInfoLog);

                printf("\n  Failed in Fragment Shader : %s", szInfoLog);
                free(szInfoLog);
                szInfoLog = NULL;

                [self release];
                
                exit(0);
            }
        }
    }


    //3 - Create Shader Program
    gShader_Program_Object_Vert = glCreateProgram();

    //attach VS & FS
    glAttachShader(gShader_Program_Object_Vert, gVertex_Shader_Object);

    glAttachShader(gShader_Program_Object_Vert, gFragment_Shader_Object);

    //prelink
    glBindAttribLocation(gShader_Program_Object_Vert, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShader_Program_Object_Vert, AMC_ATTRIBUTE_NORMAL, "vNormal");

    //Link SP
    glLinkProgram(gShader_Program_Object_Vert);


    // Error check for SP
    glGetProgramiv(gShader_Program_Object_Vert, GL_LINK_STATUS, &iProgrmLinkStatus);


    if (iProgrmLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShader_Program_Object_Vert, GL_INFO_LOG_LENGTH, &iInfoLogLength);

        if (iInfoLogLength > 0) //Error found
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei Written;
                glGetProgramInfoLog(gShader_Program_Object_Vert, iInfoLogLength, &Written, szInfoLog);

                printf("\n  Failed in Shader Program Linking , %s", szInfoLog);
                free(szInfoLog);
                szInfoLog = NULL;
                
                [self release];
                
                exit(0);
            }
        }
    }

  
 //postlink
    Model_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_model_matrix");
    Projection_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_projection_matrix");
    View_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_view_matrix");

    LD_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_LD");
    KD_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_KD");

    LA_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_LA");
    KA_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_KA");

    LS_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_LS");
    KS_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_KS");

    LisPressed_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_LKeyPressed");
    Light_Position_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_Light_Position");
    Material_Shininess = glGetUniformLocation(gShader_Program_Object_Vert, "u_Shininess");

}


//new
- (void) PerFragment
{
    // Error checking
    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    char *szInfoLog = NULL;
    GLint iProgrmLinkStatus = 0;

    
    /*
    Add 12 Uniforms for Light
    */

    //For Shaders : create
    gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

    //Define VS code
    GLchar *VertexShaderCode =
        "#version 300 es"                            \
        "\n"                                        \
        "precision highp float;"
        "precision lowp int;"
        "in vec4 vPosition;"                        \
        "in vec3 vNormal;"                            \
        "out vec3 T_Norm;"                            \
        "out vec3 Light_Direction;"                    \
        "out vec3 Viewer_Vector;"                    \
        "\n"                                        \
        "uniform mat4 u_model_matrix;"                \
        "uniform mat4 u_view_matrix;"                \
        "uniform mat4 u_projection_matrix;"            \
        "uniform vec3 u_LA;"                        \
        "uniform vec3 u_LD;"                        \
        "uniform vec3 u_LS;"                        \
        "uniform vec3 u_KA;"                        \
        "uniform vec3 u_KD;"                        \
        "uniform vec3 u_KS;"                        \
        "uniform float u_Shininess;"                \
        "uniform int u_LKeyPressed;"                \
        "uniform vec4 u_Light_Position;"            \
        "\n"                                        \
        "void main(void)"                            \
        "{"                                            \
        "vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"    \
        "T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"            \
        "Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"    \
        "Viewer_Vector = vec3(-Eye_Coordinates.xyz);"\
        "gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"    \
        "}";

    
    
    //Specify code to Obj
    glShaderSource(gVertex_Shader_Object,
        1,
        (const GLchar**)&VertexShaderCode,
        NULL);

    //Compile the shader
    glCompileShader(gVertex_Shader_Object);


    //Error Code for VS
    glGetShaderiv(gVertex_Shader_Object,
        GL_COMPILE_STATUS,
        &iShaderCompileStatus);


    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertex_Shader_Object,
            GL_INFO_LOG_LENGTH,
            &iInfoLogLength);

        if (iInfoLogLength > 0) //Error found
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei Written;
                glGetShaderInfoLog(gVertex_Shader_Object, iInfoLogLength, &Written, szInfoLog);

                printf("\n %s Failed in Vertex Shader", szInfoLog);
                free(szInfoLog);
                szInfoLog = NULL;
                [self release];
                
                exit(0);
            }
        }
    }


    //2 - FS
    gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

    //Define FS code :
    GLchar *FragmentShaderCode =
        "#version 300 es"            \
        "\n"                        \
        "precision highp float;"
        "precision lowp int;"
        "in vec3 T_Norm;"            \
        "in vec3 Light_Direction;"    \
        "in vec3 Viewer_Vector;"    \
        "out vec4 FragColor;"        \
        "\n"                        \
        "uniform vec3 u_LA;"        \
        "uniform vec3 u_LD;"        \
        "uniform vec3 u_LS;"        \
        "uniform vec3 u_KA;"        \
        "uniform vec3 u_KD;"        \
        "uniform vec3 u_KS;"        \
        "uniform float u_Shininess;"    \
        "uniform int u_LKeyPressed;"    \
        "uniform vec4 u_Light_Position;"\
        "\n"                            \
        "void main(void)"            \
        "{"                            \
        "if(u_LKeyPressed == 1)"    \
        "{"                            \
        "vec3 Normalized_View_Vector = normalize(Viewer_Vector);"    \
        "vec3 Normalized_Light_Direction = normalize(Light_Direction);"\
        "vec3 Normalized_TNorm = normalize(T_Norm);"\
        "float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0);"    \
        "vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm);"\
        "vec3 Ambient = vec3(u_LA * u_KA);"    \
        "vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);"    \
        "vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess));" \
        "vec3 Phong_ADS_Light = Ambient + Diffuse+ Specular;"    \
        "FragColor = vec4(Phong_ADS_Light,1.0);" \
        "}"                            \
        "else"                        \
        "{"                            \
        "FragColor = vec4(1.0,1.0,1.0,1.0);" \
        "}"                            \
        "}";


    //Specify code to Obj
    glShaderSource(gFragment_Shader_Object, 1,
        (GLchar**)&FragmentShaderCode, NULL);

    //Compile the shader
    glCompileShader(gFragment_Shader_Object);


    //Error check for FS
    glGetShaderiv(gFragment_Shader_Object,
        GL_COMPILE_STATUS,
        &iShaderCompileStatus);

    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragment_Shader_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);

        if (iInfoLogLength > 0) //Error found
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei Written;
                glGetShaderInfoLog(gFragment_Shader_Object, iInfoLogLength, &Written, szInfoLog);

                printf("\n  Failed in Fragment Shader : %s", szInfoLog);
                free(szInfoLog);
                szInfoLog = NULL;
                [self release];
                
                exit(0);
            }
        }
    }


    //3 - Create Shader Program
    gShader_Program_Object_Frag = glCreateProgram();

    //attach VS & FS
    glAttachShader(gShader_Program_Object_Frag, gVertex_Shader_Object);

    glAttachShader(gShader_Program_Object_Frag, gFragment_Shader_Object);

    //prelink
    glBindAttribLocation(gShader_Program_Object_Frag, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShader_Program_Object_Frag, AMC_ATTRIBUTE_NORMAL, "vNormal");


    //Link SP
    glLinkProgram(gShader_Program_Object_Frag);


    // Error check for SP
    glGetProgramiv(gShader_Program_Object_Frag, GL_LINK_STATUS, &iProgrmLinkStatus);


    if (iProgrmLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShader_Program_Object_Frag, GL_INFO_LOG_LENGTH, &iInfoLogLength);

        if (iInfoLogLength > 0) //Error found
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei Written;
                glGetProgramInfoLog(gShader_Program_Object_Frag, iInfoLogLength, &Written, szInfoLog);

                printf("\n  Failed in Shader Program Linking , %s", szInfoLog);
                free(szInfoLog);
                szInfoLog = NULL;

                [self release];
                
                exit(0);
            }
        }
    }
 
 
    //postlink
    mUniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_model_matrix");
    
    Projection_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_projection_matrix");
    
    View_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_view_matrix");

    LD_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_LD");
    KD_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_KD");

    LA_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_LA");
    KA_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_KA");

    LS_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_LS");
    KS_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_KS");

    Light_Position_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_Light_Position");
    Material_Shininess_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_Shininess");

    //common
    LisPressed_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_LKeyPressed");
    
    
}



-(void) initialize
{

        //most imp calls
     [self PerVertex]; //must be done
    
     [self PerFragment];

    
    //sphere
    makeSphere(2.0,30,30);
    
    //depth
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    flag = 1;
    bisLPressed = 1;

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    perspectiveProjectionMatrix = mat4::identity();
}


-(void) drawRect:(CGRect)rect
{
    
}

//imp
-(void) addGestures
{
    
    UITapGestureRecognizer *singleTapGestureRecognizer =
    [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector (onSingleTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    // this will allow to differentiate
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    // swipe
    UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    // long press
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    
    
}



-(void) layoutSubviews
{
    
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable: (CAEAGLLayer *)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    //Depth BUffer Regenration!
    glGenRenderbuffers(1, &depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        printf("Failed to Update FBO");
        glCheckFramebufferStatus(GL_FRAMEBUFFER);
    }
    else {
        printf("FBO Updated Successfully \n");
    }
    
    
    if(height == 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    GLfloat Width = (GLfloat)width;
    GLfloat Height = (GLfloat)height;
    
    perspectiveProjectionMatrix = perspective(45.0f, Width/Height, 0.1f, 100.0f);
    
    [self drawView: nil];
}


//VIMP

-(GLuint)loadTextureFromBMPFile:(NSString *)texFileName :(NSString *)extension
{
    NSString *textureFileNameWithPath = [[NSBundle mainBundle] pathForResource:texFileName ofType:extension];
    
    UIImage *bmpImage = [[UIImage alloc] initWithContentsOfFile:textureFileNameWithPath];
    
    if(!bmpImage)
    {
        NSLog(@"Can't find %@", textureFileNameWithPath);
        return(0);
    }
    
    CGImageRef cgImage = bmpImage.CGImage; //try
    
    int bmpWidth = (int)CGImageGetWidth(cgImage);
    int bmpHeight = (int)CGImageGetHeight(cgImage);
    
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    
    void* pixels = (void *)CFDataGetBytePtr(imageData);
    
    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 bmpWidth,
                 bmpHeight,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels); // imp
    
    glGenerateMipmap(GL_TEXTURE_2D); // new addition
    
    CFRelease(imageData);

    return(bmpTexture);

}

-(BOOL) acceptsFirstResponder
{
    return(YES);
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    //TO Start Our Code
}

-(void) onSingleTap:(UITapGestureRecognizer *) event
{
    printf("\n Single Tap.. \n");

   
}


-(void) onDoubleTap:(UITapGestureRecognizer *) event
{
    if(flag > 2)
    {
        flag = 1;
        bisLPressed = 0;
    }
    

    bisLPressed = 1;
    flag++;

    printf("\n in Double Tap : %d",flag);

        
}

-(void) onSwipe:(UISwipeGestureRecognizer *) event {
    [self release];
    exit(0);
}

-(void) onLongPress:(UILongPressGestureRecognizer *) event
{
    printf("\n Long Press");
    
}



//Display
-(void) drawView: (id) sender
{
        [EAGLContext setCurrentContext:eaglContext];
        
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
        
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );
    
        glUseProgram(gShader_Program_Object_Vert);

        if(flag == 2)
            glUseProgram(gShader_Program_Object_Frag);
    
       mat4 Model_Matrix;
       mat4 View_Matrix;
       mat4 Projection_Matrix;
       mat4 Translation_Matrix;
       mat4 Rotation_Matrix;
       mat4 ModelView_Projection_Matrix;
       
       //2 : I[]
       Model_Matrix = mat4::identity();
       ModelView_Projection_Matrix = mat4::identity();
       View_Matrix = mat4::identity();
       Rotation_Matrix = mat4::identity();
       Translation_Matrix = mat4::identity();

       //4 : Do Mat mul
       Translation_Matrix = translate(0.0f, 0.0f, -7.0f);

       Model_Matrix = Translation_Matrix * Rotation_Matrix;


       // 5    : send it to shader

       glUniformMatrix4fv(Model_Uniform, 1, GL_FALSE, Model_Matrix);
       
       glUniformMatrix4fv(Projection_Uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
       
       glUniformMatrix4fv(View_Uniform, 1, GL_FALSE, View_Matrix);
       
       if (bisLPressed == 1)
        {
               if(flag == 1)
                {
                  glUniform1i(LisPressed_Uniform, 1);
                  glUniform3f(LD_Uniform, 0.85f, 0.85f, 0.85f);
                  glUniform3f(KD_Uniform, 1.0f, 1.0f, 1.0f);
          
                  glUniform3f(LS_Uniform, 0.85f, 0.85f, 0.85f);
                  glUniform3f(KS_Uniform, 1.0f, 1.0f, 1.0f);
                  
                  glUniform3f(LA_Uniform, 0.25f, 0.25f, 0.25f);
                  glUniform3f(KA_Uniform, 0.25f, 0.25f, 0.25f);
                  
                  glUniform1f(Material_Shininess, 50.0f);
                  
              //imp : out to in Light
                  glUniform4f(Light_Position_Uniform, 100.0f, 100.0f, 50.0f, 1.0f);
          
               }
          
                  else if (flag == 2)
                  {
                      glUniform1i(LisPressed_Uniform_Frag, 1);
                      glUniform3f(LD_Uniform_Frag, 0.85f, 0.85f, 0.85f);
                      glUniform3f(KD_Uniform_Frag, 1.0f, 1.0f, 1.0f);
          
                      glUniform3f(LS_Uniform_Frag, 0.85f, 0.85f, 0.85f);
                      glUniform3f(KS_Uniform_Frag, 1.0f, 1.0f, 1.0f);
          
                      glUniform3f(LA_Uniform_Frag, 0.25f, 0.25f, 0.25f);
                      glUniform3f(KA_Uniform_Frag, 0.25f, 0.25f, 0.25f);
          
                      glUniform1f(Material_Shininess_Frag, 128.0f);
          
                      glUniform4f(Light_Position_Uniform_Frag, 125.0f, 125.0f, 50.0f, 1.0f);
          
                      glUniformMatrix4fv(mUniform_Frag, 1, GL_FALSE, Model_Matrix);
                      
                      glUniformMatrix4fv(Projection_Uniform_Frag, 1, GL_FALSE, perspectiveProjectionMatrix);
                      
                      glUniformMatrix4fv(View_Uniform_Frag, 1, GL_FALSE, View_Matrix);
                  }
            
        }

       else
       {
           glUniform1i(LisPressed_Uniform, 0);
       }

    
    draw();
          
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void) startAnimation //update
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond: animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}


-(void) stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}


-(void) dealloc
{
    //Uninitialize

/*
    //Vao & vbo
    
    if(vbo_rectangle) {
        glDeleteBuffers(1, &vbo_rectangle);
        vbo_rectangle = 0;
    }
    if(vao_rectangle) {
        glDeleteVertexArrays(1, &vao_rectangle);
        vao_rectangle = 0;
    }
*/
    
    //Safe Release
    if(gShader_Program_Object_Vert) {
        GLsizei iShaderCnt = 0;
        GLsizei iShaderNo = 0;
        
        glUseProgram(gShader_Program_Object_Vert);
        
        glGetProgramiv(gShader_Program_Object_Vert,
                       GL_ATTACHED_SHADERS,
                       &iShaderCnt);
        
        GLuint *pShaders = (GLuint*)malloc(iShaderCnt * sizeof(GLuint));
        
        if(pShaders) {
            glGetAttachedShaders(gShader_Program_Object_Vert,
                                 iShaderCnt,
                                 &iShaderCnt,
                                 pShaders);
            
            for(iShaderNo = 0; iShaderNo < iShaderCnt; iShaderNo++) {
                
                glDetachShader(gShader_Program_Object_Vert, pShaders[iShaderNo]);
                
                pShaders[iShaderNo] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gShader_Program_Object_Frag);
        gShader_Program_Object_Frag = 0;
        glUseProgram(0);
    }
    
    if(depthBuffer) {
        glDeleteRenderbuffers(1, &depthBuffer);
        depthBuffer = 0;
    }
    if(colorBuffer){
        glDeleteRenderbuffers(1, &colorBuffer);
        colorBuffer = 0;
    }
    
    if(frameBuffer) {
        glDeleteFramebuffers(1, &frameBuffer);
        frameBuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext) {
        [EAGLContext setCurrentContext:nil];
    }
    
    if(eaglContext) {
        eaglContext = nil;
    }
    
    [super dealloc];
}



@end


