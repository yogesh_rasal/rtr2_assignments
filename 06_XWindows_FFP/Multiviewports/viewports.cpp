#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

using namespace std;

bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

GLXContext gGLXContext;

int giKeyPressed = 0;

//new
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void initialize(void);
    void display(void);
    void resize(int,int);
    void update(void);
    void uninitialize();


int main(void)
{

    int giWidth = giWindowWidth;
    int giHeight = giWindowHeight;

    bool bDone = false;
    char ascii[26];
    CreateWindow();

    initialize();

    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
             XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
                        default:
                        break;
                    }

                    XLookupString(&event.xkey,ascii,sizeof(ascii),NULL,NULL);
                    switch(ascii[0])
                    {
                        case 'F':
                        case 'f':
                            if(bFullscreen == false)
                            {
                                ToggleFullscreen();
                                bFullscreen = true;
                            }
                            else
                            {
                                ToggleFullscreen();
                                bFullscreen = false;
                            }
                            break;
                        default:
                            break;
                    }
                break;

                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                        giKeyPressed = 1;
                        resize(giWidth,giHeight);
                        break;
                        
                        case 2:
                        giKeyPressed = 2;
                        resize(giWidth,giHeight);
                        break;

                        case 3:
                        giKeyPressed = 3;
                        resize(giWidth,giHeight);
                        break;
                        
                        case 4:
                        giKeyPressed = 4;
                        resize(giWidth,giHeight);
                        break;

                        case 5:
                        giKeyPressed = 5;
                        resize(giWidth,giHeight);
                        break;

                        case 6:
                        giKeyPressed = 6;
                        resize(giWidth,giHeight);
                        break;

                        case 7:
                        giKeyPressed = 7;
                        resize(giWidth,giHeight);
                        break;


                        default:
                        break;
                    }
                break;

                case ConfigureNotify:
                    giWidth = event.xconfigure.width;
                    giHeight = event.xconfigure.height;
                    resize(giWidth,giHeight);
                     break;

                case Expose:
                    break;

                case DestroyNotify:
                    break;

                case 33:
                    uninitialize();
                    exit(0);
                    break;
                
                default:
                    break;
            }
        }

        update();
        display();
    }
    uninitialize();
    return(0);
}


void CreateWindow(void)
{

    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

//new
    static int frameBufferAttributes[] =
    {
        GLX_RGBA,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE , 8,
        GLX_BLUE_SIZE , 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_DOUBLEBUFFER, True,
        None
    };

    gpDisplay = XOpenDisplay(NULL);
    
  if(gpDisplay == NULL)
    {
        printf("ERROR: Unable To Open X Display.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

    gpXVisualInfo = glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
    if(gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable To Get XVisualInfo.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
        RootWindow(gpDisplay,gpXVisualInfo->screen),
        gpXVisualInfo->visual,
        AllocNone);

    gColormap = winAttribs.colormap;
    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);  
           
    winAttribs.event_mask = ExposureMask| VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap ; 

    gWindow = XCreateWindow(gpDisplay,
                RootWindow(gpDisplay,gpXVisualInfo->screen),
                0,0,giWindowWidth,
                giWindowHeight,
                0,
                gpXVisualInfo->depth,
                InputOutput,
                gpXVisualInfo->visual,
                styleMask,&winAttribs);

        if(!gWindow)
        {
            printf("ERROR: Failed To Create Window.\n Exitting Now...\n");
            uninitialize();
            exit(1);
        }

    XStoreName(gpDisplay,gWindow,"Mouse-click Viewports : By Yogeshwar");
    
    Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
    XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

    XMapWindow(gpDisplay,gWindow);              
}


void ToggleFullscreen(void)
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 :1;
    fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev); 
}


void initialize(void)
{
    void resize(int,int);

    gGLXContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
    glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glClearColor(0.0f,0.0f,0.0f,0.0f);  //black
    resize(giWindowWidth,giWindowHeight);
}


void resize(int width,int height)
{
   if (height == 0)
        height = 1;

    //Event Based
    switch (giKeyPressed)
  {
    case 1:
            glViewport(0, (GLsizei)height / 2, (GLsizei)width / 2, (GLsizei)height / 2); //Left-Top Corner
        break;
    
    case 2:
            glViewport((GLsizei)width / 2, (GLsizei)height / 2, (GLsizei)width / 2, (GLsizei)height / 2); //Right-Top Corner
        break;
    
    case 3:
            glViewport((GLsizei)width / 2, 0, (GLsizei)width / 2, (GLsizei)height / 2); //Right-Bottom Corner
        break;
    
    case 4:
            glViewport(0, 0, (GLsizei)width / 2, (GLsizei)height / 2); //Left-Bottom Corner
        break;
    
    case 5:
            glViewport(0, (GLsizei)height / 2, (GLsizei)width, (GLsizei)height / 2); //TopSide  
        break;
    
    case 6:
            glViewport(0, 0,(GLsizei)width , (GLsizei)height/2 ); //BottomSide
        break;
    
    case 7:
            glViewport(0,0, (GLsizei)width/2, (GLsizei)height); //LeftSide  
        break;
    
    case 8:
            glViewport((GLsizei)width / 2, 0, (GLsizei)width / 2,  (GLsizei)height); //RightSide    
        break;
    
    case 9:
            glViewport(((GLsizei)width/2) - ((GLsizei)width/4), ((GLsizei)height / 2)-((GLsizei)height / 4), ((GLsizei)width / 2), (GLsizei)height/2 ); //Center    
        break;

    default:
            glViewport(0, 0,(GLsizei)width , (GLsizei)height ); //Normal
        break;

  }

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    //most imp
    gluPerspective(60.0f, (GLfloat)width/ (GLfloat)height, 0.1f, 100.0f);
}



void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f, 0.0f, -3.0f);
    
    glBegin(GL_TRIANGLES);
    glColor3f(1.0f,0.0f,0.0f);
    glVertex3f(0.0f, 1.0f, 0.0f);
    glColor3f(0.0f,1.0f,0.0f);
    glVertex3f(-1.0f, -1.0f, 0.0f);
    glColor3f(0.0f,0.0f,1.0f);
    glVertex3f(1.0f, -1.0f, 0.0f);
    glEnd();

    glXSwapBuffers(gpDisplay,gWindow);
}



//new
void update(void)
{

}

void uninitialize(void)
{
    GLXContext currentGLXContext;
    currentGLXContext =glXGetCurrentContext();

    if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,0,0);
    }

    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay,gGLXContext);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay,gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

