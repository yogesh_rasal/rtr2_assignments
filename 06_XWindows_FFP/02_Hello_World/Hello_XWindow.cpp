// Hello XWindows

#include <iostream>
#include <stdio.h>
#include <stdlib.h> 
#include <memory.h> 

#include <X11/Xlib.h>
#include <X11/Xutil.h> 
#include <X11/XKBlib.h>
#include <X11/keysym.h> 

using namespace std;

bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;


int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();
	
	// Imp
	static int winWidth = giWindowWidth;
	static int winHeight = giWindowHeight;

	// Locals
	static XFontStruct *pxfontstruct = NULL; //Font
	static GC gc; // HDC
	XGCValues gcValues; // PS
	XColor text_color;
	char Str[] = "Hello X-World";
	int strLength ,strWidth,fontHeight;
	
	
	CreateWindow();

	//message Loop
	XEvent event;
	KeySym keysym;
	
	 while(1)	
		{
		  XNextEvent(gpDisplay,&event);

		    switch(event.type)
			{
				case MapNotify:
					pxfontstruct = XLoadQueryFont(gpDisplay,"fixed"); // System Font
					break;

				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);

				switch(keysym)
					{
						case XK_Escape:
							XFreeGC(gpDisplay,gc);
							XUnloadFont(gpDisplay,pxfontstruct->fid);
							uninitialize();
							exit(0);
						
						case XK_F:
						case XK_f:
							 if(bFullscreen == false)
							 {
							    ToggleFullscreen();
							    bFullscreen = true;
							 }
							else
							{
							    ToggleFullscreen();
							    bFullscreen = false;
							}
						     break;
						default:
						     break;
					}
				    break;
				
				case ButtonPress: 
					switch(event.xbutton.button)
					{
						case 1: 
						    break;
						case 2: 
						    break;
						case 3: 
						    break;
						default:
						    break;
					}
					break;
				
				case MotionNotify: 
					break;
				
				case ConfigureNotify: 
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					break;

				case Expose: // Paint
					gc = XCreateGC(gpDisplay,gWindow,0,&gcValues);
					XSetFont(gpDisplay,gc,pxfontstruct->fid);

					XAllocNamedColor(gpDisplay,gColormap,"green",&text_color,&text_color);
					XSetForeground(gpDisplay,gc,text_color.pixel);
					
					// Len,WD,HT					
					strLength = strlen(Str);
					strWidth = XTextWidth(pxfontstruct,Str,strLength);	

					// Height = Ascent + Descent
					fontHeight = pxfontstruct->ascent + pxfontstruct->descent;
					XDrawString(gpDisplay,gWindow,gc,
						    (winWidth/2)-(strWidth/2),
						    (winHeight/2)-(fontHeight/2),
						     Str,strLength); 

					break;

				case DestroyNotify:
					break;

				case 33:
					XFreeGC(gpDisplay,gc);
					XUnloadFont(gpDisplay,pxfontstruct->fid);
					uninitialize();
					exit(1);
					break;
				default:
					break;
			}
		

	}
     return(0);
}


void CreateWindow(void)
{
	void uninitialize(void);

	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	
	gpDisplay = XOpenDisplay(NULL);

	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	defaultScreen = XDefaultScreen(gpDisplay);
	
	defaultDepth = DefaultDepth(gpDisplay,defaultScreen);

	gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
	
	if(gpXVisualInfo == NULL)
	{
		printf("\n Unable to allocate Memory for Visual  Info");	
		uninitialize();
		exit(1);
	} 		
			
	XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,gpXVisualInfo);	
	
       if(gpXVisualInfo == NULL)
	{
		printf("\n Unable to Get Memory for Visual  Info");	
		uninitialize();
		exit(1);
	} 		
	
				
	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;

	//Color Map
	winAttribs.colormap=XCreateColormap(gpDisplay, 
					    RootWindow(gpDisplay, gpXVisualInfo->screen), 
					    gpXVisualInfo->visual,
 					    AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask |
			       StructureNotifyMask;
	
	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	
	gWindow = XCreateWindow(gpDisplay,
			      RootWindow(gpDisplay, gpXVisualInfo->screen),
			      0,
			      0,
			      giWindowWidth,
			      giWindowHeight,
			      0,
			      gpXVisualInfo->depth,
			      InputOutput,
			      gpXVisualInfo->visual,
			      styleMask,
			      &winAttribs);
	
	XStoreName(gpDisplay, gWindow, "First X-Window by Yogeshwar");
	
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	
	XMapWindow(gpDisplay, gWindow);
}



void ToggleFullscreen(void)
{	
	//Best Code
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};
	
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));
	
	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;
	
	XSendEvent(gpDisplay,
	           RootWindow(gpDisplay, gpXVisualInfo->screen),
	           False,
	           StructureNotifyMask,
	           &xev);
}



void uninitialize(void)
{
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;	
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}

