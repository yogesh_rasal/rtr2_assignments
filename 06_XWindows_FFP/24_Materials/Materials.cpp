/*
24 Spehere
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h> //new


using namespace std;

bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 980;
int giWindowHeight = 720;

GLXContext gGLXContext;
bool gbLight = false;

//Vectors
GLfloat  Light_Ambient[]  = {1.0f,0.0f,0.0f,1.0f};
GLfloat  Light_Diffuse[]  = {1.0f,1.0f,1.0f,1.0f};
GLfloat  Light_Position[] = {0.0f,0.0f,0.0f,1.0f};
GLfloat  Light_Specular[] = {1.0f,1.0f,1.0f,1.0f};


// New
GLfloat Light_Model_Ambient[] = {0.2f,0.2f,0.2f,1.0f};
GLfloat Light_Model_Local_Viewer = {0.0f};
GLUquadric *Quadric[24];
GLfloat AngleOfX = 0.0f;
GLfloat AngleOfY = 0.0f;
GLfloat AngleOfZ = 0.0f;
GLint KeyPressed = 0;


//func
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void initialize(void);
    void display(void);
    void resize(int,int);
    void update(void);
    void uninitialize(void);
    void Draw24Spheres(void);

//code
int main(void)
{
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

    bool bDone = false;

    CreateWindow();

    initialize();

    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
             XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                break;
                
                case KeyPress:
                
                    keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
                
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
      
                        case XK_F:
                        case XK_f:
                            if(bFullscreen == false)
                            {
                                ToggleFullscreen();
                                bFullscreen = true;
                            }
                            else
                            {
                                ToggleFullscreen();
                                bFullscreen = false;
                            }
                         break;

                       	 case XK_L:
                           case XK_l:
                               if(gbLight == false)
                                {
                                    gbLight = true;
                                    glEnable(GL_LIGHTING);  
                                }

                                else 
                                {
                                    gbLight = false;
                                    glDisable(GL_LIGHTING);  
                                }
                            break;
     			
		case XK_x:		
		case XK_X: KeyPressed = 1;
				  AngleOfX = 0.0f;
				  break;
		
		case XK_y:		
		case XK_Y: KeyPressed = 2;
				  AngleOfY = 0.0f;
				  break;

		case XK_z:		
		case XK_Z: KeyPressed = 3;
				  AngleOfZ = 0.0f;
				  break;


                        default:
                        break;
                    }
                break;
      
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                        break;
                        case 2:
                        break;
                        case 3:
                        break;
                        default:
                        break;
                    }
                break;
      
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth,winHeight);
                break;
      
                case Expose:
                break;
      
                case DestroyNotify:
                break;
      
                case 33:
                    uninitialize();
                    exit(0);
                break;
      
                default:
                break;
            }
        }
        
        update(); //2

        display(); //most imp
    }
    return(0);
}

void CreateWindow(void)
{

    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

//new 1
    static int frameBufferAttributes[] =
    {
        GLX_RGBA,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE , 8,
        GLX_BLUE_SIZE , 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_DOUBLEBUFFER, True,
        None
    };

    gpDisplay = XOpenDisplay(NULL);
    
  if(gpDisplay == NULL)
    {
        printf("ERROR: Unable To Open X Display.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

    //2
    gpXVisualInfo = glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
    
    if(gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable To Get XVisualInfo.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
        RootWindow(gpDisplay,gpXVisualInfo->screen),
        gpXVisualInfo->visual,
        AllocNone);

    gColormap = winAttribs.colormap;
    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);  
           
    winAttribs.event_mask = ExposureMask| VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap ; 

    gWindow = XCreateWindow(gpDisplay,
                RootWindow(gpDisplay,gpXVisualInfo->screen),
                0,0,giWindowWidth,
                giWindowHeight,
                0,
                gpXVisualInfo->depth,
                InputOutput,
                gpXVisualInfo->visual,
                styleMask,&winAttribs);

        if(!gWindow)
        {
            printf("ERROR: Failed To Create Window.\n Exitting Now...\n");
            uninitialize();
            exit(1);
        }

    XStoreName(gpDisplay,gWindow,"XWindows Materials");
    
    Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
    XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

    XMapWindow(gpDisplay,gWindow);              
}


//Best
void ToggleFullscreen(void)
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 :1; //ternary value to union
    fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False); //
    xev.xclient.data.l[1] = fullscreen; //union

    XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev); 
}

void initialize(void)
{

    gGLXContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE); //H/W 
    glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

    //depth
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClearColor(0.1f,0.1f,0.1f,1.0f);  //black

    	//5
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

    // Light Config

    glLightfv(GL_LIGHT0,GL_AMBIENT,Light_Ambient);   

    glLightfv(GL_LIGHT0,GL_DIFFUSE,Light_Diffuse);

    //glLightfv(GL_LIGHT0,GL_POSITION,Light_Position);

    glLightfv(GL_LIGHT0,GL_SPECULAR,Light_Specular);
    
	glEnable(GL_LIGHT0); //similar to Fuse

	for(int i =0; i < 24;i++)
	{
		Quadric[i] = gluNewQuadric();
	}


	printf("\n Lights ");

    resize(giWindowWidth,giWindowHeight);
}



void resize(int Width,int Height)
{
   if (Height == 0)
        Height = 1;
    
    glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    	if(Width < Height )
	{
		glOrtho(0.0f,15.5f,0.0f,
				15.5*(GLfloat(Width)/GLfloat(Height)),
				-10.0f,10.0f);

	} 
	else 
		glOrtho(0.0f,15.5*(GLfloat(Width)/GLfloat(Height)),0.0f,15.5f,-10.0f,10.0f);


}


void display(void)
{
   // Core Changes
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if(KeyPressed == 1)
	{
		glRotatef(AngleOfX,1.0f,0.0f,0.0f);
		Light_Position[1] = AngleOfX; //{1.0f,0.0f,0.0f,1.0f};
		Light_Position[0] = 0.0f;
		Light_Position[2] = 0.0f;
		Light_Position[3] = 0.0f;
	}

	else if(KeyPressed == 2)
	{
		glRotatef(AngleOfY,0.0f,1.0f,0.0f);
		Light_Position[2] = AngleOfY; // {0.0f,1.0f,0.0f,1.0f};
		Light_Position[0] = 0.0f;
		Light_Position[1] = 0.0f;
		Light_Position[3] = 0.0f;
	}
	
	else if(KeyPressed == 3)
	{
		glRotatef(AngleOfZ,0.0f,0.0f,1.0f);
		Light_Position[0] = AngleOfZ; //{1.0f,0.0f,0.0f,1.0f};
		Light_Position[1] = 0.0f;
		Light_Position[2] = 0.0f;
		Light_Position[3] = 0.0f;
	}


	glLightfv(GL_LIGHT0,GL_POSITION,Light_Position);
	Draw24Spheres();

    	glXSwapBuffers(gpDisplay,gWindow);
}



//new
void update(void)
{
	AngleOfX += 0.5f;
	if(AngleOfX >= 360.0f)
		AngleOfX = 0.0f;
	
	AngleOfY += 0.5f;
	if(AngleOfY >= 360.0f)
		AngleOfY = 0.0f;
	
	AngleOfZ += 0.5f;
	if(AngleOfZ >= 360.0f)
		AngleOfZ = 0.0f;	

    
}


void uninitialize(void)
{
    GLXContext currentGLXContext;
    currentGLXContext =glXGetCurrentContext();

    if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,0,0);
    }

    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay,gGLXContext);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay,gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}


//main 
void Draw24Spheres()
{
	GLfloat  Material_Ambient[4]; // {0.0215f,0.1745f,0.0215f,1.0f};
	GLfloat  Material_Diffuse[4]; // {0.0215f,0.1745f,0.0215f,1.0f};
	GLfloat  Material_Specular [4]; //  {1.0f,1.0f,1.0f,1.0f};
	GLfloat  Material_Shininess[1]; // {128.0f};
	
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

	//24 Spheres
// 1 : Emerald
	Material_Ambient[0] = 0.0215f;
	Material_Ambient[1] = 0.1745f;
	Material_Ambient[2] = 0.0215f;
	Material_Ambient[3] = 1.0;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.0215f;
	Material_Diffuse[1] = 0.1745f;
	Material_Diffuse[2] = 0.0215f;
    Material_Diffuse[3] = 1.0;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.633f;
	Material_Specular[1] = 0.727811f;
	Material_Specular[2] = 0.633f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.6f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.9f, 14.5f, 0.0f);
    gluSphere(Quadric[0],0.8f,30,30);    

// 2 = Jade
	Material_Ambient[0] = 0.135f;
	Material_Ambient[1] = 0.2225f;
	Material_Ambient[2] = 0.1575f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.54f;
	Material_Diffuse[1] = 0.89f;
	Material_Diffuse[2] = 0.63f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.316228f;
	Material_Specular[1] = 0.316228f;
	Material_Specular[2] = 0.316228f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.1f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.9f, 12.0f, 0.0f);
    gluSphere(Quadric[1],0.8f,30,30);    

// 3 = obisidian
	Material_Ambient[0] = 0.05375f;
	Material_Ambient[1] = 0.05f;
	Material_Ambient[2] = 0.06625f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.18275f;
	Material_Diffuse[1] = 0.17f;
	Material_Diffuse[2] = 0.22525f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.332741f;
	Material_Specular[1] = 0.328634f;
	Material_Specular[2] = 0.346435f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.3f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.9f, 9.5f, 0.0f);
    gluSphere(Quadric[2],0.8f,30,30);    

// 4= Pearl
	Material_Ambient[0] = 0.25f;
	Material_Ambient[1] = 0.20725f;
	Material_Ambient[2] = 0.20725f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 1.0f;
	Material_Diffuse[1] = 0.829f;
	Material_Diffuse[2] = 0.829f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.296648f;
	Material_Specular[1] = 0.296648f;
	Material_Specular[2] = 0.296648f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.088f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.9f, 7.0f, 0.0f);
    gluSphere(Quadric[3],0.8f,30,30);    


// 5 = Ruby
	Material_Ambient[0] = 0.1745f;
	Material_Ambient[1] = 0.01175f;
	Material_Ambient[2] = 0.01175f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.61424f;
	Material_Diffuse[1] = 0.04136f;
	Material_Diffuse[2] = 0.04136f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.727811f;
	Material_Specular[1] = 0.626959f;
	Material_Specular[2] = 0.626959f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.6f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.9f, 4.5f, 0.0f);
    gluSphere(Quadric[4],0.8f,30,30);    

// 6 = Turquoise
	Material_Ambient[0] = 0.1f;
	Material_Ambient[1] = 0.18275f;
	Material_Ambient[2] = 0.1745f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.396f;
	Material_Diffuse[1] = 0.74151f;
	Material_Diffuse[2] = 0.69102f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.297524f;
	Material_Specular[1] = 0.30829f;
	Material_Specular[2] = 0.306678f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.1f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.9f, 2.0f, 0.0f);
    gluSphere(Quadric[5],0.8f,30,30);    

// Metals   ---------------------

// 1 : Brass
	Material_Ambient[0] = 0.349412f;
	Material_Ambient[1] = 0.30829f;
	Material_Ambient[2] = 0.306678f;
	Material_Ambient[3] = 1.0;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.780392f;
	Material_Diffuse[1] = 0.223529f;
	Material_Diffuse[2] = 0.027451f;
    Material_Diffuse[3] = 1.0;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.992157f;
	Material_Specular[1] = 0.941176f;
	Material_Specular[2] = 0.807843f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.21794872f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.0f, 14.5f, 0.0f);
    gluSphere(Quadric[6],0.8f,30,30);    

// 2 = Bronze
	Material_Ambient[0] = 0.2125f;
	Material_Ambient[1] = 0.1275f;
	Material_Ambient[2] = 0.054f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.714f;
	Material_Diffuse[1] = 0.4284f;
	Material_Diffuse[2] = 0.18144f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.393548f;
	Material_Specular[1] = 0.271906f;
	Material_Specular[2] = 0.166721f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.2f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.0f, 12.0f, 0.0f);
    gluSphere(Quadric[7],0.8f,30,30);    

// 3 = Chrome
	Material_Ambient[0] = 0.25f;
	Material_Ambient[1] = 0.25f;
	Material_Ambient[2] = 0.25f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.4f;
	Material_Diffuse[1] = 0.4f;
	Material_Diffuse[2] = 0.4f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.774597f;
	Material_Specular[1] = 0.774597f;
	Material_Specular[2] = 0.774597f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.6f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.0f, 9.5f, 0.0f);
    gluSphere(Quadric[8],0.8f,30,30);    

// 4= copper
	Material_Ambient[0] = 0.19125f;
	Material_Ambient[1] = 0.0735f;
	Material_Ambient[2] = 0.02025f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.7038f;
	Material_Diffuse[1] = 0.27048f;
	Material_Diffuse[2] = 0.0828f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.25677f;
	Material_Specular[1] = 0.137622f;
	Material_Specular[2] = 0.086014f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.1f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.0f, 7.0f, 0.0f);
    gluSphere(Quadric[9],0.8f,30,30);    


// 5 = Gold
	Material_Ambient[0] = 0.24725f;
	Material_Ambient[1] = 0.1995f;
	Material_Ambient[2] = 0.0745f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.7517f;
	Material_Diffuse[1] = 0.6065f;
	Material_Diffuse[2] = 0.2265f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.6283f;
	Material_Specular[1] = 0.55580f;
	Material_Specular[2] = 0.36606f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.4f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.0f, 4.5f, 0.0f);
    gluSphere(Quadric[10],0.8f,30,30);    

// 6 = Silver
	Material_Ambient[0] = 0.19225f;
	Material_Ambient[1] = 0.19225f;
	Material_Ambient[2] = 0.19225f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.5075f;
	Material_Diffuse[1] = 0.5075f;
	Material_Diffuse[2] = 0.5075f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.50828f;
	Material_Specular[1] = 0.50828f;
	Material_Specular[2] = 0.50828f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.4f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.0f, 2.0f, 0.0f);
    gluSphere(Quadric[11],0.8f,30,30);    

// Plastic ---------------------
// 1 - Black
	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.0f;
	Material_Ambient[2] = 0.0f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.01f;
	Material_Diffuse[1] = 0.01f;
	Material_Diffuse[2] = 0.01f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.5f;
	Material_Specular[1] = 0.5f;
	Material_Specular[2] = 0.5f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.25f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(17.0f, 14.5f, 0.0f);
    gluSphere(Quadric[12],0.8f,30,30);    

// 2 - Cyan
	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.1f;
	Material_Ambient[2] = 0.06f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.0f;
	Material_Diffuse[1] = 0.5098039f;
	Material_Diffuse[2] = 0.5098039f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.0f;
	Material_Specular[1] = 0.501960f;
	Material_Specular[2] = 0.501960f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.25f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(17.0f, 12.0f, 0.0f);
    gluSphere(Quadric[13],0.8f,30,30);    

//3 - Green
	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.1f;
	Material_Ambient[2] = 0.06f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.1f;
	Material_Diffuse[1] = 0.35f;
	Material_Diffuse[2] = 0.1f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.45f;
	Material_Specular[1] = 0.55f;
	Material_Specular[2] = 0.45f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.25f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(17.0f, 9.5f, 0.0f);
    gluSphere(Quadric[14],0.8f,30,30);    

// Red

	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.0f;
	Material_Ambient[2] = 0.0f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.5f;
	Material_Diffuse[1] = 0.0f;
	Material_Diffuse[2] = 0.0f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.7f;
	Material_Specular[1] = 0.6f;
	Material_Specular[2] = 0.6f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.25f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(17.0f, 7.0f, 0.0f);
    gluSphere(Quadric[15],0.8f,30,30);    


// White
	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.0f;
	Material_Ambient[2] = 0.0f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.55f;
	Material_Diffuse[1] = 0.55f;
	Material_Diffuse[2] = 0.55f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.7f;
	Material_Specular[1] = 0.7f;
	Material_Specular[2] = 0.7f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.25f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(17.0f, 4.5f, 0.0f);
    gluSphere(Quadric[16],0.8f,30,30);    

// Yellow
	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.0f;
	Material_Ambient[2] = 0.0f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.5f;
	Material_Diffuse[1] = 0.5f;
	Material_Diffuse[2] = 0.0f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.6f;
	Material_Specular[1] = 0.6f;
	Material_Specular[2] = 0.5f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.25f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(17.0f, 2.0f, 0.0f);
    gluSphere(Quadric[17],0.8f,30,30);    


// -------- Rubber
// 1 - Black
	Material_Ambient[0] = 0.02f;
	Material_Ambient[1] = 0.02f;
	Material_Ambient[2] = 0.02f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.01f;
	Material_Diffuse[1] = 0.01f;
	Material_Diffuse[2] = 0.01f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.4f;
	Material_Specular[1] = 0.4f;
	Material_Specular[2] = 0.4f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.07813f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(24.3f, 14.5f, 0.0f);
    gluSphere(Quadric[18],0.8f,30,30);    

// 2 - Cyan
	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.05f;
	Material_Ambient[2] = 0.05f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.4f;
	Material_Diffuse[1] = 0.5098039f;
	Material_Diffuse[2] = 0.5098039f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.0f;
	Material_Specular[1] = 0.501960f;
	Material_Specular[2] = 0.501960f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.07813f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(24.3f, 12.0f, 0.0f);
    gluSphere(Quadric[19],0.8f,30,30);    

//3 - Green
	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.1f;
	Material_Ambient[2] = 0.06f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.1f;
	Material_Diffuse[1] = 0.35f;
	Material_Diffuse[2] = 0.1f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.45f;
	Material_Specular[1] = 0.55f;
	Material_Specular[2] = 0.45f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.07813f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(24.3f, 9.5f, 0.0f);
    gluSphere(Quadric[20],0.8f,30,30);    

// 4- Red

	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.0f;
	Material_Ambient[2] = 0.0f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.5f;
	Material_Diffuse[1] = 0.0f;
	Material_Diffuse[2] = 0.0f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.7f;
	Material_Specular[1] = 0.6f;
	Material_Specular[2] = 0.6f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.07813f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(24.3f, 7.0f, 0.0f);
    gluSphere(Quadric[21],0.8f,30,30);    


// 5- White
	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.0f;
	Material_Ambient[2] = 0.0f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.55f;
	Material_Diffuse[1] = 0.55f;
	Material_Diffuse[2] = 0.55f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.7f;
	Material_Specular[1] = 0.7f;
	Material_Specular[2] = 0.7f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.07813f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(24.3f, 4.5f, 0.0f);
    gluSphere(Quadric[22],0.8f,30,30);    

// 6- Yellow
	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.0f;
	Material_Ambient[2] = 0.0f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

	Material_Diffuse[0] = 0.5f;
	Material_Diffuse[1] = 0.5f;
	Material_Diffuse[2] = 0.0f;
    Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
	Material_Specular[0] = 0.6f;
	Material_Specular[1] = 0.6f;
	Material_Specular[2] = 0.5f;
	Material_Specular[3] = 1.0f;
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

	Material_Shininess[0] = 0.07813f * 128.0f;
    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(24.3f, 2.0f, 0.0f);
    gluSphere(Quadric[23],0.8f,30,30);    

}
// Code Ends

