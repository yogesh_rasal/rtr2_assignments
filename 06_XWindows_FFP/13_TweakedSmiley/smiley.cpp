/*
   3D Texture Map
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h> //new
#include <SOIL/SOIL.h>


using namespace std;

bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

GLXContext gGLXContext;
int KeyPressed;
GLuint Texture_Smiley;


//func
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void initialize(void);
    void display(void);
    void resize(int,int);
    void update(void);
    void uninitialize(void);
    bool LoadTexture(GLuint*,const char *);

int main(void)
{
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

    bool bDone = false;

    CreateWindow();

    initialize();

    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
             XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
      
                        case XK_F:
                        case XK_f:
                            if(bFullscreen == false)
                            {
                                ToggleFullscreen();
                                bFullscreen = true;
                            }
                            else
                            {
                                ToggleFullscreen();
                                bFullscreen = false;
                            }
                            break;

                        case XK_T: 
                        case XK_t:
                        glEnable(GL_TEXTURE_2D);
                        break;

                       case XK_1:KeyPressed = 1;
                       break;

                      case XK_2:KeyPressed = 2;
                       break;
                       
                      case XK_3:KeyPressed = 3;
                       break;
                       
                      case XK_4:KeyPressed = 4;
                       break;
                       

                        default:
                        break;
                    }
                break;
      
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                        break;
                        case 2:
                        break;
                        case 3:
                        break;
                        default:
                        break;
                    }
                break;
      
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth,winHeight);
                break;
      
                case Expose:
                break;
      
                case DestroyNotify:
                break;
      
                case 33:
                    uninitialize();
                    exit(0);
                break;
      
                default:
                break;
            }
        }
        
        update(); //2

        display(); //most imp
    }
    return(0);
}

void CreateWindow(void)
{

    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

//new 1
    static int frameBufferAttributes[] =
    {
        GLX_RGBA,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE , 8,
        GLX_BLUE_SIZE , 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_DOUBLEBUFFER, True,
        None
    };

    gpDisplay = XOpenDisplay(NULL);
    
  if(gpDisplay == NULL)
    {
        printf("ERROR: Unable To Open X Display.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

    //2
    gpXVisualInfo = glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
    
    if(gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable To Get XVisualInfo.\n Exitting Now...\n");
        uninitialize();
        exit(1);
    }

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
        RootWindow(gpDisplay,gpXVisualInfo->screen),
        gpXVisualInfo->visual,
        AllocNone);

    gColormap = winAttribs.colormap;
    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);  
           
    winAttribs.event_mask = ExposureMask| VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap ; 

    gWindow = XCreateWindow(gpDisplay,
                RootWindow(gpDisplay,gpXVisualInfo->screen),
                0,0,giWindowWidth,
                giWindowHeight,
                0,
                gpXVisualInfo->depth,
                InputOutput,
                gpXVisualInfo->visual,
                styleMask,&winAttribs);

        if(!gWindow)
        {
            printf("ERROR: Failed To Create Window.\n Exitting Now...\n");
            uninitialize();
            exit(1);
        }

    XStoreName(gpDisplay,gWindow,"Tweaked Smiley  By Yogeshwar");
    
    Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
    XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

    XMapWindow(gpDisplay,gWindow);              
}


//Best
void ToggleFullscreen(void)
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 :1; //ternary value to union
    fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False); //
    xev.xclient.data.l[1] = fullscreen; //union

    XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev); 
}

void initialize(void)
{

    gGLXContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE); //H/W 
    glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

    //depth
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClearColor(0.0f,0.0f,0.0f,1.0f);  //black

    glEnable(GL_TEXTURE_2D);	
    LoadTexture(&Texture_Smiley,"Smiley.bmp");	     
                                 

    resize(giWindowWidth,giWindowHeight);
}

//new 
bool LoadTexture(GLuint *Texture,const char *path)
{	
	int imgwidth;
	int imgheight;
	unsigned char *imageData = NULL;
	

	//impTexture
	imageData = SOIL_load_image(path,&imgwidth,&imgheight,0,SOIL_LOAD_RGBA);
	
	glPixelStorei(GL_UNPACK_ALIGNMENT,4);
	glGenTextures(1, Texture);
    glBindTexture(GL_TEXTURE_2D,*Texture);

	
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

	gluBuild2DMipmaps(GL_TEXTURE_2D,3,imgwidth,imgheight,GL_RGBA,GL_UNSIGNED_BYTE,imageData);

	SOIL_free_image_data(imageData);
	return(true);
}


void resize(int width,int height)
{
   if (height == 0)
        height = 1;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(60.0f, (GLfloat)width/ (GLfloat)height, 0.1f, 100.0f);
}


void display(void)
{
//5th
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	
	//-------- Cube
	//8th

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -4.0f);
    
    glBindTexture(GL_TEXTURE_2D, Texture_Smiley);

	 glBegin(GL_QUADS);
	 
	 if (KeyPressed == 1)
	 {
		 glTexCoord2f(0.5f, 0.5f);
		 glVertex3f(1.0f, 1.0f, 1.0f); //1 

		 glTexCoord2f(0.0f, 0.5f);
		 glVertex3f(-1.0f, 1.0f, 1.0f); //2

		 glTexCoord2f(0.0f, 0.0f);
		 glVertex3f(-1.0f, -1.0f, 1.0f); //3

		 glTexCoord2f(0.5f, 0.0f);
		 glVertex3f(1.0f, -1.0f, 1.0f); //4 
	 }

	 else if (KeyPressed == 2)
	  {

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 1.0f); //1 

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-1.0f, 1.0f, 1.0f); //2

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f); //3

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 1.0f); //4 
	  }

	 else if (KeyPressed == 3)
	 {

		 glTexCoord2f(2.0f, 2.0f);
		 glVertex3f(1.0f, 1.0f, 1.0f); //1 

		 glTexCoord2f(0.0f, 2.0f);
		 glVertex3f(-1.0f, 1.0f, 1.0f); //2

		 glTexCoord2f(0.0f, 0.0f);
		 glVertex3f(-1.0f, -1.0f, 1.0f); //3

		 glTexCoord2f(2.0f, 0.0f);
		 glVertex3f(1.0f, -1.0f, 1.0f); //4 
		
	 }

	 else if (KeyPressed == 4)
	 {	
		 glTexCoord2f(0.5f, 0.5f);
		 glVertex3f(1.0f, 1.0f, 1.0f); //1 

		 glTexCoord2f(0.5f, 0.5f);
		 glVertex3f(-1.0f, 1.0f, 1.0f); //2

		 glTexCoord2f(0.5f, 0.5f);
		 glVertex3f(-1.0f, -1.0f, 1.0f); //3

		 glTexCoord2f(0.5f, 0.5f);
		 glVertex3f(1.0f, -1.0f, 1.0f); //4 
	 }

	 else //show rectangle
	 {
		 glColor3f(1.0f,1.0f,1.0f);
		 glVertex3f(1.0f, 1.0f, 1.0f); //1 
		 glVertex3f(-1.0f, 1.0f, 1.0f); //2
		 glVertex3f(-1.0f, -1.0f, 1.0f); //3
		 glVertex3f(1.0f, -1.0f, 1.0f); //4 

	 }

	 glEnd();
    
    glXSwapBuffers(gpDisplay,gWindow);
}



//new
void update(void)
{
   
}


void uninitialize(void)
{
    GLXContext currentGLXContext;
    currentGLXContext =glXGetCurrentContext();

    if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,0,0);
    }

    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay,gGLXContext);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay,gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}


