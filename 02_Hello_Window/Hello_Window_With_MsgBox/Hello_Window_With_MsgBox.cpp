/*
	Assignment : Window with MessageBox
*/

#include<Windows.h>


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR AppName[] = TEXT("My Window");
	
	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(hInstance, IDC_HAND);
	wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = AppName;
	wndclass.lpszMenuName = NULL;
	wndclass.cbWndExtra = 0;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(AppName, TEXT(" Window with MessageBox by Yogeshwar"), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);

	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
	
}// Winmain Ends

// Callback
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_CREATE:
			MessageBox(hwnd, TEXT("This is WM_Create"), TEXT("Message"), MB_OK);
			break;

	case WM_LBUTTONDOWN:
			MessageBox(hwnd, TEXT("This is Left Mouse BUTTON"), TEXT("Message"), MB_OK);
			break;
			
	case WM_RBUTTONDOWN:
			MessageBox(hwnd, TEXT("This is Right Mouse BUTTON"), TEXT("Message"), MB_OK);
			break;
			
	case WM_KEYDOWN:
			MessageBox(hwnd, TEXT("This is WM_KEYDOWN"), TEXT("Message"), MB_OK);
			
			switch(wParam)
			{
				case VK_ESCAPE:
					MessageBox(hwnd, TEXT("This is Escape Key"), TEXT("Message"), MB_OK);
					DestroyWindow(hwnd);
					break;
				
				case 0x46:
					MessageBox(hwnd, TEXT("F Key is Pressed"), TEXT("Message"), MB_OK);
					break;	
			}
			break;
	
	case WM_DESTROY:
		MessageBox(hwnd, TEXT("This is WM_Destroy"), TEXT("Message"), MB_OK);
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}// Callback Ends
