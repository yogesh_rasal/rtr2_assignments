/*
		Assignment : Fullscreen on F
*/

#include<windows.h>

//Forward decklartion
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
bool bIsFullScreen = false;
DWORD dwStyle;
HWND ghwnd;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

//Code
 int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
	{
		WNDCLASSEX wndclass;
		HWND hwnd;
		MSG msg;
		TCHAR AppName[] = TEXT("My Window");

		wndclass.cbClsExtra = 0;
		wndclass.cbSize = sizeof(wndclass);
		wndclass.style = CS_HREDRAW | CS_VREDRAW;
		wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		wndclass.hCursor = LoadCursor(hInstance, IDC_HAND);
		wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
		wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);
		wndclass.hInstance = hInstance;
		wndclass.lpfnWndProc = WndProc;
		wndclass.lpszClassName = AppName;
		wndclass.lpszMenuName = NULL;
		wndclass.cbWndExtra = 0;

		RegisterClassEx(&wndclass);

		hwnd = CreateWindow(AppName, TEXT(" Window with Fullscreen by Yogeshwar"), 
							WS_OVERLAPPEDWINDOW,
							100, 100, 800, 600, 
							NULL, NULL, hInstance, NULL);
		
		// 
		ghwnd = hwnd;

		ShowWindow(hwnd, nCmdShow);
		UpdateWindow(hwnd);

		while (GetMessage(&msg, NULL, 0, 0))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		return((int)msg.wParam);

 }//main ends


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Declare
	void ToggleFullScreen(void);

	switch (iMsg)
	{
	case WM_CREATE:
		break;
	
	case WM_LBUTTONDOWN:
		MessageBox(hwnd, TEXT("This is Left Mouse BUTTON"), TEXT("Message"), MB_OK);
		break;

	case WM_RBUTTONDOWN:
		MessageBox(hwnd, TEXT("This is Right Mouse BUTTON"), TEXT("Message"), MB_OK);
		break;

	case WM_KEYDOWN:

		switch (wParam)
		{
		case VK_ESCAPE:
			MessageBox(hwnd, TEXT("This is Escape Key"), TEXT("Message"), MB_OK);
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;
		}
		break;

	case WM_DESTROY:
		MessageBox(hwnd,TEXT("We are in WM_Destroy"),TEXT("Title : WM_DESTROY "),MB_OK);
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

// FullScreen
void ToggleFullScreen(void)
{
	MONITORINFO mi;

	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd,GWL_STYLE);
		mi = { sizeof(MONITORINFO) };

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement( ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				//
				SetWindowLong(ghwnd,GWL_STYLE,(dwStyle & ~WS_OVERLAPPEDWINDOW));

				//
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,
							 (mi.rcMonitor.right-mi.rcMonitor.left),// WD
							 (mi.rcMonitor.bottom-mi.rcMonitor.top), // HT
							 SWP_NOZORDER || SWP_FRAMECHANGED );

			}

			ShowCursor(FALSE);
			bIsFullScreen = true;
		}

	}

	else
	{
		SetWindowLong(ghwnd,GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,
					 SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);
		
		ShowCursor(TRUE);
		bIsFullScreen = false;

	}

}// ToggleFullScreen ends
