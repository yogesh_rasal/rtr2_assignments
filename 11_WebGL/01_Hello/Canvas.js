// Hello World

//Onload : Our Main()
/*
5 Steps : 

*/

function main()
{
    //1. get Canvas
    var canvas = document.getElementById("AMC");
    if(!canvas)
    {
        console.log("Obtaining Canvas Failed \n");
    }

    else 
        console.log("Obtaining Canvas Succed \n");

    // 2. Print Dims
    console.log("Canvas Width :  "+canvas.width+ " Canvas Height : "+canvas.height);

    // 3. Get 2D Context
    var context = canvas.getContext("2d"); // for Drawing Text
    if(!context)
    {
        console.log("Obtaining 2d Context Failed \n");
    }
    else
        console.log("Obtaining 2d Context Done \n");

    // 4. Fill Color
    context.fillStyle = "black";
    context.fillRect(0,0,canvas.width,canvas.height);

    //5. Center the Text
    context.textAlign = "center"; // Horizontal
    context.textBaseline = "middle"; // Vertical

    //6. Define String
    var str = "Hello World !!!";
    context.font = "48px sans-serif"; //font
    context.fillStyle = "white"; // Color

    //Display
    context.fillText(str,canvas.width/2,canvas.height/2);


}