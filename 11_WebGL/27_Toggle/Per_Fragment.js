// Lights

//Globals
var canvas = null;
var context = null;
var gl = null;
var bFullscreen = null;
var canvas_original_width = null;
var canvas_original_height = null;
var angle = 0.0;
var perFragmentLighting = false

//Onload : Our Main()
/*
7 Steps : 
    RequestAnimationFrame
    CancelAnimationFrame
    Macros 
    Shaders
    VAO, VBO
    Uninitialize
*/

var RequestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || 
                            window.mozRequestAnimationFrame ||  window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var CancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame || 
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

const WebGLMacros = 
{
    VDG_ATTRIBUTE_VERTEX:0,
    VDG_ATTRIBUTE_COLOR:1,
    VDG_ATTRIBUTE_NORMAL:2,
    VDG_ATTRIBUTE_TEXTURE0:3,
};                           


//F
var vertexShaderObject_YSR ;
var fragmentShaderObject_YSR ;
var shaderProgramObject_YSR;

//V
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject_YSR_V;

var sphere_YSR = null;

//toggle
var bisLPressed_YSR = true;

var isCPressed = false;
var isVPressed = false;

var PerspectiveProjectionMatrix_YSR;

//12
var mvUniform_YSR;
var Projection_Uniform_YSR;
var View_Uniform_YSR;

var LD_Uniform_YSR;
var KD_Uniform_YSR;

var LA_Uniform_YSR;
var KA_Uniform_YSR;

var LS_Uniform_YSR;
var KS_Uniform_YSR;

var Material_Shininess_YSR;
var Light_Position_Uniform_YSR;
var LisPressed_Uniform_YSR;

//Vertex
var mvUniform;
var Projection_Uniform;
var View_Uniform;

var LD_Uniform;
var KD_Uniform;

var LA_Uniform;
var KA_Uniform;

var LS_Uniform;
var KS_Uniform;

var Material_Shininess;
var Light_Position_Uniform;
var LisPressed_Uniform;

//imp
var Light_Ambient_YSR = [0.0,0.0,0.0];
var Material_Ambient_YSR = [0.20,0.20,0.20];

var Light_Diffuse_YSR = [1.0,1.0,1.0];
var Material_Diffuse_YSR = [1.0,1.0,1.0];

var Material_Specular_YSR = [1.0,1.0,1.0];
var Light_Specular_YSR = [1.0,1.0,1.0];

var shine_YSR = 128.0;   //try
var LightPosition_YSR = [100.0,100.0,100.0,1.0];

//main
function main()
{
    //1. get Canvas
    canvas = document.getElementById("AMC");
    if(!canvas)
    {
        console.log("Obtaining Canvas Failed \n");
    }

    else 
        console.log("Obtaining Canvas Succeed \n");

    // 2. Print Dims
    console.log("Canvas Width :  "+canvas.width+ " Canvas Height : "+canvas.height);

    canvas_original_height = canvas.height;
    canvas_original_width = canvas.width;

    // 7. Register Event Handlers
    window.addEventListener("keydown",keyDown,false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",Resize,false);

    //As a Game Loop
    Init();

    Resize();

    draw();
}

// KeyBoard
    function keyDown(event)
    {
        switch(event.keyCode)
        {
            case 70: //f
                ToggleFullScreen();
                break;
            
            case 76:
                if(bisLPressed_YSR == false)
                  bisLPressed_YSR = true;
                else 
                  bisLPressed_YSR = false;
                break; 
               
            case  67: //c 
                    isCPressed = true;
                    isVPressed = false;
                break;   
        
                
            case 86://v
                isVPressed = true;
                isCPressed = false;
            break;
        }

    }


// Mouse
    function mouseDown()
    {
        
    }

// ToggleFullScreen
function ToggleFullScreen()
{
    //code
    var fullscreen_Element = document.fullscreenElement || document.webkitFullscreenElement ||
                             document.mozFullScreenElement || document.msFullscreenElement || null;

    //if not
    if(fullscreen_Element == null)
    {
        if(canvas.requestFullscreen)
         canvas.requestFullscreen();

        else if(canvas.mozRequestFullscreen)
         canvas.mozRequestFullscreen();

        else if(canvas.webkitRequestFullscreen)
         canvas.webkitRequestFullscreen();

        else if(canvas.msRequestFullscreen)
        canvas.msRequestFullscreen();

        bFullscreen = true;
    }

    else // if already
    {
        if(document.exitFullscreen)
         document.exitFullscreen();

        else if(document.mozCancelFullScreen) 
         document.mozCancelFullScreen();

        else if(document.webkitExitFullscreen)
         document.webkitExitFullscreen();

        else if(document.msExitFullscreen) 
         document.msExitFullscreen();
         
         bFullscreen = false;

    }

}

//Init
function Init()
{
    //get WebGL Context
    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
        console.log("Failed to get RC \n");
        return;
    }

    gl.viewportwidth = canvas.width;
    gl.viewportheight = canvas.height;

    //Shaders
    // Vertex : 2 Attrib , 12 Uniform +         

    
    PerVertex();

    PerFrag();

// Vertices , Colors , Vao & Vbo
    sphere_YSR = new Mesh();
    makeSphere(sphere_YSR,2.0,30,30);

   //final
   gl.clearDepth(1.0);
   gl.enable(gl.DEPTH_TEST);
   gl.depthFunc(gl.LEQUAL);

   gl.clearColor(0.0,0.0,0.0,1.0); 
   PerspectiveProjectionMatrix_YSR = mat4.create();
}

function PerVertex()
{
    console.log("in Per Vertex \n");
  

    var vertexShaderSourceCode = 
    "#version 300 es" +
    "\n" +
    "precision highp float;"					+
    "in vec4 vPosition;"						+
    "in vec3 vNormal;"							+
    "out vec3 Phong_ADS_Light;"					+
    "\n"										+
    "uniform mat4 u_model_matrix;"				+
    "uniform mat4 u_view_matrix;"				+
    "uniform mat4 u_projection_matrix;"			+
    "uniform vec3 u_LA;"						+
    "uniform vec3 u_LD;"						+
    "uniform vec3 u_LS;"						+
    "uniform vec3 u_KA;"						+
    "uniform vec3 u_KD;"						+
    "uniform vec3 u_KS;"						+
    "uniform float u_Shininess;"				+
    "uniform vec4 u_Light_Position;"			+
    "uniform mediump int u_LKeyPressed;"		+
    "\n"										+
    "void main(void)"							+
    "{ \n"										+
    "if(u_LKeyPressed == 1)"					+
    "{"											+
        "vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition); \n"		+
        "vec3 T_Norm = normalize(mat3( u_view_matrix * u_model_matrix)* vNormal); \n"	+
        "vec3 Light_Direction = normalize(vec3(u_Light_Position) - Eye_Coordinates.xyz); \n"	+
        "float TN_Dot_LD = max(dot(T_Norm,Light_Direction),0.0);  \n"+
        "vec3 Reflecion_Vector = reflect(-Light_Direction, T_Norm); \n "+
        "vec3 Viewer_Vector = normalize(vec3(-Eye_Coordinates.xyz));  \n"		+
        "vec3 Ambient = vec3(u_LA * u_KA); \n"			    +
        "vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD); \n" +
        "vec3 Specular = (u_LS * u_KS * pow(max(dot(Reflecion_Vector, Viewer_Vector), 0.0), u_Shininess)); \n" +
        "Phong_ADS_Light = Ambient + Diffuse + Specular;  \n" +
    "}"+
    "else"+
    "{"+
    "	Phong_ADS_Light = vec3(1.0,1.0,1.0);  \n"	+
    "}"+
    "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition; \n"	+
    "}" ;


//"Phong_ADS_Light = Ambient + Diffuse + Specular;  \n" +

vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
gl.compileShader(vertexShaderObject);

if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS) == false)
{
  var glerror = gl.getShaderInfoLog(vertexShaderObject);

  if(glerror.length > 0)
  {
      alert(glerror);
      uninitialize();
  }
}

// Fragment : Error will occured for commanly used uniform - Keypress
var fragmentShaderSourceCode =
"#version 300 es"+
"\n"+
"precision highp float;"    +
"in vec3 Phong_ADS_Light;"	+
"out vec4 FragColor;"		+
"void main(void)"			+
"{ \n"							+
"FragColor = vec4(Phong_ADS_Light,1.0);"	+
"}" ;


fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
gl.compileShader(fragmentShaderObject);

if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS) == false)
{
var glerror = gl.getShaderInfoLog(fragmentShaderObject);

if(glerror.length >0)
{
 alert(glerror);
 uninitialize();
}
}

// Shader Program
shaderProgramObject_YSR_V = gl.createProgram();
gl.attachShader(shaderProgramObject_YSR_V,vertexShaderObject);
gl.attachShader(shaderProgramObject_YSR_V,fragmentShaderObject);

//pre link  : Attribs
gl.bindAttribLocation(shaderProgramObject_YSR_V,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
gl.bindAttribLocation(shaderProgramObject_YSR_V,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

//link
gl.linkProgram(shaderProgramObject_YSR_V);

if(!gl.getProgramParameter(shaderProgramObject_YSR_V,gl.LINK_STATUS))
{
var glerror = gl.getProgramInfoLog(shaderProgramObject_YSR_V);

if(glerror.length > 0)
{
    alert(glerror);
    uninitialize();
}
}

//get Uniform Location

mvUniform = gl.getUniformLocation(shaderProgramObject_YSR_V,"u_model_matrix");

View_Uniform = gl.getUniformLocation(shaderProgramObject_YSR_V, "u_view_matrix");

Projection_Uniform = gl.getUniformLocation(shaderProgramObject_YSR_V, "u_projection_matrix");

Light_Position_Uniform = gl.getUniformLocation(shaderProgramObject_YSR_V, "u_Light_Position");

Material_Shininess = gl.getUniformLocation(shaderProgramObject_YSR_V, "u_Shininess");

LisPressed_Uniform = gl.getUniformLocation(shaderProgramObject_YSR_V, "u_LKeyPressed");


//imp
KD_Uniform = gl.getUniformLocation(shaderProgramObject_YSR_V, "u_KD");
LD_Uniform = gl.getUniformLocation(shaderProgramObject_YSR_V, "u_LD");

LA_Uniform = gl.getUniformLocation(shaderProgramObject_YSR_V, "u_LA");
KA_Uniform = gl.getUniformLocation(shaderProgramObject_YSR_V, "u_KA");

LS_Uniform = gl.getUniformLocation(shaderProgramObject_YSR_V, "u_LS");
KS_Uniform = gl.getUniformLocation(shaderProgramObject_YSR_V, "u_KS");

}


function PerFrag()
{
    console.log("in Per Frag \n");

    var vertexShaderSourceCode_YSR = 
    "#version 300 es" +
    "\n" +
    "precision highp float;"					+
    "in vec4 vPosition;"						+
    "in vec3 vNormal;"							+
    "out vec3 T_Norm;"							+
    "out vec3 Light_Direction;"					+
    "out vec3 Viewer_Vector;"					+
    "\n"										+ 
    "uniform mat4 u_model_matrix;"				+ 
    "uniform mat4 u_view_matrix;"				+ 
    "uniform mat4 u_projection_matrix;"			+ 
    "uniform mediump int u_LKeyPressed;"		+
    "uniform vec4 u_Light_Position;"			+
    "\n"										+
    "void main(void)"							+
    "{"											+
    "if(u_LKeyPressed == 1)"					+
    "{"											+
    " vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"	+ 
    " T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"			    + 
    " Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"    +    
    " Viewer_Vector = vec3(-Eye_Coordinates.xyz);"                          +   
    "}"		+ 
    "gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"	+ 
    "}";


//"Phong_ADS_Light = Ambient + Diffuse + Specular;  \n" +

vertexShaderObject_YSR = gl.createShader(gl.VERTEX_SHADER);
gl.shaderSource(vertexShaderObject_YSR, vertexShaderSourceCode_YSR);
gl.compileShader(vertexShaderObject_YSR);

if(gl.getShaderParameter(vertexShaderObject_YSR,gl.COMPILE_STATUS) == false)
{
  var glerror = gl.getShaderInfoLog(vertexShaderObject_YSR);

  if(glerror.length > 0)
  {
      alert(glerror);
      uninitialize();
  }
}

// Fragment : Error will occured for commanly used uniform - Keypress
var fragmentShaderSourceCode_YSR =
    "#version 300 es"+
    "\n"+
    "precision highp float;"                    +
    "in vec3 T_Norm;"			                +
    "in vec3 Light_Direction;"	                +
    "in vec3 Viewer_Vector;"	                +
    "out vec4 FragColor;"		                +
    "\n"						                +
    "uniform vec3 u_LA;"						+
    "uniform vec3 u_LD;"						+
    "uniform vec3 u_LS;"						+
    "uniform vec3 u_KA;"						+
    "uniform vec3 u_KD;"						+
    "uniform vec3 u_KS;"						+
    "uniform float u_Shininess;"				+
    "uniform int u_LKeyPressed;"				+
    "uniform vec4 u_Light_Position;"			+
    "\n"										+
    "void main(void) "		                    +
    "{"						                    +
    "if(u_LKeyPressed == 1)"                    +
    "{"						                    +
      "vec3 Normalized_View_Vector = normalize(Viewer_Vector); \n"	+
      "vec3 Normalized_Light_Direction = normalize(Light_Direction); \n"+
      "vec3 Normalized_TNorm = normalize(T_Norm); \n "+
      "float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0); \n "	+
      "vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm); \n "+
      "vec3 Ambient = vec3(u_LA * u_KA); \n "	+
      "vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD); \n "	+
      "vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess)); \n " +
      "vec3 Phong_ADS_Light = Ambient + Diffuse+ Specular; \n"	+
      "FragColor = vec4(Phong_ADS_Light,1.0); \n " +
    "}"		+
    "else"	+
    "{"		+
    " FragColor = vec4(1.0,1.0,1.0,1.0);" +
    "}"+
  "}" ;


fragmentShaderObject_YSR = gl.createShader(gl.FRAGMENT_SHADER);
gl.shaderSource(fragmentShaderObject_YSR, fragmentShaderSourceCode_YSR);
gl.compileShader(fragmentShaderObject_YSR);

if(gl.getShaderParameter(fragmentShaderObject_YSR,gl.COMPILE_STATUS) == false)
{
var glerror = gl.getShaderInfoLog(fragmentShaderObject_YSR);

if(glerror.length >0)
{
 alert(glerror);
 uninitialize();
}
}

// Shader Program
shaderProgramObject_YSR = gl.createProgram();
gl.attachShader(shaderProgramObject_YSR,vertexShaderObject_YSR);
gl.attachShader(shaderProgramObject_YSR,fragmentShaderObject_YSR);

//pre link  : Attribs
gl.bindAttribLocation(shaderProgramObject_YSR,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
gl.bindAttribLocation(shaderProgramObject_YSR,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

//link
gl.linkProgram(shaderProgramObject_YSR);

if(!gl.getProgramParameter(shaderProgramObject_YSR,gl.LINK_STATUS))
{
var glerror = gl.getProgramInfoLog(shaderProgramObject_YSR);

if(glerror.length > 0)
{
    alert(glerror);
    uninitialize();
}
}

//get Uniform Location

mvUniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR,"u_model_matrix");

View_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_view_matrix");

Projection_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_projection_matrix");

Light_Position_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_Light_Position");

LisPressed_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LKeyPressed");

KD_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_KD");
LD_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LD");

LA_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LA");
KA_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_KA");

LS_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LS");
KS_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_KS");

Material_Shininess_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_Shininess");

} 



//Resize
function Resize()
{
    if(bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else 
    {
        canvas.height = canvas_original_height;
        canvas.width = canvas_original_width;
    }

    //set ViewPort
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(PerspectiveProjectionMatrix_YSR,45.0,
        (parseFloat (canvas.width) / parseFloat(canvas.height)),
         0.1,100.0);
}


//Draw
function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //imp 
    if(isVPressed == true)
      gl.useProgram(shaderProgramObject_YSR_V); //Vertex

    else
      gl.useProgram(shaderProgramObject_YSR); //Frag


    //I[]
    var Model_Matrix_YSR = mat4.create();
    var View_Matrix_YSR = mat4.create();
    
    //PerspectiveProjectionMatrix_YSR

            //Tranform
    mat4.translate(Model_Matrix_YSR,Model_Matrix_YSR,[0.0,0.0,-7.0]);
    
    //model
    gl.uniformMatrix4fv(mvUniform_YSR,false, Model_Matrix_YSR); 

    //view
    gl.uniformMatrix4fv(View_Uniform_YSR, false, View_Matrix_YSR);

    //Projection
    gl.uniformMatrix4fv(Projection_Uniform_YSR, false, PerspectiveProjectionMatrix_YSR);

    //Lights
	if (bisLPressed_YSR == true)
	{

     //C
    if(isCPressed == true)
      {
        gl.uniform1i(LisPressed_Uniform_YSR, 1);

		gl.uniform3fv(LD_Uniform_YSR, Light_Diffuse_YSR);
		gl.uniform3fv(KD_Uniform_YSR, Material_Diffuse_YSR);

        gl.uniform3fv(LS_Uniform_YSR, Light_Specular_YSR);
		gl.uniform3fv(KS_Uniform_YSR, Material_Specular_YSR);
		
		gl.uniform3fv(LA_Uniform_YSR, Light_Ambient_YSR);
        gl.uniform3fv(KA_Uniform_YSR, Material_Ambient_YSR);
                
        gl.uniform1f(Material_Shininess_YSR, shine_YSR);
		
        gl.uniform4fv(Light_Position_Uniform_YSR,LightPosition_YSR);
        
        //model
        gl.uniformMatrix4fv(mvUniform_YSR,false, Model_Matrix_YSR); 

        //view
        gl.uniformMatrix4fv(View_Uniform_YSR, false, View_Matrix_YSR);

        //Projection
        gl.uniformMatrix4fv(Projection_Uniform_YSR, false, PerspectiveProjectionMatrix_YSR);
	
      }

      //V
      else if(isVPressed == true)
        {
          gl.uniform1i(LisPressed_Uniform, 1);

          gl.uniform3fv(LD_Uniform, Light_Diffuse_YSR);
          gl.uniform3fv(KD_Uniform, Material_Diffuse_YSR);
  
          gl.uniform3fv(LS_Uniform, Light_Specular_YSR);
          gl.uniform3fv(KS_Uniform, Material_Specular_YSR);
          
          gl.uniform3fv(LA_Uniform, Light_Ambient_YSR);

          gl.uniform3fv(KA_Uniform, Material_Ambient_YSR);

          gl.uniform1f(Material_Shininess, shine_YSR);
		
          gl.uniform4fv(Light_Position_Uniform,LightPosition_YSR);
          

          //model
         gl.uniformMatrix4fv(mvUniform,false, Model_Matrix_YSR); //tried modelMatrix

        //view
         gl.uniformMatrix4fv(View_Uniform, false, View_Matrix_YSR);

        //Projection
         gl.uniformMatrix4fv(Projection_Uniform, false, PerspectiveProjectionMatrix_YSR);

        }

	}

	else
	{
		gl.uniform1i(LisPressed_Uniform_YSR, 0);
	}

   
    sphere_YSR.draw();
	
    gl.useProgram(null);

    //Show
    requestAnimationFrame(draw,canvas);
}

//Degree
function degToRad(degrees)
{
    return(degrees*Math.PI/180);
}


//Uninitialize
function uninitialize()
{
    
    if(sphere_YSR)
    {
        sphere_YSR.deallocate();
        sphere_YSR = null;
    }

    fragmentShaderObject_YSR = null;
    vertexShaderObject_YSR = null;
    shaderProgramObject_YSR = null;
    shaderProgramObject_YSR_V = null;
}

