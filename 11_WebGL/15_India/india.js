// perspective

//Globals
var canvas = null;
var context = null;
var gl = null;
var bFullscreen = null;
var canvas_original_width = null;
var canvas_original_height = null;

var CHECK_IMG_WIDTH = 64;
var CHECK_IMG_HEIGHT = 64;


//Onload : Our Main()
/*
7 Steps : 
    RequestAnimationFrame
    CancelAnimationFrame
    Macros 
    Shaders
    VAO, VBO
    Uninitialize
*/

var RequestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || 
                            window.mozRequestAnimationFrame ||  window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var CancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame || 
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

const WebGLMacros = 
{
    VDG_ATTRIBUTE_VERTEX:0,
    VDG_ATTRIBUTE_COLOR:1,
    VDG_ATTRIBUTE_NORMAL:2,
    VDG_ATTRIBUTE_TEXTURE0:3,
};                           


//new
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_I,vbo_IP,vbo_IC;
var vao_N,vbo_NP,vbo_NC;
var vao_D,vbo_DP,vbo_DC;
var vao_A,vbo_AP,vbo_AC;

var mvpUniform;

var PerspectiveProjectionMatrix;

//main
function main()
{
    //1. get Canvas
    canvas = document.getElementById("AMC");
    if(!canvas)
    {
        console.log("Obtaining Canvas Failed \n");
    }

    else 
        console.log("Obtaining Canvas Succeed \n");

    // 2. Print Dims
    console.log("Canvas Width :  "+canvas.width+ " Canvas Height : "+canvas.height);

    canvas_original_height = canvas.height;
    canvas_original_width = canvas.width;

    // 7. Register Event Handlers
    window.addEventListener("keydown",keyDown,false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",Resize,false);

    //As a Game Loop
    Init();

    Resize();

    draw();
}

// KeyBoard
    function keyDown(event)
    {
        switch(event.keyCode)
        {
            case 70:
                ToggleFullScreen();
                break;
         }
    }


// Mouse
    function mouseDown()
    {
        
    }

// ToggleFullScreen
function ToggleFullScreen()
{
    //code
    var fullscreen_Element = document.fullscreenElement || document.webkitFullscreenElement ||
                             document.mozFullScreenElement || document.msFullscreenElement || null;

    //if not
    if(fullscreen_Element == null)
    {
        if(canvas.requestFullscreen)
         canvas.requestFullscreen();

        else if(canvas.mozRequestFullscreen)
         canvas.mozRequestFullscreen();

        else if(canvas.webkitRequestFullscreen)
         canvas.webkitRequestFullscreen();

        else if(canvas.msRequestFullscreen)
        canvas.msRequestFullscreen();

        bFullscreen = true;
    }

    else // if already
    {
        if(document.exitFullscreen)
         document.exitFullscreen();

        else if(document.mozCancelFullScreen) 
         document.mozCancelFullScreen();

        else if(document.webkitExitFullscreen)
         document.webkitExitFullscreen();

        else if(document.msExitFullscreen) 
         document.msExitFullscreen();
         
         bFullscreen = false;

    }

}

//Init
function Init()
{
	CheckImage= new Uint8Array(64*64*4);
	
    //get WebGL Context
    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
        console.log("Failed to get RC \n");
        return;
    }

    gl.viewportwidth = canvas.width;
    gl.viewportheight = canvas.height;

    //Shaders
    // Vertex
    var vertexShaderSourceCode = 
    "#version 300 es" +
    "\n" +
	"in vec4 vPosition;"						+
	"in vec4 vColor;"							+
	"out vec4 Out_Color;"						+
	"uniform mat4 u_mvp_matrix;"				+
	"void main(void)"							+
	"{"											+
	"gl_Position = u_mvp_matrix * vPosition;"	+
	"Out_Color = vColor;"						+
	"}" ;

   vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
   gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
   gl.compileShader(vertexShaderObject);

   if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS) == false)
   {
      var glerror = gl.getShaderInfoLog(vertexShaderObject);

      if(glerror.length > 0)
      {
          alert(glerror);
          uninitialize();
      }
   }

    // Fragment 
   var fragmentShaderSourceCode =
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
	"in vec4 Out_Color;"	 +
	"out vec4 FragColor;"    +
	"void main(void)"        +
	"{"                      +
	"FragColor = Out_Color;" +
	"}";
   
 fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
 gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
 gl.compileShader(fragmentShaderObject);

 if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS) == false)
  {
    var glerror = gl.getShaderInfoLog(fragmentShaderObject);

    if(glerror.length > 0)
    {
     alert(glerror);
     uninitialize();
    }
  }

 // Shader Program
  shaderProgramObject = gl.createProgram();
  gl.attachShader(shaderProgramObject,vertexShaderObject);
  gl.attachShader(shaderProgramObject,fragmentShaderObject);

  //pre link
   gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

   gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_COLOR,"vColor");
   
   //link
   gl.linkProgram(shaderProgramObject);
   
   if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
   {
    var glerror = gl.getProgramInfoLog(shaderProgramObject);

    if(glerror.length > 0)
    {
        alert(glerror);
        uninitialize();
    }
   }

   //get Uniform Location
   mvpUniform = gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");


   //Vertices , Colors , Vao & Vbo
 // I   
   var I_Vertices = new Float32Array
   ([       
		0.05 , 0.5 , 0.0 ,
		-0.05 , 0.5 , 0.0 ,
		-0.05 , -0.5 , 0.0 ,
		0.05 , -0.5 , 0.0 
    ]);
	
	var I_Colors = new Float32Array
   ([       
        1.0 ,0.5 ,0.0 ,
        1.0 ,1.0 ,1.0 ,
        1.0 ,1.0 ,1.0 ,
        0.0 ,1.0 ,0.0 
    ]);
	
   //vao
   vao_I = gl.createVertexArray();
   gl.bindVertexArray(vao_I);
 
 //vbo_position
   vbo_IP = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo_IP);
   gl.bufferData(gl.ARRAY_BUFFER,I_Vertices,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 
                            3, // x,y,z
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);


  //vbo_Color
   	vbo_IC = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_IC);
	gl.bufferData(gl.ARRAY_BUFFER,I_Colors , gl.DYNAMIC_DRAW);

   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR, 
                            3, //
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);

    //Unbind vbo
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
	//vao
	gl.bindVertexArray(null);

// N
   
   var N_Vertices = new Float32Array
   ([       
	            //I
		 0.3 , 0.5 , 0.0 ,
		 0.2 , 0.5 , 0.0 ,
		 0.2 , -0.5 , 0.0 ,
		 0.3 , -0.5 , 0.0 ,

		 -0.2 , 0.5 , 0.0 ,
		 -0.3 , 0.5 , 0.0 ,
		 -0.3 , -0.5 , 0.0 ,
		 -0.2 , -0.5 , 0.0 ,

		//I
		 -0.1 , 0.5 , 0.0 ,
		 -0.22 , 0.5 , 0.0 ,
		 0.1 , -0.5 , 0.0 ,
		 0.22 , -0.5 , 0.0 

    ]);
	
	var N_Colors = new Float32Array
   ([       
        1.0 ,0.5 ,0.0 ,
		1.0 ,1.0 ,1.0 ,
		1.0 ,1.0 ,1.0 ,
		0.0 ,1.0 ,0.0 ,

		1.0 ,0.5 ,0.0 ,
		1.0 ,1.0 ,1.0 ,
		1.0 ,1.0 ,1.0 ,
		0.0 ,1.0 ,0.0 ,

		1.0 ,0.5 ,0.0 ,
		1.0 ,1.0 ,1.0 ,
		1.0 ,1.0 ,1.0 ,
		0.0 ,1.0 ,0.0  

    ]);
	
   //vao
   vao_N = gl.createVertexArray();
   gl.bindVertexArray(vao_N);
 
 //vbo_position
   vbo_NP = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo_NP);
   gl.bufferData(gl.ARRAY_BUFFER,N_Vertices,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 
                            3, // x,y,z
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);


  //vbo_Color
   	vbo_NC = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_IC);
	gl.bufferData(gl.ARRAY_BUFFER,N_Colors , gl.DYNAMIC_DRAW);

   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR, 
                            3, //
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);

    //Unbind vbo
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
	//vao
	gl.bindVertexArray(null);


// D
   
        /*        
		-0.3f, 0.5f, 0.0f,
		-0.2f, 0.5f, 0.0f,
		-0.2f, -0.5f, 0.0f,
		-0.3f, -0.5f, 0.0f,

		// D
		-0.2f, 0.4f, 0.0f,
		-0.2f, 0.5f, 0.0f,
		0.0f, 0.4f, 0.0f,
		0.0f, 0.5f, 0.0f,

		0.15f, 0.3f, 0.0f,
		0.25f, 0.36f, 0.0f,
		0.15f, -0.3f, 0.0f ,
		0.25f, -0.36f, 0.0f ,

		0.0f, -0.4f, 0.0f ,
		0.0f, -0.5f, 0.0f ,
		-0.2f, -0.4f, 0.0f ,
		-0.2f, -0.5f, 0.0f
 
        */

   var D_Vertices = new Float32Array
   ([       
	   	-0.3,0.5,0.0,
		-0.2,0.5,0.0,
		-0.2,-0.5,0.0,
		-0.3,-0.5,0.0,

		// D
		-0.2 ,0.4 ,0.0,
		-0.2 ,0.5 ,0.0,
		 0.0 ,0.4 ,0.0,
		 0.0 ,0.5, 0.0,

		0.15,0.3,0.0,
		0.25,0.36,0.0,
		0.15,-0.3,0.0,
		0.25,-0.36,0.0,

		0.0,-0.4,0.0,
		0.0,-0.5,0.0,
		-0.2,-0.4,0.0,
		-0.2,-0.5,0.0 

    ]);
	
	var D_Colors = new Float32Array
   ([       
        1.0 ,0.5 ,0.0,
		1.0 ,1.0 ,1.0,
		1.0 ,1.0 ,1.0,
		0.0 ,1.0 ,0.0,

		1.0 ,0.5 ,0.0,
		1.0 ,0.5 ,0.0,
		1.0 ,0.5 ,0.0,
		1.0 ,0.5 ,0.0,

		1.0 ,1.0 ,1.0,
		1.0 ,1.0 ,1.0,
		1.0 ,1.0 ,1.0,
		1.0 ,1.0 ,1.0,

		0.0 ,1.0 ,0.0,
		0.0 ,1.0 ,0.0,
		0.0 ,1.0 ,0.0,
		0.0 ,1.0 ,0.0 
    
    ]);
	
   //vao
   vao_D = gl.createVertexArray();
   gl.bindVertexArray(vao_D);
 
 //vbo_position
   vbo_DP = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo_DP);
   gl.bufferData(gl.ARRAY_BUFFER,D_Vertices,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 
                            3, // x,y,z
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);


  //vbo_Color
   	vbo_DC = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_DC);
	gl.bufferData(gl.ARRAY_BUFFER,D_Colors,gl.STATIC_DRAW);

   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR, 
                            3, //
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);

    //Unbind vbo
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
	//vao
	gl.bindVertexArray(null);

// A
   var A_Vertices = new Float32Array
   ([       
        //strips
		 1.35 , 0.3 , 0.0 ,
		 1.05 , 0.3 , 0.0 ,
	     1.05 , 0.2 , 0.0 ,
		 1.35 , 0.2 , 0.0 ,

		 //W
		1.35 , 0.26 , 0.0 ,
		1.05 , 0.26 , 0.0 ,
		1.05 , 0.24 , 0.0 ,
		1.35 , 0.24 , 0.0 ,

		// A 4
		 1.05 , 0.5 , 0.0 ,
		 0.5 ,  0.6 , 0.0 ,
	     0.5 , -0.5 , 0.0 ,
		 1.05 , -0.6 , 0.0 
    ]);
	
	var A_Colors = new Float32Array
   ([       
        1.0 ,0.5 ,0.0,
        1.0 ,0.5 ,0.0,
		0.0 ,1.0 ,0.0,
		0.0 ,1.0 ,0.0,

		1.0 ,1.0 ,1.0,
		1.0 ,1.0 ,1.0,
		1.0 ,1.0 ,1.0,
        1.0 ,1.0 ,1.0,
        
        1.0 ,0.5 ,0.0,
        1.0 ,0.5 ,0.0,
		1.0 ,1.0 ,1.0,
		0.0 ,1.0 ,0.0
    ]);
	
   //vao
   vao_A = gl.createVertexArray();
   gl.bindVertexArray(vao_A);
 
 //vbo_position
   vbo_AP = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo_AP);
   gl.bufferData(gl.ARRAY_BUFFER,A_Vertices,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 
                            3, // x,y,z
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);


  //vbo_Color
   	vbo_AC = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_AC);
	gl.bufferData(gl.ARRAY_BUFFER,A_Colors,gl.STATIC_DRAW);

   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR, 
                            3, //
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);

    //Unbind vbo
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
	//vao
	gl.bindVertexArray(null);


   gl.clearColor(0.0,0.0,0.0,1.0); 
   PerspectiveProjectionMatrix = mat4.create();
}

		

//Resize
function Resize()
{
    if(bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else 
    {
        canvas.height = canvas_original_height;
        canvas.width = canvas_original_width;
    }

    //set ViewPort
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(PerspectiveProjectionMatrix,45.0,
        (parseFloat (canvas.width) / parseFloat (canvas.height)),
         0.1,100.0);
}


//Draw
function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //Display
    gl.useProgram(shaderProgramObject);

    //I[]
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
	var translateMatrix = mat4.create();
    var RotationMatrix = mat4.create();

 // I 
    mat4.translate(translateMatrix,translateMatrix,[-2.5,0.0,-4.0]); 
    mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
    mat4.multiply(modelViewMatrix,modelViewMatrix,RotationMatrix);
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

	//uniform
	gl.uniformMatrix4fv(mvpUniform, false,modelViewProjectionMatrix);						

    gl.bindVertexArray(vao_I);			
                        
    //scene
    gl.drawArrays(gl.TRIANGLE_FAN,0,4); // index is imp 
    gl.drawArrays(gl.TRIANGLE_FAN,4,4); // index is imp 
	
    //Unbind
    gl.bindVertexArray(null);

//N
	//I[]
	modelViewMatrix = mat4.create();
	modelViewProjectionMatrix = mat4.create();
	translateMatrix = mat4.create();
	RotationMatrix = mat4.create();

    mat4.translate(translateMatrix,translateMatrix,[-1.2,0.0,-4.0]); 
    mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
    mat4.multiply(modelViewMatrix,modelViewMatrix,RotationMatrix);
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

	//uniform
	gl.uniformMatrix4fv(mvpUniform, false,modelViewProjectionMatrix);						

    gl.bindVertexArray(vao_N);			
                        
    //scene
    gl.drawArrays(gl.TRIANGLE_FAN,0,4); // index is imp 
    gl.drawArrays(gl.TRIANGLE_FAN,4,4);
    gl.drawArrays(gl.TRIANGLE_FAN,8,4);
	
    //Unbind
    gl.bindVertexArray(null);


// D
	//I[]
	modelViewMatrix = mat4.create();
	modelViewProjectionMatrix = mat4.create();
	translateMatrix = mat4.create();

    mat4.translate(translateMatrix,translateMatrix,[0.2,0.0,-4.0]); 
    mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

	//uniform
	gl.uniformMatrix4fv(mvpUniform, false,modelViewProjectionMatrix);						

    gl.bindVertexArray(vao_D);			
                        
    //scene
    gl.drawArrays(gl.TRIANGLE_FAN,0,4); 
    gl.drawArrays(gl.TRIANGLE_STRIP,4,12); 
	
    //Unbind
    gl.bindVertexArray(null);

	
// I
	//I[]
	modelViewMatrix = mat4.create();
	modelViewProjectionMatrix = mat4.create();
	translateMatrix = mat4.create();
	RotationMatrix = mat4.create();

    mat4.translate(translateMatrix,translateMatrix,[1.35,0.0,-4.0]); 
    mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
    mat4.multiply(modelViewMatrix,modelViewMatrix,RotationMatrix);
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

	//uniform
	gl.uniformMatrix4fv(mvpUniform, false,modelViewProjectionMatrix);						

    gl.bindVertexArray(vao_I);			
                        
    //scene
    gl.drawArrays(gl.TRIANGLE_FAN,0,4); // index is imp 
    gl.drawArrays(gl.TRIANGLE_FAN,4,4); // index is imp 
	
    //Unbind
    gl.bindVertexArray(null);
	
// A
	// strips
	modelViewMatrix = mat4.create();
	modelViewProjectionMatrix = mat4.create();
	translateMatrix = mat4.create();
	RotationMatrix = mat4.create();

    mat4.translate(translateMatrix,translateMatrix,[0.95,-0.2,-3.4]); 
    mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
    mat4.multiply(modelViewMatrix,modelViewMatrix,RotationMatrix);
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

	//uniform
	gl.uniformMatrix4fv(mvpUniform, false,modelViewProjectionMatrix);						

    gl.bindVertexArray(vao_A);			
                        
    //scene
    gl.drawArrays(gl.TRIANGLE_FAN,0,4);
    gl.drawArrays(gl.TRIANGLE_FAN,4,4);

    //Unbind
    gl.bindVertexArray(null);
			
// A 2
	modelViewMatrix = mat4.create();
	modelViewProjectionMatrix = mat4.create();
	translateMatrix = mat4.create();
	RotationMatrix = mat4.create();

    mat4.translate(translateMatrix,translateMatrix,[1.92,-0.4,-3.98]); 
    
    mat4.rotateX(RotationMatrix,RotationMatrix,degToRad(45.0));
    
    mat4.rotateY(RotationMatrix,RotationMatrix,degToRad(45.0));
    
    mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix); // T
    mat4.multiply(modelViewMatrix,modelViewMatrix,RotationMatrix);  // R
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

	//uniform
	gl.uniformMatrix4fv(mvpUniform, false,modelViewProjectionMatrix);						

    gl.bindVertexArray(vao_A);			
    gl.drawArrays(gl.TRIANGLE_FAN,8,12);
    gl.bindVertexArray(null);

// A 3
	modelViewMatrix = mat4.create();
	modelViewProjectionMatrix = mat4.create();
	translateMatrix = mat4.create();
	RotationMatrix = mat4.create();

    mat4.translate(translateMatrix,translateMatrix,[2.71,0.32,-4.3]); 
    
    mat4.rotateX(RotationMatrix,RotationMatrix,degToRad(325.0));
    
    mat4.rotateY(RotationMatrix,RotationMatrix,degToRad(-318.0));
    
    mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix); // T

    mat4.multiply(modelViewMatrix,modelViewMatrix,RotationMatrix);  // R

    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

	//uniform
	gl.uniformMatrix4fv(mvpUniform, false,modelViewProjectionMatrix);						

    gl.bindVertexArray(vao_A);			
                        
    //scene
    gl.drawArrays(gl.TRIANGLE_FAN,8,12);

    //Unbind
    gl.bindVertexArray(null);

    gl.useProgram(null);

    //Show
    requestAnimationFrame(draw,canvas);
}

//Degree
function degToRad(degrees)
{
    return(degrees*Math.PI/180);
}


//Uninitialize
function uninitialize()
{
    fragmentShaderObject = null;
    vertexShaderObject = null;
    shaderProgramObject = null;
}

