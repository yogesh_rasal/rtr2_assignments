// Hello World with Fullscreen

//Globals
var canvas = null;
var context = null;

//Onload : Our Main()
/*
5 Steps : 

*/

function main()
{
    //1. get Canvas
    canvas = document.getElementById("AMC");
    if(!canvas)
    {
        console.log("Obtaining Canvas Failed \n");
    }

    else 
        console.log("Obtaining Canvas Succed \n");

    // 2. Print Dims
    console.log("Canvas Width :  "+canvas.width+ " Canvas Height : "+canvas.height);

    // 3. Get 2D Context
    context = canvas.getContext("2d"); // for Drawing Text
    if(!context)
    {
        console.log("Obtaining 2d Context Failed \n");
    }
    else
        console.log("Obtaining 2d Context Done \n");

    // 4. Fill Color
    context.fillStyle = "black";
    context.fillRect(0,0,canvas.width,canvas.height);

    //5. Center the Text
    context.textAlign = "center"; // Horizontal
    context.textBaseline = "middle"; // Vertical

    //6. Define String
    var str = "Hello World !!!";
    context.font = "48px sans-serif"; //font
    context.fillStyle = "green"; // Color

    //Display
    DrawText(str);

    // 7. Register Event Handlers
    window.addEventListener("keydown",keyDown,false);
    window.addEventListener("click",mouseDown,false);

}

//DrawText
function DrawText(text)
{
    //1. Center the Text
    context.textAlign = "center"; // Horizontal
    context.textBaseline = "middle"; // Vertical

    //2. Define Style
    context.font = "48px sans-serif"; //font
    context.fillStyle = "green"; // Color

    //Display
    context.fillText(text, canvas.width / 2, canvas.height / 2);

}

// Browser Independant
function ToggleFullscreen() {
    var fullscreen_element = document.fullscreenElement ||
                              document.webkitCurrentFullScreenElement ||
                              document.mozFullScreenElement ||
                              document.msFullscreenElement ||
                              null;

    // if Not
    if (fullscreen_element == null) {
        if (canvas.requestFullScreen)
            canvas.requestFullScreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
    }
    else //already 
    {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitCancelFullScreen)
            document.webkitCancelFullScreen();
        else if (document.msExitFullScreen)
            document.msExitFullScreen();
    }

}

// KeyBoard
    function keyDown(myEvent)
    {
        switch(event.keyCode)
        {
            case 70:
                ToggleFullscreen();
                break;
        }
        
    }


// Mouse
    function mouseDown()
    {
        alert("Mouse is Clicked");
    }

