// Lights

//Globals
var canvas = null;
var context = null;
var gl = null;
var bFullscreen = null;
var canvas_original_width = null;
var canvas_original_height = null;
var angle = 0.0;

//Onload : Our Main()
/*
7 Steps : 
    RequestAnimationFrame
    CancelAnimationFrame
    Macros 
    Shaders
    VAO, VBO
    Uninitialize
*/

var RequestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || 
                            window.mozRequestAnimationFrame ||  window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var CancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame || 
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

const WebGLMacros = 
{
    VDG_ATTRIBUTE_VERTEX:0,
    VDG_ATTRIBUTE_COLOR:1,
    VDG_ATTRIBUTE_NORMAL:2,
    VDG_ATTRIBUTE_TEXTURE0:3,
};                           


//new
var vertexShaderObject_YSR ;
var fragmentShaderObject_YSR ;
var shaderProgramObject_YSR;

var vbo_position_YSR;
var sphere_YSR = null;

//toggle
var bisLPressed_YSR = false;

var PerspectiveProjectionMatrix_YSR;

//12
var mvUniform_YSR;
var Projection_Uniform_YSR;
var View_Uniform_YSR;

var LD_Uniform_YSR;
var KD_Uniform_YSR;

var LA_Uniform_YSR;
var KA_Uniform_YSR;

var LS_Uniform_YSR;
var KS_Uniform_YSR;
var Material_Shininess_YSR;

var Light_Position_Uniform_YSR;
var LisPressed_Uniform_YSR;

//imp
var Light_Ambient_YSR = [0.20,0.20,0.20];
var Material_Ambient_YSR = [0.20,0.20,0.20];

var Light_Diffuse_YSR = [1.0,1.0,1.0];
var Material_Diffuse_YSR = [1.0,1.0,1.0];

var Material_Specular_YSR = [1.0,1.0,1.0];
var Light_Specular_YSR = [1.0,1.0,1.0];

var shine_YSR = 128.0;   //try
var LightPosition_YSR = [100.0,100.0,100.0,1.0];

//main
function main()
{
    //1. get Canvas
    canvas = document.getElementById("AMC");
    if(!canvas)
    {
        console.log("Obtaining Canvas Failed \n");
    }

    else 
        console.log("Obtaining Canvas Succeed \n");

    // 2. Print Dims
    console.log("Canvas Width :  "+canvas.width+ " Canvas Height : "+canvas.height);

    canvas_original_height = canvas.height;
    canvas_original_width = canvas.width;

    // 7. Register Event Handlers
    window.addEventListener("keydown",keyDown,false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",Resize,false);

    //As a Game Loop
    Init();

    Resize();

    draw();
}

// KeyBoard
    function keyDown(event)
    {
        switch(event.keyCode)
        {
            case 70:
                ToggleFullScreen();
                break;
            
            case 76:
                if(bisLPressed_YSR == false)
                  bisLPressed_YSR = true;
                else 
                  bisLPressed_YSR = false;
                break; 
        
        }

    }


// Mouse
    function mouseDown()
    {
        
    }

// ToggleFullScreen
function ToggleFullScreen()
{
    //code
    var fullscreen_Element = document.fullscreenElement || document.webkitFullscreenElement ||
                             document.mozFullScreenElement || document.msFullscreenElement || null;

    //if not
    if(fullscreen_Element == null)
    {
        if(canvas.requestFullscreen)
         canvas.requestFullscreen();

        else if(canvas.mozRequestFullscreen)
         canvas.mozRequestFullscreen();

        else if(canvas.webkitRequestFullscreen)
         canvas.webkitRequestFullscreen();

        else if(canvas.msRequestFullscreen)
        canvas.msRequestFullscreen();

        bFullscreen = true;
    }

    else // if already
    {
        if(document.exitFullscreen)
         document.exitFullscreen();

        else if(document.mozCancelFullScreen) 
         document.mozCancelFullScreen();

        else if(document.webkitExitFullscreen)
         document.webkitExitFullscreen();

        else if(document.msExitFullscreen) 
         document.msExitFullscreen();
         
         bFullscreen = false;

    }

}

//Init
function Init()
{
    //get WebGL Context
    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
        console.log("Failed to get RC \n");
        return;
    }

    gl.viewportwidth = canvas.width;
    gl.viewportheight = canvas.height;

    //Shaders
    // Vertex : 2 Attrib , 12 Uniform +         

    var vertexShaderSourceCode_YSR = 
        "#version 300 es" +
        "\n" +
        "precision highp float;"					+
        "in vec4 vPosition;"						+
		"in vec3 vNormal;"							+
		"out vec3 T_Norm;"							+
		"out vec3 Light_Direction;"					+
		"out vec3 Viewer_Vector;"					+
		"\n"										+ 
		"uniform mat4 u_model_matrix;"				+ 
		"uniform mat4 u_view_matrix;"				+ 
		"uniform mat4 u_projection_matrix;"			+ 
		"uniform mediump int u_LKeyPressed;"		+
		"uniform vec4 u_Light_Position;"			+
		"\n"										+
		"void main(void)"							+
		"{"											+
		"if(u_LKeyPressed == 1)"					+
		"{"											+
		" vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"	+ 
		" T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"			    + 
		" Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"    +    
		" Viewer_Vector = vec3(-Eye_Coordinates.xyz);"                          +   
		"}"		+ 
		"gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"	+ 
		"}";


//"Phong_ADS_Light = Ambient + Diffuse + Specular;  \n" +

   vertexShaderObject_YSR = gl.createShader(gl.VERTEX_SHADER);
   gl.shaderSource(vertexShaderObject_YSR, vertexShaderSourceCode_YSR);
   gl.compileShader(vertexShaderObject_YSR);

   if(gl.getShaderParameter(vertexShaderObject_YSR,gl.COMPILE_STATUS) == false)
   {
      var glerror = gl.getShaderInfoLog(vertexShaderObject_YSR);

      if(glerror.length > 0)
      {
          alert(glerror);
          uninitialize();
      }
   }

    // Fragment : Error will occured for commanly used uniform - Keypress
   var fragmentShaderSourceCode_YSR =
        "#version 300 es"+
        "\n"+
        "precision highp float;"                    +
		"in vec3 T_Norm;"			                +
		"in vec3 Light_Direction;"	                +
		"in vec3 Viewer_Vector;"	                +
		"out vec4 FragColor;"		                +
		"\n"						                +
		"uniform vec3 u_LA;"						+
		"uniform vec3 u_LD;"						+
		"uniform vec3 u_LS;"						+
		"uniform vec3 u_KA;"						+
		"uniform vec3 u_KD;"						+
		"uniform vec3 u_KS;"						+
		"uniform float u_Shininess;"				+
		"uniform int u_LKeyPressed;"				+
		"uniform vec4 u_Light_Position;"			+
		"\n"										+
		"void main(void) "		                    +
		"{"						                    +
		"if(u_LKeyPressed == 1)"                    +
	    "{"						                    +
		  "vec3 Normalized_View_Vector = normalize(Viewer_Vector); \n"	+
		  "vec3 Normalized_Light_Direction = normalize(Light_Direction); \n"+
		  "vec3 Normalized_TNorm = normalize(T_Norm); \n "+
		  "float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0); \n "	+
		  "vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm); \n "+
		  "vec3 Ambient = vec3(u_LA * u_KA); \n "	+
		  "vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD); \n "	+
		  "vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess)); \n " +
		  "vec3 Phong_ADS_Light = Ambient + Diffuse+ Specular; \n"	+
		  "FragColor = vec4(Phong_ADS_Light,1.0); \n " +
	    "}"		+
		"else"	+
		"{"		+
		" FragColor = vec4(1.0,1.0,1.0,1.0);" +
		"}"+
	  "}" ;


    fragmentShaderObject_YSR = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_YSR, fragmentShaderSourceCode_YSR);
    gl.compileShader(fragmentShaderObject_YSR);

  if(gl.getShaderParameter(fragmentShaderObject_YSR,gl.COMPILE_STATUS) == false)
   {
    var glerror = gl.getShaderInfoLog(fragmentShaderObject_YSR);

    if(glerror.length >0)
    {
     alert(glerror);
     uninitialize();
    }
   }

 // Shader Program
  shaderProgramObject_YSR = gl.createProgram();
  gl.attachShader(shaderProgramObject_YSR,vertexShaderObject_YSR);
  gl.attachShader(shaderProgramObject_YSR,fragmentShaderObject_YSR);

  //pre link  : Attribs
  gl.bindAttribLocation(shaderProgramObject_YSR,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
  gl.bindAttribLocation(shaderProgramObject_YSR,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");
 
   //link
   gl.linkProgram(shaderProgramObject_YSR);
   
   if(!gl.getProgramParameter(shaderProgramObject_YSR,gl.LINK_STATUS))
   {
    var glerror = gl.getProgramInfoLog(shaderProgramObject_YSR);

    if(glerror.length > 0)
    {
        alert(glerror);
        uninitialize();
    }
   }

//get Uniform Location

    mvUniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR,"u_model_matrix");

	View_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_view_matrix");
	
    Projection_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_projection_matrix");

    Light_Position_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_Light_Position");

    LisPressed_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LKeyPressed");

    KD_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_KD");
    LD_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LD");
   
    LA_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LA");
    KA_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_KA");

    LS_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LS");
    KS_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_KS");

    Material_Shininess_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_Shininess");
	

// Vertices , Colors , Vao & Vbo
    sphere_YSR = new Mesh();
    makeSphere(sphere_YSR,2.0,30,30);

   //final
   gl.clearDepth(1.0);
   gl.enable(gl.DEPTH_TEST);
   gl.depthFunc(gl.LEQUAL);

   gl.clearColor(0.0,0.0,0.0,1.0); 
   PerspectiveProjectionMatrix_YSR = mat4.create();
}


//Resize
function Resize()
{
    if(bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else 
    {
        canvas.height = canvas_original_height;
        canvas.width = canvas_original_width;
    }

    //set ViewPort
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(PerspectiveProjectionMatrix_YSR,45.0,
        (parseFloat (canvas.width) / parseFloat(canvas.height)),
         0.1,100.0);
}


//Draw
function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //Display
    gl.useProgram(shaderProgramObject_YSR);

    //I[]
    var Model_Matrix_YSR = mat4.create();
    var View_Matrix_YSR = mat4.create();
	
    //Lights
	if (bisLPressed_YSR == true)
	{
        gl.uniform1i(LisPressed_Uniform_YSR, 1);
        
		gl.uniform3fv(LD_Uniform_YSR, Light_Diffuse_YSR);
		gl.uniform3fv(KD_Uniform_YSR, Material_Diffuse_YSR);

        gl.uniform3fv(LS_Uniform_YSR, Light_Specular_YSR);
		gl.uniform3fv(KS_Uniform_YSR, Material_Specular_YSR);
		
		gl.uniform3fv(LA_Uniform_YSR, Light_Ambient_YSR);
		gl.uniform3fv(KA_Uniform_YSR, Material_Ambient_YSR);
		
		gl.uniform1f(Material_Shininess_YSR, shine_YSR);
		
        //imp : out to in Light
		gl.uniform4fv(Light_Position_Uniform_YSR,LightPosition_YSR);
		
	}

	else
	{
		gl.uniform1i(LisPressed_Uniform_YSR, 0);
	}

        //Tranform
	mat4.translate(Model_Matrix_YSR,Model_Matrix_YSR,[0.0,0.0,-7.0]);
   
    //model
    gl.uniformMatrix4fv(mvUniform_YSR,false, Model_Matrix_YSR); //tried modelMatrix

	//view
    gl.uniformMatrix4fv(View_Uniform_YSR, false, View_Matrix_YSR);

    //Projection
    gl.uniformMatrix4fv(Projection_Uniform_YSR, false, PerspectiveProjectionMatrix_YSR);

    sphere_YSR.draw();
	
    gl.useProgram(null);

    //Show
    requestAnimationFrame(draw,canvas);
}

//Degree
function degToRad(degrees)
{
    return(degrees*Math.PI/180);
}


//Uninitialize
function uninitialize()
{
    
    if(sphere_YSR)
    {
        sphere_YSR.deallocate();
        sphere_YSR = null;
    }

	if(vbo_position_YSR)
	{
		vbo_position_YSR.deleteBuffer();
		vbo_position_YSR = null;
	}
	
    fragmentShaderObject_YSR = null;
    vertexShaderObject_YSR = null;
    shaderProgramObject_YSR = null;
}

