
// perspective

/*
Access-Control-Allow-Origin: *


*/

//Response.AppendHeader("Access-Control-Allow-Origin", "*");


/*
    Keypress Used : w s e d , Single & Double Click

*/


//Globals
var canvas = null;
var context = null;
var gl = null;
var bFullscreen = null;
var canvas_original_width = null;
var canvas_original_height = null;
var angle = 0.0;
var sphere = null;
var Shoulder = 1, Elbow = 1 ;

//Onload : Our Main()
/*
7 Steps : 
    RequestAnimationFrame
    CancelAnimationFrame
    Macros 
    Shaders
    VAO, VBO
    Uninitialize
*/

var RequestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || 
                            window.mozRequestAnimationFrame ||  window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var CancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame || 
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

const WebGLMacros = 
{
    VDG_ATTRIBUTE_VERTEX:0,
    VDG_ATTRIBUTE_COLOR:1,
    VDG_ATTRIBUTE_NORMAL:2,
    VDG_ATTRIBUTE_TEXTURE0:3,
};                           


//new
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao;
var vbo_position,vbo_color;
var vao_Rect;

//for texture
var vbo_texture;
var pyramid_texture = 0;
var cube_texture = 0;
var moon_texture = 0;

//Uniform
var mvpUniform;
var uniform_texture0_sampler;

var PerspectiveProjectionMatrix;

//main
function main()
{
    //1. get Canvas
    canvas = document.getElementById("AMC");
    if(!canvas)
    {
        console.log("Obtaining Canvas Failed \n");
    }

    else 
        console.log("Obtaining Canvas Succed \n");

    // 2. Print Dims
    console.log("Canvas Width :  "+canvas.width+ " Canvas Height : "+canvas.height);

    canvas_original_height = canvas.height;
    canvas_original_width = canvas.width;

    // 7. Register Event Handlers
    window.addEventListener("keydown",keyDown,false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",Resize,false);
    window.addEventListener("dblclick",doubleclick,false);
    
    //As a Game Loop
    Init();

    Resize();

    draw();
}

// KeyBoard
    function keyDown(event)
    {
        switch(event.keyCode)
        {
            case 70:
                ToggleFullScreen();
                break;

            case 87: //w
                Shoulder = ((Shoulder + 3) % 360);
                break;
    
            case 83: //s
                Shoulder = ((Shoulder - 3) % 360);
                break;
    
            case 68: //d
                Elbow = ((Elbow + 3) % 360);
                break;
    
            case 69: //e
                Elbow = ((Elbow - 3) % 360);
                break;
        
        }
    }


// Mouse
    function mouseDown()
    {
        Elbow = ((Elbow + 3) % 360);

    }


//Doubleclick

function doubleclick()
{
        Shoulder = ((Shoulder + 3) % 360);

}



// ToggleFullScreen
function ToggleFullScreen()
{
    //code
    var fullscreen_Element = document.fullscreenElement || document.webkitFullscreenElement ||
                             document.mozFullScreenElement || document.msFullscreenElement || null;

    //if not
    if(fullscreen_Element == null)
    {
        if(canvas.requestFullscreen)
         canvas.requestFullscreen();

        else if(canvas.mozRequestFullscreen)
         canvas.mozRequestFullscreen();

        else if(canvas.webkitRequestFullscreen)
         canvas.webkitRequestFullscreen();

        else if(canvas.msRequestFullscreen)
        canvas.msRequestFullscreen();

        bFullscreen = true;
    }

    else // if already
    {
        if(document.exitFullscreen)
         document.exitFullscreen();

        else if(document.mozCancelFullScreen) 
         document.mozCancelFullScreen();

        else if(document.webkitExitFullscreen)
         document.webkitExitFullscreen();

        else if(document.msExitFullscreen) 
         document.msExitFullscreen();
         
         bFullscreen = false;

    }

}

//Init
function Init()
{
    //get WebGL Context
    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
        console.log("Failed to get RC \n");
        return;
    }

    gl.viewportwidth = canvas.width;
    gl.viewportheight = canvas.height;

    //Shaders
    // Vertex
    var vertexShaderSourceCode = 
				"#version 300 es" +
                "\n" +
                "in vec4 vPosition;" +
				"in vec2 vTexture0_Coord;" +
				"out vec2 out_Texture0_Coord;" +
                "uniform mat4 u_mvp_matrix;" +
                "void main(void)" +
                "{" +
                "gl_Position = u_mvp_matrix * vPosition;" +
				"out_Texture0_Coord = vTexture0_Coord;" +
                "}";

   vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
   gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
   gl.compileShader(vertexShaderObject);

   if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS) == false)
   {
      var glerror = gl.getShaderInfoLog(vertexShaderObject);

      if(glerror.length >0)
      {
          alert(glerror);
          uninitialize();
      }
   }

    // Fragment 
   var fragmentShaderSourceCode =
   "#version 300 es" +
   "\n" +
   "precision highp float;"+
   "in vec2 out_Texture0_Coord;" +
   "out vec4 FragColor;"+
   "uniform highp sampler2D u_texture0_sampler;" +
   "void main(void)"+
   "{"+
   "FragColor = texture(u_texture0_sampler,out_Texture0_Coord);"+
   "}";
   
 fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
 gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
 gl.compileShader(fragmentShaderObject);

 if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS) == false)
  {
    var glerror = gl.getShaderInfoLog(fragmentShaderObject);

    if(glerror.length >0)
    {
     alert(glerror);
     uninitialize();
    }
  }

 // Shader Program
  shaderProgramObject = gl.createProgram();
  gl.attachShader(shaderProgramObject,vertexShaderObject);
  gl.attachShader(shaderProgramObject,fragmentShaderObject);

  //pre link
   gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
   gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");

   
   //link
   gl.linkProgram(shaderProgramObject);
   
   if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
   {
    var glerror = gl.getProgramInfoLog(shaderProgramObject);

    if(glerror.length > 0)
    {
        alert(glerror);
        uninitialize();
    }
   }

   //get Uniform Location
   mvpUniform = gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	
   uniform_texture0_sampler = gl.getUniformLocation(shaderProgramObject,"u_texture0_sampler");

   //load pyramid_texture
	pyramid_texture = gl.createTexture();
	pyramid_texture.image = new Image();
	pyramid_texture.image.src = "Sun.png";
	
	//onload
	pyramid_texture.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D,pyramid_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true); //imp
		gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,
					   gl.RGBA,gl.UNSIGNED_BYTE , pyramid_texture.image);
		
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D,null);
	}
		
	
   //vao
   vao = gl.createVertexArray();
   gl.bindVertexArray(vao);
 
  
//Pyramid Texture
	var pyramidTexcoords = new Float32Array([
		0.5,1.0, //front top
		0.0,0.0,
		1.0,0.0, 
		
		0.5,1.0,
		1.0,0.0,
		0.0,0.0,
		
		0.5,1.0,
		1.0,0.0,
		0.0,0.0,
		
		0.5,1.0,
		0.0,0.0,
		1.0,0.0
		
	]);
 
 // gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);
  
   vbo_texture = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo_texture);
   gl.bufferData(gl.ARRAY_BUFFER,pyramidTexcoords,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0, 
                            2, // s t 
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
  
	//vao
    gl.bindVertexArray(null);
	
//Cube : Vertices , Colors , Vao & Vbo
    //load 
	cube_texture = gl.createTexture();
	cube_texture.image = new Image();
	cube_texture.image.src = "Earth.png";
	
	//onload
	cube_texture.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D,cube_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true); //imp
		gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,
					   gl.RGBA,gl.UNSIGNED_BYTE , cube_texture.image);
		
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D,null);
	}
	 
	// Texture
	var cube_Texcord = new Float32Array([
		0.0,0.0, //front top
		1.0,0.0,
		1.0,1.0, 
		0.0,1.0,
		
		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0,
		
		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0,
		
		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0,
		
		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0,
		
		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0
		
	]);
 
 // gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);
  
   texture = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,texture);
   gl.bufferData(gl.ARRAY_BUFFER,cube_Texcord,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0, 
                            2, // s t 
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
  
	//vao
    gl.bindVertexArray(null);

    //Moon
    moon_texture = gl.createTexture();
	moon_texture.image = new Image();
	moon_texture.image.src = "skin.png";
	
	//onload
	moon_texture.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D,moon_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true); //imp
		gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,
					   gl.RGBA,gl.UNSIGNED_BYTE , moon_texture.image);
		
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D,null);
	}
	
    sphere = new Mesh();
    makeSphere(sphere,2.0,30,30);


   //final
   gl.clearDepth(1.0);
   gl.enable(gl.DEPTH_TEST);
   gl.depthFunc(gl.LEQUAL);

   //gl.clearColor(1.0,1.0,1.0,1.0); 

   gl.clearColor(0.0,0.0,0.0,1.0);    

   PerspectiveProjectionMatrix = mat4.create();
}


//Resize
function Resize()
{
    if(bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else 
    {
        canvas.height = canvas_original_height;
        canvas.width = canvas_original_width;
    }

    //set ViewPort
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(PerspectiveProjectionMatrix,45.0,
        (parseFloat (canvas.width / canvas.height)), 0.1,100.0);
}


//Draw
function draw()
{
    //Display

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);

    //1
    var Model_Matrix = mat4.create();
    var ModelView_Matrix  = mat4.create();
	var ModelView_Projection_Matrix = mat4.create();
	var Translation_Matrix = mat4.create();
	var Rotation_Matrix = mat4.create();
    
  //Shoulder

    //Trans
    mat4.translate(Translation_Matrix , Translation_Matrix,[0.0, 0.0, -8.5]);
    mat4.multiply(ModelView_Matrix,ModelView_Matrix, Translation_Matrix);

    mat4.rotateZ(Rotation_Matrix,Rotation_Matrix,degToRad(Shoulder)); //1
    mat4.multiply(ModelView_Matrix, ModelView_Matrix, Rotation_Matrix);

    mat4.translate(Translation_Matrix , Translation_Matrix,[1.0, 0.0, 0.0]); //2
    mat4.multiply(ModelView_Matrix, ModelView_Matrix, Translation_Matrix );

    Model_Matrix = ModelView_Matrix;

    mat4.scale(ModelView_Matrix,ModelView_Matrix,[2.0,0.5,1.0]); //4

//imp
    mat4.multiply(ModelView_Projection_Matrix,PerspectiveProjectionMatrix, ModelView_Matrix);

    gl.uniformMatrix4fv(mvpUniform,false, ModelView_Projection_Matrix);
    
    //ABU
    gl.bindTexture(gl.TEXTURE_2D, moon_texture);
    gl.uniform1i(uniform_texture0_sampler,0);

    sphere.draw();


  //Elbow ///////////////******************** */

  //1
    ModelView_Matrix = Model_Matrix;

    //Transform
    mat4.translate(Translation_Matrix , Translation_Matrix,[2.5, 0.0, 0.0]); //1
    mat4.multiply(ModelView_Matrix, ModelView_Matrix, Translation_Matrix );

    mat4.rotateZ(Rotation_Matrix,Rotation_Matrix,degToRad(Elbow)); //2
    mat4.multiply(ModelView_Matrix, ModelView_Matrix, Rotation_Matrix);

    mat4.translate(Translation_Matrix , Translation_Matrix,[1.0, 0.0, 0.0]); //3
    mat4.multiply(ModelView_Matrix, ModelView_Matrix, Translation_Matrix );

    mat4.scale(ModelView_Matrix,ModelView_Matrix,[1.7,0.7,0.8]); //4

//imp
    mat4.multiply(ModelView_Projection_Matrix,PerspectiveProjectionMatrix, ModelView_Matrix);

    gl.uniformMatrix4fv(mvpUniform,false, ModelView_Projection_Matrix);
    
    //ABU
    gl.bindTexture(gl.TEXTURE_2D,moon_texture);
    gl.uniform1i(uniform_texture0_sampler,0);

    sphere.draw();

    //Unuse
    gl.useProgram(null);

    //Show
    requestAnimationFrame(draw,canvas);
}


//Degree
function degToRad(degrees)
{
    return(degrees*Math.PI/180);
}


//Uninitialize
function uninitialize()
{
    vao = null;
    vbo_color = null;
	vbo_position = null;
    fragmentShaderObject = null;
    vertexShaderObject = null;
    shaderProgramObject = null;
}

