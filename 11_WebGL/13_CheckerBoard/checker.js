// perspective

//Globals
var canvas = null;
var context = null;
var gl = null;
var bFullscreen = null;
var canvas_original_width = null;
var canvas_original_height = null;

var CHECK_IMG_WIDTH = 64;
var CHECK_IMG_HEIGHT = 64;


//Onload : Our Main()
/*
7 Steps : 
    RequestAnimationFrame
    CancelAnimationFrame
    Macros 
    Shaders
    VAO, VBO
    Uninitialize
*/

var RequestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || 
                            window.mozRequestAnimationFrame ||  window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var CancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame || 
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

const WebGLMacros = 
{
    VDG_ATTRIBUTE_VERTEX:0,
    VDG_ATTRIBUTE_COLOR:1,
    VDG_ATTRIBUTE_NORMAL:2,
    VDG_ATTRIBUTE_TEXTURE0:3,
};                           


//new
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao;
var vbo_texture,vbo_position;

var Texture_CheckImg = 0;
var TexName;
var uniform_texture0_sampler;
var CheckImage; //array

var mvpUniform;

var PerspectiveProjectionMatrix;

//main
function main()
{
    //1. get Canvas
    canvas = document.getElementById("AMC");
    if(!canvas)
    {
        console.log("Obtaining Canvas Failed \n");
    }

    else 
        console.log("Obtaining Canvas Succeed \n");

    // 2. Print Dims
    console.log("Canvas Width :  "+canvas.width+ " Canvas Height : "+canvas.height);

    canvas_original_height = canvas.height;
    canvas_original_width = canvas.width;

    // 7. Register Event Handlers
    window.addEventListener("keydown",keyDown,false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",Resize,false);

    //As a Game Loop
    Init();

    Resize();

    draw();
}

// KeyBoard
    function keyDown(event)
    {
        switch(event.keyCode)
        {
            case 70:
                ToggleFullScreen();
                break;
         }
    }


// Mouse
    function mouseDown()
    {
        
    }

// ToggleFullScreen
function ToggleFullScreen()
{
    //code
    var fullscreen_Element = document.fullscreenElement || document.webkitFullscreenElement ||
                             document.mozFullScreenElement || document.msFullscreenElement || null;

    //if not
    if(fullscreen_Element == null)
    {
        if(canvas.requestFullscreen)
         canvas.requestFullscreen();

        else if(canvas.mozRequestFullscreen)
         canvas.mozRequestFullscreen();

        else if(canvas.webkitRequestFullscreen)
         canvas.webkitRequestFullscreen();

        else if(canvas.msRequestFullscreen)
        canvas.msRequestFullscreen();

        bFullscreen = true;
    }

    else // if already
    {
        if(document.exitFullscreen)
         document.exitFullscreen();

        else if(document.mozCancelFullScreen) 
         document.mozCancelFullScreen();

        else if(document.webkitExitFullscreen)
         document.webkitExitFullscreen();

        else if(document.msExitFullscreen) 
         document.msExitFullscreen();
         
         bFullscreen = false;

    }

}

//Init
function Init()
{
	CheckImage= new Uint8Array(64*64*4);
	
    //get WebGL Context
    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
        console.log("Failed to get RC \n");
        return;
    }

    gl.viewportwidth = canvas.width;
    gl.viewportheight = canvas.height;

    //Shaders
    // Vertex
    var vertexShaderSourceCode = 
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec2 vTexture0_Coord;" +
    "out vec2 out_Texture0_Coord;" +
    "uniform mat4 u_mvp_matrix;" +
	"\n"+
    "void main(void)" +
    "{" +
    "gl_Position = u_mvp_matrix * vPosition;" +
    "out_Texture0_Coord = vTexture0_Coord;" +
    "}";

   vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
   gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
   gl.compileShader(vertexShaderObject);

   if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS) == false)
   {
      var glerror = gl.getShaderInfoLog(vertexShaderObject);

      if(glerror.length > 0)
      {
          alert(glerror);
          uninitialize();
      }
   }

    // Fragment 
   var fragmentShaderSourceCode =
   "#version 300 es" +
   "\n" +
   "precision highp float;"+
   "in vec2 out_Texture0_Coord;" +
   "out vec4 FragColor;"+
   "uniform highp sampler2D u_texture0_sampler;" +
   "\n"	+
   "void main(void)"+
   "{"+
   "FragColor = texture(u_texture0_sampler,out_Texture0_Coord);"+
   "}";
   
   
 fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
 gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
 gl.compileShader(fragmentShaderObject);

 if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS) == false)
  {
    var glerror = gl.getShaderInfoLog(fragmentShaderObject);

    if(glerror.length > 0)
    {
     alert(glerror);
     uninitialize();
    }
  }

 // Shader Program
  shaderProgramObject = gl.createProgram();
  gl.attachShader(shaderProgramObject,vertexShaderObject);
  gl.attachShader(shaderProgramObject,fragmentShaderObject);

  //pre link
   gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

   gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");
   
   //link
   gl.linkProgram(shaderProgramObject);
   
   if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
   {
    var glerror = gl.getProgramInfoLog(shaderProgramObject);

    if(glerror.length > 0)
    {
        alert(glerror);
        uninitialize();
    }
   }

   //get Uniform Location
   mvpUniform = gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");

   uniform_texture0_sampler = gl.getUniformLocation(shaderProgramObject,"u_texture0_sampler");

    //************** load texture
	MakeCheckImage();
	  
	Texture_CheckImg = gl.createTexture();
	//Texture_CheckImg.image = new Image();//not needed
	
	gl.bindTexture(gl.TEXTURE_2D,Texture_CheckImg);
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true); //imp
	
	gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,CHECK_IMG_WIDTH,CHECK_IMG_HEIGHT,0,
				  gl.RGBA,gl.UNSIGNED_BYTE, CheckImage);
				  
	/*
	gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
			width, height, border, srcFormat, srcType, pixel);
			
	Or
	
	gl.texImage2D(target, level, internalformat, width, height, 
					border, format, type, ArrayBufferView srcData, srcOffset);
	
	*/
	
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);

	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
	
	
	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
	
	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
	
	gl.texParameteri(gl.TEXTURE_2D,gl.UNPACK_ALIGNMENT,1);

	//gl.texEnvf(gl.TEXTURE_ENV,gl.TEXTURE_ENV_MODE,gl.REPLACE);
	
	gl.bindTexture(gl.TEXTURE_2D,null);
 

   //Vertices , Colors , Vao & Vbo
   var Vertices = new Float32Array
   ([       1.0,1.0,0.0,
            -1.0,1.0,0.0,
            -1.0,-1.0,0.0,
            1.0,-1.0,0.0
    ]);
	
	
   //vao
   vao = gl.createVertexArray();
   gl.bindVertexArray(vao);
 
 //vbo_position
   vbo_position = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position);
   gl.bufferData(gl.ARRAY_BUFFER,Vertices,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 
                            3, // x,y,z
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);


   //vbo_texture
   	vbo_texture = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture);
	gl.bufferData(gl.ARRAY_BUFFER, 8*3*4, gl.DYNAMIC_DRAW);

	//unbind 
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	//Array : Anticlock
	var TexCoords = new Float32Array([
		1.0,1.0,
		0.0,1.0,
		
		0.0,0.0,
		1.0,0.0,
		
		1.0,1.0,
		0.0,1.0,
		
		0.0,0.0,
		1.0,0.0
	]);
 
   vbo_texture = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo_texture);
   gl.bufferData(gl.ARRAY_BUFFER,TexCoords,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0, 
                            2, //s t
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);

    //Unbind vbo
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
	//vao
	gl.bindVertexArray(null);

    console.log("Vao VBO \n");

	gl.enable(gl.CULL_FACE);	

   gl.clearColor(0.25,0.25,0.25,1.0); 
   PerspectiveProjectionMatrix = mat4.create();
}

		

//Maths Texture
   function MakeCheckImage()
    {
        var i,j,c;

        for(i=0; i < 64 ;i++)
        {
            for(j=0; j < 64 ; j++)
            {
                //Most imp 
                c = (((i&0x8) == 0) ^ ((j&0x8) == 0))*255;

                CheckImage[(i*64+j)*4 +0] =  c;   // R
                CheckImage[(i*64+j)*4 +1] =  c;   //G
                CheckImage[(i*64+j)*4 +2] =  c;  //B
                CheckImage[(i*64+j)*4 +3] =  255; //A

            }

        }
    }	//Texture Loading


//Resize
function Resize()
{
    if(bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else 
    {
        canvas.height = canvas_original_height;
        canvas.width = canvas_original_width;
    }

    //set ViewPort
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(PerspectiveProjectionMatrix,45.0,
        (parseFloat (canvas.width / canvas.height)),
         0.1,100.0);
}


//Draw
function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //Display
    gl.useProgram(shaderProgramObject);

    //I[]
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
	var translateMatrix = mat4.create();
    var RotationMatrix = mat4.create();

    //product
    mat4.translate(translateMatrix,translateMatrix,[0.0,0.0,-5.0]); 
    mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
    mat4.multiply(modelViewMatrix,modelViewMatrix,RotationMatrix);
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

	//uniform
	gl.uniformMatrix4fv(mvpUniform,
                        false, // Transpose
                        modelViewProjectionMatrix);
						
	var squareVertices = new Float32Array(
	[
		 0.0 , 1.0 , 0.0 ,
		-2.0 , 1.0 , 0.0 ,
		-2.0 , -1.0 , 0.0 ,
		 0.0 , -1.0 , 0.0 ,
		 
		2.41421 , 1.0 , -1.41421 ,
		1.0 , 1.0 , 0.0 ,
		1.0 , -1.0 , 0.0 ,
		2.41421 , -1.0 , -1.41421
	]);

    gl.bindVertexArray(vao);
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, squareVertices, gl.DYNAMIC_DRAW);
	
						
	//texture bind code
	gl.bindTexture(gl.TEXTURE_2D,Texture_CheckImg);

    gl.uniform1i(uniform_texture0_sampler,0);
                        
    //scene
    gl.drawArrays(gl.TRIANGLE_FAN,0,4); // index is imp 
    gl.drawArrays(gl.TRIANGLE_FAN,4,4); // index is imp 
	
    //Unbind
    gl.bindVertexArray(null);
	
    gl.useProgram(null);

    //Show
    requestAnimationFrame(draw,canvas);
}

//Degree
function degToRad(degrees)
{
    return(degrees*Math.PI/180);
}


//Uninitialize
function uninitialize()
{
    vao = null;
	vbo_texture = null;
    vbo_position = null;
    fragmentShaderObject = null;
    vertexShaderObject = null;
    shaderProgramObject = null;
}

