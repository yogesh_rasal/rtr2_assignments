// Lights

//Globals
var canvas = null;
var context = null;
var gl = null;
var bFullscreen = null;
var canvas_original_width = null;
var canvas_original_height = null;
var AngleTri = 0.0, Angle = 50.0 ;

//Onload : Our Main()
/*
7 Steps : 
    RequestAnimationFrame
    CancelAnimationFrame
    Macros 
    Shaders
    VAO, VBO
    Uninitialize
*/

var RequestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || 
                            window.mozRequestAnimationFrame ||  window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var CancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame || 
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

const WebGLMacros = 
{
    VDG_ATTRIBUTE_VERTEX:0,
    VDG_ATTRIBUTE_COLOR:1,
    VDG_ATTRIBUTE_NORMAL:2,
    VDG_ATTRIBUTE_TEXTURE0:3,
};                           


//new
var vertexShaderObject_YSR ;
var fragmentShaderObject_YSR ;
var shaderProgramObject_YSR;

//toggle
var bisLPressed_YSR = false;

var PerspectiveProjectionMatrix_YSR;

var sphere_YSR;

//12
var mvUniform_YSR;
var Projection_Uniform_YSR;
var View_Uniform_YSR;

var LD_Uniform_YSR;
var KD_Uniform_YSR;

var LA_Uniform_YSR;
var KA_Uniform_YSR;

var LS_Uniform_YSR;
var KS_Uniform_YSR;
var Material_Shininess_YSR;

var Light_Position_Uniform_YSR;
var LisPressed_Uniform_YSR;

//for Red
var LD_Uniform_Red, LA_Uniform_Red, LS_Uniform_Red;
var KD_Uniform_Red, KA_Uniform_Red, KS_Uniform_Red;
var Light_Position_Uniform_Red;

//For Green
var LD_Uniform_Green, LA_Uniform_Green, LS_Uniform_Green;
var KD_Uniform_Green, KA_Uniform_Green, KS_Uniform_Green;
var Light_Position_Uniform_Green;

//main
function main()
{
    //1. get Canvas
    canvas = document.getElementById("AMC");
    if(!canvas)
    {
        console.log("Obtaining Canvas Failed \n");
    }

    else 
        console.log("Obtaining Canvas Succeed \n");

    // 2. Print Dims
    console.log("Canvas Width :  "+canvas.width+ " Canvas Height : "+canvas.height);

    canvas_original_height = canvas.height;
    canvas_original_width = canvas.width;

    // 7. Register Event Handlers
    window.addEventListener("keydown",keyDown,false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",Resize,false);

    //As a Game Loop
    Init();

    Resize();

    draw();
}

// KeyBoard
    function keyDown(event)
    {
        switch(event.keyCode)
        {
            case 70:
                ToggleFullScreen();
                break;
            
            case 76:
                if(bisLPressed_YSR == false)
                  bisLPressed_YSR = true;
                else 
                  bisLPressed_YSR = false;
                break; 
        
        }

    }


// Mouse
    function mouseDown()
    {
        
    }

// ToggleFullScreen
function ToggleFullScreen()
{
    //code
    var fullscreen_Element = document.fullscreenElement || document.webkitFullscreenElement ||
                             document.mozFullScreenElement || document.msFullscreenElement || null;

    //if not
    if(fullscreen_Element == null)
    {
        if(canvas.requestFullscreen)
         canvas.requestFullscreen();

        else if(canvas.mozRequestFullscreen)
         canvas.mozRequestFullscreen();

        else if(canvas.webkitRequestFullscreen)
         canvas.webkitRequestFullscreen();

        else if(canvas.msRequestFullscreen)
        canvas.msRequestFullscreen();

        bFullscreen = true;
    }

    else // if already
    {
        if(document.exitFullscreen)
         document.exitFullscreen();

        else if(document.mozCancelFullScreen) 
         document.mozCancelFullScreen();

        else if(document.webkitExitFullscreen)
         document.webkitExitFullscreen();

        else if(document.msExitFullscreen) 
         document.msExitFullscreen();
         
         bFullscreen = false;

    }

}

//Init
function Init()
{
    //get WebGL Context
    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
        console.log("Failed to get RC \n");
        return;
    }

    gl.viewportwidth = canvas.width;
    gl.viewportheight = canvas.height;

    //Shaders
    // Vertex : 2 Attrib , 12 Uniform +         

    var vertexShaderSourceCode_YSR = 
            "#version 300 es" +
            "\n" +
            "precision highp float;"	                +
            "in vec4 vPosition;"						+
            "in vec3 vNormal;"							+
            "out vec3 T_Norm;"							+
            "out vec3 Light_Direction;"					+
            "out vec3 Light_Direction_R;"				+
            "out vec3 Light_Direction_G;"				+
            "out vec3 Viewer_Vector;"					+
            "\n"										+
            "uniform mat4 u_model_matrix;"				+
            "uniform mat4 u_view_matrix;"				+
            "uniform mat4 u_projection_matrix;"			+
            "uniform int u_LKeyPressed;"				+
            "uniform vec4 u_Light_Position;"			+
            "uniform vec4 u_Light_Position_R;"			+
            "uniform vec4 u_Light_Position_G;"			+
            "\n"										+
            "void main(void)"							+
            "{"											+
            "if(u_LKeyPressed == 1)"					+
            "{"											+
            "vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"	+
            "T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"			+
            "Light_Direction   = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"	+
            "Light_Direction_R = vec3(u_Light_Position_R) - (Eye_Coordinates.xyz);"	+
            "Light_Direction_G = vec3(u_Light_Position_G) - (Eye_Coordinates.xyz);"	+
            "Viewer_Vector = vec3(-Eye_Coordinates.xyz);"	+
            "}"		+
            "gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"	+
            "}" ;

//"Phong_ADS_Light = Ambient + Diffuse + Specular;  \n" +

   vertexShaderObject_YSR = gl.createShader(gl.VERTEX_SHADER);
   gl.shaderSource(vertexShaderObject_YSR, vertexShaderSourceCode_YSR);
   gl.compileShader(vertexShaderObject_YSR);

   if(gl.getShaderParameter(vertexShaderObject_YSR,gl.COMPILE_STATUS) == false)
   {
      var glerror = gl.getShaderInfoLog(vertexShaderObject_YSR);

      if(glerror.length > 0)
      {
          alert(glerror);
          uninitialize();
      }
   }

    // Fragment : Error will occured for commanly used uniform - Keypress
   var fragmentShaderSourceCode_YSR =
        "#version 300 es"+
        "\n"+
        "precision highp float;"            +
        "precision highp int;"              +
        "in vec3 T_Norm;"		        	+
        "in vec3 Light_Direction;"	        +
        "in vec3 Light_Direction_R;"	    +
        "in vec3 Light_Direction_G;"	    +
        "in vec3 Viewer_Vector;"	        +
        "out vec4 FragColor;"		        +
        "\n"						        +       
        "uniform vec3 u_LA;"				+
        "uniform vec3 u_LD;"				+
        "uniform vec3 u_LS;"				+
        "uniform vec3 u_KA;"				+
        "uniform vec3 u_KD;"				+
        "uniform vec3 u_KS;"				+
        "uniform vec3 u_LA_R;"				+
        "uniform vec3 u_LD_R;"				+
        "uniform vec3 u_LS_R;"				+
        "uniform vec3 u_KA_R;"				+
        "uniform vec3 u_KD_R;"				+
        "uniform vec3 u_KS_R;"				+
        "\n"+
        "uniform vec3 u_LA_G;"				+
        "uniform vec3 u_LD_G;"				+
        "uniform vec3 u_LS_G;"				+
        "uniform vec3 u_KA_G;"				+
        "uniform vec3 u_KD_G;"				+
        "uniform vec3 u_KS_G;"				+
        "uniform float u_Shininess;"		+
        "uniform int u_LKeyPressed;"		+
        "uniform vec4 u_Light_Position;"	+
        "uniform vec4 u_Light_Position_R;"	+
        "uniform vec4 u_Light_Position_G;"	+
        "\n"								+
        "void main(void)"					+
        "{"						            +
        "if(u_LKeyPressed == 1)"            +
        "{"						            +
        "vec3 Normalized_View_Vector = normalize(Viewer_Vector);"	+
        "vec3 Normalized_Light_Direction = normalize(Light_Direction);"+
        "vec3 Normalized_Light_Direction_R = normalize(Light_Direction_R);"+
        "vec3 Normalized_Light_Direction_G = normalize(Light_Direction_G);"+
        "vec3 Normalized_TNorm = normalize(T_Norm);"+
        "float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0);"	+
        "vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm);"+
        "float TN_Dot_LD_R = max(dot(Normalized_Light_Direction_R,Normalized_TNorm), 0.0);"	+
        "vec3 Reflection_Vector_R = reflect(-Normalized_Light_Direction_R, Normalized_TNorm);"+
        "float TN_Dot_LD_G = max(dot(Normalized_Light_Direction_G,Normalized_TNorm), 0.0);"	+
        "vec3 Reflection_Vector_G = reflect(-Normalized_Light_Direction_G, Normalized_TNorm);"+
        "\n"+
        "vec3 Ambient = vec3(u_LA * u_KA);"	+
        "vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);"	+
        "vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess));" +
        "vec3 Ambient_R = vec3(u_LA_R * u_KA_R);"	+
        "vec3 Diffuse_R = vec3(u_LD_R * u_KD_R * TN_Dot_LD_R);"	+
        "vec3 Specular_R = vec3(u_LS_R * u_KS_R * pow(max(dot(Reflection_Vector_R, Normalized_View_Vector), 0.0), u_Shininess));" +
        "vec3 Ambient_G = vec3(u_LA_G * u_KA_G);"	+
        "vec3 Diffuse_G = vec3(u_LD_G * u_KD_G * TN_Dot_LD_G);"	+
        "vec3 Specular_G = vec3(u_LS_G * u_KS_G * pow(max(dot(Reflection_Vector_G, Normalized_View_Vector), 0.0), u_Shininess));" +
        "\n"+
        "vec3 Phong_ADS_Light = Ambient + Diffuse+ Specular;"	+
        "vec3 Phong_ADS_Light_R = Ambient_R + Diffuse_R+ Specular_R;"	+
        "vec3 Phong_ADS_Light_G = Ambient_G + Diffuse_G + Specular_G;"	+
        "FragColor = vec4(Phong_ADS_Light+Phong_ADS_Light_R+Phong_ADS_Light_G,1.0);" +
        "}"							+
        "else"						+
        "{"							+
        "FragColor = vec4(1.0,1.0,1.0,1.0);" +
        "}"							+
        "}" ;

    fragmentShaderObject_YSR = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_YSR, fragmentShaderSourceCode_YSR);
    gl.compileShader(fragmentShaderObject_YSR);

  if(gl.getShaderParameter(fragmentShaderObject_YSR,gl.COMPILE_STATUS) == false)
   {
    var glerror = gl.getShaderInfoLog(fragmentShaderObject_YSR);

    if(glerror.length >0)
    {
     alert(glerror);
     uninitialize();
    }
   }

 // Shader Program
  shaderProgramObject_YSR = gl.createProgram();
  gl.attachShader(shaderProgramObject_YSR,vertexShaderObject_YSR);
  gl.attachShader(shaderProgramObject_YSR,fragmentShaderObject_YSR);

  //pre link  : Attribs
  gl.bindAttribLocation(shaderProgramObject_YSR,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
  gl.bindAttribLocation(shaderProgramObject_YSR,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");
 
   //link
   gl.linkProgram(shaderProgramObject_YSR);
   
   if(!gl.getProgramParameter(shaderProgramObject_YSR,gl.LINK_STATUS))
   {
    var glerror = gl.getProgramInfoLog(shaderProgramObject_YSR);

    if(glerror.length > 0)
    {
        alert(glerror);
        uninitialize();
    }
   }

//get Uniform Location

    mvUniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR,"u_model_matrix");

	View_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_view_matrix");
	
    Projection_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_projection_matrix");

    LisPressed_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LKeyPressed");

    KD_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_KD");
    LD_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LD");
   
    LA_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LA");
    KA_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_KA");

    LS_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LS");
    KS_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_KS");

    Light_Position_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_Light_Position");


    Material_Shininess_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_Shininess");
	LisPressed_Uniform = gl.getUniformLocation(shaderProgramObject_YSR, "u_LKeyPressed");    
    
    //Red
	LD_Uniform_Red = gl.getUniformLocation(shaderProgramObject_YSR, "u_LD_R");
	KD_Uniform_Red = gl.getUniformLocation(shaderProgramObject_YSR, "u_KD_R");

	LA_Uniform_Red = gl.getUniformLocation(shaderProgramObject_YSR, "u_LA_R");
	KA_Uniform_Red = gl.getUniformLocation(shaderProgramObject_YSR, "u_KA_R");

	LS_Uniform_Red = gl.getUniformLocation(shaderProgramObject_YSR, "u_LS_R");
	KS_Uniform_Red = gl.getUniformLocation(shaderProgramObject_YSR, "u_KS_R");

	Light_Position_Uniform_Red = gl.getUniformLocation(shaderProgramObject_YSR, "u_Light_Position_R");

    //Green
    LD_Uniform_Green = gl.getUniformLocation(shaderProgramObject_YSR, "u_LD_G");
	KD_Uniform_Green = gl.getUniformLocation(shaderProgramObject_YSR, "u_KD_G");

	LA_Uniform_Green = gl.getUniformLocation(shaderProgramObject_YSR, "u_LA_G");
	KA_Uniform_Green = gl.getUniformLocation(shaderProgramObject_YSR, "u_KA_G");

	LS_Uniform_Green = gl.getUniformLocation(shaderProgramObject_YSR, "u_LS_G");
	KS_Uniform_Green = gl.getUniformLocation(shaderProgramObject_YSR, "u_KS_G");

	Light_Position_Uniform_Green = gl.getUniformLocation(shaderProgramObject_YSR, "u_Light_Position_G");
   
    // Vertices , Colors , Vao & Vbo
    sphere_YSR = new Mesh();
    makeSphere(sphere_YSR,2.0,30,30);

 
   //final
   gl.clearDepth(1.0);
   gl.enable(gl.DEPTH_TEST);
   gl.depthFunc(gl.LEQUAL);

   gl.clearColor(0.20,0.20,0.20,1.0); 
   PerspectiveProjectionMatrix_YSR = mat4.create();
}


//Resize
function Resize()
{
    if(bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else 
    {
        canvas.height = canvas_original_height;
        canvas.width = canvas_original_width;
    }

    //set ViewPort
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(PerspectiveProjectionMatrix_YSR,45.0,
        (parseFloat (canvas.width) / parseFloat(canvas.height)),
         0.1,100.0);
}


//Draw
function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //Display
    gl.useProgram(shaderProgramObject_YSR);

    //I[]
    var Model_Matrix_YSR = mat4.create();
    var View_Matrix_YSR = mat4.create();
    var RotationMatrix = mat4.create();

    //Lights
	if (bisLPressed_YSR == true)
	{
        gl.uniform1i(LisPressed_Uniform_YSR, 1);
        gl.uniform1f(Material_Shininess_YSR, 128.0);

        //Blue
		gl.uniform3f(LD_Uniform_YSR, 0.0, 0.0, 1.0);
		gl.uniform3f(KD_Uniform_YSR, 1.0, 1.0, 1.0);

        gl.uniform3f(LS_Uniform_YSR, 1.0, 1.0, 1.0);
		gl.uniform3f(KS_Uniform_YSR, 1.0, 1.0, 1.0);
		
		gl.uniform3f(LA_Uniform_YSR, 0.0, 0.0, 0.0);
		gl.uniform3f(KA_Uniform_YSR, 0.0, 0.0, 0.0);

        //imp : out to in Light
        gl.uniform4f(Light_Position_Uniform_YSR, 0.0, Math.sin(Angle)*4, Math.cos(Angle)*4 , 1.0); //y z
        
        //Red
        gl.uniform3f(LD_Uniform_Red, 1.0, 0.0, 0.0);
        gl.uniform3f(KD_Uniform_Red, 1.0, 1.0, 1.0);

        gl.uniform3f(LS_Uniform_Red, 1.0, 1.0, 1.0);
        gl.uniform3f(KS_Uniform_Red, 1.0, 1.0, 1.0);

        gl.uniform3f(LA_Uniform_Red, 0.0, 0.0, 0.0);
        gl.uniform3f(KA_Uniform_Red, 0.25, 0.25, 0.25);

        gl.uniform4f(Light_Position_Uniform_Red,Math.sin(Angle)*4, Math.cos(Angle)*4 , 0.0, 1.0); //x y

        //Green
        gl.uniform3f(LD_Uniform_Green, 0.0, 1.0, 0.0);
        gl.uniform3f(KD_Uniform_Green, 1.0, 1.0, 1.0);

        gl.uniform3f(LS_Uniform_Green, 1.0, 1.0, 1.0);
        gl.uniform3f(KS_Uniform_Green, 1.0, 1.0, 1.0);

        gl.uniform3f(LA_Uniform_Green, 0.0, 0.0, 0.0);
        gl.uniform3f(KA_Uniform_Green, 0.25, 0.25, 0.25);

        gl.uniform4f(Light_Position_Uniform_Green, Math.sin(Angle)*4, 0.0, Math.cos(Angle)*4 , 1.0); //x z

	}

	else
	{
		gl.uniform1i(LisPressed_Uniform_YSR, 0);
	}

    //Tranform
	mat4.translate(Model_Matrix_YSR,Model_Matrix_YSR,[0.0,0.0,-7.0]);
   
    mat4.multiply(Model_Matrix_YSR,Model_Matrix_YSR,RotationMatrix);

    //model
    gl.uniformMatrix4fv(mvUniform_YSR,false, Model_Matrix_YSR); //tried modelMatrix

	//view
    gl.uniformMatrix4fv(View_Uniform_YSR, false, View_Matrix_YSR);

    //Projection
    gl.uniformMatrix4fv(Projection_Uniform_YSR, false, PerspectiveProjectionMatrix_YSR);
    
    sphere_YSR.draw();
	
    gl.useProgram(null);

    
    //Angle = degToRad(AngleTri);
    Angle = Angle + 0.02;
    
    if(Angle >= 360.0)
        Angle = Angle - 360.0;

    //Show
    requestAnimationFrame(draw,canvas);
}

//Degree
function degToRad(degrees)
{
    return(degrees*Math.PI/180);
}


//Uninitialize
function uninitialize()
{
    
    fragmentShaderObject_YSR = null;
    vertexShaderObject_YSR = null;
    shaderProgramObject_YSR = null;
}

