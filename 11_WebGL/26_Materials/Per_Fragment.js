// Lights

//Globals
var canvas = null;
var context = null;
var gl = null;
var bFullscreen = null;
var canvas_original_width = null;
var canvas_original_height = null;

var Angle = 50.0;
var XPressed,YPressed,ZPressed;

//Onload : Our Main()
/*
7 Steps : 
    RequestAnimationFrame
    CancelAnimationFrame
    Macros 
    Shaders
    VAO, VBO
    Uninitialize
*/

var RequestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || 
                            window.mozRequestAnimationFrame ||  window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var CancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame || 
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

const WebGLMacros = 
{
    VDG_ATTRIBUTE_VERTEX:0,
    VDG_ATTRIBUTE_COLOR:1,
    VDG_ATTRIBUTE_NORMAL:2,
    VDG_ATTRIBUTE_TEXTURE0:3,
};                           


//new
var vertexShaderObject_YSR ;
var fragmentShaderObject_YSR ;
var shaderProgramObject_YSR;

var vbo_position_YSR;
var sphere_YSR = null;

//toggle
var bisLPressed_YSR = false;

var PerspectiveProjectionMatrix_YSR;

//12
var mvUniform_YSR;
var Projection_Uniform_YSR;
var View_Uniform_YSR;

var LD_Uniform_YSR;
var KD_Uniform_YSR;

var LA_Uniform_YSR;
var KA_Uniform_YSR;

var LS_Uniform_YSR;
var KS_Uniform_YSR;
var Material_Shininess_YSR;

var Light_Position_Uniform_YSR;
var LisPressed_Uniform_YSR;

//imp
var Light_Ambient_YSR = [0.20,0.20,0.20];

var Light_Diffuse_YSR = [1.0,1.0,1.0];

var Light_Specular_YSR = [0.0,0.0,0.0];


//main
function main()
{
    //1. get Canvas
    canvas = document.getElementById("AMC");
    if(!canvas)
    {
        console.log("Obtaining Canvas Failed \n");
    }

    else 
        console.log("Obtaining Canvas Succeed \n");

    // 2. Print Dims
    console.log("Canvas Width :  "+canvas.width+ " Canvas Height : "+canvas.height);

    canvas_original_height = canvas.height;
    canvas_original_width = canvas.width;

    // 7. Register Event Handlers
    window.addEventListener("keydown",keyDown,false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",Resize,false);

    //As a Game Loop
    Init();

    Resize();

    draw();
}

// KeyBoard
    function keyDown(event)
    {
      switch(event.keyCode)
       {
            case 70:
                ToggleFullScreen();
                break;
            
            case 76:
                if(bisLPressed_YSR == false)
                  bisLPressed_YSR = true;
                else 
                  bisLPressed_YSR = false;
                break; 
        
            case 88:
			XPressed = true;
			YPressed = false;
			ZPressed = false;
			break;
	
		case 89:
			YPressed = true;
			ZPressed = false;
			XPressed = false;
			break;

		case 90:
			XPressed = false;
			YPressed = false;
			ZPressed = true;
			break;
	
      }

    }


// Mouse
    function mouseDown()
    {
        
    }

// ToggleFullScreen
function ToggleFullScreen()
{
    //code
    var fullscreen_Element = document.fullscreenElement || document.webkitFullscreenElement ||
                             document.mozFullScreenElement || document.msFullscreenElement || null;

    //if not
    if(fullscreen_Element == null)
    {
        if(canvas.requestFullscreen)
         canvas.requestFullscreen();

        else if(canvas.mozRequestFullscreen)
         canvas.mozRequestFullscreen();

        else if(canvas.webkitRequestFullscreen)
         canvas.webkitRequestFullscreen();

        else if(canvas.msRequestFullscreen)
        canvas.msRequestFullscreen();

        bFullscreen = true;
    }

    else // if already
    {
        if(document.exitFullscreen)
         document.exitFullscreen();

        else if(document.mozCancelFullScreen) 
         document.mozCancelFullScreen();

        else if(document.webkitExitFullscreen)
         document.webkitExitFullscreen();

        else if(document.msExitFullscreen) 
         document.msExitFullscreen();
         
         bFullscreen = false;

    }

}

//Init
function Init()
{
    //get WebGL Context
    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
        console.log("Failed to get RC \n");
        return;
    }

    gl.viewportwidth = canvas.width;
    gl.viewportheight = canvas.height;

    //Shaders
    // Vertex : 2 Attrib , 12 Uniform +         

    var vertexShaderSourceCode_YSR = 
        "#version 300 es" +
        "\n" +
        "precision highp float;"					+
        "in vec4 vPosition;"						+
		"in vec3 vNormal;"							+
		"out vec3 T_Norm;"							+
		"out vec3 Light_Direction;"					+
		"out vec3 Viewer_Vector;"					+
		"\n"										+ 
		"uniform mat4 u_model_matrix;"				+ 
		"uniform mat4 u_view_matrix;"				+ 
		"uniform mat4 u_projection_matrix;"			+ 
		"uniform mediump int u_LKeyPressed;"		+
		"uniform vec4 u_Light_Position;"			+
		"\n"										+
		"void main(void)"							+
		"{"											+
		"if(u_LKeyPressed == 1)"					+
		"{"											+
		" vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"	+ 
		" T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"			    + 
		" Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"    +    
		" Viewer_Vector = vec3(-Eye_Coordinates.xyz);"                          +   
		"}"		+ 
		"gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"	+ 
		"}";


//"Phong_ADS_Light = Ambient + Diffuse + Specular;  \n" +

   vertexShaderObject_YSR = gl.createShader(gl.VERTEX_SHADER);
   gl.shaderSource(vertexShaderObject_YSR, vertexShaderSourceCode_YSR);
   gl.compileShader(vertexShaderObject_YSR);

   if(gl.getShaderParameter(vertexShaderObject_YSR,gl.COMPILE_STATUS) == false)
   {
      var glerror = gl.getShaderInfoLog(vertexShaderObject_YSR);

      if(glerror.length > 0)
      {
          alert(glerror);
          uninitialize();
      }
   }

    // Fragment : Error will occured for commanly used uniform - Keypress
   var fragmentShaderSourceCode_YSR =
        "#version 300 es"+
        "\n"+
        "precision highp float;"                    +
		"in vec3 T_Norm;"			                +
		"in vec3 Light_Direction;"	                +
		"in vec3 Viewer_Vector;"	                +
		"out vec4 FragColor;"		                +
		"\n"						                +
		"uniform vec3 u_LA;"						+
		"uniform vec3 u_LD;"						+
		"uniform vec3 u_LS;"						+
		"uniform vec3 u_KA;"						+
		"uniform vec3 u_KD;"						+
		"uniform vec3 u_KS;"						+
		"uniform float u_Shininess;"				+
		"uniform int u_LKeyPressed;"				+
		"uniform vec4 u_Light_Position;"			+
		"\n"										+
		"void main(void) "		                    +
		"{"						                    +
		"if(u_LKeyPressed == 1)"                    +
	    "{"						                    +
		  "vec3 Normalized_View_Vector = normalize(Viewer_Vector); \n"	+
		  "vec3 Normalized_Light_Direction = normalize(Light_Direction); \n"+
		  "vec3 Normalized_TNorm = normalize(T_Norm); \n "+
		  "float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0); \n "	+
		  "vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm); \n "+
		  "vec3 Ambient = vec3(u_LA * u_KA); \n "	+
		  "vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD); \n "	+
		  "vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess)); \n " +
		  "vec3 Phong_ADS_Light = Ambient + Diffuse+ Specular; \n"	+
		  "FragColor = vec4(Phong_ADS_Light,1.0); \n " +
	    "}"		+
		"else"	+
		"{"		+
		" FragColor = vec4(1.0,1.0,1.0,1.0);" +
		"}"+
	  "}" ;


    fragmentShaderObject_YSR = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_YSR, fragmentShaderSourceCode_YSR);
    gl.compileShader(fragmentShaderObject_YSR);

  if(gl.getShaderParameter(fragmentShaderObject_YSR,gl.COMPILE_STATUS) == false)
   {
    var glerror = gl.getShaderInfoLog(fragmentShaderObject_YSR);

    if(glerror.length >0)
    {
     alert(glerror);
     uninitialize();
    }
   }

 // Shader Program
  shaderProgramObject_YSR = gl.createProgram();
  gl.attachShader(shaderProgramObject_YSR,vertexShaderObject_YSR);
  gl.attachShader(shaderProgramObject_YSR,fragmentShaderObject_YSR);

  //pre link  : Attribs
  gl.bindAttribLocation(shaderProgramObject_YSR,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
  gl.bindAttribLocation(shaderProgramObject_YSR,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");
 
   //link
   gl.linkProgram(shaderProgramObject_YSR);
   
   if(!gl.getProgramParameter(shaderProgramObject_YSR,gl.LINK_STATUS))
   {
    var glerror = gl.getProgramInfoLog(shaderProgramObject_YSR);

    if(glerror.length > 0)
    {
        alert(glerror);
        uninitialize();
    }
   }

//get Uniform Location

    mvUniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR,"u_model_matrix");

	View_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_view_matrix");
	
    Projection_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_projection_matrix");

    Light_Position_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_Light_Position");

    LisPressed_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LKeyPressed");

    KD_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_KD");
    LD_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LD");
   
    LA_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LA");
    KA_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_KA");

    LS_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_LS");
    KS_Uniform_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_KS");

    Material_Shininess_YSR = gl.getUniformLocation(shaderProgramObject_YSR, "u_Shininess");
	

// Vertices , Colors , Vao & Vbo
    sphere_YSR = new Mesh();
    makeSphere(sphere_YSR,2.0,30,30);

   //final
   gl.clearDepth(1.0);
   gl.enable(gl.DEPTH_TEST);
   gl.depthFunc(gl.LEQUAL);

   gl.clearColor(0.0,0.0,0.0,1.0); 
   PerspectiveProjectionMatrix_YSR = mat4.create();
}


//Resize
function Resize()
{
    if(bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else 
    {
        canvas.height = canvas_original_height;
        canvas.width = canvas_original_width;
    }

    //set ViewPort
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(PerspectiveProjectionMatrix_YSR,45.0,
        (parseFloat (canvas.width) / parseFloat(canvas.height)),
         0.1,100.0);
}


//Draw
function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //Display
    gl.useProgram(shaderProgramObject_YSR);

    //I[]
    var Model_Matrix_YSR = mat4.create();
    var View_Matrix_YSR = mat4.create();
	
    //Lights
	if (bisLPressed_YSR == true)
	{
        gl.uniform1i(LisPressed_Uniform_YSR, 1);
        
		gl.uniform3fv(LD_Uniform_YSR, Light_Diffuse_YSR);
		
        gl.uniform3fv(LS_Uniform_YSR, Light_Specular_YSR);

		gl.uniform3fv(LA_Uniform_YSR, Light_Ambient_YSR);
		
        
        //imp : out to in Light
        if(XPressed)
        gl.uniform4f(Light_Position_Uniform_YSR, 0.0, Math.sin(Angle)*6, Math.cos(Angle)*6 , 1.0); //y z
        
        if(ZPressed)
        gl.uniform4f(Light_Position_Uniform_YSR,Math.sin(Angle)*6, Math.cos(Angle)*6 , 0.0, 1.0); //x y
        
        if(YPressed)
        gl.uniform4f(Light_Position_Uniform_YSR,Math.sin(Angle)*6, 0.0, Math.cos(Angle)*6 , 1.0); //x z
	}

	else
	{
		gl.uniform1i(LisPressed_Uniform_YSR, 0);
	}

    
    //24 Spheres
    var Material_Shininess = [24]; 
    var Material_Ambient =   [72]; 
    var Material_Diffuse =  [72]; 
    var Material_Specular =  [72]; 
    
 
// 1 : Emerald
	Material_Ambient[0] = 0.0215;
	Material_Ambient[1] = 0.1745;
	Material_Ambient[2] = 0.0215;

	Material_Diffuse[0] = 0.0215;
	Material_Diffuse[1] = 0.1745;
	Material_Diffuse[2] = 0.0215;
    //
    
	Material_Specular[0] = 0.633;
	Material_Specular[1] = 0.727811;
	Material_Specular[2] = 0.633;

	Material_Shininess[0] = 0.6 * 128.0;


// 2 = Jade
	Material_Ambient[3] = 0.135;
	Material_Ambient[4] = 0.2225;
	Material_Ambient[5] = 0.1575;
	//
	///

	Material_Diffuse[3] = 0.54;
	Material_Diffuse[4] = 0.89;
	Material_Diffuse[5] = 0.63;
    //
	//
    
	Material_Specular[3] = 0.316228;
	Material_Specular[4] = 0.316228;
	Material_Specular[5] = 0.316228;
	//

	Material_Shininess[1] = 0.1 * 128.0;


// 3 = obisidian
	Material_Ambient[6] = 0.05375;
	Material_Ambient[7] = 0.05;
	Material_Ambient[8] = 0.06625;
	//

	Material_Diffuse[6] = 0.18275;
	Material_Diffuse[7] = 0.17;
	Material_Diffuse[8] = 0.22525;
    //
	//
    
	Material_Specular[6] = 0.332741;
	Material_Specular[7] = 0.328634;
	Material_Specular[8] = 0.346435;
	//
    //

	Material_Shininess[2] = 0.3 * 128.0;
    //   

// 4= Pearl
	Material_Ambient[9] = 0.25;
	Material_Ambient[10] = 0.20725;
	Material_Ambient[11] = 0.20725;
	//
	///

	Material_Diffuse[9] = 1.0;
	Material_Diffuse[10] = 0.829;
	Material_Diffuse[11] = 0.829;
    //
	//
    
	Material_Specular[9] = 0.296648;
	Material_Specular[10] = 0.296648;
	Material_Specular[11] = 0.296648;
	//
    //

	Material_Shininess[3] = 0.088 * 128.0;
    //

// 5 = Ruby
	Material_Ambient[12] = 0.1745;
	Material_Ambient[13] = 0.01175;
	Material_Ambient[14] = 0.01175;
	//
	///

	Material_Diffuse[12] = 0.61424;
	Material_Diffuse[13] = 0.04136;
	Material_Diffuse[14] = 0.04136;
    //
    
	Material_Specular[12] = 0.727811;
	Material_Specular[13] = 0.626959;
	Material_Specular[14] = 0.626959;
	//

	Material_Shininess[4] = 0.6 * 128.0;
    //

// 6 = Turquoise
	Material_Ambient[15] = 0.1;
	Material_Ambient[16] = 0.18275;
	Material_Ambient[17] = 0.1745;
	//
	///

	Material_Diffuse[15] = 0.396;
	Material_Diffuse[16] = 0.74151;
	Material_Diffuse[17] = 0.69102;
    //
	//
    
	Material_Specular[15] = 0.297524;
	Material_Specular[16] = 0.30829;
	Material_Specular[17] = 0.306678;
	//
    //

	Material_Shininess[5] = 0.1 * 128.0;
    //
  

// Metals   ---------------------

// 1 : Brass
	Material_Ambient[18] = 0.349412;
	Material_Ambient[19] = 0.30829;
	Material_Ambient[20] = 0.306678;
	///

	Material_Diffuse[18] = 0.780392;
	Material_Diffuse[19] = 0.223529;
	Material_Diffuse[20] = 0.027451;
    //
	//
    
	Material_Specular[18] = 0.992157;
	Material_Specular[19] = 0.941176;
	Material_Specular[20] = 0.807843;
	//
    //

	Material_Shininess[6] = 0.21794872 * 128.0;
    //

// 2 = Bronze
	Material_Ambient[21] = 0.2125;
	Material_Ambient[22] = 0.1275;
	Material_Ambient[23] = 0.054;
	//
	///

	Material_Diffuse[21] = 0.714;
	Material_Diffuse[22] = 0.4284;
	Material_Diffuse[23] = 0.18144;
    //
	//
    
	Material_Specular[21] = 0.393548;
	Material_Specular[22] = 0.271906;
	Material_Specular[23] = 0.166721;
	//
    //

	Material_Shininess[7] = 0.2 * 128.0;
    //

// 3 = Chrome
	Material_Ambient[24] = 0.25;
	Material_Ambient[25] = 0.25;
	Material_Ambient[26] = 0.25;
	//
	///

	Material_Diffuse[24] = 0.4;
	Material_Diffuse[25] = 0.4;
	Material_Diffuse[26] = 0.4;
    //
	//
    
	Material_Specular[24] = 0.774597;
	Material_Specular[25] = 0.774597;
	Material_Specular[26] = 0.774597;
	//
    //

	Material_Shininess[8] = 0.6 * 128.0;
    //

	
// 4= copper
	Material_Ambient[27] = 0.19125;
	Material_Ambient[28] = 0.0735;
	Material_Ambient[29] = 0.02025;
	//
	///

	Material_Diffuse[27] = 0.7038;
	Material_Diffuse[28] = 0.27048;
	Material_Diffuse[29] = 0.0828;
    //
	//
    
	Material_Specular[27] = 0.25677;
	Material_Specular[28] = 0.137622;
	Material_Specular[29] = 0.086014;
	//
    //

	Material_Shininess[9] = 0.1 * 128.0;


// 5 = Gold
	Material_Ambient[30] = 0.24725;
	Material_Ambient[31] = 0.1995;
	Material_Ambient[32] = 0.0745;
	//
	///

	Material_Diffuse[30] = 0.7517;
	Material_Diffuse[31] = 0.6065;
	Material_Diffuse[32] = 0.2265;
    //
    
	Material_Specular[30] = 0.6283;
	Material_Specular[31] = 0.55580;
	Material_Specular[32] = 0.36606;
	//

	Material_Shininess[10] = 0.4 * 128.0;
    //

// 6 = Silver
	Material_Ambient[33] = 0.19225;
	Material_Ambient[34] = 0.19225;
	Material_Ambient[35] = 0.19225;
	//

	Material_Diffuse[33] = 0.5075;
	Material_Diffuse[34] = 0.5075;
	Material_Diffuse[35] = 0.5075;
    //
	//
    
	Material_Specular[33] = 0.50828;
	Material_Specular[34] = 0.50828;
	Material_Specular[35] = 0.50828;
	//
    //

	Material_Shininess[11] = 0.4 * 128.0;
    //


// Plastic ---------------------
// 1 - Black
	Material_Ambient[36] = 0.0;
	Material_Ambient[37] = 0.0;
	Material_Ambient[38] = 0.0;
	//
	///

	Material_Diffuse[36] = 0.01;
	Material_Diffuse[37] = 0.01;
	Material_Diffuse[38] = 0.01;
    //
	//
    
	Material_Specular[36] = 0.5;
	Material_Specular[37] = 0.5;
	Material_Specular[38] = 0.5;
	//
    //

	Material_Shininess[12] = 0.25 * 128.0;
    //


// 2 - Cyan
	Material_Ambient[39] = 0.0;
	Material_Ambient[40] = 0.1;
	Material_Ambient[41] = 0.06;
	//
	///

	Material_Diffuse[39] = 0.0;
	Material_Diffuse[40] = 0.5098039;
	Material_Diffuse[41] = 0.5098039;
    //
	//
    
	Material_Specular[39] = 0.0;
	Material_Specular[40] = 0.501960;
	Material_Specular[41] = 0.501960;
	//

	Material_Shininess[13] = 0.25 * 128.0;
    //

//3 - Green
	Material_Ambient[42] = 0.0;
	Material_Ambient[43] = 0.1;
	Material_Ambient[44] = 0.06;
	//
	///

	Material_Diffuse[42] = 0.1;
	Material_Diffuse[43] = 0.35;
	Material_Diffuse[44] = 0.1;
    //
	//
    
	Material_Specular[42] = 0.45;
	Material_Specular[43] = 0.55;
	Material_Specular[44] = 0.45;
	//
    //

	Material_Shininess[14] = 0.25 * 128.0;
    //


// Red

	Material_Ambient[45] = 0.0;
	Material_Ambient[46] = 0.0;
	Material_Ambient[47] = 0.0;
	//
	///

	Material_Diffuse[45] = 0.5;
	Material_Diffuse[46] = 0.0;
	Material_Diffuse[47] = 0.0;
    //
	//
    
	Material_Specular[45] = 0.7;
	Material_Specular[46] = 0.6;
	Material_Specular[47] = 0.6;
	//
    //

	Material_Shininess[15] = 0.25 * 128.0;


// White
	Material_Ambient[48] = 0.0;
	Material_Ambient[49] = 0.0;
	Material_Ambient[50] = 0.0;
	//
	///

	Material_Diffuse[48] = 0.55;
	Material_Diffuse[49] = 0.55;
	Material_Diffuse[50] = 0.55;
    //
	//
    
	Material_Specular[48] = 0.7;
	Material_Specular[49] = 0.7;
	Material_Specular[50] = 0.7;
	//
    //

	Material_Shininess[16] = 0.25 * 128.0;
    //


// Yellow
	Material_Ambient[51] = 0.0;
	Material_Ambient[52] = 0.0;
	Material_Ambient[53] = 0.0;
	//
	///

	Material_Diffuse[51] = 0.5;
	Material_Diffuse[52] = 0.5;
	Material_Diffuse[53] = 0.0;
    
	Material_Specular[51] = 0.6;
	Material_Specular[52] = 0.6;
	Material_Specular[53] = 0.5;

	Material_Shininess[17] = 0.25 * 128.0;


// -------- Rubber
// 1 - Black
	Material_Ambient[54] = 0.02;
	Material_Ambient[55] = 0.02;
	Material_Ambient[56] = 0.02;
	//
	///

	Material_Diffuse[54] = 0.01;
	Material_Diffuse[55] = 0.01;
	Material_Diffuse[56] = 0.01;
    //
	//
    
	Material_Specular[54] = 0.4;
	Material_Specular[55] = 0.4;
	Material_Specular[56] = 0.4;
	//
    //

	Material_Shininess[18] = 0.07813 * 128.0;
    //


// 2 - Cyan
	Material_Ambient[57] = 0.0;
	Material_Ambient[58] = 0.05;
	Material_Ambient[59] = 0.05;
	//
	///

	Material_Diffuse[57] = 0.4;
	Material_Diffuse[58] = 0.5098039;
	Material_Diffuse[59] = 0.5098039;
    //
	//
    
	Material_Specular[57] = 0.0;
	Material_Specular[58] = 0.501960;
	Material_Specular[59] = 0.501960;
	//
    //

	Material_Shininess[19] = 0.07813 * 128.0;
    //

	//
	
//3 - Green
	Material_Ambient[60] = 0.0;
	Material_Ambient[61] = 0.1;
	Material_Ambient[62] = 0.06;
	//
	///

	Material_Diffuse[60] = 0.1;
	Material_Diffuse[61] = 0.35;
	Material_Diffuse[62] = 0.1;
    //
	//
    
	Material_Specular[60] = 0.45;
	Material_Specular[61] = 0.55;
	Material_Specular[62] = 0.45;
	//
    //

	Material_Shininess[20] = 0.07813 * 128.0;
    //

// 4- Red

	Material_Ambient[63] = 0.0;
	Material_Ambient[64] = 0.0;
	Material_Ambient[65] = 0.0;
	//
	///

	Material_Diffuse[63] = 0.5;
	Material_Diffuse[64] = 0.0;
	Material_Diffuse[65] = 0.0;
    //
	//
    
	Material_Specular[63] = 0.7;
	Material_Specular[64] = 0.6;
	Material_Specular[65] = 0.6;
	//
    //

	Material_Shininess[21] = 0.07813 * 128.0;
    //

// 5- White
	Material_Ambient[66] = 0.0;
	Material_Ambient[67] = 0.0;
	Material_Ambient[68] = 0.0;
	//
	///

	Material_Diffuse[66] = 0.55;
	Material_Diffuse[67] = 0.55;
	Material_Diffuse[68] = 0.55;
    //
	//
    
	Material_Specular[66] = 0.7;
	Material_Specular[67] = 0.7;
	Material_Specular[68] = 0.7;
	//
    //

	Material_Shininess[22] = 0.07813 * 128.0;

// 6- Yellow
	Material_Ambient[69] = 0.0;
	Material_Ambient[70] = 0.0;
	Material_Ambient[71] = 0.0;

	Material_Diffuse[69] = 0.5;
	Material_Diffuse[70] = 0.5;
	Material_Diffuse[71] = 0.0;
    
	Material_Specular[69] = 0.6;
	Material_Specular[70] = 0.6;
	Material_Specular[71] = 0.5;

	Material_Shininess[23] = 0.07813 * 128.0;


        //Tranform
	mat4.translate(Model_Matrix_YSR,Model_Matrix_YSR,[0.0,0.0,-8.0]);
   
    //model
    gl.uniformMatrix4fv(mvUniform_YSR,false, Model_Matrix_YSR); //tried modelMatrix

	//view
    gl.uniformMatrix4fv(View_Uniform_YSR, false, View_Matrix_YSR);

    //Projection
    gl.uniformMatrix4fv(Projection_Uniform_YSR, false, PerspectiveProjectionMatrix_YSR);

    //imp

    var width = canvas.width/6;
    var height = canvas.height/4;

    var i,j;

       for(i = 0; i <= 6; i++)
        {
         for(j = 0; j <= 4 ; j++)
         {
             //1
            gl.uniform1f(Material_Shininess_YSR,Material_Shininess[i+j]);

            gl.uniform3f(KD_Uniform_YSR, Material_Diffuse[3*i+j],Material_Diffuse[3*i+j+1],Material_Diffuse[3*i+j+2]);

            gl.uniform3f(KS_Uniform_YSR, Material_Specular[3*i+j],Material_Specular[3*i+j+1],Material_Specular[3*i+j+2]);

            gl.uniform3f(KA_Uniform_YSR, Material_Ambient[3*i+j],Material_Ambient[3*i+j+1],Material_Ambient[3*i+j+2]);
                    
            //2
            gl.viewport(width*i,height*j,width,height);

            mat4.perspective(PerspectiveProjectionMatrix_YSR,45.0,(parseFloat (width) / parseFloat(height)),0.1,100.0);

            sphere_YSR.draw();
	
         }
        }
        
    gl.useProgram(null);

    Angle = Angle + 0.02;

    if(Angle >= 360.0)
     Angle = Angle - 350.0;


    //Show
    requestAnimationFrame(draw,canvas);
}

//Degree
function degToRad(degrees)
{
    return(degrees*Math.PI/180);
}


//Uninitialize
function uninitialize()
{
    
    if(sphere_YSR)
    {
        sphere_YSR.deallocate();
        sphere_YSR = null;
    }

	if(vbo_position_YSR)
	{
		vbo_position_YSR.deleteBuffer();
		vbo_position_YSR = null;
	}
	
    fragmentShaderObject_YSR = null;
    vertexShaderObject_YSR = null;
    shaderProgramObject_YSR = null;
}

