// perspective

//Globals
var canvas = null;
var context = null;
var gl = null;
var bFullscreen = null;
var canvas_original_width = null;
var canvas_original_height = null;

//Onload : Our Main()
/*
7 Steps : 
    RequestAnimationFrame
    CancelAnimationFrame
    Macros 
    Shaders
    VAO, VBO
    Uninitialize
*/

var RequestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || 
                            window.mozRequestAnimationFrame ||  window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var CancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame || 
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

const WebGLMacros = 
{
    VDG_ATTRIBUTE_VERTEX:0,
    VDG_ATTRIBUTE_COLOR:1,
    VDG_ATTRIBUTE_NORMAL:2,
    VDG_ATTRIBUTE_TEXTURE0:3,
};                           


//new
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao,vao_Triangle;
var vbo,vbo_color;

var mvpUniform;

var PerspectiveProjectionMatrix;

//New : like Android
var SLICES = 20;

//for Graph

var  vao_HLines ;
var  vao_Lines ;

var vbo_PositionHLines ;
var vbo_PositionVLines ;
var vbo_ColorHLines ;
var vbo_ColorVLines ;


//Circle
var vao_circle;
var vbo_circle_pos,vbo_circle_color;

var NUM_POINTS = 1000;

//Axes
var vao_XAxis ;
var vao_YAxis ;
var vbo_XPosition ;
var vbo_XColor;
var vbo_YPosition ;
var vbo_YColor ;

//main
function main()
{
    //1. get Canvas
    canvas = document.getElementById("AMC");
    if(!canvas)
    {
        console.log("Obtaining Canvas Failed \n");
    }

    else 
        console.log("Obtaining Canvas Succed \n");

    // 2. Print Dims
    console.log("Canvas Width :  "+canvas.width+ " Canvas Height : "+canvas.height);

    canvas_original_height = canvas.height;
    canvas_original_width = canvas.width;

    // 7. Register Event Handlers
    window.addEventListener("keydown",keyDown,false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",Resize,false);

    //As a Game Loop
    Init();

    Resize();

    draw();
}

// KeyBoard
    function keyDown(event)
    {
        switch(event.keyCode)
        {
            case 70:
                ToggleFullScreen();
                break;
        }
    }


// Mouse
    function mouseDown()
    {
    }

// ToggleFullScreen
function ToggleFullScreen()
{
    //code
    var fullscreen_Element = document.fullscreenElement || document.webkitFullscreenElement ||
                             document.mozFullScreenElement || document.msFullscreenElement || null;

    //if not
    if(fullscreen_Element == null)
    {
        if(canvas.requestFullscreen)
         canvas.requestFullscreen();

        else if(canvas.mozRequestFullscreen)
         canvas.mozRequestFullscreen();

        else if(canvas.webkitRequestFullscreen)
         canvas.webkitRequestFullscreen();

        else if(canvas.msRequestFullscreen)
        canvas.msRequestFullscreen();

        bFullscreen = true;
    }

    else // if already
    {
        if(document.exitFullscreen)
         document.exitFullscreen();

        else if(document.mozCancelFullScreen) 
         document.mozCancelFullScreen();

        else if(document.webkitExitFullscreen)
         document.webkitExitFullscreen();

        else if(document.msExitFullscreen) 
         document.msExitFullscreen();
         
         bFullscreen = false;

    }

}

//Init
function Init()
{
    //get WebGL Context
    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
        console.log("Failed to get RC \n");
        return;
    }

    gl.viewportwidth = canvas.width;
    gl.viewportheight = canvas.height;

    //Shaders
    // Vertex
    var vertexShaderSourceCode = 
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec4 vColor;" +
        "out vec4 outColor;" +
        "uniform mat4 u_mvp_matrix;" +
        "void main(void)" +
        "{" +
        "gl_Position = u_mvp_matrix * vPosition;" +
        "outColor = vColor;"+
        "}";

   vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
   gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
   gl.compileShader(vertexShaderObject);

   if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS) == false)
   {
      var glerror = gl.getShaderInfoLog(vertexShaderObject);

      if(glerror.length >0)
      {
          alert(glerror);
          uninitialize();
      }
   }

    // Fragment 
   var fragmentShaderSourceCode =
   "#version 300 es" +
   "\n" +
   "precision highp float;"+
   "in vec4 outColor;" +
   "out vec4 FragColor;"+
   "void main(void)"+
   "{"+
   "FragColor = outColor;"+
   "}";
   
 fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
 gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
 gl.compileShader(fragmentShaderObject);

 if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS) == false)
  {
    var glerror = gl.getShaderInfoLog(fragmentShaderObject);

    if(glerror.length >0)
    {
     alert(glerror);
     uninitialize();
    }
  }

 // Shader Program
  shaderProgramObject = gl.createProgram();
  gl.attachShader(shaderProgramObject,vertexShaderObject);
  gl.attachShader(shaderProgramObject,fragmentShaderObject);

  //pre link
   gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
   gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_COLOR,"vColor");
   
   //link
   gl.linkProgram(shaderProgramObject);
   
   if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
   {
    var glerror = gl.getProgramInfoLog(shaderProgramObject);

    if(glerror.length > 0)
    {
        alert(glerror);
        uninitialize();
    }
   }

   //get Uniform Location
   mvpUniform = gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");

   //Triangle
   var TriangleVertices = new Float32Array([
        0.0,1.0,0.0,
       -1.0,-1.0,0.0,

        1.0,-1.0,0.0,
        0.0,1.0,0.0,

        1.0,-1.0,0.0,
        -1.0,-1.0,0.0
   ]);

   var TriangleColor = new Float32Array([
    1.0,1.0,1.0,
    1.0,1.0,1.0,

    1.0,1.0,1.0,
    1.0,1.0,1.0,

    1.0,1.0,1.0,
    1.0,1.0,1.0

    ]);

      //vao
   vao_Triangle = gl.createVertexArray();
   gl.bindVertexArray(vao_Triangle);
 
   //vbo
   vbo = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo);
   gl.bufferData(gl.ARRAY_BUFFER,TriangleVertices,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 
                            3, // x,y,z
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    
    //vbo_color

   vbo_color = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color);
   gl.bufferData(gl.ARRAY_BUFFER,TriangleColor,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR, 
                            3, // x,y,z
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    //vao
    gl.bindVertexArray(null);



   //Rectangle , Colors , Vao & Vbo
   var Vertices = new Float32Array(
       [ 
         1.0,1.0,0.0,
        -1.0,1.0,0.0,
        
		-1.0,1.0,0.0,
		-1.0,-1.0,0.0,
        
        1.0,-1.0,0.0,
		-1.0,-1.0,0.0,
        
        1.0,-1.0,0.0,
		1.0,1.0,0.0
       ]);

    var RectColor = new Float32Array(
        [ 
            1.0,0.5,0.0,
            1.0,0.5,0.0,

            1.0,0.5,0.0,
            1.0,0.5,0.0,

            1.0,0.5,0.0,
            1.0,0.5,0.0,

            1.0,0.5,0.0,
            1.0,0.5,0.0

        ]);


   //vao
   vao = gl.createVertexArray();
   gl.bindVertexArray(vao);
 
   //vbo
   vbo = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo);
   gl.bufferData(gl.ARRAY_BUFFER,Vertices,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 
                            3, // x,y,z
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    
    //vbo_color

   vbo_color = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color);
   gl.bufferData(gl.ARRAY_BUFFER,RectColor,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR, 
                            3, // x,y,z
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    //vao
    gl.bindVertexArray(null);

    //Graph
    ShowGraph();

    //Circle
    MakeCircle();

    gl.clearColor(0.0,0.0,0.0,1.0); 
   
    PerspectiveProjectionMatrix = mat4.create();
}


//Circle
function MakeCircle()
{
    var cVertices = new Float32Array([SLICES * 16]); //4*20*4

    var LinesColor = new Float32Array(
        [ 1.0,1.0,0.0,
          1.0,1.0,0.0
        ]);

//********************************************

	// Create Lines vao
    vao_circle = gl.createVertexArray();
	gl.bindVertexArray(vao_circle);

    //vbo
	vbo_circle_pos = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_circle_pos);

    //gl.bufferData(target, ArrayBufferView srcData, usage, srcOffset, length);
    gl.bufferData(gl.ARRAY_BUFFER, cVertices, gl.DYNAMIC_DRAW);

    //gl.vertexAttribPointer(index, size, type, normalized, stride, offset);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);

	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);

	// Unbind 
	gl.bindBuffer(gl.ARRAY_BUFFER, null);


	// Bind color
	vbo_circle_color = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_circle_color);

    gl.bufferData(gl.ARRAY_BUFFER, LinesColor, gl.STATIC_DRAW);

    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);

	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);

	// Unbind 
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	// Unbind vao Lines
	gl.bindVertexArray(null);

}


//Display
function DisplayCircle() 
{
    var PI = 3.146;
    var angle = 0.0;

    var circleangle = new Float32Array([0.0,1.0,0.0,
            0.0,-1.0,0.0
        ]);


 for (var index = 0; index < NUM_POINTS; index++)
  {
    angle = (2 * PI *  index) / NUM_POINTS;
    circleangle[0] = Math.cos(angle);
    circleangle[1] = Math.sin(angle);
    circleangle[2] = 0;


    angle = (2 * PI * (index + 1)) / NUM_POINTS;
    circleangle[3] = Math.cos(angle);
    circleangle[4] = Math.sin(angle);
    circleangle[5] = 0;


    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_circle_pos);
    gl.bufferData(gl.ARRAY_BUFFER, circleangle, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.drawArrays(gl.LINES, 0, 2); //imp
 }

}


//Graph
function ShowGraph()
{
    //Graph Lines : new Float32Array()
    var hLinesVertices = new Float32Array([SLICES * 16]); //4*20*4

    var LinesColor = new Float32Array(
        [ 0.0,1.0,0.0,
          0.0,1.0,0.0
        ]);

    //Vertical
    var vLinesVertices = new Float32Array([SLICES * 16]);


//********************************************

	// Create Lines vao
    vao_Lines = gl.createVertexArray();
	gl.bindVertexArray(vao_Lines);

    //vbo
	vbo_PositionVLines = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_PositionVLines);

    //gl.bufferData(target, ArrayBufferView srcData, usage, srcOffset, length);
    gl.bufferData(gl.ARRAY_BUFFER, vLinesVertices, gl.DYNAMIC_DRAW);

    //gl.vertexAttribPointer(index, size, type, normalized, stride, offset);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);

	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);

	// Unbind 
	gl.bindBuffer(gl.ARRAY_BUFFER, null);


	// Bind color
	vbo_ColorVLines = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_ColorVLines);

    gl.bufferData(gl.ARRAY_BUFFER, LinesColor, gl.STATIC_DRAW);

    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);

	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);

	// Unbind 
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	// Unbind vao Lines
	gl.bindVertexArray(null);


//********************************************

	// Create vao for Hzt
	vao_HLines = gl.createVertexArray();
	gl.bindVertexArray(vao_HLines);

    //vbo
	vbo_PositionHLines = gl.createBuffer();
    
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_PositionHLines);
    
    gl.bufferData(gl.ARRAY_BUFFER, hLinesVertices, gl.DYNAMIC_DRAW);

    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);

	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);

	// Unbind 
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

  
// Bind color
    
    vbo_ColorHLines = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_ColorHLines);

    gl.bufferData(gl.ARRAY_BUFFER, LinesColor, gl.STATIC_DRAW);

    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);

	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);

	// Unbind color
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	// Unbind vao
	gl.bindVertexArray(null);

	
}


//Resize
function Resize()
{
    if(bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else 
    {
        canvas.height = canvas_original_height;
        canvas.width = canvas_original_width;
    }

    //set ViewPort
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(PerspectiveProjectionMatrix,45.0,
        (parseFloat (canvas.width / canvas.height)),
         0.1,100.0);
}


//Draw
function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //Display
    gl.useProgram(shaderProgramObject);

    //I[]
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
	var translateMatrix = mat4.create();

    //Graph

    // Hzt
    mat4.translate(translateMatrix,translateMatrix,[0.0,0.0,-1.0]); 
    mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

    gl.uniformMatrix4fv(mvpUniform,
                        false, // Transpose
                        modelViewProjectionMatrix);

    gl.bindVertexArray(vao_HLines);

    //scene
        //Hzt
        for (var i = 1.0; i > -1.0 ; i -= 0.05)
        {
            var horizon = new Float32Array([
                1.0 , i , 0.0 ,
                -1.0 , i , 0.0 
            ]);


        gl.bindBuffer(gl.ARRAY_BUFFER, vbo_PositionHLines);

        gl.bufferData(gl.ARRAY_BUFFER, horizon, gl.STATIC_DRAW);

        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.drawArrays(gl.LINES,0,2); //shapes

        }
    

    //Unbind
    gl.bindVertexArray(null); //most imp
        
    
    //Vertical
    modelViewMatrix = mat4.create();
    modelViewProjectionMatrix = mat4.create();
    translateMatrix = mat4.create();

    mat4.translate(translateMatrix,translateMatrix,[0.0,0.0,-1.0]); 
    mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

    gl.uniformMatrix4fv(mvpUniform,
                        false, // Transpose
                        modelViewProjectionMatrix);

    gl.bindVertexArray(vao_Lines);

    //scene
        for (var i = -1.0; i < 1.0 ; i += 0.05)
        {
            var vertical = new Float32Array([
                i, 1.0 , 0.0,
                i,-1.0 , 0.0 
            ]);


        gl.bindBuffer(gl.ARRAY_BUFFER, vbo_PositionVLines);

        gl.bufferData(gl.ARRAY_BUFFER, vertical, gl.STATIC_DRAW);

        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.drawArrays(gl.LINES,0,2); //shapes

        }

    //Unbind
    gl.bindVertexArray(null); //most imp
    

 //for Circle
    
        modelViewMatrix = mat4.create();
        modelViewProjectionMatrix = mat4.create();
        translateMatrix = mat4.create();
    
        mat4.translate(translateMatrix,translateMatrix,[0.0,0.0,-4.0]); 
        mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
        mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);
    
        gl.uniformMatrix4fv(mvpUniform,
                            false, // Transpose
                            modelViewProjectionMatrix);
    
    //vao                            
        gl.bindVertexArray(vao_circle);
    
    //Show Circle
        DisplayCircle();
        
        gl.drawArrays(gl.POINTS,0,NUM_POINTS); //show
    
    //Unbind
        gl.bindVertexArray(null); //most imp

        
	//for Rectangle
    modelViewMatrix = mat4.create();
    modelViewProjectionMatrix = mat4.create();
    translateMatrix = mat4.create();

	mat4.translate(translateMatrix,translateMatrix,[0.0,0.0,-6.0]); 
	mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

    gl.uniformMatrix4fv(mvpUniform,
                        false, // Transpose
                        modelViewProjectionMatrix);

   
        gl.bindVertexArray(vao);

        //scene
        gl.drawArrays(gl.LINES,0,2); //shapes
        gl.drawArrays(gl.LINES,2,2); //shapes
        gl.drawArrays(gl.LINES,4,2); //shapes
        gl.drawArrays(gl.LINES,6,2); //shapes
       
        //Unbind
        gl.bindVertexArray(null);

//for Triangle
    modelViewMatrix = mat4.create();
    modelViewProjectionMatrix = mat4.create();
    translateMatrix = mat4.create();

    mat4.translate(translateMatrix,translateMatrix,[0.0,0.0,-7.0]); 
    mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

    gl.uniformMatrix4fv(mvpUniform,
                        false, // Transpose
                        modelViewProjectionMatrix);


    gl.bindVertexArray(vao_Triangle);

    //scene
    gl.drawArrays(gl.LINES,0,2); //shapes
    gl.drawArrays(gl.LINES,2,2); //shapes
    gl.drawArrays(gl.LINES,4,2); //shapes
   
    //Unbind
    gl.bindVertexArray(null);

        
        
    gl.useProgram(null);

    //Show
    requestAnimationFrame(draw,canvas);
}

//Uninitialize
function uninitialize()
{
    vao = null;
    vbo = null;
    fragmentShaderObject = null;
    vertexShaderObject = null;
    shaderProgramObject = null;
}

