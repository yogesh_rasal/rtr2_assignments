// perspective

//Globals
var canvas = null;
var context = null;
var gl = null;
var bFullscreen = null;
var canvas_original_width = null;
var canvas_original_height = null;
var angle = 0.0;

//Onload : Our Main()
/*
7 Steps : 
    RequestAnimationFrame
    CancelAnimationFrame
    Macros 
    Shaders
    VAO, VBO
    Uninitialize
*/

var RequestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || 
                            window.mozRequestAnimationFrame ||  window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var CancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame || 
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

const WebGLMacros = 
{
    VDG_ATTRIBUTE_VERTEX:0,
    VDG_ATTRIBUTE_COLOR:1,
    VDG_ATTRIBUTE_NORMAL:2,
    VDG_ATTRIBUTE_TEXTURE0:3,
};                           


//new
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao;
var vbo;
var mvpUniform;

var PerspectiveProjectionMatrix;

//main
function main()
{
    //1. get Canvas
    canvas = document.getElementById("AMC");
    if(!canvas)
    {
        console.log("Obtaining Canvas Failed \n");
    }

    else 
        console.log("Obtaining Canvas Succed \n");

    // 2. Print Dims
    console.log("Canvas Width :  "+canvas.width+ " Canvas Height : "+canvas.height);

    canvas_original_height = canvas.height;
    canvas_original_width = canvas.width;

    // 7. Register Event Handlers
    window.addEventListener("keydown",keyDown,false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",Resize,false);

    //As a Game Loop
    Init();

    Resize();

    draw();
}

// KeyBoard
    function keyDown(event)
    {
        switch(event.keyCode)
        {
            case 70:
                ToggleFullScreen();
                break;
        }
    }


// Mouse
    function mouseDown()
    {
        alert("Mouse Click");
    }

// ToggleFullScreen
function ToggleFullScreen()
{
    //code
    var fullscreen_Element = document.fullscreenElement || document.webkitFullscreenElement ||
                             document.mozFullScreenElement || document.msFullscreenElement || null;

    //if not
    if(fullscreen_Element == null)
    {
        if(canvas.requestFullscreen)
         canvas.requestFullscreen();

        else if(canvas.mozRequestFullscreen)
         canvas.mozRequestFullscreen();

        else if(canvas.webkitRequestFullscreen)
         canvas.webkitRequestFullscreen();

        else if(canvas.msRequestFullscreen)
        canvas.msRequestFullscreen();

        bFullscreen = true;
    }

    else // if already
    {
        if(document.exitFullscreen)
         document.exitFullscreen();

        else if(document.mozCancelFullScreen) 
         document.mozCancelFullScreen();

        else if(document.webkitExitFullscreen)
         document.webkitExitFullscreen();

        else if(document.msExitFullscreen) 
         document.msExitFullscreen();
         
         bFullscreen = false;

    }

}

//Init
function Init()
{
    //get WebGL Context
    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
        console.log("Failed to get RC \n");
        return;
    }

    gl.viewportwidth = canvas.width;
    gl.viewportheight = canvas.height;

    //Shaders
    // Vertex
    var vertexShaderSourceCode = 
        "#version 300 es" +
                "\n" +
                "in vec4 vPosition;" +
                "uniform mat4 u_mvp_matrix;" +
                "void main(void)" +
                "{" +
                "gl_Position = u_mvp_matrix * vPosition;" +
                "}";

   vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
   gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
   gl.compileShader(vertexShaderObject);

   if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS) == false)
   {
      var glerror = gl.getShaderInfoLog(vertexShaderObject);

      if(glerror.length >0)
      {
          alert(glerror);
          uninitialize();
      }
   }

    // Fragment 
   var fragmentShaderSourceCode =
   "#version 300 es" +
   "\n" +
   "precision highp float;"+
   "out vec4 FragColor;"+
   "void main(void)"+
   "{"+
   "FragColor = vec4(1.0,0.5,0.0,1.0);"+
   "}";
   
 fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
 gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
 gl.compileShader(fragmentShaderObject);

 if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS) == false)
  {
    var glerror = gl.getShaderInfoLog(fragmentShaderObject);

    if(glerror.length >0)
    {
     alert(glerror);
     uninitialize();
    }
  }

 // Shader Program
  shaderProgramObject = gl.createProgram();
  gl.attachShader(shaderProgramObject,vertexShaderObject);
  gl.attachShader(shaderProgramObject,fragmentShaderObject);

  //pre link
   gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
   
   //link
   gl.linkProgram(shaderProgramObject);
   
   if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
   {
    var glerror = gl.getProgramInfoLog(shaderProgramObject);

    if(glerror.length > 0)
    {
        alert(glerror);
        uninitialize();
    }
   }

   //get Uniform Location
   mvpUniform = gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");

   //Vertices , Colors , Vao & Vbo
   var Vertices = new Float32Array(
       [   0.0,1.0,0.0, //apex
           -1.0,-1.0,0.0, //left
           1.0,-1.0,0.0, //Right
		   -1.0,1.0,0.0, //apex
           -1.0,-1.0,0.0, //left
           1.0,-1.0,0.0, //Right
		   1.0,1.0,0.0 // Top
       ] );

   //vao
   vao = gl.createVertexArray();
   gl.bindVertexArray(vao);
 
   //vbo
   vbo = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo);
   gl.bufferData(gl.ARRAY_BUFFER,Vertices,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 
                            3, // x,y,z
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    gl.bindVertexArray(null);

   gl.clearColor(0.0,0.1,0.0,1.0); 
   PerspectiveProjectionMatrix = mat4.create();
}


//Resize
function Resize()
{
    if(bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else 
    {
        canvas.height = canvas_original_height;
        canvas.width = canvas_original_width;
    }

    //set ViewPort
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(PerspectiveProjectionMatrix,45.0,
        (parseFloat (canvas.width / canvas.height)),
         0.1,100.0);
}


//Draw
function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //Display
    gl.useProgram(shaderProgramObject);

    //I[]
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
	var translateMatrix = mat4.create();
    var RotationMatrix = mat4.create();

    //product
    mat4.translate(translateMatrix,translateMatrix,[-1.5,0.0,-6.0]); 
    mat4.rotateY(RotationMatrix,RotationMatrix,degToRad(angle));
    mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
    mat4.multiply(modelViewMatrix,modelViewMatrix,RotationMatrix);
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

    gl.uniformMatrix4fv(mvpUniform,
                        false, // Transpose
                        modelViewProjectionMatrix);

    gl.bindVertexArray(vao);

    //scene
    gl.drawArrays(gl.TRIANGLES,0,3); //shapes
	
    
    //Unbind
    gl.bindVertexArray(null); //most imp
	
	//for Rectangle
	 modelViewMatrix = mat4.create();
     modelViewProjectionMatrix = mat4.create();
	 translateMatrix = mat4.create();
     RotationMatrix = mat4.create();

     //Tranform
	mat4.translate(translateMatrix,translateMatrix,[1.5,0.0,-6.0]); 
    mat4.rotateY(RotationMatrix,RotationMatrix,degToRad(angle));
    mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
    mat4.multiply(modelViewMatrix,modelViewMatrix,RotationMatrix);
   
    //imp
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

    gl.uniformMatrix4fv(mvpUniform,
                        false, // Transpose
                        modelViewProjectionMatrix);

    gl.bindVertexArray(vao);

    //scene
    gl.drawArrays(gl.TRIANGLE_FAN,3,4); // index is imp
    
    //Unbind
    gl.bindVertexArray(null);
	
    gl.useProgram(null);

    angle = angle + 2.0;
    if(angle >= 360.0)
     angle = angle - 360.0;

    //Show
    requestAnimationFrame(draw,canvas);
}

//Degree
function degToRad(degrees)
{
    return(degrees*Math.PI/180);
}


//Uninitialize
function uninitialize()
{
    vao = null;
    vbo = null;
    fragmentShaderObject = null;
    vertexShaderObject = null;
    shaderProgramObject = null;
}

