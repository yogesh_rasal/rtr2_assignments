// perspective

//Globals
var canvas = null;
var context = null;
var gl = null;
var bFullscreen = null;
var canvas_original_width = null;
var canvas_original_height = null;
var angle = 0.0;

//Onload : Our Main()
/*
7 Steps : 
    RequestAnimationFrame
    CancelAnimationFrame
    Macros 
    Shaders
    VAO, VBO
    Uninitialize
*/

var RequestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || 
                            window.mozRequestAnimationFrame ||  window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var CancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame || 
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

const WebGLMacros = 
{
    VDG_ATTRIBUTE_VERTEX:0,
    VDG_ATTRIBUTE_COLOR:1,
    VDG_ATTRIBUTE_NORMAL:2,
    VDG_ATTRIBUTE_TEXTURE0:3,
};                           


//new
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao;
var vbo_position,vbo_color;
var vao_Rect;

//for texture
var vbo_texture;
var pyramid_texture = 0;
var cube_texture = 0;

//Uniform
var mvpUniform;
var uniform_texture0_sampler;

var PerspectiveProjectionMatrix;

//main
function main()
{
    //1. get Canvas
    canvas = document.getElementById("AMC");
    if(!canvas)
    {
        console.log("Obtaining Canvas Failed \n");
    }

    else 
        console.log("Obtaining Canvas Succed \n");

    // 2. Print Dims
    console.log("Canvas Width :  "+canvas.width+ " Canvas Height : "+canvas.height);

    canvas_original_height = canvas.height;
    canvas_original_width = canvas.width;

    // 7. Register Event Handlers
    window.addEventListener("keydown",keyDown,false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",Resize,false);

    //As a Game Loop
    Init();

    Resize();

    draw();
}

// KeyBoard
    function keyDown(event)
    {
        switch(event.keyCode)
        {
            case 70:
                ToggleFullScreen();
                break;
        }
    }


// Mouse
    function mouseDown()
    {
        alert("Mouse Click");
    }

// ToggleFullScreen
function ToggleFullScreen()
{
    //code
    var fullscreen_Element = document.fullscreenElement || document.webkitFullscreenElement ||
                             document.mozFullScreenElement || document.msFullscreenElement || null;

    //if not
    if(fullscreen_Element == null)
    {
        if(canvas.requestFullscreen)
         canvas.requestFullscreen();

        else if(canvas.mozRequestFullscreen)
         canvas.mozRequestFullscreen();

        else if(canvas.webkitRequestFullscreen)
         canvas.webkitRequestFullscreen();

        else if(canvas.msRequestFullscreen)
        canvas.msRequestFullscreen();

        bFullscreen = true;
    }

    else // if already
    {
        if(document.exitFullscreen)
         document.exitFullscreen();

        else if(document.mozCancelFullScreen) 
         document.mozCancelFullScreen();

        else if(document.webkitExitFullscreen)
         document.webkitExitFullscreen();

        else if(document.msExitFullscreen) 
         document.msExitFullscreen();
         
         bFullscreen = false;

    }

}

//Init
function Init()
{
    //get WebGL Context
    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
        console.log("Failed to get RC \n");
        return;
    }

    gl.viewportwidth = canvas.width;
    gl.viewportheight = canvas.height;

    //Shaders
    // Vertex
    var vertexShaderSourceCode = 
				"#version 300 es" +
                "\n" +
                "in vec4 vPosition;" +
				"in vec2 vTexture0_Coord;" +
				"out vec2 out_Texture0_Coord;" +
                "uniform mat4 u_mvp_matrix;" +
                "void main(void)" +
                "{" +
                "gl_Position = u_mvp_matrix * vPosition;" +
				"out_Texture0_Coord = vTexture0_Coord;" +
                "}";

   vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
   gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
   gl.compileShader(vertexShaderObject);

   if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS) == false)
   {
      var glerror = gl.getShaderInfoLog(vertexShaderObject);

      if(glerror.length >0)
      {
          alert(glerror);
          uninitialize();
      }
   }

    // Fragment 
   var fragmentShaderSourceCode =
   "#version 300 es" +
   "\n" +
   "precision highp float;"+
   "in vec2 out_Texture0_Coord;" +
   "out vec4 FragColor;"+
   "uniform highp sampler2D u_texture0_sampler;" +
   "void main(void)"+
   "{"+
   "FragColor = texture(u_texture0_sampler,out_Texture0_Coord);"+
   "}";
   
 fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
 gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
 gl.compileShader(fragmentShaderObject);

 if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS) == false)
  {
    var glerror = gl.getShaderInfoLog(fragmentShaderObject);

    if(glerror.length >0)
    {
     alert(glerror);
     uninitialize();
    }
  }

 // Shader Program
  shaderProgramObject = gl.createProgram();
  gl.attachShader(shaderProgramObject,vertexShaderObject);
  gl.attachShader(shaderProgramObject,fragmentShaderObject);

  //pre link
   gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
   gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");

   
   //link
   gl.linkProgram(shaderProgramObject);
   
   if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
   {
    var glerror = gl.getProgramInfoLog(shaderProgramObject);

    if(glerror.length > 0)
    {
        alert(glerror);
        uninitialize();
    }
   }

   //get Uniform Location
   mvpUniform = gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	
   uniform_texture0_sampler = gl.getUniformLocation(shaderProgramObject,"u_texture0_sampler");

   //load pyramid_texture
	pyramid_texture = gl.createTexture();
	pyramid_texture.image = new Image();
	pyramid_texture.image.src = "stone.png";
	
	//onload
	pyramid_texture.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D,pyramid_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true); //imp
		gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,
					   gl.RGBA,gl.UNSIGNED_BYTE , pyramid_texture.image);
		
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D,null);
	}
		
	
   //Vertices , Colors , Vao & Vbo
   var Vertices = new Float32Array(
       [  
	    // Front face
    0.0,  1.0,  0.0,
    -1.0, -1.0,  1.0,
    1.0, -1.0,  1.0,
    // Right face
    0.0,  1.0,  0.0,
    1.0, -1.0,  1.0,
    1.0, -1.0, -1.0,
    // Back face
    0.0,  1.0,  0.0,
    1.0, -1.0, -1.0,
    -1.0, -1.0, -1.0,
    // Left face
    0.0,  1.0,  0.0,
    -1.0, -1.0, -1.0,
    -1.0, -1.0,  1.0
       ] );

   //vao
   vao = gl.createVertexArray();
   gl.bindVertexArray(vao);
 
   //vbo
   vbo_position = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position);
   gl.bufferData(gl.ARRAY_BUFFER,Vertices,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 
                            3, // x,y,z
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

//Pyramid Texture
	var pyramidTexcoords = new Float32Array([
		0.5,1.0, //front top
		0.0,0.0,
		1.0,0.0, 
		
		0.5,1.0,
		1.0,0.0,
		0.0,0.0,
		
		0.5,1.0,
		1.0,0.0,
		0.0,0.0,
		
		0.5,1.0,
		0.0,0.0,
		1.0,0.0
		
	]);
 
 // gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);
  
   vbo_texture = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo_texture);
   gl.bufferData(gl.ARRAY_BUFFER,pyramidTexcoords,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0, 
                            2, // s t 
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
  
	//vao
    gl.bindVertexArray(null);
	
//Cube : Vertices , Colors , Vao & Vbo
    //load 
	cube_texture = gl.createTexture();
	cube_texture.image = new Image();
	cube_texture.image.src = "Vijay_Kundali.png";
	
	//onload
	cube_texture.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D,cube_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true); //imp
		gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,
					   gl.RGBA,gl.UNSIGNED_BYTE , cube_texture.image);
		
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D,null);
	}
	 
	
	Vertices = new Float32Array(
       [  //top
		1.0 , 1.0 , -1.0 ,
		-1.0 , 1.0 , -1.0 ,
		-1.0 , 1.0 , 1.0 ,
		1.0 , 1.0 , 1.0 ,
		
		//bottom
		1.0 , -1.0 , 1.0 ,
		-1.0 , -1.0 , 1.0 ,
		-1.0 , -1.0 , -1.0 ,
		1.0 , -1.0 , -1.0 ,
		
		//3
		1.0 , 1.0 , 1.0 ,
		-1.0 , 1.0 , 1.0 ,
		-1.0 , -1.0 , 1.0 ,
		1.0 , -1.0 , 1.0 ,
		
		//4
		1.0 , -1.0 , -1.0 ,
		-1.0 , -1.0 , -1.0 ,
		-1.0 , 1.0 , -1.0 ,
		1.0 , 1.0 , 1.0 ,
		
		//5
		-1.0 , 1.0 , 1.0 ,
		-1.0 , 1.0 , -1.0 ,
		-1.0 , -1.0 , -1.0 ,
		-1.0 , -1.0 , 1.0 ,
		
		//6
		1.0 , 1.0 , -1.0 ,
		1.0 , 1.0 , 1.0 ,
		1.0 , -1.0 , 1.0 ,
		1.0 , -1.0 , -1.0	
	
       ] );

   //vao
   vao_Rect = gl.createVertexArray();
   gl.bindVertexArray(vao_Rect);
 
   //vbo
   vbo_position = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position);
   gl.bufferData(gl.ARRAY_BUFFER,Vertices,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 
                            3, // x,y,z
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

	//Pyramid Texture
	var cube_Texcord = new Float32Array([
		0.0,0.0, //front top
		1.0,0.0,
		1.0,1.0, 
		0.0,1.0,
		
		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0,
		
		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0,
		
		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0,
		
		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0,
		
		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0
		
	]);
 
 // gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);
  
   vbo_texture = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo_texture);
   gl.bufferData(gl.ARRAY_BUFFER,cube_Texcord,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0, 
                            2, // s t 
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
  
	//vao
    gl.bindVertexArray(null);


   //final
   gl.clearDepth(1.0);
   gl.enable(gl.DEPTH_TEST);
   gl.depthFunc(gl.LEQUAL);

   gl.clearColor(0.0,0.1,0.0,1.0); 
   PerspectiveProjectionMatrix = mat4.create();
}


//Resize
function Resize()
{
    if(bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else 
    {
        canvas.height = canvas_original_height;
        canvas.width = canvas_original_width;
    }

    //set ViewPort
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(PerspectiveProjectionMatrix,45.0,
        (parseFloat (canvas.width / canvas.height)), 0.1,100.0);
}


//Draw
function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //Display
    gl.useProgram(shaderProgramObject);

    //I[]
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
	var translateMatrix = mat4.create();
    var RotationMatrix = mat4.create();

    //product
    mat4.translate(translateMatrix,translateMatrix,[-1.5,0.0,-6.0]); 
    mat4.rotateY(RotationMatrix,RotationMatrix,degToRad(angle));
    mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
    mat4.multiply(modelViewMatrix,modelViewMatrix,RotationMatrix);
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

    gl.uniformMatrix4fv(mvpUniform,
                        false, // Transpose
                        modelViewProjectionMatrix);
	
	//ABU
	gl.bindTexture(gl.TEXTURE_2D,pyramid_texture);
	gl.uniform1i(uniform_texture0_sampler,0);

    gl.bindVertexArray(vao);

    //scene
    gl.drawArrays(gl.TRIANGLES,0,12); //shapes
	
    
    //Unbind
    gl.bindVertexArray(null); //most imp
	
//for Rectangle
	 modelViewMatrix = mat4.create();
     modelViewProjectionMatrix = mat4.create();
	 translateMatrix = mat4.create();
     RotationMatrix = mat4.create();

     //Tranform
	mat4.translate(translateMatrix,translateMatrix,[1.5,0.0,-6.5]);

	mat4.rotateX(RotationMatrix,RotationMatrix,degToRad(angle/2));	
    mat4.rotateY(RotationMatrix,RotationMatrix,degToRad(angle/2));
	mat4.rotateZ(RotationMatrix,RotationMatrix,degToRad(angle/2));
    
	mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
    mat4.multiply(modelViewMatrix,modelViewMatrix,RotationMatrix);
   
    //imp
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

	//ABU
	gl.bindTexture(gl.TEXTURE_2D,cube_texture);
	gl.uniform1i(uniform_texture0_sampler,0);

    gl.uniformMatrix4fv(mvpUniform,
                        false, // Transpose
                        modelViewProjectionMatrix);

    gl.bindVertexArray(vao_Rect);

    //scene
    gl.drawArrays(gl.TRIANGLE_FAN,0,4); // index is imp
    gl.drawArrays(gl.TRIANGLE_FAN,4,4);
	gl.drawArrays(gl.TRIANGLE_FAN,8,4);
	gl.drawArrays(gl.TRIANGLE_FAN,12,4);
	gl.drawArrays(gl.TRIANGLE_FAN,16,4);
	gl.drawArrays(gl.TRIANGLE_FAN,20,4);
	
	
    //Unbind
    gl.bindVertexArray(null);
	
	
    gl.useProgram(null);

    angle = angle + 1.0;
    if(angle >= 360.0)
     angle = angle - 360.0;

    //Show
    requestAnimationFrame(draw,canvas);
}

//Degree
function degToRad(degrees)
{
    return(degrees*Math.PI/180);
}


//Uninitialize
function uninitialize()
{
    vao = null;
    vbo_color = null;
	vbo_position = null;
    fragmentShaderObject = null;
    vertexShaderObject = null;
    shaderProgramObject = null;
}

