// Lights

//Globals
var canvas = null;
var context = null;
var gl = null;
var bFullscreen = null;
var canvas_original_width = null;
var canvas_original_height = null;
var angle = 0.0;
var bisLPressed = false;

//Onload : Our Main()
/*
7 Steps : 
    RequestAnimationFrame
    CancelAnimationFrame
    Macros 
    Shaders
    VAO, VBO
    Uninitialize
*/

var RequestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || 
                            window.mozRequestAnimationFrame ||  window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var CancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame || 
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

const WebGLMacros = 
{
    VDG_ATTRIBUTE_VERTEX:0,
    VDG_ATTRIBUTE_COLOR:1,
    VDG_ATTRIBUTE_NORMAL:2,
    VDG_ATTRIBUTE_TEXTURE0:3,
};                           


//new
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vbo_position,Vbo_cube_normal;
var vao_Rect;

var mvUniform;
var PerspectiveProjectionMatrix;

//new

var Model_View_Uniform;
var Projection_Uniform;
var LD_Uniform;
var KD_Uniform;
var Light_Position_Uniform;
var LisPressed_Uniform;

//main
function main()
{
    //1. get Canvas
    canvas = document.getElementById("AMC");
    if(!canvas)
    {
        console.log("Obtaining Canvas Failed \n");
    }

    else 
        console.log("Obtaining Canvas Succed \n");

    // 2. Print Dims
    console.log("Canvas Width :  "+canvas.width+ " Canvas Height : "+canvas.height);

    canvas_original_height = canvas.height;
    canvas_original_width = canvas.width;

    // 7. Register Event Handlers
    window.addEventListener("keydown",keyDown,false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",Resize,false);

    //As a Game Loop
    Init();

    Resize();

    draw();
}

// KeyBoard
    function keyDown(event)
    {
        switch(event.keyCode)
        {
            case 70:
                ToggleFullScreen();
                break;
            
            case 76:
                if(bisLPressed == false)
                  bisLPressed = true;
                else 
                  bisLPressed = false;
                break; 
        
        }

    }


// Mouse
    function mouseDown()
    {
        
    }

// ToggleFullScreen
function ToggleFullScreen()
{
    //code
    var fullscreen_Element = document.fullscreenElement || document.webkitFullscreenElement ||
                             document.mozFullScreenElement || document.msFullscreenElement || null;

    //if not
    if(fullscreen_Element == null)
    {
        if(canvas.requestFullscreen)
         canvas.requestFullscreen();

        else if(canvas.mozRequestFullscreen)
         canvas.mozRequestFullscreen();

        else if(canvas.webkitRequestFullscreen)
         canvas.webkitRequestFullscreen();

        else if(canvas.msRequestFullscreen)
        canvas.msRequestFullscreen();

        bFullscreen = true;
    }

    else // if already
    {
        if(document.exitFullscreen)
         document.exitFullscreen();

        else if(document.mozCancelFullScreen) 
         document.mozCancelFullScreen();

        else if(document.webkitExitFullscreen)
         document.webkitExitFullscreen();

        else if(document.msExitFullscreen) 
         document.msExitFullscreen();
         
         bFullscreen = false;

    }

}

//Init
function Init()
{
    //get WebGL Context
    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
        console.log("Failed to get RC \n");
        return;
    }

    gl.viewportwidth = canvas.width;
    gl.viewportheight = canvas.height;

    //Shaders
    // Vertex
    var vertexShaderSourceCode = 
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;"						+
        "in vec3 vNormal;"							+
        "out vec3 diffuse_Color;"					+
        "\n"										+
        "uniform mat4 u_mv_matrix;"					+
        "uniform mat4 u_projection_matrix;"			+
        "uniform vec3 u_LD;"						+
        "uniform vec3 u_KD;"						+
        "uniform vec4 u_Light_Position;"			+
        "\n"										+
        "void main(void)"							+
        "\n"										+
        "{"											+
        "vec4 Eye_Coordinates = u_mv_matrix * vPosition;"	+
        "mat3 Normal_Matrix = mat3(transpose(inverse(u_mv_matrix)));"	+
        "vec3 T_Norm = normalize(Normal_Matrix * vNormal);"	+
        "vec3 S = vec3(u_Light_Position) - (Eye_Coordinates.xyz);" +
        "diffuse_Color = (u_LD * u_KD * dot(S,T_Norm));"  +
        "gl_Position = (u_projection_matrix * u_mv_matrix * vPosition);"	+
        "}" ;

   vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
   gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
   gl.compileShader(vertexShaderObject);

   if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS) == false)
   {
      var glerror = gl.getShaderInfoLog(vertexShaderObject);

      if(glerror.length >0)
      {
          alert(glerror);
          uninitialize();
      }
   }

    // Fragment 
   var fragmentShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "precision highp float;"+
    "in vec3 diffuse_Color;"	+
    "out vec4 FragColor;"		+
    "uniform int u_LKeyPressed;"+
    "\n"						+
    "void main(void)"		+
    "{"						+
    "if(u_LKeyPressed == 1)"	+
    "{"						    +
    "FragColor = vec4(diffuse_Color,1.0);"		+
    "}"							+
    "else"						+
    "{"							+
    "FragColor = vec4(0.2,0.2,0.2,1.0);" +
    "}"							+
    "}" ;

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

  if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS) == false)
   {
    var glerror = gl.getShaderInfoLog(fragmentShaderObject);

    if(glerror.length >0)
    {
     alert(glerror);
     uninitialize();
    }
   }

 // Shader Program
  shaderProgramObject = gl.createProgram();
  gl.attachShader(shaderProgramObject,vertexShaderObject);
  gl.attachShader(shaderProgramObject,fragmentShaderObject);

  //pre link
  gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
  gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");
 
   //link
   gl.linkProgram(shaderProgramObject);
   
   if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
   {
    var glerror = gl.getProgramInfoLog(shaderProgramObject);

    if(glerror.length > 0)
    {
        alert(glerror);
        uninitialize();
    }
   }

//get Uniform Location
   mvUniform = gl.getUniformLocation(shaderProgramObject,"u_mv_matrix");

   Projection_Uniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");

   KD_Uniform = gl.getUniformLocation(shaderProgramObject, "u_KD");
   
   Light_Position_Uniform = gl.getUniformLocation(shaderProgramObject, "u_Light_Position");

   LD_Uniform = gl.getUniformLocation(shaderProgramObject, "u_LD");
   
   LisPressed_Uniform = gl.getUniformLocation(shaderProgramObject, "u_LKeyPressed");


//Cube	: Vertices , Colors , Vao & Vbo
 var Vertices = new Float32Array(
       [  //1
		1.0 , 1.0 , 1.0 ,
		-1.0 , 1.0 , 1.0 ,
		-1.0 , -1.0 , 1.0 ,
		1.0 , -1.0 , 1.0 ,
		//2
		1.0 , 1.0 , -1.0 ,
		1.0 , 1.0 , 1.0 ,
		1.0 , -1.0 , 1.0 ,
		1.0 , -1.0 , -1.0 ,
		//3
		-1.0 , 1.0 , -1.0 ,
		1.0 , 1.0 , -1.0 ,
		1.0 , -1.0 , -1.0 ,
		-1.0 , -1.0 , -1.0 ,
		//4
		-1.0 , 1.0 , 1.0 ,
		-1.0 , 1.0 , -1.0 ,
		-1.0 , -1.0 , -1.0 ,
		-1.0 , -1.0 , 1.0 ,
		//5
		-1.0 , 1.0 , 1.0 ,
		1.0 , 1.0 , 1.0 ,
		1.0 , 1.0 , -1.0 ,
		-1.0 , 1.0 , -1.0 ,
		//6
		1.0 , -1.0 , 1.0 ,
		-1.0 , -1.0 , 1.0 ,
		-1.0 , -1.0 , -1.0 ,
		1.0 , -1.0 , -1.0	
	
       ] );

   //vao
   vao_Rect = gl.createVertexArray();
   gl.bindVertexArray(vao_Rect);
 
   //vbo
   vbo_position = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position);
   gl.bufferData(gl.ARRAY_BUFFER,Vertices,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 
                            3, // x,y,z
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

//Color
 var CubeNormals = new Float32Array
 ([
 		0.0 , 0.0 , 1.0 , 
		0.0 , 0.0 , 1.0 , 
		0.0 , 0.0 , 1.0 , 
		0.0 , 0.0 , 1.0 , 
		
		1.0 , 0.0 , 0.0 , 
		1.0 , 0.0 , 0.0 , 
		1.0 , 0.0 , 0.0 , 
		1.0 , 0.0 , 0.0 , 
		
		0.0 , 0.0 , -1.0 , 
		0.0 , 0.0 , -1.0 , 
		0.0 , 0.0 , -1.0 , 
		0.0 , 0.0 , -1.0 , 
		
		-1.0 , 0.0 , 0.0 , 
		-1.0 , 0.0 , 0.0 , 
		-1.0 , 0.0 , 0.0 , 
		-1.0 , 0.0 , 0.0 , 
		
		0.0 , 1.0 , 0.0 , 
		0.0 , 1.0 , 0.0 , 
		0.0 , 1.0 , 0.0 , 
		0.0 , 1.0 , 0.0 , 
		
		0.0 , -1.0 , 0.0 , 
		0.0 , -1.0 , 0.0 , 
		0.0 , -1.0 , 0.0 , 
		0.0 , -1.0 , 0.0
  ]);
  
 // gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);
  
   Vbo_cube_normal = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER,Vbo_cube_normal);
   gl.bufferData(gl.ARRAY_BUFFER,CubeNormals,gl.STATIC_DRAW);
    
   //vaptr
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_NORMAL, 
                            3, // x y z
                            gl.FLOAT, //type
                            false, // normalize
                            0, // Jump
                            0); // Start

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_NORMAL);

    //Unbind
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
  
	//vao
    gl.bindVertexArray(null);


   //final
   gl.clearDepth(1.0);
   gl.enable(gl.DEPTH_TEST);
   gl.depthFunc(gl.LEQUAL);

   gl.clearColor(0.0,0.1,0.0,1.0); 
   PerspectiveProjectionMatrix = mat4.create();
}


//Resize
function Resize()
{
    if(bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else 
    {
        canvas.height = canvas_original_height;
        canvas.width = canvas_original_width;
    }

    //set ViewPort
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(PerspectiveProjectionMatrix,45.0,
        (parseFloat (canvas.width / canvas.height)),
         0.1,100.0);
}


//Draw
function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //Display
    gl.useProgram(shaderProgramObject);

    //I[]
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
	var translateMatrix = mat4.create();
    var RotationMatrix = mat4.create();
    var ProjectionMatrix = mat4.create();
	
//for Rectangle
	 modelViewMatrix = mat4.create();
     modelViewProjectionMatrix = mat4.create();
	 translateMatrix = mat4.create();
     RotationMatrix = mat4.create();

     //Tranform
	mat4.translate(translateMatrix,translateMatrix,[0.0,0.0,-6.5]);

	mat4.rotateX(RotationMatrix,RotationMatrix,degToRad(angle/2));	
    mat4.rotateY(RotationMatrix,RotationMatrix,degToRad(angle/2));
	mat4.rotateZ(RotationMatrix,RotationMatrix,degToRad(angle/2));
    
	mat4.multiply(modelViewMatrix,translateMatrix,modelViewMatrix);
    mat4.multiply(modelViewMatrix,modelViewMatrix,RotationMatrix);
   
    //imp
    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

    //Lights
	if (bisLPressed == true)
	{
		gl.uniform1i(LisPressed_Uniform, 1);
		gl.uniform3f(LD_Uniform, 1.0, 1.0, 1.0);
		gl.uniform3f(KD_Uniform, 0.5, 0.5, 0.5);

        //imp : out to in Light
        var Lightposition = [0.5, 0.0, 2.0, 1.0];
		gl.uniform4fv(Light_Position_Uniform, Lightposition);
	}

	else
	{
		gl.uniform1i(LisPressed_Uniform, 0);
	}

    gl.uniformMatrix4fv(mvUniform,
                        false, // Transpose
                        modelViewMatrix);

    //Projection
        
    gl.uniformMatrix4fv(Projection_Uniform,
        false, // Transpose
        PerspectiveProjectionMatrix);


    gl.bindVertexArray(vao_Rect);

    //scene
    gl.drawArrays(gl.TRIANGLE_FAN,0,4); // index is imp
    gl.drawArrays(gl.TRIANGLE_FAN,4,4);
	gl.drawArrays(gl.TRIANGLE_FAN,8,4);
	gl.drawArrays(gl.TRIANGLE_FAN,12,4);
	gl.drawArrays(gl.TRIANGLE_FAN,16,4);
	gl.drawArrays(gl.TRIANGLE_FAN,20,4);
	
	
    //Unbind
    gl.bindVertexArray(null);
	
	
    gl.useProgram(null);

    angle = angle + 1.0;
     if(angle >= 360.0)
      angle = angle - 360.0;

    //Show
    requestAnimationFrame(draw,canvas);
}

//Degree
function degToRad(degrees)
{
    return(degrees*Math.PI/180);
}


//Uninitialize
function uninitialize()
{
    vao_Rect = null;
    Vbo_cube_normal = null;
	vbo_position = null;
    fragmentShaderObject = null;
    vertexShaderObject = null;
    shaderProgramObject = null;
}

