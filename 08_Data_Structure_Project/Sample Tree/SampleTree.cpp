
// To Create & Insert Nodes in Binary Tree

#include<stdio.h> 
#include<stdlib.h> 

struct node
{
	int key;
	char *value;
	struct node *left, *right;
};

// Struct :  create a new node 
struct node *newNode(int item,char *val)
{
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->key = item;
	temp->value = val;
	temp->left = temp->right = NULL;
	return temp;
}

// Arrange & Print 
void PrintTree(struct node *root)
{
	if (root != NULL)
	{
		PrintTree(root->left);
		printf("\t %d \t", root->key);
		printf("%s \n", root->value);
		PrintTree(root->right);
	}
}

// insert a new node 
struct node* insert(struct node* node, int key, char* value)
{
	if (node == NULL)
	{
		return newNode(key,value);
	}

	/* Otherwise, recur down the tree */
	if (key < node->key)
	{
		node->left = insert(node->left, key,value);
	}
	
	else if (key > node->key)
	{
		node->right = insert(node->right, key,value);
	}
	
	return node; // new node
}

// Main
int main()
{
	//Simple tree
	struct node *root = NULL;
	root = insert(root, 50,"Shantanu"); //Root
	//1st
	insert(root, 30,"Ganga");
	insert(root, 20,"Vyas");
	insert(root, 40,"Bhism");
	insert(root, 70,"Vichitr");
	
	//2nd
	insert(root, 60,"Pandu");
	insert(root, 80,"Dhritrashatra");
	
	//3rd
	insert(root, 55, "Dharma");
	insert(root, 85, "Duryodhan");
	insert(root, 65, "Bhim");
	insert(root, 90, "Dusshasan");
	// print Tree
	PrintTree(root);

	return 0;
}

/* Output 

/*
      50
    /     \
  30      70
 /  \     /   \
20  40   60   80
		 /		\
		55		 85




*/