#pragma once

// ID
#define IDBITMAP_TREE 111
#define IDBITMAP_ICON 112

GLUquadric *Quadric[24];
GLfloat  Light_Ambient[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat  Light_Diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat  Light_Position[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat  Light_Specular[] = { 1.0f,1.0f,1.0f,1.0f };
int gflag = 0;
BOOL gBlend = false;


// Function Declarations
void ToggleFullScreen(void);
void Resize(int, int);
int  Initialize(void);
void UnInitialize(void);
void Display(void);
void Update(void);
void Draw24Spheres();
void DrawChakra(void);
void DrawStars();
