
/*

 Fixed_Functional_Pipeline :  Tree

*/

//Headers
#include<stdio.h>
#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include<math.h>
#include "TreeHeader.h"

#pragma comment (lib,"opengl32.lib")
#pragma comment (lib,"gdi32.lib")
#pragma comment (lib,"user32.lib")
#pragma comment (lib,"glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// Globals
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
FILE *gpFile = NULL;

bool gbActiveWindow = false;
bool gbIsFullScreen = false;
bool gbLight = false;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
GLfloat Light_Angle0 = 0.0f;
GLfloat Light_Angle1 = 0.0f;
GLfloat Light_Angle2 = 0.0f;

//Vectors : Zero
GLfloat  Light_AmbientZero[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat  Light_DiffuseZero[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat  Light_SpecularZero[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat  Light_PositionZero[] = { 0.0f,0.0f,0.0f,1.0f };

//One 
GLfloat  Light_AmbientOne[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat  Light_DiffuseOne[] = { 0.0f,1.0f,0.0f,1.0f };
GLfloat  Light_SpecularOne[] = { 0.0f,1.0f,0.0f,1.0f };
GLfloat  Light_PositionOne[] = { 0.0f,0.0f,0.0f,1.0f };

//Two
GLfloat  Light_AmbientTwo[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat  Light_DiffuseTwo[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat  Light_SpecularTwo[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat  Light_PositionTwo[] = { 0.0f,0.0f,0.0f,1.0f };

// Material
GLfloat  Material_Ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat  Material_Diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat  Material_Specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat  Material_Shininess[] = { 128.0f };

// New
GLfloat Light_Model_Ambient[] = { 0.2f,0.2f,0.2f,1.0f };
GLfloat Light_Model_Local_Viewer = { 0.0f };

GLfloat AngleOfX = 0.0f;
GLfloat AngleOfY = 0.0f;
GLfloat AngleOfZ = 0.0f;
GLint KeyPressed = 0;

// Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIns, LPSTR lpCmdLine, int iCmdShow)
{
	//code

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Tree");
	int iRet = 0;
	bool bDone = false;

	//File IO
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can not Created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	else
	{
		fprintf(gpFile, "Log File Successfully Created");
	}

	// Use rc
	

	//initialize the class
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(hInstance, IDC_HAND);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE (IDBITMAP_TREE));
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.cbWndExtra = 0;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("Tree by Yogeshwar"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100, 100, WIN_WIDTH, WIN_HEIGHT,
		NULL, NULL, hInstance, NULL);

	// For Update
	ghwnd = hwnd;
	iRet = Initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\n ChoosePixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -2)
	{
		fprintf(gpFile, "\n SetPixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -3)
	{
		fprintf(gpFile, "\n wglCreateContext Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -4)
	{
		fprintf(gpFile, "\n wglMakeCurrent Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == 0)
	{
		fprintf(gpFile, "\n Initialization Successful");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd); //UpdateWindow
	SetFocus(hwnd);

	// Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else if (gbActiveWindow == true)
		{
			//update
			Update();  // for Animation
		}

		Display();

	}

	return((int)msg.wParam);
}//Main Ends


 //WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{

	switch (iMsg)
	{
	case WM_CREATE:
		
		/* Splash
		PAINTSTRUCT ps;
		TEXTMETRIC tm;
		RECT rc;
		BITMAP bmp;
		HDC hdcBitmap;
		static HBITMAP hBitmap = LoadBitmap(HINSTANCE(ghdc), MAKEINTRESOURCE(IDBITMAP_ICON));

		ghdc = BeginPaint(ghwnd, &ps);
		hdcBitmap = CreateCompatibleDC(ghdc);
		SelectObject(hdcBitmap, hBitmap);
		GetObject(hBitmap, sizeof(bmp), &bmp);
		GetClientRect(hwnd, &rc);
		StretchBlt(ghdc, 0, 0, rc.right - rc.left, rc.bottom - rc.top, hdcBitmap, 0, 0, bmp.bmWidth, bmp.bmHeight, SRCCOPY);
		DeleteDC(hdcBitmap);
		GetClientRect(hwnd, &rc);
		//SetBkColor(hdc, RGB(0, 0, 255));
		SelectObject(ghdc, CreateFont(50, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Cambria")));
		SetTextColor(ghdc, RGB(200, 0, 0));
		SetBkColor(ghdc, TRANSPARENT);
		SetBkMode(ghdc, TRANSPARENT);
		*/
		
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;
		
		case 'b':
		case 'B':
			if (gBlend == false)
			{
				gBlend = true;
				glEnable(GL_BLEND);
			}
			else
			{
				gBlend = false;
				glDisable(GL_BLEND);
			}

			break;

		case 'd':
		case 'D':
			if(gflag == 0)
				gflag = 1;
			else if (gflag == 1)
				gflag = 0;
			break;

		case 't':
		case 'T':
			/*
			if (gflag == 0)
				gflag = 1;
			else if (gflag == 1)
				gflag = 0;
			*/
			//DrawStars();
			break;

		case 'l':
		case 'L':
			if (gbLight == false)
			{
				gbLight = true;
				glEnable(GL_LIGHTING);
			}

			else
			{
				gbLight = false;
				glDisable(GL_LIGHTING);
			}

			break;

		case 'x':
		case 'X': KeyPressed = 1;
			AngleOfX = 0.0f;
			break;

		case 'y':
		case 'Y': KeyPressed = 2;
			AngleOfY = 0.0f;
			break;

		case 'z':
		case 'Z': KeyPressed = 3;
			AngleOfZ = 0.0f;
			break;

		}
		break;

	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}//WndProc Ends


 //init()
int Initialize(void)
{
	void Resize(int, int); // imp
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;

	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	else if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	//Bridging API
	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return(-3);
	}

	else if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}
	else
		//glClearColor(0.35f, 0.25f, 0.25f, 1.0f); //Grey
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f); //Grey

	// 2
	glClearDepth(1.0f);

	//4
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//5
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// Light Config
	glLightfv(GL_LIGHT0, GL_AMBIENT, Light_Ambient);

	glLightfv(GL_LIGHT0, GL_DIFFUSE, Light_Diffuse);

	//glLightfv(GL_LIGHT0,GL_POSITION,Light_Position);

	glLightfv(GL_LIGHT0, GL_SPECULAR, Light_Specular);

	glEnable(GL_LIGHT0); //similar to Fuse

	for (int i = 0; i < 24; i++)
	{
		Quadric[i] = gluNewQuadric();
	}

	//Warm up call 
	Resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}


//ToggleFullScreen
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	mi.cbSize = sizeof(MONITORINFO);

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//mi = { sizeof(MONITORINFO) };// not supported in VS2008

		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			if ((GetWindowPlacement(ghwnd, &wpPrev)) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),// WD
					(mi.rcMonitor.bottom - mi.rcMonitor.top), // HT
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

		}

		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}

	else
		if (gbIsFullScreen == true)
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

			SetWindowPlacement(ghwnd, &wpPrev);

			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
				SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

			ShowCursor(TRUE);
			gbIsFullScreen = false;

		}
}


//UnInitialize
void UnInitialize(void)
{
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

	}

	//Break Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	for (int i = 0; i < 24; i++)
	{
		gluDeleteQuadric(Quadric[i]);
	}

	/*
	if(Quadric)
	Quadric = NULL;
	*/

	fprintf(gpFile, "\n Application Closed ");
	fclose(gpFile);
	gpFile = NULL;
}


//Resize
void Resize(int Width, int Height)
{
	if (Height == 0)
		Height = 1;

	glViewport(0, 0, GLsizei(Width), GLsizei(Height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (Width < Height)
	{
		glOrtho(0.0f, 15.5f, 0.0f,
			15.5*(GLfloat(Width) / GLfloat(Height)),
			-10.0f, 10.0f);
	}

	/*
	else if (gflag)
			DrawChakra();

	else
	glOrtho(0.0f, 15.5*(GLfloat(Width) / GLfloat(Height)), 0.0f, 15.5f, -10.0f, 10.0f);
*/
	gluPerspective(45.0f, (GLfloat)WIN_WIDTH / (GLfloat)WIN_HEIGHT, 0.1f, 100.0f);
}


//Display
void Display(void)
{
	//5th
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // | GL_ALPHA_TEST_REF);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (KeyPressed == 1)
	{
		glRotatef(AngleOfX, 1.0f, 0.0f, 0.0f);
		Light_Position[1] = AngleOfX; //{1.0f,0.0f,0.0f,1.0f};
		Light_Position[0] = 0.0f;
		Light_Position[2] = 0.0f;
		Light_Position[3] = 0.0f;
	}

	else if (KeyPressed == 2)
	{
		glRotatef(AngleOfY, 0.0f, 1.0f, 0.0f);
		Light_Position[2] = AngleOfY; // {0.0f,1.0f,0.0f,1.0f};
		Light_Position[0] = 0.0f;
		Light_Position[1] = 0.0f;
		Light_Position[3] = 0.0f;
	}

	else if (KeyPressed == 3)
	{
		glRotatef(AngleOfZ, 0.0f, 0.0f, 1.0f);
		Light_Position[0] = AngleOfZ; //{1.0f,0.0f,0.0f,1.0f};
		Light_Position[1] = 0.0f;
		Light_Position[2] = 0.0f;
		Light_Position[3] = 0.0f;
	}

	//Before nodes
	
	glLightfv(GL_LIGHT0, GL_POSITION, Light_Position);
	
	Draw24Spheres(); // most imp
	
	//glTranslatef(5,5,1);
	DrawChakra();
	
	//DrawStars();

	SwapBuffers(ghdc);
}


//Update
void Update(void)
{
	AngleOfX += 0.5f;
	if (AngleOfX >= 360.0f)
		AngleOfX = 0.0f;

	AngleOfY += 0.5f;
	if (AngleOfY >= 360.0f)
		AngleOfY = 0.0f;

	AngleOfZ += 0.5f;
	if (AngleOfZ >= 360.0f)
		AngleOfZ = 0.0f;

}

void DrawChakra(void)
{
	gluPerspective(45.0f, (GLfloat)WIN_WIDTH / (GLfloat)WIN_HEIGHT, 0.1f, 100.0f);
	
	GLfloat angle = 0.0f,MyPI = 3.146f;
	static GLfloat Z_axis = 0.0f;
	const int points = 100;
	int iPts = 0;

	glTranslatef(10.0f, 10.0f, 0.0f);
	// Inner Circle

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -5.0f);

	glBegin(GL_POINTS);
	angle = 2.0f * MyPI;

	for (; angle <= 360.0f; angle += 0.01f)
	{
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(cos(angle), sin(angle), 0.0f);
	}

	glEnd();

	//Outer

	Z_axis += 0.05f;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -2.5f);

	glBegin(GL_LINE_LOOP);

	for (iPts = 0; iPts <= points; iPts++)
	{
		angle = (2.0f * MyPI * iPts * Z_axis) / points;//most imp	

		glColor3f(1.0f, 0.50f, 0.0f);
		glVertex3f(cos(angle), sin(angle), 0.0f);
		glVertex3f(cos(angle), sin(angle), 0.0f);
	}
	glEnd();


}


void DrawStars()
{
	gluPerspective(45.0f, (GLfloat)WIN_WIDTH / (GLfloat)WIN_HEIGHT, 0.1f, 100.0f);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE);	//New

	//future matrix manipulations should affect the modelview matrix 
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0, 0.0, 10.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

	glColor4f(0.5f, 1.0f, 0.5f, 0.1f);

	glPointSize(4.0f);

	glBegin(GL_POINTS);

	for(int ctr = 0; ctr < 100000; ctr++)
	{
		glVertex3f(rand() % 500, rand() % 500, 0);

	}
	glEnd();

/*
	glLoadIdentity();
	gluCylinder(gluNewQuadric(), 15, 100, 10, 20, 2); //gluCylinder(gluNewQuadric(),base,top,height,slices,stacks);

	glTranslatef(20, 10, 1);
	gluCylinder(gluNewQuadric(), 15, 100, 10, 20, 2);
	
	glTranslatef(10, 10, 1);	 
	gluCylinder(gluNewQuadric(), 15, 100, 10, 20, 2);

	glTranslatef(8, 10, 1);
	gluCylinder(gluNewQuadric(), 15, 100, 10, 20, 2);

	glTranslatef(4, 8, 1);
	gluCylinder(gluNewQuadric(), 15, 100, 10, 20, 2);
*/

}




void Draw24Spheres()
{
	GLfloat  Material_Ambient[4]; // {0.0215f,0.1745f,0.0215f,1.0f};
	GLfloat  Material_Diffuse[4]; // {0.0215f,0.1745f,0.0215f,1.0f};
	GLfloat  Material_Specular[4]; //  {1.0f,1.0f,1.0f,1.0f};
	GLfloat  Material_Shininess[1]; // {128.0f};

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// Head : Shantanu ---------------------
	// 1 - Black
	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.0f;
	Material_Ambient[2] = 0.0f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, Material_Ambient);

	Material_Diffuse[0] = 0.01f;
	Material_Diffuse[1] = 0.01f;
	Material_Diffuse[2] = 0.01f;
	Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, Material_Diffuse);

	Material_Specular[0] = 0.5f;
	Material_Specular[1] = 0.5f;
	Material_Specular[2] = 0.5f;
	Material_Specular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, Material_Specular);

	Material_Shininess[0] = 0.25f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(13.0f, 13.5f, 0.0f);
	gluSphere(Quadric[12], 1.2f, 30, 30);

	// 1st Parent : Bhism
	// 3 = Chrome
	Material_Ambient[0] = 0.25f;
	Material_Ambient[1] = 0.25f;
	Material_Ambient[2] = 0.25f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, Material_Ambient);

	Material_Diffuse[0] = 0.4f;
	Material_Diffuse[1] = 0.4f;
	Material_Diffuse[2] = 0.4f;
	Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, Material_Diffuse);

	Material_Specular[0] = 0.774597f;
	Material_Specular[1] = 0.774597f;
	Material_Specular[2] = 0.774597f;
	Material_Specular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, Material_Specular);

	Material_Shininess[0] = 0.6f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(13.0f, 10.0f, 0.0f);
	gluSphere(Quadric[8], 0.8f, 30, 30);


	// 2 = Jade : vyas
	Material_Ambient[0] = 0.135f;
	Material_Ambient[1] = 0.2225f;
	Material_Ambient[2] = 0.1575f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, Material_Ambient);

	Material_Diffuse[0] = 0.54f;
	Material_Diffuse[1] = 0.89f;
	Material_Diffuse[2] = 0.63f;
	Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, Material_Diffuse);

	Material_Specular[0] = 0.316228f;
	Material_Specular[1] = 0.316228f;
	Material_Specular[2] = 0.316228f;
	Material_Specular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, Material_Specular);

	Material_Shininess[0] = 0.1f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.9f, 12.0f, 0.0f);
	gluSphere(Quadric[1], 0.8f, 30, 30);

	
	// 4= Pearl :  Vichitravirya
	Material_Ambient[0] = 0.25f;
	Material_Ambient[1] = 0.20725f;
	Material_Ambient[2] = 0.20725f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, Material_Ambient);

	Material_Diffuse[0] = 1.0f;
	Material_Diffuse[1] = 0.829f;
	Material_Diffuse[2] = 0.829f;
	Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, Material_Diffuse);

	Material_Specular[0] = 0.296648f;
	Material_Specular[1] = 0.296648f;
	Material_Specular[2] = 0.296648f;
	Material_Specular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, Material_Specular);

	Material_Shininess[0] = 0.088f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.9f, 7.0f, 0.0f);
	gluSphere(Quadric[3], 0.8f, 30, 30);


	// 5 = Ruby
	Material_Ambient[0] = 0.1745f;
	Material_Ambient[1] = 0.01175f;
	Material_Ambient[2] = 0.01175f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, Material_Ambient);

	Material_Diffuse[0] = 0.61424f;
	Material_Diffuse[1] = 0.04136f;
	Material_Diffuse[2] = 0.04136f;
	Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, Material_Diffuse);

	Material_Specular[0] = 0.727811f;
	Material_Specular[1] = 0.626959f;
	Material_Specular[2] = 0.626959f;
	Material_Specular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, Material_Specular);

	Material_Shininess[0] = 0.6f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.9f, 4.5f, 0.0f);
	gluSphere(Quadric[4], 0.8f, 30, 30);


	// 6 = Turquoise
	Material_Ambient[0] = 0.1f;
	Material_Ambient[1] = 0.18275f;
	Material_Ambient[2] = 0.1745f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, Material_Ambient);

	Material_Diffuse[0] = 0.396f;
	Material_Diffuse[1] = 0.74151f;
	Material_Diffuse[2] = 0.69102f;
	Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, Material_Diffuse);

	Material_Specular[0] = 0.297524f;
	Material_Specular[1] = 0.30829f;
	Material_Specular[2] = 0.306678f;
	Material_Specular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, Material_Specular);

	Material_Shininess[0] = 0.1f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.9f, 2.0f, 0.0f);
	gluSphere(Quadric[5], 0.8f, 30, 30);

	
	// 4= copper
	Material_Ambient[0] = 0.19125f;
	Material_Ambient[1] = 0.0735f;
	Material_Ambient[2] = 0.02025f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, Material_Ambient);

	Material_Diffuse[0] = 0.7038f;
	Material_Diffuse[1] = 0.27048f;
	Material_Diffuse[2] = 0.0828f;
	Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, Material_Diffuse);

	Material_Specular[0] = 0.25677f;
	Material_Specular[1] = 0.137622f;
	Material_Specular[2] = 0.086014f;
	Material_Specular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, Material_Specular);

	Material_Shininess[0] = 0.1f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.0f, 7.0f, 0.0f);
	gluSphere(Quadric[9], 0.8f, 30, 30);


	// 6 = Silver
	Material_Ambient[0] = 0.19225f;
	Material_Ambient[1] = 0.19225f;
	Material_Ambient[2] = 0.19225f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, Material_Ambient);

	Material_Diffuse[0] = 0.5075f;
	Material_Diffuse[1] = 0.5075f;
	Material_Diffuse[2] = 0.5075f;
	Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, Material_Diffuse);

	Material_Specular[0] = 0.50828f;
	Material_Specular[1] = 0.50828f;
	Material_Specular[2] = 0.50828f;
	Material_Specular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, Material_Specular);

	Material_Shininess[0] = 0.4f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.0f, 2.0f, 0.0f);
	gluSphere(Quadric[11], 0.8f, 30, 30);

	
	// Red

	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.0f;
	Material_Ambient[2] = 0.0f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, Material_Ambient);

	Material_Diffuse[0] = 0.5f;
	Material_Diffuse[1] = 0.0f;
	Material_Diffuse[2] = 0.0f;
	Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, Material_Diffuse);

	Material_Specular[0] = 0.7f;
	Material_Specular[1] = 0.6f;
	Material_Specular[2] = 0.6f;
	Material_Specular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, Material_Specular);

	Material_Shininess[0] = 0.25f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(17.0f, 7.0f, 0.0f);
	gluSphere(Quadric[15], 0.8f, 30, 30);


	// White
	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.0f;
	Material_Ambient[2] = 0.0f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, Material_Ambient);

	Material_Diffuse[0] = 0.55f;
	Material_Diffuse[1] = 0.55f;
	Material_Diffuse[2] = 0.55f;
	Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, Material_Diffuse);

	Material_Specular[0] = 0.7f;
	Material_Specular[1] = 0.7f;
	Material_Specular[2] = 0.7f;
	Material_Specular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, Material_Specular);

	Material_Shininess[0] = 0.25f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(17.0f, 4.5f, 0.0f);
	gluSphere(Quadric[16], 0.8f, 30, 30);

	
	// Yellow
	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.0f;
	Material_Ambient[2] = 0.0f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, Material_Ambient);

	Material_Diffuse[0] = 0.5f;
	Material_Diffuse[1] = 0.5f;
	Material_Diffuse[2] = 0.0f;
	Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, Material_Diffuse);

	Material_Specular[0] = 0.6f;
	Material_Specular[1] = 0.6f;
	Material_Specular[2] = 0.5f;
	Material_Specular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, Material_Specular);

	Material_Shininess[0] = 0.25f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(17.0f, 2.0f, 0.0f);
	gluSphere(Quadric[17], 0.8f, 30, 30);


	// -------- Rubber

	// 2 - Cyan
	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.05f;
	Material_Ambient[2] = 0.05f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, Material_Ambient);

	Material_Diffuse[0] = 0.4f;
	Material_Diffuse[1] = 0.5098039f;
	Material_Diffuse[2] = 0.5098039f;
	Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, Material_Diffuse);

	Material_Specular[0] = 0.0f;
	Material_Specular[1] = 0.501960f;
	Material_Specular[2] = 0.501960f;
	Material_Specular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, Material_Specular);

	Material_Shininess[0] = 0.07813f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(24.3f, 12.0f, 0.0f);
	gluSphere(Quadric[19], 0.8f, 30, 30);


	// 4- Red

	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.0f;
	Material_Ambient[2] = 0.0f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, Material_Ambient);

	Material_Diffuse[0] = 0.5f;
	Material_Diffuse[1] = 0.0f;
	Material_Diffuse[2] = 0.0f;
	Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, Material_Diffuse);

	Material_Specular[0] = 0.7f;
	Material_Specular[1] = 0.6f;
	Material_Specular[2] = 0.6f;
	Material_Specular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, Material_Specular);

	Material_Shininess[0] = 0.07813f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(24.3f, 7.0f, 0.0f);
	gluSphere(Quadric[21], 0.8f, 30, 30);


	// 6- Yellow
	Material_Ambient[0] = 0.0f;
	Material_Ambient[1] = 0.0f;
	Material_Ambient[2] = 0.0f;
	Material_Ambient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, Material_Ambient);

	Material_Diffuse[0] = 0.5f;
	Material_Diffuse[1] = 0.5f;
	Material_Diffuse[2] = 0.0f;
	Material_Diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, Material_Diffuse);

	Material_Specular[0] = 0.6f;
	Material_Specular[1] = 0.6f;
	Material_Specular[2] = 0.5f;
	Material_Specular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, Material_Specular);

	Material_Shininess[0] = 0.07813f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, Material_Shininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(24.3f, 2.0f, 0.0f);
	gluSphere(Quadric[23], 0.8f, 30, 30);

}



