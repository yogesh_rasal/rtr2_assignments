
/*

Assignment : Fixed_Functional_Pipeline : Dynamic India

*/


#include <Windows.h>
#include <math.h>
#include "gl/GL.h"
#include "gl/GLU.h"
#include "Dynamic.h"


#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "Winmm.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define MyPI 3.1456

//all done
static bool AllDone = false;


// global variables
HWND ghwnd = NULL;
bool gbIsFullScreen = false;
DWORD gdwStyle;
WINDOWPLACEMENT gwpPrev = { sizeof(WINDOWPLACEMENT) };
HDC ghdc = NULL;
HGLRC ghrc = NULL; 
bool gbActiveWindow = false;
FILE* gpFile = NULL;

//Fun Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Display(void);
void ToggleFullScreen(void);
void staticIndia(void);


// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// functions
	int initialize(void);


	// variables
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("Dynamic India");
	HWND hwnd;
	MSG msg;
	bool bDone = false;
	int iRet = 0;

	// code
	if (fopen_s(&gpFile, "Logs.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File cannot be created"),TEXT("Error"),MB_OK);
		exit(0);
	}

	else
	{
		fprintf(gpFile, "Log file created Successfully.\n");
	}

	// Window class initialization
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
				szAppName,TEXT("Dynamic India : By Yogeshwar"),
				WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
				100,100,WIN_WIDTH,WIN_HEIGHT,
				NULL,NULL,hInstance,NULL);

	ghwnd = hwnd;
	
	iRet = initialize();
	ToggleFullScreen(); // for Fullscreen

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat() failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext() failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent() failed\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initialization successfull\n");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// start the game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//  update();
			}

			Display();
		}
	}
	return((int)msg.wParam);
}// Main Ends

//Callback
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Fwd Decl
	void Resize(int, int);
	void uninitialize(void);

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'f':
		case 'F':
			ToggleFullScreen();
			break;
		}
		break;

	case WM_ERASEBKGND:
		return(0);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


int initialize(void)
{
	 // function declarations
	void Resize(int, int);

	// variables
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR)); 
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1; 
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	// get the normal device context
	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd); 
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	// use bridging api 
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	// set ghrc 
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); 
	PlaySound("music.wav", NULL, SND_ASYNC | SND_SYSTEM);

	//warmup call
	Resize(WIN_WIDTH, WIN_HEIGHT); 
	return(0);
}

//
void uninitialize(void)
{
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, gdwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &gwpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,0,0,0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
			
		ShowCursor(TRUE);
	}
	
	// break the current context 
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	
	fprintf(gpFile, "Log File is being closed\n");
	fclose(gpFile);
	gpFile = NULL;
}


//ToggleFullScreen
void ToggleFullScreen(void)
{
	MONITORINFO mi = { sizeof(MONITORINFO) };
	
	if (gbIsFullScreen == false)
	{
		gdwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (gdwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &gwpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, gdwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
				ShowCursor(FALSE);
				gbIsFullScreen = true;
			}
		}
	}

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, gdwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &gwpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,0,	0,0,0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbIsFullScreen = false;
	}
}


// Resize 
void Resize(int width, int height)
{
	// code
	if (height == 0)
	{
		height = 1; 
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,((GLfloat)width / (GLfloat)height),0.1f,100.0f);
}


//Drawing
void Display(void)
{

	glClear(GL_COLOR_BUFFER_BIT);

	// variables
	static float ITrans = -2.5f;
	static float ax = 2.0f;
	static float ny = 1.0f;
	static float iy = -1.0f;
	
	static float r = 0.0f;
	static float g = 0.0f;
	const float stepRed = 0.0002;
	const float stepGreen = 0.0001;
	
	const float step = 0.0003f;
		
	static bool bFirstIDone = false;
	static bool bADone = false;
	static bool bNDone = false;
	static bool bSecondIDone = false;
	static bool bDDone = false;
	static bool bPlanes = false;	

	// I
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(ITrans, 0.0f, -3.0f);

	glBegin(GL_QUADS);
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.05f, 0.5f, 0.0f);
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.05f, 0.5f, 0.0f);
		glColor3f(0.0f, 0.5f, 0.0f);
		glVertex3f(-0.05f, -0.5f, 0.0f);
		glColor3f(0.0f, 0.5f, 0.0f);
		glVertex3f(0.05f, -0.5f, 0.0f);
	glEnd();
	
	if(ITrans < -1.35f)
	{
		ITrans = ITrans + step;
	}

	if(ITrans >= -1.35f)
	{
		bFirstIDone = true;
	}
	
		
	// N
	if(bADone || bNDone)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.8f, ny, -3.0f);

		glBegin(GL_QUADS);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.3f, 0.5f, 0.0f);
			glVertex3f(0.2f, 0.5f, 0.0f);
		
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.2f, -0.5f, 0.0f);	
			glVertex3f(0.3f, -0.5f, 0.0f);
		glEnd();
		
		
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.8f, ny, -3.0f);

		glBegin(GL_QUADS);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.2f, 0.5f, 0.0f);
			glVertex3f(-0.3f, 0.5f, 0.0f);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(-0.3f, -0.5f, 0.0f);	
			glVertex3f(-0.2f, -0.5f, 0.0f);
		glEnd();
		
		
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.8f, ny, -3.0f);

		glBegin(GL_QUADS);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.1f, 0.5f, 0.0f);
			glVertex3f(-0.22f, 0.5f, 0.0f);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.1f, -0.5f, 0.0f);	
			glVertex3f(0.22f, -0.5f, 0.0f);
		glEnd();
		
		if(ny > 0.0f)
		{
			ny = ny - step;
		}
		if(ny <= 0.0f)
		{
			bNDone = true;
		}
		
	}
	
	
	// D
	if(bSecondIDone || bDDone)
	{		
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -3.0f);

		// Line
		glBegin(GL_QUADS);
			glColor3f(r, g, 0.0f);
			glVertex3f(-0.3f, 0.5f, 0.0f);
			glVertex3f(-0.2f, 0.5f, 0.0f);
		
			glColor3f(0.0f, g, 0.0f);
			glVertex3f(-0.2f, -0.5f, 0.0f);	
			glVertex3f(-0.3f, -0.5f, 0.0f);
		
		glEnd();

		// Half	
		glBegin(GL_QUAD_STRIP);
			glColor3f(r, g, 0.0f); //for Fedding
			glVertex3f(-0.2f, 0.4f, 0.0f);
			glVertex3f(-0.2f, 0.5f, 0.0f);
			glVertex3f(0.0f, 0.4f, 0.0f);
			glVertex3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.15f, 0.3f, 0.0f);
			glVertex3f(0.25f, 0.36f, 0.0f);
		
			glColor3f(0.0f, g, 0.0f);
			glVertex3f(0.15f, -0.3f, 0.0f);
			glVertex3f(0.25f, -0.36f, 0.0f);
			glVertex3f(0.0f, -0.4f, 0.0f);
			glVertex3f(0.0f, -0.5f, 0.0f);
			glVertex3f(-0.2f, -0.4f, 0.0f);
			glVertex3f(-0.2f, -0.5f, 0.0f);
		glEnd();
		
		if( (r >= 0.0f) && (r < 1.01f))
		{
			r = r + stepRed;
		}

		if( (g >= 0.0f) && (g < 0.51f))
		{
			g = g + stepGreen;
		}

		if((r >= 1.0f) && (g >= 0.5f))
		{
			bDDone = true;
		}
	}
		
	
	// I
	if(bNDone || bSecondIDone)
	{	
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.5f, iy, -3.0f);

		glBegin(GL_QUADS);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.05f, 0.5f, 0.0f);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.05f, 0.5f, 0.0f);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(-0.05f, -0.5f, 0.0f);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.05f, -0.5f, 0.0f);
		glEnd();
		
		if(iy < 0.0f)
		{
			iy = iy + step;
		}

		if(iy >= 0.0f)
		{
			bSecondIDone = true;
		}
	}
	
	
	// A
	
	if(bFirstIDone || bADone)
	{
		
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(ax, 0.0f, -3.0f);

		glBegin(GL_QUADS);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(-0.2f, -0.5f, 0.0f);
			glVertex3f(-0.3f, -0.5f, 0.0f);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.1f, 0.5f, 0.0f);
			glVertex3f(0.02f, 0.5f, 0.0f);
		glEnd();
		
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(ax, 0.0f, -3.0f);

		glBegin(GL_QUADS);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.2f, -0.5f, 0.0f);
			glVertex3f(0.3f, -0.5f, 0.0f);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.1f, 0.5f, 0.0f);
			glVertex3f(-0.02f, 0.5f, 0.0f);
		glEnd();
		
		
		if(ax > 1.0f)
		{
			ax = ax - step;
		}
		if(ax <= 1.0f)
		{
			bADone = true;
		}
	}		
		
	// ------------------------- Planes-------------------------------
	
		
	if (bDDone)
	{	

	//upper
		DrawUpPlane();

	//Lower
		DrawDownPlane();		

	//mid
		AnimatePlanes();

		bPlanes = true;	
	}

	
	SwapBuffers(ghdc);
}


// Curved Planes
void DrawUpPlane()
{	
	static float one = 0.1f;
	static float x = -0.9f;
			
		//------------- Testing path 1

		glLoadIdentity();

		if ((one <= 90.0f))
		{
			glTranslatef(1.0f * cos((one * MyPI / 180.0f) + MyPI) - 0.9f, 
						 1.0f * sin((one * MyPI / 180.0f) + MyPI) + 1.0f, 
						-3.0f);
		
			StaticPlane();
		}

/*
		else
		{
			glTranslatef(x, 0.0f, -3.0f);
			x = x + 0.0001f;
		}
*/

		if (one <= 90.0f)
			one += 0.035f;

}


//2nd Plane
void DrawDownPlane(void)
{
	static float two = 180.0f;
	bool DemoPath2 = true;
	static float x = -0.9f;

		//-------------------Testing Path 2

		glLoadIdentity();

		if (DemoPath2 && (two >= 90.0f))
		{
			glTranslatef(1.0f * cos((two * MyPI / 180.0f)) - 0.9f, 
						 1.0f * sin((two * MyPI / 180.0f)) - 1.0f,
						-3.0f);
					
			StaticPlane();
		}
	
/*		else
		{
			glTranslatef(x, 0.0f, -3.0f);
			x = x + 0.0001f;
		}
*/

		if (two >= 90.0f)
			two -= 0.035f;

}

// Drawing Static Plane
void StaticPlane()
{
	// Head
	glBegin(GL_TRIANGLES);
		glColor3f(pRed,pGreen,pBlue);
		glVertex3f(0.15f, 0.0f, 0.0f);
		glVertex3f(0.05f, 0.05f, 0.0f);
		glVertex3f(0.05f, -0.05f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
		glColor3f(pRed, pGreen, pBlue);
		glVertex3f(0.05f, 0.05f, 0.0f);
		glVertex3f(-0.15f, 0.05f, 0.0f);
		glVertex3f(-0.15f, -0.05f, 0.0f);
		glVertex3f(0.05f, -0.05f, 0.0f);
	glEnd();


	// Text 
	// I
	glLineWidth(1.0f);

	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.1f, 0.04f, 0.0f);
		glVertex3f(-0.1f, -0.04f, 0.0f);
	glEnd();


	// A
	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.07f, 0.0f, 0.0f);
		glVertex3f(-0.05f, 0.0f, 0.0f);
	glEnd();


	glBegin(GL_LINE_STRIP);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.08f, -0.04f, 0.0f);
		glVertex3f(-0.06f, 0.04f, 0.0f);
		glVertex3f(-0.04f, -0.04f, 0.0f);
	glEnd();

	// F
	glBegin(GL_LINE_STRIP);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.02f, -0.04f, 0.0f);
		glVertex3f(-0.02f, 0.04f, 0.0f);
		glVertex3f(0.02f, 0.04f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.02f, 0.0f, 0.0f);
		glVertex3f(0.01f, 0.0f, 0.0f);
	glEnd();

	//Wings
	glBegin(GL_TRIANGLES);
		glColor3f(pRed, pGreen, pBlue);
		glVertex3f(-0.12f, 0.05f, 0.0f);
		glVertex3f(-0.12f, 0.15f, 0.0f);
		glVertex3f(-0.05f, 0.05f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
		glColor3f(pRed, pGreen, pBlue);
		glVertex3f(-0.12f, -0.05f, 0.0f);
		glVertex3f(-0.12f, -0.15f, 0.0f);
		glVertex3f(-0.05f, -0.05f, 0.0f);
	glEnd();


	//Body
	glBegin(GL_QUADS);
		glColor3f(pRed, pGreen, pBlue);
		glVertex3f(-0.15f, 0.05f, 0.0f);
		glVertex3f(-0.18f, 0.08f, 0.0f);
		glVertex3f(-0.18f, -0.08f, 0.0f);
		glVertex3f(-0.15f, -0.05f, 0.0f);
	glEnd();


	//Tail Flag
	glLineWidth(4.5f);

		// Orange
		glBegin(GL_LINES);
			glColor3f(1.0, 0.5, 0.0f);
			glVertex3f(-0.20f, 0.02f, 0.0f);
			glVertex3f(-0.28f, 0.02f, 0.0f);
		glEnd();

		//Green
		glBegin(GL_LINES);
			glColor3f(0.0f, 0.5, 0.0f);
			glVertex3f(-0.20f, -0.025f, 0.0f);
			glVertex3f(-0.28, -0.025f, 0.0f);
		glEnd();

		//White
		glBegin(GL_LINES);
			glColor3f(1,1,1);
			glVertex3f(-0.20f, 0.0f, 0.0f);
			glVertex3f(-0.28f, 0.0f, 0.0f);
		glEnd();


}


// Animate Planes
void AnimatePlanes(void)
{	
	static GLfloat transPlaneX = -2.0f; //1
    static GLfloat flagx2 = -0.3f;
	static GLfloat flagx1 = -1.25f;
  
	static bool bplane = false; //2
	static bool bplane2 = false;

	static GLfloat fR = 1.0f, fG = 0.5f , fB = 0.0f; //3


	// Plane
    glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(transPlaneX, 0.0f, -3.0f);

	StaticPlane();

    //most imp
	transPlaneX += 0.00015; //Movement

    if(transPlaneX > -1.05f)
    {
		flagx2 = -1.35f;

		//-------------------------------------Mid flag--------------------------------------
		glLoadIdentity();
		glTranslatef(0.0, 0.0f, -3.0f);
		glLineWidth(7.0f);

		// Orange
		glBegin(GL_LINES);
			glColor3f(fR, fG, 0.0f);
			glVertex3f(flagx1, 0.03f, 0.0f);
			glVertex3f(flagx2, 0.03f, 0.0f);
		glEnd();

		//White
		glBegin(GL_LINES);
			glColor3f(fR, fR,fR);
			glVertex3f(flagx1, 0.0f, 0.0f);
			glVertex3f(flagx2, 0.0f, 0.0f);
		glEnd();

		//Green
		glBegin(GL_LINES);
			glColor3f(0.0f, fG, 0.0f);
			glVertex3f(flagx1, -0.025f, 0.0f);
			glVertex3f(flagx2, -0.025f, 0.0f);
		glEnd();

		// at Last
		if (transPlaneX > 2.30f)
		{
			flagx1 = 2.10f;
   		}

	    else 
		    flagx1 += 0.00015f; // flag Movement

		
		// Enable Testpaths
			if(transPlaneX > 1.350f)
			{
				bplane = true;
				bplane2 = true;
			}	


	// Reverse flag Fedding
	 //if (flagx1 > 2.09f && 
	 if(transPlaneX > 2.31f)
	  {
		static float rf = 1.0f;
		static float gf = 0.5f;
		const float stepRed = 0.002;
		const float stepGreen = 0.001;
	

			flagx1 -= 0.9f;
			
			if(fR > 0.0f)
			{
				fR -= 0.5f;
				fG -= 0.5f; 
			}

			 staticIndia();
   	/*	
	
		   		// Fedding
		glBegin(GL_LINES);
			glColor3f(rf, gf, 0.0f); //for Fedding
			glVertex3f(flagx1, -0.025f, 0.0f);
			glVertex3f(flagx2, -0.025f, 0.0f);
		glEnd();

		
		glBegin(GL_LINES);
			glColor3f(0.0f, gf, 0.0f);
			glVertex3f(flagx1, -0.025f, 0.0f);
			glVertex3f(flagx2, -0.025f, 0.0f);
		glEnd();
		
		if((rf >= 0.0f) && (rf < 1.01f))
		{
			rf = rf - stepRed;
		}

		if((gf >= 0.0f) && (gf < 0.51f))
		{
			gf = gf - stepGreen;
		}
   */
	 }
	
    }

    else
	  {
		//-------------------------------------Flag--------------------------------------
		glLoadIdentity();
		glTranslatef(transPlaneX, 0.0f, -3.0f);
		glLineWidth(7.0f);

		// Orange
		glBegin(GL_LINES);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.2f, 0.03f, 0.0f);
			glVertex3f(flagx2, 0.03f, 0.0f);
		glEnd();

		//White
		glBegin(GL_LINES);
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(-0.2f, 0.0f, 0.0f);
			glVertex3f(flagx2, 0.0f, 0.0f);
		glEnd();

		//Green
		glBegin(GL_LINES);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(-0.2f, -0.025f, 0.0f);
			glVertex3f(flagx2, -0.025f, 0.0f);
		glEnd();

		
	}


 // Invert Planes
	
	if(bplane)
	{		

		//------------- Testing path 3 using Parabola
		static GLfloat x4 = 1.67f;
		static GLfloat y4 = 0.0f;
		static GLfloat angle2 = 0.0f;

		x4 = x4 + 0.0001f;
		y4 = (x4*x4) - 3.0f;

		glLoadIdentity();
		glTranslatef(x4,y4,-3.0f);
		glRotatef(angle2,0.0f,0.0f,0.8f);
		
		StaticPlane();	

		if(angle2 <= 90.0f)
			angle2 = angle2 + 0.009f;

	}


//------------- Testing path 3 using Parabola

	if(bplane2)
	{	   
		
		static GLfloat x3 = 1.67f;
		static GLfloat y3 = 0.0f;
		static GLfloat angle = 0.0f;

		x3 = x3 + 0.0001f;
		y3 = -(x3*x3) + 3.0f;

		glLoadIdentity();
		glTranslatef(x3,y3,-3.0f);
		glRotatef(-angle,0.0f,0.0f,0.8f);
		
		StaticPlane();	

		if(angle >= -90.0f)
			angle = angle + 0.009f;	

	}
	//else staticIndia();
	
}

//Final
void staticIndia(void)
{
	// I
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.3f, 0.0f, -3.0f);

	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.0f, 0.5f, 0.0f);
		glVertex3f(-0.05f, 0.5f, 0.0f);
		
		glColor3f(0.0f, 0.5f, 0.0f);
		glVertex3f(-0.05f, -0.5f, 0.0f);
		glVertex3f(0.0f, -0.5f, 0.0f);

	glEnd();

	
	// N
	
		// |

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.8f, 0.0f, -3.0f);

		glBegin(GL_QUADS);

			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.3f, 0.5f, 0.0f);
			glVertex3f(0.2f, 0.5f, 0.0f);

			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.2f, -0.5f, 0.0f);
			glVertex3f(0.3f, -0.5f, 0.0f);

		glEnd();

		// \

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.8f, 0.0f, -3.0f);

		glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.2f, 0.5f, 0.0f);
		glVertex3f(-0.3f, 0.5f, 0.0f);
		glColor3f(0.0f, 0.5f, 0.0f);
		glVertex3f(-0.3f, -0.5f, 0.0f);
		glVertex3f(-0.2f, -0.5f, 0.0f);

		glEnd();

		// |

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.8f, 0.0f, -3.0f);

		glBegin(GL_QUADS);

			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.1f, 0.5f, 0.0f);
			glVertex3f(-0.22f, 0.5f, 0.0f);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.1f, -0.5f, 0.0f);
			glVertex3f(0.22f, -0.5f, 0.0f);

		glEnd();
		

	// D
	
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -3.0f);

		// Line
		glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.3f, 0.5f, 0.0f);
		glVertex3f(-0.2f, 0.5f, 0.0f);

		glColor3f(0.0f, 0.5f, 0.0f);
		glVertex3f(-0.2f, -0.5f, 0.0f);
		glVertex3f(-0.3f, -0.5f, 0.0f);

		glEnd();

		// Half	
		glBegin(GL_QUAD_STRIP);
			
			glColor3f(1.0f, 0.5f, 0.0f);//Orange
			glVertex3f(-0.2f, 0.4f, 0.0f);
			glVertex3f(-0.2f, 0.5f, 0.0f);
			glVertex3f(0.0f, 0.4f, 0.0f);
			glVertex3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.15f, 0.3f, 0.0f);
			glVertex3f(0.25f, 0.36f, 0.0f);

			glColor3f(0.0f, 0.5f, 0.0f); // Green
			glVertex3f(0.15f, -0.3f, 0.0f);
			glVertex3f(0.25f, -0.36f, 0.0f);
			glVertex3f(0.0f, -0.4f, 0.0f);
			glVertex3f(0.0f, -0.5f, 0.0f);
			glVertex3f(-0.2f, -0.4f, 0.0f);
			glVertex3f(-0.2f, -0.5f, 0.0f);

		glEnd();


	// I

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.5f, 0.0f, -3.0f);

		glBegin(GL_QUADS);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.05f, 0.5f, 0.0f);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.05f, 0.5f, 0.0f);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(-0.05f, -0.5f, 0.0f);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.05f, -0.5f, 0.0f);
		glEnd();


	// A
		
		//   strips

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, 0.0f, -3.0f);
		glLineWidth(7.0f);

		glBegin(GL_LINES);
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(-0.093f, 0.0f, 0.0f);
			glVertex3f(0.093f, 0.0f, 0.0f);
		glEnd();

		glPointSize(5.0f);
		glBegin(GL_POINTS);
			glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);
		glEnd();


		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, 0.0f, -3.0f);
		glLineWidth(6.0f);

		glBegin(GL_LINES);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.088f, 0.021f, 0.0f);
			glVertex3f(0.088f, 0.021f, 0.0f);
		glEnd();


		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, 0.0f, -3.0f);
		glLineWidth(7.0f);

		glBegin(GL_LINES);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(-0.097f, -0.02f, 0.0f);
			glVertex3f(0.097f, -0.02f, 0.0f);
		glEnd();

		//  /\

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, 0.0f, -3.0f);

		glBegin(GL_QUADS);

			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(-0.2f, -0.5f, 0.0f);
			glVertex3f(-0.3f, -0.5f, 0.0f);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.1f, 0.5f, 0.0f);
			glVertex3f(0.02f, 0.5f, 0.0f);

		glEnd();


		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, 0.0f, -3.0f);

		glBegin(GL_QUADS);

			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.2f, -0.5f, 0.0f);
			glVertex3f(0.3f, -0.5f, 0.0f);

			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.1f, 0.5f, 0.0f);
			glVertex3f(-0.02f, 0.5f, 0.0f);

		glEnd();


}

//EOF
