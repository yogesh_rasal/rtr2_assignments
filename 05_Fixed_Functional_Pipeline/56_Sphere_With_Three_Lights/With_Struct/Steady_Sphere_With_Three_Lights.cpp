
/*

    Assignment : Fixed_Functional_Pipeline :  Lights

*/

//Headers
#include<stdio.h>
#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>

#pragma comment (lib,"opengl32.lib")
#pragma comment (lib,"gdi32.lib")
#pragma comment (lib,"user32.lib")
#pragma comment (lib,"glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// Globals
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
FILE *gpFile = NULL;

bool gbActiveWindow = false;
bool gbIsFullScreen = false;
bool gbLight = false;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

GLUquadric *Quadric = NULL;


//Vectors 
struct Light 
{
 GLfloat  Light_Ambient[4];
 GLfloat  Light_Diffuse[4];
 GLfloat  Light_Position[4];
 GLfloat  Light_Specular[4];
 GLfloat  Light_Angle;
}lights[3];


// Material
GLfloat  Material_Ambient[]  = {0.0f,0.0f,0.0f,1.0f};
GLfloat  Material_Diffuse[]  = {1.0f,1.0f,1.0f,1.0f};
GLfloat  Material_Specular[] = {1.0f,1.0f,1.0f,1.0f};
GLfloat  Material_Shininess[] = {128.0f};

// Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int Initialize(void);
void UnInitialize(void);
void Display(void);
void Update(void);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIns, LPSTR lpCmdLine, int iCmdShow)
{
    //code
    
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Sphere");
	int iRet = 0;
	bool bDone = false;

	//File IO
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can not Created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	else
	{
		fprintf(gpFile, "Log File Successfully Created");
	}

	//initialize the class
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(hInstance, IDC_HAND);
	wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.cbWndExtra = 0;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		        TEXT("Sphere_With_Three_Lights by Yogeshwar"),
		        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		        100, 100, WIN_WIDTH, WIN_HEIGHT,
		        NULL, NULL, hInstance, NULL);

	// For Update
	ghwnd = hwnd;
	iRet = Initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\n ChoosePixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -2)
	{
		fprintf(gpFile, "\n SetPixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -3)
	{
		fprintf(gpFile, "\n wglCreateContext Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -4)
	{
		fprintf(gpFile, "\n wglMakeCurrent Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == 0)
	{
		fprintf(gpFile, "\n Initialization Successful");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd); //UpdateWindow
	SetFocus(hwnd);

	// Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else if (gbActiveWindow == true)
		{
			//update
			Update();  // for Animation
		}
	
		Display();
		
	}

	return((int)msg.wParam);
}//Main Ends


 //WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function Declarations
	void ToggleFullScreen(void);
	void Resize(int, int);

  switch (iMsg)
   {
	case WM_CREATE:
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
	  switch (wParam)
	   {
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;

        case 'l':
        case 'L':
                if(gbLight == false)
                {
                    gbLight = true;
                    glEnable(GL_LIGHTING);  
                }

               else 
                {
                    gbLight = false;
                    glDisable(GL_LIGHTING);  
                }

                break;
		}
	   break;

	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}//WndProc Ends


 //init()
int Initialize(void)
{
	void Resize(int, int); // imp
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;

	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	else if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	//Bridging API
	ghrc = wglCreateContext(ghdc);
	
    if (ghrc == NULL)
	{
		return(-3);
	}

	else if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}
	else
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);//Black

	// 2
	glClearDepth(1.0f);

	//4
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//5
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	//6
	// Zero
   lights[0] = {{0.0f,0.0f,0.0f,1.0f},{1.0f,0.0f,0.0f,1.0f}, {1.0f,0.0f,0.0f,1.0f},{0.0f,0.0f,0.0f,1.0f},0.0f};

	//one
	lights[1] = {{0.0f,0.0f,0.0f,1.0f},{0.0f,1.0f,0.0f,1.0f}, {0.0f,1.0f,0.0f,1.0f},{0.0f,0.0f,0.0f,1.0f},0.0f};

	//Two
	lights[2] = {{0.0f,0.0f,0.0f,1.0f},{0.0f,0.0f,1.0f,1.0f}, {0.0f,0.0f,1.0f,1.0f},{0.0f,0.0f,0.0f,1.0f},0.0f};

 // Red Light Config
    glLightfv(GL_LIGHT0,GL_AMBIENT,lights[0].Light_Ambient);   

    glLightfv(GL_LIGHT0,GL_DIFFUSE,lights[0].Light_Diffuse);

    glLightfv(GL_LIGHT0,GL_SPECULAR,lights[0].Light_Specular);

	glEnable(GL_LIGHT0);
    
	//  Green Light Config
    glLightfv(GL_LIGHT1,GL_AMBIENT,lights[1].Light_Ambient);   

    glLightfv(GL_LIGHT1,GL_DIFFUSE,lights[1].Light_Diffuse);

    glLightfv(GL_LIGHT1,GL_SPECULAR,lights[1].Light_Specular);

	 //similar to Fuse
	glEnable(GL_LIGHT1); 


    //  Blue Light Config
	glLightfv(GL_LIGHT2,GL_AMBIENT,lights[2].Light_Ambient);   

    glLightfv(GL_LIGHT2,GL_DIFFUSE,lights[2].Light_Diffuse);

    glLightfv(GL_LIGHT2,GL_SPECULAR,lights[2].Light_Specular);

	 //similar to Fuse
	glEnable(GL_LIGHT2); 


    // Material
    glMaterialfv(GL_FRONT,GL_AMBIENT,Material_Ambient);

    glMaterialfv(GL_FRONT,GL_DIFFUSE,Material_Diffuse);
    
    glMaterialfv(GL_FRONT,GL_SPECULAR,Material_Specular);

    glMaterialfv(GL_FRONT,GL_SHININESS,Material_Shininess);

	    
	//Warm up call 
	Resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}


//ToggleFullScreen
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	mi.cbSize = sizeof(MONITORINFO);

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//mi = { sizeof(MONITORINFO) };// not supported in VS2008

		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			if ((GetWindowPlacement(ghwnd, &wpPrev)) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),// WD
					(mi.rcMonitor.bottom - mi.rcMonitor.top), // HT
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}

		}

		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}

	else
		if (gbIsFullScreen == true)
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

			SetWindowPlacement(ghwnd, &wpPrev);

			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
				SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

			ShowCursor(TRUE);
			gbIsFullScreen = false;

		}
}


//UnInitialize
void UnInitialize(void)
{
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

	}

	//Break Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if(Quadric)
	    gluDeleteQuadric(Quadric);
    Quadric = NULL;

	fprintf(gpFile, "\n Application Closed ");
	fclose(gpFile);
	gpFile = NULL;
}


//Resize
void Resize(int Width, int Height)
{
	if (Height == 0)
		Height = 1;

	glViewport(0, 0, GLsizei(Width), GLsizei(Height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Most imp
	gluPerspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
}


//Display
void Display(void)
{
	//5th
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix(); //Model
    gluLookAt(0.0f,0.0f,3.0f,
             0.0f,0.0f,0.0f,
			 0.0f,1.0f,0.0f);
    
    glPushMatrix(); //for 1st Light
    glRotatef(lights[0].Light_Angle,1.0f,0.0f,0.0f);
    lights[0].Light_Position[1] = lights[0].Light_Angle;
    glLightfv(GL_LIGHT0,GL_POSITION,lights[0].Light_Position);
    glPopMatrix();

    glPushMatrix(); //for 2nd Light
    glRotatef(lights[1].Light_Angle,0.0f,1.0f,0.0f);
    lights[1].Light_Position[0] = lights[1].Light_Angle;
    glLightfv(GL_LIGHT1,GL_POSITION,lights[1].Light_Position);
    glPopMatrix();

    glPushMatrix(); //for 3rd Light
    glRotatef(lights[2].Light_Angle,0.0f,0.0f,1.0f);
    lights[2].Light_Position[0] = lights[2].Light_Angle;
    glLightfv(GL_LIGHT2,GL_POSITION,lights[2].Light_Position);
    glPopMatrix();


    //Model
    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

    Quadric = gluNewQuadric();

    gluSphere(Quadric,0.6f,30,30);    

    glPopMatrix();
	SwapBuffers(ghdc);
}


//Update
void Update(void)
{ 

		lights[0].Light_Angle += 0.2f;

	if (lights[0].Light_Angle >= 360.0f)
		lights[0].Light_Angle = 0.0f;
	
    	lights[1].Light_Angle += 0.2f;

	if (lights[1].Light_Angle >= 360.0f)
		lights[1].Light_Angle = 0.0f;
	
		lights[2].Light_Angle += 0.2f;

	if (lights[2].Light_Angle >= 360.0f)
		lights[2].Light_Angle = 0.0f;
	
}
