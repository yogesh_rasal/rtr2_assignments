

/*

	Assignment : Fixed_Functional_Pipeline : Circle on Graph

*/

//Headers
#include<stdio.h>
#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include<math.h>

#pragma comment (lib,"opengl32.lib")
#pragma comment (lib,"gdi32.lib")
#pragma comment (lib,"user32.lib")
#pragma comment (lib,"glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define MyPI 3.14

// Globals
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
FILE *gpFile = NULL;
GLfloat angle = 0.0f;
const int points = 1000;


bool gbActiveWindow = false;
bool gbIsFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Display(void);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIns, LPSTR lpCmdLine, int iCmdShow)
{
	// Function Declarations
	int Initialize(void);
	void UnInitialize(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Circle ");
	int iRet = 0;
	bool bDone = false;

	//File IO
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can not Created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	else
	{
		fprintf(gpFile, "Log File Successfully Created");
	}

	//initialize the class
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(hInstance, IDC_HAND);
	wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.cbWndExtra = 0;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT(" Circle on Graph Paper by Yogeshwar"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100, 100, WIN_WIDTH, WIN_HEIGHT,
		NULL, NULL, hInstance, NULL);

	// For Update
	ghwnd = hwnd;
	iRet = Initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\n ChoosePixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -2)
	{
		fprintf(gpFile, "\n SetPixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -3)
	{
		fprintf(gpFile, "\n wglCreateContext Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -4)
	{
		fprintf(gpFile, "\n wglMakeCurrent Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == 0)
	{
		fprintf(gpFile, "\n Initialization Successful");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd); //UpdateWindow
	SetFocus(hwnd);

	// Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else if (gbActiveWindow == true)
		{

		}
		Display();
	}

	return((int)msg.wParam);

}//Main Ends

 //WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function Declarations
	void ToggleFullScreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	switch (iMsg)
	{
	case WM_CREATE:
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:

		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		
		case 0x46:
			ToggleFullScreen();
			break;

		}
		break;

	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}//WndProc Ends

 //init()
int Initialize(void)
{
	void Resize(int, int); // imp
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;

	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	else if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	//Bridging API
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	else if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}
	else
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);//Black

		//Warm up call 
	Resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

//ToggleFullScreen
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	mi.cbSize = sizeof(MONITORINFO);

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//mi = { sizeof(MONITORINFO) };

		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			if ((GetWindowPlacement(ghwnd, &wpPrev)) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),// WD
					(mi.rcMonitor.bottom - mi.rcMonitor.top), // HT
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}

		}
		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}

	else
		if (gbIsFullScreen == true)
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

			SetWindowPlacement(ghwnd, &wpPrev);

			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
				SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

			ShowCursor(TRUE);
			gbIsFullScreen = false;

		}
}


//UnInitialize
void UnInitialize(void)
{
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

	}

	//Break Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "\n Application Closed ");
	fclose(gpFile);
	gpFile = NULL;

}

// Actual Drawing
//Resize
void Resize(int Width, int Height)
{
	if (Height == 0)
		Height = 1;

	glViewport(0,0, GLsizei(Width), GLsizei(Height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Most imp
	gluPerspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);

}

//Display
void Display(void)
{	
	glTranslatef(0.0f,0.0f,-1.0f);	

	// Graph
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-0.35f);

	glLineWidth(1.0f);
	
	glBegin(GL_LINES);

	GLfloat fX = -1.0f;
	GLfloat fY = -1.0f;

	for(;fX <= 1.0f, fY <= 1.0f; fX = fX+0.02f , fY = fY+0.02f)
	{
  		glColor3f(0.0f, 0.5f, 0.0f);
		glVertex2f(fX, 1.0f);
		glVertex2f(fX, -1.0f);
	
		glColor3f(0.0f, 0.5f, 0.0f);
		glVertex2f(1.0f,fY);
		glVertex2f(-1.0f,fY);
	}	
	
	glLineWidth(3.0f);

	// X
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);
	glVertex2f(1.0f, 0.0f);

	// Y	
	glVertex2f(0.0f, -1.0f);
	glVertex2f(0.0f, 1.0f);

	glEnd();

		
	// Circle

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-2.0f);

	glBegin(GL_LINE_LOOP);
	
	for(int iPts = 0; iPts < points ; iPts++)
	{
		angle = (GLfloat)(2.0f*MyPI*iPts)/points;//most imp	

		glColor3f(1.0f,1.0f,0.0f); 
		glVertex3f(cos(angle),sin(angle),0.0f);
		glVertex3f(cos(angle),sin(angle),0.0f);
	}
	glEnd();


	SwapBuffers(ghdc);
}

