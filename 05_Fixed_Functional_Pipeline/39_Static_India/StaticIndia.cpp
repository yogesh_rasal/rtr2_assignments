
/*

Assignment : Fixed_Functional_Pipeline : Static India

*/


#include<Windows.h>
#include<stdio.h>
#include<math.h>
#include "gl/GL.h"
#include "gl/GLU.h"

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variables
HWND ghwnd = NULL;
bool gbIsFullScreen = false;
DWORD gdwStyle;
WINDOWPLACEMENT gwpPrev = { sizeof(WINDOWPLACEMENT) };
HDC ghdc = NULL;
HGLRC ghrc = NULL; 
bool gbActiveWindow = false;
FILE* gpFile = NULL;

//Functions
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Display(void);
void ToggleFullScreen(void);


// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int initialize(void);

	// variable declarations
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	HWND hwnd;
	MSG msg;
	bool bDone = false;
	int iRet = 0;

	// code
	if (fopen_s(&gpFile, "Logs.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File cannot be created"),
			TEXT("Error"),
			MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file created Successfully.\n");
	}

	// initialize window class
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Static India : By Yogeshwar"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet = initialize();
	ToggleFullScreen();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat() failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext() failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent() failed\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initialization successfull\n");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// start the game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//  update();
			}

			Display();
		}
	}
	return((int)msg.wParam);
}// Main Ends

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//
	void resize(int, int);
	void uninitialize(void);

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	
	case WM_CHAR:
		switch (wParam)
		{
		case 'f':
		case 'F':
			ToggleFullScreen();
			break;
		}
		break;
	
	case WM_ERASEBKGND:
		return(0);
		break;
	
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


int initialize(void)
{
	 // function declarations
	void resize(int, int);
	ToggleFullScreen();

	// variables
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR)); 
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1; 
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	// get the normal device context
	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd); 
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	// use bridging api 
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	// set ghrc 
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); 
	
	//warmup call
	resize(WIN_WIDTH, WIN_HEIGHT); 
	return(0);
}


void ToggleFullScreen(void)
{
	// variable declararions
	MONITORINFO mi = { sizeof(MONITORINFO) };

	// code
	if (gbIsFullScreen == false)
	{
		gdwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (gdwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &gwpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, gdwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
				ShowCursor(FALSE);
				gbIsFullScreen = true;
			}
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, gdwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &gwpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbIsFullScreen = false;
	}
}


void uninitialize(void)
{
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, gdwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &gwpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,0,0,0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
			
		ShowCursor(TRUE);
	}
	
	// break the current context 
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	
	fprintf(gpFile, "Log File is being closed\n");
	fclose(gpFile);
	gpFile = NULL;
}

// Resize 
void resize(int width, int height)
{
	// code
	if (height == 0)
	{
		height = 1; 
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,((GLfloat)width / (GLfloat)height),0.1f,100.0f);
}


void Display(void)
{	
	glClear(GL_COLOR_BUFFER_BIT);

	// I
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.3f, 0.0f, -3.0f);

	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.05f, 0.5f, 0.0f);
		glVertex3f(-0.05f, 0.5f, 0.0f);
		
		glColor3f(0.0f, 0.5f, 0.0f);
		glVertex3f(-0.05f, -0.5f, 0.0f);
		glVertex3f(0.05f, -0.5f, 0.0f);

	glEnd();

	
	// N
	
		// |

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.8f, 0.0f, -3.0f);

		glBegin(GL_QUADS);

			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.3f, 0.5f, 0.0f);
			glVertex3f(0.2f, 0.5f, 0.0f);

			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.2f, -0.5f, 0.0f);
			glVertex3f(0.3f, -0.5f, 0.0f);

		glEnd();

		// \

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.8f, 0.0f, -3.0f);

		glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.2f, 0.5f, 0.0f);
		glVertex3f(-0.3f, 0.5f, 0.0f);
		glColor3f(0.0f, 0.5f, 0.0f);
		glVertex3f(-0.3f, -0.5f, 0.0f);
		glVertex3f(-0.2f, -0.5f, 0.0f);

		glEnd();

		// |

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.8f, 0.0f, -3.0f);

		glBegin(GL_QUADS);

			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.1f, 0.5f, 0.0f);
			glVertex3f(-0.22f, 0.5f, 0.0f);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.1f, -0.5f, 0.0f);
			glVertex3f(0.22f, -0.5f, 0.0f);

		glEnd();
		

	// D
	
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -3.0f);

		// Line
		glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.3f, 0.5f, 0.0f);
		glVertex3f(-0.2f, 0.5f, 0.0f);

		glColor3f(0.0f, 0.5f, 0.0f);
		glVertex3f(-0.2f, -0.5f, 0.0f);
		glVertex3f(-0.3f, -0.5f, 0.0f);

		glEnd();

		// Half	
		glBegin(GL_QUAD_STRIP);
			
			glColor3f(1.0f, 0.5f, 0.0f);//Orange
			glVertex3f(-0.2f, 0.4f, 0.0f);
			glVertex3f(-0.2f, 0.5f, 0.0f);
			glVertex3f(0.0f, 0.4f, 0.0f);
			glVertex3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.15f, 0.3f, 0.0f);
			glVertex3f(0.25f, 0.36f, 0.0f);

			glColor3f(0.0f, 0.5f, 0.0f); // Green
			glVertex3f(0.15f, -0.3f, 0.0f);
			glVertex3f(0.25f, -0.36f, 0.0f);
			glVertex3f(0.0f, -0.4f, 0.0f);
			glVertex3f(0.0f, -0.5f, 0.0f);
			glVertex3f(-0.2f, -0.4f, 0.0f);
			glVertex3f(-0.2f, -0.5f, 0.0f);

		glEnd();


	// I

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.5f, 0.0f, -3.0f);

		glBegin(GL_QUADS);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.05f, 0.5f, 0.0f);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.05f, 0.5f, 0.0f);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(-0.05f, -0.5f, 0.0f);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.05f, -0.5f, 0.0f);
		glEnd();


	// A
		
		//   strips

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, 0.0f, -3.0f);
		glLineWidth(10.0f);

		glBegin(GL_LINES);
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(-0.093f, 0.0f, 0.0f);
			glVertex3f(0.093f, 0.0f, 0.0f);
		glEnd();

		glPointSize(5.0f);
		glBegin(GL_POINTS);
			glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);
		glEnd();


		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, 0.0f, -3.0f);
		glLineWidth(10.0f);

		glBegin(GL_LINES);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.088f, 0.021f, 0.0f);
			glVertex3f(0.088f, 0.021f, 0.0f);
		glEnd();


		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, 0.0f, -3.0f);
		glLineWidth(10.0f);

		glBegin(GL_LINES);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(-0.097f, -0.02f, 0.0f);
			glVertex3f(0.097f, -0.02f, 0.0f);
		glEnd();

		//  /\

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, 0.0f, -3.0f);

		glBegin(GL_QUADS);

			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(-0.2f, -0.5f, 0.0f);
			glVertex3f(-0.3f, -0.5f, 0.0f);
			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.1f, 0.5f, 0.0f);
			glVertex3f(0.02f, 0.5f, 0.0f);

		glEnd();


		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, 0.0f, -3.0f);

		glBegin(GL_QUADS);

			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.2f, -0.5f, 0.0f);
			glVertex3f(0.3f, -0.5f, 0.0f);

			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.1f, 0.5f, 0.0f);
			glVertex3f(-0.02f, 0.5f, 0.0f);

		glEnd();

	
	SwapBuffers(ghdc);
}
