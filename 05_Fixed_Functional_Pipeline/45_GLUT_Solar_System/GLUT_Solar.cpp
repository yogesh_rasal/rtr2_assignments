/*
 Solar System using GLUT
*/

#include "windows.h"
#include "GL\freeglut.h"
#include "GL\glu.h"

#pragma comment (lib,"freeglut.lib")
#pragma comment (lib,"opengl32.lib")
#pragma comment (lib,"glu32.lib")


bool bIsfullScreen = false;
int Year = 0, Day = 0; //1

int main(int argc, char *argv[])
{
	// Function Declrations
	void Initialize(void);
	void Unintialize(void);
	void Reshape(int, int);
	void Display(void);
	void KeyBoard(unsigned char, int, int);
	void Mouse(int, int, int, int);

	// Code
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA); //2
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Solar System using GLUT");

	Initialize();

	// CallBacks
	glutDisplayFunc(Display);
	glutReshapeFunc(Reshape);
	glutKeyboardFunc(KeyBoard);
	glutMouseFunc(Mouse);
	glutCloseFunc(Unintialize);

	glutMainLoop();

	return (0);

} // Main Ends


void Initialize(void)
{
	//code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_FLAT); //3

}

void Unintialize(void)
{
	//Code
}

void Reshape(int Width, int Height)
{
	//code
	if (Height == 0)
		Height = 1;

	glViewport(0, 0, GLsizei(Width), GLsizei(Height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Most imp
	gluPerspective(60.0f, (GLfloat)Width / (GLfloat)Height, 1.0f, 20.0f);

}

// Keydown
void KeyBoard(unsigned char key, int x, int y)
{
	//Code
	switch (key)
	{
	case 27: glutLeaveMainLoop();
		break;

	case 'F':
	case 'f':
		if (bIsfullScreen == false)
		{
			glutFullScreen();
			bIsfullScreen = true;
		}

		else 
		{
			glutLeaveFullScreen();
			bIsfullScreen = false;
		}
		break;

	case 'Y':
		Year = (Year + 5) % 360;
		glutPostRedisplay();
		break;

	case 'y':
		Year = (Year - 5) % 360;
		glutPostRedisplay();
		break;

	case 'D':
		Day = (Day+10) % 360;
		glutPostRedisplay();
		break;

	case 'd':
		Day = (Day - 10) % 360;
		glutPostRedisplay();
		break;

	} //switch
}

//On-click
void Mouse(int Button, int State, int x, int y)
{
	//Code
	switch (Button)
	{
	case GLUT_LEFT_BUTTON:
		break;

	case GLUT_RIGHT_BUTTON://for close App
		glutLeaveMainLoop();
		break;

	}//switch

}

void Display(void)
{
	// Core Changes
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(  0.0f,0.0f,5.0f,
				0.0f,0.0f,0.0f,
				0.0f,1.0f,0.0f );
	
	// Push the CTM
	glPushMatrix(); 
	
	//Sun
	glutWireSphere(1.0f,20.0f,16.0f);

	//Rotate CTM
	glRotatef(GLfloat(Year),0.0f,1.0f,0.0f);

	glTranslatef(2.0f,0.0f,0.0f); // Radius

	//for Earth
	glRotatef(GLfloat(Day), 0.0f, 1.0f, 0.0f);

	glutWireSphere(0.2f, 10.0f, 8.0f);

	//Pop the Top
	glPopMatrix();

	glutSwapBuffers();

} //Display = Paint

