/*
3D Shapes using GLUT
*/

#include "windows.h"
#include "GL\freeglut.h"
#include "GL\glu.h"

#pragma comment (lib,"freeglut.lib")
#pragma comment (lib,"opengl32.lib")
#pragma comment (lib,"glu32.lib")


bool bIsfullScreen = false;
GLfloat AngleTri = 0.0f;
GLfloat AngleCube = 0.0f;


int main(int argc, char *argv[])
{
	// Function Declrations
	void Initialize(void);
	void Unintialize(void);
	void Reshape(int, int);
	void Display(void);
	void KeyBoard(unsigned char, int, int);
	void Mouse(int, int, int, int);
	void Update(void);

	// Code
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA); //2
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Cube & Pyramid using GLUT");

	Initialize();

	// CallBacks
	glutDisplayFunc(Display);
	glutReshapeFunc(Reshape);
	glutKeyboardFunc(KeyBoard);
	glutMouseFunc(Mouse);
	glutIdleFunc(Update);
	glutCloseFunc(Unintialize);
	
	glutMainLoop();

	return (0);

} // Main Ends


void Initialize(void)
{
	//code for Depth
	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

}

void Unintialize(void)
{
	//Code
}

void Reshape(int Width, int Height)
{
	//code
	if (Height == 0)
		Height = 1;

	glViewport(0, 0, GLsizei(Width), GLsizei(Height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Most imp
	gluPerspective(45.0f, (GLfloat)Width / (GLfloat)Height, 1.0f, 20.0f);

}

// Keydown
void KeyBoard(unsigned char key, int x, int y)
{
	//Code
	switch (key)
	{
	case 27: glutLeaveMainLoop();
		break;

	case 'F':
	case 'f':
		if (bIsfullScreen == false)
		{
			glutFullScreen();
			bIsfullScreen = true;
		}

		else
		{
			glutLeaveFullScreen();
			bIsfullScreen = false;
		}
		break;

	} //switch
}

//On-click
void Mouse(int Button, int State, int x, int y)
{
	//Code
	switch (Button)
	{
	case GLUT_LEFT_BUTTON:
		break;

	case GLUT_RIGHT_BUTTON://for close App
		glutLeaveMainLoop();
		break;

	}//switch

}

void Display(void)
{
	// Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//-------------Pyramid

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.4f, 0.0f, -5.0f);
	glRotatef(AngleTri, 0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);

	// Front
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); //apex 

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f); //left-bottom

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f); //right-bottom 

	   //Right
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); //apex 

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f); //left-bottom

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f); //right-bottom 


		//Back
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); //apex 

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f); //left-bottom

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f); //right-bottom 

	 //Left
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); //apex 

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f); //left-bottom

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f); //right-bottom 

	glEnd();


	//-------- Cube

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 0.0f, -5.0f);
	glScalef(0.75f, 0.75f, 0.75f); // 6th
	glRotatef(AngleCube, 1.0f, 1.0f, 1.0f);


	glBegin(GL_QUADS);

	// Front
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex3f(1.0f, 1.0f, 1.0f); //1 
	glVertex3f(-1.0f, 1.0f, 1.0f); //2
	glVertex3f(-1.0f, -1.0f, 1.0f); //3
	glVertex3f(1.0f, -1.0f, 1.0f); //4 


	   //Right
	glColor3f(1.0f, 1.0f, 0.0f);

	glVertex3f(1.0f, 1.0f, -1.0f); //1 
	glVertex3f(1.0f, 1.0f, 1.0f); //2
	glVertex3f(1.0f, -1.0f, 1.0f); //3
	glVertex3f(1.0f, -1.0f, -1.0f); //4


	//Back
	glColor3f(0.0f, 1.0f, 1.0f);

	glVertex3f(1.0f, -1.0f, -1.0f); //1 
	glVertex3f(-1.0f, -1.0f, -1.0f); //2
	glVertex3f(-1.0f, 1.0f, -1.0f); //3
	glVertex3f(1.0f, 1.0f, -1.0f); //4

   //Left
	glColor3f(1.0f, 1.0f, 0.0f);

	glVertex3f(-1.0f, 1.0f, 1.0f); //right top  
	glVertex3f(-1.0f, 1.0f, -1.0f); //left-top
	glVertex3f(-1.0f, -1.0f, -1.0f); //left-bottom 
	glVertex3f(-1.0f, -1.0f, 1.0f); //right-bottom 

	//Top
	glColor3f(1.0f, 0.0f, 0.0f);

	glVertex3f(1.0f, 1.0f, -1.0f); //right top 
	glVertex3f(-1.0f, 1.0f, -1.0f); //left-top
	glVertex3f(-1.0f, 1.0f, 1.0f); //left-bottom 
	glVertex3f(1.0f, 1.0f, 1.0f); //right-bottom 

	  //Bottom
	glColor3f(1.0f, 0.0f, 1.0f);

	glVertex3f(1.0f, -1.0f, 1.0f); //right top  
	glVertex3f(-1.0f, -1.0f, 1.0f); //left-top
	glVertex3f(-1.0f, -1.0f, -1.0f); //left-bottom 
	glVertex3f(1.0f, -1.0f, -1.0f); //right-bottom 

	glEnd();

	glutSwapBuffers();

} //Display = Paint



void Update(void)
{

	if (AngleTri >= 360.0f)
		AngleTri = 0.0f;
	
	AngleTri += 0.5f;


	if (AngleCube >= 360.0f)
		AngleCube = 0.0f;
	
	AngleCube += 0.5f;

	glutPostRedisplay();

}// IdleFunc


