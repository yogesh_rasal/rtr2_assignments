package com.astromedicomp.blueWindow;

//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.view.WindowManager;
import android.view.Window;
import android.view.Gravity;
import android.content.Context;
import android.view.View;
import android.graphics.Color;

//1
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

//2
import android.opengl.GLSurfaceView; //OGL Supportive View
import android.opengl.GLES32; //3.2
import javax.microedition.khronos.opengles.GL10; //J2ME 1.0
import javax. microedition.khronos.egl.EGLConfig;

public class GLESView extends GLSurfaceView implements OnGestureListener,OnDoubleTapListener,GLSurfaceView.Renderer
 {
    private GestureDetector gestureDetector;
    private final Context context;

    public GLESView(Context drawContext)
    {

        super(drawContext);
        context = drawContext; //came from MainActivity

        setEGLContextClientVersion(3); //gives Best Context setEGLContextClientVersion(3)

        setRenderer(this); //who will render

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); //set mode

        gestureDetector = new GestureDetector(drawContext,this,null,false);
        gestureDetector.setOnDoubleTapListener(this);

    }

    // 9 Methods  : Most imp
    @Override 
    public boolean onTouchEvent(MotionEvent event)
    {
        int eventaction = event.getAction();

        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event); // like defwndproc

        return(true);
    } 

    @Override 
    public boolean onDoubleTap(MotionEvent e)
    {
        return(true);
    }

    @Override 
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return(true); //special
    }

    @Override 
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        return(true);
    }

    // Gesture Listener
    @Override 
    public boolean onDown(MotionEvent e)
    {
        return(true); //already done in onSingleTapConfirmed
    }

    @Override
    public void onLongPress(MotionEvent e)
    {
        
    }

    @Override public boolean onScroll(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
       System.exit(0);
       return(true);
    }

    @Override 
    public boolean onFling(MotionEvent e1,MotionEvent e2,float distanceX,float distanceY)
    {

        return(true);
    }

    @Override 
    public void onShowPress(MotionEvent e)
    {

    }

    @Override public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }


    @Override 
    public void onSurfaceCreated(GL10 gl,EGLConfig config) 
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR: GLES Version :"+version);        

        String ogl = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR: GLSL Version :"+ogl);        


        Initialize();
    }

    @Override 
    public void onSurfaceChanged(GL10 unused,int Width,int Height)
    {
        Resize(Width,Height);

    }

    @Override 
    public void onDrawFrame(GL10 unused)
    {
        Display();
    }

    //Own Methods
    private void Initialize()
    {
        GLES32.glClearColor(0.0f,0.0f,1.0f,0.0f);
    }

    private void Resize(int Width,int Height)
    {
        GLES32.glViewport(0,0,Width,Height);
        System.out.println("RTR: in Resize");
    }

    private void Display()
    {
     GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
     requestRender(); //from SurfaceView

    }
}
