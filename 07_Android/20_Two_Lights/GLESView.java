package com.astromedicomp.twolights;

import android.content.Context;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;


import android.opengl.GLSurfaceView;
import android.opengl.GLES32; // opengl version 3.2

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig; //Extended

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;    //new
import android.opengl.Matrix;

public class GLESView extends GLSurfaceView
        implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener 
{
    //Fields   
    private GestureDetector gestureDetector;
    private final Context context;

    private int fragmentShaderObject_YSR;
    private int vertexShaderObject_YSR;
    private int shaderProgramObject_YSR;

    private float[] PerspectiveProjectionMatrix_YSR = new float[16];

    //angles for rotation
    static float angleTriangle= 0.0f;

    //uniforms    
    private int mvUniform;
    private int View_Uniform;
    private int Projection_Uniform;

    private int LD_Uniform;
    private int KD_Uniform;

    private int LA_Uniform;
    private int KA_Uniform;

    private int LS_Uniform;
    private int KS_Uniform;

    private int Material_Shininess_YSR;

    private int Light_Position_Uniform;
    private int LisPressed_Uniform;

    //Red
    private int LD_Uniform_Red, LA_Uniform_Red, LS_Uniform_Red;
    private int KD_Uniform_Red, KA_Uniform_Red, KS_Uniform_Red;
    private int Light_Position_Uniform_Red;


    private Boolean bisLPressed = false;

    //for sphere
    private int[] Vao_pyramid = new int[1];
    private int[] Vbo_pyramid_position = new int[1];
    private int[] Vbo_pyramid_normal = new int[1];

    private int numVertices ,numElements;

    // constructor
    public GLESView(Context drawingContext) 
    {
        super(drawingContext);
        context = drawingContext; //ghrc

        gestureDetector = new GestureDetector(drawingContext, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);

        setEGLContextClientVersion(3); // 3.x

        setRenderer(this); // onDrawFrame() - display()

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // NOTE
    }


    // Handling Events
    //1
    @Override
    public boolean onTouchEvent(MotionEvent me) 
    {
        // keyboard actions
        int eventaction = me.getAction();

        if (!gestureDetector.onTouchEvent(me)) 
        {
            super.onTouchEvent(me);
        }

        return (true);
    }

    //2
    @Override
    public boolean onDoubleTap(MotionEvent me) 
    {
        return (true);
    }

    //3
    @Override
    public boolean onDoubleTapEvent(MotionEvent me) 
    {
       
        return (true);
    }

    //4
    @Override
    public boolean onSingleTapConfirmed(MotionEvent me) {
        return (true);
    }

    //5
    @Override
    public boolean onDown(MotionEvent me) 
    {
       
        return (true);
    }

    //6
    @Override
    public boolean onFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) 
    {
        // swipe
        return (true);
    }

    //7
    @Override
    public void onLongPress(MotionEvent me) 
    {
        if(bisLPressed == false)
                bisLPressed = true;
        else 
                bisLPressed = false;
                
    }


    //8
    @Override
    public boolean onScroll(MotionEvent me1, MotionEvent me2, float distanceX, float distanceY) 
    {
        uninitialize();
        System.exit(0);
        return (true);
    }

    //9
    @Override
    public void onShowPress(MotionEvent me) 
    {

    }

    //10
    @Override
    public boolean onSingleTapUp(MotionEvent me) 
    {
        return (true);
    }


    // 3 functions of GLSurfaceView.Renderer
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) 
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR: GL10.GL_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_VENDOR);
        System.out.println("RTR: GLES32.GL_VENDOR: " + version);

        version = gl.glGetString(GLES32.GL_RENDERER);
        System.out.println("RTR: GLES32.GL_RENDERER: " + version);

        Initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) 
    {
        Resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused) 
    {
        Update();
        Display();
    }

    //3 Methods 
    private void Initialize() 
    {
        //Shaders
        vertexShaderObject_YSR = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
        final String vertexShaderSourceCode = String.format(
            "#version 320 es"           +
            "\n"                        +
            "precision highp float;"	+
            "in vec4 vPosition;"						+
            "in vec3 vNormal;"							+
            "out vec3 T_Norm;"							+
            "out vec3 Light_Direction;"					+
            "out vec3 Light_Direction_R;"				+
            "out vec3 Viewer_Vector;"					+
            "\n"										+
            "uniform mat4 u_model_matrix;"				+
            "uniform mat4 u_view_matrix;"				+
            "uniform mat4 u_projection_matrix;"			+
            "uniform int u_LKeyPressed;"				+
            "uniform vec4 u_Light_Position;"			+
            "uniform vec4 u_Light_Position_R;"			+
            "\n"										+
            "void main(void)"							+
            "{"											+
            "if(u_LKeyPressed == 1)"					+
            "{"											+
            "vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"	+
            "T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"			+
            "Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"	+
            "Light_Direction_R = vec3(u_Light_Position_R) - (Eye_Coordinates.xyz);"	+
            "Viewer_Vector = vec3(-Eye_Coordinates.xyz);"	+
            "}"		+
            "gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"	+
            "}" 
            );
        
        GLES32.glShaderSource(vertexShaderObject_YSR, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject_YSR);

        // Error checking
        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject_YSR, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); 
        
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
         {
            GLES32.glGetShaderiv(vertexShaderObject_YSR, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject_YSR);
                System.out.println("RTR2: Error in Vertex shader compilation log: " + szInfoLog);
            }
             else
             {
                System.out.println("RTR2:Error in Vertex Shader.");
             }
            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Vertex Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        fragmentShaderObject_YSR = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        final String fragmentShaderSourceCode = String.format(
            "#version 320 es"                   +
            "\n"                                +
            "precision highp float;"            +
            "precision highp int;"              +
            "in vec3 T_Norm;"		        	+
            "in vec3 Light_Direction;"	        +
            "in vec3 Light_Direction_R;"	    +
            "in vec3 Viewer_Vector;"	        +
            "out vec4 FragColor;"		        +
            "\n"						        +       
            "uniform vec3 u_LA;"				+
            "uniform vec3 u_LD;"				+
            "uniform vec3 u_LS;"				+
            "uniform vec3 u_KA;"				+
            "uniform vec3 u_KD;"				+
            "uniform vec3 u_KS;"				+
            "uniform vec3 u_LA_R;"				+
            "uniform vec3 u_LD_R;"				+
            "uniform vec3 u_LS_R;"				+
            "uniform vec3 u_KA_R;"				+
            "uniform vec3 u_KD_R;"				+
            "uniform vec3 u_KS_R;"				+
            "uniform float u_Shininess;"		+
            "uniform int u_LKeyPressed;"		+
            "uniform vec4 u_Light_Position;"	+
            "uniform vec4 u_Light_Position_R;"	+
            "\n"								+
            "void main(void)"					+
            "{"						            +
            "if(u_LKeyPressed == 1)"            +
            "{"						            +
            "vec3 Normalized_View_Vector = normalize(Viewer_Vector);"	+
            "vec3 Normalized_Light_Direction = normalize(Light_Direction);"+
            "vec3 Normalized_Light_Direction_R = normalize(Light_Direction_R);"+
            "vec3 Normalized_TNorm = normalize(T_Norm);"+
            "float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0);"	+
            "vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm);"+
            "float TN_Dot_LD_R = max(dot(Normalized_Light_Direction_R,Normalized_TNorm), 0.0);"	+
            "vec3 Reflection_Vector_R = reflect(-Normalized_Light_Direction_R, Normalized_TNorm);"+
            "vec3 Ambient = vec3(u_LA * u_KA);"	+
            "vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);"	+
            "vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess));" +
            "vec3 Ambient_R = vec3(u_LA_R * u_KA_R);"	+
            "vec3 Diffuse_R = vec3(u_LD_R * u_KD_R * TN_Dot_LD_R);"	+
            "vec3 Specular_R = vec3(u_LS_R * u_KS_R * pow(max(dot(Reflection_Vector_R, Normalized_View_Vector), 0.0), u_Shininess));" +
            "vec3 Phong_ADS_Light = Ambient + Diffuse+ Specular;"	+
            "vec3 Phong_ADS_Light_R = Ambient_R + Diffuse_R+ Specular_R;"	+
            "FragColor = vec4(Phong_ADS_Light+Phong_ADS_Light_R,1.0);" +
            "}"							+
            "else"						+
            "{"							+
            "FragColor = vec4(1.0,1.0,1.0,1.0);" +
            "}"							+
            "}" 
        );
        GLES32.glShaderSource(fragmentShaderObject_YSR, fragmentShaderSourceCode);
        GLES32.glCompileShader(fragmentShaderObject_YSR);

        // Error checking
        GLES32.glGetShaderiv(fragmentShaderObject_YSR, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // 1st
        
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE) 
        {
            GLES32.glGetShaderiv(fragmentShaderObject_YSR, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject_YSR);
                System.out.println("RTR2: Error in Fragment shader compilation log: " + szInfoLog);
            } 
            else 
            {
                System.out.println("RTR2:Error in Fragment Shader.");
            }
            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Fragment Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        shaderProgramObject_YSR = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject_YSR, vertexShaderObject_YSR);
        GLES32.glAttachShader(shaderProgramObject_YSR, fragmentShaderObject_YSR);

        // Pre Link Attribute Binding
        GLES32.glBindAttribLocation(shaderProgramObject_YSR, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        
        //Normals
        GLES32.glBindAttribLocation(shaderProgramObject_YSR, GLESMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

        GLES32.glLinkProgram(shaderProgramObject_YSR);

        // Error checking
        int[] iProgramLinkStatus = new int[1];

        GLES32.glGetProgramiv(shaderProgramObject_YSR, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if (iProgramLinkStatus[0] == GLES32.GL_FALSE) 
        {
            GLES32.glGetProgramiv(shaderProgramObject_YSR, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
        
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject_YSR);
                System.out.println("RTR2: Error in Shader Program compilation log: " + szInfoLog);
            } 
            else 
            {
                System.out.println("RTR2: Error in Shader Program.");
            }

            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Shader Program compiled successfully.");
        }

        // Post Link Uniform Location
        mvUniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_model_matrix");

        View_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_view_matrix");

        Projection_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_projection_matrix");

        LD_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_LD");

        KD_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_KD");

        LS_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_LS");

        KS_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_KS");

        LA_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_LA");

        KA_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_KA");
     
        Light_Position_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_Light_Position");
     
        //Red
        LD_Uniform_Red = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_LD_R");

        KD_Uniform_Red = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_KD_R");

        LS_Uniform_Red = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_LS_R");

        KS_Uniform_Red = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_KS_R");

        LA_Uniform_Red = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_LA_R");

        KA_Uniform_Red = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_KA_R");
     
        Light_Position_Uniform_Red = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_Light_Position_R");
     
        Material_Shininess_YSR = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_Shininess");
        LisPressed_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_LKeyPressed");
        
     
        // reset flags
        iProgramLinkStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

//Shapes
        
	//pyramid
	final float[] pyramidVertices = new float[]
	{
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f
	};

	// normal vbo
	final float[] pyramid_normals = new float[]
	{
		0.0f,0.447214f,0.894427f, //1
		0.0f,0.447214f,0.894427f, //2
		0.0f,0.447214f,0.894427f, //3
		0.894427f,0.447214f,0.0f, //4
		0.894427f,0.447214f,0.0f, //5
		0.894427f,0.447214f,0.0f, //6
		0.0f,0.447214f,-0.894427f, //7
		0.0f,0.447214f,-0.894427f, //8
		0.0f,0.447214f,-0.894427f, //9
		-0.894427f,0.447214f,0.0f, //10
		-0.894427f,0.447214f,0.0f, //11
		-0.894427f,0.447214f,0.0f //12
	};

    // vao
    GLES32.glGenVertexArrays(1,Vao_pyramid,0);
    GLES32.glBindVertexArray(Vao_pyramid[0]);

    // position vbo
    GLES32.glGenBuffers(1,Vbo_pyramid_position,0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,Vbo_pyramid_position[0]);

    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(pyramidVertices.length * 4);
    byteBuffer.order(ByteOrder.nativeOrder());

    FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
    verticesBuffer.put(pyramidVertices);
    verticesBuffer.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                        pyramidVertices.length * 4,
                        verticesBuffer,
                        GLES32.GL_STATIC_DRAW);

    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                                3,
                                GLES32.GL_FLOAT,
                                false,0,0);

    GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

    // normal vbo
    GLES32.glGenBuffers(1,Vbo_pyramid_normal,0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,Vbo_pyramid_normal[0]);

    byteBuffer = ByteBuffer.allocateDirect(pyramid_normals.length * 4);
    byteBuffer.order(ByteOrder.nativeOrder());
    FloatBuffer normBuffer =byteBuffer.asFloatBuffer();
    normBuffer.put(pyramid_normals);
    normBuffer.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                        pyramid_normals.length * 4,
                        normBuffer,
                        GLES32.GL_STATIC_DRAW);

    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL,
                                3,
                                GLES32.GL_FLOAT,
                                false,0,0);

    GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);

    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
    
    GLES32.glBindVertexArray(0);

    // Hidden Surface Removal
      GLES32.glEnable(GLES32.GL_DEPTH_TEST); 
      GLES32.glDepthFunc(GLES32.GL_LEQUAL);

      Matrix.setIdentityM(PerspectiveProjectionMatrix_YSR, 0);

      GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
   }

  private void Resize(int width, int height) 
   {
        if (height == 0) 
        {
            height = 1;
        }
        
        GLES32.glViewport(0, 0, width, height);
    
        Matrix.perspectiveM(PerspectiveProjectionMatrix_YSR, 0,
            45.0f, (float)width / (float)height, 0.1f, 100.0f);
        
   }

    // Scene
    private void Display()
     {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject_YSR); // Binding shaders to SPO

        float[] modelMatrix = new float[16];
        float[] ViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];
        float[] rotationMatrix = new float[16]; //new

        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.setIdentityM(ViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        Matrix.translateM(modelMatrix, 0, 0.0f, 0.0f, -5.0f);
        Matrix.rotateM(rotationMatrix, 0, angleTriangle, 0.0f, 1.0f, 0.0f);

        Matrix.multiplyMM(modelMatrix,0, modelMatrix,0, rotationMatrix,0);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, PerspectiveProjectionMatrix_YSR,0, modelMatrix,0);
        
        //Uniforms
        GLES32.glUniformMatrix4fv(mvUniform, 1, false, modelMatrix, 0);

        GLES32.glUniformMatrix4fv(View_Uniform, 1, false, ViewMatrix, 0);

        //Projection            
        GLES32.glUniformMatrix4fv(Projection_Uniform,1,
            false, // Transpose
            PerspectiveProjectionMatrix_YSR,
            0);

            //Lights
        if (bisLPressed == true)
        {
            GLES32.glUniform1i(LisPressed_Uniform, 1);

            GLES32.glUniform3f(LD_Uniform, 0.0f, 0.0f, 1.0f);
            GLES32.glUniform3f(KD_Uniform, 1.0f, 1.0f, 1.0f);

            GLES32.glUniform3f(LS_Uniform, 1.0f, 1.0f, 1.0f);
            GLES32.glUniform3f(KS_Uniform, 1.0f, 1.0f, 1.0f);

            GLES32.glUniform3f(LA_Uniform, 0.2f, 0.2f, 0.2f);
            GLES32.glUniform3f(KA_Uniform, 0.2f, 0.2f, 0.2f);

            GLES32.glUniform1f(Material_Shininess_YSR, 128.0f);
            
            //imp : out to in Light
            GLES32.glUniform4f(Light_Position_Uniform, -100.0f, 100.0f, 100.0f, 1.0f);

            //2
            GLES32.glUniform3f(LD_Uniform_Red, 1.0f, 0.0f, 0.0f);
			GLES32.glUniform3f(KD_Uniform_Red, 1.0f, 1.0f, 1.0f);

			GLES32.glUniform3f(LS_Uniform_Red, 1.0f, 0.0f, 0.0f);
			GLES32.glUniform3f(KS_Uniform_Red, 1.0f, 1.0f, 1.0f);

			GLES32.glUniform3f(LA_Uniform_Red, 0.0f, 0.0f, 0.0f);
			GLES32.glUniform3f(KA_Uniform_Red, 0.25f, 0.25f, 0.25f);

			GLES32.glUniform4f(Light_Position_Uniform_Red, 100.0f, 100.0f, 100.0f, 1.0f);

        }

        else
        {
            GLES32.glUniform1i(LisPressed_Uniform, 0);
        }


        // bind vao
        GLES32.glBindVertexArray(Vao_pyramid[0]);
            
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, Vbo_pyramid_position[0]);

        GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12); // most imp
        
        // unbind vao
        GLES32.glBindVertexArray(0);

        //common
        GLES32.glUseProgram(0);

        requestRender(); //  swapBuffers
    }

    //new
    private void Update()
    {
        angleTriangle = angleTriangle + 0.5f;
        
        if(angleTriangle >= 360.0f)
        {
            angleTriangle = 0.0f;
        }
    }
    

    private void uninitialize() 
    {
        //Vao & Vbo
        if(Vao_pyramid[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, Vao_pyramid, 0);
            Vao_pyramid[0]=0;
        }
        
        //  position vbo
        if(Vbo_pyramid_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, Vbo_pyramid_position, 0);
            Vbo_pyramid_position[0]=0;
        }
        
        //  normal vbo
        if(Vbo_pyramid_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, Vbo_pyramid_normal, 0);
            Vbo_pyramid_normal[0]=0;
        }
        
        //SPO
        if (shaderProgramObject_YSR != 0) 
        {
            int[] shaderCount = new int[1];
            int shaderNo;

            GLES32.glUseProgram(shaderProgramObject_YSR);
            GLES32.glGetProgramiv(shaderProgramObject_YSR, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            int[] shaders = new int[shaderCount[0]];
        
            GLES32.glGetAttachedShaders(shaderProgramObject_YSR,
                    shaderCount[0],
                    shaderCount, 0,
                    shaders, 0);

            for (shaderNo = 0; shaderNo < shaderCount[0]; shaderNo++) 
            {
                GLES32.glDetachShader(shaderProgramObject_YSR, shaders[shaderNo]);
                GLES32.glDeleteShader(shaders[shaderNo]);
                shaders[shaderNo] = 0;
            }

            GLES32.glDeleteProgram(shaderProgramObject_YSR);
            shaderProgramObject_YSR = 0;
            GLES32.glUseProgram(0);
        }

    }

}
