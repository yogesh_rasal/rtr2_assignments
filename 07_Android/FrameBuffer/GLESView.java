package com.astromedicomp.framebuffer;

import android.content.Context;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;


import android.opengl.GLSurfaceView;
import android.opengl.GLES32; // opengl version 3.2

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig; //Extended

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;
//for Texture
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils; //for texImage2D

public class GLESView extends GLSurfaceView
        implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener 
{
    private GestureDetector gestureDetector;
    private final Context context;

    //old
    private int fragmentShaderObject;
    private int vertexShaderObject;
    private int shaderProgramObject;

    //shapes
    private int[] vaoPyramid = new int[1];
    private int[] vaoCube = new int[1];

    private int[] vboPyramidPosition = new int[1];
    private int[] vboPyramidTexture = new int[1];

    private int[] vboCubePosition = new int[1];
    private int[] vboCubeTexture = new int[1];

    private int[] Texture_Stone_YSR = new int[1];
    private int[] Texture_Kundali_YSR = new int[1];

    //angles for rotation
     static float angleTriangle= 0.0f;
     static float angleRectangle = 0.0f;
    
    private int mvpUniform;
    private int uniform_texture0_sampler;

    private float[] PerspectiveProjectionMatrix = new float[16];

    //new
    private int FBO_fragmentShaderObject;
    private int FBO_vertexShaderObject;
    private int FBO_shaderProgramObject;


    //RT
    private int[] fbo = new int[1];
    private int[] fbo_depth = new int[1];
    private int[] fbo_texture = new int[1];

    //Uniforms
    private int fbo_mvpUniform;
    private int fbo_samplerUniform;

    private int gWidth, gHeight;
    
    // constructor
    public GLESView(Context drawingContext) 
    {
        super(drawingContext);
        context = drawingContext; //ghrc

        gestureDetector = new GestureDetector(drawingContext, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);

        setEGLContextClientVersion(3); // 3.x

        setRenderer(this); // onDrawFrame() - display()

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // NOTE
    }


    // Handling Events
    //1
    @Override
    public boolean onTouchEvent(MotionEvent me) 
    {
        // keyboard actions
        int eventaction = me.getAction();

        if (!gestureDetector.onTouchEvent(me)) 
        {
            super.onTouchEvent(me);
        }

        return (true);
    }

    //2
    @Override
    public boolean onDoubleTap(MotionEvent me) 
    {
        return (true);
    }

    //3
    @Override
    public boolean onDoubleTapEvent(MotionEvent me) 
    {
       
        return (true);
    }

    //4
    @Override
    public boolean onSingleTapConfirmed(MotionEvent me)
    {
        return (true);
    }

    //5
    @Override
    public boolean onDown(MotionEvent me) 
    {
       
        return (true);
    }

    //6
    @Override
    public boolean onFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) 
    {
        // swipe
        return (true);
    }

    //7
    @Override
    public void onLongPress(MotionEvent me) 
    {

    }


    //8
    @Override
    public boolean onScroll(MotionEvent me1, MotionEvent me2, float distanceX, float distanceY) 
    {
        uninitialize();
        System.exit(0);
        return (true);
    }

    //9
    @Override
    public void onShowPress(MotionEvent me) 
    {
    }

    //10
    @Override
    public boolean onSingleTapUp(MotionEvent me) 
    {
        return (true);
    }


    // 3 functions of GLSurfaceView.Renderer
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) 
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR: GL10.GL_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_VENDOR);
        System.out.println("RTR: GLES32.GL_VENDOR: " + version);

        version = gl.glGetString(GLES32.GL_RENDERER);
        System.out.println("RTR: GLES32.GL_RENDERER: " + version);

        Initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) 
    {
        gWidth = width;
        gHeight = height;

        Resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused) 
    {
        Update();
        Display();
    }


    //3 Methods 
    
    //Texture Loading
    private int LoadTexture(int imageResourceID) 
    {
		int[] Texture  =  new int[1];

        //1
        BitmapFactory.Options options = new BitmapFactory.Options();

        //2
        options.inScaled = false;

		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, Texture[0]);

		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);

		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);

        //3 - ask Activity to find imageResourceID
        Bitmap bitmap =  BitmapFactory.decodeResource(context.getResources(),
									imageResourceID,
									options );
        
        //4
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D,
            0, //mipmap
            bitmap, 0);

		//5
        GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D); //new
        
		//6
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		return(Texture[0]);

    }

    private void Initialize() 
    {
        //Inner : FBO

        FBO_vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
        final String FBO_vertexShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec2 vTexture0_Coord;" +
            "out vec2 out_Texture0_Coord;" +
            "uniform mat4 u_mvp_matrix;" +
            "void main(void)    \n" +
            "{" +
             "gl_Position = u_mvp_matrix * vPosition;" +
             "out_Texture0_Coord = vTexture0_Coord;"   +
            "}"
        );
        
        GLES32.glShaderSource(FBO_vertexShaderObject, FBO_vertexShaderSourceCode);
        GLES32.glCompileShader(FBO_vertexShaderObject);

        // Error checking
        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(FBO_vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); 
        
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
         {
            GLES32.glGetShaderiv(FBO_vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetShaderInfoLog(FBO_vertexShaderObject);
                System.out.println("RTR2: Error in Vertex shader compilation log: " + szInfoLog);
            }
             else
             {
                System.out.println("RTR2:Error in Vertex Shader.");
             }
            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Vertex Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        //FS
        FBO_fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        
        final String FBO_fragmentShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;"+
            "in vec2 out_Texture0_Coord;" +
            "out vec4 FragColor;"+
            "uniform highp sampler2D u_texture0_sampler;" +
            "void main(void)"+
            "{"+
             "FragColor = texture(u_texture0_sampler,out_Texture0_Coord);"+
            "}"
        );
        
        GLES32.glShaderSource(FBO_fragmentShaderObject, FBO_fragmentShaderSourceCode);
        GLES32.glCompileShader(FBO_fragmentShaderObject);

        // Error checking
        GLES32.glGetShaderiv(FBO_fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // 1st
        
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE) 
        {
            GLES32.glGetShaderiv(FBO_fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetShaderInfoLog(FBO_fragmentShaderObject);
                System.out.println("RTR2: Error in Fragment shader compilation log: " + szInfoLog);
            } 
            else 
            {
                System.out.println("RTR2:Error in Fragment Shader.");
            }
            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Fragment Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        FBO_shaderProgramObject= GLES32.glCreateProgram();
        GLES32.glAttachShader(FBO_shaderProgramObject, FBO_vertexShaderObject);
        GLES32.glAttachShader(FBO_shaderProgramObject, FBO_fragmentShaderObject);

        // Pre Link Attribute Binding
        GLES32.glBindAttribLocation(FBO_shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(FBO_shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_TEXCOORD, "vTexture0_Coord");

        GLES32.glLinkProgram(FBO_shaderProgramObject);

        // Error checking
        int[] iProgramLinkStatus = new int[1];

        GLES32.glGetProgramiv(FBO_shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

        if (iProgramLinkStatus[0] == GLES32.GL_FALSE) 
        {
            GLES32.glGetProgramiv(FBO_shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
        
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetProgramInfoLog(FBO_shaderProgramObject);
                System.out.println("RTR2: Error in FBO Shader Program compilation log: " + szInfoLog);
            } 
            else 
            {
                System.out.println("RTR2: FBO Error in Shader Program.");
            }

            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: FBO Shader Program compiled successfully.");
        }

        // Post Link Uniform Location
        fbo_mvpUniform = GLES32.glGetUniformLocation(FBO_shaderProgramObject, "u_mvp_matrix");
        fbo_samplerUniform = GLES32.glGetUniformLocation(FBO_shaderProgramObject, "u_texture0_sampler");

        // reset flags
        iProgramLinkStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;



//*************  Outer Shape  ******** */ 
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
        final String vertexShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec2 vTexture0_Coord;" +
            "out vec2 out_Texture0_Coord;" +
            "uniform mat4 u_mvp_matrix;" +
            "void main(void)    \n" +
            "{" +
             "gl_Position = u_mvp_matrix * vPosition;" +
             "out_Texture0_Coord = vTexture0_Coord;"   +
            "}"
        );
        
        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        // Error checking
        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); 
        
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
         {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR2: Error in Vertex shader compilation log: " + szInfoLog);
            }
             else
             {
                System.out.println("RTR2:Error in Vertex Shader.");
             }
            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Vertex Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        //FS
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        
        final String fragmentShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;"+
            "in vec2 out_Texture0_Coord;" +
            "out vec4 FragColor;"+
            "uniform highp sampler2D u_texture0_sampler;" +
            "void main(void)"+
            "{"+
             "FragColor = texture(u_texture0_sampler,out_Texture0_Coord);"+
            "}"
        );
        
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
        GLES32.glCompileShader(fragmentShaderObject);

        // Error checking
        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // 1st
        
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE) 
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR2: Error in Fragment shader compilation log: " + szInfoLog);
            } 
            else 
            {
                System.out.println("RTR2:Error in Fragment Shader.");
            }
            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Fragment Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        shaderProgramObject = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Pre Link Attribute Binding
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_TEXCOORD, "vTexture0_Coord");

        GLES32.glLinkProgram(shaderProgramObject);

        // Error checking

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if (iProgramLinkStatus[0] == GLES32.GL_FALSE) 
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
        
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("RTR2: Error in Shader Program compilation log: " + szInfoLog);
            } 
            else 
            {
                System.out.println("RTR2: Error in Shader Program.");
            }

            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Shader Program compiled successfully.");
        }

        // Post Link Uniform Location
        mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        uniform_texture0_sampler = GLES32.glGetUniformLocation(shaderProgramObject, "u_texture0_sampler");

        // reset flags
        iProgramLinkStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

    //Shapes
        
            
    /********* Cube **********/
    final float[] CubeVertices = new float[]
    {
          1.0f, 1.0f, 1.0f,
         -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
         1.0f, -1.0f, 1.0f,

         1.0f, 1.0f, -1.0f,
          1.0f, 1.0f, 1.0f,
         1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,

         -1.0f, 1.0f, -1.0f,
          1.0f, 1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,

         -1.0f, 1.0f, 1.0f,
         -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
         -1.0f, -1.0f, 1.0f,

        -1.0f, 1.0f, 1.0f,
         1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, -1.0f,
       -1.0f, 1.0f, -1.0f,

         1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f	

    };  

    //Texture
    final float[] CubeText = new float[]
    {
		0.0f,0.0f,
		1.0f,0.0f,
		1.0f,1.0f,
		0.0f,1.0f,

		1.0f,0.0f,
		1.0f,1.0f,
		0.0f,1.0f,
		0.0f,0.0f,

		1.0f,0.0f,
		1.0f,1.0f,
		0.0f,1.0f,
		0.0f,0.0f,

		0.0f,0.0f,
		1.0f,0.0f,
		1.0f,1.0f,
		0.0f,1.0f,

		0.0f,1.0f,
		0.0f,0.0f,
		1.0f,0.0f,
		1.0f,1.0f,
		
		1.0f,1.0f,
		0.0f,1.0f,
		0.0f,0.0f,
		1.0f,0.0f		
    };


    //1
    // Generation
    GLES32.glGenVertexArrays(1, vaoPyramid, 0);
    GLES32.glBindVertexArray(vaoPyramid[0]);

    GLES32.glGenBuffers(1, vboPyramidPosition, 0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPyramidPosition[0]);

    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(CubeVertices.length * 4); // float
    byteBuffer.order(ByteOrder.nativeOrder());
    
    FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
    positionBuffer.put(CubeVertices);
    positionBuffer.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, CubeVertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

    GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
    
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind


    //2
    GLES32.glGenBuffers(1, vboPyramidTexture, 0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPyramidTexture[0]);

    ByteBuffer byteBuffer_t = ByteBuffer.allocateDirect(CubeText.length * 4); // float
    byteBuffer_t.order(ByteOrder.nativeOrder());
    FloatBuffer Buffer_YSR = byteBuffer_t.asFloatBuffer();
    Buffer_YSR.put(CubeText);
    Buffer_YSR.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, CubeText.length * 4, Buffer_YSR, GLES32.GL_STATIC_DRAW);
    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD, 2, GLES32.GL_FLOAT, false, 0, 0);
    
    GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD);

    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind

    GLES32.glBindVertexArray(0);


    //3
    GLES32.glGenVertexArrays(1, vaoCube, 0);
    GLES32.glBindVertexArray(vaoCube[0]);

    GLES32.glGenBuffers(1, vboCubePosition, 0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboCubePosition[0]);
    ByteBuffer byteBufferCube = ByteBuffer.allocateDirect(CubeVertices.length * 4);// 4 : sizeof float
    byteBufferCube.order(ByteOrder.nativeOrder());

    FloatBuffer positionBufferCube = byteBufferCube.asFloatBuffer();
    positionBufferCube.put(CubeVertices);
    positionBufferCube.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, CubeVertices.length * 4, positionBufferCube, GLES32.GL_STATIC_DRAW);
    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
    GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind


    //4
    GLES32.glGenBuffers(1, vboCubeTexture, 0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboCubeTexture[0]);
    
    ByteBuffer BufferCube = ByteBuffer.allocateDirect(CubeText.length * 4);// 4 : sizeof float
    BufferCube.order(ByteOrder.nativeOrder());
    FloatBuffer TexBufferCube = BufferCube.asFloatBuffer();
    TexBufferCube.put(CubeText);
    TexBufferCube.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, CubeText.length * 4, TexBufferCube, GLES32.GL_STATIC_DRAW);
    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD, 2, GLES32.GL_FLOAT, false, 0, 0);

    GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD);

    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind

        //Vao
    GLES32.glBindVertexArray(0);

    //Texture Load
    GLES32.glEnable(GLES32.GL_TEXTURE_2D);

//    Texture_Stone_YSR[0] = LoadTexture(R.raw.stone);
//   Texture_Kundali_YSR[0] = LoadTexture(R.raw.kundali);

    	//Actual FBO
    GLES32.glGenFramebuffers(1, fbo, 0);
    GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER,fbo[0]);

    GLES32.glGenTextures(1, fbo_texture,0);
    GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, fbo_texture[0]);

    GLES32.glTexStorage2D(GLES32.GL_TEXTURE_2D, 1, GLES32.GL_RGBA8, 1024, 1024);
    GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);

    GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);

    GLES32.glFramebufferTexture(GLES32.GL_FRAMEBUFFER, GLES32.GL_COLOR_ATTACHMENT0, fbo_texture[0], 0);

    GLES32.glGenRenderbuffers(1, fbo_depth, 0);
    GLES32.glBindRenderbuffer(GLES32.GL_RENDERBUFFER, fbo_depth[0]);
    GLES32.glRenderbufferStorage(GLES32.GL_RENDERBUFFER, GLES32.GL_DEPTH24_STENCIL8, 1024, 1024);
    GLES32.glFramebufferRenderbuffer(GLES32.GL_FRAMEBUFFER, GLES32.GL_DEPTH_ATTACHMENT, GLES32.GL_RENDERBUFFER, fbo_depth[0]);

    final int[] draw_buffer = { GLES32.GL_COLOR_ATTACHMENT0 };
    GLES32.glDrawBuffers(1, draw_buffer, 0);
    GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, 0);

    GLES32.glEnable(GLES32.GL_DEPTH_TEST); // Hidden Surface Removal
    GLES32.glDepthFunc(GLES32.GL_LEQUAL);

    Matrix.setIdentityM(PerspectiveProjectionMatrix, 0);

    GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
  }


  private void Resize(int width, int height) 
    {
        if (height == 0) 
        {
            height = 1;
        }
        
        GLES32.glViewport(0, 0, width, height);
    
        Matrix.perspectiveM(PerspectiveProjectionMatrix, 0,
            45.0f, (float)width / (float)height, 0.1f, 100.0f);

    }

    // Scene
    private void Display()
     {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject); // Binding shaders to SPO
        GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, fbo[0]);

        GLES32.glViewport(0, 0, 1080, 1080);

        GLES32.glClearColor(0.80f, 0.40f, 0.10f, 1.0f);

        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);


        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];
        float[] rotationMatrix = new float[16]; //new

        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        //Pyramid
        //1
        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -4.5f);
     
        //2
        Matrix.rotateM(rotationMatrix, 0, angleTriangle, 0.0f, 1.0f, 0.0f);

        Matrix.multiplyMM(modelViewMatrix,0, modelViewMatrix,0, rotationMatrix,0); //Rotate
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, PerspectiveProjectionMatrix, 0, modelViewMatrix, 0);

        //Uniforms
        GLES32.glUniformMatrix4fv(fbo_mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        Texture_Stone_YSR[0] = LoadTexture(R.raw.kundali);    //imp
    
        //ABU
        GLES32.glEnable(GLES32.GL_TEXTURE_2D);
        GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, Texture_Stone_YSR[0]);
        GLES32.glUniform1i(uniform_texture0_sampler, 0);
       
        GLES32.glBindVertexArray(vaoPyramid[0]);
        
        //scene
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);

        //unbind
        GLES32.glBindVertexArray(0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,0);

        GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, 0);

        GLES32.glUseProgram(0);
    
    //Outer Cube
        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        //imp
        GLES32.glViewport(0, 0, gWidth, gHeight);
    
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(FBO_shaderProgramObject); // Binding shaders to FBO


        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -5.5f);
        
        Matrix.rotateM(rotationMatrix, 0, angleRectangle, 1.0f, 0.0f, 0.0f);
        Matrix.rotateM(rotationMatrix, 0, angleRectangle, 0.0f, 0.0f, 1.0f);
        Matrix.rotateM(rotationMatrix, 0, angleRectangle, 0.0f, 1.0f, 0.0f);

        Matrix.scaleM(modelViewMatrix,0,0.75f,0.75f,0.75f);

        Matrix.multiplyMM(modelViewMatrix,0, modelViewMatrix,0, rotationMatrix,0);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, PerspectiveProjectionMatrix,0, modelViewMatrix,0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
        

        //ABU
        GLES32.glEnable(GLES32.GL_TEXTURE_2D);
        GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, fbo_texture[0]);
        GLES32.glUniform1i(fbo_samplerUniform, 0);

        //Drawing
        GLES32.glBindVertexArray(vaoCube[0]);

        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);

        GLES32.glBindVertexArray(0);

        //common
        GLES32.glUseProgram(0);

        requestRender(); //  swapBuffers
    }

    private void Update()
    {
        angleTriangle = angleTriangle + 1.0f;
        
        if(angleTriangle >= 360.0f)
        {
            angleTriangle = 0.0f;
        }

        angleRectangle = angleRectangle + 1.0f;
        
        if(angleRectangle >= 360.0f)
        {
            angleRectangle = 0.0f;
        }

    }
    

    private void uninitialize() 
    {
        //Vao & Vbo
        if (vboPyramidPosition[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboPyramidPosition, 0);
            vboPyramidPosition[0] = 0;
        }

        if (vboPyramidTexture[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboPyramidTexture, 0);
            vboPyramidTexture[0] = 0;
        }

        if (vboCubePosition[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboCubePosition, 0);
            vboCubePosition[0] = 0;
        }

        if (vboCubeTexture[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboCubeTexture, 0);
            vboCubeTexture[0] = 0;
        }

        if (vaoPyramid[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vaoPyramid, 0);
            vaoPyramid[0] = 0;
        }

        //SPO
        if (shaderProgramObject != 0) 
        {
            int[] shaderCount = new int[1];
            int shaderNo;

            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            int[] shaders = new int[shaderCount[0]];
        
            GLES32.glGetAttachedShaders(shaderProgramObject,
                    shaderCount[0],
                    shaderCount, 0,
                    shaders, 0);

            for (shaderNo = 0; shaderNo < shaderCount[0]; shaderNo++) 
            {
                GLES32.glDetachShader(shaderProgramObject, shaders[shaderNo]);
                GLES32.glDeleteShader(shaders[shaderNo]);
                shaders[shaderNo] = 0;
            }

            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
            GLES32.glUseProgram(0);
        }

    }

}
