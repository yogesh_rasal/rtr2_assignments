package com.astromedicomp.ads;

import android.content.Context;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;


import android.opengl.GLSurfaceView;
import android.opengl.GLES32; // opengl version 3.2

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig; //Extended

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView
        implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener 
{   
    private GestureDetector gestureDetector;
    private final Context context;

    private int fragmentShaderObject;
    private int vertexShaderObject;
    private int shaderProgramObject;

    //new
    private int[] vaoCube = new int[1];

    private int[] vboCubePosition = new int[1];
    private int[] vboCubeNormal = new int[1];

    //uniforms    
    private int mvUniform;
    private int Projection_Uniform;
    private int LD_Uniform;
    private int KD_Uniform;
    private int Light_Position_Uniform;
    private int LisPressed_Uniform;


    //angles for rotation
    static float angleRectangle = 0.0f;

    private float[] PerspectiveProjectionMatrix = new float[16];

    private Boolean bisLPressed = false;

    // constructor
    public GLESView(Context drawingContext) 
    {
        super(drawingContext);
        context = drawingContext; //ghrc

        gestureDetector = new GestureDetector(drawingContext, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);

        setEGLContextClientVersion(3); // 3.x

        setRenderer(this); // onDrawFrame() - display()

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // NOTE
    }


    // Handling Events
    //1
    @Override
    public boolean onTouchEvent(MotionEvent me) 
    {
        // keyboard actions
        int eventaction = me.getAction();

        if (!gestureDetector.onTouchEvent(me)) 
        {
            super.onTouchEvent(me);
        }

        return (true);
    }

    //2
    @Override
    public boolean onDoubleTap(MotionEvent me) 
    {
        return (true);
    }

    //3
    @Override
    public boolean onDoubleTapEvent(MotionEvent me) 
    {
       
        return (true);
    }

    //4
    @Override
    public boolean onSingleTapConfirmed(MotionEvent me) {
        return (true);
    }

    //5
    @Override
    public boolean onDown(MotionEvent me) 
    {
       
        return (true);
    }

    //6
    @Override
    public boolean onFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) 
    {
        // swipe
        return (true);
    }

    //7
    @Override
    public void onLongPress(MotionEvent me) 
    {
        if(bisLPressed == false)
                  bisLPressed = true;
        else 
                  bisLPressed = false;
                
    }


    //8
    @Override
    public boolean onScroll(MotionEvent me1, MotionEvent me2, float distanceX, float distanceY) 
    {
        uninitialize();
        System.exit(0);
        return (true);
    }

    //9
    @Override
    public void onShowPress(MotionEvent me) {
    }

    //10
    @Override
    public boolean onSingleTapUp(MotionEvent me) 
    {
        return (true);
    }


    // 3 functions of GLSurfaceView.Renderer
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) 
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR: GL10.GL_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_VENDOR);
        System.out.println("RTR: GLES32.GL_VENDOR: " + version);

        version = gl.glGetString(GLES32.GL_RENDERER);
        System.out.println("RTR: GLES32.GL_RENDERER: " + version);

        Initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) 
    {
        Resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused) 
    {
        Update();
        Display();
    }

    //3 Methods 
    private void Initialize() 
    {
        //Shaders
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
        final String vertexShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "in vec4 vPosition;"						+
            "in vec3 vNormal;"							+
            "out vec3 diffuse_Color;"					+
            "\n"										+
            "uniform mat4 u_mv_matrix;"					+
            "uniform mat4 u_projection_matrix;"			+
            "uniform int u_LKeyPressed;"				+
            "uniform vec3 u_LD;"						+
            "uniform vec3 u_KD;"						+
            "uniform vec4 u_Light_Position;"			+
            "void main(void)"							+
            "{"											+
            "if(u_LKeyPressed == 1)"					+
            "{"											+
            "vec4 Eye_Coordinates = u_mv_matrix * vPosition;"	+
            "mat3 Normal_Matrix = mat3(transpose(inverse(u_mv_matrix)));"	+
            "vec3 T_Norm = normalize(Normal_Matrix * vNormal);"	+
            "vec3 S = vec3(u_Light_Position) - (Eye_Coordinates.xyz);" +
            "diffuse_Color = (u_LD * u_KD * dot(S,T_Norm));"  +
            "}"											+
            "gl_Position = (u_projection_matrix * u_mv_matrix * vPosition);"	+
            "}"                
            );
        
        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        // Error checking
        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); 
        
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
         {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR2: Error in Vertex shader compilation log: " + szInfoLog);
            }
             else
             {
                System.out.println("RTR2:Error in Vertex Shader.");
             }
            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Vertex Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        final String fragmentShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;"    +
            "precision highp int;"      +
            "in vec3 diffuse_Color;"	+
            "out vec4 FragColor;"		+
            "uniform int u_LKeyPressed;"+
            "\n"						+
            "void main(void)"		    +
            "{"						    +
            "if(u_LKeyPressed == 1)"	+
            "{"						    +
            "FragColor = vec4(diffuse_Color,1.0);"		+
            "}"							+
            "else"						+
            "{"							+
            "FragColor = vec4(0.5,0.5,0.5,1.0);" +
            "}"							+
            "}" 
        );
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
        GLES32.glCompileShader(fragmentShaderObject);

        // Error checking
        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // 1st
        
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE) 
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR2: Error in Fragment shader compilation log: " + szInfoLog);
            } 
            else 
            {
                System.out.println("RTR2:Error in Fragment Shader.");
            }
            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Fragment Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        shaderProgramObject = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Pre Link Attribute Binding
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        
        //Normals
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

        GLES32.glLinkProgram(shaderProgramObject);

        // Error checking
        int[] iProgramLinkStatus = new int[1];

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if (iProgramLinkStatus[0] == GLES32.GL_FALSE) 
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
        
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("RTR2: Error in Shader Program compilation log: " + szInfoLog);
            } 
            else 
            {
                System.out.println("RTR2: Error in Shader Program.");
            }

            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Shader Program compiled successfully.");
        }

        // Post Link Uniform Location
        mvUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mv_matrix");

        Projection_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

        KD_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_KD");
        
        Light_Position_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Light_Position");
     
        LD_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_LD");
        
        LisPressed_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_LKeyPressed");
     
        // reset flags
        iProgramLinkStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;


    /********* Cube **********/
      final float[] CubeVertices = new float[]
        {
            1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,

            1.0f, 1.0f, -1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,

            -1.0f, 1.0f, -1.0f,
            1.0f, 1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,

            -1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f,

            -1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,

            1.0f, -1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            -1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f	
        };  

    GLES32.glGenVertexArrays(1, vaoCube, 0);
    GLES32.glBindVertexArray(vaoCube[0]);
    GLES32.glGenBuffers(1, vboCubePosition, 0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboCubePosition[0]);
    ByteBuffer byteBufferCube = ByteBuffer.allocateDirect(CubeVertices.length * 4);// 4 : sizeof float
    byteBufferCube.order(ByteOrder.nativeOrder());

    FloatBuffer positionBufferCube = byteBufferCube.asFloatBuffer();
    positionBufferCube.put(CubeVertices);
    positionBufferCube.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, CubeVertices.length * 4, positionBufferCube, GLES32.GL_STATIC_DRAW);
    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
    GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind
    positionBufferCube.clear();


    //color
    final float[] CubeNormals = new float[]
    {
		0.0f, 0.0f, 1.0f , 
		0.0f, 0.0f, 1.0f , 
		0.0f, 0.0f, 1.0f , 
		0.0f, 0.0f, 1.0f , 
		
		1.0f, 0.0f, 0.0f, 
		1.0f , 0.0f , 0.0f , 
		1.0f , 0.0f , 0.0f , 
		1.0f , 0.0f , 0.0f , 
		
		0.0f , 0.0f , -1.0f , 
		0.0f , 0.0f , -1.0f , 
		0.0f , 0.0f , -1.0f , 
		0.0f , 0.0f , -1.0f , 
		
		-1.0f , 0.0f , 0.0f , 
		-1.0f , 0.0f , 0.0f , 
		-1.0f , 0.0f , 0.0f , 
		-1.0f , 0.0f , 0.0f , 
		
		0.0f , 1.0f , 0.0f , 
		0.0f , 1.0f , 0.0f , 
		0.0f , 1.0f , 0.0f , 
		0.0f , 1.0f , 0.0f , 
		
		0.0f , -1.0f , 0.0f , 
		0.0f , -1.0f , 0.0f , 
		0.0f , -1.0f , 0.0f , 
		0.0f , -1.0f , 0.0f
    };

    //Light
    GLES32.glGenBuffers(1, vboCubeNormal, 0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboCubeNormal[0]);
    positionBufferCube.put(CubeNormals);
    positionBufferCube.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, CubeNormals.length * 4, positionBufferCube, GLES32.GL_STATIC_DRAW);
    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);

    GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);

    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind


        //common
      GLES32.glBindVertexArray(0);


      GLES32.glEnable(GLES32.GL_DEPTH_TEST); // Hidden Surface Removal
      GLES32.glDepthFunc(GLES32.GL_LEQUAL);

      Matrix.setIdentityM(PerspectiveProjectionMatrix, 0);

      GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    }

    private void Resize(int width, int height) 
    {
        if (height == 0) 
        {
            height = 1;
        }
        
        GLES32.glViewport(0, 0, width, height);
    
        Matrix.perspectiveM(PerspectiveProjectionMatrix, 0,
            45.0f, (float)width / (float)height, 0.1f, 100.0f);
        
    }

    // Scene
    private void Display()
     {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject); // Binding shaders to SPO
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];
        float[] rotationMatrix = new float[16]; //new

        //Cube
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -5.0f);
        
      //  Matrix.translateM(modelViewMatrix, 0, 1.5f, 0.0f, -3.0f);
        Matrix.rotateM(rotationMatrix, 0, angleRectangle, 1.0f, 0.0f, 0.0f);

        Matrix.rotateM(rotationMatrix, 0, angleRectangle, 0.0f, 1.0f, 0.0f);
        Matrix.rotateM(rotationMatrix, 0, angleRectangle, 0.0f, 0.0f, 1.0f);

        Matrix.multiplyMM(modelViewMatrix,0, modelViewMatrix,0, rotationMatrix,0);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, PerspectiveProjectionMatrix,0, modelViewMatrix,0);
        
        //Uniforms
        GLES32.glUniformMatrix4fv(mvUniform, 1, false, modelViewMatrix, 0);

        //Projection            
        GLES32.glUniformMatrix4fv(Projection_Uniform,1,
        false, // Transpose
        PerspectiveProjectionMatrix,
        0);

            //Lights
        if (bisLPressed == true)
        {
            GLES32.glUniform1i(LisPressed_Uniform, 1);
            GLES32.glUniform3f(LD_Uniform, 0.5f, 0.5f, 0.5f);
            GLES32.glUniform3f(KD_Uniform, 0.5f, 0.5f, 0.5f);

            //imp : out to in Light
           // float Lightposition[] = {0.5f, 0.0f, 2.0f, 1.0f};
            GLES32.glUniform4f(Light_Position_Uniform, 0.0f, 0.0f, -1.0f, 1.0f);
        }

        else
        {
            GLES32.glUniform1i(LisPressed_Uniform, 0);
        }


        //vao
        GLES32.glBindVertexArray(vaoCube[0]);

        //Drawing
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);

        GLES32.glBindVertexArray(0);

        //common
        GLES32.glUseProgram(0);

        requestRender(); //  swapBuffers
    }

    //new
    private void Update()
    {

        angleRectangle = angleRectangle + 0.5f;
        
        if(angleRectangle >= 360.0f)
        {
            angleRectangle = 0.0f;
        }


    }
    

    private void uninitialize() 
    {
        //Vao & Vbo
        if (vboCubePosition[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboCubePosition, 0);
            vboCubePosition[0] = 0;
        }

        if (vboCubeNormal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboCubeNormal, 0);
            vboCubeNormal[0] = 0;
        }

                //SPO
        if (shaderProgramObject != 0) 
        {
            int[] shaderCount = new int[1];
            int shaderNo;

            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            int[] shaders = new int[shaderCount[0]];
        
            GLES32.glGetAttachedShaders(shaderProgramObject,
                    shaderCount[0],
                    shaderCount, 0,
                    shaders, 0);

            for (shaderNo = 0; shaderNo < shaderCount[0]; shaderNo++) 
            {
                GLES32.glDetachShader(shaderProgramObject, shaders[shaderNo]);
                GLES32.glDeleteShader(shaders[shaderNo]);
                shaders[shaderNo] = 0;
            }

            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
            GLES32.glUseProgram(0);
        }

    }

}
