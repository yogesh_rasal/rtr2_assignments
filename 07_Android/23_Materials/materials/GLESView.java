package com.astromedicomp.materials;

import android.content.Context;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;


import android.opengl.GLSurfaceView;
import android.opengl.GLES32; // opengl version 3.2

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig; //Extended

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;    
//new
import android.opengl.Matrix;

public class GLESView extends GLSurfaceView
        implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener 
{
    //Fields   
    private GestureDetector gestureDetector;
    private final Context context;

    private int fragmentShaderObject_YSR;
    private int vertexShaderObject_YSR;
    private int shaderProgramObject_YSR;

    private float[] PerspectiveProjectionMatrix_YSR = new float[16];

    //angles for rotation
    static float AngleTri = 0.0f;
    //int XAxis,YAxis,ZAxis;

    //uniforms    
    private int mvUniform;
    private int View_Uniform;
    private int Projection_Uniform;

    private int LD_Uniform;
    private int KD_Uniform;

    private int LA_Uniform;
    private int KA_Uniform;

    private int LS_Uniform;
    private int KS_Uniform;

    private int Material_Shininess_YSR;

    private int Light_Position_Uniform;
    private int LisPressed_Uniform;

    private Boolean bisLPressed = false;

    //for sphere
    private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];
    //optional
    float sphere_textures[]=new float[764];

    private int numVertices ,numElements;

    //new
    int g_Width,g_Height,tap=0;


    // constructor
    public GLESView(Context drawingContext) 
    {
        super(drawingContext);
        context = drawingContext; //ghrc

        gestureDetector = new GestureDetector(drawingContext, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);

        setEGLContextClientVersion(3); // 3.x

        setRenderer(this); // onDrawFrame() - display()

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // NOTE
    }


    // Handling Events
    //1
    @Override
    public boolean onTouchEvent(MotionEvent me) 
    {
        // keyboard actions
        int eventaction = me.getAction();

        if (!gestureDetector.onTouchEvent(me)) 
        {
            super.onTouchEvent(me);
        }

        return (true);
    }

    //2
    @Override
    public boolean onDoubleTap(MotionEvent me) 
    {
        if(bisLPressed == false)
         bisLPressed = true;
        else 
         bisLPressed = false;

        return (true);
    }

    //3
    @Override
    public boolean onDoubleTapEvent(MotionEvent me) 
    {

        return (true);
    }

    //4
    @Override
    public boolean onSingleTapConfirmed(MotionEvent me) 
    {
        return (true);
    }

    //5
    @Override
    public boolean onDown(MotionEvent me) 
    {
       
        return (true);
    }

    //6
    @Override
    public boolean onFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) 
    {
        // swipe
       return (true);
    }

    //7
    @Override
    public void onLongPress(MotionEvent me) 
    {
                
                uninitialize();
                System.exit(0);
            
    }


    //8
    @Override
    public boolean onScroll(MotionEvent me1, MotionEvent me2, float distanceX, float distanceY) 
    {
        tap = tap+1;

        if(tap > 3)
            tap = 1;
  
        return (true);
    }

    //9
    @Override
    public void onShowPress(MotionEvent me) 
    {

    }

    //10
    @Override
    public boolean onSingleTapUp(MotionEvent me) 
    {
        return (true);
    }


    // 3 functions of GLSurfaceView.Renderer
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) 
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR: GL10.GL_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_VENDOR);
        System.out.println("RTR: GLES32.GL_VENDOR: " + version);

        version = gl.glGetString(GLES32.GL_RENDERER);
        System.out.println("RTR: GLES32.GL_RENDERER: " + version);

        Initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) 
    {
        g_Width = width;
        g_Height = height;
        Resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused) 
    {
        Update();
        Display();
    }

    //3 Methods 
    private void Initialize() 
    {
        //Shaders
        vertexShaderObject_YSR = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
        final String vertexShaderSourceCode = String.format(
            "#version 320 es"           +
            "\n"                        +
            "precision highp float;"	+
            "in vec4 vPosition;"						+
            "in vec3 vNormal;"							+
            "out vec3 T_Norm;"							+
            "out vec3 Light_Direction;"					+
            "out vec3 Viewer_Vector;"					+
            "\n"										+ 
            "uniform mat4 u_model_matrix;"				+ 
            "uniform mat4 u_view_matrix;"				+ 
            "uniform mat4 u_projection_matrix;"			+ 
            "uniform vec4 u_Light_Position;"			+
            "\n"										+
            "void main(void)"							+
            "{"											+
            " vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"	+ 
            " T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"			    + 
            " Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"    +    
            " Viewer_Vector = vec3(-Eye_Coordinates.xyz);"                          +   
            "gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"	+ 
            "}"                
            );
        
        GLES32.glShaderSource(vertexShaderObject_YSR, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject_YSR);

        // Error checking
        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject_YSR, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); 
        
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
         {
            GLES32.glGetShaderiv(vertexShaderObject_YSR, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject_YSR);
                System.out.println("RTR2: Error in Vertex shader compilation log: " + szInfoLog);
            }
             else
             {
                System.out.println("RTR2:Error in Vertex Shader.");
             }
            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Vertex Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        fragmentShaderObject_YSR = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        final String fragmentShaderSourceCode = String.format(
            "#version 320 es"           +
            "\n"                        +
            "precision highp float;"    +
            "precision highp int;"      +
            "in vec3 T_Norm;"			                +
            "in vec3 Light_Direction;"	                +
            "in vec3 Viewer_Vector;"	                +
            "out vec4 FragColor;"		                +
            "\n"						                +
            "uniform vec3 u_LA;"						+
            "uniform vec3 u_LD;"						+
            "uniform vec3 u_LS;"						+
            "uniform vec3 u_KA;"						+
            "uniform vec3 u_KD;"						+
            "uniform vec3 u_KS;"						+
            "uniform float u_Shininess;"				+
            "uniform int u_LKeyPressed;"				+
            "uniform vec4 u_Light_Position;"			+
            "\n"										+
            "void main(void) "		                    +
            "{"						                    +
            "if(u_LKeyPressed == 1)"                    +
            "{"						                    +
              "vec3 Normalized_View_Vector = normalize(Viewer_Vector); \n"	+
              "vec3 Normalized_Light_Direction = normalize(Light_Direction); \n"+
              "vec3 Normalized_TNorm = normalize(T_Norm); \n "+
              "float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0); \n "	+
              "vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm); \n "+
              "vec3 Ambient = vec3(u_LA * u_KA); \n "	+
              "vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD); \n "	+
              "vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess)); \n " +
              "vec3 Phong_ADS_Light = Ambient + Diffuse+ Specular; \n"	+
              "FragColor = vec4(Phong_ADS_Light,1.0); \n " +
            "}"		+
            "else"	+
            "{"		+
            " FragColor = vec4(1.0,1.0,1.0,1.0);" +
            "}"+
          "}" 
        );
        GLES32.glShaderSource(fragmentShaderObject_YSR, fragmentShaderSourceCode);
        GLES32.glCompileShader(fragmentShaderObject_YSR);

        // Error checking
        GLES32.glGetShaderiv(fragmentShaderObject_YSR, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // 1st
        
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE) 
        {
            GLES32.glGetShaderiv(fragmentShaderObject_YSR, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject_YSR);
                System.out.println("RTR2: Error in Fragment shader compilation log: " + szInfoLog);
            } 
            else 
            {
                System.out.println("RTR2:Error in Fragment Shader.");
            }
            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Fragment Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        shaderProgramObject_YSR = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject_YSR, vertexShaderObject_YSR);
        GLES32.glAttachShader(shaderProgramObject_YSR, fragmentShaderObject_YSR);

        // Pre Link Attribute Binding
        GLES32.glBindAttribLocation(shaderProgramObject_YSR, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        
        //Normals
        GLES32.glBindAttribLocation(shaderProgramObject_YSR, GLESMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

        GLES32.glLinkProgram(shaderProgramObject_YSR);

        // Error checking
        int[] iProgramLinkStatus = new int[1];

        GLES32.glGetProgramiv(shaderProgramObject_YSR, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if (iProgramLinkStatus[0] == GLES32.GL_FALSE) 
        {
            GLES32.glGetProgramiv(shaderProgramObject_YSR, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
        
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject_YSR);
                System.out.println("RTR2: Error in Shader Program compilation log: " + szInfoLog);
            } 
            else 
            {
                System.out.println("RTR2: Error in Shader Program.");
            }

            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Shader Program compiled successfully.");
        }

        // Post Link Uniform Location
        mvUniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_model_matrix");

        View_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_view_matrix");

        Projection_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_projection_matrix");

        LD_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_LD");

        KD_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_KD");

        LS_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_LS");

        KS_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_KS");

        LA_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_LA");

        KA_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_KA");
     
        Material_Shininess_YSR = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_Shininess");

        Light_Position_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_Light_Position");
     
        LisPressed_Uniform = GLES32.glGetUniformLocation(shaderProgramObject_YSR, "u_LKeyPressed");
     
        // reset flags
        iProgramLinkStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

//Shapes

        Sphere sphere = new Sphere();
        float sphere_vertices[] = new float[1146];
        float sphere_normals[] = new float[1146];
        short sphere_elements[] = new short[2280];

        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
          numVertices = sphere.getNumberOfSphereVertices();
          numElements = sphere.getNumberOfSphereElements();

        // vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);

        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());

        FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                                    3,
                                    GLES32.GL_FLOAT,
                                    false,0,0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);

        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL,
                                    3,
                                    GLES32.GL_FLOAT,
                                    false,0,0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);

        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);

        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);


      GLES32.glEnable(GLES32.GL_DEPTH_TEST); // Hidden Surface Removal
      GLES32.glDepthFunc(GLES32.GL_LEQUAL);

      Matrix.setIdentityM(PerspectiveProjectionMatrix_YSR, 0);

      GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
   }

  private void Resize(int width, int height) 
   {
        if (height == 0) 
        {
            height = 1;
        }
        
        GLES32.glViewport(0, 0, width, height);
    
        Matrix.perspectiveM(PerspectiveProjectionMatrix_YSR, 0,
            45.0f, (float)width / (float)height, 0.1f, 100.0f);
        
   }

    // Scene
    private void Display()
     {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject_YSR); // Binding shaders to SPO

        float[] modelMatrix = new float[16];
        float[] ViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];
        float[] rotationMatrix = new float[16]; 
        
        //new

        float [] Material_Ambient = new float[72]; 
	    float [] Material_Diffuse = new float[72]; 
	    float [] Material_Specular = new float[72]; 
	    float [] Material_Shininess = new float[24]; 
	
        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.setIdentityM(ViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        
	//24 Spheres
// 1 : Emerald
	Material_Ambient[0] = 0.0215f;
	Material_Ambient[1] = 0.1745f;
	Material_Ambient[2] = 0.0215f;

	Material_Diffuse[0] = 0.0215f;
	Material_Diffuse[1] = 0.1745f;
	Material_Diffuse[2] = 0.0215f;
    //
    
	Material_Specular[0] = 0.633f;
	Material_Specular[1] = 0.727811f;
	Material_Specular[2] = 0.633f;

	Material_Shininess[0] = 0.6f * 128.0f;


// 2 = Jade
	Material_Ambient[3] = 0.135f;
	Material_Ambient[4] = 0.2225f;
	Material_Ambient[5] = 0.1575f;
	//
	///

	Material_Diffuse[3] = 0.54f;
	Material_Diffuse[4] = 0.89f;
	Material_Diffuse[5] = 0.63f;
    //
	//
    
	Material_Specular[3] = 0.316228f;
	Material_Specular[4] = 0.316228f;
	Material_Specular[5] = 0.316228f;
	//

	Material_Shininess[1] = 0.1f * 128.0f;


// 3 = obisidian
	Material_Ambient[6] = 0.05375f;
	Material_Ambient[7] = 0.05f;
	Material_Ambient[8] = 0.06625f;
	//

	Material_Diffuse[6] = 0.18275f;
	Material_Diffuse[7] = 0.17f;
	Material_Diffuse[8] = 0.22525f;
    //
	//
    
	Material_Specular[6] = 0.332741f;
	Material_Specular[7] = 0.328634f;
	Material_Specular[8] = 0.346435f;
	//
    //

	Material_Shininess[2] = 0.3f * 128.0f;
    //   

// 4= Pearl
	Material_Ambient[9] = 0.25f;
	Material_Ambient[10] = 0.20725f;
	Material_Ambient[11] = 0.20725f;
	//
	///

	Material_Diffuse[9] = 1.0f;
	Material_Diffuse[10] = 0.829f;
	Material_Diffuse[11] = 0.829f;
    //
	//
    
	Material_Specular[9] = 0.296648f;
	Material_Specular[10] = 0.296648f;
	Material_Specular[11] = 0.296648f;
	//
    //

	Material_Shininess[3] = 0.088f * 128.0f;
    //

// 5 = Ruby
	Material_Ambient[12] = 0.1745f;
	Material_Ambient[13] = 0.01175f;
	Material_Ambient[14] = 0.01175f;
	//
	///

	Material_Diffuse[12] = 0.61424f;
	Material_Diffuse[13] = 0.04136f;
	Material_Diffuse[14] = 0.04136f;
    //
    
	Material_Specular[12] = 0.727811f;
	Material_Specular[13] = 0.626959f;
	Material_Specular[14] = 0.626959f;
	//

	Material_Shininess[4] = 0.6f * 128.0f;
    //

// 6 = Turquoise
	Material_Ambient[15] = 0.1f;
	Material_Ambient[16] = 0.18275f;
	Material_Ambient[17] = 0.1745f;
	//
	///

	Material_Diffuse[15] = 0.396f;
	Material_Diffuse[16] = 0.74151f;
	Material_Diffuse[17] = 0.69102f;
    //
	//
    
	Material_Specular[15] = 0.297524f;
	Material_Specular[16] = 0.30829f;
	Material_Specular[17] = 0.306678f;
	//
    //

	Material_Shininess[5] = 0.1f * 128.0f;
    //
  

// Metals   ---------------------

// 1 : Brass
	Material_Ambient[18] = 0.349412f;
	Material_Ambient[19] = 0.30829f;
	Material_Ambient[20] = 0.306678f;
	///

	Material_Diffuse[18] = 0.780392f;
	Material_Diffuse[19] = 0.223529f;
	Material_Diffuse[20] = 0.027451f;
    //
	//
    
	Material_Specular[18] = 0.992157f;
	Material_Specular[19] = 0.941176f;
	Material_Specular[20] = 0.807843f;
	//
    //

	Material_Shininess[6] = 0.21794872f * 128.0f;
    //

// 2 = Bronze
	Material_Ambient[21] = 0.2125f;
	Material_Ambient[22] = 0.1275f;
	Material_Ambient[23] = 0.054f;
	//
	///

	Material_Diffuse[21] = 0.714f;
	Material_Diffuse[22] = 0.4284f;
	Material_Diffuse[23] = 0.18144f;
    //
	//
    
	Material_Specular[21] = 0.393548f;
	Material_Specular[22] = 0.271906f;
	Material_Specular[23] = 0.166721f;
	//
    //

	Material_Shininess[7] = 0.2f * 128.0f;
    //

// 3 = Chrome
	Material_Ambient[24] = 0.25f;
	Material_Ambient[25] = 0.25f;
	Material_Ambient[26] = 0.25f;
	//
	///

	Material_Diffuse[24] = 0.4f;
	Material_Diffuse[25] = 0.4f;
	Material_Diffuse[26] = 0.4f;
    //
	//
    
	Material_Specular[24] = 0.774597f;
	Material_Specular[25] = 0.774597f;
	Material_Specular[26] = 0.774597f;
	//
    //

	Material_Shininess[8] = 0.6f * 128.0f;
    //

	
// 4= copper
	Material_Ambient[27] = 0.19125f;
	Material_Ambient[28] = 0.0735f;
	Material_Ambient[29] = 0.02025f;
	//
	///

	Material_Diffuse[27] = 0.7038f;
	Material_Diffuse[28] = 0.27048f;
	Material_Diffuse[29] = 0.0828f;
    //
	//
    
	Material_Specular[27] = 0.25677f;
	Material_Specular[28] = 0.137622f;
	Material_Specular[29] = 0.086014f;
	//
    //

	Material_Shininess[9] = 0.1f * 128.0f;


// 5 = Gold
	Material_Ambient[30] = 0.24725f;
	Material_Ambient[31] = 0.1995f;
	Material_Ambient[32] = 0.0745f;
	//
	///

	Material_Diffuse[30] = 0.7517f;
	Material_Diffuse[31] = 0.6065f;
	Material_Diffuse[32] = 0.2265f;
    //
    
	Material_Specular[30] = 0.6283f;
	Material_Specular[31] = 0.55580f;
	Material_Specular[32] = 0.36606f;
	//

	Material_Shininess[10] = 0.4f * 128.0f;
    //

// 6 = Silver
	Material_Ambient[33] = 0.19225f;
	Material_Ambient[34] = 0.19225f;
	Material_Ambient[35] = 0.19225f;
	//

	Material_Diffuse[33] = 0.5075f;
	Material_Diffuse[34] = 0.5075f;
	Material_Diffuse[35] = 0.5075f;
    //
	//
    
	Material_Specular[33] = 0.50828f;
	Material_Specular[34] = 0.50828f;
	Material_Specular[35] = 0.50828f;
	//
    //

	Material_Shininess[11] = 0.4f * 128.0f;
    //


// Plastic ---------------------
// 1 - Black
	Material_Ambient[36] = 0.0f;
	Material_Ambient[37] = 0.0f;
	Material_Ambient[38] = 0.0f;
	//
	///

	Material_Diffuse[36] = 0.01f;
	Material_Diffuse[37] = 0.01f;
	Material_Diffuse[38] = 0.01f;
    //
	//
    
	Material_Specular[36] = 0.5f;
	Material_Specular[37] = 0.5f;
	Material_Specular[38] = 0.5f;
	//
    //

	Material_Shininess[12] = 0.25f * 128.0f;
    //


// 2 - Cyan
	Material_Ambient[39] = 0.0f;
	Material_Ambient[40] = 0.1f;
	Material_Ambient[41] = 0.06f;
	//
	///

	Material_Diffuse[39] = 0.0f;
	Material_Diffuse[40] = 0.5098039f;
	Material_Diffuse[41] = 0.5098039f;
    //
	//
    
	Material_Specular[39] = 0.0f;
	Material_Specular[40] = 0.501960f;
	Material_Specular[41] = 0.501960f;
	//

	Material_Shininess[13] = 0.25f * 128.0f;
    //

//3 - Green
	Material_Ambient[42] = 0.0f;
	Material_Ambient[43] = 0.1f;
	Material_Ambient[44] = 0.06f;
	//
	///

	Material_Diffuse[42] = 0.1f;
	Material_Diffuse[43] = 0.35f;
	Material_Diffuse[44] = 0.1f;
    //
	//
    
	Material_Specular[42] = 0.45f;
	Material_Specular[43] = 0.55f;
	Material_Specular[44] = 0.45f;
	//
    //

	Material_Shininess[14] = 0.25f * 128.0f;
    //


// Red

	Material_Ambient[45] = 0.0f;
	Material_Ambient[46] = 0.0f;
	Material_Ambient[47] = 0.0f;
	//
	///

	Material_Diffuse[45] = 0.5f;
	Material_Diffuse[46] = 0.0f;
	Material_Diffuse[47] = 0.0f;
    //
	//
    
	Material_Specular[45] = 0.7f;
	Material_Specular[46] = 0.6f;
	Material_Specular[47] = 0.6f;
	//
    //

	Material_Shininess[15] = 0.25f * 128.0f;


// White
	Material_Ambient[48] = 0.0f;
	Material_Ambient[49] = 0.0f;
	Material_Ambient[50] = 0.0f;
	//
	///

	Material_Diffuse[48] = 0.55f;
	Material_Diffuse[49] = 0.55f;
	Material_Diffuse[50] = 0.55f;
    //
	//
    
	Material_Specular[48] = 0.7f;
	Material_Specular[49] = 0.7f;
	Material_Specular[50] = 0.7f;
	//
    //

	Material_Shininess[16] = 0.25f * 128.0f;
    //


// Yellow
	Material_Ambient[51] = 0.0f;
	Material_Ambient[52] = 0.0f;
	Material_Ambient[53] = 0.0f;
	//
	///

	Material_Diffuse[51] = 0.5f;
	Material_Diffuse[52] = 0.5f;
	Material_Diffuse[53] = 0.0f;
    
	Material_Specular[51] = 0.6f;
	Material_Specular[52] = 0.6f;
	Material_Specular[53] = 0.5f;

	Material_Shininess[17] = 0.25f * 128.0f;


// -------- Rubber
// 1 - Black
	Material_Ambient[54] = 0.02f;
	Material_Ambient[55] = 0.02f;
	Material_Ambient[56] = 0.02f;
	//
	///

	Material_Diffuse[54] = 0.01f;
	Material_Diffuse[55] = 0.01f;
	Material_Diffuse[56] = 0.01f;
    //
	//
    
	Material_Specular[54] = 0.4f;
	Material_Specular[55] = 0.4f;
	Material_Specular[56] = 0.4f;
	//
    //

	Material_Shininess[18] = 0.07813f * 128.0f;
    //


// 2 - Cyan
	Material_Ambient[57] = 0.0f;
	Material_Ambient[58] = 0.05f;
	Material_Ambient[59] = 0.05f;
	//
	///

	Material_Diffuse[57] = 0.4f;
	Material_Diffuse[58] = 0.5098039f;
	Material_Diffuse[59] = 0.5098039f;
    //
	//
    
	Material_Specular[57] = 0.0f;
	Material_Specular[58] = 0.501960f;
	Material_Specular[59] = 0.501960f;
	//
    //

	Material_Shininess[19] = 0.07813f * 128.0f;
    //

	//
	
//3 - Green
	Material_Ambient[60] = 0.0f;
	Material_Ambient[61] = 0.1f;
	Material_Ambient[62] = 0.06f;
	//
	///

	Material_Diffuse[60] = 0.1f;
	Material_Diffuse[61] = 0.35f;
	Material_Diffuse[62] = 0.1f;
    //
	//
    
	Material_Specular[60] = 0.45f;
	Material_Specular[61] = 0.55f;
	Material_Specular[62] = 0.45f;
	//
    //

	Material_Shininess[20] = 0.07813f * 128.0f;
    //

// 4- Red

	Material_Ambient[63] = 0.0f;
	Material_Ambient[64] = 0.0f;
	Material_Ambient[65] = 0.0f;
	//
	///

	Material_Diffuse[63] = 0.5f;
	Material_Diffuse[64] = 0.0f;
	Material_Diffuse[65] = 0.0f;
    //
	//
    
	Material_Specular[63] = 0.7f;
	Material_Specular[64] = 0.6f;
	Material_Specular[65] = 0.6f;
	//
    //

	Material_Shininess[21] = 0.07813f * 128.0f;
    //

// 5- White
	Material_Ambient[66] = 0.0f;
	Material_Ambient[67] = 0.0f;
	Material_Ambient[68] = 0.0f;
	//
	///

	Material_Diffuse[66] = 0.55f;
	Material_Diffuse[67] = 0.55f;
	Material_Diffuse[68] = 0.55f;
    //
	//
    
	Material_Specular[66] = 0.7f;
	Material_Specular[67] = 0.7f;
	Material_Specular[68] = 0.7f;
	//
    //

	Material_Shininess[22] = 0.07813f * 128.0f;

// 6- Yellow
	Material_Ambient[69] = 0.0f;
	Material_Ambient[70] = 0.0f;
	Material_Ambient[71] = 0.0f;

	Material_Diffuse[69] = 0.5f;
	Material_Diffuse[70] = 0.5f;
	Material_Diffuse[71] = 0.0f;
    
	Material_Specular[69] = 0.6f;
	Material_Specular[70] = 0.6f;
	Material_Specular[71] = 0.5f;

	Material_Shininess[23] = 0.07813f * 128.0f;

    //Code
    
    Matrix.translateM(modelMatrix, 0, 0.0f, 0.0f, -2.0f);
        
    Matrix.multiplyMM(modelMatrix,0, modelMatrix,0, rotationMatrix,0);
    Matrix.multiplyMM(modelViewProjectionMatrix,0, PerspectiveProjectionMatrix_YSR,0, modelMatrix,0);
    
    //Uniforms
    GLES32.glUniformMatrix4fv(mvUniform, 1, false, modelMatrix, 0);

    GLES32.glUniformMatrix4fv(View_Uniform, 1, false, ViewMatrix, 0);

    //Projection            
    GLES32.glUniformMatrix4fv(Projection_Uniform,1,
        false, // Transpose
        PerspectiveProjectionMatrix_YSR,
        0);

        //Lights
    if (bisLPressed == true)
    {
        GLES32.glUniform1i(LisPressed_Uniform, 1);

        GLES32.glUniform3f(LD_Uniform, 1.0f, 1.0f, 1.0f);

        GLES32.glUniform3f(LS_Uniform, 1.0f, 1.0f, 1.0f);

        GLES32.glUniform3f(LA_Uniform, 0.2f, 0.2f, 0.2f);
        
        //imp : out to in Light

        if(tap == 1)
        GLES32.glUniform4f(Light_Position_Uniform, 0.0f, (float)Math.sin(AngleTri+45)*3, (float)Math.cos(AngleTri+45)*3 , 1.0f); 

        //y
         if(tap == 2)
        GLES32.glUniform4f(Light_Position_Uniform,(float)Math.sin(AngleTri+45)*3 ,(float)Math.cos(AngleTri+45)*3 , 0.0f, 1.0f);

        //z
         if(tap == 3)
        GLES32.glUniform4f(Light_Position_Uniform,(float)Math.sin(AngleTri+45)*3,  0.0f, (float)Math.cos(AngleTri+45)*3, 1.0f);

    }

    else
    {
        GLES32.glUniform1i(LisPressed_Uniform, 0);
    }

    // bind vao
    GLES32.glBindVertexArray(vao_sphere[0]);
        
    // *** draw by glDrawTriangles() or glDrawArrays() or glDrawElements()
    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);


    //imp

    int width = (int)(g_Width / 6);
	int height = (int)(g_Height / 4);

  	int i,j;

   for(i = 0; i <= 6; i++)
    {
     for(j = 0; j <= 4 ; j++)
	 {

        GLES32.glUniform1f(Material_Shininess_YSR, Material_Shininess[i+j]);

        GLES32.glUniform3f(KD_Uniform, Material_Diffuse[3*i+j],Material_Diffuse[3*i+j+1],Material_Diffuse[3*i+j+2]);

        GLES32.glUniform3f(KS_Uniform, Material_Specular[3*i+j],Material_Specular[3*i+j+1],Material_Specular[3*i+j+2]);
        
        GLES32.glUniform3f(KA_Uniform, Material_Ambient[3*i+j],Material_Ambient[3*i+j+1],Material_Ambient[3*i+j+2]);

        //viewport
        GLES32.glViewport(width*i, height*j, width, height);
    
        Matrix.perspectiveM(PerspectiveProjectionMatrix_YSR, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);

        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

     }
  }

    // unbind vao
    GLES32.glBindVertexArray(0);

    //common
    GLES32.glUseProgram(0);

    //imp
    AngleTri = AngleTri + 0.035f;
        
    if(AngleTri >= 360.0f)
    {
        AngleTri = 0.0f;
    }

    requestRender(); //  swapBuffers
 }

    //new
    private void Update()
    {

    }
    

    private void uninitialize() 
    {
        //Vao & Vbo
        if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        //  position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        //  normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        //  element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }
        
        //SPO
        if (shaderProgramObject_YSR != 0) 
        {
            int[] shaderCount = new int[1];
            int shaderNo;

            GLES32.glUseProgram(shaderProgramObject_YSR);
            GLES32.glGetProgramiv(shaderProgramObject_YSR, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            int[] shaders = new int[shaderCount[0]];
        
            GLES32.glGetAttachedShaders(shaderProgramObject_YSR,
                    shaderCount[0],
                    shaderCount, 0,
                    shaders, 0);

            for (shaderNo = 0; shaderNo < shaderCount[0]; shaderNo++) 
            {
                GLES32.glDetachShader(shaderProgramObject_YSR, shaders[shaderNo]);
                GLES32.glDeleteShader(shaders[shaderNo]);
                shaders[shaderNo] = 0;
            }

            GLES32.glDeleteProgram(shaderProgramObject_YSR);
            shaderProgramObject_YSR = 0;
            GLES32.glUseProgram(0);
        }

    }

}
