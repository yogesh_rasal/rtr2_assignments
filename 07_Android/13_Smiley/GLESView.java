
//smiley
package com.astromedicomp.smiley;

import android.content.Context;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32; // opengl version 3.2

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig; //Extended

// 3 For OGL Buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

//for Texture
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils; //for texImage2D

public class GLESView extends GLSurfaceView
        implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener 
{
    //fields
    private GestureDetector gestureDetector;
    private final Context context;

    // java neither has uint / GLuint
    private int fragmentShaderObject;
    private int vertexShaderObject;
    private int shaderProgramObject;

    //new
    private int[] vao = new int[1];
    private int[] vbo_position = new int[1];
    private int[] vbo_texture = new int[1];
    private int[] Texture_Smiley = new int[2];

    //uniforms
    private int mvpUniform;
    private int uniform_texture0_sampler;

    private float[] PerspectiveProjectionMatrix = new float[16];

    // constructor
    public GLESView(Context drawingContext) 
    {
        super(drawingContext);
        context = drawingContext; //ghrc

        gestureDetector = new GestureDetector(drawingContext, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);

        // call 3 functions of GLES
        setEGLContextClientVersion(3); // 3.x
        // will give highest supported to 3, ie 3.2

        setRenderer(this); // onDrawFrame() = Display()
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // NOTE
    }


    // Handling Events
    //1
    @Override
    public boolean onTouchEvent(MotionEvent me) 
    {
        // keyboard actions
        int eventaction = me.getAction();

        if (!gestureDetector.onTouchEvent(me)) 
        {
            super.onTouchEvent(me);
        }

        return (true);
    }

    //2
    @Override
    public boolean onDoubleTap(MotionEvent me) 
    {
        return (true);
    }

    //3
    @Override
    public boolean onDoubleTapEvent(MotionEvent me) 
    {
       
        return (true);
    }

    //4
    @Override
    public boolean onSingleTapConfirmed(MotionEvent me) {
        return (true);
    }

    //5
    @Override
    public boolean onDown(MotionEvent me) 
    {
       
        return (true);
    }

    //6
    @Override
    public boolean onFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) 
    {
        // swipe
        return (true);
    }

    //7
    @Override
    public void onLongPress(MotionEvent me) 
    {
    
    }


    //8
    @Override
    public boolean onScroll(MotionEvent me1, MotionEvent me2, float distanceX, float distanceY) 
    {
        uninitialize();
        System.exit(0);
        return (true);
    }

    //9
    @Override
    public void onShowPress(MotionEvent me) {
    }

    //10
    @Override
    public boolean onSingleTapUp(MotionEvent me) 
    {
        return (true);
    }


    // 3 functions of GLSurfaceView.Renderer
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) 
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR: GL10.GL_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_VENDOR);
        System.out.println("RTR: GLES32.GL_VENDOR: " + version);

        version = gl.glGetString(GLES32.GL_RENDERER);
        System.out.println("RTR: GLES32.GL_RENDERER: " + version);

        Initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) 
    {
        Resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused) 
    {
        Display();
    }

    //4 Methods 
    
    //Texture Loading
    private int LoadTexture(int imageResourceID) 
    {
		int[] Texture  =  new int[1];

        //1
        BitmapFactory.Options options = new BitmapFactory.Options();

        //2
        options.inScaled = false;

		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, Texture[0]);

		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);

		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);


        //3 - ask Activity to find imageResourceID
        Bitmap bitmap =  BitmapFactory.decodeResource(context.getResources(),
									imageResourceID,
									options		
        );
        
        //4
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D,
            0, //mipmap
            bitmap,
            0);

		//5
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D); //new
		//6
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		return(Texture[0]);

    }

 
    private void Initialize() 
    {

		//call
		Texture_Smiley[0] = LoadTexture(R.raw.smiley); 

        //Shaders
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
        final String vertexShaderSourceCode = String.format(
                "#version 320 es" +
                "\n" +
                "in vec4 vPosition;" +
                "in vec2 vTexture0_Coord;" +
                "out vec2 out_Texture0_Coord;" +
                "uniform mat4 u_mvp_matrix;" +
                "void main(void)" +
                "{" +
                "gl_Position = u_mvp_matrix * vPosition;" +
                "out_Texture0_Coord = vTexture0_Coord;" +
                "}"
        );
        
        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        // Error checking
        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); 
        
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
         {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR2: Error in Vertex shader compilation log: " + szInfoLog);
            }
             else
             {
                System.out.println("RTR2:Error in Vertex Shader.");
             }
            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Vertex Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        final String fragmentShaderSourceCode = String.format(
                "#version 320 es" +
                "\n" +
                "precision highp float;"+
                "in vec2 out_Texture0_Coord;" +
                "out vec4 FragColor;"+
                "uniform highp sampler2D u_texture0_sampler;" +
                "void main(void)"+
                "{"+
                "FragColor = texture(u_texture0_sampler,out_Texture0_Coord);"+
                "}"
        );

        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
        GLES32.glCompileShader(fragmentShaderObject);

        // Error checking
        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // 1st
        
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE) 
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR2: Error in Fragment shader compilation log: " + szInfoLog);
            } 
            else 
            {
                System.out.println("RTR2:Error in Fragment Shader.");
            }
            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Fragment Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        shaderProgramObject = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Pre Link Attribute Binding
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_TEXCOORD, "vTexture0_Coord");

        GLES32.glLinkProgram(shaderProgramObject);  //Link

        // Error checking
        int[] iProgramLinkStatus = new int[1];

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if (iProgramLinkStatus[0] == GLES32.GL_FALSE) {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0) {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("RTR2: Error in Shader Program compilation log: " + szInfoLog);
            } 
            else 
            {
                System.out.println("RTR2: Error in Shader Program.");
            }

            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Shader Program compiled successfully.");
        }

        // Post Link Uniform Location
        mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        uniform_texture0_sampler = GLES32.glGetUniformLocation(shaderProgramObject, "u_texture0_sampler");

        // reset flags
        iProgramLinkStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        final float[] Vertices = new float[]
                {
                     1.0f,1.0f,0.0f,
                    -1.0f,1.0f,0.0f,
                    -1.0f,-1.0f,0.0f,
                     1.0f,-1.0f,0.0f 
                };

        
        GLES32.glGenVertexArrays(1, vao, 0);
        GLES32.glBindVertexArray(vao[0]);

        GLES32.glGenBuffers(1, vbo_position, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);

        // convert the array which passed to glBufferData() : as In C,C++ array name is iteself pointer

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(Vertices.length * 4); // float
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
        positionBuffer.put(Vertices);
        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, Vertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind

        //Vbo Texture
        final float[] TexCord = new float[]
        {
            1.0f,1.0f,
            0.0f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f
        };

        GLES32.glGenBuffers(1, vbo_texture, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_texture[0]);

        ByteBuffer byteBuffer_t = ByteBuffer.allocateDirect(TexCord.length * 4); // float
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer Buffer_YSR = byteBuffer.asFloatBuffer();
        Buffer_YSR.put(TexCord);
        Buffer_YSR.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, TexCord.length * 4, Buffer_YSR, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD, 2, GLES32.GL_FLOAT, false, 0, 0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind

        GLES32.glBindVertexArray(0); //Vao

        GLES32.glEnable(GLES32.GL_DEPTH_TEST); // Hidden Surface Removal
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        Matrix.setIdentityM(PerspectiveProjectionMatrix, 0);

        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    }


    
    private void Resize(int width, int height) 
    {
        if (height == 0) 
        {
            height = 1;
        }
        
        GLES32.glViewport(0, 0, width, height);
    
        Matrix.perspectiveM(PerspectiveProjectionMatrix, 0,
            45.0f, (float)width / (float)height, 0.1f, 100.0f);
        
    }

    // Scene
    private void Display()
     {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject); // Binding shaders to SPO
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];

        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -4.0f);
            
        //4 : Do Mat mul
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, PerspectiveProjectionMatrix, 0, modelViewMatrix, 0);
        
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
        GLES32.glUniform1i(uniform_texture0_sampler, 0);

        //7 : texture bind code
        GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, Texture_Smiley[0]);        
        
        //draw
        GLES32.glBindVertexArray(vao[0]);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);

        GLES32.glBindVertexArray(0);
        GLES32.glUseProgram(0);

        requestRender(); //  swapBuffers
    }


    private void uninitialize() 
    {
        if (vbo_position[0] != 0)
         {
            GLES32.glDeleteBuffers(1, vbo_position, 0);
            vbo_position[0] = 0;
        }
        
        if (vbo_texture[0] != 0)
         {
            GLES32.glDeleteBuffers(1, vbo_texture, 0);
            vbo_texture[0] = 0;
        }

        if (vao[0] != 0) 
        {
            GLES32.glDeleteVertexArrays(1, vao, 0);
            vao[0] = 0;
        }

        if (shaderProgramObject != 0) 
        {
            int[] shaderCount = new int[1];
            int shaderNo;

            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            int[] shaders = new int[shaderCount[0]];
        
            GLES32.glGetAttachedShaders(shaderProgramObject,
                    shaderCount[0],
                    shaderCount, 0,
                    shaders, 0);

            for (shaderNo = 0; shaderNo < shaderCount[0]; shaderNo++) 
            {
                GLES32.glDetachShader(shaderProgramObject, shaders[shaderNo]);
                GLES32.glDeleteShader(shaders[shaderNo]);
                shaders[shaderNo] = 0;
            }

            GLES32.glDeleteProgram(shaderProgramObject); // Not actually deleting but changing machine state. Not shaikh chilli
            shaderProgramObject = 0;
            GLES32.glUseProgram(0);
        }

    }

}
