package com.astromedicomp.win_hello;

//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.view.WindowManager;
import android.view.Window;
import android.view.Gravity;
import android.content.Context;
import android.view.View;
import android.graphics.Color;
import androidx.appcompat.widget.AppCompatTextView;

public class MyView extends AppCompatTextView
 {
    public MyView(Context drawContext)
    {
        super(drawContext);
        setTextColor(Color.rgb(0,255,0));
        setTextSize(60);
        setGravity(Gravity.CENTER);
        setText("Hello World");
    }
}
