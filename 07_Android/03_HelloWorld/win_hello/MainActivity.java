//own package
package com.astromedicomp.win_hello;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.Window;
import android.content.pm.ActivityInfo;
import android.content.Context;
import android.graphics.Color;
import android.view.View;

public class MainActivity extends AppCompatActivity  
{
    private 
        MyView myView; //Field

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        
        // make fullscreen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        

        // Landscape
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        
        //this.getWindow().getDecorView(setBackgroudColor(Color.BLACK));
        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        myView = new MyView(this);

        setContentView(myView);

    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

}
