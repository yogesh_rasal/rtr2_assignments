package com.astromedicomp.win_events_exit;

//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.view.WindowManager;
import android.view.Window;
import android.view.Gravity;
import android.content.Context;
import android.view.View;
import android.graphics.Color;
import androidx.appcompat.widget.AppCompatTextView;

//
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

public class MyView extends AppCompatTextView implements OnGestureListener,OnDoubleTapListener
 {
    private GestureDetector gestureDetector;

    public MyView(Context drawContext)
    {

        super(drawContext);
        setTextColor(Color.rgb(0,255,0));
        setTextSize(60);
        setGravity(Gravity.CENTER);
        setText("Hello World !!!");

        gestureDetector = new GestureDetector(drawContext,this,null,false);
        gestureDetector.setOnDoubleTapListener(this);

    }

    // 9 Methods  : Most imp
    @Override 
    public boolean onTouchEvent(MotionEvent event)
    {
        int eventaction = event.getAction();

        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event); // like defwndproc

        return(true);
    } 

    @Override 
    public boolean onDoubleTap(MotionEvent e)
    {
        setText("Double Tap");
        return(true);
    }

    @Override 
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return(true); //special
    }

    @Override 
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        setText("Single Tap");
        return(true);
    }

    // Gesture Listener
    @Override 
    public boolean onDown(MotionEvent e)
    {
        return(true); //already done in onSingleTapConfirmed
    }

    @Override
    public void onLongPress(MotionEvent e)
    {
        setText("Long Press");
    }

    @Override public boolean onScroll(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
       System.exit(0);
       return(true);
    }

    @Override 
    public boolean onFling(MotionEvent e1,MotionEvent e2,float distanceX,float distanceY)
    {

        return(true);
    }

    @Override 
    public void onShowPress(MotionEvent e)
    {

    }

    @Override public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }


}
