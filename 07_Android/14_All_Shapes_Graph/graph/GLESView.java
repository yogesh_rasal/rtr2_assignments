package com.astromedicomp.graph;

import android.content.Context;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;


import android.opengl.GLSurfaceView;
import android.opengl.GLES32; // opengl version 3.2

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig; //Extended

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;
import android.media.MediaPlayer;

public class GLESView extends GLSurfaceView
        implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener 
{
    private GestureDetector gestureDetector;
    private final Context context;

    private int fragmentShaderObject;
    private int vertexShaderObject;
    private int shaderProgramObject;

    //new
    private int[] vaoPyramid = new int[1];
    private int[] vaoCube = new int[1];

    private int[] vboPyramidPosition = new int[1];
    private int[] vboPyramidColor = new int[1];
    private int[] vboCubePosition = new int[1];
    private int[] vboCubeColor = new int[1];
    
    private int[] vao_Circle = new int[1];
    private int[] vbo_Position_Circle = new int[1];
    private int[] vbo_Color_Circle  = new int[1];
   // private int[]   = new int[1];
   

   //for Graph
    private int[] vao_XAxis = new int[1];
    private int[] vao_YAxis = new int[1];
    private int[] vbo_XPosition = new int[1];
    private int[] vbo_XColor= new int[1];
    private int[] vbo_YPosition = new int[1];
    private int[] vbo_YColor = new int[1];

    private int[]  vao_HLines = new int[1];
    private int[]  vao_Lines = new int[1];
    
    private int[] vbo_PositionHLines = new int[1];
    private int[] vbo_PositionVLines = new int[1];
    private int[] vbo_ColorHLines = new int[1];
    private int[] vbo_ColorVLines = new int[1];

    //angles for rotation
     static float angleTriangle= 0.0f;
     static float angleRectangle = 0.0f;
     static int NUM_POINTS = 1000;
     static int SLICES = 20;
    
    private int mvpUniform;
    private float[] PerspectiveProjectionMatrix = new float[16];

    //music
    private MediaPlayer mediaPlayer ;
    boolean anim = false;

    // constructor
    public GLESView(Context drawingContext) 
    {
        super(drawingContext);
        context = drawingContext; //ghrc

        gestureDetector = new GestureDetector(drawingContext, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);

        setEGLContextClientVersion(3); // 3.x

        setRenderer(this); // onDrawFrame() - display()

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // NOTE
        
        mediaPlayer =  MediaPlayer.create(context,R.raw.music); //imp
    }


    // Handling Events
    //1
    @Override
    public boolean onTouchEvent(MotionEvent me) 
    {
        // keyboard actions
        int eventaction = me.getAction();

        if (!gestureDetector.onTouchEvent(me)) 
        {
            super.onTouchEvent(me);
        }

        return (true);
    }

    //2
    @Override
    public boolean onDoubleTap(MotionEvent me) 
    {
        mediaPlayer.start();
        
        if(anim == false)
            anim = true;
        
         return (true);
    }

    //3
    @Override
    public boolean onDoubleTapEvent(MotionEvent me) 
    {
        return (true);
    }

    //4
    @Override
    public boolean onSingleTapConfirmed(MotionEvent me) {
        return (true);
    }

    //5
    @Override
    public boolean onDown(MotionEvent me) 
    {
       
        return (true);
    }

    //6
    @Override
    public boolean onFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) 
    {
        // swipe
        return (true);
    }

    //7
    @Override
    public void onLongPress(MotionEvent me) 
    {
        if(anim == true)
            anim = false;
        
        mediaPlayer.pause();
    }


    //8
    @Override
    public boolean onScroll(MotionEvent me1, MotionEvent me2, float distanceX, float distanceY) 
    {
        uninitialize();
        System.exit(0);
        return (true);
    }

    //9
    @Override
    public void onShowPress(MotionEvent me) {
    }

    //10
    @Override
    public boolean onSingleTapUp(MotionEvent me) 
    {
        return (true);
    }


    // 3 functions of GLSurfaceView.Renderer
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) 
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR: GL10.GL_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_VENDOR);
        System.out.println("RTR: GLES32.GL_VENDOR: " + version);

        version = gl.glGetString(GLES32.GL_RENDERER);
        System.out.println("RTR: GLES32.GL_RENDERER: " + version);

        Initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) 
    {
        Resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused) 
    {
        if(anim == true)
        {
            Update();
        }

        Display();
    }

    //3 Methods 
    private void Initialize() 
    {
        //Shaders
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
       
        final String vertexShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec4 vColor;"	+
            "out vec4 Out_Color;" +
            "uniform mat4 u_mvp_matrix;" +
            "void main(void)" +
            "{" +
            " gl_Position = u_mvp_matrix * vPosition;" +
            " Out_Color = vColor;" +
            "}"
        );
        
        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        // Error checking
        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); 
        
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
         {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR2: Error in Vertex shader compilation log: " + szInfoLog);
            }
             else
             {
                System.out.println("RTR2:Error in Vertex Shader.");
             }
            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Vertex Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        final String fragmentShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;" +
            "in vec4 Out_Color;" +
            "out vec4 FragColor;" +
            "void main(void)" +
            "{" +
            " FragColor = Out_Color;" +
            "}"
        );
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
        GLES32.glCompileShader(fragmentShaderObject);

        // Error checking
        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // 1st
        
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE) 
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR2: Error in Fragment shader compilation log: " + szInfoLog);
            } 
            else 
            {
                System.out.println("RTR2:Error in Fragment Shader.");
            }
            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Fragment Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        shaderProgramObject = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Pre Link Attribute Binding
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor");
        
        GLES32.glLinkProgram(shaderProgramObject);

        // Error checking
        int[] iProgramLinkStatus = new int[1];

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if (iProgramLinkStatus[0] == GLES32.GL_FALSE) 
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
        
            if (iInfoLogLength[0] > 0) 
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("RTR2: Error in Shader Program compilation log: " + szInfoLog);
            } 
            else 
            {
                System.out.println("RTR2: Error in Shader Program.");
            }

            uninitialize();
            System.exit(0);
        } 
        else 
        {
            System.out.println("RTR2: Shader Program compiled successfully.");
        }

        // Post Link Uniform Location
        mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

        // reset flags
        iProgramLinkStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        //Pyramid
        final float[] PyramidVertices = new float[]
        {
            0.0f,1.0f,0.0f,
            -1.0f,-1.0f,0.0f,
            1.0f,-1.0f,0.0f,
            0.0f,1.0f,0.0f,
            1.0f,-1.0f,0.0f,
           -1.0f,-1.0f,0.0f
        };

        // Generation
    GLES32.glGenVertexArrays(1, vaoPyramid, 0);
    GLES32.glBindVertexArray(vaoPyramid[0]);

    GLES32.glGenBuffers(1, vboPyramidPosition, 0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPyramidPosition[0]);

    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(PyramidVertices.length * 4); // float
    byteBuffer.order(ByteOrder.nativeOrder());
    
    FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
    positionBuffer.put(PyramidVertices);
    positionBuffer.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, PyramidVertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

    GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
    
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind

    //Color
    final float[] PyramidColors = new float[]
        {
            0.0f, 1.0f, 0.5f,
            0.0f, 1.0f, 0.5f,
            0.0f, 1.0f, 0.5f,
            0.0f, 1.0f, 0.5f,
            0.0f, 1.0f, 0.5f,
            0.0f, 1.0f, 0.5f 
        };


    //color
    GLES32.glGenBuffers(1, vboPyramidColor, 0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPyramidColor[0]);

    ByteBuffer byteBufferC = ByteBuffer.allocateDirect(PyramidVertices.length * 4); // float
    byteBufferC.order(ByteOrder.nativeOrder());
    
    FloatBuffer colorBuffer = byteBufferC.asFloatBuffer();
    colorBuffer.put(PyramidColors);
    colorBuffer.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, PyramidColors.length*4, colorBuffer, GLES32.GL_STATIC_DRAW);
    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);

    GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind

    GLES32.glBindVertexArray(0);


//Circle
	float fPerimeter;
	float fRadius;

	float CircleVertices[] = new float[NUM_POINTS*3];
	float CircleColor[] = new float[NUM_POINTS*3];

	//Code
	fPerimeter = (float)Math.sqrt(((1.0f)*(1.0f)) + ((1.0f)*(1.0f))); 
	fRadius = fPerimeter / 2;


	for (int i = 0, j = 0 ; i < NUM_POINTS; i++, j=j+3)
	{
        float angle = (float)(2 * Math.PI * i) / NUM_POINTS; //2 Pie R
        
        CircleVertices[j] = (float)Math.cos(angle) * fRadius; 
		CircleVertices[j+1] = (float)Math.sin(angle) * fRadius;
		CircleVertices[j+2] = 0.0f;

		//yellow
	    CircleColor[j+0] = 1.0f;
		CircleColor[j+1] = 1.0f;
		CircleColor[j+2] = 0.0f;
    
    }

	// Create vao
    GLES32.glGenVertexArrays(1, vao_Circle, 0);
    GLES32.glBindVertexArray(vao_Circle[0]);
    GLES32.glGenBuffers(1, vbo_Position_Circle, 0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Position_Circle[0]);

    ByteBuffer byteBuffer_YSR = ByteBuffer.allocateDirect(CircleVertices.length * 4); // 4 : sizeof float
    byteBuffer_YSR.order(ByteOrder.nativeOrder());

    FloatBuffer positionBuffer_YSR = byteBuffer_YSR.asFloatBuffer();
    positionBuffer_YSR.put(CircleVertices); // Exception
    positionBuffer_YSR.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, NUM_POINTS * 3 * 4, positionBuffer_YSR, GLES32.GL_STATIC_DRAW);
    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
    GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind

	// Bind vboColor
    GLES32.glGenBuffers(1, vbo_Color_Circle, 0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Color_Circle[0]);
    ByteBuffer byteBufferColor_YSR = ByteBuffer.allocateDirect(CircleVertices.length * 4); // 4 : sizeof float
    byteBufferColor_YSR.order(ByteOrder.nativeOrder());

    FloatBuffer colorBuffer_YSR = byteBuffer_YSR.asFloatBuffer();
    colorBuffer_YSR.put(CircleColor);
    colorBuffer_YSR.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, NUM_POINTS * 3 * 4, colorBuffer_YSR, GLES32.GL_STATIC_DRAW);
    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
    GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind

	// Unbind
	GLES32.glBindVertexArray(0);

    
    /********* Cube **********/
    final float[] CubeVertices = new float[]
    {
        1.0f,1.0f,0.0f,
		-1.0f,1.0f,0.0f,
		-1.0f,1.0f,0.0f,
		-1.0f,-1.0f,0.0f,
		1.0f,-1.0f,0.0f,
		-1.0f,-1.0f,0.0f,
		1.0f,-1.0f,0.0f,
		1.0f,1.0f,0.0f
    };  

    GLES32.glGenVertexArrays(1, vaoCube, 0);
    GLES32.glBindVertexArray(vaoCube[0]);
    GLES32.glGenBuffers(1, vboCubePosition, 0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboCubePosition[0]);
  
    ByteBuffer byteBufferCube = ByteBuffer.allocateDirect(CubeVertices.length * 4);// 4 : sizeof float
    byteBufferCube.order(ByteOrder.nativeOrder());

    FloatBuffer positionBufferCube = byteBufferCube.asFloatBuffer();
    positionBufferCube.put(CubeVertices);
    positionBufferCube.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, CubeVertices.length * 4, positionBufferCube, GLES32.GL_STATIC_DRAW);
    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
    GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind


    //color
    final float[] CubeColors = new float[]
    {
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f
    };

    //Color
    GLES32.glGenBuffers(1, vboCubeColor, 0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboCubeColor[0]);

    ByteBuffer byteBufferCubecolor = ByteBuffer.allocateDirect(CubeColors.length * 4);// 4 : sizeof float
    byteBufferCubecolor.order(ByteOrder.nativeOrder());

    FloatBuffer colorBufferCube = byteBufferCubecolor.asFloatBuffer();
    colorBufferCube.put(CubeColors);
    colorBufferCube.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, CubeColors.length * 4, colorBufferCube, GLES32.GL_STATIC_DRAW);
    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);

    GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind

    GLES32.glBindVertexArray(0);

  //Graph  
    //Axes
    float xAxis[] = new float[]
                       { 1.0f, 0.0f, 0.0f, 
					   -1.0f, 0.0f, 0.0f };

    float AxisColor[] = new float[]
                        { 1.0f,0.5f,1.0f,
						 1.0f,0.5f,1.0f };
	
    float YAxis[] = new float[]
                        { 0.0f, 1.0f, 0.0f,
						0.0f, -1.0f, 0.0f };

	// Create vao
	GLES32.glGenVertexArrays(1, vao_XAxis,0);
	GLES32.glBindVertexArray(vao_XAxis[0]);

	GLES32.glGenBuffers(1, vbo_XPosition,0);
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_XPosition[0]);

    ByteBuffer byteBufferX = ByteBuffer.allocateDirect(xAxis.length * 4);// 4 : sizeof float
    byteBufferX.order(ByteOrder.nativeOrder());

    FloatBuffer BufferX = byteBufferX.asFloatBuffer();
    BufferX.put(xAxis);
    BufferX.position(0);

  
	GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 4 * 6, BufferX , GLES32.GL_STATIC_DRAW);

	GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
    
    GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

    // Unbind 
    BufferX.clear();
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

	// Bind color
	GLES32.glGenBuffers(1, vbo_XColor,0);
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_XColor[0]);
                             
    ByteBuffer byteBufferXc = ByteBuffer.allocateDirect(AxisColor.length * 4);// 4 : sizeof float
    byteBufferXc.order(ByteOrder.nativeOrder());

    FloatBuffer colorBufferX = byteBufferXc.asFloatBuffer();
    colorBufferX.put(AxisColor);
    colorBufferX.position(0);

	GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 4*6 , colorBufferX, GLES32.GL_STATIC_DRAW);

	GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);

	GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

	// Unbind color
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

	// Unbind vao
    colorBufferX.clear();
    GLES32.glBindVertexArray(0);

 //Y-Axis
	
	// Create vao
	
	GLES32.glGenVertexArrays(1, vao_YAxis,0);
	GLES32.glBindVertexArray(vao_YAxis[0]);

	GLES32.glGenBuffers(1, vbo_YPosition,0);
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_YPosition[0]);

    ByteBuffer byteBufferY = ByteBuffer.allocateDirect(YAxis.length * 4);// 4 : sizeof float
    byteBufferY.order(ByteOrder.nativeOrder());

    FloatBuffer BufferY = byteBufferY.asFloatBuffer();
    BufferY.put(YAxis);
    BufferY.position(0);
    
    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 4*YAxis.length, BufferY , GLES32.GL_STATIC_DRAW);

	GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

	GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

    // Unbind 
    BufferY.clear();
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

// Bind color
	GLES32.glGenBuffers(1, vbo_YColor,0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_YColor[0]);
    
    ByteBuffer byteBufferYc = ByteBuffer.allocateDirect(AxisColor.length * 4);// 4 : sizeof float
    byteBufferYc.order(ByteOrder.nativeOrder());

    FloatBuffer colorBufferYc = byteBufferYc.asFloatBuffer();
    colorBufferYc.put(AxisColor);
    colorBufferYc.position(0);

	GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 24 , colorBufferYc, GLES32.GL_STATIC_DRAW);

	GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);

	GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

    // Unbind color
    //colorBufferX.clear();
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

	// Unbind vao
	GLES32.glBindVertexArray(0);

    //Graph
    ShowGraph();

    // Common
    Matrix.setIdentityM(PerspectiveProjectionMatrix, 0);

    GLES32.glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
}

private void ShowGraph()
{
    //Graph Lines 
    float[] hLinesVertices = new float[SLICES * 16];
    float[] hLinesColor = new float[SLICES * 16];

    //Vert
    float[] vLinesVertices = new float[SLICES * 16];
    float[] vLinesColor = new float[SLICES * 16];

    float fGraphLines = 0.0f;
    float fPerQuad = 1.0f / 16;

    int totalVertices = SLICES * 12;

    //like FFP
    fGraphLines = fPerQuad;

    //Quadrants
    for (int i = 0; i < totalVertices; i++)
        {
            if (fGraphLines <= 1.01f)
            {
            //1
                hLinesVertices[i * 3 + 0] = 1.0f;
                hLinesVertices[i * 3 + 1] = fGraphLines;
                hLinesVertices[i * 3 + 2] = 0.0f;
    
                hLinesColor[i * 3 + 0] = 0.0f;
                hLinesColor[i * 3 + 1] = 1.0f;
                hLinesColor[i * 3 + 2] = 0.0f;
    
                //vert
    
                vLinesVertices[i * 3 + 0] = fGraphLines;
                vLinesVertices[i * 3 + 1] = 1.0f;
                vLinesVertices[i * 3 + 2] = 0.0f;
    
                vLinesColor[i * 3 + 0] = 0.0f;
                vLinesColor[i * 3 + 1] = 1.0f;
                vLinesColor[i * 3 + 2] = 0.0f;
    
                i++;
            
            //2
    
                hLinesVertices[i * 3 + 0] = -1.0f;
                hLinesVertices[i * 3 + 1] = fGraphLines;
                hLinesVertices[i * 3 + 2] = 0.0f;
    
                hLinesColor[i * 3 + 0] = 0.0f;
                hLinesColor[i * 3 + 1] = 1.0f;
                hLinesColor[i * 3 + 2] = 0.0f;
    
                //vert
                vLinesVertices[i * 3 + 0] = fGraphLines;
                vLinesVertices[i * 3 + 1] = -1.0f;
                vLinesVertices[i * 3 + 2] = 0.0f;
    
                vLinesColor[i * 3 + 0] = 0.0f;
                vLinesColor[i * 3 + 1] = 1.0f;
                vLinesColor[i * 3 + 2] = 0.0f;
    
                i++;
        
            //3
    
                hLinesVertices[i * 3 + 0] = 1.0f;
                hLinesVertices[i * 3 + 1] = -fGraphLines;
                hLinesVertices[i * 3 + 2] = 0.0f;
    
                hLinesColor[i * 3 + 0] = 0.0f;
                hLinesColor[i * 3 + 1] = 1.0f;
                hLinesColor[i * 3 + 2] = 0.0f;
    
                //vert
                vLinesVertices[i * 3 + 0] = -fGraphLines;
                vLinesVertices[i * 3 + 1] = 1.0f;
                vLinesVertices[i * 3 + 2] = 0.0f;
    
                vLinesColor[i * 3 + 0] = 0.0f;
                vLinesColor[i * 3 + 1] = 1.0f;
                vLinesColor[i * 3 + 2] = 0.0f;
    
                i++;
    
            //4
                
                hLinesVertices[i * 3 + 0] = -1.0f;
                hLinesVertices[i * 3 + 1] = -fGraphLines;
                hLinesVertices[i * 3 + 2] = 0.0f;
    
                hLinesColor[i * 3 + 0] = 0.0f;
                hLinesColor[i * 3 + 1] = 1.0f;
                hLinesColor[i * 3 + 2] = 0.0f;
    
                //vert
                vLinesVertices[i * 3 + 0] = -fGraphLines;
                vLinesVertices[i * 3 + 1] = -1.0f;
                vLinesVertices[i * 3 + 2] = 0.0f;
    
                vLinesColor[i * 3 + 0] = 0.0f;
                vLinesColor[i * 3 + 1] = 1.0f;
                vLinesColor[i * 3 + 2] = 0.0f;
            }
    
          fGraphLines = fGraphLines + fPerQuad ; //imp
        }

         //Lines
	// Create vao
	GLES32.glGenVertexArrays(1, vao_HLines,0);
	GLES32.glBindVertexArray(vao_HLines[0]);

	GLES32.glGenBuffers(1, vbo_PositionHLines,0);
    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_PositionHLines[0]);
    
    ByteBuffer byteBufferG = ByteBuffer.allocateDirect(hLinesVertices.length * 4); // 4 : sizeof float
    byteBufferG.order(ByteOrder.nativeOrder());

    FloatBuffer FBuffer = byteBufferG.asFloatBuffer();
    FBuffer.put(hLinesVertices);
    FBuffer.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, hLinesVertices.length, FBuffer, GLES32.GL_STATIC_DRAW);

    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

	GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

	// Unbind 
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

// Bind color
    GLES32.glGenBuffers(1, vbo_ColorHLines,0);
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_ColorHLines[0]);

     byteBufferG = ByteBuffer.allocateDirect(hLinesColor.length * 4); // 4 : sizeof float
    byteBufferG.order(ByteOrder.nativeOrder());

     FBuffer = byteBufferG.asFloatBuffer();
    FBuffer.put(hLinesColor);
    FBuffer.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, hLinesColor.length, FBuffer, GLES32.GL_STATIC_DRAW);

    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);

	GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

	// Unbind color
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

	// Unbind vao
	GLES32.glBindVertexArray(0);

	
//Vertical

	// Create Lines vao
	GLES32.glGenVertexArrays(1, vao_Lines,0);
	GLES32.glBindVertexArray(vao_Lines[0]);

	GLES32.glGenBuffers(1, vbo_PositionVLines,0);
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_PositionVLines[0]);

    byteBufferG = ByteBuffer.allocateDirect(vLinesVertices.length * 4); // 4 : sizeof float
    byteBufferG.order(ByteOrder.nativeOrder());

     FBuffer = byteBufferG.asFloatBuffer();
    FBuffer.put(vLinesVertices);
    FBuffer.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, vLinesVertices.length, FBuffer, GLES32.GL_STATIC_DRAW);

    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

	GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

	// Unbind 
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

	// Bind color
	GLES32.glGenBuffers(1, vbo_ColorVLines,0);
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_ColorVLines[0]);

    byteBufferG = ByteBuffer.allocateDirect(vLinesColor.length * 4); // 4 : sizeof float
    byteBufferG.order(ByteOrder.nativeOrder());

     FBuffer = byteBufferG.asFloatBuffer();
    FBuffer.put(vLinesColor);
    FBuffer.position(0);

    GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, vLinesColor.length, FBuffer, GLES32.GL_STATIC_DRAW);

    GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);

	GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

	// Unbind 
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

	// Unbind vao Lines
	GLES32.glBindVertexArray(0);

}

 private void Resize(int width, int height) 
    {
        if (height == 0) 
        {
            height = 1;
        }
        
        GLES32.glViewport(0, 0, width, height);
    
        Matrix.perspectiveM(PerspectiveProjectionMatrix, 0,
            45.0f, (float)width / (float)height, 0.1f, 100.0f);
        
    }

 // Scene
  private void Display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT); // | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject); // Binding shaders to SPO

        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];
        float[] rotationMatrix = new float[16]; //new

        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

    //graph
        //1
        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -1.2f);
        
        //2
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, PerspectiveProjectionMatrix, 0, modelViewMatrix, 0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        GLES32.glBindVertexArray(vao_XAxis[0]);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2); // most imp

        GLES32.glBindVertexArray(0);

    //Y-Axis
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        //1
        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -1.2f);
        
        //2
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, PerspectiveProjectionMatrix, 0, modelViewMatrix, 0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        GLES32.glBindVertexArray(vao_YAxis[0]);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2); // most imp

        GLES32.glBindVertexArray(0);

    // Hz Lines
    
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        //1
        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -0.58f);
        
        //2
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, PerspectiveProjectionMatrix, 0, modelViewMatrix, 0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        GLES32.glBindVertexArray(vao_HLines[0]);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, SLICES*4); // most imp

        GLES32.glBindVertexArray(0);

    // Vertical
        
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        //1
        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -0.58f);
        
        //2
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, PerspectiveProjectionMatrix, 0, modelViewMatrix, 0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        GLES32.glBindVertexArray(vao_Lines[0]);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, SLICES*4); // most imp

        GLES32.glBindVertexArray(0);

    //Triangle
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);
        
        //1
        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -5.1f);
        //2
        Matrix.rotateM(rotationMatrix, 0, angleTriangle, 1.0f, 0.0f, 0.0f);

        Matrix.multiplyMM(modelViewMatrix,0, modelViewMatrix,0, rotationMatrix,0); //Rotate
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, PerspectiveProjectionMatrix, 0, modelViewMatrix, 0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        GLES32.glBindVertexArray(vaoPyramid[0]);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 6); // most imp

        GLES32.glBindVertexArray(0);

    //Rectangle
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -5.0f);
        
        Matrix.rotateM(rotationMatrix, 0, angleRectangle, 0.0f, 1.0f, 0.0f);

        Matrix.multiplyMM(modelViewMatrix,0, modelViewMatrix,0, rotationMatrix,0);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, PerspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        //Drawing
        GLES32.glBindVertexArray(vaoCube[0]);
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
        GLES32.glDrawArrays(GLES32.GL_LINES, 2, 2);
        GLES32.glDrawArrays(GLES32.GL_LINES, 4, 2);
        GLES32.glDrawArrays(GLES32.GL_LINES, 6, 2);
    
        GLES32.glBindVertexArray(0);


    //Circle
        //1
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -2.5f);
       
        //2
        Matrix.rotateM(rotationMatrix, 0, angleTriangle*4, 1.0f, 1.0f, 1.0f);

        Matrix.multiplyMM(modelViewMatrix,0, modelViewMatrix,0, rotationMatrix,0); //Rotate
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, PerspectiveProjectionMatrix, 0, modelViewMatrix, 0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        GLES32.glBindVertexArray(vao_Circle[0]);
        GLES32.glDrawArrays(GLES32.GL_LINE_LOOP, 0, NUM_POINTS); // most imp

        GLES32.glBindVertexArray(0);


        //common
        GLES32.glUseProgram(0);

        requestRender(); //  swapBuffers
    }

   //new
  private void Update()
    {
        angleTriangle = angleTriangle + 1.5f;
        
        if(angleTriangle >= 360.0f)
        {
            angleTriangle = 0.0f;
        }

        angleRectangle = angleRectangle + 1.5f;
        
        if(angleRectangle >= 360.0f)
        {
            angleRectangle = 0.0f;
        }

    }
    

    private void uninitialize() 
    {
        //Vao & Vbo
        if (vboPyramidPosition[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboPyramidPosition, 0);
            vboPyramidPosition[0] = 0;
        }

        if (vboPyramidColor[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboPyramidColor, 0);
            vboPyramidColor[0] = 0;
        }

        if (vboCubePosition[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboCubePosition, 0);
            vboCubePosition[0] = 0;
        }

        if (vboCubeColor[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboCubeColor, 0);
            vboCubeColor[0] = 0;
        }

        if (vaoPyramid[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vaoPyramid, 0);
            vaoPyramid[0] = 0;
        }

        //SPO
        if (shaderProgramObject != 0) 
        {
            int[] shaderCount = new int[1];
            int shaderNo;

            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            int[] shaders = new int[shaderCount[0]];
        
            GLES32.glGetAttachedShaders(shaderProgramObject,
                    shaderCount[0],
                    shaderCount, 0,
                    shaders, 0);

            for (shaderNo = 0; shaderNo < shaderCount[0]; shaderNo++) 
            {
                GLES32.glDetachShader(shaderProgramObject, shaders[shaderNo]);
                GLES32.glDeleteShader(shaders[shaderNo]);
                shaders[shaderNo] = 0;
            }

            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
            GLES32.glUseProgram(0);
        }

    }

}
