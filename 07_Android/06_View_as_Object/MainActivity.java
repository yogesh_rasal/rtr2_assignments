//own package
package com.astromedicomp.view_as_object;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.Window;
import android.content.pm.ActivityInfo;
import android.content.Context;
import android.graphics.Color;
import android.view.View;

//new
import androidx.appcompat.widget.AppCompatTextView;
import android.view.Gravity;

public class MainActivity extends AppCompatActivity //AppCompatTextView 
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        
        // make fullscreen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        

        // Landscape
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        
        //this.getWindow().getDecorView(setBackgroudColor(Color.BLACK));
        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        // View as object
        AppCompatTextView myView = new AppCompatTextView(this);

        myView.setTextColor(Color.rgb(0,255,0));
        myView.setTextSize(60);
        myView.setGravity(Gravity.CENTER);
        myView.setText("Hello World !!!");

        setContentView(myView);

    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

}
