#include <iostream>
#include <stdio.h>
#include <assert.h>
#include <string.h>

using namespace std;

#define SUCCESS 1

//put in header

struct Vec_int 
{
    int *P;
    int size;
};
 
 struct Vec_int* Create_vec_int(void);

 int Push_back_vec_int(struct Vec_int* , int);

 void Show_vec_int(struct Vec_int*);

 void Destroy_vec_int(struct Vec_int*); 


// ************ Client  *************
 
int main(void)
{
    struct Vec_int* P_vec_int = NULL;
    
    P_vec_int = Create_vec_int(); //constr

    for(int i =0 ; i < 10; i++)
    {
        Push_back_vec_int(P_vec_int,(i+1)*10); //insert
    }

    Show_vec_int(P_vec_int); //Show

    Destroy_vec_int(P_vec_int);

    P_vec_int = NULL;

    return 0;
}


// ***** Server *******
  //1
 struct Vec_int* Create_vec_int(void)
 {
     struct Vec_int* P_new = NULL;

     P_new = (struct Vec_int *)malloc(sizeof(struct Vec_int));
     assert(P_new);
     memset(P_new , 0, sizeof(struct Vec_int));
     
     return(P_new);

 }

//2
 int Push_back_vec_int(struct Vec_int* p_vec, int new_data)
 {
     p_vec->P = (int*)realloc(p_vec->P , (p_vec->size+1)*sizeof(int));

     assert(p_vec->P);

     p_vec->size = (p_vec->size)+1;

     p_vec->P[p_vec->size -1] = new_data;

    return(SUCCESS); 
 }


//3
 void Show_vec_int(struct Vec_int* p_vec)
 {
     for(int i=0; i < (p_vec->size); i++)
     {
         printf("P_Vec->P[%d] : %d \n",i,p_vec->P[i]);
     }
 }

//4
 void Destroy_vec_int(struct Vec_int *p_vec)
 {
     free(p_vec->P);
     free(p_vec);
 }

