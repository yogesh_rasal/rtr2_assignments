#include <iostream>
#include <stdio.h>
#include <assert.h>
#include <string.h>

using namespace std;

#define SUCCESS 1

//put in header

struct Vec_float 
{
    float *P;
    int size;
};
 
 struct Vec_float* Create_Vec_float(void);

 int Push_back_Vec_float(struct Vec_float* , float);

 void Show_Vec_float(struct Vec_float*);

 void Destroy_Vec_float(struct Vec_float*); 


// ************ Client  *************
 
int main(void)
{
    struct Vec_float* P_Vec_float = NULL;
    
    P_Vec_float = Create_Vec_float(); //constr

    for(int i =0 ; i < 10; i++)
    {
        Push_back_Vec_float(P_Vec_float,(i+1)*10.0f); //insert
    }

    Show_Vec_float(P_Vec_float); //Show

    Destroy_Vec_float(P_Vec_float); //Dest

    P_Vec_float = NULL;

    return 0;
}


// ***** Server *******
  //1
 struct Vec_float* Create_Vec_float(void)
 {
     struct Vec_float* P_new = NULL;

     P_new = (struct Vec_float *)malloc(sizeof(struct Vec_float));
     assert(P_new);
     memset(P_new , 0, sizeof(struct Vec_float));
     
     return(P_new);

 }

//2
 int Push_back_Vec_float(struct Vec_float* p_vec, float new_data)
 {
     p_vec->P = (float*)realloc(p_vec->P , (p_vec->size+1)*sizeof(float));

     assert(p_vec->P);

     p_vec->size = (p_vec->size)+1;

     p_vec->P[p_vec->size -1] = new_data;

    return(SUCCESS); 
 }


//3
 void Show_Vec_float(struct Vec_float* p_vec)
 {
     for(int i=0; i < (p_vec->size); i++)
     {
         printf("P_Vec->P[%d] : %f \n",i,p_vec->P[i]);
     }
 }

//4
 void Destroy_Vec_float(struct Vec_float *p_vec)
 {
     free(p_vec->P);
     free(p_vec);
 }

