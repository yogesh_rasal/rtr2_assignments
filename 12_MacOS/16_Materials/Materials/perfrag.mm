
/* 
   Sphere using Shaders
*/

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
#import "YSR_Sphere.h"

using namespace vmath;


// 'C' style global function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags , CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;

// interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end


// Entry point 
int main(int argc, const char *argv[])
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate :[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];
	
	return(0);
}


// interface
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	/* 
		Log file : 5 steps
		
	*/
	
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];

	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];

	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

	gpFile = fopen(pszLogFileNameWithPath, "w");

	if(gpFile == NULL)
	{
		printf("Can Not Create Log File.\n");
		[self release];
		[NSApp terminate:self];
	}
	
	fprintf(gpFile, "Program Started Successfully \n");

	/* create window */
	
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	window = [[NSWindow alloc]initWithContentRect:win_rect 
		styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | 
		NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
		backing:NSBackingStoreBuffered defer:NO];
	
	[window setTitle:@"macOS : Materials by YSR"];
	
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}


- (void)applicationWillTerminate:(NSNotification *)notification
{
	/* code */
	fprintf(gpFile, "Program is Terminated Successfully\n");

	if (gpFile)
	{
		/* code */
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];

	[window release];

	[super dealloc];
}
@end


@implementation GLView
{
@private
	CVDisplayLinkRef displaylink;

	// for Shaders
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

    GLuint gVao_sphere;
    GLuint gVbo_sphere_position;
    GLuint gVbo_sphere_normal;
    GLuint gVbo_sphere_element;
    
    //12 Uniforms
    GLuint Model_Uniform;
    GLuint View_Uniform;
    GLuint Projection_Uniform;
    
    GLuint LD_Uniform, LA_Uniform, LS_Uniform;
    GLuint KD_Uniform, KA_Uniform, KS_Uniform;
    
    GLuint u_Material_Shininess;
    GLuint Light_Position_Uniform;
    GLuint LisPressed_Uniform;
    
    float Angle;
    
    bool XPressed,YPressed , ZPressed;
    int winWidth;
    int winHeight;

    bool bisLPressed;
    
    bool bLighting;

    int gNumVertices , gNumElements;
    
    GLuint mvpUniform;
	mat4 PerspectProjectionMatrix;
}


-(id)initWithFrame:(NSRect)frame;
{
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFAAccelerated,
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		if (pixelFormat == nil)
		{
			fprintf(gpFile, "No valid OpenGL Pixel Format is Available.");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext];
	}
	return(self);
}

//imp
-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return(kCVReturnSuccess);
}


//Init
- (void)prepareOpenGL
{
	fprintf(gpFile, "OpenGL Version	:	%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version 	:	%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	// VERTEX SHADER
	
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
        "#version 410 core" \
        "\n" \
		"in vec4 vPosition;"						\
		"in vec3 vNormal;"							\
		"out vec3 T_Norm;"							\
		"out vec3 Light_Direction;"					\
		"out vec3 Viewer_Vector;"					\
		"\n"										\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform int u_LKeyPressed;"				\
		"uniform vec4 u_Light_Position;"			\
		"\n"										\
		"void main(void)"							\
		"{"											\
		"if(u_LKeyPressed == 1)"					\
		"{"											\
		"vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"	\
		"T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"			\
		"Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"	\
		"Viewer_Vector = vec3(-Eye_Coordinates.xyz);"\
		"}"		\
		"gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"	\
		"}" ;
    
    

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	
	// Compile
	glCompileShader(gVertexShaderObject);


	// Error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
				exit(0);
			}
		}
	}


	// FRAGMENT SHADER
	
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	
	const GLchar *fragmentShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "precision highp float;"
		"in vec3 T_Norm;"			\
		"in vec3 Light_Direction;"	\
		"in vec3 Viewer_Vector;"	\
		"out vec4 FragColor;"		\
		"\n"						\
		"uniform vec3 u_LA;"						\
		"uniform vec3 u_LD;"						\
		"uniform vec3 u_LS;"						\
		"uniform vec3 u_KA;"						\
		"uniform vec3 u_KD;"						\
		"uniform vec3 u_KS;"						\
		"uniform float u_Shininess;"				\
		"uniform int u_LKeyPressed;"				\
		"uniform vec4 u_Light_Position;"			\
		"\n"										\
		"void main(void)"		\
		"{"						\
		"if(u_LKeyPressed == 1)"\
		"{"						\
		"vec3 Normalized_View_Vector = normalize(Viewer_Vector);"	\
		"vec3 Normalized_Light_Direction = normalize(Light_Direction);"\
		"vec3 Normalized_TNorm = normalize(T_Norm);"\
		"float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0);"	\
		"vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm);"\
		"vec3 Ambient = vec3(u_LA * u_KA);"	\
		"vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);"	\
		"vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess));" \
		"vec3 Phong_ADS_Light = Ambient + Diffuse+ Specular;"	\
		"FragColor = vec4(Phong_ADS_Light,1.0);" \
		"}"							\
		"else"						\
		"{"							\
		"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		"}"							\
		"}" ;

    
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	
	// Compile
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);


	// Prelinking
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);

				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);

				free(szInfoLog);

				[self release];

				[NSApp terminate:self];

				exit(0);
			}
		}
	}


	// Post Linking
    
    mvpUniform = glGetUniformLocation(gShaderProgramObject,"u_model_matrix");

    View_Uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
    
    Projection_Uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
    
    KD_Uniform = glGetUniformLocation(gShaderProgramObject, "u_KD");
        
    LD_Uniform = glGetUniformLocation(gShaderProgramObject, "u_LD");
    
    Light_Position_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Light_Position");

    LisPressed_Uniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");


	LA_Uniform = glGetUniformLocation(gShaderProgramObject, "u_LA");
	KA_Uniform = glGetUniformLocation(gShaderProgramObject, "u_KA");

	LS_Uniform = glGetUniformLocation(gShaderProgramObject, "u_LS");
	KS_Uniform = glGetUniformLocation(gShaderProgramObject, "u_KS");

	u_Material_Shininess = glGetUniformLocation(gShaderProgramObject, "u_Shininess");
	

    //call
    makeSphere(2.0,30,30);

	//Depth
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.1f, 0.0f, 0.0f, 1.0f);

	PerspectProjectionMatrix = mat4::identity();

	//new
	CVDisplayLinkCreateWithActiveCGDisplays(&displaylink);
	CVDisplayLinkSetOutputCallback(displaylink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displaylink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displaylink);
	
    [super prepareOpenGL];
}

//Resize
- (void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat Width = rect.size.width;
	GLfloat Height = rect.size.height;
    
    winWidth = Width;
    winHeight = Height;

	if (Height == 0)
		Height = 1;

	glViewport(0, 0,(GLsizei)Width,(GLsizei)Height);

    PerspectProjectionMatrix = vmath::perspective(45.0f,Width/Height,0.1f,100.0f);
    
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

- (void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
}

//Display
- (void)drawView
{
	[[self openGLContext]makeCurrentContext];
	
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
    
    
    mat4 Model_Matrix;
    mat4 View_Matrix;
    mat4 Projection_Matrix;
    mat4 Translation_Matrix;
    mat4 Rotation_Matrix;
    mat4 ModelView_Projection_Matrix;
	
	//2 : I[]
	Model_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	View_Matrix = mat4::identity();
	Rotation_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//4 : Do Mat mul
	Translation_Matrix = translate(0.0f, 0.0f, -7.0f);

	Model_Matrix = Translation_Matrix * Rotation_Matrix;


	// 5	: send it to shader

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, Model_Matrix);
    
	glUniformMatrix4fv(Projection_Uniform, 1, GL_FALSE, PerspectProjectionMatrix);
    
	glUniformMatrix4fv(View_Uniform, 1, GL_FALSE, View_Matrix);
		
    //imp
    if (bLighting == true)
    {
        glUniform1i(LisPressed_Uniform, 1);
    
        glUniform3f(LD_Uniform, 1.0f, 1.0f, 1.0f);

        glUniform3f(LS_Uniform, 1.0f, 1.0f, 1.0f);
        
        glUniform3f(LA_Uniform, 0.25f, 0.25f, 0.25f);
        
    //imp : out to in Light

        if(XPressed)
        glUniform4f(Light_Position_Uniform, 0.0f,sinf(Angle)*8 ,cosf(Angle)*8 , 1.0f);

        if(YPressed)
        glUniform4f(Light_Position_Uniform,sinf(Angle)*8 , 0.0f,cosf(Angle)*8 , 1.0f);

        if(ZPressed)
        glUniform4f(Light_Position_Uniform,sinf(Angle)*8 ,cosf(Angle)*8 , 0.0f, 1.0f);
    }

	else
	{
		glUniform1i(LisPressed_Uniform, 0);
	}

    Angle += 0.08f;
    
        if (Angle >= 360.0f)
            Angle -= 359.0f;
    
    
    //24 Sph

        GLfloat  Material_Ambient[72]; //
        GLfloat  Material_Diffuse[72]; // {0.0215f,0.1745f,0.0215f,1.0f};
        GLfloat  Material_Specular[72]; //  {1.0f,1.0f,1.0f,1.0f};
        GLfloat  Material_Shininess[24]; // {128.0f};
        

        //24 Spheres
    // 1 : Emerald
        Material_Ambient[0] = 0.0215f;
        Material_Ambient[1] = 0.1745f;
        Material_Ambient[2] = 0.0215f;

        Material_Diffuse[0] = 0.0215f;
        Material_Diffuse[1] = 0.1745f;
        Material_Diffuse[2] = 0.0215f;
        //
        
        Material_Specular[0] = 0.633f;
        Material_Specular[1] = 0.727811f;
        Material_Specular[2] = 0.633f;

        Material_Shininess[0] = 0.6f * 128.0f;


    // 2 = Jade
        Material_Ambient[3] = 0.135f;
        Material_Ambient[4] = 0.2225f;
        Material_Ambient[5] = 0.1575f;
        //
        ///

        Material_Diffuse[3] = 0.54f;
        Material_Diffuse[4] = 0.89f;
        Material_Diffuse[5] = 0.63f;
        //
        //
        
        Material_Specular[3] = 0.316228f;
        Material_Specular[4] = 0.316228f;
        Material_Specular[5] = 0.316228f;
        //

        Material_Shininess[1] = 0.1f * 128.0f;


    // 3 = obisidian
        Material_Ambient[6] = 0.05375f;
        Material_Ambient[7] = 0.05f;
        Material_Ambient[8] = 0.06625f;
        //

        Material_Diffuse[6] = 0.18275f;
        Material_Diffuse[7] = 0.17f;
        Material_Diffuse[8] = 0.22525f;
        //
        //
        
        Material_Specular[6] = 0.332741f;
        Material_Specular[7] = 0.328634f;
        Material_Specular[8] = 0.346435f;
        //
        //

        Material_Shininess[2] = 0.3f * 128.0f;
        //

    // 4= Pearl
        Material_Ambient[9] = 0.25f;
        Material_Ambient[10] = 0.20725f;
        Material_Ambient[11] = 0.20725f;
        //
        ///

        Material_Diffuse[9] = 1.0f;
        Material_Diffuse[10] = 0.829f;
        Material_Diffuse[11] = 0.829f;
        //
        //
        
        Material_Specular[9] = 0.296648f;
        Material_Specular[10] = 0.296648f;
        Material_Specular[11] = 0.296648f;
        //
        //

        Material_Shininess[3] = 0.088f * 128.0f;
        //

    // 5 = Ruby
        Material_Ambient[12] = 0.1745f;
        Material_Ambient[13] = 0.01175f;
        Material_Ambient[14] = 0.01175f;
        //
        ///

        Material_Diffuse[12] = 0.61424f;
        Material_Diffuse[13] = 0.04136f;
        Material_Diffuse[14] = 0.04136f;
        //
        
        Material_Specular[12] = 0.727811f;
        Material_Specular[13] = 0.626959f;
        Material_Specular[14] = 0.626959f;
        //

        Material_Shininess[4] = 0.6f * 128.0f;
        //

    // 6 = Turquoise
        Material_Ambient[15] = 0.1f;
        Material_Ambient[16] = 0.18275f;
        Material_Ambient[17] = 0.1745f;
        //
        ///

        Material_Diffuse[15] = 0.396f;
        Material_Diffuse[16] = 0.74151f;
        Material_Diffuse[17] = 0.69102f;
        //
        //
        
        Material_Specular[15] = 0.297524f;
        Material_Specular[16] = 0.30829f;
        Material_Specular[17] = 0.306678f;
        //
        //

        Material_Shininess[5] = 0.1f * 128.0f;
        //
      

    // Metals   ---------------------

    // 1 : Brass
        Material_Ambient[18] = 0.349412f;
        Material_Ambient[19] = 0.30829f;
        Material_Ambient[20] = 0.306678f;
        ///

        Material_Diffuse[18] = 0.780392f;
        Material_Diffuse[19] = 0.223529f;
        Material_Diffuse[20] = 0.027451f;
        //
        //
        
        Material_Specular[18] = 0.992157f;
        Material_Specular[19] = 0.941176f;
        Material_Specular[20] = 0.807843f;
        //
        //

        Material_Shininess[6] = 0.21794872f * 128.0f;
        //

    // 2 = Bronze
        Material_Ambient[21] = 0.2125f;
        Material_Ambient[22] = 0.1275f;
        Material_Ambient[23] = 0.054f;
        //
        ///

        Material_Diffuse[21] = 0.714f;
        Material_Diffuse[22] = 0.4284f;
        Material_Diffuse[23] = 0.18144f;
        //
        //
        
        Material_Specular[21] = 0.393548f;
        Material_Specular[22] = 0.271906f;
        Material_Specular[23] = 0.166721f;
        //
        //

        Material_Shininess[7] = 0.2f * 128.0f;
        //

    // 3 = Chrome
        Material_Ambient[24] = 0.25f;
        Material_Ambient[25] = 0.25f;
        Material_Ambient[26] = 0.25f;
        //
        ///

        Material_Diffuse[24] = 0.4f;
        Material_Diffuse[25] = 0.4f;
        Material_Diffuse[26] = 0.4f;
        //
        //
        
        Material_Specular[24] = 0.774597f;
        Material_Specular[25] = 0.774597f;
        Material_Specular[26] = 0.774597f;
        //
        //

        Material_Shininess[8] = 0.6f * 128.0f;
        //

        
    // 4= copper
        Material_Ambient[27] = 0.19125f;
        Material_Ambient[28] = 0.0735f;
        Material_Ambient[29] = 0.02025f;
        //
        ///

        Material_Diffuse[27] = 0.7038f;
        Material_Diffuse[28] = 0.27048f;
        Material_Diffuse[29] = 0.0828f;
        //
        //
        
        Material_Specular[27] = 0.25677f;
        Material_Specular[28] = 0.137622f;
        Material_Specular[29] = 0.086014f;
        //
        //

        Material_Shininess[9] = 0.1f * 128.0f;


    // 5 = Gold
        Material_Ambient[30] = 0.24725f;
        Material_Ambient[31] = 0.1995f;
        Material_Ambient[32] = 0.0745f;
        //
        ///

        Material_Diffuse[30] = 0.7517f;
        Material_Diffuse[31] = 0.6065f;
        Material_Diffuse[32] = 0.2265f;
        //
        
        Material_Specular[30] = 0.6283f;
        Material_Specular[31] = 0.55580f;
        Material_Specular[32] = 0.36606f;
        //

        Material_Shininess[10] = 0.4f * 128.0f;
        //

    // 6 = Silver
        Material_Ambient[33] = 0.19225f;
        Material_Ambient[34] = 0.19225f;
        Material_Ambient[35] = 0.19225f;
        //

        Material_Diffuse[33] = 0.5075f;
        Material_Diffuse[34] = 0.5075f;
        Material_Diffuse[35] = 0.5075f;
        //
        //
        
        Material_Specular[33] = 0.50828f;
        Material_Specular[34] = 0.50828f;
        Material_Specular[35] = 0.50828f;
        //
        //

        Material_Shininess[11] = 0.4f * 128.0f;
        //


    // Plastic ---------------------
    // 1 - Black
        Material_Ambient[36] = 0.0f;
        Material_Ambient[37] = 0.0f;
        Material_Ambient[38] = 0.0f;
        //
        ///

        Material_Diffuse[36] = 0.01f;
        Material_Diffuse[37] = 0.01f;
        Material_Diffuse[38] = 0.01f;
        //
        //
        
        Material_Specular[36] = 0.5f;
        Material_Specular[37] = 0.5f;
        Material_Specular[38] = 0.5f;
        //
        //

        Material_Shininess[12] = 0.25f * 128.0f;
        //


    // 2 - Cyan
        Material_Ambient[39] = 0.0f;
        Material_Ambient[40] = 0.1f;
        Material_Ambient[41] = 0.06f;
        //
        ///

        Material_Diffuse[39] = 0.0f;
        Material_Diffuse[40] = 0.5098039f;
        Material_Diffuse[41] = 0.5098039f;
        //
        //
        
        Material_Specular[39] = 0.0f;
        Material_Specular[40] = 0.501960f;
        Material_Specular[41] = 0.501960f;
        //

        Material_Shininess[13] = 0.25f * 128.0f;
        //

    //3 - Green
        Material_Ambient[42] = 0.0f;
        Material_Ambient[43] = 0.1f;
        Material_Ambient[44] = 0.06f;
        //
        ///

        Material_Diffuse[42] = 0.1f;
        Material_Diffuse[43] = 0.35f;
        Material_Diffuse[44] = 0.1f;
        //
        //
        
        Material_Specular[42] = 0.45f;
        Material_Specular[43] = 0.55f;
        Material_Specular[44] = 0.45f;
        //
        //

        Material_Shininess[14] = 0.25f * 128.0f;
        //


    // Red

        Material_Ambient[45] = 0.0f;
        Material_Ambient[46] = 0.0f;
        Material_Ambient[47] = 0.0f;
        //
        ///

        Material_Diffuse[45] = 0.5f;
        Material_Diffuse[46] = 0.0f;
        Material_Diffuse[47] = 0.0f;
        //
        //
        
        Material_Specular[45] = 0.7f;
        Material_Specular[46] = 0.6f;
        Material_Specular[47] = 0.6f;
        //
        //

        Material_Shininess[15] = 0.25f * 128.0f;


    // White
        Material_Ambient[48] = 0.0f;
        Material_Ambient[49] = 0.0f;
        Material_Ambient[50] = 0.0f;
        //
        ///

        Material_Diffuse[48] = 0.55f;
        Material_Diffuse[49] = 0.55f;
        Material_Diffuse[50] = 0.55f;
        //
        //
        
        Material_Specular[48] = 0.7f;
        Material_Specular[49] = 0.7f;
        Material_Specular[50] = 0.7f;
        //
        //

        Material_Shininess[16] = 0.25f * 128.0f;
        //


    // Yellow
        Material_Ambient[51] = 0.0f;
        Material_Ambient[52] = 0.0f;
        Material_Ambient[53] = 0.0f;
        //
        ///

        Material_Diffuse[51] = 0.5f;
        Material_Diffuse[52] = 0.5f;
        Material_Diffuse[53] = 0.0f;
        
        Material_Specular[51] = 0.6f;
        Material_Specular[52] = 0.6f;
        Material_Specular[53] = 0.5f;

        Material_Shininess[17] = 0.25f * 128.0f;


    // -------- Rubber
    // 1 - Black
        Material_Ambient[54] = 0.02f;
        Material_Ambient[55] = 0.02f;
        Material_Ambient[56] = 0.02f;
        //
        ///

        Material_Diffuse[54] = 0.01f;
        Material_Diffuse[55] = 0.01f;
        Material_Diffuse[56] = 0.01f;
        //
        //
        
        Material_Specular[54] = 0.4f;
        Material_Specular[55] = 0.4f;
        Material_Specular[56] = 0.4f;
        //
        //

        Material_Shininess[18] = 0.07813f * 128.0f;
        //


    // 2 - Cyan
        Material_Ambient[57] = 0.0f;
        Material_Ambient[58] = 0.05f;
        Material_Ambient[59] = 0.05f;
        //
        ///

        Material_Diffuse[57] = 0.4f;
        Material_Diffuse[58] = 0.5098039f;
        Material_Diffuse[59] = 0.5098039f;
        //
        //
        
        Material_Specular[57] = 0.0f;
        Material_Specular[58] = 0.501960f;
        Material_Specular[59] = 0.501960f;
        //
        //

        Material_Shininess[19] = 0.07813f * 128.0f;
        //

        //
        
    //3 - Green
        Material_Ambient[60] = 0.0f;
        Material_Ambient[61] = 0.1f;
        Material_Ambient[62] = 0.06f;
        //
        ///

        Material_Diffuse[60] = 0.1f;
        Material_Diffuse[61] = 0.35f;
        Material_Diffuse[62] = 0.1f;
        //
        //
        
        Material_Specular[60] = 0.45f;
        Material_Specular[61] = 0.55f;
        Material_Specular[62] = 0.45f;
        //
        //

        Material_Shininess[20] = 0.07813f * 128.0f;
        //

    // 4- Red

        Material_Ambient[63] = 0.0f;
        Material_Ambient[64] = 0.0f;
        Material_Ambient[65] = 0.0f;
        //
        ///

        Material_Diffuse[63] = 0.5f;
        Material_Diffuse[64] = 0.0f;
        Material_Diffuse[65] = 0.0f;
        //
        //
        
        Material_Specular[63] = 0.7f;
        Material_Specular[64] = 0.6f;
        Material_Specular[65] = 0.6f;
        //
        //

        Material_Shininess[21] = 0.07813f * 128.0f;
        //

    // 5- White
        Material_Ambient[66] = 0.0f;
        Material_Ambient[67] = 0.0f;
        Material_Ambient[68] = 0.0f;
        //
        ///

        Material_Diffuse[66] = 0.55f;
        Material_Diffuse[67] = 0.55f;
        Material_Diffuse[68] = 0.55f;
        //
        //
        
        Material_Specular[66] = 0.7f;
        Material_Specular[67] = 0.7f;
        Material_Specular[68] = 0.7f;
        //
        //

        Material_Shininess[22] = 0.07813f * 128.0f;

    // 6- Yellow
        Material_Ambient[69] = 0.0f;
        Material_Ambient[70] = 0.0f;
        Material_Ambient[71] = 0.0f;

        Material_Diffuse[69] = 0.5f;
        Material_Diffuse[70] = 0.5f;
        Material_Diffuse[71] = 0.0f;
        
        Material_Specular[69] = 0.6f;
        Material_Specular[70] = 0.6f;
        Material_Specular[71] = 0.5f;

        Material_Shininess[23] = 0.07813f * 128.0f;


        // ---------- Set
        int width = (GLint)(winWidth / 6);
        int height = (GLint)(winHeight / 4);

        int i,j;

      // material : i*3+j
      for(i = 0; i <= 5; i++) //x
       {
        for(j = 0; j <= 3; j++) //y
        {
            //1
            glUniform1f(u_Material_Shininess,Material_Shininess[i+j]);
            
            glUniform3f(KS_Uniform,Material_Specular[3*i+j],Material_Specular[3*i+j+1],Material_Specular[3*i+j+2]);
            glUniform3f(KD_Uniform,Material_Diffuse[3*i+j],Material_Diffuse[3*i+j+1],Material_Diffuse[3*i+j+2]);
            glUniform3f(KA_Uniform,Material_Ambient[3*i+j],Material_Ambient[3*i+j+1],Material_Ambient[3*i+j+2]);

            //2
            glViewport(width*i, height*j, width, height);

            PerspectProjectionMatrix = vmath::perspective(45.0f, (GLfloat)(width) / (GLfloat)(height), 0.1f, 100.0f);
            
            //imp
            draw();

        }

     }

    
    //Scene
    
    
	//common
	glUseProgram(0);


	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}


-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}


-(void)keyDown:(NSEvent *)theEvent
{
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[self release];
			[NSApp terminate:self];
			break;

		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repaint
			break;

        case 'L':
        case 'l':
            if (!bLighting)
            {
                bLighting = true;
                bisLPressed = true;
            }
            else
            {
                bLighting = false;
                bisLPressed = false;
            }
            break;
            
        case 'x':
        case 'X':
            XPressed = true;
            YPressed = false;
            ZPressed = false;
            break;
    
        case 'y':
        case 'Y':
            YPressed = true;
            ZPressed = false;
            XPressed = false;
            break;

        case 'z':
        case 'Z':
            ZPressed = true;
            XPressed = false;
            YPressed = false;
            break;

        
        default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	/* code */
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	/* code */
}

//Uninit
-(void) dealloc
{
	

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	
	gShaderProgramObject = 0;
	
	glUseProgram(0);

	CVDisplayLinkStop(displaylink);
	CVDisplayLinkRelease(displaylink);

	[super dealloc];
}

@end


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displaylink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagOut, void *pDisplayLingContext)
{
	CVReturn result = [(GLView *)pDisplayLingContext getFrameForTime:pOutputTime];
	return(result);
}

