
/* 
   Sphere using Shaders
*/

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
#import "YSR_Sphere.h"

using namespace vmath;


// 'C' style global function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags , CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;

// interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end


// Entry point 
int main(int argc, const char *argv[])
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate :[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];
	
	return(0);
}


// interface
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	/* 
		Log file : 5 steps
		
	*/
	
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];

	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];

	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

	gpFile = fopen(pszLogFileNameWithPath, "w");

	if(gpFile == NULL)
	{
		printf("Can Not Create Log File.\n");
		[self release];
		[NSApp terminate:self];
	}
	
	fprintf(gpFile, "Program Started Successfully \n");

	/* create window */
	
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	window = [[NSWindow alloc]initWithContentRect:win_rect 
		styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | 
		NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
		backing:NSBackingStoreBuffered defer:NO];
	
	[window setTitle:@"macOS : Per Fragment Light on Sphere by YSR"];
	
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}


- (void)applicationWillTerminate:(NSNotification *)notification
{
	/* code */
	fprintf(gpFile, "Program is Terminated Successfully\n");

	if (gpFile)
	{
		/* code */
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];

	[window release];

	[super dealloc];
}
@end


@implementation GLView
{
@private
	CVDisplayLinkRef displaylink;

	// for Shaders
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

    GLuint gVao_sphere;
    GLuint gVbo_sphere_position;
    GLuint gVbo_sphere_normal;
    GLuint gVbo_sphere_element;
    
    //12 Uniforms
    GLuint Model_Uniform;
    GLuint View_Uniform;
    GLuint Projection_Uniform;
    
    GLuint LD_Uniform, LA_Uniform, LS_Uniform;
    GLuint KD_Uniform, KA_Uniform, KS_Uniform;
    
    GLuint Material_Shininess;
    GLuint Light_Position_Uniform;
    GLuint LisPressed_Uniform;

    
    bool bisLPressed ;
    
    bool bLighting ;

    int gNumVertices , gNumElements;
    
    GLuint mvpUniform;
	mat4 PerspectProjectionMatrix;
}


-(id)initWithFrame:(NSRect)frame;
{
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFAAccelerated,
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		if (pixelFormat == nil)
		{
			fprintf(gpFile, "No valid OpenGL Pixel Format is Available.");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext];
	}
	return(self);
}

//imp
-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return(kCVReturnSuccess);
}


//Init
- (void)prepareOpenGL
{
	fprintf(gpFile, "OpenGL Version	:	%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version 	:	%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	// VERTEX SHADER
	
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
        "#version 410 core" \
        "\n" \
		"in vec4 vPosition;"						\
		"in vec3 vNormal;"							\
		"out vec3 T_Norm;"							\
		"out vec3 Light_Direction;"					\
		"out vec3 Viewer_Vector;"					\
		"\n"										\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform int u_LKeyPressed;"				\
		"uniform vec4 u_Light_Position;"			\
		"\n"										\
		"void main(void)"							\
		"{"											\
		"if(u_LKeyPressed == 1)"					\
		"{"											\
		"vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"	\
		"T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"			\
		"Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"	\
		"Viewer_Vector = vec3(-Eye_Coordinates.xyz);"\
		"}"		\
		"gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"	\
		"}" ;
    
    

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	
	// Compile
	glCompileShader(gVertexShaderObject);


	// Error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}


	// FRAGMENT SHADER
	
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	
	const GLchar *fragmentShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "precision highp float;"
		"in vec3 T_Norm;"			\
		"in vec3 Light_Direction;"	\
		"in vec3 Viewer_Vector;"	\
		"out vec4 FragColor;"		\
		"\n"						\
		"uniform vec3 u_LA;"						\
		"uniform vec3 u_LD;"						\
		"uniform vec3 u_LS;"						\
		"uniform vec3 u_KA;"						\
		"uniform vec3 u_KD;"						\
		"uniform vec3 u_KS;"						\
		"uniform float u_Shininess;"				\
		"uniform int u_LKeyPressed;"				\
		"uniform vec4 u_Light_Position;"			\
		"\n"										\
		"void main(void)"		\
		"{"						\
		"if(u_LKeyPressed == 1)"\
		"{"						\
		"vec3 Normalized_View_Vector = normalize(Viewer_Vector);"	\
		"vec3 Normalized_Light_Direction = normalize(Light_Direction);"\
		"vec3 Normalized_TNorm = normalize(T_Norm);"\
		"float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0);"	\
		"vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm);"\
		"vec3 Ambient = vec3(u_LA * u_KA);"	\
		"vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);"	\
		"vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess));" \
		"vec3 Phong_ADS_Light = Ambient + Diffuse+ Specular;"	\
		"FragColor = vec4(Phong_ADS_Light,1.0);" \
		"}"							\
		"else"						\
		"{"							\
		"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		"}"							\
		"}" ;

    
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	
	// Compile
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);


	// Prelinking
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);

				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);

				free(szInfoLog);

				[self release];

				[NSApp terminate:self];

				exit(0);
			}
		}
	}


	// Post Linking
    
    mvpUniform = glGetUniformLocation(gShaderProgramObject,"u_model_matrix");

    View_Uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
    
    Projection_Uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
    
    KD_Uniform = glGetUniformLocation(gShaderProgramObject, "u_KD");
        
    LD_Uniform = glGetUniformLocation(gShaderProgramObject, "u_LD");
    
    Light_Position_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Light_Position");

    LisPressed_Uniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");


	LA_Uniform = glGetUniformLocation(gShaderProgramObject, "u_LA");
	KA_Uniform = glGetUniformLocation(gShaderProgramObject, "u_KA");

	LS_Uniform = glGetUniformLocation(gShaderProgramObject, "u_LS");
	KS_Uniform = glGetUniformLocation(gShaderProgramObject, "u_KS");

	Material_Shininess = glGetUniformLocation(gShaderProgramObject, "u_Shininess");
	

    //call
    makeSphere(2.0,30,30);

	//Depth
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.1f, 0.0f, 0.0f, 1.0f);

	PerspectProjectionMatrix = mat4::identity();

	//new
	CVDisplayLinkCreateWithActiveCGDisplays(&displaylink);
	CVDisplayLinkSetOutputCallback(displaylink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displaylink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displaylink);
	
    [super prepareOpenGL];
}

//Resize
- (void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat Width = rect.size.width;
	GLfloat Height = rect.size.height;

	if (Height == 0)
		Height = 1;

	glViewport(0, 0,(GLsizei)Width,(GLsizei)Height);

    PerspectProjectionMatrix = vmath::perspective(45.0f,Width/Height,0.1f,100.0f);
    
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

- (void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
}

//Display
- (void)drawView
{
	[[self openGLContext]makeCurrentContext];
	
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
    
    
    mat4 Model_Matrix;
    mat4 View_Matrix;
    mat4 Projection_Matrix;
    mat4 Translation_Matrix;
    mat4 Rotation_Matrix;
    mat4 ModelView_Projection_Matrix;
	
	//2 : I[]
	Model_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	View_Matrix = mat4::identity();
	Rotation_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//4 : Do Mat mul
	Translation_Matrix = translate(0.0f, 0.0f, -7.0f);

	Model_Matrix = Translation_Matrix * Rotation_Matrix;

	//ModelView_Projection_Matrix = Perspective_Projection_Matrix * View_Matrix * Model_Matrix;


	// 5	: send it to shader

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, Model_Matrix);
    
	glUniformMatrix4fv(Projection_Uniform, 1, GL_FALSE, PerspectProjectionMatrix);
    
	glUniformMatrix4fv(View_Uniform, 1, GL_FALSE, View_Matrix);
	
	if (bLighting == true)
	{
		glUniform1i(LisPressed_Uniform, 1);
		glUniform3f(LD_Uniform, 1.0f, 1.0f, 1.0f);
		glUniform3f(KD_Uniform, 1.0f, 1.0f, 1.0f);

		glUniform3f(LS_Uniform, 1.0f, 1.0f, 1.0f);
		glUniform3f(KS_Uniform, 1.0f, 1.0f, 1.0f);
		
		glUniform3f(LA_Uniform, 0.0f, 0.0f, 0.0f);
		glUniform3f(KA_Uniform, 0.0f, 0.0f, 0.0f);
		
		glUniform1f(Material_Shininess, 128.0f);
		
	//imp : out to in Light
		glUniform4f(Light_Position_Uniform, 100.0f, 100.0f, 50.0f, 1.0f);
	}

	else
	{
		glUniform1i(LisPressed_Uniform, 0);
	}

    //Scene
    
    draw();
    
	//common
	glUseProgram(0);


	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}


-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}


-(void)keyDown:(NSEvent *)theEvent
{
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[self release];
			[NSApp terminate:self];
			break;

		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repaint
			break;

        case 'L':
        case 'l':
            if (!bLighting)
            {
                bLighting = true;
                bisLPressed = true;
            }
            else
            {
                bLighting = false;
                bisLPressed = false;
            }
            break;
        
        default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	/* code */
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	/* code */
}

//Uninit
-(void) dealloc
{
	
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}
    
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}
	
    if (gVbo_sphere_position)
    {
        glDeleteBuffers(1, &gVbo_sphere_position);
        gVbo_sphere_position = 0;
    }

    if (gVao_sphere)
    {
        glDeleteVertexArrays(1, &gVao_sphere);
        gVao_sphere = 0;
    }

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	
	gShaderProgramObject = 0;
	
	glUseProgram(0);

	CVDisplayLinkStop(displaylink);
	CVDisplayLinkRelease(displaylink);

	[super dealloc];
}

@end


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displaylink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagOut, void *pDisplayLingContext)
{
	CVReturn result = [(GLView *)pDisplayLingContext getFrameForTime:pOutputTime];
	return(result);
}

