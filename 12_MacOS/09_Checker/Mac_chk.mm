
/* 
	3D
*/

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

using namespace vmath;

//imp
enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// 'C' style global function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags , CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;
int CHECK_IMG_WIDTH = 64;
int CHECK_IMG_HEIGHT = 64;
GLubyte CheckImage[64][64][4];


// interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end


// Entry point 
int main(int argc, const char *argv[])
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate :[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];
	
	return(0);
}


// interface
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	/* 
		Log file : 5 steps
		
	*/
	
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];

	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];

	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

	gpFile = fopen(pszLogFileNameWithPath, "w");

	if(gpFile == NULL)
	{
		printf("Can Not Create Log File.\n");
		[self release];
		[NSApp terminate:self];
	}
	
	fprintf(gpFile, "Program Started Successfully \n");

	/* create window */
	
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	window = [[NSWindow alloc]initWithContentRect:win_rect 
		styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | 
		NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
		backing:NSBackingStoreBuffered defer:NO];
	
	[window setTitle:@"macOS : CheckerBoard by YSR"];
	
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}


- (void)applicationWillTerminate:(NSNotification *)notification
{
	/* code */
	fprintf(gpFile, "Program is Terminated Successfully\n");

	if (gpFile)
	{
		/* code */
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];

	[window release];

	[super dealloc];
}
@end


@implementation GLView
{
@private
	CVDisplayLinkRef displaylink;

	// for Shaders
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

    GLuint vao_Rectangle;
    GLuint vbo_Position_Rectangle;
    GLuint vbo_Texture;
    
  
    //for Texture
    GLuint Texture_CheckImg;
    
    
	GLuint mvpUniform;
    GLuint samplerUniform;
	mat4 PerspectProjectionMatrix;
}


-(id)initWithFrame:(NSRect)frame;
{
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFAAccelerated,
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		if (pixelFormat == nil)
		{
			fprintf(gpFile, "No valid OpenGL Pixel Format is Available.");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext];
	}
	return(self);
}

//imp
-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return(kCVReturnSuccess);
}


//Init
- (void)prepareOpenGL
{
	fprintf(gpFile, "OpenGL Version	:	%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version 	:	%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	// VERTEX SHADER
	
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;" \
        "in vec2 vTexCoord;" \
        "out vec2 out_texCoord;" \
        "uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
        "out_texCoord = vTexCoord;" \
        "}";
	
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	
	// Compile
	glCompileShader(gVertexShaderObject);


	// Error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}


	// FRAGMENT SHADER
	
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	
	const GLchar *fragmentShaderSourceCode =
                    "#version 410 core" \
                    "\n" \
                    "precision highp float;"
                    "in vec2 out_texCoord;" \
                    "uniform sampler2D u_sampler;" \
                    "out vec4 FragColor;" \
                    "void main(void)" \
                    "{" \
                    "FragColor = texture(u_sampler, out_texCoord);"\
                    "}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	
	// Compile
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);


	// Prelinking
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);

				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);

				free(szInfoLog);

				[self release];

				[NSApp terminate:self];

				exit(0);
			}
		}
	}

   
	// Post Linking
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

    samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_sampler");
    
    
    //shapes

   //Rectangle
    //**********
    
    //create vao for Rectangle
    glGenVertexArrays(1, &vao_Rectangle);

    //Binding
    glBindVertexArray(vao_Rectangle);

    //Buffer
    glGenBuffers(1, &vbo_Position_Rectangle);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Rectangle);

    // 3 = axis, 4 = vertices, 4 = size of type(float)
    glBufferData(GL_ARRAY_BUFFER, 3*4*4, NULL, GL_DYNAMIC_DRAW);

    //most imp
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Set Position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);


//New
    const GLfloat TexCoords[]=
    {
        1.0f,1.0f,
        0.0f,1.0f,
        0.0f,0.0f,
        1.0f,0.0f
    };

    glGenBuffers(1, &vbo_Texture);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Texture);
    glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords),TexCoords, GL_STATIC_DRAW);

    glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
    

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);

    

	//Depth
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

    //Vimp
       // Load Textures
    [self loadTextureFromBMPFile];
    
    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);  //imp

	PerspectProjectionMatrix = mat4::identity();

	//new
	CVDisplayLinkCreateWithActiveCGDisplays(&displaylink);
	CVDisplayLinkSetOutputCallback(displaylink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displaylink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displaylink);
	
    [super prepareOpenGL];
    
}

//Maths Texture
 void MakeCheckImage()
    {
        int i,j,c;

        for(i=0; i < CHECK_IMG_HEIGHT;i++)
        {
            for(j=0; j < CHECK_IMG_WIDTH; j++)
            {
                //Most imp
                c = (((i&0x8) == 0) ^ ((j&0x8) == 0))*255;

                CheckImage[i][j][0] = (GLubyte) c;   // R
                CheckImage[i][j][1] = (GLubyte) c;   //G
                CheckImage[i][j][2] = (GLubyte) c;   //B
                CheckImage[i][j][3] = (GLubyte) 255; //A

            }

        }

    }    //Texture Loading


//VIMP

-(void)loadTextureFromBMPFile
{

    //try
    MakeCheckImage();

    
    glGenTextures(1, &Texture_CheckImg);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    glBindTexture(GL_TEXTURE_2D, Texture_CheckImg);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, CHECK_IMG_WIDTH, CHECK_IMG_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, CheckImage); //pixels

    glGenerateMipmap(GL_TEXTURE_2D);
    
    glBindTexture(GL_TEXTURE_2D, 0);
}



//Resize
- (void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat Width = rect.size.width;
	GLfloat Height = rect.size.height;

	if (Height == 0)
		Height = 1;

	glViewport(0, 0,(GLsizei)Width,(GLsizei)Height);

    PerspectProjectionMatrix = vmath::perspective(45.0f,Width/Height,0.1f,100.0f);
    
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

- (void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
}

//Display
- (void)drawView
{
	[[self openGLContext]makeCurrentContext];
	
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
	
	mat4 ModelViewMatrix;
	mat4 ModelViewProjectionMatrix;
	mat4 translateMatrix;
    mat4 Rotation_Matrix;

//  Rectangle
	ModelViewMatrix = mat4::identity();
	ModelViewProjectionMatrix = mat4::identity();
    translateMatrix = mat4::identity();
    Rotation_Matrix = mat4::identity();

    //new
    translateMatrix = vmath::translate(0.0f,0.0f,-6.0f);

    
    ModelViewMatrix = translateMatrix * Rotation_Matrix;

	// Matrix Multiplication
	ModelViewProjectionMatrix = PerspectProjectionMatrix * ModelViewMatrix;

	glUniformMatrix4fv(mvpUniform,	1,	GL_FALSE,ModelViewProjectionMatrix);

	//imp
    // texture bind code
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D , Texture_CheckImg);
    glUniform1i(samplerUniform, 0);

    
    //new
    GLfloat RectVertices1[12] =
    {
        -2.0f, -1.0f, 0.0f,
        -2.0f, 1.0f, 0.0f,
         0.0f, 1.0f, 0.0f,
         0.0f, -1.0f, 0.0f
    };

    GLfloat RectVertices2[12] =
    {
        1.0f, -1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        2.41421f, 1.0f, -1.41421f,
        2.41421f, -1.0f, -1.41421f
    };

    //1st
    glBindVertexArray(vao_Rectangle);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Rectangle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(RectVertices1), RectVertices1, GL_STATIC_DRAW);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);

    //2nd
    glBindVertexArray(vao_Rectangle);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Rectangle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(RectVertices2), RectVertices2, GL_STATIC_DRAW);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    
	// Unbind 
	glBindVertexArray(0);

	//common
	glUseProgram(0);

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}


-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}


-(void)keyDown:(NSEvent *)theEvent
{
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[self release];
			[NSApp terminate:self];
			break;

		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repaint
			break;

        default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	/* code */
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	/* code */
}

//Uninit
-(void) dealloc
{
	
	
	if (vao_Rectangle)
	{
		glDeleteBuffers(1, &vao_Rectangle);
		vao_Rectangle = 0;
	}
	
	
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	
	gShaderProgramObject = 0;
	
	glUseProgram(0);

	CVDisplayLinkStop(displaylink);
	CVDisplayLinkRelease(displaylink);

	[super dealloc];
}

@end


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displaylink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagOut, void *pDisplayLingContext)
{
	CVReturn result = [(GLView *)pDisplayLingContext getFrameForTime:pOutputTime];
	return(result);
}

