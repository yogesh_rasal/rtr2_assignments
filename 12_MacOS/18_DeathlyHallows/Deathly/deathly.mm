
/* 
	3D
*/

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

using namespace vmath;

//imp
enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// 'C' style global function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags , CVOptionFlags *, void *);

#define SLICES 20
#define NUM_POINTS 1000

void showGraph();


FILE *gpFile = NULL;


// interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end


// Entry point 
int main(int argc, const char *argv[])
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate :[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];
	
	return(0);
}


// interface
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	/* 
		Log file : 5 steps
		
	*/
	
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];

	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];

	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

	gpFile = fopen(pszLogFileNameWithPath, "w");

	if(gpFile == NULL)
	{
		printf("Can Not Create Log File.\n");
		[self release];
		[NSApp terminate:self];
	}
	
	fprintf(gpFile, "Program Started Successfully \n");

	/* create window */
	
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	window = [[NSWindow alloc]initWithContentRect:win_rect 
		styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | 
		NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
		backing:NSBackingStoreBuffered defer:NO];
	
	[window setTitle:@"macOS : Deathly Hallows by YSR"];
	
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}


- (void)applicationWillTerminate:(NSNotification *)notification
{
	/* code */
	fprintf(gpFile, "Program is Terminated Successfully\n");

	if (gpFile)
	{
		/* code */
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];

	[window release];

	[super dealloc];
}
@end


@implementation GLView
{
@private
	CVDisplayLinkRef displaylink;

	// for Shaders
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

	GLuint mvpUniform;
    GLuint samplerUniform;
	mat4 PerspectiveProjectionMatrix;

    //new
    GLuint vao_Triangle,vao_Rectagle;
    GLuint vbo_Position_Triangle;
    GLuint vbo_Position_Rectangle;
    GLuint vbo_Color_Triangle;
    GLuint vbo_Color_Rectangle;


    //for circle
    GLuint vao_Circle, vbo_Position_Circle, vbo_Color_Circle;


}


-(id)initWithFrame:(NSRect)frame;
{
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFAAccelerated,
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		if (pixelFormat == nil)
		{
			fprintf(gpFile, "No valid OpenGL Pixel Format is Available.");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext];
	}
	return(self);
}

//imp
-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return(kCVReturnSuccess);
}


//Init
- (void)prepareOpenGL
{
	fprintf(gpFile, "OpenGL Version	:	%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version 	:	%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	// VERTEX SHADER
	
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 410 core" \
		"\n" \
        "in vec4 vPosition;"                        \
        "in vec4 vColor;"                            \
        "out vec4 Out_Color;"                        \
        "uniform mat4 u_mvp_matrix;"                \
        "void main(void)"                            \
        "{"                                           \
        "gl_Position = u_mvp_matrix * vPosition;"    \
        "Out_Color = vColor;"                        \
        "}" ;

    
    
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	
	// Compile
	glCompileShader(gVertexShaderObject);


	// Error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}


	// FRAGMENT SHADER
	
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	
	const GLchar *fragmentShaderSourceCode =
            "#version 410 core" \
            "\n" \
            "precision highp float;"
            "in vec4 Out_Color;"    \
            "out vec4 FragColor;"    \
            "void main(void)"        \
            " { "                    \
            "FragColor = Out_Color;" \
            " } " ;

    
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	
	// Compile
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);


	// Prelinking
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
    
    
	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);

				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);

				free(szInfoLog);

				[self release];

				[NSApp terminate:self];

				exit(0);
			}
		}
	}

   
	// Post Linking
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
    
    
    //shapes


	//use Triangle vertices
	const GLfloat Triangle_Vertices[] =
    {   0.0f,1.0f,0.0f,
        -1.0f,-1.0f,0.0f,
        
        1.0f,-1.0f,0.0f,
        0.0f,1.0f,0.0f,
        
        1.0f,-1.0f,0.0f,
       -1.0f,-1.0f,0.0f

    };

	const GLfloat Triangle_Colors[] =
    {
          1.0f,1.0f,1.0f,
          1.0f,1.0f,1.0f,
         
          1.0f,1.0f,1.0f,
          1.0f,1.0f,1.0f,
        
          1.0f,1.0f,1.0f,
          1.0f,1.0f,1.0f,
          
    };

	//create vao for Triangle
	glGenVertexArrays(1, &vao_Triangle);

	//Binding
	glBindVertexArray(vao_Triangle);

	//Buffer
	glGenBuffers(1, &vbo_Position_Triangle);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_Position_Triangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Triangle_Vertices),Triangle_Vertices,GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	//vao

	//Color
	//Buffer
	glGenBuffers(1, &vbo_Color_Triangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Triangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Triangle_Colors),
		Triangle_Colors, GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	glBindVertexArray(0); //vao

	//**********
	const GLfloat Rectangle_Vertices[] =
    {
		0.0f,-1.0f,0.0f,
		0.0f,1.0f,0.0f
	};

	const GLfloat Rectangle_Colors[] =
    {
		1.0f,0.50f,0.50f,
		1.0f,0.50f,0.50f,
        
    };

	//create vao for Rectangle
	glGenVertexArrays(1, &vao_Rectagle);

	//Binding
	glBindVertexArray(vao_Rectagle);

	//Buffer
	glGenBuffers(1, &vbo_Position_Rectangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Rectangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Rectangle_Vertices),Rectangle_Vertices, GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Color

	glGenBuffers(1, &vbo_Color_Rectangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Rectangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Rectangle_Colors),	Rectangle_Colors, GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); //vao
	

    //Circle
    GLfloat fPerimeter;
    GLfloat fRadius;

    GLfloat CircleVertices[NUM_POINTS][3];
    GLfloat CircleColor[NUM_POINTS][3];

    //Code
    fPerimeter = (GLfloat)sqrt(((1.0f)*(1.0f)) + ((1.0f)*(1.0f))); //imp
    fRadius = fPerimeter / 2;

    for (int i = 0; i < NUM_POINTS; i++)
    {
        GLfloat angle = (GLfloat)(2 * M_PI * i) / NUM_POINTS; //2 Pie R
        CircleVertices[i][0] = (GLfloat)cos(angle) * fRadius;
        CircleVertices[i][1] = (GLfloat)sin(angle) * fRadius;
        CircleVertices[i][2] = 0.0f;

        //yellow
        CircleColor[i][0] = 1.0f;
        CircleColor[i][1] = 1.0f;
        CircleColor[i][2] = 0.0f;
    }


    // Create vao
    glGenVertexArrays(1, &vao_Circle);
    glBindVertexArray(vao_Circle);

    glGenBuffers(1, &vbo_Position_Circle);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Circle);

    glBufferData(GL_ARRAY_BUFFER, sizeof(CircleVertices), CircleVertices, GL_STATIC_DRAW);

    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    // Unbind vbo
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Bind vboColor
    glGenBuffers(1, &vbo_Color_Circle);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Circle);

    glBufferData(GL_ARRAY_BUFFER, sizeof(CircleColor), CircleColor, GL_STATIC_DRAW);

    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

    // Unbind vbo
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Unbind
    glBindVertexArray(0);

    
	//Depth
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);  //imp

	PerspectiveProjectionMatrix = mat4::identity();

	//new
	CVDisplayLinkCreateWithActiveCGDisplays(&displaylink);
	CVDisplayLinkSetOutputCallback(displaylink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displaylink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displaylink);
	
    [super prepareOpenGL];
    
}


//Resize
- (void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat Width = rect.size.width;
	GLfloat Height = rect.size.height;

	if (Height == 0)
		Height = 1;

	glViewport(0, 0,(GLsizei)Width,(GLsizei)Height);

    PerspectiveProjectionMatrix = vmath::perspective(45.0f,Width/Height,0.1f,100.0f);
    
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}


- (void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
}


//Display
- (void)drawView
{
	[[self openGLContext]makeCurrentContext];
	
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

    //code : 9 steps
        //1
    mat4 ModelView_Matrix;
    mat4 ModelView_Projection_Matrix;
    mat4 Translation_Matrix;
    mat4 Rotation_Matrix;

    //Circle
    //2 : I[]
    Rotation_Matrix = mat4::identity();
    ModelView_Matrix = mat4::identity();
    ModelView_Projection_Matrix = mat4::identity();
    Translation_Matrix = mat4::identity();

    //3 : Transforms
    Translation_Matrix = translate(0.0f, -0.45f, -3.89f);

    //4 : Do Mat mul
    ModelView_Matrix = Translation_Matrix * Rotation_Matrix;

    //Sequence is imp
    ModelView_Projection_Matrix = PerspectiveProjectionMatrix * ModelView_Matrix;


    // 5 : send it to shader
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

    //Bind
    glBindVertexArray(vao_Circle);

    //draw
    glLineWidth(2.0f);
    glDrawArrays(GL_LINE_LOOP, 0, NUM_POINTS);

    // Unbind vao
    glBindVertexArray(0);

    
    //Rectangle
    //2 : I[]
    ModelView_Matrix = mat4::identity();
    ModelView_Projection_Matrix = mat4::identity();
    Rotation_Matrix = mat4::identity();
    Translation_Matrix = mat4::identity();

    //4 : Do Mat mul
    Translation_Matrix = translate(0.0f, 0.0f, -3.40f);

    ModelView_Matrix = Translation_Matrix * Rotation_Matrix;

    ModelView_Projection_Matrix = PerspectiveProjectionMatrix * ModelView_Matrix;


    // 5 : send it to shader
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

    // 6 : bind to vao
    glBindVertexArray(vao_Rectagle);

    //8 : draw scene
    glDrawArrays(GL_LINES, 0, 2);

    //9 : Unbind vao
    glBindVertexArray(0);

    
//Triangle
    //2 : I[]
    Rotation_Matrix = mat4::identity();
    ModelView_Matrix = mat4::identity();
    ModelView_Projection_Matrix = mat4::identity();
    Translation_Matrix = mat4::identity();

    //3 : Transforms

    Translation_Matrix = translate(0.0f, 0.0f, -3.32f);

    //4 : Do Mat mul

    ModelView_Matrix = Translation_Matrix * Rotation_Matrix;

    //new
    ModelView_Projection_Matrix = PerspectiveProjectionMatrix * ModelView_Matrix;


    // 5 : send it to shader
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

    glBindVertexArray(vao_Triangle);

    //8 : draw scene
    glLineWidth(3.0f);
    glDrawArrays(GL_LINES, 0, 6);

    //9 : Unbind vao
    glBindVertexArray(0);


	//common
	glUseProgram(0);

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}


-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}


-(void)keyDown:(NSEvent *)theEvent
{
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[self release];
			[NSApp terminate:self];
			break;

		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repaint
			break;

        default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	/* code */
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	/* code */
}

//Uninit
-(void) dealloc
{
	
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	
	gShaderProgramObject = 0;
	
	glUseProgram(0);

	CVDisplayLinkStop(displaylink);
	CVDisplayLinkRelease(displaylink);

	[super dealloc];
}

@end


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displaylink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagOut, void *pDisplayLingContext)
{
	CVReturn result = [(GLView *)pDisplayLingContext getFrameForTime:pOutputTime];
	return(result);
}

