
/* 
	Texture
*/

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

using namespace vmath;

//imp
enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// 'C' style global function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags , CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;

// interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end


// Entry point 
int main(int argc, const char *argv[])
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate :[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];
	
	return(0);
}


// interface
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	/* 
		Log file : 5 steps
		
	*/
	
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];

	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];

	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

	gpFile = fopen(pszLogFileNameWithPath, "w");

	if(gpFile == NULL)
	{
		printf("Can Not Create Log File.\n");
		[self release];
		[NSApp terminate:self];
	}
	
	fprintf(gpFile, "Program Started Successfully \n");

	/* create window */
	
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	window = [[NSWindow alloc]initWithContentRect:win_rect 
		styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | 
		NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
		backing:NSBackingStoreBuffered defer:NO];
	
	[window setTitle:@"macOS Static INDIA by YSR"];
	
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}


- (void)applicationWillTerminate:(NSNotification *)notification
{
	/* code */
	fprintf(gpFile, "Program is Terminated Successfully\n");

	if (gpFile)
	{
		/* code */
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];

	[window release];

	[super dealloc];
}
@end


@implementation GLView
{
@private
	CVDisplayLinkRef displaylink;

	// for Shaders
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

	//Vao
    GLuint vao_I , vbo_IP,vbo_IC;
    
    GLuint vao_N , vbo_NP,vbo_NC;
    
    GLuint vao_D , vbo_DP,vbo_DC;
    
    GLuint vao_A , vbo_AP,vbo_AC;

    GLuint mvpUniform;
	mat4 Perspective_Projection_Matrix;
}


-(id)initWithFrame:(NSRect)frame;
{
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFAAccelerated,
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		if (pixelFormat == nil)
		{
			fprintf(gpFile, "No valid OpenGL Pixel Format is Available.");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext];
	}
	return(self);
}

//imp
-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return(kCVReturnSuccess);
}


//Init
- (void)prepareOpenGL
{
	fprintf(gpFile, "OpenGL Version	:	%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version 	:	%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	// VERTEX SHADER
	
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;" \
	    "in vec4 vColor;"\
    	"out vec4 out_color;"\
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;"\
		"}";
	
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	
	// Compile
	glCompileShader(gVertexShaderObject);


	// Error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}


	// FRAGMENT SHADER
	
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	
	const GLchar *fragmentShaderSourceCode =
	"#version 410 core" \
	"\n" \
    "precision highp float;"
	"in vec4 out_color;"\
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "FragColor = out_color;" \
    "}";
	
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	
	// Compile
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);


	// Prelinking
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);

				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);

				free(szInfoLog);

				[self release];

				[NSApp terminate:self];

				exit(0);
			}
		}
	}


	// Post Linking
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

    // N
        const GLfloat N_Vertices[] =
        {
            //I
             0.3f, 0.5f, 0.0f,
             0.2f, 0.5f, 0.0f,
             0.2f, -0.5f, 0.0f,
             0.3f, -0.5f, 0.0f,
    
             -0.2f, 0.5f, 0.0f,
             -0.3f, 0.5f, 0.0f,
             -0.3f, -0.5f, 0.0f,
             -0.2f, -0.5f, 0.0f,
    
            //I
             -0.1f, 0.5f, 0.0f,
             -0.22f, 0.5f, 0.0f,
             0.1f, -0.5f, 0.0f,
             0.22f, -0.5f, 0.0f
    
        };
    
        const GLfloat N_Colors[] =
        {     1.0f,0.5f,0.0f,
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
            0.0f,1.0f,0.0f ,
    
            1.0f,0.5f,0.0f,
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
            0.0f,1.0f,0.0f,
    
            1.0f,0.5f,0.0f,
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
            0.0f,1.0f,0.0f
    
        };
    
        //create vao
        glGenVertexArrays(1, &vao_N);
    
        //Binding
        glBindVertexArray(vao_N);
    
    //Pos
        glGenBuffers(1, &vbo_NP);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_NP);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(N_Vertices),
                    N_Vertices,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        //Set Position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Color
        //Buffer
        glGenBuffers(1, &vbo_NC);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_NC);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(N_Colors),
                    N_Colors,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        //Set Position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //vao
        glBindVertexArray(0);
    
    
    
    //    I
    const GLfloat I_Vertices[] =
        {
            0.05f, 0.5f, 0.0f,
            -0.05f, 0.5f, 0.0f,
            -0.05f, -0.5f, 0.0f,
            0.05f, -0.5f, 0.0f
         };
    
        const GLfloat I_Colors[] =
        {     1.0f,0.5f,0.0f,
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
            0.0f,1.0f,0.0f
        };
    
        //create vao
        glGenVertexArrays(1, &vao_I);
    
        //Binding
        glBindVertexArray(vao_I);
    
    //Pos
        glGenBuffers(1, &vbo_IP);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_IP);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(I_Vertices),
                    I_Vertices,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        //Set Position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Color
        //Buffer
        glGenBuffers(1, &vbo_IC);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_IC);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(I_Colors),
                    I_Colors,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        //Set Position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //vao
        glBindVertexArray(0);
    
    
    //D :  vao_D , vbo_DP,vbo_DC
    
    const GLfloat D_Vertices[] =
        {
                  -0.3f, 0.5f, 0.0f,
                   -0.2f, 0.5f, 0.0f,
                   -0.2f, -0.5f, 0.0f,
                   -0.3f, -0.5f, 0.0f,
           
                   // D
                   -0.2f, 0.4f, 0.0f,
                   -0.2f, 0.5f, 0.0f,
                   0.0f, 0.4f, 0.0f,
                   0.0f, 0.5f, 0.0f,
           
                   0.15f, 0.3f, 0.0f,
                   0.25f, 0.36f, 0.0f,
                   0.15f, -0.3f, 0.0f ,
                   0.25f, -0.36f, 0.0f ,
           
                   0.0f, -0.4f, 0.0f ,
                   0.0f, -0.5f, 0.0f ,
                   -0.2f, -0.4f, 0.0f ,
                   -0.2f, -0.5f, 0.0f
        
        };
    
      const GLfloat D_Colors[] =
        {
            1.0f,0.5f,0.0f,
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
            0.0f,1.0f,0.0f,
    
            1.0f,0.5f,0.0f,
            1.0f,0.5f,0.0f,
            1.0f,0.5f,0.0f,
            1.0f,0.5f,0.0f,
    
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
    
            0.0f,1.0f,0.0f,
            0.0f,1.0f,0.0f,
            0.0f,1.0f,0.0f,
            0.0f,0.0f,0.0f //for look
        };
    
        //create vao
        glGenVertexArrays(1, &vao_D);
    
        //Binding
        glBindVertexArray(vao_D);
    
    //Pos
        glGenBuffers(1, &vbo_DP);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_DP);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(D_Vertices),
                    D_Vertices,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        //Set Position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Color
        //Buffer
        glGenBuffers(1, &vbo_DC);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_DC);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(D_Colors),
                    D_Colors,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        //Set Position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //vao
        glBindVertexArray(0);
    
    
    //    A  vao_A , vbo_AP,vbo_AC;
    
        const GLfloat A_Vertices[] =
        {
                // for A
                     1.0f, 0.5f, 0.0f,
                     0.95f, 0.5f, 0.0f,
                     0.0f, -0.5f, 0.0f,
                     0.25f, -0.5f, 0.0f,
                     
                     //
                     1.15f, 0.5f, 0.0f,
                     1.0f, 0.5f, 0.0f,
                     1.05f, -0.5f, 0.0f,
                     1.35f, -0.5f, 0.0f,
            
                    //strips
                     1.1f, 0.1f, 0.0f,
                     0.6f, 0.1f, 0.0f,
                     0.6f, 0.0f, 0.0f,
                     1.1f, 0.0f, 0.0f,
            
                     //W
                    1.1f, 0.06f, 0.0f,
                    0.6f, 0.06f, 0.0f,
                    0.6f, 0.04f, 0.0f,
                    1.1f, 0.04f, 0.0f
            

        };
    
        const GLfloat A_Colors[] =
        {
                    1.0f,0.5f,0.0f,
                    1.0f,1.0f,1.0f,
                    1.0f,1.0f,1.0f,
                    0.0f,1.0f,0.0f,
            
                    //
                    1.0f,0.5f,0.0f,
                    1.0f,1.0f,1.0f,
                    1.0f,1.0f,1.0f,
                    0.0f,1.0f,0.0f,
            
                    
                    //RG
                    1.0f, 0.5f, 0.0f,
                    1.0f, 0.5f, 0.0f,
                    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,
            
                    // W
                    1.0f, 1.0f, 1.0f,
                    1.0f, 1.0f, 1.0f,
                    1.0f, 1.0f, 1.0f,
                    1.0f, 1.0f, 1.0f
        };
    
    
    //create vao
        glGenVertexArrays(1, &vao_A);
    
        glBindVertexArray(vao_A);
    
    //Pos
        glGenBuffers(1, &vbo_AP);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_AP);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(A_Vertices), A_Vertices,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        //Set Position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Color
        //Buffer
        glGenBuffers(1, &vbo_AC);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_AC);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(A_Colors),    A_Colors,GL_STATIC_DRAW);
    
        //most imp
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    
        //unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //vao
        glBindVertexArray(0);
    

    
    
	//Depth
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.2f, 1.0f);

	Perspective_Projection_Matrix = mat4::identity();

	//new
	CVDisplayLinkCreateWithActiveCGDisplays(&displaylink);
	CVDisplayLinkSetOutputCallback(displaylink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displaylink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displaylink);
	
    [super prepareOpenGL];
}

//Resize
- (void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat Width = rect.size.width;
	GLfloat Height = rect.size.height;

	if (Height == 0)
		Height = 1;

	glViewport(0, 0,(GLsizei)Width,(GLsizei)Height);

    Perspective_Projection_Matrix = vmath::perspective(45.0f,Width/Height,0.1f,100.0f);
    
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

- (void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
}

//Display
- (void)drawView
    {
        [[self openGLContext]makeCurrentContext];
        
        CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //
        mat4 ModelView_Matrix,ModelView_Projection_Matrix;
        
    // I
        //2 : I[]
        ModelView_Matrix = mat4::identity();
        ModelView_Projection_Matrix = mat4::identity();
    
        //3 : Transforms
        ModelView_Matrix = translate(-2.5f, 0.0f, -5.0f);
    
        //4 : Do Mat mul
        ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;
    
        // 5 : send it to shader
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);
    
        // 6 : bind to vao
        glBindVertexArray(vao_I);
    
        //8 : draw scene
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
        //9 : Unbind vao
        glBindVertexArray(0);
    
    
    //    N
    //2 : I[]
        ModelView_Matrix = mat4::identity();
        ModelView_Projection_Matrix = mat4::identity();
    
        //3 : Transforms
        ModelView_Matrix = translate(-1.2f, 0.0f, -5.0f);
    
        //4 : Do Mat mul
        ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;
    
        // 5 : send it to shader
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);
    
        // 6 : bind to vao
        glBindVertexArray(vao_N);
    
        //8 : draw scene
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    
        //9 : Unbind vao
        glBindVertexArray(0);
    
    
    //    D vao_D
    //2 : I[]
        ModelView_Matrix = mat4::identity();
        ModelView_Projection_Matrix = mat4::identity();
    
        //3 : Transforms
        ModelView_Matrix = translate(0.2f, 0.0f, -5.0f);
    
        //4 : Do Mat mul
        ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;
    
        // 5 : send it to shader
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);
    
        // 6 : bind to vao
        glBindVertexArray(vao_D);
    
        //8 : draw scene : Imp
    
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        // glDrawArrays(GL_QUAD_STRIP, 4,12);
    
        glDrawArrays(GL_TRIANGLE_STRIP, 4,12);

        
        
        //9 : Unbind vao
        glBindVertexArray(0);
    
    
    // I    I
    //2 : I[]
        ModelView_Matrix = mat4::identity();
        ModelView_Projection_Matrix = mat4::identity();
    
        //3 : Transforms
        ModelView_Matrix = translate(1.4f, 0.0f, -5.0f);
    
        //4 : Do Mat mul
        ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;
    
        // 5 : send it to shader
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);
    
        // 6 : bind to vao
        glBindVertexArray(vao_I);
    
        //8 : draw scene
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
        //9 : Unbind vao
        glBindVertexArray(0);
    
    
    // A : Vao_A
        mat4 scaleA , rotateA;
    
    // 1
        scaleA = mat4::identity();
        rotateA = mat4::identity();
        ModelView_Matrix = mat4::identity();
        ModelView_Projection_Matrix = mat4::identity();
    
        //3 : Transforms
        ModelView_Matrix = translate(2.0f,0.0f,-5.6f);
    
        ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;
    
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);
    
        glBindVertexArray(vao_A);
    
        //A
        glDrawArrays(GL_TRIANGLE_FAN, 0,4); //QUADS
        
        glDrawArrays(GL_TRIANGLE_FAN, 4,4);

        glDrawArrays(GL_TRIANGLE_FAN, 8,4);
        
        glDrawArrays(GL_TRIANGLE_FAN, 12,4);
        
        glBindVertexArray(0);
    
    
        glUseProgram(gShaderProgramObject);
        
        CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
        CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    }


-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}


-(void)keyDown:(NSEvent *)theEvent
{
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[self release];
			[NSApp terminate:self];
			break;

		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repaint
			break;

        default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	/* code */
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	/* code */
}

//Uninit
-(void) dealloc
{
	
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	
	gShaderProgramObject = 0;
	
	glUseProgram(0);

	CVDisplayLinkStop(displaylink);
	CVDisplayLinkRelease(displaylink);

	[super dealloc];
}

@end


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displaylink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagOut, void *pDisplayLingContext)
{
	CVReturn result = [(GLView *)pDisplayLingContext getFrameForTime:pOutputTime];
	return(result);
}

