
/*
	Blue Window
*/

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface MyView : NSView
@end

// Entry point
int main(int argc, const char* argv[]) 
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication sharedApplication];
	
	[NSApp setDelegate : [[AppDelegate alloc]init]];
	
	[NSApp run];
	
	[pPool release];
	
	return (0);
}

// interface implemetations
@implementation AppDelegate 
{
@private 
	NSWindow *window;
	MyView *view;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification 
{

	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
	
	// create simple window
	window = [[NSWindow alloc] initWithContentRect:win_rect
										styleMask:NSWindowStyleMaskTitled |
										NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable |
										NSWindowStyleMaskResizable
										backing:NSBackingStoreBuffered
										defer:NO];
	
	[window setTitle:@"1st macOS Window By YSR"];
	[window center];
	
	view = [[MyView alloc]initWithFrame:win_rect];
	
	[window setContentView:view];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification 
{
	// code
}

- (void)windowWillClose:(NSNotification *)notification 
{

	[NSApp terminate:self];
}

//dest
- (void)dealloc 
{
	[view release];
	
	[window release];
	
	[super dealloc];
}
@end

@implementation MyView 
{
	NSString *centralText;
}

- (id)initWithFrame:(NSRect)frame;
{
	self = [super initWithFrame:frame];
	
	if (self) 
	{
		[[self window]setContentView:self];
		
		centralText = @"Hello Mac World!!!";
	}
	return (self);
}


//Painting
- (void)drawRect:(NSRect)dirtyRect 
{
	// black background
	NSColor *fillColor = [NSColor blackColor];

	[fillColor set];

	NSRectFill(dirtyRect);
	
	// dictionary with key-Value
	NSDictionary *dictionaryForTextAttributes = [NSDictionary 
		dictionaryWithObjectsAndKeys:
											[NSFont fontWithName:@"Helvetica" 
							size:32], NSFontAttributeName,
												[NSColor greenColor],
							NSForegroundColorAttributeName, 
												nil];
												
	NSSize textSize = [centralText sizeWithAttributes:dictionaryForTextAttributes];
	
	NSPoint point;
	point.x = (dirtyRect.size.width / 2) - (textSize.width / 2);
	point.y = (dirtyRect.size.height / 2) - (textSize.height / 2) + 12;
	
	[centralText drawAtPoint:point withAttributes:dictionaryForTextAttributes];
}

//imp
-(BOOL)acceptsFirstResponder 
{
	[[self window]makeFirstResponder:self];
	
	return (YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	int key = (int)[[theEvent characters]characterAtIndex:0];
	
	switch(key)
	{
		case 27:
			[self release];
			[NSApp terminate:self];
			break;
			
		case 'F':
		case 'f':
			centralText=@"'F' or 'f' Key is Pressed";
			[[self window]toggleFullScreen:self];	// repainting
			break;
			
		default:
			break;
	}
}

//LButton
-(void)mouseDown:(NSEvent *)theEvent
{
	centralText = @"Left Mouse Button is Clicked";
	[self setNeedsDisplay:YES];			// repainting
}


-(void)mouseDragged:(NSEvent *)theEvent
{
	// code
}

//RButton
-(void)rightMouseDown:(NSEvent *)theEvent
{
	centralText = @"Right Mouse Button is Clicked";
	[self setNeedsDisplay:YES];			// repainting
}

-(void)dealloc
{
	[super dealloc];
}

@end

