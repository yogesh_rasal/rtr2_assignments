
/* 
   Sphere using Shaders
*/

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
#import "YSR_Sphere.h"

using namespace vmath;


// 'C' style global function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags , CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;
float AngleRect = 0.0f;

// interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end


// Entry point 
int main(int argc, const char *argv[])
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate :[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];
	
	return(0);
}


// interface
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	/* 
		Log file : 5 steps
		
	*/
	
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];

	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];

	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

	gpFile = fopen(pszLogFileNameWithPath, "w");

	if(gpFile == NULL)
	{
		printf("Can Not Create Log File.\n");
		[self release];
		[NSApp terminate:self];
	}
	
	fprintf(gpFile, "Program Started Successfully \n");

	/* create window */
	
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	window = [[NSWindow alloc]initWithContentRect:win_rect 
		styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | 
		NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
		backing:NSBackingStoreBuffered defer:NO];
	
	[window setTitle:@"macOS : Diffuse Light on Cube by YSR"];
	
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}


- (void)applicationWillTerminate:(NSNotification *)notification
{
	/* code */
	fprintf(gpFile, "Program is Terminated Successfully\n");

	if (gpFile)
	{
		/* code */
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];

	[window release];

	[super dealloc];
}
@end


@implementation GLView
{
@private
	CVDisplayLinkRef displaylink;

	// for Shaders
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

    GLuint Vao_cube;
    GLuint Vbo_cube_position;
    GLuint Vbo_cube_normal;

    
    //New
    GLuint Model_View_Uniform;
    GLuint Projection_Uniform;
    GLuint LD_Uniform;
    GLuint KD_Uniform;
    GLuint Light_Position_Uniform;
    GLuint LisPressed_Uniform;
    
    
    bool bisLPressed ;
    bool bLighting ;

    int gNumVertices , gNumElements;
    
    GLuint mvpUniform;
	mat4 PerspectProjectionMatrix;
}


-(id)initWithFrame:(NSRect)frame;
{
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFAAccelerated,
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		if (pixelFormat == nil)
		{
			fprintf(gpFile, "No valid OpenGL Pixel Format is Available.");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext];
	}
	return(self);
}

//imp
-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return(kCVReturnSuccess);
}


//Init
- (void)prepareOpenGL
{
	fprintf(gpFile, "OpenGL Version	:	%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version 	:	%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	// VERTEX SHADER
	
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;"                        \
    "in vec3 vNormal;"                            \
    "out vec3 diffuse_Color;"                    \
    "\n"                                        \
    "uniform mat4 u_mv_matrix;"                    \
    "uniform mat4 u_projection_matrix;"            \
    "uniform int u_LKeyPressed;"                \
    "uniform vec3 u_LD;"                        \
    "uniform vec3 u_KD;"                        \
    "uniform vec4 u_Light_Position;"            \
    "void main(void)"                            \
    "{"                                            \
    "if(u_LKeyPressed == 1)"                    \
    "{"                                            \
    "vec4 Eye_Coordinates = u_mv_matrix * vPosition;"    \
    "mat3 Normal_Matrix = mat3(transpose(inverse(u_mv_matrix)));"    \
    "vec3 T_Norm = normalize(Normal_Matrix * vNormal);"    \
    "vec3 S = vec3(u_Light_Position) - (Eye_Coordinates.xyz);" \
    "diffuse_Color = u_LD * u_KD * dot(S,T_Norm);"  \
    "}"                                            \
    "gl_Position = u_projection_matrix * u_mv_matrix * vPosition;"    \
    "}" ;

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	
	// Compile
	glCompileShader(gVertexShaderObject);


	// Error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}


	// FRAGMENT SHADER
	
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	
	const GLchar *fragmentShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "precision highp float;"
        "in vec3 diffuse_Color;"	\
		"out vec4 FragColor;"		\
		"uniform int u_LKeyPressed;"\
		"\n"						\
		"void main(void)"			\
		"{"							\
		"if(u_LKeyPressed == 1)"	\
		"{"							\
		"FragColor = vec4(diffuse_Color,1.0);"		\
		"}"							\
		"else"						\
		"{"\
		"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		"}"\
		"}" ;
    
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	
	// Compile
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);


	// Prelinking
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);

				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);

				free(szInfoLog);

				[self release];

				[NSApp terminate:self];

				exit(0);
			}
		}
	}


	// Post Linking
    mvpUniform = glGetUniformLocation(gShaderProgramObject,"u_mv_matrix");
    
    Projection_Uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
    
    KD_Uniform = glGetUniformLocation(gShaderProgramObject, "u_KD");
    
    Light_Position_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Light_Position");
    
    LD_Uniform = glGetUniformLocation(gShaderProgramObject, "u_LD");
    
    LisPressed_Uniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");

    
    //cube
        const GLfloat cubeVertices[] =
        {
            1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
    
            1.0f, 1.0f, -1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,
            
            -1.0f, 1.0f, -1.0f,
            1.0f, 1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            
            -1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f,
            
            -1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,
            
            1.0f, -1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            -1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f
        };
    
        glGenVertexArrays(1, &Vao_cube);
        glBindVertexArray(Vao_cube);
    
        glGenBuffers(1, &Vbo_cube_position);
        glBindBuffer(GL_ARRAY_BUFFER, Vbo_cube_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
    
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Lights - AMC_ATTRIBUTE_NORMAL
        const GLfloat CubeNormals[] =
        {
            0.0f,0.0f,1.0f,
            0.0f,0.0f,1.0f,
            0.0f,0.0f,1.0f,
            0.0f,0.0f,1.0f,
            
            1.0f,0.0f,0.0f,
            1.0f,0.0f,0.0f,
            1.0f,0.0f,0.0f,
            1.0f,0.0f,0.0f,
            
            0.0f,0.0f,-1.0f,
            0.0f,0.0f,-1.0f,
            0.0f,0.0f,-1.0f,
            0.0f,0.0f,-1.0f,
            
            -1.0f,0.0f,0.0f,
            -1.0f,0.0f,0.0f,
            -1.0f,0.0f,0.0f,
            -1.0f,0.0f,0.0f,
            
            0.0f,1.0f,0.0f,
            0.0f,1.0f,0.0f,
            0.0f,1.0f,0.0f,
            0.0f,1.0f,0.0f,
            
            0.0f,-1.0f,0.0f,
            0.0f,-1.0f,0.0f,
            0.0f,-1.0f,0.0f,
            0.0f,-1.0f,0.0f
        };
    
        //imp
        glGenBuffers(1, &Vbo_cube_normal);
        glBindBuffer(GL_ARRAY_BUFFER, Vbo_cube_normal);
        glBufferData(GL_ARRAY_BUFFER, sizeof(CubeNormals), CubeNormals, GL_STATIC_DRAW);
    
        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //common
        glBindVertexArray(0); //vbo
        

	//Depth
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.1f, 0.0f, 0.0f, 1.0f);

	PerspectProjectionMatrix = mat4::identity();

	//new
	CVDisplayLinkCreateWithActiveCGDisplays(&displaylink);
	CVDisplayLinkSetOutputCallback(displaylink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displaylink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displaylink);
	
    [super prepareOpenGL];
}

//Resize
- (void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat Width = rect.size.width;
	GLfloat Height = rect.size.height;

	if (Height == 0)
		Height = 1;

	glViewport(0, 0,(GLsizei)Width,(GLsizei)Height);

    PerspectProjectionMatrix = vmath::perspective(45.0f,Width/Height,0.1f,100.0f);
    
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

- (void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
}

//Display
- (void)drawView
{
	[[self openGLContext]makeCurrentContext];
	
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
	
	mat4 ModelViewMatrix;
	mat4 ModelViewProjectionMatrix;
	mat4 translateMatrix;
    mat4 Rotation_Matrix;

	//  identity
	ModelViewMatrix = mat4::identity();
	ModelViewProjectionMatrix = mat4::identity();
    translateMatrix = mat4::identity();
    Rotation_Matrix = mat4::identity();

    //new
    translateMatrix = vmath::translate(0.0f,0.0f,-6.0f);

    Rotation_Matrix = rotate(AngleRect, 0.0f, 1.0f, 0.0f);

    ModelViewMatrix = translateMatrix * Rotation_Matrix;

	// Matrix Multiplication
	ModelViewProjectionMatrix = PerspectProjectionMatrix * ModelViewMatrix;

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelViewMatrix);
    
    glUniformMatrix4fv(Projection_Uniform, 1, GL_FALSE, PerspectProjectionMatrix);
    
    //imp
    if (bLighting == true)
        {
            glUniform1i(LisPressed_Uniform, 1);
            glUniform3f(LD_Uniform, 1.0f, 1.0f, 1.0f);
            glUniform3f(KD_Uniform, 0.25f, 0.25f, 0.25f);
    
            //imp : out to in Light
            glUniform4f(Light_Position_Uniform, 10.0f, 0.0f, 5.0f, 1.0f);
        }
    
        else
        {
            glUniform1i(LisPressed_Uniform, 0);
        }

    //Scene
    
    
    // 6 : bind to vao
    glBindVertexArray(Vao_cube);

    //8 : draw scene

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);


        //9 : Unbind vao
    glBindVertexArray(NULL);
    
    //common
	glUseProgram(0);

    AngleRect += 0.5f;

    if (AngleRect >= 360.0f)
        AngleRect -= 360.0f;



	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}


-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}


-(void)keyDown:(NSEvent *)theEvent
{
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[self release];
			[NSApp terminate:self];
			break;

		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repaint
			break;

        case 'L':
        case 'l':
            if (!bLighting)
            {
                bLighting = true;
                bisLPressed = true;
            }
            else
            {
                bLighting = false;
                bisLPressed = false;
            }
            break;
        
        default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	/* code */
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	/* code */
}

//Uninit
-(void) dealloc
{
	

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	
	gShaderProgramObject = 0;
	
	glUseProgram(0);

	CVDisplayLinkStop(displaylink);
	CVDisplayLinkRelease(displaylink);

	[super dealloc];
}

@end


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displaylink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagOut, void *pDisplayLingContext)
{
	CVReturn result = [(GLView *)pDisplayLingContext getFrameForTime:pOutputTime];
	return(result);
}

