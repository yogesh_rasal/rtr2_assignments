
/* 
   Sphere using Shaders
*/

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
#import "YSR_Sphere.h"

using namespace vmath;


// 'C' style global function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags , CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;

// for Shaders
GLuint gVertex_Shader_Object;
GLuint gFragment_Shader_Object;

GLuint gShader_Program_Object_Vert;
GLuint gShader_Program_Object_Frag;

// interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end


// Entry point 
int main(int argc, const char *argv[])
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate :[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];
	
	return(0);
}


// interface
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	/* 
		Log file : 5 steps
		
	*/
	
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];

	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];

	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

	gpFile = fopen(pszLogFileNameWithPath, "w");

	if(gpFile == NULL)
	{
		printf("Can Not Create Log File.\n");
		[self release];
		[NSApp terminate:self];
	}
	
	fprintf(gpFile, "Program Started Successfully \n");

	/* create window */
	
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	window = [[NSWindow alloc]initWithContentRect:win_rect 
		styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | 
		NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
		backing:NSBackingStoreBuffered defer:NO];
	
	[window setTitle:@"macOS : Light Toggling on Sphere by YSR"];
	
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}


- (void)applicationWillTerminate:(NSNotification *)notification
{
	/* code */
	fprintf(gpFile, "Program is Terminated Successfully\n");

	if (gpFile)
	{
		/* code */
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];

	[window release];

	[super dealloc];
}
@end


@implementation GLView
{
@public
	CVDisplayLinkRef displaylink;


    //12 Uniforms
    GLuint Model_Uniform, Model_Uniform_Frag;
    GLuint View_Uniform, View_Uniform_Frag;
    GLuint Projection_Uniform, Projection_Uniform_Frag;
    
    GLuint LD_Uniform, LA_Uniform, LS_Uniform;
    GLuint KD_Uniform, KA_Uniform, KS_Uniform;
    
    GLuint Material_Shininess;
    GLuint Light_Position_Uniform;
    
    //Fragment
    GLuint mUniform_Frag;
    GLuint LD_Uniform_Frag, LA_Uniform_Frag, LS_Uniform_Frag;
    GLuint KD_Uniform_Frag, KA_Uniform_Frag, KS_Uniform_Frag;
    
    GLuint Material_Shininess_Frag;
    GLuint Light_Position_Uniform_Frag;
    GLuint LisPressed_Uniform , LisPressed_Uniform_Frag;

    
    //bool
    bool bVertex,bFragment;
    bool bisLPressed ;
    bool bLighting ;
    
    GLuint mvpUniform;
	mat4 PerspectProjectionMatrix;
}


-(id)initWithFrame:(NSRect)frame;
{
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFAAccelerated,
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		if (pixelFormat == nil)
		{
			fprintf(gpFile, "No valid OpenGL Pixel Format is Available.");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext];
	}
	return(self);
}

//imp
-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return(kCVReturnSuccess);
}

//new
- (void) PerVertex
{
    //code
    /*
    Add 6 Uniforms for Light
    */

    //For Shaders : create
    gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

    //Define VS code
    GLchar *VertexShaderCode =
        "#version 410 core"                            \
        "\n"                                        \
        "in vec4 vPosition;"                        \
        "in vec3 vNormal;"                            \
        "out vec3 Phong_ADS_Light;"                    \
        "\n"                                        \
        "uniform mat4 u_model_matrix;"                \
        "uniform mat4 u_view_matrix;"                \
        "uniform mat4 u_projection_matrix;"            \
        "uniform vec3 u_LA;"                        \
        "uniform vec3 u_LD;"                        \
        "uniform vec3 u_LS;"                        \
        "uniform vec3 u_KA;"                        \
        "uniform vec3 u_KD;"                        \
        "uniform vec3 u_KS;"                        \
        "uniform float u_Shininess;"                \
        "uniform int u_LKeyPressed;"                \
        "uniform vec4 u_Light_Position;"            \
        "\n"                                        \
        "void main(void)"                            \
        "{"                                            \
        "if(u_LKeyPressed == 1)"                    \
        "{"                                            \
        "vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"        \
        "vec3 T_Norm = normalize(mat3( u_view_matrix * u_model_matrix)* vNormal);"    \
        "vec3 Light_Direction = normalize(vec3(u_Light_Position) - (Eye_Coordinates.xyz));"    \
        "float TN_Dot_LD = max(dot(Light_Direction,T_Norm), 0);"\
        "vec3 Reflecion_Vector = reflect(-Light_Direction, T_Norm);"\
        "vec3 Viewer_Vector = normalize(-Eye_Coordinates.xyz);"        \
        "vec3 Ambient = vec3(u_LA * u_KA);"                \
        "vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);" \
        "vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflecion_Vector, Viewer_Vector), 0.0), u_Shininess));" \
        "Phong_ADS_Light = Ambient + Diffuse + Specular;" \
        "}"        \
        "else"    \
        "{"        \
        "Phong_ADS_Light = vec3(1.0,1.0,1.0);"    \
        "}"        \
        "gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"    \
        "}";

    //Specify code to Obj
    glShaderSource(gVertex_Shader_Object,
        1,
        (const GLchar**)&VertexShaderCode,
        NULL);

    
    // Error checking
    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    char *szInfoLog = NULL;
    GLint iProgrmLinkStatus = 0;


    //Compile the shader
    glCompileShader(gVertex_Shader_Object);

    //Error Code for VS
    glGetShaderiv(gVertex_Shader_Object,
        GL_COMPILE_STATUS,
        &iShaderCompileStatus);


    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertex_Shader_Object,
            GL_INFO_LOG_LENGTH,
            &iInfoLogLength);

        if (iInfoLogLength > 0) //Error found
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei Written;
                glGetShaderInfoLog(gVertex_Shader_Object, iInfoLogLength, &Written, szInfoLog);

                fprintf(gpFile, "\n %s Failed in Vertex Shader", szInfoLog);
                free(szInfoLog);
                szInfoLog = NULL;
                
                [self release];

                [NSApp terminate:self];

                exit(0);
            }
        }
    }


    
    //2 - FS
    gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

    //Define FS code
    GLchar *FragmentShaderCode =
        "#version 410 core"            \
        "\n"                        \
        "in vec3 Phong_ADS_Light;"    \
        "out vec4 FragColor;"        \
        "uniform int u_LKeyPressed;"\
        "\n"                        \
        "void main(void)"            \
        "{"                            \
        "if(u_LKeyPressed == 1)"    \
        "{"                            \
        "FragColor = vec4(Phong_ADS_Light,1.0);"        \
        "}"                            \
        "else"                        \
        "{"                            \
        "FragColor = vec4(1.0,1.0,1.0,1.0);" \
        "}"                            \
        "}";

    //Specify code to Obj
    glShaderSource(gFragment_Shader_Object, 1,
        (GLchar**)&FragmentShaderCode, NULL);

    //Compile the shader
    glCompileShader(gFragment_Shader_Object);


    //Error check for FS
    glGetShaderiv(gFragment_Shader_Object,
        GL_COMPILE_STATUS,
        &iShaderCompileStatus);

    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragment_Shader_Object,
            GL_INFO_LOG_LENGTH,
            &iInfoLogLength);

        if (iInfoLogLength > 0) //Error found
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei Written;
                glGetShaderInfoLog(gFragment_Shader_Object, iInfoLogLength, &Written, szInfoLog);

                fprintf(gpFile, "\n  Failed in Fragment Shader : %s", szInfoLog);
                free(szInfoLog);
                szInfoLog = NULL;

                [self release];
                [NSApp terminate:self];
                
                exit(0);
            }
        }
    }


    //3 - Create Shader Program
    gShader_Program_Object_Vert = glCreateProgram();

    //attach VS & FS
    glAttachShader(gShader_Program_Object_Vert, gVertex_Shader_Object);

    glAttachShader(gShader_Program_Object_Vert, gFragment_Shader_Object);

    //prelink
    glBindAttribLocation(gShader_Program_Object_Vert, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShader_Program_Object_Vert, AMC_ATTRIBUTE_NORMAL, "vNormal");

    //Link SP
    glLinkProgram(gShader_Program_Object_Vert);


    // Error check for SP
    glGetProgramiv(gShader_Program_Object_Vert, GL_LINK_STATUS, &iProgrmLinkStatus);


    if (iProgrmLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShader_Program_Object_Vert, GL_INFO_LOG_LENGTH, &iInfoLogLength);

        if (iInfoLogLength > 0) //Error found
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei Written;
                glGetProgramInfoLog(gShader_Program_Object_Vert, iInfoLogLength, &Written, szInfoLog);

                fprintf(gpFile, "\n  Failed in Shader Program Linking , %s", szInfoLog);
                free(szInfoLog);
                szInfoLog = NULL;
                
                [self release];
                [NSApp terminate:self];
                
                exit(0);
            }
        }
    }

  
 //postlink
    Model_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_model_matrix");
    Projection_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_projection_matrix");
    View_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_view_matrix");

    LD_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_LD");
    KD_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_KD");

    LA_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_LA");
    KA_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_KA");

    LS_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_LS");
    KS_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_KS");

    LisPressed_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_LKeyPressed");
    Light_Position_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_Light_Position");
    Material_Shininess = glGetUniformLocation(gShader_Program_Object_Vert, "u_Shininess");

}


//new
- (void) PerFragment
{
    // Error checking
    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    char *szInfoLog = NULL;
    GLint iProgrmLinkStatus = 0;

    
    /*
    Add 12 Uniforms for Light
    */

    //For Shaders : create
    gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

    //Define VS code
    GLchar *VertexShaderCode =
        "#version 410 core"                            \
        "\n"                                        \
        "in vec4 vPosition;"                        \
        "in vec3 vNormal;"                            \
        "out vec3 T_Norm;"                            \
        "out vec3 Light_Direction;"                    \
        "out vec3 Viewer_Vector;"                    \
        "\n"                                        \
        "uniform mat4 u_model_matrix;"                \
        "uniform mat4 u_view_matrix;"                \
        "uniform mat4 u_projection_matrix;"            \
        "uniform vec3 u_LA;"                        \
        "uniform vec3 u_LD;"                        \
        "uniform vec3 u_LS;"                        \
        "uniform vec3 u_KA;"                        \
        "uniform vec3 u_KD;"                        \
        "uniform vec3 u_KS;"                        \
        "uniform float u_Shininess;"                \
        "uniform int u_LKeyPressed;"                \
        "uniform vec4 u_Light_Position;"            \
        "\n"                                        \
        "void main(void)"                            \
        "{"                                            \
        "vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"    \
        "T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"            \
        "Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"    \
        "Viewer_Vector = vec3(-Eye_Coordinates.xyz);"\
        "gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"    \
        "}";

    
    
    //Specify code to Obj
    glShaderSource(gVertex_Shader_Object,
        1,
        (const GLchar**)&VertexShaderCode,
        NULL);

    //Compile the shader
    glCompileShader(gVertex_Shader_Object);


    //Error Code for VS
    glGetShaderiv(gVertex_Shader_Object,
        GL_COMPILE_STATUS,
        &iShaderCompileStatus);


    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertex_Shader_Object,
            GL_INFO_LOG_LENGTH,
            &iInfoLogLength);

        if (iInfoLogLength > 0) //Error found
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei Written;
                glGetShaderInfoLog(gVertex_Shader_Object, iInfoLogLength, &Written, szInfoLog);

                fprintf(gpFile, "\n %s Failed in Vertex Shader", szInfoLog);
                free(szInfoLog);
                szInfoLog = NULL;
                [self release];
                [NSApp terminate:self];
                
                exit(0);
            }
        }
    }


    //2 - FS
    gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

    //Define FS code : "precision highp float;"
    GLchar *FragmentShaderCode =
        "#version 410 core"            \
        "\n"                        \
        "in vec3 T_Norm;"            \
        "in vec3 Light_Direction;"    \
        "in vec3 Viewer_Vector;"    \
        "out vec4 FragColor;"        \
        "\n"                        \
        "uniform vec3 u_LA;"        \
        "uniform vec3 u_LD;"        \
        "uniform vec3 u_LS;"        \
        "uniform vec3 u_KA;"        \
        "uniform vec3 u_KD;"        \
        "uniform vec3 u_KS;"        \
        "uniform float u_Shininess;"    \
        "uniform int u_LKeyPressed;"    \
        "uniform vec4 u_Light_Position;"\
        "\n"                            \
        "void main(void)"            \
        "{"                            \
        "if(u_LKeyPressed == 1)"    \
        "{"                            \
        "vec3 Normalized_View_Vector = normalize(Viewer_Vector);"    \
        "vec3 Normalized_Light_Direction = normalize(Light_Direction);"\
        "vec3 Normalized_TNorm = normalize(T_Norm);"\
        "float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0);"    \
        "vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm);"\
        "vec3 Ambient = vec3(u_LA * u_KA);"    \
        "vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);"    \
        "vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess));" \
        "vec3 Phong_ADS_Light = Ambient + Diffuse+ Specular;"    \
        "FragColor = vec4(Phong_ADS_Light,1.0);" \
        "}"                            \
        "else"                        \
        "{"                            \
        "FragColor = vec4(1.0,1.0,1.0,1.0);" \
        "}"                            \
        "}";


    //Specify code to Obj
    glShaderSource(gFragment_Shader_Object, 1,
        (GLchar**)&FragmentShaderCode, NULL);

    //Compile the shader
    glCompileShader(gFragment_Shader_Object);


    //Error check for FS
    glGetShaderiv(gFragment_Shader_Object,
        GL_COMPILE_STATUS,
        &iShaderCompileStatus);

    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragment_Shader_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);

        if (iInfoLogLength > 0) //Error found
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei Written;
                glGetShaderInfoLog(gFragment_Shader_Object, iInfoLogLength, &Written, szInfoLog);

                fprintf(gpFile, "\n  Failed in Fragment Shader : %s", szInfoLog);
                free(szInfoLog);
                szInfoLog = NULL;
                [self release];
                [NSApp terminate:self];
                
                exit(0);
            }
        }
    }


    //3 - Create Shader Program
    gShader_Program_Object_Frag = glCreateProgram();

    //attach VS & FS
    glAttachShader(gShader_Program_Object_Frag, gVertex_Shader_Object);

    glAttachShader(gShader_Program_Object_Frag, gFragment_Shader_Object);

    //prelink
    glBindAttribLocation(gShader_Program_Object_Frag, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShader_Program_Object_Frag, AMC_ATTRIBUTE_NORMAL, "vNormal");


    //Link SP
    glLinkProgram(gShader_Program_Object_Frag);


    // Error check for SP
    glGetProgramiv(gShader_Program_Object_Frag, GL_LINK_STATUS, &iProgrmLinkStatus);


    if (iProgrmLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShader_Program_Object_Frag, GL_INFO_LOG_LENGTH, &iInfoLogLength);

        if (iInfoLogLength > 0) //Error found
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei Written;
                glGetProgramInfoLog(gShader_Program_Object_Frag, iInfoLogLength, &Written, szInfoLog);

                fprintf(gpFile, "\n  Failed in Shader Program Linking , %s", szInfoLog);
                free(szInfoLog);
                szInfoLog = NULL;

                [self release];
                [NSApp terminate:self];
                
                exit(0);
            }
        }
    }
 
 
    //postlink
    mUniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_model_matrix");
    Projection_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_projection_matrix");
    View_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_view_matrix");

    LD_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_LD");
    KD_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_KD");

    LA_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_LA");
    KA_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_KA");

    LS_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_LS");
    KS_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_KS");

    Light_Position_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_Light_Position");
    Material_Shininess_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_Shininess");

    //common
    LisPressed_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_LKeyPressed");
}



//Init
- (void)prepareOpenGL
{
	fprintf(gpFile, "OpenGL Version	:	%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version 	:	%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	
	    //most imp calls
     [self PerVertex]; //must be done
    
     [self PerFragment];


    //call
    makeSphere(2.0,30,30);

	//Depth
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.1f, 0.0f, 0.0f, 1.0f);

	PerspectProjectionMatrix = mat4::identity();

	//new
	CVDisplayLinkCreateWithActiveCGDisplays(&displaylink);
	CVDisplayLinkSetOutputCallback(displaylink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displaylink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displaylink);
	
    [super prepareOpenGL];
}

//Resize
- (void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat Width = rect.size.width;
	GLfloat Height = rect.size.height;

	if (Height == 0)
		Height = 1;

	glViewport(0, 0,(GLsizei)Width,(GLsizei)Height);

    PerspectProjectionMatrix = vmath::perspective(45.0f,Width/Height,0.1f,100.0f);
    
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

- (void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
}

//Display
- (void)drawView
{
	[[self openGLContext]makeCurrentContext];
	
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    mat4 Model_Matrix;
    mat4 View_Matrix;
    mat4 Projection_Matrix;
    mat4 Translation_Matrix;
    mat4 Rotation_Matrix;
    mat4 ModelView_Projection_Matrix;
	
	//2 : I[]
	Model_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	View_Matrix = mat4::identity();
	Rotation_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//4 : Do Mat mul
	Translation_Matrix = translate(0.0f, 0.0f, -7.0f);

	Model_Matrix = Translation_Matrix * Rotation_Matrix;

    glUseProgram(gShader_Program_Object_Vert);
        
    
         if (bFragment)
        {
            glUseProgram(gShader_Program_Object_Frag);
        }

    
	// 5	: send it to shader

    glUniformMatrix4fv(Model_Uniform, 1, GL_FALSE, Model_Matrix);
    
	glUniformMatrix4fv(Projection_Uniform, 1, GL_FALSE, PerspectProjectionMatrix);
    
	glUniformMatrix4fv(View_Uniform, 1, GL_FALSE, View_Matrix);
	
	//imp
        if (bLighting == true)
        {
    
         if(bVertex == true)
          {
            glUniform1i(LisPressed_Uniform, 1);
            glUniform3f(LD_Uniform, 0.85f, 0.85f, 0.85f);
            glUniform3f(KD_Uniform, 1.0f, 1.0f, 1.0f);
    
            glUniform3f(LS_Uniform, 0.85f, 0.85f, 0.85f);
            glUniform3f(KS_Uniform, 1.0f, 1.0f, 1.0f);
            
            glUniform3f(LA_Uniform, 0.25f, 0.25f, 0.25f);
            glUniform3f(KA_Uniform, 0.25f, 0.25f, 0.25f);
            
            glUniform1f(Material_Shininess, 50.0f);
            
        //imp : out to in Light
            glUniform4f(Light_Position_Uniform, 125.0f, 125.0f, 125.0f, 1.0f);
    
         }
    
            else if (bFragment == true)
            {
                glUniform1i(LisPressed_Uniform_Frag, 1);
                glUniform3f(LD_Uniform_Frag, 0.85f, 0.85f, 0.85f);
                glUniform3f(KD_Uniform_Frag, 1.0f, 1.0f, 1.0f);
    
                glUniform3f(LS_Uniform_Frag, 0.85f, 0.85f, 0.85f);
                glUniform3f(KS_Uniform_Frag, 1.0f, 1.0f, 1.0f);
    
                glUniform3f(LA_Uniform_Frag, 0.25f, 0.25f, 0.25f);
                glUniform3f(KA_Uniform_Frag, 0.25f, 0.25f, 0.25f);
    
                glUniform1f(Material_Shininess_Frag, 128.0f);
    
                glUniform4f(Light_Position_Uniform_Frag, 125.0f, 125.0f, 125.0f, 1.0f);
    
                glUniformMatrix4fv(mUniform_Frag, 1, GL_FALSE, Model_Matrix);
                glUniformMatrix4fv(Projection_Uniform_Frag, 1, GL_FALSE, PerspectProjectionMatrix);
                glUniformMatrix4fv(View_Uniform_Frag, 1, GL_FALSE, View_Matrix);
            }
        }
    
        else
        {
            glUniform1i(LisPressed_Uniform, 0);
        }
    
    
    //Scene
    
    draw();
    
	//common
	glUseProgram(0);


	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}


-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}


-(void)keyDown:(NSEvent *)theEvent
{
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[self release];
			[NSApp terminate:self];
			break;

		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repaint
			break;

        case 'L':
        case 'l':
            if (!bLighting)
            {
                bLighting = true;
                bisLPressed = true;
            }
            else
            {
                bLighting = false;
                bisLPressed = false;
            }
            break;
               
        case 'c':
        case 'C':
                    if (bFragment == false)
                    {
                        bFragment = true;
                        bVertex = false;
        
                    }
                    else
                    {
                        bFragment = false;
                        bVertex = true;
        
                    }
                    break;
        
        case 'V':
        case 'v':
                    if (bVertex == false)
                    {
                        bVertex = true;
                        bFragment = false;
        
                    }
                    else
                    {
                        bVertex = false;
                        bFragment = true;
        
                    }
                    break;
            
        default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	/* code */
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	/* code */
}

//Uninit
-(void) dealloc
{
	

	glUseProgram(gShader_Program_Object_Frag);
	glDetachShader(gShader_Program_Object_Frag, gFragment_Shader_Object);
	gFragment_Shader_Object = 0;
	
	glDetachShader(gShader_Program_Object_Vert, gVertex_Shader_Object);
	gVertex_Shader_Object = 0;
	glDeleteProgram(gShader_Program_Object_Vert);
	
	gShader_Program_Object_Vert = 0;
	
	glUseProgram(0);

	CVDisplayLinkStop(displaylink);
	CVDisplayLinkRelease(displaylink);

	[super dealloc];
}

@end


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displaylink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagOut, void *pDisplayLingContext)
{
	CVReturn result = [(GLView *)pDisplayLingContext getFrameForTime:pOutputTime];
	return(result);
}

