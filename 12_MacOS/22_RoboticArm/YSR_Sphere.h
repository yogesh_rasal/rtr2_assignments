/*
	This file is created for Mac & iOS using Sir's Logic
*/

#include<stdio.h>
#include<string.h>
#include<iostream>

//for Mac
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

// as given
float VDG_PI = 3.14159265358979323846;

enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

//Globals
unsigned short elements[30 * 30 * 6];
float verts[30 * 30 * 6];
float norms[30 * 30 * 6];
float texCoords[30 * 30 * 4];

//for sphere
int numOfElements = 0;
int maxElements = 0;
int numVertices = 0;

// Vao & Vbo
GLuint vao = 0;
GLuint vbo_position = 0;
GLuint vbo_normal = 0;
GLuint vbo_texture = 0;
GLuint vbo_index = 0;

//functions
void cleanupMeshData();
void normalizeVector(float *);
bool isFoundIdentical(float, float, float);


//Code for Mesh
void allocate()
{
	//imp
	cleanupMeshData();
	
	int numIndices = 30 * 30 * 6;
	maxElements = numIndices;
	numOfElements = 0;
	numVertices = 0;

}

void addTriangle1(float** single_vertex, float** single_normal, float** single_texture)
{
	printf("Added");
}

void addTriangle(float** single_vertex, float** single_normal, float** single_texture)
{
	
	//Code
	float diff = 0.00001;
	int i, j;

	normalizeVector(single_normal[0]);
	normalizeVector(single_normal[1]);
	normalizeVector(single_normal[2]);

	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < numVertices; j++)
		{
			if (isFoundIdentical(verts[j * 3], single_vertex[i][0], diff) &&
				isFoundIdentical(verts[(j * 3) + 1], single_vertex[i][1], diff) &&
				isFoundIdentical(verts[(j * 3) + 2], single_vertex[i][2], diff) &&

				isFoundIdentical(norms[j * 3], single_normal[i][0], diff) &&
				isFoundIdentical(norms[(j * 3) + 1], single_normal[i][1], diff) &&
				isFoundIdentical(norms[(j * 3) + 2], single_normal[i][2], diff) &&

				isFoundIdentical(texCoords[j * 2], single_texture[i][0], diff) &&
				isFoundIdentical(texCoords[(j * 2) + 1], single_texture[i][1], diff))
			{
				elements[numOfElements] = j;
				numOfElements++;
				break;
			}
		}


	   //Vimp
		if (j == numVertices && numVertices < maxElements && numOfElements < maxElements)
		{
			verts[numVertices * 3] = single_vertex[i][0];
			verts[(numVertices * 3) + 1] = single_vertex[i][1];
			verts[(numVertices * 3) + 2] = single_vertex[i][2];

			norms[numVertices * 3] = single_normal[i][0];
			norms[(numVertices * 3) + 1] = single_normal[i][1];
			norms[(numVertices * 3) + 2] = single_normal[i][2];

			texCoords[numVertices * 2] = single_texture[i][0];
			texCoords[(numVertices * 2) + 1] = single_texture[i][1];

			
			elements[numOfElements] = numVertices; //adding the index to the end of list of elements
			
			numOfElements++; // end of list
			
			numVertices++; // count of vertices
		}
	}
}


void prepareToDraw()
{
	// vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// position vbo
	glGenBuffers(1, &vbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &vbo_normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(norms), norms, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// texture vbo
	glGenBuffers(1, &vbo_texture);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_texture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texCoords), texCoords, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &vbo_index);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);

	cleanupMeshData();
}


//Display
void draw()
{
	// bind
	glBindVertexArray(vao);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);

	glDrawElements(GL_TRIANGLES, numOfElements, GL_UNSIGNED_SHORT, 0);

	// unbind 
	glBindVertexArray(0);
}


int getIndexCount()
{
	return(numOfElements);
}


int getVertexCount()
{
	return(numVertices);
}


void normalizeVector(float *v)
{
	// square the vector length
	float squaredVectorLength = (v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]);

	// get square root of above 
	float squareRootOfSquaredVectorLength = sqrt(squaredVectorLength);

	// scale the vector with 1/sqrt
	v[0] = v[0] * (1.0 / squareRootOfSquaredVectorLength);
	v[1] = v[1] * (1.0 / squareRootOfSquaredVectorLength);
	v[2] = v[2] * (1.0 / squareRootOfSquaredVectorLength);
}


bool isFoundIdentical(float val1, float val2, float diff)
{
	float d = (val1 - val2);
	if (d < 0)
		d = d * -1.0f;

	if (d < diff)
		return(true);
	else
		return(false);
}

void cleanupMeshData()
{
	// code
}


//Sphere
void makeSphere(float fRadius,int iSlices,int iStacks)
{
    // code
    float drho = (float)VDG_PI / (float)iStacks;
    float dtheta = 2.0 *(float)(VDG_PI) / (float)(iSlices);
    float ds = 1.0 / (float)(iSlices);
    float dt = 1.0 / (float)(iStacks);
    float t = 1.0;
    float s = 0.0;
    int i = 0;
    int j = 0;
    
    allocate();
    
    for (i = 0; i < iStacks; i++)
    {
        float rho = (float)(i * drho);
        float srho = (float)(sin(rho));
        float crho = (float)(cos(rho));
        float srhodrho = (float)(sin(rho + drho));
        float crhodrho = (float)(cos(rho + drho));
        
        s = 0.0; //imp
        
        // initialization of three 2-D arrays
		float **vertex = NULL;
		float **normal = NULL;
		float **texture = NULL;

		//Set Data
		vertex = (float **)malloc(sizeof(float *) * 4); // 4 rows
	
		for (int a = 0; a < 4; a++) 
		{
			vertex[a] = (float*)malloc(sizeof(float) * 3); // 3 columns
		}

		normal = (float**)malloc(sizeof(float *) * 4); // 4 rows
        
		for (int b = 0; b < 4; b++)
		{
			normal[b] = (float*)malloc(sizeof(float) * 3); // 3 columns
		}

		texture = (float**)malloc(sizeof(float *) * 4);
        
		for (int c = 0; c < 4; c++)
		{
			texture[c] = (float*)malloc(sizeof(float) * 2); // 3 columns
		}
		

		//Draw
        for (j = 0; j < iSlices; j++)
        {
            float theta = (j == iSlices) ? 0.0 : j * dtheta;
            float stheta = (float)(-sin(theta));
            float ctheta = (float)(cos(theta));
            
            float x = stheta * srho;
            float y = ctheta * srho;
            float z = crho;
            
            texture[0][0] = s;
            texture[0][1] = t;
            normal[0][0] = x;
            normal[0][1] = y;
            normal[0][2] = z;
            vertex[0][0] = x * fRadius;
            vertex[0][1] = y * fRadius;
            vertex[0][2] = z * fRadius;
            
            x = stheta * srhodrho;
            y = ctheta * srhodrho;
            z = crhodrho;
            
            texture[1][0] = s;
            texture[1][1] = t - dt;
            normal[1][0] = x;
            normal[1][1] = y;
            normal[1][2] = z;
            vertex[1][0] = x * fRadius;
            vertex[1][1] = y * fRadius;
            vertex[1][2] = z * fRadius;
            
            theta = ((j+1) == iSlices) ? 0.0 : (j+1) * dtheta;
            stheta = (float)(-sin(theta));
            ctheta = (float)(cos(theta));
            
            x = stheta * srho;
            y = ctheta * srho;
            z = crho;
            
            s = s + ds;

            texture[2][0] = s;
            texture[2][1] = t;
            normal[2][0] = x;
            normal[2][1] = y;
            normal[2][2] = z;
            vertex[2][0] = x * fRadius;
            vertex[2][1] = y * fRadius;
            vertex[2][2] = z * fRadius;
            
            x = stheta * srhodrho;
            y = ctheta * srhodrho;
            z = crhodrho;
            
            texture[3][0] = s;
            texture[3][1] = t - dt;
            normal[3][0] = x;
            normal[3][1] = y;
            normal[3][2] = z;
            vertex[3][0] = x * fRadius;
            vertex[3][1] = y * fRadius;
            vertex[3][2] = z * fRadius;
            
            addTriangle(vertex, normal, texture);
            
            // Rearrange for next triangle
            vertex[0][0]=vertex[1][0];
            vertex[0][1]=vertex[1][1];
            vertex[0][2]=vertex[1][2];
            normal[0][0]=normal[1][0];
            normal[0][1]=normal[1][1];
            normal[0][2]=normal[1][2];
            texture[0][0]=texture[1][0];
            texture[0][1]=texture[1][1];
            
            vertex[1][0]=vertex[3][0];
            vertex[1][1]=vertex[3][1];
            vertex[1][2]=vertex[3][2];
            normal[1][0]=normal[3][0];
            normal[1][1]=normal[3][1];
            normal[1][2]=normal[3][2];
            texture[1][0]=texture[3][0];
            texture[1][1]=texture[3][1];
            
            addTriangle(vertex, normal, texture);
        }
        t = t-dt;
    }

    prepareToDraw();
}

//Safe Release
void deallocate()
{
	//VAO & VBO
	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	if (vbo_index)
	{
		glDeleteBuffers(1, &vbo_index);
		vbo_index = 0;
	}

	if (vbo_texture)
	{
		glDeleteBuffers(1, &vbo_texture);
		vbo_texture = 0;
	}

	if (vbo_normal)
	{
		glDeleteBuffers(1, &vbo_normal);
		vbo_normal = 0;
	}

	if (vbo_position)
	{
		glDeleteBuffers(1, &vbo_position);
		vbo_position = 0;
	}
}

