/* 

C_Assignment_for_Prime_Numbers

Input  : Max.Number till which we have to Print

Output : List of Prime Numbers

*/

#include<stdio.h>

int main(void)
{
	// Variables
	int N = 0;
	int i = 0; // index
	int num = 2; //middle one
	int flag = 0; // Flag 
	
	printf("\n Enter the Max Number below 1000 : \t");
	scanf("%d",&N);
	
	printf("List of Prime Numbers is :\n ");
	
	while (num < N)
    {
        flag = 0;

        for(i = 2; i <= num/2; ++i)
        {
			// Loop till Half of Limit
			
            if(num % i == 0)
            {
                flag = 1;
                break;
            }
        }
		
		
        if (flag == 0)
		{
			printf("%d \n ", num);
		}
        ++num;
    }

 return(0);
}
