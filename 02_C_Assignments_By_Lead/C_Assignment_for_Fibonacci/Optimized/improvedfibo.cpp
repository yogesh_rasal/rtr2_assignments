#include<stdio.h>

int fnArray[50];



int main(void)
{

	int fibo(int);

	for (int i = 0; i < 50; ++i)
		fnArray[i] = 0;

	fnArray[0] = 0;
	fnArray[1] = 1;
	fnArray[2] = 1;

	int num;
	int result;
	printf("Enter number for fibo : ");
	scanf("%d", &num);
	result = fibo(num);
	printf("Fibonacii of %d is %d\n", num, result);
	return(0);
}

int fibo(int num)
{

	printf("Fibo call for %d\n", num);
	int res;
	if ((num == 1) || num == 2)
		return 1;
	if (fnArray[num] != 0)
		return fnArray[num];
	res = fibo(num - 1) + fibo(num - 2);
	fnArray[num] = res;
	return res;
}