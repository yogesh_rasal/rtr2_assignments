/*
		Assignemnt : Fibonacci Series 
*/

#include <stdio.h>

int main(void)
{
	int num = 1,Result;
	
	printf("Enter Number below 100 :  \t");
	scanf("%d",&num);
	
	Result = fibonacci(num);
	printf("Eg. 1 1 2 ");
	printf(" fibonacci Series of %d is : %d  \n",num,Result);
	
	return(0);
	
}// main ends

// Fibonacci using Recursion
int fibonacci(int num)
{
	if(num <= 2)
	{
		return (num);
	}
	else 
	{
		//printf("%d ",num-1);	
		return(fibonacci(num-1)+fibonacci(num-2));
	}
}
