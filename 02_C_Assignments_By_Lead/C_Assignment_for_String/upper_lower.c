/* 
	C Assignment : Upper to Lower Case & Lower to Upper Case
*/

#include <stdio.h>
 
int main ()
{
   int index = 0;
   char ch, str[100];
 
	// Code
   printf("\n Enter a String : \t");
   scanf("%s",&str);
   
   while (str[index] != '\0') 
   {
      ch = str[index];
      if (ch >= 'A' && ch <= 'Z')
	  {
         str[index] = str[index] + 32; // Upper to Lower
      }
	  
	  else 
	  if (ch >= 'a' && ch <= 'z')
      {
		str[index] = str[index] - 32;  // Lower to Upper
		index++;  
	  }
   }
   printf("\n Converted String is %s \n", str);
 
   return 0;
}// main ends