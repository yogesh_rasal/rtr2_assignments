/*
	Assignment : Factorial of Number using for & while
	1 < N <= 10
*/

#include <stdio.h>

int main(void)
{
	int Number = 1,i,index;
	int Factorial = 1;
	
	printf("\n Enter Any No.less than 10 : ");
	scanf("%d",&Number);
	
	//Factorial using for
	for(i = Number;i > 1;i--)
	{
		Factorial = Factorial * i;
	}
	printf("\n Factorial of %d using for Loop :%d",Number,Factorial);
	
	//Factorial using while
	index = 2;
	while(index <= Number)
	{
		Factorial = Factorial*i;
		index++;
	}
	printf("\n Factorial of %d using While Loop :%d \n",Number,Factorial);
 return(0);	
}// main ends


