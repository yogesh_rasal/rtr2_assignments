/*
	Assignment : Factorial of Number using for & while
	1 < N <= 10
*/

#include <stdio.h>

void Print_Factorial(int);
int Print_Factorial_Recursion(int);


int main(void)
{
	int Number = 1,ans;
	
	printf("\n Enter Any No.less than 10 : ");
	scanf("%d",&Number);
	
	//Factorial using Normal Function
	Print_Factorial(Number);
	
	//Factorial using Recursion
	ans = Print_Factorial_Recursion(Number);
	printf("\n Factorial of %d using Recursive Function :%d \n",Number,ans);	
		
 return(0);	
}// main ends

//Factorial using Normal Function
void Print_Factorial(int Num)
{
	int index = 1,Factorial =1;
	
	for(index = Num;index > 1;index--)
	{
		Factorial = Factorial * index;
	}
	printf("\n Factorial of %d using Normal Function :%d \n",Num,Factorial);
}


//Factorial using Recursive Function
int Print_Factorial_Recursion(int Number)
{
	int index = Number;
	
	if(Number >= 1)
	{
		return (Number * Print_Factorial_Recursion(Number-1));
	}
	else
		return 1;
}
