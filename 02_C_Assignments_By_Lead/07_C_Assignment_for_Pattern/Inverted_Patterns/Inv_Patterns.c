/*

Required Output : 
 
3. Inverted Right Triangle Star

*/
#include <stdio.h>

int main(void)
{
	int i,j,row=5,row_rev;
	
	printf("\n 3. Inverted Mirrored Right Triangle Star : \n \n ");
	for(i = row; i >= 1 ; i--)
		{
			for( j = 0; j < row-i ; j++)
			{
				printf("  ");//for initial space
			}
		  
			for(row_rev = (2-i); row_rev < ((2-i)+(2*i-1));++row_rev)
			{
				printf("*");
			}
		  printf("\n");
		}
	

	printf("\n 4.Invert Right Triangle Pattern Star : \n \n");
	  
	for(i=1; i<=row; i++)
    {
        for(j=i; j<row; j++)
        {
            printf(" "); //for spaces in decreasing order
        }

        for(j=1; j<=i; j++)
        {
            printf("*"); //for stars in increasing order
        }

        printf("\n");
    }

	return(0);
}// main ends

