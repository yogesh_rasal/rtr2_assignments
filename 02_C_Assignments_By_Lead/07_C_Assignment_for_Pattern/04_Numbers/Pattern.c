/*
Required Output : Number Patterns


*/

#include <stdio.h>

int main(void)
{
	int num1,num2,max;
	
	printf("\n Enter Max No: ");
    scanf("%d", &max);

	printf("\n Number Pattern : \n \n");
    for(num1 = 1; num1 <= max; num1++)
    {
        for(num2 = 1; num2<= num1; num2++)
        {
            printf("%d", num2); // for first part of pattern
        }

        
        for(num2 = num1*2; num2 < max*2; num2++)
        {
            printf(" "); // for spaces between two parts
        }

   
        for(num2 = num1; num2 >= 1; num2--)
        {
            printf("%d", num2); //second part of the pattern
        }

        printf("\n");
	}
	return (0);
	
}//main ends
