/*

Required Outputs : 

1. Right Triangle Star

2. Mirrored Right Triangle Star


*/

#include<stdio.h>

int main(void)
{
	// Variables
	int Ist,IInd,row;
	
	printf("\n");
	printf("\n Enter No.of Rows tobe Print :\t");
	scanf("%d",&row);
	
	printf("1. Right Triangle Star : \n \n");
	for(Ist = 1; Ist <= row ; ++Ist)
		{
			for( IInd = 1; IInd <= Ist ; ++IInd)
			{
				printf(" * ");
			}
		  printf("\n");
		}
	
	printf("\n 2. Mirrored Right Triangle Star : \n \n");
	for(Ist = row; Ist >= 1 ; --Ist)
		{
			for( IInd = 1; IInd <= Ist ; ++IInd)
			{
				printf(" * ");
			}
		  printf("\n");
		}
	
	
	getch();
	return(0);
}// main ends