/*

Input : 
Accept N Photos with W*H Dimensions
Minimum dimensions of Frame : L*L

Contraints :

1 <= L,W,H,N <= 1000

Output : 
Display Appropriate Message for each Photo

*/
#include<stdio.h>

int main(void)
{
	int L,N,W,H;
	
	printf("\n Enter Size of Frame : \t");
	scanf("%d",&L);
	
	printf("\n Enter No of Pics : \t");
	scanf("%d",&N);
	
	if(N>0)
	{
		printf("\n Enter Width & Height : \t");
		scanf("%d %d",&W,&H);
		
		if(W==L && H==L)
		{
			printf("\n Accept It");
		}
		else if(W > L || H > L)
		{
			printf("\n Crop it");
		}
		else printf("\n Upload Other \n");
	}
	else printf("\n Enter valid Count\n");
		
	return(0);
}//MAIN ENDS




