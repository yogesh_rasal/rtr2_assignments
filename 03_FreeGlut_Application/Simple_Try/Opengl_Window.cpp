#include "GL\freeglut.h"
#include "math.h"

bool bIsfullScreen = false;

int main(int argc, char *argv[])
{
	// Function Declrations
	void Initialize(void);
	void Unintialize(void);
	void Reshape(int, int);
	void display(void);
	void KeyBoard(unsigned char, int, int);
	void Mouse(int, int, int, int);

	// Code
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(800,600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Freeglut Trial");

	Initialize();

	// CallBacks
	glutDisplayFunc(display);
	glutReshapeFunc(Reshape);
	glutKeyboardFunc(KeyBoard);
	glutMouseFunc(Mouse);
	glutCloseFunc(Unintialize);

	glutMainLoop();
	
	return (0);

} // Main Ends

void Initialize(void)
{
	//code
	glClearColor(0.0f,0.0f,0.0f,1.0f);

}

void Unintialize(void)
{
 //Code
}

void Reshape(int Width, int Height)
{
	//code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

}

void display(void)
{
	// Code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	// 1st Triangle
	glBegin(GL_TRIANGLES);
	
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(-0.5f,1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex2f(1.0f, 1.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, -1.0f);

	glEnd();
	
	// 2nd Triangle
	glBegin(GL_TRIANGLES);
	
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(1.0f,0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(-0.5f, 1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex2f(0.0f, -1.0f);

	glEnd();
	
	/* code for Eye is not working
	glBegin(GL_TRIANGLES);
	
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(0.20f,0.20f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(-0.25f, 0.25f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex2f(0.0f, -0.25f);

	glEnd();

	*/
	
	/*////////////////

	glBegin(GL_POLYGON);
		glColor3f(1.0f, 1.0f, 1.0f);
		for(float angle = 0.0f; angle<(2.0f * 180); angle = angle+0.01f)
		{
			glVertex3f(cos(angle), sin(angle), 0.0f);
			
		}
		glEnd();

	/////////////////////*/
	
	glFlush();

} //Display = Paint


void KeyBoard(unsigned char key,int x,int y)
{
	//Code
	switch (key)
	{
	case 27: glutLeaveMainLoop();
		break;

	case 'F':
	case 'f' :
		if (bIsfullScreen == false)
		{
			glutFullScreen();
			bIsfullScreen = true;
		}
		
		else {
			glutLeaveFullScreen();
			bIsfullScreen = false;
			}
		break;
	} //switch
}


void Mouse(int Button, int State, int x, int y)
{
	//Code
	switch (Button)
	{
	case GLUT_LEFT_BUTTON:
		break;

	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;

	}//switch

}
