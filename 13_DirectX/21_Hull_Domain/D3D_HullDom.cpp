/*
	 HULL
*/


// Headers
#include<Windows.h>
#include<stdio.h> // For file IO
#include<d3d11.h> // For Dx
#include<d3dcompiler.h> // for Shader Compile

#pragma warning(disable:4838)
#include "XNAMath\xnamath.h"

//Export libs
#pragma comment(lib, "d3d11.lib") 
#pragma comment(lib, "gdi32.lib") 
#pragma comment(lib, "user32.lib") 
#pragma comment(lib, "D3dcompiler.lib") 
#pragma comment(lib, "DXGI.lib")

//Macros
#define WIN_WIDTH 700 
#define WIN_HEIGHT 600 
#define UNICODE


// file IO
FILE *gpFile = NULL;
char gszLogFileName[] = "Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool bFullScreen = false;

float gClearColor[4];//RGBA

//for Dx
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;


//for Shaders
ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
//1
ID3D11HullShader *gpID3D11HullShader = NULL;    
ID3D11DomainShader *gpID3D11DomainShader = NULL; 

ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_HullShader = NULL;      
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_DomainShader = NULL;

ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_PixelShader = NULL;

//for VB
ID3D11Buffer *gpID3D11Buffer_TriVertexBuffer = NULL;

//for Backface CULL : 3 Steps
ID3D11RasterizerState *gpID3D11RasterizerState = NULL;

//for Depth
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL; //DSV


//imp : 3 CB

struct CBUFFER_HULL_SHADER
{
    XMVECTOR Hull_Constant_Function_Param;
};

struct CBUFFER_DOMAIN_SHADER 
{
    XMMATRIX WorldViewProjectionMatrix; 
};

struct CBUFFER_PIXEL_SHADER
{
    XMVECTOR LineColor;
};

// 3
unsigned int guiNumberOfLineSegments = 1;
unsigned int guiNumberOfLineStrips = 1;


XMMATRIX Perspective_Projection_Matrix ; //Perspective

// Function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
HRESULT Initialize(void);
void Update(void);
void UnInitialize(void);
void Display(void); 
HRESULT Resize(int, int);
void ToggleFullScreen(void);

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) 
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Direct3D11");
	bool bDone = false; 

	// Create Log File
	if (fopen_s(&gpFile, gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created\n ."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Successfully Created \n");
		fclose(gpFile);
	}

	//  WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Change 11
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register wnd class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("Direct3D11 Hull Domain : By YSR"),
			 WS_OVERLAPPEDWINDOW,
			 100, 100,
			 WIN_WIDTH,	WIN_HEIGHT,
			 NULL,
		  	 NULL,
		 	 hInstance,
			 NULL);

	ghwnd = hwnd;

	fopen_s(&gpFile, gszLogFileName, "a+");
	fprintf_s(gpFile, "Window is Created.\n");
	fclose(gpFile);

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Check
	HRESULT hr;
	hr = Initialize();

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() Failed .\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() Succeed. \n");
		fclose(gpFile);
	}

	// Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else if (gbActiveWindow == true)
		{
			Update();
		}

		Display();
	}

	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) 
{
	// Variables
	HRESULT hr;

	// code
	switch (iMsg) 
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
		{
			gbActiveWindow = true;
		}
		else
		{
			gbActiveWindow = false;
		}
		break;
	
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND: 
		return(0); // No Break

	case WM_SIZE:
	if(gpID3D11DeviceContext)
	{
		hr = Resize(LOWORD(lParam), HIWORD(lParam));
		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "Resize() Failed...\n");
			fclose(gpFile);
			return(hr);
		}
		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "Resize() Succeed.\n");
			fclose(gpFile);
		}
	}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'f':
		case 'F':
			if(bFullScreen == false)
			{
				ToggleFullScreen();
				bFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				bFullScreen = false;
			}
			break;
		
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		
		case VK_UP:
                    guiNumberOfLineSegments++;
                    if(guiNumberOfLineSegments >= 30)
                    {
                        guiNumberOfLineSegments = 30; // reset
                    }
                    break;
        
		case VK_DOWN:
                    guiNumberOfLineSegments--;
                    if(guiNumberOfLineSegments <= 1)
                    {
                        guiNumberOfLineSegments = 1; // reset
                    }
                    break;

		default:
			break;
		}
		break;

	case WM_DESTROY:
		UnInitialize(); 
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


//imp
void ToggleFullScreen(void) 
{
	MONITORINFO mi;
	mi = { sizeof(MONITORINFO) };

	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) &&
				GetMonitorInfo(	MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,	HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	
	else //already
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd,	HWND_TOP,
			0,	0,	0,	0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bFullScreen = false;
	}
}


//Major code
HRESULT Initialize(void) 
{
	// Variables
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE, //GC 
										 D3D_DRIVER_TYPE_WARP,   // WARP
										 D3D_DRIVER_TYPE_REFERENCE //sw
									   };

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; //lowest

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	// Code
	fopen_s(&gpFile, gszLogFileName, "a+");

	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);
	fprintf_s(gpFile, "in Initialize() : %d...\n",numDriverTypes);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC)); // To make members of structure 0.

	fprintf_s(gpFile, "in Initialize() : with dxgiSwapChainDesc...\n");
	fclose(gpFile);

	//Members
	dxgiSwapChainDesc.BufferCount = 1;

	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; //RGBA
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60; //RR
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;

	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;

	dxgiSwapChainDesc.SampleDesc.Count = 1; // as Dx gives +1
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE; //Toggle-Able


	//Get the Driver
	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex]; //current 

		//12 Members
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,						//Adapter
			d3dDriverType,				//Driver-type
			NULL,						//Rasterizer
			createDeviceFlags,			//Flags
			&d3dFeatureLevel_required,  //Feature : 11
			numFeatureLevels, 			//No. of FL
			D3D11_SDK_VERSION,			//Library's SDK
			&dxgiSwapChainDesc,			//SCD 
			&gpIDXGISwapChain,			//SC
			&gpID3D11Device,			//Device
			&d3dFeatureLevel_acquired,	// FL
			&gpID3D11DeviceContext);	//Device Context

		if (SUCCEEDED(hr))
		{
			break;
		}
	}

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Failed...\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Succeed. \n");
		fprintf_s(gpFile, "The chosen Driver is of : ");

		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type. \n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "WARP Type. \n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Software Ref Type. \n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type. \n");
		}
		fclose(gpFile);
	}

	//Shaders , Input Layouts & Buffers
	// a. Vertex
	const char *VertexShaderSourceCode = 
    "struct vertex_output"                                          \
    "{"                                                             \
        "float4 position: position;"                                \
    "};"                                                            \
    "vertex_output main(float2 pos: POSITION)"                     \
    "{"                                                             \
        "vertex_output output;"                                     \
        "output.position = float4(pos, 0.0, 1.0);"                \
        "return(output);"                                           \
    "}";

	//1
	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;		

	//2
	hr = D3DCompile(VertexShaderSourceCode, //src
					lstrlenA(VertexShaderSourceCode)+1, //len + EOF
					"VS",	//Code String
					NULL,	//No Macros
					D3D_COMPILE_STANDARD_FILE_INCLUDE, // take ur std
					"main", 	//Entry Point
					"vs_5_0",	//Feature Level
					0,		//How to compile
					0,  	//Effect Const
					&pID3DBlob_VertexShaderCode,	//Actual VS
					&pID3DBlob_Error	//for Errors
	);

	if(FAILED(hr))
	{
		if(pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"D3DCompile() Failed for Vertex Shader : %s \n",
						(char *)pID3DBlob_Error->GetBufferPointer() ); //convert
			fclose(gpFile);

			//imp : Like COM
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}

	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"D3DCompile() Succeed for Vertex Shader \n");						
			fclose(gpFile);
	}
	
	//3 : Create
	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(),	//Bytecode
											pID3DBlob_VertexShaderCode->GetBufferSize(),
											NULL,
											&gpID3D11VertexShader);

	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateVertexShader() Failed in Vertex Shader \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateVertexShader() Succeed in Vertex Shader \n");						
			fclose(gpFile);
	}
	
	//4
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader,
										0,	//shareable
										0);	//Member count 


	//b. HULL Shader
    const char *hullShaderSourceCode =
    "cbuffer ConstantBuffer"                                                                        \
    "{"                                                                                             \
        "float4 hull_constant_function_param;"                                                     \
    "};"                                                                                             \
    "struct vertex_output"                                                                          \
    "{"                                                                                             \
        "float4 position: position;"                                                                \
    "};"                                                                                            \
    "struct hull_constant_output"                                                                   \
    "{"                                                                                             \
        "float edges[2]: SV_TESSFACTOR;"                                                           \
    "};"                                                                                            \
    "hull_constant_output hull_constant_function(void)"                                             \
    "{"                                                                                             \
        "hull_constant_output output;"                                                              \
        "float numberOfStrips = hull_constant_function_param[0];"                                  \
        "float numberOfSegments = hull_constant_function_param[1];"                                \
        "output.edges[0] = numberOfStrips;"                                                         \
        "output.edges[1] = numberOfSegments;"                                                       \
        "return(output);"                                                                           \
    "}"                                                                                             \
    "struct hull_output"                                                                            \
    "{"                                                                                             \
    "float4 position: position;"                                                               \
    "};"                                                                                            \
    "[domain(\"isoline\")]"                                                                         \
    "[partitioning(\"integer\")]"                                                                   \
    "[outputtopology(\"line\")]"                                                                    \
    "[outputcontrolpoints(4)]"                                                                      \
    "[patchconstantfunc(\"hull_constant_function\")]"                                               \
    "hull_output main(InputPatch<vertex_output, 4> input_patch, uint i : SV_OUTPUTCONTROLPOINTID)"  \
    "{"                                                                                             \
    "hull_output output;"                                                                       \
    "output.position = input_patch[i].position;"                                                \
    "return(output);"                                                                           \
    "}";  
	//SV_OutputControlPointID - SV_OUTPUTCONTROLPOINTID

	//1
	ID3DBlob *pID3DBlob_HullShaderCode = NULL;
	pID3DBlob_Error = NULL;		

	//2
	hr = D3DCompile(hullShaderSourceCode, //src
					(SIZE_T)lstrlenA(hullShaderSourceCode)+1, //len + EOF
					"HS",	//Code String
					NULL,	//No Macros
					D3D_COMPILE_STANDARD_FILE_INCLUDE, // take ur std
					"main", 	//Entry Point
					"hs_5_0",	//Feature Level
					0,		//How to compile
					0,  	//Effect Const
					&pID3DBlob_HullShaderCode,	//Actual GS
					&pID3DBlob_Error	//for Errors
	);

	if(FAILED(hr))
	{
		if(pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"D3DCompile() Failed for Hull Shader : %s \n",
						(char *)pID3DBlob_Error->GetBufferPointer() ); //convert
			fclose(gpFile);

			//imp : Like COM
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}

	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"D3DCompile() Succeed for Hull Shader \n");						
			fclose(gpFile);
	}
	
	//3 : Create
	hr = gpID3D11Device->CreateHullShader(pID3DBlob_HullShaderCode->GetBufferPointer(),	//Bytecode
											pID3DBlob_HullShaderCode->GetBufferSize(),
											NULL,
											&gpID3D11HullShader);

	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateHullShader() Failed in HULL Shader \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateHullShader() Succeed in HULL Shader \n");						
			fclose(gpFile);
	}
	
	//4
	gpID3D11DeviceContext->HSSetShader(gpID3D11HullShader,0,0);	


	// c. Domain  = TES
	 const char *domainShaderSourceCode =
    "cbuffer ConstantBuffer"                                                                                                            \
    "{"                                                                                                                                 \
        "float4x4 worldViewProjectionMatrix;"                                                                                           \
    "};"                                                                                                                                 \
    "struct hull_constant_output"                                                                                                       \
    "{"                                                                                                                                 \
        "float edges[2]: SV_TessFactor;"                                                                                               \
    "};"                                                                                                                                \
    "struct hull_output"                                                                                                                \
    "{"                                                                                                                                 \
        "float4 position : position;"                                                                                                   \
    "};"                                                                                                                                \
    "struct domain_output"                                                                                                              \
    "{"                                                                                                                                 \
        "float4 position : SV_POSITION;"                                                                                                \
    "};"                                                                                                                                \
    "[domain(\"isoline\")]"                                                                                                             \
    "domain_output main(hull_constant_output input, OutputPatch<hull_output, 4> output_patch, float2 tessCoord : SV_DOMAINLOCATION)"    \
    "{"                                                                                                                                 \
        "domain_output output;"                                                                                                         \
        "float u = tessCoord.x;"                                                                                                        \
        "float3 p0 = output_patch[0].position.xyz;"                                                                                     \
        "float3 p1 = output_patch[1].position.xyz;"                                                                                     \
        "float3 p2 = output_patch[2].position.xyz;"                                                                                     \
        "float3 p3 = output_patch[3].position.xyz;"                                                                                     \
        "float u1  = (1.0 - u);"                                                                                                        \
        "float u2  = u * u;"                                                                                                             \
        "float b3  = u2 * u;"                                                                                                            \
        "float b2  = 3.0 * u2 * u1;"                                                                                                    \
        "float b1  = 3.0 * u * u1 * u1;"                                                                                                \
        "float b0  = u1 * u1 * u1;"                                                                                                      \
        "float3 p  = p0 * b0 + p1 * b1 + p2 * b2 + p3 * b3;"                                                                             \
        "output.position = mul(worldViewProjectionMatrix, float4(p, 1.0f));"                                                            \
        "return(output);"                                                                                                               \
    "}"; 
   
   //1
	ID3DBlob *pID3DBlob_DomainShaderCode = NULL; 
    pID3DBlob_Error = NULL;

    hr = D3DCompile( domainShaderSourceCode,
                     lstrlenA(domainShaderSourceCode) + 1, 
                     "DS",  // Shader String
                      NULL,
                     (ID3DInclude *)D3D_COMPILE_STANDARD_FILE_INCLUDE,
                     (LPCSTR)"main",            
                     "ds_5_0",         
                     (UINT)0,                   
                     (UINT)0,                   
                     &pID3DBlob_DomainShaderCode,
                     &pID3DBlob_Error);         
         
    if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateShader() Failed in Domain Shader \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateShader() Succeed in Domain Shader \n");						
			fclose(gpFile);
	}
	
    hr = gpID3D11Device->CreateDomainShader(pID3DBlob_DomainShaderCode->GetBufferPointer(), 
                             pID3DBlob_DomainShaderCode->GetBufferSize(),                  
                             NULL, 
                             &gpID3D11DomainShader);

    if(FAILED(hr))
    {
        fopen_s(&gpFile, gszLogFileName, "a+");
        fprintf_s(gpFile, "CreateDomainShader() Failed.\n");
        fclose(gpFile);
        return(hr);
    }
    else
    {
        fopen_s(&gpFile, gszLogFileName, "a+");
        fprintf_s(gpFile, "CreateDomainShader() Succeed.\n");
        fclose(gpFile);
    }

    gpID3D11DeviceContext->DSSetShader(gpID3D11DomainShader,
                                   NULL,                    
                                   0);                      



	// d. Pixel = Fragment 
	const char *PixelShaderSourceCode = 
    "cbuffer ConstantBuffer"                \
    "{"                                     \
    "float4 lineColor: COLOR;"                \
    "};"                                     \
    "float4 main() : SV_TARGET"         \
    "{"                                     \
        "float4 color = lineColor;"          \
        "return(color);"                    \
	"}";
	 //lineColor - float4(1.0, 1.0, 1.0, 1.0)

	//1
	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;		

	//2
	hr = D3DCompile(PixelShaderSourceCode, //src
					lstrlenA(PixelShaderSourceCode)+1, //len + EOF
					"PS",	//Code String
					NULL,	//No Macros
					D3D_COMPILE_STANDARD_FILE_INCLUDE, // take ur std
					"main", 	//Entry Point
					"ps_5_0",	//Feature Level
					0,	//How to compile
					0,  //Effect Const
					&pID3DBlob_PixelShaderCode,	//Actual 
					&pID3DBlob_Error	//for Errors
	);

	if(FAILED(hr))
	{
		if(pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"D3DCompile() Failed for Pixel Shader : %s \n",
						(char *)pID3DBlob_Error->GetBufferPointer() ); //convert
			fclose(gpFile);

			//imp : Like COM
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}

	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"D3DCompile() Succeed for Pixel Shader \n");						
			fclose(gpFile);
	}
	
	//3 : Create
	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(),	//Bytecode
											pID3DBlob_PixelShaderCode->GetBufferSize(),
											NULL,
											&gpID3D11PixelShader);


	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreatePixelShader() Failed \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreatePixelShader() Succeed in Pixel Shader \n");						
			fclose(gpFile);
	}	

	//4
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader,0,0);


//Input Layout : Like glBindAttribLoc

	D3D11_INPUT_ELEMENT_DESC inputElementDesc;
	ZeroMemory(&inputElementDesc,sizeof(D3D11_INPUT_ELEMENT_DESC));

	//Members : Pos
	inputElementDesc.SemanticName = "POSITION";
	inputElementDesc.SemanticIndex = 0;
	inputElementDesc.Format = DXGI_FORMAT_R32G32B32_FLOAT; //Pos : 3 , Tex : 2
	inputElementDesc.InputSlot = 0;	//Layout
	inputElementDesc.AlignedByteOffset = 0; //gap
	inputElementDesc.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA; //Class slot
	inputElementDesc.InstanceDataStepRate = 0;


  hr = gpID3D11Device->CreateInputLayout(&inputElementDesc,
										 1,  //_ARRAYSIZE(inputElementDesc)
										 pID3DBlob_VertexShaderCode->GetBufferPointer(),
										 pID3DBlob_VertexShaderCode->GetBufferSize(),
										 &gpID3D11InputLayout );
	
	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateInputLayout() Failed \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateInputLayout() Succeed  \n");						
			fclose(gpFile);
	}	

	//Set IL 
	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

	//VIMP
	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode= NULL;

	pID3DBlob_HullShaderCode->Release();
	pID3DBlob_HullShaderCode = NULL;
	
	pID3DBlob_DomainShaderCode->Release();
	pID3DBlob_DomainShaderCode = NULL;

	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;


//Triangle
//Vertex Buffer	
	float Vertices[] = 
		{ 
			1.0f, 1.0f, 0.5f, -1.0f, -0.5f, 1.0f, -1.0f, -1.0f
		};

	//Create VB
	D3D11_BUFFER_DESC bufferDesc_Vertex_Tri; // 6 Members
	ZeroMemory(&bufferDesc_Vertex_Tri,sizeof(D3D11_BUFFER_DESC));

	bufferDesc_Vertex_Tri.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_Vertex_Tri.ByteWidth = sizeof(float) * ARRAYSIZE(Vertices);
	bufferDesc_Vertex_Tri.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_Vertex_Tri.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	//b.
   hr = gpID3D11Device->CreateBuffer(&bufferDesc_Vertex_Tri,
 									NULL,
									&gpID3D11Buffer_TriVertexBuffer);	


	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateBuffer() Failed : Vertex Buffer \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateBuffer() Succeed : Vertex Buffer \n");						
			fclose(gpFile);
	}	

	//C. Copy
	D3D11_MAPPED_SUBRESOURCE mapped_Subresource;
	ZeroMemory(&mapped_Subresource,sizeof(D3D11_MAPPED_SUBRESOURCE)) ;

	//VIMP
	gpID3D11DeviceContext->Map(gpID3D11Buffer_TriVertexBuffer,0,
								D3D11_MAP_WRITE_DISCARD,0,
								&mapped_Subresource	);

	//like MMIO
	memcpy(mapped_Subresource.pData,Vertices,sizeof(Vertices));

	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_TriVertexBuffer,NULL);

//3 Constant Buffers
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer_HullShader;
    
	ZeroMemory((void *)&bufferDesc_ConstantBuffer_HullShader, sizeof(D3D11_BUFFER_DESC));
    bufferDesc_ConstantBuffer_HullShader.Usage = D3D11_USAGE_DEFAULT; // use default
    bufferDesc_ConstantBuffer_HullShader.ByteWidth = sizeof(CBUFFER_HULL_SHADER);
    bufferDesc_ConstantBuffer_HullShader.BindFlags = D3D11_BIND_CONSTANT_BUFFER; 
	
    hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer_HullShader, 0, 
						&gpID3D11Buffer_ConstantBuffer_HullShader);
    
	if(FAILED(hr))
    {
        fopen_s(&gpFile, gszLogFileName, "a+");
        fprintf_s(gpFile, "CreateBuffer() Failed for Constant Buffer : Hull Shader.\n");
        fclose(gpFile);
        return(hr);
    }

    else
    {
        fopen_s(&gpFile, gszLogFileName, "a+");
        fprintf_s(gpFile, "CreateBuffer() Succeed for Constant Buffer : Hull Shader.\n");
        fclose(gpFile);
    } 

    gpID3D11DeviceContext->HSSetConstantBuffers(0, 
                                                1, 
                                                &gpID3D11Buffer_ConstantBuffer_HullShader);


// domain shader's Constant Buffer

    D3D11_BUFFER_DESC bufferDesc_ConstantBuffer_DomainShader;
    ZeroMemory((void *)&bufferDesc_ConstantBuffer_DomainShader, sizeof(D3D11_BUFFER_DESC));
    bufferDesc_ConstantBuffer_DomainShader.Usage = D3D11_USAGE_DEFAULT; // use default
    bufferDesc_ConstantBuffer_DomainShader.ByteWidth = sizeof(CBUFFER_DOMAIN_SHADER);
    bufferDesc_ConstantBuffer_DomainShader.BindFlags = D3D11_BIND_CONSTANT_BUFFER; 

    hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer_DomainShader, 0, 
						&gpID3D11Buffer_ConstantBuffer_DomainShader);
    
	if(FAILED(hr))
    {
        fopen_s(&gpFile, gszLogFileName, "a+");
        fprintf_s(gpFile, "CreateBuffer() Failed for Constant Buffer : Domain Shader.\n");
        fclose(gpFile);
        return(hr);
    }
    else
    {
        fopen_s(&gpFile, gszLogFileName, "a+");
        fprintf_s(gpFile, "CreateBuffer() Succeed for Constant Buffer : Domain Shader.\n");
        fclose(gpFile);
    } 

    gpID3D11DeviceContext->DSSetConstantBuffers(0, 
                                                1,
                                                &gpID3D11Buffer_ConstantBuffer_DomainShader);
                                                
   
    // pixel shader's Constant Buffer
    D3D11_BUFFER_DESC bufferDesc_ConstantBuffer_PixelShader;
    ZeroMemory((void *)&bufferDesc_ConstantBuffer_PixelShader, sizeof(D3D11_BUFFER_DESC));
    bufferDesc_ConstantBuffer_PixelShader.Usage = D3D11_USAGE_DEFAULT;
    bufferDesc_ConstantBuffer_PixelShader.ByteWidth = sizeof(CBUFFER_PIXEL_SHADER);
    bufferDesc_ConstantBuffer_PixelShader.BindFlags = D3D11_BIND_CONSTANT_BUFFER; 

    hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer_PixelShader, 0, 
				&gpID3D11Buffer_ConstantBuffer_PixelShader);
    
	if(FAILED(hr))
    {
        fopen_s(&gpFile, gszLogFileName, "a+");
        fprintf_s(gpFile, "CreateBuffer() Failed for Constant Buffer : Pixel Shader.\n");
        fclose(gpFile);
        return(hr);
    }
    else
    {
        fopen_s(&gpFile, gszLogFileName, "a+");
        fprintf_s(gpFile, "CreateBuffer() Succeed for Constant Buffer : Pixel Shader.\n");
        fclose(gpFile);
    } 

    gpID3D11DeviceContext->PSSetConstantBuffers(0, 
                                                1, 
                                                &gpID3D11Buffer_ConstantBuffer_PixelShader);
    

// Back-face CULL : 2 & 3
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory((void *)&rasterizerDesc,sizeof(D3D11_RASTERIZER_DESC));

	//10 Members
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID; //SOLID - WIREFRAME
	//for knowledge
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	
	//like OGL
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;

	hr = gpID3D11Device->CreateRasterizerState(&rasterizerDesc,&gpID3D11RasterizerState);

	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateRasterizerState() Failed : Culling \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateRasterizerState() Succeed : Culling \n");						
			fclose(gpFile);
	}	

	//set RS
	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);


	// End
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 1.0f;

	Perspective_Projection_Matrix = XMMatrixIdentity();

	// Warm up call
	hr = Resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize() Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize() Succeed.\n");
		fclose(gpFile);
	}

	return(S_OK);
}


//Resize
HRESULT Resize(int width, int height) 
{
	// Code:
	HRESULT hr = S_OK;

	// safe release : VIMP
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// resize swap chain 
	if(gpIDXGISwapChain)
	{
		gpIDXGISwapChain->ResizeBuffers(1, width, height,
										DXGI_FORMAT_R8G8B8A8_UNORM, 
										0);
										
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize:: gpIDXGISwapChain is Valid...\n");
		fclose(gpFile);
	}

	// Create Fastest FBO
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), 
								(LPVOID *)&pID3D11Texture2D_BackBuffer);

	//again get render targetView from d3d11 device using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, 
												NULL, //RTV-descriptor
												&gpID3D11RenderTargetView); //Location

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() is Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() is Succeed.\n");
		fclose(gpFile);
	}
	
	//Created locally
	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// VIMP : set render target view as render target : RTV , DSV
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL);

//Set Depth : as a Black Texture
	D3D11_TEXTURE2D_DESC textureDesc; //zbuffer
	ZeroMemory(&textureDesc,sizeof(D3D11_TEXTURE2D_DESC));

	//11 Members
	textureDesc.Width = (UINT)width;
	textureDesc.Height = (UINT)height;
	textureDesc.ArraySize = 1; //2d Image
	textureDesc.MipLevels = 1;
	//imp
	textureDesc.SampleDesc.Count = 1; // can be 4
	textureDesc.SampleDesc.Quality = 0; // Let DX Set
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	//Flags
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL; // most imp
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.MiscFlags = 0;
	textureDesc.CPUAccessFlags = 0;
	
	//Create Depth Buffer 
	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer;
	gpID3D11Device->CreateTexture2D(&textureDesc,
						NULL,
						&pID3D11Texture2D_DepthBuffer);

	//DSV
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc,sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	//2 Members
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer,
				&depthStencilViewDesc,
				&gpID3D11DepthStencilView);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() is Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() is Succeed.\n");
		fclose(gpFile);
	}
	
	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;


	// VIMP : set render target view as render target : RTV , DSV
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, 
											 gpID3D11DepthStencilView);


	// Set Viewport
	D3D11_VIEWPORT d3dViewPort;

	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	
	gpID3D11DeviceContext->RSSetViewports(1,&d3dViewPort); // Rasterizer stage

	//Projection
	Perspective_Projection_Matrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f),
									((float)width / (float)height),
									0.1f,100.0f);

	return(hr);
}

//Show
void Display(void) 
{
	// like Clear
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);

	//Depth
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView,
											D3D11_CLEAR_DEPTH,1.0f,0); // 0 - Stencil


	//selection of VB
	UINT stride = sizeof(float)*2;
	UINT offset = 0;

	gpID3D11DeviceContext->IASetVertexBuffers(0,	//slot
		1,	//VB Count
		&gpID3D11Buffer_TriVertexBuffer,	//VB
		&stride , 	//stride
		&offset	//offset
	);

	//Primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST);

	//Matrices
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX translationMatrix = XMMatrixIdentity();
	XMMATRIX wvpMatrix = XMMatrixIdentity();

	//Transformation
	worldMatrix = XMMatrixTranslation(-0.5f,-0.2f,3.0f);


	//MVP
	wvpMatrix = worldMatrix * viewMatrix * Perspective_Projection_Matrix;

	//Load data in CB (domain shader)
    CBUFFER_DOMAIN_SHADER constantBuffer_domainShader;
    
	constantBuffer_domainShader.WorldViewProjectionMatrix = wvpMatrix;
    
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_DomainShader,  
                                        0,              
                                        NULL,           
                                        &constantBuffer_domainShader,
                                        0,              
                                        0);     


    // Load data in CB (hull shader)
    CBUFFER_HULL_SHADER constantBuffer_hullShader;
    
	constantBuffer_hullShader.Hull_Constant_Function_Param = XMVectorSet(1, (float)guiNumberOfLineSegments, 0.0, 0.0); //
    
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_HullShader, 
                                        0,              
                                        NULL,           
                                        &constantBuffer_hullShader,
                                        0,              
                                        0); 

	// Seg										            
    TCHAR str[50];
    wsprintf(str, TEXT(" D3D Curve [Segments = %d ]"), guiNumberOfLineSegments);
    SetWindowText(ghwnd, str);
                                                           
  // Load data in CB (pixel shader)
    CBUFFER_PIXEL_SHADER constantBuffer_pixelShader;
   
    constantBuffer_pixelShader.LineColor = XMVectorSet(1.0, 0.5, 0.0, 0.0); 
   
    gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_PixelShader,  
			0,
			NULL,
			&constantBuffer_pixelShader,
			0,
			0);


	//Draw
	gpID3D11DeviceContext->Draw(4,0);
	
	// front and back buffer : SwapBuffers
	gpIDXGISwapChain->Present(0, 0); // Both 0 are imp
}


//Anim
void Update(void) 
{
	//updt

}


//safe Release
void UnInitialize(void) 
{
	// else of Toggle
	if (bFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,0, 0, 0, 0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if(gpID3D11Buffer_ConstantBuffer_PixelShader)
	{
		gpID3D11Buffer_ConstantBuffer_PixelShader->Release();
		gpID3D11Buffer_ConstantBuffer_PixelShader = NULL;
	}

	if(gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

// All Buffers
	if(gpID3D11RasterizerState)
	{
		gpID3D11RasterizerState->Release();
		gpID3D11RasterizerState = NULL;
	}


	if(gpID3D11HullShader)
	{
		gpID3D11HullShader->Release();
		gpID3D11HullShader = NULL;
	}

	if(gpID3D11DomainShader)
	{
		gpID3D11DomainShader->Release();
		gpID3D11DomainShader = NULL;
	}

	if(gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if(gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}


	//1
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	//2
	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	//3
	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	//4
	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	//5
	if (gpFile)
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "UnInitialize() Succeed \n");
		fprintf(gpFile, "File Closed Successfully \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

