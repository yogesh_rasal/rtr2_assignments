/*
	D3D Blue Window
*/



// Headers
#include<Windows.h>
#include<stdio.h> // For file IO
#include<d3d11.h> // For Dx

//Export libs
#pragma comment(lib, "d3d11.lib") 
#pragma comment(lib, "gdi32.lib") 
#pragma comment(lib, "user32.lib") 

//Macros
#define WIN_WIDTH 800 
#define WIN_HEIGHT 600 
#define UNICODE


// file IO
FILE *gpFile = NULL;
char gszLogFileName[] = "Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool bFullScreen = false;

float gClearColor[4];//RGBA

//for Dx
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

// Function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
HRESULT Initialize(void);
void Update(void);
void UnInitialize(void);
void Display(void); 
HRESULT Resize(int, int);
void ToggleFullScreen(void);


//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) 
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Direct3D11");
	bool bDone = false; 

	// Create Log File
	if (fopen_s(&gpFile, gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created\n ."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Successfully Created \n");
		fclose(gpFile);
	}

	//  WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Change 11
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register wnd class
	RegisterClassEx(&wndclass);
/*	fopen_s(&gpFile, gszLogFileName, "a+");
	fprintf_s(gpFile, "inside WndClass.. \n");
	fclose(gpFile);
*/
	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("Direct3D11 Blue Window : By YSR"),
			 WS_OVERLAPPEDWINDOW,
			 100, 100,
			 WIN_WIDTH,	WIN_HEIGHT,
			 NULL,
		  	 NULL,
		 	 hInstance,
			 NULL);

	ghwnd = hwnd;

	fopen_s(&gpFile, gszLogFileName, "a+");
	fprintf_s(gpFile, "Window is Created.\n");
	fclose(gpFile);

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	fopen_s(&gpFile, gszLogFileName, "a+");
	fprintf_s(gpFile, "Before Initialize() : hwnd %d \n",(int)ghwnd);
	fclose(gpFile);

	//Check
	HRESULT hr;
	hr = Initialize();

	fopen_s(&gpFile, gszLogFileName, "a+");
	fprintf_s(gpFile, "After Initialize() .\n");
	fclose(gpFile);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() Failed .\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() Succeed. \n");
		fclose(gpFile);
	}

	// Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else if (gbActiveWindow == true)
		{
			//Update();
		}

		Display();
	}

	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) 
{
	// Variables
	HRESULT hr;

	// code
	switch (iMsg) 
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
		{
			gbActiveWindow = true;
		}
		else
		{
			gbActiveWindow = false;
		}
		break;
	
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND: 
		return(0); // No Break

	case WM_SIZE:
	if(gpID3D11DeviceContext)
	{
		hr = Resize(LOWORD(lParam), HIWORD(lParam));
		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "Resize() Failed...\n");
			fclose(gpFile);
			return(hr);
		}
		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "Resize() Succeed.\n");
			fclose(gpFile);
		}
	}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'f':
		case 'F':
			if(bFullScreen == false)
			{
				ToggleFullScreen();
				bFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				bFullScreen = false;
			}
			
			break;
		
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		
		default:
			break;
		}
		break;

	case WM_DESTROY:
		UnInitialize(); 
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


//imp
void ToggleFullScreen(void) 
{
	MONITORINFO mi;
	mi = { sizeof(MONITORINFO) };

	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) &&
				GetMonitorInfo(	MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,	HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	
	else //already
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd,	HWND_TOP,
			0,	0,	0,	0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bFullScreen = false;
	}
}


//Major code
HRESULT Initialize(void) 
{
	// Variables
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE, //GC 
										 D3D_DRIVER_TYPE_WARP,   // WARP
										 D3D_DRIVER_TYPE_REFERENCE //sw
									   };

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; //lowest

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	// Code
	fopen_s(&gpFile, gszLogFileName, "a+");

	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);
	fprintf_s(gpFile, "in Initialize() : %d...\n",numDriverTypes);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC)); // To make members of structure 0.

	fprintf_s(gpFile, "in Initialize() : with dxgiSwapChainDesc...\n");
	fclose(gpFile);

	//Members
	dxgiSwapChainDesc.BufferCount = 1;

	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; //RGBA
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60; //RR
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;

	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;

	dxgiSwapChainDesc.SampleDesc.Count = 1; // as Dx gives +1
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE; //Toggle-Able


	//Get the Driver
	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex]; //current 

		//12 Members
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,						//Adapter
			d3dDriverType,				//Driver-type
			NULL,						//Rasterizer
			createDeviceFlags,			//Flags
			&d3dFeatureLevel_required,  //Feature : 11
			numFeatureLevels, 			//No. of FL
			D3D11_SDK_VERSION,			//Library's SDK
			&dxgiSwapChainDesc,			//SCD 
			&gpIDXGISwapChain,			//SC
			&gpID3D11Device,			//Device
			&d3dFeatureLevel_acquired,	// FL
			&gpID3D11DeviceContext);	//Device Context

		if (SUCCEEDED(hr))
		{
			break;
		}
	}

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Failed...\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Succeed. \n");
		fprintf_s(gpFile, "The chosen Driver is of : ");

		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type. \n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "WARP Type. \n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Software Ref Type. \n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type. \n");
		}
		fclose(gpFile);
	}

	//  (blue window)
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 1.0f;
	gClearColor[3] = 1.0f;

	// Warm up call
	hr = Resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize() Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize() Succeed.\n");
		fclose(gpFile);
	}
	return(S_OK);
}

//Resize
HRESULT Resize(int width, int height) 
{
	// Code:
	HRESULT hr = S_OK;

	// safe release : VIMP
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// resize swap chain 
	if(gpIDXGISwapChain)
	{
		gpIDXGISwapChain->ResizeBuffers(1, width, height,
										DXGI_FORMAT_R8G8B8A8_UNORM, 
										0);
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize:: gpIDXGISwapChain is Valid...\n");
		fclose(gpFile);
	}

	// Create Fastest FBO
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), 
								(LPVOID *)&pID3D11Texture2D_BackBuffer);

	//again get render targetView from d3d11 device using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, 
												NULL, //RTV-descriptor
												&gpID3D11RenderTargetView); //Location

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() is Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() is Succeed.\n");
		fclose(gpFile);
	}
	
	//Created locally
	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// VIMP : set render target view as render target : RTV , DSV
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL);

	// Set Viewport
	D3D11_VIEWPORT d3dViewPort;

	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort); // Rasterizer stage
	
	return(hr);
}

//Show
void Display(void) 
{
	// like Clear
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);

	// front and back buffer : SwapBuffers
	gpIDXGISwapChain->Present(0, 0); // Both 0 are imp
}


//Anim
void Update(void) 
{
	// Code:
}


//safe Release
void UnInitialize(void) 
{
	// else of Toggle
	if (bFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,0, 0, 0, 0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	//1
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	//2
	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	//3
	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	//4
	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	//5
	if (gpFile)
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "UnInitialize() Succeed \n");
		fprintf(gpFile, "File Closed Successfully \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

