/*
	 D3D11_Perspective
*/


// Headers
#include<Windows.h>
#include<stdio.h> // For file IO
#include<d3d11.h> // For Dx
#include<d3dcompiler.h> // for Shader Compile

#pragma warning(disable:4838)
#include "XNAMath\xnamath.h"
#include "WICTextureLoader.h"

//Export libs
#pragma comment(lib, "d3d11.lib") 
#pragma comment(lib, "gdi32.lib") 
#pragma comment(lib, "user32.lib") 
#pragma comment(lib, "D3dcompiler.lib") 
#pragma comment(lib, "DirectXTK.lib") 

//Macros
#define WIN_WIDTH 700 
#define WIN_HEIGHT 600 
#define UNICODE

using namespace DirectX;

// file IO
FILE *gpFile = NULL;
char gszLogFileName[] = "Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool bFullScreen = false;

float gClearColor[4];//RGBA

//for Dx
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;


//for Shaders
ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;

ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;

//for Pyramid
ID3D11Buffer *gpID3D11Buffer_PyramidVertexBuffer = NULL;
ID3D11Buffer *gpID3D11Buffer_PyramidTextureBuffer = NULL;

//cube
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Cube = NULL;
ID3D11Buffer *gpID3D11Buffer_CubeTextureBuffer = NULL;

//for Backface CULL : 3 Steps
ID3D11RasterizerState *gpID3D11RasterizerState = NULL;

//for Depth
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL; //DSV

// for Texture
ID3D11ShaderResourceView *gpID3D11ShaderResourceView_Texture_Pyramid = NULL;
ID3D11SamplerState *gpID3D11SamplerState_Texture_Pyramid = NULL;

ID3D11ShaderResourceView *gpID3D11ShaderResourceView_Texture_Cube = NULL;
ID3D11SamplerState *gpID3D11SamplerState_Texture_Cube = NULL;

//imp
struct CBUFFER
{
	XMMATRIX WorldViewProjection; // Model : World , Camera : View
};

XMMATRIX Perspective_Projection_Matrix ; 
float Angle = 0.0f;


// Function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
HRESULT Initialize(void);
HRESULT Resize(int, int);
void ToggleFullScreen(void);
void Display(void); 
void Update(void);
void UnInitialize(void);
HRESULT LoadD3DTexture(const wchar_t *,ID3D11ShaderResourceView **);

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) 
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Direct3D11");
	bool bDone = false; 

	// Create Log File
	if (fopen_s(&gpFile, gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created\n ."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Successfully Created \n");
		fclose(gpFile);
	}

	//  WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Change 11
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register wnd class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("Direct3D11 3D Texture : By YSR"),
			 WS_OVERLAPPEDWINDOW,
			 100, 100,
			 WIN_WIDTH,	WIN_HEIGHT,
			 NULL,
		  	 NULL,
		 	 hInstance,
			 NULL);

	ghwnd = hwnd;

	fopen_s(&gpFile, gszLogFileName, "a+");
	fprintf_s(gpFile, "Window is Created.\n");
	fclose(gpFile);

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Check
	HRESULT hr;
	hr = Initialize();

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() Failed .\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() Succeed. \n");
		fclose(gpFile);
	}

	// Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else if (gbActiveWindow == true)
		{
			Update();
		}

		Display();
	}

	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) 
{
	// Variables
	HRESULT hr;

	// code
	switch (iMsg) 
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
		{
			gbActiveWindow = true;
		}
		else
		{
			gbActiveWindow = false;
		}
		break;
	
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND: 
		return(0); // No Break

	case WM_SIZE:
	if(gpID3D11DeviceContext)
	{
		hr = Resize(LOWORD(lParam), HIWORD(lParam));

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "Resize() Failed...\n");
			fclose(gpFile);
			return(hr);
		}
		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "Resize() Succeed.\n");
			fclose(gpFile);
		}
	}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'f':
		case 'F':
			if(bFullScreen == false)
			{
				ToggleFullScreen();
				bFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				bFullScreen = false;
			}
			
			break;
		
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		
		default:
			break;
		}
		break;

	case WM_DESTROY:
		UnInitialize(); 
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


//imp
void ToggleFullScreen(void) 
{
	MONITORINFO mi;
	mi = { sizeof(MONITORINFO) };

	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) &&
				GetMonitorInfo(	MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,	HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	
	else //already
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd,	HWND_TOP,
			0,	0,	0,	0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bFullScreen = false;
	}
}


//Major code
HRESULT Initialize(void) 
{
	// Variables
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE, //GC 
										 D3D_DRIVER_TYPE_WARP,   // WARP
										 D3D_DRIVER_TYPE_REFERENCE //sw
									   };

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; //lowest

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	// Code
	fopen_s(&gpFile, gszLogFileName, "a+");

	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);
	fprintf_s(gpFile, "in Initialize() : %d...\n",numDriverTypes);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC)); // To make members of structure 0.

	fprintf_s(gpFile, "in Initialize() : with dxgiSwapChainDesc...\n");
	fclose(gpFile);

	//Members
	dxgiSwapChainDesc.BufferCount = 1;

	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; //RGBA
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60; //RR
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;

	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;

	dxgiSwapChainDesc.SampleDesc.Count = 1; // as Dx gives +1
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE; //Toggle-Able


	//Get the Driver
	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex]; //current 

		//12 Members
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,						//Adapter
			d3dDriverType,				//Driver-type
			NULL,						//Rasterizer
			createDeviceFlags,			//Flags
			&d3dFeatureLevel_required,  //Feature : 11
			numFeatureLevels, 			//No. of FL
			D3D11_SDK_VERSION,			//Library's SDK
			&dxgiSwapChainDesc,			//SCD 
			&gpIDXGISwapChain,			//SC
			&gpID3D11Device,			//Device
			&d3dFeatureLevel_acquired,	// FL
			&gpID3D11DeviceContext);	//Device Context

		if (SUCCEEDED(hr))
		{
			break;
		}
	}

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Failed...\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Succeed. \n");
		fprintf_s(gpFile, "The chosen Driver is of : ");

		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type. \n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "WARP Type. \n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Software Ref Type. \n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type. \n");
		}
		fclose(gpFile);
	}

	//Shaders , Input Layouts & Buffers
	// a. Vertex
	const char *VertexShaderSourceCode = 
	"cbuffer ConstantBuffer"	\
	"{"	\
	"float4x4 worldViewProjectionMatrix;"	\
	"}"	\
	"struct vertex_output"	\
	"{"	\
	"float4 position : SV_POSITION;"	\
	"float2 texcoord : TEXCOORD;"	\
	"};"	\
	"vertex_output main(float4 pos : POSITION, float2 texcoord : TEXCOORD)"	\
	"{"	\
	"vertex_output Output;"
	"Output.position = mul(worldViewProjectionMatrix,pos);"	\
	"Output.texcoord = texcoord;"
	"return(Output);"	\
	"}";

	//1
	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;		

	//2
	hr = D3DCompile(VertexShaderSourceCode, //src
					lstrlenA(VertexShaderSourceCode)+1, //len + EOF
					"VS",	//Code String
					NULL,	//No Macros
					D3D_COMPILE_STANDARD_FILE_INCLUDE, // take ur std
					"main", 	//Entry Point
					"vs_5_0",	//Feature Level
					0,		//How to compile
					0,  	//Effect Const
					&pID3DBlob_VertexShaderCode,	//Actual VS
					&pID3DBlob_Error	//for Errors
	);

	if(FAILED(hr))
	{
		if(pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"D3DCompile() Failed for Vertex Shader : %s \n",
						(char *)pID3DBlob_Error->GetBufferPointer() ); //convert
			fclose(gpFile);

			//imp : Like COM
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}

	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"D3DCompile() Succeed for Vertex Shader \n");						
			fclose(gpFile);
	}
	
	//3 : Create
	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(),	//Bytecode
											pID3DBlob_VertexShaderCode->GetBufferSize(),
											NULL,
											&gpID3D11VertexShader);


	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateVertexShader() Failed in Vertex Shader \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateVertexShader() Succeed in Vertex Shader \n");						
			fclose(gpFile);
	}
	
	//4
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader,
										0,	//shareable
										0);	//Member count 


	// b. Pixel = Fragment 
	const char *PixelShaderSourceCode = 
	"Texture2D myTexture2D;"	\
	"SamplerState mySamplerState;"	\
	"float4 main(float4 Position : SV_POSITION, float2 texcoord : TEXCOORD) : SV_TARGET"	\
	"{"	\
	"float4 Color = myTexture2D.Sample(mySamplerState,texcoord);" \
	"return(Color);"\
	"}";

	//1
	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;		

	//2
	hr = D3DCompile(PixelShaderSourceCode, //src
					lstrlenA(PixelShaderSourceCode)+1, //len + EOF
					"PS",	//Code String
					NULL,	//No Macros
					D3D_COMPILE_STANDARD_FILE_INCLUDE, // take ur std
					"main", 	//Entry Point
					"ps_5_0",	//Feature Level
					0,	//How to compile
					0,  //Effect Const
					&pID3DBlob_PixelShaderCode,	//Actual VS
					&pID3DBlob_Error	//for Errors
	);

	if(FAILED(hr))
	{
		if(pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"D3DCompile() Failed for Pixel Shader : %s \n",
						(char *)pID3DBlob_Error->GetBufferPointer() ); //convert
			fclose(gpFile);

			//imp : Like COM
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}

	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"D3DCompile() Succeed for Pixel Shader \n");						
			fclose(gpFile);
	}
	
	//3 : Create
	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(),	//Bytecode
											pID3DBlob_PixelShaderCode->GetBufferSize(),
											NULL,
											&gpID3D11PixelShader);


	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreatePixelShader() Failed \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreatePixelShader() Succeed in Pixel Shader \n");						
			fclose(gpFile);
	}	

	//4
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader,0,0);

//Input Layout : Like glBindAttribLoc
	D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];
	ZeroMemory(&inputElementDesc,sizeof(D3D11_INPUT_ELEMENT_DESC));

	//Members : Pos
	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT; //Pos : 3 , Tex : 2
	inputElementDesc[0].InputSlot = 0;	//Layout
	inputElementDesc[0].AlignedByteOffset = 0; //gap
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA; //Class slot
	inputElementDesc[0].InstanceDataStepRate = 0;

	//Texcoord : imp - 9
	inputElementDesc[1].SemanticName = "TEXCOORD";
	inputElementDesc[1].SemanticIndex = 0;
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32_FLOAT; // u v 
	inputElementDesc[1].InputSlot = 1;	//Layout slot
	inputElementDesc[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT; //gap
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA; //Class slot
	inputElementDesc[1].InstanceDataStepRate = 0;


  hr = gpID3D11Device->CreateInputLayout(inputElementDesc, //array
										 _ARRAYSIZE(inputElementDesc),
										 pID3DBlob_VertexShaderCode->GetBufferPointer(),
										 pID3DBlob_VertexShaderCode->GetBufferSize(),
										 &gpID3D11InputLayout );
	
	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateInputLayout() Failed \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateInputLayout() Succeed  \n");						
			fclose(gpFile);
	}	

	//Set IL 
	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

	//VIMP
	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode= NULL;
	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;


//Pyramid
//Vertex Buffer	
	float triVertices [] = 
	{
		0.0f,1.0f,0.0f,   //Apex
		-1.0f,-1.0f,1.0f, //Right
	    1.0f,-1.0f,1.0f , //Left

		//Right
		0.0f,1.0f,0.0f,
		1.0f,-1.0f,1.0f,
		1.0f,-1.0f,-1.0f,

		//Back
		0.0f,1.0f,0.0f, //apex
		1.0f,-1.0f,-1.0f, //Right
	    -1.0f,-1.0f,-1.0f , //Left

		//Left
		0.0f,1.0f,0.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f,-1.0f,1.0f 
	};

	//Create VB
	D3D11_BUFFER_DESC bufferDesc_Vertex_Tri; // 6 Members
	ZeroMemory(&bufferDesc_Vertex_Tri,sizeof(D3D11_BUFFER_DESC));

	bufferDesc_Vertex_Tri.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_Vertex_Tri.ByteWidth = sizeof(float)*_ARRAYSIZE(triVertices);
	bufferDesc_Vertex_Tri.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_Vertex_Tri.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	//b.
  hr = gpID3D11Device->CreateBuffer(&bufferDesc_Vertex_Tri,
 									NULL,
									&gpID3D11Buffer_PyramidVertexBuffer);	


	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateBuffer() Failed : VB \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateBuffer() Succeed : VB \n");						
			fclose(gpFile);
	}	

	//C. Copy
	D3D11_MAPPED_SUBRESOURCE mapped_Subresource;
	ZeroMemory(&mapped_Subresource,sizeof(D3D11_MAPPED_SUBRESOURCE)) ;

	//VIMP
	gpID3D11DeviceContext->Map(gpID3D11Buffer_PyramidVertexBuffer,0,
								D3D11_MAP_WRITE_DISCARD,0,
								&mapped_Subresource	);

	//like MMIO
	memcpy(mapped_Subresource.pData,triVertices,sizeof(triVertices));

	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_PyramidVertexBuffer,NULL);

//Text
	
	float triTexcoord[] =
	{
		//front
		0.5f,1.0f,
		0.0f,0.0f,
		1.0f,0.0f,

		//Right
		0.5f,1.0f,
		1.0f,0.0f,
		0.0f,0.0f,

		//Back
		0.5f,1.0f,
		1.0f,0.0f,
		0.0f,0.0f,

		//left
		0.5f,1.0f,
		0.0f,0.0f,
		1.0f,0.0f

	};
	
	//Create VB
	D3D11_BUFFER_DESC bufferDesc_TriTexture; // 6 Members
	ZeroMemory(&bufferDesc_TriTexture,sizeof(D3D11_BUFFER_DESC));

	bufferDesc_TriTexture.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_TriTexture.ByteWidth = sizeof(float)*_ARRAYSIZE(triTexcoord);
	bufferDesc_TriTexture.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_TriTexture.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	//b.
 	hr = gpID3D11Device->CreateBuffer(&bufferDesc_TriTexture,
 									NULL,
									&gpID3D11Buffer_PyramidTextureBuffer);	

	if(FAILED(hr))
	{
		fopen_s(&gpFile , gszLogFileName,"a+");
		fprintf_s(gpFile,"CreateBuffer() Failed : VB \n");						
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile , gszLogFileName,"a+");
		fprintf_s(gpFile,"CreateBuffer() Succeed : VB \n");						
		fclose(gpFile);
	}	

	//C. Copy
	D3D11_MAPPED_SUBRESOURCE mappedSubresource_TriTexure;
	ZeroMemory(&mappedSubresource_TriTexure,sizeof(D3D11_MAPPED_SUBRESOURCE)) ;

	//VIMP
	gpID3D11DeviceContext->Map(gpID3D11Buffer_PyramidTextureBuffer,0,
								D3D11_MAP_WRITE_DISCARD,0,
								&mappedSubresource_TriTexure);

	//like MMIO
	memcpy(mappedSubresource_TriTexure.pData,triTexcoord,sizeof(triTexcoord));

	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_PyramidTextureBuffer,NULL);


//Vertex Buffer	: Cube
	float vertices[] = 
	{
		//Top
		-1.0f,1.0f,1.0f,   //1
		1.0f,1.0f,1.0f,    //2
	    -1.0f,1.0f,-1.0f,  //3

		-1.0f,1.0f,-1.0f,  //3
		1.0f,1.0f,1.0f,    //2
		1.0f,1.0f,-1.0f,    //1

		//Bottom
		1.0f,-1.0f,-1.0f,
		1.0f,-1.0f,1.0f,
		-1.0f,-1.0f,-1.0f,

		-1.0f,-1.0f,-1.0f,
		1.0f,-1.0f,1.0f,
		-1.0f,-1.0f,1.0f,
		
		//Front
		-1.0f,1.0f,-1.0f,
		1.0f,1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,
		
		-1.0f,-1.0f,-1.0f,
		1.0f,1.0f,-1.0f,
		1.0f,-1.0f,-1.0f,

		//4 : Back
		1.0f,-1.0f,1.0f,
		1.0f,1.0f,1.0f,
		-1.0f,-1.0f,1.0f,
		
		-1.0f,-1.0f,1.0f,
		1.0f,1.0f,1.0f,
		-1.0f,1.0f,1.0f,

		//5 : Left
		-1.0f,1.0f,1.0f,
		-1.0f,1.0f,-1.0f,
		-1.0f,-1.0f,1.0f,
		
		-1.0f,-1.0f,1.0f,
		-1.0f,1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,


		//6 Right
		1.0f,-1.0f,-1.0f,
		1.0f,1.0f,-1.0f,
		1.0f,-1.0f,1.0f,
		
		1.0f,-1.0f,1.0f,
		1.0f,1.0f,-1.0f,
		1.0f,1.0f,1.0f

	};

	//Create VB
	D3D11_BUFFER_DESC bufferDesc_Vertex; // 6 Members
	ZeroMemory(&bufferDesc_Vertex,sizeof(D3D11_BUFFER_DESC));

	bufferDesc_Vertex.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_Vertex.ByteWidth = sizeof(float)*_ARRAYSIZE(vertices);
	bufferDesc_Vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_Vertex.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

//b.
 	hr = gpID3D11Device->CreateBuffer(&bufferDesc_Vertex,
 									NULL,
									&gpID3D11Buffer_VertexBuffer_Cube);	


	if(FAILED(hr))
	{
		fopen_s(&gpFile , gszLogFileName,"a+");
		fprintf_s(gpFile,"CreateBuffer() Failed : VB \n");						
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile , gszLogFileName,"a+");
		fprintf_s(gpFile,"CreateBuffer() Succeed : VB \n");						
		fclose(gpFile);
	}	

	//C. Copy
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;
	ZeroMemory(&mappedSubresource,sizeof(D3D11_MAPPED_SUBRESOURCE)) ;

	//VIMP
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Cube,0,
								D3D11_MAP_WRITE_DISCARD,0,
								&mappedSubresource	);

	//like MMIO
	memcpy(mappedSubresource.pData,vertices,sizeof(vertices));

	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Cube,NULL);

//Texture
	
  float texcoordcube[] =
   {
	//top
	0.0f,0.0f,
	0.0f,1.0f,
	1.0f,0.0f,

	1.0f,0.0f,
	0.0f,1.0f,
	1.0f,1.0f,

	//bottom
	0.0f,0.0f,
	0.0f,1.0f,
	1.0f,0.0f,

	1.0f,0.0f,
	0.0f,1.0f,
	1.0f,1.0f,

	//front
	0.0f,0.0f,
	0.0f,1.0f,
	1.0f,0.0f,

	1.0f,0.0f,
	0.0f,1.0f,
	1.0f,1.0f,

	//back
	0.0f,0.0f,
	0.0f,1.0f,
	1.0f,0.0f,

	1.0f,0.0f,
	0.0f,1.0f,
	1.0f,1.0f,

	//left
	0.0f,0.0f,
	0.0f,1.0f,
	1.0f,0.0f,

	1.0f,0.0f,
	0.0f,1.0f,
	1.0f,1.0f,

	//right
	0.0f,0.0f,
	0.0f,1.0f,
	1.0f,0.0f,

	1.0f,0.0f,
	0.0f,1.0f,
	1.0f,1.0f

   };
	
	//Create VB
	D3D11_BUFFER_DESC bufferDesc_Texture; // 6 Members
	ZeroMemory(&bufferDesc_Texture,sizeof(D3D11_BUFFER_DESC));

	bufferDesc_Texture.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_Texture.ByteWidth = sizeof(float)*_ARRAYSIZE(texcoordcube);
	bufferDesc_Texture.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_Texture.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	//b.
 hr = gpID3D11Device->CreateBuffer(&bufferDesc_Texture,
 									NULL,
									&gpID3D11Buffer_CubeTextureBuffer);	


	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateBuffer() Failed : VB \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateBuffer() Succeed : VB \n");						
			fclose(gpFile);
	}	

	//C. Copy
	D3D11_MAPPED_SUBRESOURCE mappedSubresource_Texture;
	ZeroMemory(&mappedSubresource_Texture,sizeof(D3D11_MAPPED_SUBRESOURCE)) ;

	//VIMP
	gpID3D11DeviceContext->Map(gpID3D11Buffer_CubeTextureBuffer,0,
								D3D11_MAP_WRITE_DISCARD,0,
								&mappedSubresource_Texture);

	//like MMIO
	memcpy(mappedSubresource_Texture.pData, texcoordcube,sizeof(texcoordcube));

	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_CubeTextureBuffer,NULL);



//Constant Buffer : Page 12

	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer,sizeof(D3D11_BUFFER_DESC));
	
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer,0,	&gpID3D11Buffer_ConstantBuffer);

	if(FAILED(hr))
	{
		fopen_s(&gpFile , gszLogFileName,"a+");
		fprintf_s(gpFile,"CreateBuffer() Failed : CB \n");						
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile , gszLogFileName,"a+");
		fprintf_s(gpFile,"CreateBuffer() Succeed : CB \n");						
		fclose(gpFile);
	}	

	//set CB
	gpID3D11DeviceContext->VSSetConstantBuffers(0,	//slot
		1,	//count
		&gpID3D11Buffer_ConstantBuffer //Buffer
	);

// Back-face CULL : 2 & 3
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory((void *)&rasterizerDesc,sizeof(D3D11_RASTERIZER_DESC));

	//10 Members
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	//for knowledge
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	//like OGL
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;

	hr = gpID3D11Device->CreateRasterizerState(&rasterizerDesc,&gpID3D11RasterizerState);

	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateRasterizerState() Failed : Culling \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateRasterizerState() Succeed : Culling \n");						
			fclose(gpFile);
	}	

	//set RS
	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	//tex 10
	hr = LoadD3DTexture(L"Stone.bmp", //BSTR
				&gpID3D11ShaderResourceView_Texture_Pyramid);

	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"LoadD3DTexture() Failed : Pyramid \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"LoadD3DTexture() Succeed : Pyramid  \n");						
			fclose(gpFile);
	}	

	//Sampler State
	D3D11_SAMPLER_DESC samplerDesc_YSR;
	ZeroMemory(&samplerDesc_YSR,sizeof(D3D11_SAMPLER_DESC));
	
	//4 members
	samplerDesc_YSR.Filter   = D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT; //all
	samplerDesc_YSR.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc_YSR.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc_YSR.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	hr = gpID3D11Device->CreateSamplerState(&samplerDesc_YSR,
				 &gpID3D11SamplerState_Texture_Pyramid);

	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateSamplerState() Failed for Pyramid \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateSamplerState() Succeed for Pyramid \n");						
			fclose(gpFile);
	}	


// Cube 11
	hr = LoadD3DTexture(L"Kundali.bmp", //BSTR
				&gpID3D11ShaderResourceView_Texture_Cube);

	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"LoadD3DTexture() Failed for cube \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"LoadD3DTexture() Succeed : Cube \n");						
			fclose(gpFile);
	}	

	//Sampler State
	D3D11_SAMPLER_DESC samplerDesc;
	ZeroMemory(&samplerDesc,sizeof(D3D11_SAMPLER_DESC));
	
	//4 members
	samplerDesc.Filter   = D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT; //all
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	hr = gpID3D11Device->CreateSamplerState(&samplerDesc,
				 &gpID3D11SamplerState_Texture_Cube);

	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateSamplerState() Failed for Cube \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateSamplerState() Succeed : Cube \n");						
			fclose(gpFile);
	}	


	// End
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 1.0f;

	Perspective_Projection_Matrix = XMMatrixIdentity();

	// Warm up call
	hr = Resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize() Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize() Succeed.\n");
		fclose(gpFile);
	}

	return(S_OK);
}

 //Texture
 HRESULT LoadD3DTexture(const wchar_t *texFileName, ID3D11ShaderResourceView **ppID3D11ShaderResourceView)
 {
	 HRESULT hr = S_OK;

	 //create texture 
	 hr = DirectX::CreateWICTextureFromFile(gpID3D11Device,
	 									gpID3D11DeviceContext,
										texFileName,
										NULL,	//out - used for TexMap
										ppID3D11ShaderResourceView );
	
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "CreateWICTextureFromFile() is Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "CreateWICTextureFromFile() is Succeed.\n");
		fclose(gpFile);
	}

  return(hr);

 }


//Resize
HRESULT Resize(int width, int height) 
{
	// Code:
	HRESULT hr = S_OK;

	// safe release : VIMP

	if(gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// resize swap chain 
	if(gpIDXGISwapChain)
	{
		gpIDXGISwapChain->ResizeBuffers(1, width, height,
										DXGI_FORMAT_R8G8B8A8_UNORM, 
										0);

		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize:: gpIDXGISwapChain is Valid...\n");
		fclose(gpFile);
	}

	// Create Fastest FBO
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), 
								(LPVOID *)&pID3D11Texture2D_BackBuffer);

	//again get render targetView from d3d11 device using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, 
												NULL, //RTV-descriptor
												&gpID3D11RenderTargetView); //Location

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() is Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() is Succeed.\n");
		fclose(gpFile);
	}
	
	//Created locally
	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	//Set Depth : as a Black Texture
	D3D11_TEXTURE2D_DESC textureDesc; //zbuffer
	ZeroMemory(&textureDesc,sizeof(D3D11_TEXTURE2D_DESC));

	//11 Members
	textureDesc.Width = (UINT)width;
	textureDesc.Height = (UINT)height;
	textureDesc.ArraySize = 1; //2d Image
	textureDesc.MipLevels = 1;
	//imp
	textureDesc.SampleDesc.Count = 1; // can be 4
	textureDesc.SampleDesc.Quality = 0; // Let DX Set
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	//Flags
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL; // most imp
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.MiscFlags = 0;
	textureDesc.CPUAccessFlags = 0;
	
	//Create Depth Buffer 
	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer;

	gpID3D11Device->CreateTexture2D(&textureDesc,
						NULL,
						&pID3D11Texture2D_DepthBuffer);

	//DSV
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc,sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	//2 Members
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer,
				&depthStencilViewDesc,
				&gpID3D11DepthStencilView);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() is Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() is Succeed.\n");
		fclose(gpFile);
	}
	
	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;


	// VIMP : set render target view as render target : RTV , DSV
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, 
											 gpID3D11DepthStencilView);

	// Set Viewport
	D3D11_VIEWPORT d3dViewPort;

	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort); // Rasterizer stage

	//Projection
	Perspective_Projection_Matrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f),
									((float)width / (float)height),
									0.1f,100.0f);

	return(hr);
}


//Show
void Display(void) 
{
	// like Clear
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);

	//Depth
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView,
											D3D11_CLEAR_DEPTH,1.0f,0); // 0 - Stencil

	//selection of VB
	UINT stride = sizeof(float)*3;
	UINT offset = 0;

	gpID3D11DeviceContext->IASetVertexBuffers(0,	//slot
		1,	//VB Count
		&gpID3D11Buffer_PyramidVertexBuffer,	//VB
		&stride , 	//stride
		&offset	//offset
		);

	//texture
	stride = sizeof(float)*2;
	offset = 0;

	gpID3D11DeviceContext->IASetVertexBuffers(1,	//slot
		1,	//VB Count
		&gpID3D11Buffer_PyramidTextureBuffer,	//VB
		&stride , 	//stride
		&offset	//offset
		);

	//Bind Texture
	gpID3D11DeviceContext->PSSetShaderResources(0, //SRV
							 			 	   1, //shader state
						&gpID3D11ShaderResourceView_Texture_Pyramid
						);

	gpID3D11DeviceContext->PSSetSamplers( 0, 1,
						&gpID3D11SamplerState_Texture_Pyramid);


	//Primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST); //strip

	//Matrices
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX translationMatrix = XMMatrixIdentity();
	XMMATRIX scaleMatrix = XMMatrixIdentity();
	XMMATRIX RotationMatrix = XMMatrixIdentity();

	//Transformation
	translationMatrix = XMMatrixTranslation(-1.5f,0.0f,5.0f);
	scaleMatrix = XMMatrixScaling(0.75f,0.75f,0.75f);
	RotationMatrix = XMMatrixRotationY(-Angle);

	worldMatrix =  RotationMatrix *scaleMatrix * translationMatrix ;

	//MVP
	XMMATRIX wvpMatrix = worldMatrix * viewMatrix * Perspective_Projection_Matrix;

	//Load data in CB
	CBUFFER constantBuffer;
	constantBuffer.WorldViewProjection = wvpMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer,
			0,
			NULL,
			&constantBuffer,
			0,
			0
	);

	//Draw Pyramid
	gpID3D11DeviceContext->Draw(12,0);


//Cube Using Triangle List
	stride = sizeof(float) * 3;
	offset = 0;

	gpID3D11DeviceContext->IASetVertexBuffers(0,	//slot
		1,	//VB Count
		&gpID3D11Buffer_VertexBuffer_Cube,	//VB
		&stride , 	//stride
		&offset	//offset
	);
	
	stride = sizeof(float) * 2;
	offset = 0;

	gpID3D11DeviceContext->IASetVertexBuffers(1,	//slot
		1,	//VB Count
		&gpID3D11Buffer_CubeTextureBuffer,	//VB
		&stride , 	//stride
		&offset	//offset
	);

	//Bind Texture
	gpID3D11DeviceContext->PSSetShaderResources(0, //SRV
							1, //shader state
				&gpID3D11ShaderResourceView_Texture_Cube );

	gpID3D11DeviceContext->PSSetSamplers(0, 1,
				&gpID3D11SamplerState_Texture_Cube );

	
	translationMatrix = XMMatrixTranslation(1.5f,0.0f,5.0f);
	scaleMatrix = XMMatrixScaling(0.75f,0.75f,0.75f);

	XMMATRIX R1 = XMMatrixRotationX(Angle);
	XMMATRIX R2 = XMMatrixRotationY(Angle);
	XMMATRIX R3 = XMMatrixRotationZ(Angle);


	RotationMatrix = R1 * R2 * R3;
	worldMatrix =  scaleMatrix * RotationMatrix * translationMatrix ;

	//MVP
	 wvpMatrix = worldMatrix * viewMatrix * Perspective_Projection_Matrix;

	//Load data in CB
	constantBuffer.WorldViewProjection = wvpMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer,
			0,
			NULL,
			&constantBuffer,
			0,
			0
	);

	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//Draw
	gpID3D11DeviceContext->Draw(6,0);
	gpID3D11DeviceContext->Draw(6,6);
	gpID3D11DeviceContext->Draw(6,12);
	gpID3D11DeviceContext->Draw(6,18);
	gpID3D11DeviceContext->Draw(6,24);
	gpID3D11DeviceContext->Draw(6,30);


	// front and back buffer : SwapBuffers
	gpIDXGISwapChain->Present(0, 0); // Both 0 are imp
}


//Anim
void Update(void) 
{
	//updt
	Angle = Angle + 0.003f;
	
	if(Angle >= 360.f)
	 Angle = Angle - 350.0f;

}


//safe Release
void UnInitialize(void) 
{
	// else of Toggle
	if (bFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,0, 0, 0, 0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if(gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if(gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

// All Buffers
	if(gpID3D11RasterizerState)
	{
		gpID3D11RasterizerState->Release();
		gpID3D11RasterizerState = NULL;
	}

	if(gpID3D11Buffer_VertexBuffer_Cube)
	{
		gpID3D11Buffer_VertexBuffer_Cube->Release();
		gpID3D11Buffer_VertexBuffer_Cube = NULL;
	}

	if(gpID3D11Buffer_PyramidVertexBuffer)
	{
		gpID3D11Buffer_PyramidVertexBuffer->Release();
		gpID3D11Buffer_PyramidVertexBuffer = NULL;
	}

	if(gpID3D11Buffer_CubeTextureBuffer)
	{
		gpID3D11Buffer_CubeTextureBuffer->Release();
		gpID3D11Buffer_CubeTextureBuffer = NULL;
	}

	if(gpID3D11Buffer_PyramidTextureBuffer)
	{
		gpID3D11Buffer_PyramidTextureBuffer->Release();
		gpID3D11Buffer_PyramidTextureBuffer = NULL;
	}

	if(gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if(gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}


	//1
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	//2
	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	//3
	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	//4
	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	//5
	if (gpFile)
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "UnInitialize() Succeed \n");
		fprintf(gpFile, "File Closed Successfully \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

