/*
	 D3D11_Perspective
*/



// Headers
#include<Windows.h>
#include<stdio.h> // For file IO
#include<d3d11.h> // For Dx
#include<d3dcompiler.h> // for Shader Compile

#pragma warning(disable:4838)
#include "XNAMath\xnamath.h"
#include "Sphere.h"

//Export libs
#pragma comment(lib, "d3d11.lib") 
#pragma comment(lib, "gdi32.lib") 
#pragma comment(lib, "user32.lib") 
#pragma comment(lib, "Sphere.lib") 
#pragma comment(lib, "D3dcompiler.lib") 

//Macros
#define WIN_WIDTH 800 
#define WIN_HEIGHT 600 
#define UNICODE


// file IO
FILE *gpFile = NULL;
char gszLogFileName[] = "Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool bFullScreen = false;
bool gbLight = false;  //imp
bool gbVPressed = false;

float gClearColor[4];//RGBA

//for Dx
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL; //DSV

//for Shaders
ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer = NULL;
ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;

//for Sphere
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Normal_Sphere = NULL; // imp
ID3D11Buffer *gpID3D11Buffer_IndexBuffer = NULL;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumElements;
unsigned int gNumVertices;

//imp : Sequence must be same in Shader
struct CBUFFER
{
	unsigned int LKeyPressed;
	unsigned int VKeyPressed;
	XMMATRIX WorldMatrix; // Model : World , Camera : View
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;
	XMVECTOR LD;
	XMVECTOR KD;
	XMVECTOR LA;
	XMVECTOR KA;
	XMVECTOR LS;
	XMVECTOR KS;
	float Material_Shininess;
	XMVECTOR LightPosition;
}ConstantBuffer;

//VIMP
float LightAmbient[]  = {0.2f, 0.2f, 0.2f, 1.0f};
float LightDiffuse[]  = {1.0f,1.0f,1.0f,1.0f};
float LightSpecular[] = {1.0f,1.0f,1.0f,1.0f};

float Material_Ambient[]  = {0.0f,0.0f,0.0f,1.0f};
float Material_Diffuse[]  = {1.0f,1.0f,1.0f,1.0f};
float Material_Specular[] = {1.0f,1.0f,1.0f,1.0f};

float Light_Pos[] = {100.0f,100.0f,-100.0f,1.0f}; //100

XMMATRIX Perspective_Projection_Matrix ; //Perspective

// Function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
HRESULT Initialize(void);
void Display(void); 
HRESULT Resize(int, int);
void ToggleFullScreen(void);
void Update(void);
void UnInitialize(void);


//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) 
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Direct3D11");
	bool bDone = false; 

	// Create Log File
	if (fopen_s(&gpFile, gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created\n ."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Successfully Created \n");
		fclose(gpFile);
	}

	//  WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Change 11
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register wnd class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("Direct3D11 Light Toggling on Sphere : By YSR"),
			 WS_OVERLAPPEDWINDOW,
			 100, 100,
			 WIN_WIDTH,	WIN_HEIGHT,
			 NULL,
		  	 NULL,
		 	 hInstance,
			 NULL);

	ghwnd = hwnd;

	fopen_s(&gpFile, gszLogFileName, "a+");
	fprintf_s(gpFile, "Window is Created.\n");
	fclose(gpFile);

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Check
	HRESULT hr;
	hr = Initialize();

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() Failed .\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() Succeed. \n");
		fclose(gpFile);
	}

	// Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else if (gbActiveWindow == true)
		{
			//Update();
		}

		Display();
	}

	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) 
{
	// Variables
	HRESULT hr;

	// code
	switch (iMsg) 
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
		{
			gbActiveWindow = true;
		}
		else
		{
			gbActiveWindow = false;
		}
		break;
	
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND: 
		return(0); // No Break

	case WM_SIZE:
	if(gpID3D11DeviceContext)
	{
		hr = Resize(LOWORD(lParam), HIWORD(lParam));

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "Resize() Failed...\n");
			fclose(gpFile);
			return(hr);
		}
		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "Resize() Succeed.\n");
			fclose(gpFile);
		}
	}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'f':
		case 'F':
			if(bFullScreen == false)
			{
				ToggleFullScreen();
				bFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				bFullScreen = false;
			}
			break;

		case 'l':
		case 'L':
			if(gbLight == false)
			{
			 	gbLight = true;
			}
			else if(gbLight == true)
			{
				gbLight = false;
			}
			break;

		case 'v':	
		case 'V': 
			if(gbVPressed == false)
			{
			 	gbVPressed = true;
			}
			else if(gbVPressed == true)
			{
				gbVPressed = false;
			}
			break;

		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		
		default:
			break;
		}
		break;

	case WM_DESTROY:
		UnInitialize(); 
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


//imp
void ToggleFullScreen(void) 
{
	MONITORINFO mi;
	mi = { sizeof(MONITORINFO) };

	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) &&
				GetMonitorInfo(	MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,	HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	
	else //already
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd,	HWND_TOP,
			0,	0,	0,	0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bFullScreen = false;
	}
}


//Major code
HRESULT Initialize(void) 
{
	// Variables
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE, //GC 
										 D3D_DRIVER_TYPE_WARP,   // WARP
										 D3D_DRIVER_TYPE_REFERENCE //sw
									   };

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; //lowest

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	// Code
	fopen_s(&gpFile, gszLogFileName, "a+");

	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);
	fprintf_s(gpFile, "in Initialize() : %d...\n",numDriverTypes);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC)); // To make members of structure 0.

	fprintf_s(gpFile, "in Initialize() : with dxgiSwapChainDesc...\n");
	fclose(gpFile);

	//Members
	dxgiSwapChainDesc.BufferCount = 1;

	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; //RGBA
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60; //RR
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;

	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;

	dxgiSwapChainDesc.SampleDesc.Count = 1; // as Dx gives +1
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE; //Toggle-Able


	//Get the Driver
	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex]; //current 

		//12 Members
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,						//Adapter
			d3dDriverType,				//Driver-type
			NULL,						//Rasterizer
			createDeviceFlags,			//Flags
			&d3dFeatureLevel_required,  //Feature : 11
			numFeatureLevels, 			//No. of FL
			D3D11_SDK_VERSION,			//Library's SDK
			&dxgiSwapChainDesc,			//SCD 
			&gpIDXGISwapChain,			//SC
			&gpID3D11Device,			//Device
			&d3dFeatureLevel_acquired,	// FL
			&gpID3D11DeviceContext);	//Device Context

		if (SUCCEEDED(hr))
		{
			break;
		}
	}

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Failed...\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Succeed. \n");
		fprintf_s(gpFile, "The chosen Driver is of : ");

		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type. \n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "WARP Type. \n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Software Ref Type. \n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type. \n");
		}
		fclose(gpFile);
	}

	//Shaders , Input Layouts & Buffers
	// a. Vertex
	const char *VertexShaderSourceCode = 
	"cbuffer ConstantBuffer"		\
	"{"	\
		"uint LKeyPressed;"			\
		"uint VKeyPressed;"			\
		"float4x4 worldMatrix;"		\
		"float4x4 viewMatrix;"		\
		"float4x4 ProjectionMatrix;" \
		"float4 LD;"	\
		"float4 KD;" 	\
		"float4 LA;"	\
		"float4 KA;"	\
		"float4 LS;"	\
		"float4 KS;"	\
		"float Material_shininess;" \
		"float4 Light_Position;"	\
	"}"	\
    "struct vertex_output"                             \
    "{"                                                \
        "float4 position        : SV_POSITION;"       	\
        "float3 t_norm          : NORMAL0;"             \
		"float3 light_dir       : NORMAL1;"             \
        "float3 viewer_vector   : NORMAL2;"             \
		"float4 phong_ads_light : NORMAL3;"				\
    "};"                                               \
	"vertex_output main(float4 pos : POSITION,float4 Normal : NORMAL)"\
	"{"	\
	"vertex_output Output;"	\
	"if(LKeyPressed == 1)"	\
	"{"						\
	  "if(VKeyPressed == 1)" \
		"{"\
		"float4 eye_coordinates = mul(worldMatrix,pos);"	\
		"eye_coordinates = mul(viewMatrix,eye_coordinates);"	\
		"float3 TNorm =  normalize(mul((float3x3)mul(worldMatrix,viewMatrix),Normal));"\
		"float3 S = (float3)(normalize(Light_Position - eye_coordinates)); "	\
		"float TN_dot_LD = max(dot(TNorm,S),0.0);"\
		"float4 ambient = LA * KA;"\
		"float4 diffuse = LD * KD * TN_dot_LD;"\
		"float3 reflection_vector = reflect(-S,TNorm);"\
		"float3 viewer_vector = normalize(-eye_coordinates.xyz);"\
		"float4 specular = LS * KS * pow(max(dot(reflection_vector,viewer_vector),0.0),Material_shininess);"\
		"Output.phong_ads_light = (ambient + diffuse + specular);"	\
		"}"\
	  "else"\
		"{"\
		"float4 eye_coordinates = mul(worldMatrix,pos);"				\
		"eye_coordinates = mul(viewMatrix, eye_coordinates);"			\
		"float3 TNorm = mul((float3x3)worldMatrix, (float3)Normal);"   \
		"float3 Direction = (float3)(Light_Position - eye_coordinates);"\
		"float3 viewer_vect = (-eye_coordinates.xyz);" 				\
        "Output.t_norm = TNorm;	"									\
        "Output.light_dir = Direction;"								\
        "Output.viewer_vector = viewer_vect;"						\
		"}"\
	"}"    											\
	"float4 worldView =  mul(worldMatrix,pos);"			 \
	"worldView = mul(viewMatrix ,worldView);" 			\
	"Output.position = mul(ProjectionMatrix,worldView);"  \
	"return(Output);"									  \
	"}";

	//1
	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;		

	//2
	hr = D3DCompile(VertexShaderSourceCode, //src
					lstrlenA(VertexShaderSourceCode)+1, //len + EOF
					"VS",	//Code String
					NULL,	//No Macros
					D3D_COMPILE_STANDARD_FILE_INCLUDE, // take ur std
					"main", 	//Entry Point
					"vs_5_0",	//Feature Level
					0,		//How to compile
					0,  	//Effect Const
					&pID3DBlob_VertexShaderCode,	//Actual VS
					&pID3DBlob_Error	//for Errors
	);

	if(FAILED(hr))
	{
		if(pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"D3DCompile() Failed for Vertex Shader : %s \n",
						(char *)pID3DBlob_Error->GetBufferPointer()); //convert
			fclose(gpFile);

			//imp : Like COM
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}

	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"D3DCompile() Succeed for Vertex Shader \n");						
			fclose(gpFile);
	}
	
	//3 : Create
	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(),	//Bytecode
											pID3DBlob_VertexShaderCode->GetBufferSize(),
											NULL,
											&gpID3D11VertexShader);


	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateVertexShader() Failed in Vertex Shader \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateVertexShader() Succeed in Vertex Shader \n");						
			fclose(gpFile);
	}
	
	//4
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader,
										0,	//shareable
										0);	//Member count 


	// b. Pixel = Fragment 
	const char *PixelShaderSourceCode = 
		"cbuffer ConstantBuffer"		\
		"{"	\
		"uint LKeyPressed;"			\
		"uint VKeyPressed;"			\
		"float4x4 worldMatrix;"		\
		"float4x4 viewMatrix;"		\
		"float4x4 ProjectionMatrix;" \
		"float4 LD;"	\
		"float4 KD;" 	\
		"float4 LA;"	\
		"float4 KA;"	\
		"float4 LS;"	\
		"float4 KS;"	\
		"float Material_shininess;" \
		"float4 Light_Position;"	\
	"}"	\
    "struct vertex_output"                                                                                                  \
    "{"                                                                                                                     \
        "float4 position        : SV_POSITION;"                                                                             \
        "float3 t_norm          : NORMAL0;"                                                                                 \
        "float3 light_dir       : NORMAL1;"                                                                                 \
        "float3 viewer_vec      : NORMAL2;"                                                                                 \
		"float4 phong_ads_light : NORMAL3;"				\
    "};"                                                                                                                    \
    "float4 main(float4 pos : SV_POSITION, vertex_output Input) : SV_TARGET"                                                \
    "{"                                                                                                                     \
       	 "float4 phong_light;"                                                 										\
         "if(LKeyPressed == 1)"
		  "{"\
			 "if (VKeyPressed == 1)"             \
        	 "{" \
			 "phong_light = Input.phong_ads_light;" \
			 "}"\
			"else"\
			"{"\
		    "float3 normalized_TNorm = normalize(Input.t_norm);"                                                           \
            "float3 normalized_S = normalize(Input.light_dir);"                                                             \
            "float3 normalized_viewer_vec = normalize(Input.viewer_vec);"                                                   \
            "float TN_dot_LD = max(dot(normalized_S, normalized_TNorm), 0.0);"                                             \
            "float4 ambient = LA * KA;"                                                                                     \
            "float4 diffuse = LD * KD * TN_dot_LD;"                                                                         \
            "float3 reflection_vec = reflect(-normalized_S, normalized_TNorm);"                                            \
            "float4 specular = LS * KS * pow(max(dot(reflection_vec, normalized_viewer_vec), 0.0), Material_shininess);"    \
			"phong_light = ambient + diffuse + specular;"                      \
			 "}"\
		   "}" \
 			"else"                                  \
      	    "{"                                           \
            "phong_light = float4(1.0, 1.0, 1.0, 1.0);"              \
        	"}"															\
        "return(phong_light);"                                       \
    "}";


	//1
	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;		

	//2
	hr = D3DCompile(PixelShaderSourceCode, //src
					lstrlenA(PixelShaderSourceCode)+1, //len + EOF
					"PS",	//Code String
					NULL,	//No Macros
					D3D_COMPILE_STANDARD_FILE_INCLUDE, // take ur std
					"main", 	//Entry Point
					"ps_5_0",	//Feature Level
					0,		//How to compile
					0,  	//Effect Const
					&pID3DBlob_PixelShaderCode,	//Actual VS
					&pID3DBlob_Error	//for Errors
	);

	if(FAILED(hr))
	{
		if(pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"D3DCompile() Failed for Pixel Shader : %s \n",
						(char *)pID3DBlob_Error->GetBufferPointer() ); //convert
			fclose(gpFile);

			//imp : Like COM
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}

	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"D3DCompile() Succeed for Pixel Shader \n");						
			fclose(gpFile);
	}
	
	//3 : Create
	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(),	//Bytecode
											pID3DBlob_PixelShaderCode->GetBufferSize(),
											NULL,
											&gpID3D11PixelShader);


	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreatePixelShader() Failed \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreatePixelShader() Succeed in Pixel Shader \n");						
			fclose(gpFile);
	}	

	//4
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader,0,0);

//Input Layout : Like glBindAttribLocation
	D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];
	ZeroMemory(&inputElementDesc,sizeof(D3D11_INPUT_ELEMENT_DESC));

	//Members
	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT; //Pos : 3 , Tex : 2
	inputElementDesc[0].InputSlot = 0;		//Layout
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;  //Class slot
	inputElementDesc[0].AlignedByteOffset = 0; 		//gap
	inputElementDesc[0].InstanceDataStepRate = 0;

	//Normals
	inputElementDesc[1].SemanticName = "NORMAL";
	inputElementDesc[1].SemanticIndex = 0;
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT; // u v 
	inputElementDesc[1].InputSlot = 1;		//Layout slot
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA; //Class slot
	inputElementDesc[1].AlignedByteOffset = 0;  // D3D11_APPEND_ALIGNED_ELEMENT
	inputElementDesc[1].InstanceDataStepRate = 0;


  hr = gpID3D11Device->CreateInputLayout(inputElementDesc, //array
										 _ARRAYSIZE(inputElementDesc), //2
										 pID3DBlob_VertexShaderCode->GetBufferPointer(),
										 pID3DBlob_VertexShaderCode->GetBufferSize(),
										 &gpID3D11InputLayout );
	
	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateInputLayout() Failed \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateInputLayout() Succeed  \n");						
			fclose(gpFile);
	}	

	//Set IL 
	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

	//VIMP
	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;
	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;


//Vertex & Index Buffer	with Sphere

	getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elements);

	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


// Vertices
	D3D11_BUFFER_DESC bufferDesc; // 6 Members
	ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));

	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_vertices); //try
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	//b.
 hr = gpID3D11Device->CreateBuffer(&bufferDesc,
 									NULL,
									&gpID3D11Buffer_VertexBuffer);	


	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateBuffer() Failed : VB \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateBuffer() Succeed : VB \n");						
			fclose(gpFile);
	}	

	//C. Copy Data
	D3D11_MAPPED_SUBRESOURCE mappedSubresource; //imp
	ZeroMemory(&mappedSubresource,sizeof(D3D11_MAPPED_SUBRESOURCE)) ;

	//VIMP
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer,0,
								D3D11_MAP_WRITE_DISCARD,0,
								&mappedSubresource);

	//like MMIO
	memcpy(mappedSubresource.pData,sphere_vertices,sizeof(sphere_vertices));

	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer,NULL);


//Normals

	ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));

	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_normals); //1
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;	// same
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	//b.
 	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
 									  NULL,
									 &gpID3D11Buffer_VertexBuffer_Normal_Sphere);	//imp


	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateBuffer() Failed : for Normals \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateBuffer() Succeed : for Normals  \n");						
			fclose(gpFile);
	}	

	//C. Copy & map data

	ZeroMemory(&mappedSubresource,sizeof(D3D11_MAPPED_SUBRESOURCE)) ;

	//VIMP
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Normal_Sphere,0, //3
								D3D11_MAP_WRITE_DISCARD,0,
								&mappedSubresource);

	//like MMIO
	memcpy(mappedSubresource.pData,sphere_normals,sizeof(sphere_normals)); //4

	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Normal_Sphere,NULL); //5
	
	
//elements : Create Index Buffer

 	ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));
	
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * _ARRAYSIZE(sphere_elements);
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER; // try D3D11_BIND_VERTEX_BUFFER
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
			NULL,
			&gpID3D11Buffer_IndexBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::IndexBuffer() Failed for elements. \n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::IndexBuffer()  done for elements \n");
		fclose(gpFile);
	}

	//Copy
	ZeroMemory((void *)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	gpID3D11DeviceContext->Map(gpID3D11Buffer_IndexBuffer, 0,
								D3D11_MAP_WRITE_DISCARD, 0, 
								&mappedSubresource);

	memcpy(mappedSubresource.pData, sphere_elements, sizeof(sphere_elements));
	
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_IndexBuffer, 0);


//Constant Buffer 

	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer,sizeof(D3D11_BUFFER_DESC));
	
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer,0,	
						&gpID3D11Buffer_ConstantBuffer);

	if(FAILED(hr))
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateBuffer() Failed : in ConstantBuffer \n");						
			fclose(gpFile);
			return(hr);
	}

	else
	{
			fopen_s(&gpFile , gszLogFileName,"a+");
			fprintf_s(gpFile,"CreateBuffer() Succeed : for ConstantBuffer \n");						
			fclose(gpFile);
	}	

	//set CB
	gpID3D11DeviceContext->VSSetConstantBuffers(0,	//slot
					1,	//count
				&gpID3D11Buffer_ConstantBuffer //Buffer
	);

	//PS set
	gpID3D11DeviceContext->PSSetConstantBuffers(0,	//slot
					1,	//count
				&gpID3D11Buffer_ConstantBuffer //Buffer
	);
	//Raster state 

	//   window
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 1.0f;

	Perspective_Projection_Matrix = XMMatrixIdentity();

	// Warm up call
	hr = Resize(WIN_WIDTH, WIN_HEIGHT);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize() Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize() Succeed.\n");
		fclose(gpFile);
	}

	return(S_OK);
}


//Resize
HRESULT Resize(int width, int height) 
{
	// Code:
	HRESULT hr = S_OK;

	// safe release : VIMP
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// resize swap chain 
	if(gpIDXGISwapChain)
	{
		gpIDXGISwapChain->ResizeBuffers(1, width, height,
										DXGI_FORMAT_R8G8B8A8_UNORM, 
										0);

		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize:: gpIDXGISwapChain is Valid...\n");
		fclose(gpFile);
	}

	// Create Fastest FBO
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), 
								(LPVOID *)&pID3D11Texture2D_BackBuffer);

	//again get render targetView from d3d11 device using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, 
												NULL, //RTV-descriptor
												&gpID3D11RenderTargetView); //Location

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() is Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() is Succeed.\n");
		fclose(gpFile);
	}
	
	//Created locally
	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

//Set Depth : as a Black Texture
	D3D11_TEXTURE2D_DESC textureDesc; //zbuffer
	ZeroMemory(&textureDesc,sizeof(D3D11_TEXTURE2D_DESC));

	//11 Members
	textureDesc.Width = (UINT)width;
	textureDesc.Height = (UINT)height;
	textureDesc.ArraySize = 1; //2d Image
	textureDesc.MipLevels = 1;
	//imp
	textureDesc.SampleDesc.Count = 1; // can be 4
	textureDesc.SampleDesc.Quality = 0; // Let DX Set
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	//Flags
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL; // most imp
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.MiscFlags = 0;
	textureDesc.CPUAccessFlags = 0;
	
	//Create Depth Buffer 
	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer;

	gpID3D11Device->CreateTexture2D(&textureDesc,
						NULL,
						&pID3D11Texture2D_DepthBuffer);

	//DSV
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc,sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	//2 Members
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer,
				&depthStencilViewDesc,
				&gpID3D11DepthStencilView);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() is Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() is Succeed.\n");
		fclose(gpFile);
	}
	
	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;

	// VIMP : set render target view as render target : RTV , DSV
//	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL);

// set DSV
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView);

	// Set Viewport
	D3D11_VIEWPORT d3dViewPort;

	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort); // Rasterizer stage

	//Projection
	Perspective_Projection_Matrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f),
									((float)width / (float)height),
									0.1f,100.0f);
	
	return(hr);
}

//Show
void Display(void) 
{
	// like Clear
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);


// Clear DSV
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, 
											D3D11_CLEAR_DEPTH, 
											1.0f,
											0);


	//selection of VB
	UINT stride = sizeof(float)*3;
	UINT offset = 0;

	gpID3D11DeviceContext->IASetVertexBuffers(0,	//slot
			1,	//VB Count
			&gpID3D11Buffer_VertexBuffer,	//VB
			&stride, 	//stride
			&offset		//offset
	);
	
	
	stride = sizeof(float) * 3;
	offset = 0;
	
	//Normals
		gpID3D11DeviceContext->IASetVertexBuffers(1,	//slot
			1,	//VB Count
			&gpID3D11Buffer_VertexBuffer_Normal_Sphere,	//VB
			&stride, 	//stride
			&offset		//offset
	);


	// IB
	gpID3D11DeviceContext->IASetIndexBuffer(gpID3D11Buffer_IndexBuffer,	//IB
						DXGI_FORMAT_R16_UINT, 	//short
						0 );

	//Primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//Matrices
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX translationMatrix = XMMatrixIdentity();
	XMMATRIX scaleMatrix = XMMatrixIdentity();
	XMMATRIX wvMatrix = XMMatrixIdentity();

	//Transformation
	translationMatrix = XMMatrixTranslation(0.0f,0.0f,2.0f);

	worldMatrix =  translationMatrix;

	//MVP
	ConstantBuffer.WorldMatrix = worldMatrix;
	ConstantBuffer.ViewMatrix =	viewMatrix;
	ConstantBuffer.ProjectionMatrix = Perspective_Projection_Matrix;

	//for Lights
	if(gbLight == true)
	{
		ConstantBuffer.LKeyPressed = 1;

		if(gbVPressed == true)
	  	  {
		   ConstantBuffer.VKeyPressed = 1; //imp
		  }
		//same
	/*	ConstantBuffer.WorldMatrix = worldMatrix;
		ConstantBuffer.ViewMatrix =	viewMatrix;
		ConstantBuffer.ProjectionMatrix = Perspective_Projection_Matrix;*/
		
		ConstantBuffer.LD = XMVectorSet(LightDiffuse[0], LightDiffuse[1], LightDiffuse[2], LightDiffuse[3]);
		ConstantBuffer.KD = XMVectorSet(Material_Diffuse[0],Material_Diffuse[1],Material_Diffuse[2],Material_Diffuse[3]);
		
		//new
		ConstantBuffer.LA = XMVectorSet(LightAmbient[0],LightAmbient[1],LightAmbient[2],LightAmbient[3]);
		ConstantBuffer.KA = XMVectorSet(Material_Ambient[0],Material_Ambient[1],Material_Ambient[2],Material_Ambient[3]);

		ConstantBuffer.LS = XMVectorSet(LightSpecular[0],LightSpecular[1],LightSpecular[2],LightSpecular[3]);
		ConstantBuffer.KS = XMVectorSet(Material_Specular[0],Material_Specular[1],Material_Specular[2],Material_Specular[3]);
		
		ConstantBuffer.Material_Shininess = 128.0f;
		ConstantBuffer.LightPosition = XMVectorSet(Light_Pos[0], Light_Pos[1], Light_Pos[2], Light_Pos[3]);
	}

	else 
	{
		ConstantBuffer.LKeyPressed = 0;
		ConstantBuffer.VKeyPressed = 0;
	}

	//CB
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer,
			0,
			NULL,
			&ConstantBuffer,
			0,
			0
	);

	//Draw sphere
	gpID3D11DeviceContext->DrawIndexed(gNumElements,0,0);// gNumVertices

	// front and back buffer : SwapBuffers
	gpIDXGISwapChain->Present(0, 0); // Both 0 are imp
}


//Anim
void Update(void) 
{
	
}


//safe Release
void UnInitialize(void) 
{
	// else of Toggle
	if (bFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,0, 0, 0, 0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if(gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if(gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if(gpID3D11Buffer_VertexBuffer)
	{
		gpID3D11Buffer_VertexBuffer->Release();
		gpID3D11Buffer_VertexBuffer = NULL;
	}

	if(gpID3D11Buffer_IndexBuffer)
	{
		gpID3D11Buffer_IndexBuffer->Release();
		gpID3D11Buffer_IndexBuffer = NULL;
	}

	if(gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if(gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}

	//1
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	//2
	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	//3
	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	//4
	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	//5
	if (gpFile)
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf(gpFile, "File Closed Successfully \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

