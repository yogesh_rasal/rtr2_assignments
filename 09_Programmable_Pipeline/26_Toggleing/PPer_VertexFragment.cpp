
/*

	Assignment :  Sphere 

*/

//Headers
#include <stdio.h>
#include <windows.h>
#include "GL/glew.h"
#include <stdlib.h>
#include "vmath.h"
#include "Sphere.h"

#pragma comment (lib,"opengl32.lib")
#pragma comment (lib,"gdi32.lib")
#pragma comment (lib,"user32.lib")
#pragma comment (lib,"glew32.lib")
#pragma comment (lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

// Globals
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
FILE *gpFile = NULL;

bool gbActiveWindow = false;
bool gbIsFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
GLenum Result;

//For Shaders
GLuint gVertex_Shader_Object;
GLuint gFragment_Shader_Object;
GLuint gShader_Program_Object_Vert;
GLuint gShader_Program_Object_Frag;

GLint iShaderCompileStatus = 0;
GLint iInfoLogLength = 0;
GLchar *szInfoLog = NULL;
GLint iProgrmLinkStatus = 0;

// New
GLuint mUniform;
mat4 Perspective_Projection_Matrix ;
mat4 View_Matrix;

GLuint gVao_sphere, gVbo_sphere_position, gVbo_sphere_normal, gVbo_sphere_element;
GLuint gNumElements, gNumVertices;

//for Lights

bool bLighting = false;
bool bVertex = true;
bool bFragment = false;

//12 Uniforms
GLuint Model_Uniform, Model_Uniform_Frag;
GLuint View_Uniform, View_Uniform_Frag;
GLuint Projection_Uniform, Projection_Uniform_Frag;

GLuint LD_Uniform, LA_Uniform, LS_Uniform;
GLuint KD_Uniform, KA_Uniform, KS_Uniform;

GLuint Material_Shininess;
GLuint Light_Position_Uniform;

//Fragment
GLuint mUniform_Frag;
GLuint LD_Uniform_Frag, LA_Uniform_Frag, LS_Uniform_Frag;
GLuint KD_Uniform_Frag, KA_Uniform_Frag, KS_Uniform_Frag;

GLuint Material_Shininess_Frag;
GLuint Light_Position_Uniform_Frag;
GLuint LisPressed_Uniform , LisPressed_Uniform_Frag;

//ENUM
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

GLsizei shaderCount, shaderNumber;

//for sphere

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

// Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int Initialize(void);
void Display(void);
void UnInitialize(void);
void Update(void);
void ToggleFullScreen(void);
void Resize(int, int);
void UnInitialize(void);
void PerVertex(void);
void PerFragment(void);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIns, LPSTR lpCmdLine, int iCmdShow)
{
	//code
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("PPShape");
	int iRet = 0;
	bool bDone = false;

	//File IO
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can not Created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	else
	{
		fprintf(gpFile, "Log File Successfully Created");
	}

	//initialize the class
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(hInstance, IDC_HAND);
	wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.cbWndExtra = 0;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("Per Vertex (V)- Per Fragment(C) ADS Lights"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100, 100, WIN_WIDTH, WIN_HEIGHT,
		NULL, NULL, hInstance, NULL);

	// For Update
	ghwnd = hwnd;
	iRet = Initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\n ChoosePixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -2)
	{
		fprintf(gpFile, "\n SetPixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -3)
	{
		fprintf(gpFile, "\n wglCreateContext Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -4)
	{
		fprintf(gpFile, "\n wglMakeCurrent Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == 0)
	{
		fprintf(gpFile, "\n Initialization Successful");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd); //UpdateWindow
	SetFocus(hwnd);

	// Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else if (gbActiveWindow == true)
		{
			Update();
		}

		Display();
	}

	return((int)msg.wParam);

}//Main Ends


 //WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_CREATE:
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;

		case 'L':
		case 'l':
			if (!bLighting)
			{
				bLighting = true;
			}
			else
			{
				bLighting = false;
			}
			break;
		
		case 'c':
		case 'C':
			if (bFragment == false)
			{
				bFragment = true;
				bVertex = false;

			}
			else
			{
				bFragment = false;
				bVertex = true;

			}
			break;

		case 'V':
		case 'v':
			if (bVertex == false)
			{
				bVertex = true;
				bFragment = false;

			}
			else
			{
				bVertex = false;
				bFragment = true;

			}
			break;

		}
		break;

	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}//WndProc Ends


 //init()
int Initialize(void)
{
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;

	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits   = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits  = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	else if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	//Bridging API
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	else if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}
		

	//*****************************

	Result = glewInit();

	if (Result != GLEW_OK)
	{
		UnInitialize();
	}

	//most imp calls
	PerVertex();  //must be done
	
	PerFragment();


	/*

	LD_Uniform, LA_Uniform, LS_Uniform
	KD_Uniform, KA_Uniform, KS_Uniform

	*/

	//drawing
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

//	Perspective_Projection_Matrix = perspective(45.0f, (GLfloat)(WIN_WIDTH) / (GLfloat)(WIN_HEIGHT), 0.1f, 100.0f);

	// vao
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// position vbo 
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);


	//Depth 
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	Perspective_Projection_Matrix = mat4::identity();
	

	//Warm up call 
	Resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}


//Resize
void Resize(int Width, int Height)
{
	if (Height == 0)
	{
		Height = 1;
	}

	glViewport(0, 0, GLsizei(Width), GLsizei(Height));
	
	Perspective_Projection_Matrix = perspective(45.0f, (GLfloat)(Width) / (GLfloat)(Height), 0.1f, 100.0f);

}

//new
void PerVertex(void)
{
	//code
	/*
	Add 6 Uniforms for Light
	*/

	//For Shaders : create 
	gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

	//Define VS code
	GLchar *VertexShaderCode =
		"#version 450 core"							\
		"\n"										\
		"in vec4 vPosition;"						\
		"in vec3 vNormal;"							\
		"out vec3 Phong_ADS_Light;"					\
		"\n"										\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform vec3 u_LA;"						\
		"uniform vec3 u_LD;"						\
		"uniform vec3 u_LS;"						\
		"uniform vec3 u_KA;"						\
		"uniform vec3 u_KD;"						\
		"uniform vec3 u_KS;"						\
		"uniform float u_Shininess;"				\
		"uniform int u_LKeyPressed;"				\
		"uniform vec4 u_Light_Position;"			\
		"\n"										\
		"void main(void)"							\
		"{"											\
		"if(u_LKeyPressed == 1)"					\
		"{"											\
		"vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"		\
		"vec3 T_Norm = normalize(mat3( u_view_matrix * u_model_matrix)* vNormal);"	\
		"vec3 Light_Direction = normalize(vec3(u_Light_Position) - (Eye_Coordinates.xyz));"	\
		"float TN_Dot_LD = max(dot(Light_Direction,T_Norm), 0);"\
		"vec3 Reflecion_Vector = reflect(-Light_Direction, T_Norm);"\
		"vec3 Viewer_Vector = normalize(-Eye_Coordinates.xyz);"		\
		"vec3 Ambient = vec3(u_LA * u_KA);"			    \
		"vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);" \
		"vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflecion_Vector, Viewer_Vector), 0.0), u_Shininess));" \
		"Phong_ADS_Light = Ambient + Diffuse + Specular;" \
		"}"		\
		"else"	\
		"{"		\
		"Phong_ADS_Light = vec3(1.0,1.0,1.0);"	\
		"}"		\
		"gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"	\
		"}";

	//Specify code to Obj
	glShaderSource(gVertex_Shader_Object,
		1,
		(const GLchar**)&VertexShaderCode,
		NULL);

	//Compile the shader
	glCompileShader(gVertex_Shader_Object);

	//Error Code for VS
	glGetShaderiv(gVertex_Shader_Object,
		GL_COMPILE_STATUS,
		&iShaderCompileStatus);


	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertex_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gVertex_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n %s Failed in Vertex Shader", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//2 - FS
	gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

	//Define FS code
	GLchar *FragmentShaderCode =
		"#version 450 core"			\
		"\n"						\
		"in vec3 Phong_ADS_Light;"	\
		"out vec4 FragColor;"		\
		"uniform int u_LKeyPressed;"\
		"\n"						\
		"void main(void)"			\
		"{"							\
		"if(u_LKeyPressed == 1)"	\
		"{"							\
		"FragColor = vec4(Phong_ADS_Light,1.0);"		\
		"}"							\
		"else"						\
		"{"							\
		"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		"}"							\
		"}";

	//Specify code to Obj
	glShaderSource(gFragment_Shader_Object, 1,
		(GLchar**)&FragmentShaderCode, NULL);

	//Compile the shader
	glCompileShader(gFragment_Shader_Object);

	//Error check for FS
	glGetShaderiv(gFragment_Shader_Object,
		GL_COMPILE_STATUS,
		&iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragment_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gFragment_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Fragment Shader : %s", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//3 - Create Shader Program
	gShader_Program_Object_Vert = glCreateProgram();

	//attach VS & FS
	glAttachShader(gShader_Program_Object_Vert, gVertex_Shader_Object);

	glAttachShader(gShader_Program_Object_Vert, gFragment_Shader_Object);

	//prelink
	glBindAttribLocation(gShader_Program_Object_Vert, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShader_Program_Object_Vert, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//Link SP
	glLinkProgram(gShader_Program_Object_Vert);

	// Error check for SP
	glGetProgramiv(gShader_Program_Object_Vert, GL_LINK_STATUS, &iProgrmLinkStatus);


	if (iProgrmLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShader_Program_Object_Vert, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetProgramInfoLog(gShader_Program_Object_Vert, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Shader Program Linking , %s", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//postlink
	mUniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_model_matrix");
	Projection_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_projection_matrix");
	View_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_view_matrix");

	LD_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_LD");
	KD_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_KD");

	LA_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_LA");
	KA_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_KA");

	LS_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_LS");
	KS_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_KS");

	LisPressed_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_LKeyPressed");
	Light_Position_Uniform = glGetUniformLocation(gShader_Program_Object_Vert, "u_Light_Position");
	Material_Shininess = glGetUniformLocation(gShader_Program_Object_Vert, "u_Shininess");

}

//new
void PerFragment(void)
{
	/*
	Add 12 Uniforms for Light
	*/

	//For Shaders : create 
	gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

	//Define VS code
	GLchar *VertexShaderCode =
		"#version 450 core"							\
		"\n"										\
		"in vec4 vPosition;"						\
		"in vec3 vNormal;"							\
		"out vec3 T_Norm;"							\
		"out vec3 Light_Direction;"					\
		"out vec3 Viewer_Vector;"					\
		"\n"										\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform vec3 u_LA;"						\
		"uniform vec3 u_LD;"						\
		"uniform vec3 u_LS;"						\
		"uniform vec3 u_KA;"						\
		"uniform vec3 u_KD;"						\
		"uniform vec3 u_KS;"						\
		"uniform float u_Shininess;"				\
		"uniform int u_LKeyPressed;"				\
		"uniform vec4 u_Light_Position;"			\
		"\n"										\
		"void main(void)"							\
		"{"											\
		"vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"	\
		"T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"			\
		"Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"	\
		"Viewer_Vector = vec3(-Eye_Coordinates.xyz);"\
		"gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"	\
		"}";

	//Specify code to Obj
	glShaderSource(gVertex_Shader_Object,
		1,
		(const GLchar**)&VertexShaderCode,
		NULL);

	//Compile the shader
	glCompileShader(gVertex_Shader_Object);

	//Error Code for VS
	glGetShaderiv(gVertex_Shader_Object,
		GL_COMPILE_STATUS,
		&iShaderCompileStatus);


	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertex_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gVertex_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n %s Failed in Vertex Shader", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//2 - FS
	gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

	//Define FS code : "precision highp float;"	
	GLchar *FragmentShaderCode =
		"#version 450 core"			\
		"\n"						\
		"in vec3 T_Norm;"			\
		"in vec3 Light_Direction;"	\
		"in vec3 Viewer_Vector;"	\
		"out vec4 FragColor;"		\
		"\n"						\
		"uniform vec3 u_LA;"		\
		"uniform vec3 u_LD;"		\
		"uniform vec3 u_LS;"		\
		"uniform vec3 u_KA;"		\
		"uniform vec3 u_KD;"		\
		"uniform vec3 u_KS;"		\
		"uniform float u_Shininess;"	\
		"uniform int u_LKeyPressed;"	\
		"uniform vec4 u_Light_Position;"\
		"\n"							\
		"void main(void)"			\
		"{"							\
		"if(u_LKeyPressed == 1)"	\
		"{"							\
		"vec3 Normalized_View_Vector = normalize(Viewer_Vector);"	\
		"vec3 Normalized_Light_Direction = normalize(Light_Direction);"\
		"vec3 Normalized_TNorm = normalize(T_Norm);"\
		"float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0);"	\
		"vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm);"\
		"vec3 Ambient = vec3(u_LA * u_KA);"	\
		"vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);"	\
		"vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess));" \
		"vec3 Phong_ADS_Light = Ambient + Diffuse+ Specular;"	\
		"FragColor = vec4(Phong_ADS_Light,1.0);" \
		"}"							\
		"else"						\
		"{"							\
		"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		"}"							\
		"}";


	//Specify code to Obj
	glShaderSource(gFragment_Shader_Object, 1,
		(GLchar**)&FragmentShaderCode, NULL);

	//Compile the shader
	glCompileShader(gFragment_Shader_Object);

	//Error check for FS
	glGetShaderiv(gFragment_Shader_Object,
		GL_COMPILE_STATUS,
		&iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragment_Shader_Object,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gFragment_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Fragment Shader : %s", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//3 - Create Shader Program
	gShader_Program_Object_Frag = glCreateProgram();

	//attach VS & FS
	glAttachShader(gShader_Program_Object_Frag, gVertex_Shader_Object);

	glAttachShader(gShader_Program_Object_Frag, gFragment_Shader_Object);

	//prelink
	glBindAttribLocation(gShader_Program_Object_Frag, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShader_Program_Object_Frag, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//Link SP
	glLinkProgram(gShader_Program_Object_Frag);

	// Error check for SP
	glGetProgramiv(gShader_Program_Object_Frag, GL_LINK_STATUS, &iProgrmLinkStatus);


	if (iProgrmLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShader_Program_Object_Frag, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetProgramInfoLog(gShader_Program_Object_Frag, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Shader Program Linking , %s", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//postlink
	mUniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_model_matrix");
	Projection_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_projection_matrix");
	View_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_view_matrix");

	LD_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_LD");
	KD_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_KD");

	LA_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_LA");
	KA_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_KA");

	LS_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_LS");
	KS_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_KS");

	Light_Position_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_Light_Position");
	Material_Shininess_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_Shininess");

	//common
	LisPressed_Uniform_Frag = glGetUniformLocation(gShader_Program_Object_Frag, "u_LKeyPressed");
}


//Display
void Display(void)
{
	//1
	mat4 Model_Matrix;
	mat4 View_Matrix;
	mat4 Projection_Matrix;
	mat4 Translation_Matrix;
	mat4 Rotation_Matrix;
	mat4 ModelView_Projection_Matrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//code : 9 steps

	//2 : I[]
	Model_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	View_Matrix = mat4::identity();
	Rotation_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//4 : Do Mat mul
	Translation_Matrix = translate(0.1f, 0.0f, -3.0f);

	Model_Matrix = Translation_Matrix;

	ModelView_Projection_Matrix = (Perspective_Projection_Matrix * View_Matrix * Model_Matrix);


	// 5	: send it to shader

	if (bVertex)
	{
		glUseProgram(gShader_Program_Object_Vert);
	}

	else if (bFragment)
	{
		glUseProgram(gShader_Program_Object_Frag);
	}

	
		glUniformMatrix4fv(mUniform, 1, GL_FALSE, Model_Matrix);
	
	    glUniformMatrix4fv(View_Uniform, 1, GL_FALSE, View_Matrix);
	
	    glUniformMatrix4fv(Projection_Uniform, 1, GL_FALSE, Perspective_Projection_Matrix);
	
	
	if (bLighting == true)
	{

	 if(bVertex == true)
	  {
		glUniform1i(LisPressed_Uniform, 1);
		glUniform3f(LD_Uniform, 0.85f, 0.85f, 0.85f);
		glUniform3f(KD_Uniform, 1.0f, 1.0f, 1.0f);

		glUniform3f(LS_Uniform, 0.85f, 0.85f, 0.85f);
		glUniform3f(KS_Uniform, 1.0f, 1.0f, 1.0f);
		
		glUniform3f(LA_Uniform, 0.25f, 0.25f, 0.25f);
		glUniform3f(KA_Uniform, 0.25f, 0.25f, 0.25f);
		
		glUniform1f(Material_Shininess, 50.0f);
		
	//imp : out to in Light
		glUniform4f(Light_Position_Uniform, 125.0f, 125.0f, 125.0f, 1.0f);

	 }

		else if (bFragment == true)
		{
			glUniform1i(LisPressed_Uniform_Frag, 1);
			glUniform3f(LD_Uniform_Frag, 0.85f, 0.85f, 0.85f);
			glUniform3f(KD_Uniform_Frag, 1.0f, 1.0f, 1.0f);

			glUniform3f(LS_Uniform_Frag, 0.85f, 0.85f, 0.85f);
			glUniform3f(KS_Uniform_Frag, 1.0f, 1.0f, 1.0f);

			glUniform3f(LA_Uniform_Frag, 0.25f, 0.25f, 0.25f);
			glUniform3f(KA_Uniform_Frag, 0.25f, 0.25f, 0.25f);

			glUniform1f(Material_Shininess_Frag, 128.0f);

			//imp : out to in Light
			//glUniform4fv(Light_Position_Uniform, 1, (GLfloat *)lightPosition);
			glUniform4f(Light_Position_Uniform_Frag, 125.0f, 125.0f, 125.0f, 1.0f);

			glUniformMatrix4fv(mUniform_Frag, 1, GL_FALSE, Model_Matrix);
			glUniformMatrix4fv(Projection_Uniform_Frag, 1, GL_FALSE, Perspective_Projection_Matrix);
			glUniformMatrix4fv(View_Uniform_Frag, 1, GL_FALSE, View_Matrix);
		}
	}

	else
	{
		glUniform1i(LisPressed_Uniform, 0);
	}



	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	//  glDrawArrays() 
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	//Common
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void Update(void)
{
	
}


//ToggleFullScreen
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	mi.cbSize = sizeof(MONITORINFO);

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//mi = { sizeof(MONITORINFO) };// not supported in VS2008

		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			if ((GetWindowPlacement(ghwnd, &wpPrev)) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),// WD
					(mi.rcMonitor.bottom - mi.rcMonitor.top), // HT
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}

	else
		if (gbIsFullScreen == true)
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

			SetWindowPlacement(ghwnd, &wpPrev);

			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
				SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

			ShowCursor(TRUE);
			gbIsFullScreen = false;

		}
}


//UnInitialize
void UnInitialize(void)
{
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	//for Shaders
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}


	if (gVbo_sphere_normal)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}


	glUseProgram(gShader_Program_Object_Vert);
	glDetachShader(gShader_Program_Object_Vert, gFragment_Shader_Object);
	glDetachShader(gShader_Program_Object_Vert, gVertex_Shader_Object);

	glDeleteShader(gFragment_Shader_Object);
	gFragment_Shader_Object = 0;

	glDeleteShader(gVertex_Shader_Object);
	gVertex_Shader_Object = 0;

	glDeleteProgram(gShader_Program_Object_Vert);
	glDeleteProgram(gShader_Program_Object_Frag);
	glUseProgram(0);

	
	//Break Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "\n Application Closed ");
	fclose(gpFile);
	gpFile = NULL;
}

