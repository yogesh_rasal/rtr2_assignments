
/*

	Assignment :  Inter 

*/

//Headers
#include <stdio.h>
#include <windows.h>
#include "GL/glew.h"
#include <stdlib.h>
#include "vmath.h"
#include "resource.h"

#pragma comment (lib,"opengl32.lib")
#pragma comment (lib,"gdi32.lib")
#pragma comment (lib,"user32.lib")
#pragma comment (lib,"glew32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

// Globals
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
FILE *gpFile = NULL;

bool gbActiveWindow = false;
bool gbIsFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
GLenum Result;

//For Shaders
GLuint gVertex_Shader_Object;
GLuint gFragment_Shader_Object;
GLuint gShader_Program_Object;

GLint iShaderCompileStatus = 0;
GLint iInfoLogLength = 0;
GLchar *szInfoLog = NULL;
GLint iProgrmLinkStatus = 0;

// New
GLuint mvpUniform;
mat4 Perspective_Projection_Matrix;
mat4 View_Matrix;

//Pyramid
GLuint Vao_pyramid;
GLuint Vbo_pyramid;

//Cube
GLuint Vao_cube, Vbo_cube;

//Texture
GLuint Texture_Sampler_Uniform;
GLuint Texture_Marble;

//for Lights
bool bLighting = false;
bool bAnimate = false;
GLfloat AngleTri = 0.0f;

//12 Uniforms
GLuint Model_Uniform;
GLuint View_Uniform;
GLuint Projection_Uniform;

GLuint LD_Uniform, LA_Uniform, LS_Uniform;
GLuint KD_Uniform, KA_Uniform, KS_Uniform;

GLuint Material_Shininess;
GLuint Light_Position_Uniform;
GLuint LisPressed_Uniform;

//ENUM
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

GLsizei shaderCount, shaderNumber;

// Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int Initialize(void);
void Display(void);
void UnInitialize(void);
void Update(void);
void ToggleFullScreen(void);
void Resize(int, int);
void UnInitialize(void);

//imp
BOOL LoadTexture(GLuint *, TCHAR[]);


//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIns, LPSTR lpCmdLine, int iCmdShow)
{
	//code
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("PPShape");
	int iRet = 0;
	bool bDone = false;

	//File IO
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can not Created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	else
	{
		fprintf(gpFile, "Log File Successfully Created");
	}

	//initialize the class
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(hInstance, IDC_HAND);
	wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.cbWndExtra = 0;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("3D Shapes : InterLeaved Array By YSR"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100, 100, WIN_WIDTH, WIN_HEIGHT,
		NULL, NULL, hInstance, NULL);

	// For Update
	ghwnd = hwnd;
	iRet = Initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\n ChoosePixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -2)
	{
		fprintf(gpFile, "\n SetPixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -3)
	{
		fprintf(gpFile, "\n wglCreateContext Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -4)
	{
		fprintf(gpFile, "\n wglMakeCurrent Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == 0)
	{
		fprintf(gpFile, "\n Initialization Successful");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd); //UpdateWindow
	SetFocus(hwnd);

	// Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else if (gbActiveWindow == true)
		{
			Update();
		}

		Display();
	}

	return((int)msg.wParam);

}//Main Ends


 //WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_CREATE:
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;

		case 'L':
		case 'l':
			if (!bLighting)
			{
				bLighting = true;
			}
			else
			{
				bLighting = false;
			}
			break;
		
		case 'A':
		case 'a':
			if (!bAnimate)
			{
				bAnimate = true;
			}
			else
			{
				bAnimate = false;
			}
			break;
		}
		break;

	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}//WndProc Ends


 //init()
int Initialize(void)
{
	
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;

	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits   = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits  = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	else if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	//Bridging API
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	else if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}
		

	//*****************************

	Result = glewInit();

	if (Result != GLEW_OK)
	{
		UnInitialize();
	}


	/*
			Add 12 Uniforms for Light
 	*/

	//For Shaders : create 
	gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

	//Define VS code
	GLchar *VertexShaderCode =
		"#version 450 core"							\
		"\n"										\
		"in vec4 vPosition;"						\
		"in vec4 vColor;"							\
		"in vec3 vNormal;"							\
		"in vec2 vTexture_Coord;"					\
		"out vec4 Out_Color;"						\
		"out vec2 out_texture_coord;"				\
		"out vec3 T_Norm;"							\
		"out vec3 Light_Direction;"					\
		"out vec3 Viewer_Vector;"					\
		"\n"										\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform int u_LKeyPressed;"				\
		"uniform vec4 u_Light_Position;"			\
		"\n"										\
		"void main(void)"							\
		"{"											\
		"if(u_LKeyPressed == 1)"					\
		"{"											\
		"vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"	\
		"T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"				\
		"Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"		\
		"Viewer_Vector = vec3(-Eye_Coordinates.xyz);"	\
		"}"		\
		"gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"	\
		"out_texture_coord = vTexture_Coord;"		\
		"Out_Color = vColor;"						\
		"}" ;

	//Specify code to Obj
	glShaderSource(gVertex_Shader_Object,
					1,
				(const GLchar**)&VertexShaderCode,
				NULL);

	//Compile the shader
	glCompileShader(gVertex_Shader_Object);

	//Error Code for VS
	glGetShaderiv(gVertex_Shader_Object,
				GL_COMPILE_STATUS,
				&iShaderCompileStatus);


	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertex_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gVertex_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n %s Failed in Vertex Shader",szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//2 - FS
	gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

	//Define FS code : "precision highp float;"	
	GLchar *FragmentShaderCode =
		"#version 450 core"				\
		"\n"							\
		"in vec2 out_texture_coord;"	\
		"in vec3 T_Norm;"				\
		"in vec3 Light_Direction;"		\
		"in vec3 Viewer_Vector;"		\
		"in vec4 Out_Color;"			\
		"out vec4 FragColor;"			\
		"uniform sampler2D u_texture_sampler;"	\
		"uniform vec3 u_LA;"				\
		"uniform vec3 u_LD;"				\
		"uniform vec3 u_LS;"				\
		"uniform vec3 u_KA;"				\
		"uniform vec3 u_KD;"				\
		"uniform vec3 u_KS;"				\
		"uniform float u_Shininess;"		\
		"uniform int u_LKeyPressed;"		\
		"uniform vec4 u_Light_Position;"	\
		"\n"								\
		"void main(void)"					\
		"{"									\
		"if(u_LKeyPressed == 1)"			\
		"{"									\
		"vec3 Normalized_View_Vector = normalize(Viewer_Vector);"	\
		"vec3 Normalized_Light_Direction = normalize(Light_Direction);"\
		"vec3 Normalized_TNorm = normalize(T_Norm);"\
		"float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0);"	\
		"vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm);"\
		"vec3 Ambient = vec3(u_LA * u_KA);"	\
		"vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);"	\
		"vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess));" \
		"vec3 Phong_ADS_Light = Ambient + Diffuse+ Specular;"	\
		"FragColor = vec4(Phong_ADS_Light,1.0) * texture(u_texture_sampler,out_texture_coord) * Out_Color;" \
		"}"							\
		"else"						\
		"{"							\
		"FragColor = texture(u_texture_sampler,out_texture_coord) * Out_Color;" \
		"}"							\
		"}" ;


	//Specify code to Obj
	glShaderSource(gFragment_Shader_Object,1,
				  ( GLchar**)&FragmentShaderCode,NULL);

	//Compile the shader
	glCompileShader(gFragment_Shader_Object);

	//Error check for FS
	glGetShaderiv(gFragment_Shader_Object,
				GL_COMPILE_STATUS,
				&iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragment_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gFragment_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Fragment Shader : %s",szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//3 - Create Shader Program
	gShader_Program_Object = glCreateProgram();

	//attach VS & FS
	glAttachShader(gShader_Program_Object, gVertex_Shader_Object);

	glAttachShader(gShader_Program_Object, gFragment_Shader_Object);

	//prelink
	glBindAttribLocation(gShader_Program_Object,AMC_ATTRIBUTE_POSITION,"vPosition");
	glBindAttribLocation(gShader_Program_Object, AMC_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(gShader_Program_Object, AMC_ATTRIBUTE_NORMAL, "vNormal");
	glBindAttribLocation(gShader_Program_Object, AMC_ATTRIBUTE_TEXCOORD, "vTexture_Coord");

	//Link SP
	glLinkProgram(gShader_Program_Object);

	// Error check for SP
	glGetProgramiv(gShader_Program_Object, GL_LINK_STATUS, &iProgrmLinkStatus);


	if (iProgrmLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShader_Program_Object, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetProgramInfoLog(gShader_Program_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Shader Program Linking , %s",szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//postlink

	/*

	LD_Uniform, LA_Uniform, LS_Uniform
	KD_Uniform, KA_Uniform, KS_Uniform

	*/

	mvpUniform = glGetUniformLocation(gShader_Program_Object,"u_model_matrix");
	Projection_Uniform = glGetUniformLocation(gShader_Program_Object, "u_projection_matrix");
	View_Uniform = glGetUniformLocation(gShader_Program_Object, "u_view_matrix");

	LD_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LD");
	KD_Uniform = glGetUniformLocation(gShader_Program_Object, "u_KD");

	LA_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LA");
	KA_Uniform = glGetUniformLocation(gShader_Program_Object, "u_KA");

	LS_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LS");
	KS_Uniform = glGetUniformLocation(gShader_Program_Object, "u_KS");

	LisPressed_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LKeyPressed");
	Light_Position_Uniform = glGetUniformLocation(gShader_Program_Object, "u_Light_Position");
	Material_Shininess = glGetUniformLocation(gShader_Program_Object, "u_Shininess");

	Texture_Sampler_Uniform = glGetUniformLocation( gShader_Program_Object, "u_texture_sampler");
	
	/*
		//pyramid
	const GLfloat pyramidVertices[] =
	{
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f
		
	};

	*/
	//Data
	const GLfloat PyramidData[] =
	{
		0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,0.447214f,0.894427f, 0.5f,1.0f, //1
	  -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f,0.447214f,0.894427f, 0.0f,0.0f, //2
	   1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,0.447214f,0.894427f, 1.0f,0.0f, //3

	    0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.894427f,0.447214f,0.0f, 0.5f,1.0f, //4
	   1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.894427f,0.447214f,0.0f, 1.0f,0.0f, //5
	  1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.894427f,0.447214f,0.0f, 0.0f,0.0f, //6

	    0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,0.447214f,-0.894427f, 0.5f,1.0f, //7
	  1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f,0.447214f,-0.894427f, 1.0f,0.0f, //8
	 -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,0.447214f,-0.894427f, 0.0f,0.0f, //9

	   0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, -0.894427f,0.447214f,0.0f, 0.5f,1.0f, //10
	-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, -0.894427f,0.447214f,0.0f, 0.0f,0.0f, //11
	 -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, -0.894427f,0.447214f,0.0f, 1.0f,0.0f  //12
	};

	
	glGenVertexArrays(1, &Vao_pyramid);
	glBindVertexArray(Vao_pyramid);

	//Position VBO
	glGenBuffers(1, &Vbo_pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_pyramid);
	glBufferData(GL_ARRAY_BUFFER, 24 * 11 * sizeof(float), PyramidData, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, (11*sizeof(float)), (void*)(0 * sizeof(float)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, (11 * sizeof(float)), (void*) (3 * sizeof(float)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, (11 * sizeof(float)), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, (11 * sizeof(float)), (void*)(9 * sizeof(float)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); //vao

	/*

	
	
	0.0f,1.0f,0.0f,
	0.0f,1.0f,0.0f,
	0.0f,1.0f,0.0f,

	
	*/
// Cube
   const GLfloat CubeData[] = 
	{
	   1.0f, 1.0f, 1.0f,    1.0f, 0.0f, 0.0f,	0.0f,0.0f,1.0f,	0.0f,0.0f,
	   -1.0f, 1.0f, 1.0f,   1.0f, 0.0f, 0.0f,	0.0f,0.0f,1.0f, 1.0f,0.0f,
	   -1.0f, -1.0f, 1.0f,  1.0f, 0.0f, 0.0f,	0.0f,0.0f,1.0f,	1.0f,1.0f,
	   1.0f, -1.0f, 1.0f,   1.0f, 0.0f, 0.0f,	0.0f,0.0f,1.0f,	0.0f,1.0f,

	   1.0f, 1.0f, -1.0f,  1.0f, 0.0f, 1.0f,	1.0f,0.0f,0.0f,	1.0f,0.0f,
	   1.0f, 1.0f, 1.0f,   1.0f, 0.0f, 1.0f,	1.0f,0.0f,0.0f,	1.0f,1.0f,
	   1.0f, -1.0f, 1.0f,  1.0f, 0.0f, 1.0f,	1.0f,0.0f,0.0f,	0.0f,1.0f,
	   1.0f, -1.0f, -1.0f,  1.0f, 0.0f, 1.0f,	1.0f,0.0f,0.0f,	0.0f,0.0f,

	   -1.0f, 1.0f, -1.0f,  0.0f, 1.0f, 1.0f,	0.0f,0.0f,-1.0f, 1.0f,0.0f,
	   1.0f, 1.0f, -1.0f,   0.0f, 1.0f, 1.0f,	0.0f,0.0f,-1.0f, 1.0f,1.0f,
	   1.0f, -1.0f, -1.0f,  0.0f, 1.0f, 1.0f,	0.0f,0.0f,-1.0f, 0.0f,1.0f,
	   -1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 1.0f,	0.0f,0.0f,-1.0f, 0.0f,0.0f,
	     
	   -1.0f, 1.0f, 1.0f,   1.0f, 1.0f, 0.0f,	-1.0f,0.0f,0.0f, 0.0f,0.0f,
	   -1.0f, 1.0f, -1.0f,  1.0f, 1.0f, 0.0f,	-1.0f,0.0f,0.0f, 1.0f,0.0f,
	   -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f,	-1.0f,0.0f,0.0f, 1.0f,1.0f,
	   -1.0f, -1.0f, 1.0f,  1.0f, 1.0f, 0.0f,	-1.0f,0.0f,0.0f, 0.0f,1.0f,

	   -1.0f, 1.0f, 1.0f,  0.0f, 0.0f, 1.0f,	0.0f,1.0f,0.0f, 0.0f,1.0f,
	   1.0f, 1.0f, 1.0f,   0.0f, 0.0f, 1.0f,	0.0f,1.0f,0.0f, 0.0f,0.0f,
	   1.0f, 1.0f, -1.0f,  0.0f, 0.0f, 1.0f,	0.0f,1.0f,0.0f, 1.0f,0.0f,
	   -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f,	0.0f,1.0f,0.0f, 1.0f,1.0f,

	   1.0f, -1.0f, 1.0f,	0.0f, 1.0f, 0.0f,   0.0f, -1.0f, 0.0f, 1.0f,1.0f,
	   -1.0f, -1.0f, 1.0f,  0.0f, 1.0f, 0.0f,	0.0f, -1.0f, 0.0f, 0.0f,1.0f,
	   -1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f,	0.0f, -1.0f, 0.0f, 0.0f,0.0f,
	   1.0f, -1.0f, -1.0f,  0.0f, 1.0f, 0.0f,	0.0f, -1.0f, 0.0f, 1.0f,0.0f
	};

  
	glGenVertexArrays(1, &Vao_cube);
	glBindVertexArray(Vao_cube);

	glGenBuffers(1, &Vbo_cube);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_cube);
	glBufferData(GL_ARRAY_BUFFER, sizeof(CubeData), CubeData, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, (11 * sizeof(float)), (void*)(0 * sizeof(float)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, (11 * sizeof(float)), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, (11 * sizeof(float)), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, (11 * sizeof(float)), (void*)(9 * sizeof(float)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); //vao


	//Depth 
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//texture
	LoadTexture(&Texture_Marble, MAKEINTRESOURCE(IDBITMAP_MARBLE));
	glEnable(GL_TEXTURE_2D);
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	Perspective_Projection_Matrix = mat4::identity();

	//Warm up call 
	Resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}


//Resize
void Resize(int Width, int Height)
{
	if (Height == 0)
	{
		Height = 1;
	}

	glViewport(0, 0, GLsizei(Width), GLsizei(Height));
	Perspective_Projection_Matrix = perspective(45.0f, (GLfloat)(Width) / (GLfloat)(Height), 1.0f, 100.0f);

}


//Texture Loading
BOOL LoadTexture(GLuint *texture, TCHAR imageResourceId[])
{
	HBITMAP hBitmap = NULL;
	BITMAP bmp;
	BOOL bStatus = FALSE;

	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

	if (hBitmap)
	{
		bStatus = TRUE;

		GetObject(hBitmap, sizeof(BITMAP), &bmp);
		
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

		glGenTextures(1, texture);

		fprintf(gpFile, "\n Texture Created \n");

		glBindTexture(GL_TEXTURE_2D, *texture);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		//new
		glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGB,
			bmp.bmWidth,
			bmp.bmHeight,
			0,
			GL_BGR_EXT,
			GL_UNSIGNED_BYTE,
			bmp.bmBits);

		glGenerateMipmap(GL_TEXTURE_2D); //new
		glBindTexture(GL_TEXTURE_2D, 0);

		DeleteObject(hBitmap);
	}
	return(bStatus);
}


//Display
void Display(void)
{
	//1
	mat4 Model_Matrix;
	mat4 View_Matrix;
	mat4 Projection_Matrix;
	mat4 Translation_Matrix;
	mat4 Rotation_Matrix;
	mat4 ModelView_Projection_Matrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//code : 9 steps
	glUseProgram(gShader_Program_Object);

	//2 : I[]
	Model_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	View_Matrix = mat4::identity();
	Rotation_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//4 : Do Mat mul
	Translation_Matrix = translate(-1.8f, 0.0f, -7.0f);

	Rotation_Matrix = rotate(AngleTri, 0.0f, 1.0f, 0.0f);

	Model_Matrix = Translation_Matrix * Rotation_Matrix;

	ModelView_Projection_Matrix = Perspective_Projection_Matrix * View_Matrix * Model_Matrix;

	// 5	: send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, Model_Matrix);
	glUniformMatrix4fv(Projection_Uniform, 1, GL_FALSE, Perspective_Projection_Matrix);
	glUniformMatrix4fv(View_Uniform, 1, GL_FALSE, View_Matrix);

	//6 : texture bind code
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, Texture_Marble);
	glUniform1i(Texture_Sampler_Uniform, 0);
	
	//7: Lights
	if (bLighting == true)
	{
		glUniform1i(LisPressed_Uniform, 1);

		glUniform3f(LD_Uniform, 1.0f, 1.0f, 1.0f);		//0.75f, 0.55f, 0.25f);
		glUniform3f(KD_Uniform, 1.0f, 1.0f, 1.0f);

		glUniform3f(LS_Uniform, 1.0f, 1.0f, 1.0f);
		glUniform3f(KS_Uniform, 1.0f, 1.0f, 1.0f);
		
		glUniform3f(LA_Uniform, 0.0f, 0.0f, 0.0f);
		glUniform3f(KA_Uniform, 0.0f, 0.0f, 0.0f);			//0.25f, 0.25f, 0.25f);
		
		glUniform1f(Material_Shininess, 128.0f);
		
	//imp : out to in Light
		
	}

	else
	{
		glUniform1i(LisPressed_Uniform, 0);
	}

	glUniform4f(Light_Position_Uniform, -1.8f, 0.0f, 2.0f, 1.0f);

	// 6 : bind to vao
	glBindVertexArray(Vao_pyramid);

	//8 : draw scene
	glDrawArrays(GL_TRIANGLES, 0, 12);

	// *** unbind vao ***
	glBindVertexArray(0);


//Cube
//2 : I[]
	Model_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	View_Matrix = mat4::identity();
	Rotation_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//4 : Do Mat mul
	Translation_Matrix = translate(1.8f, 0.0f, -8.5f);

	Rotation_Matrix *= rotate(AngleTri, 1.0f, 0.0f, 0.0f);
	
	Rotation_Matrix *= rotate(AngleTri, 0.0f, 1.0f, 0.0f);

	Rotation_Matrix *= rotate(AngleTri, 0.0f, 0.0f, 1.0f);
	
	Model_Matrix = Translation_Matrix * Rotation_Matrix;

	ModelView_Projection_Matrix = Perspective_Projection_Matrix * View_Matrix * Model_Matrix;

	// 5	: send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, Model_Matrix);
	glUniformMatrix4fv(Projection_Uniform, 1, GL_FALSE, Perspective_Projection_Matrix);
	glUniformMatrix4fv(View_Uniform, 1, GL_FALSE, View_Matrix);

	glUniform4f(Light_Position_Uniform, 1.8f, 0.0f, 2.0f, 1.0f);


	// 6 : bind to vao
	glBindVertexArray(Vao_cube);

	//8 : draw scene
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

	// *** unbind vao ***
	glBindVertexArray(0);

	//Common
	glUseProgram(0);

	SwapBuffers(ghdc);
}


void Update(void)
{
	AngleTri += 0.5f;

	if (AngleTri >= 360.0f)
		AngleTri -= 360.0f;

}


//ToggleFullScreen
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	mi.cbSize = sizeof(MONITORINFO);

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//mi = { sizeof(MONITORINFO) };// not supported in VS2008

		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			if ((GetWindowPlacement(ghwnd, &wpPrev)) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),// WD
					(mi.rcMonitor.bottom - mi.rcMonitor.top), // HT
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}

	else
		if (gbIsFullScreen == true)
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

			SetWindowPlacement(ghwnd, &wpPrev);

			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
				SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

			ShowCursor(TRUE);
			gbIsFullScreen = false;

		}
}


//UnInitialize	
void UnInitialize(void)
{
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}


	//for Shaders
	if (Vao_pyramid)
	{
		glDeleteBuffers(1, &Vao_pyramid);
		Vao_pyramid = 0;
	}

	if (Vbo_pyramid)
	{
		glDeleteBuffers(1, &Vbo_pyramid);
		Vbo_pyramid = 0;
	}

	if (Vao_cube)
	{
		glDeleteBuffers(1, &Vao_cube);
		Vao_cube = 0;
	}
	

	if(Vbo_cube)
	{
		glDeleteBuffers(1, &Vbo_cube);
		Vbo_cube = 0;
	}


	glUseProgram(gShader_Program_Object);
	glDetachShader(gShader_Program_Object, gFragment_Shader_Object);
	glDetachShader(gShader_Program_Object, gVertex_Shader_Object);

	glDeleteShader(gFragment_Shader_Object);
	gFragment_Shader_Object = 0;

	glDeleteShader(gVertex_Shader_Object);
	gVertex_Shader_Object = 0;

	glDeleteProgram(gShader_Program_Object);
	glUseProgram(0);

	
	//Break Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "\n Application Closed ");
	fclose(gpFile);
	gpFile = NULL;
}

