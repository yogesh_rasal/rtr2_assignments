
/*

	Assignment : 24 Spheres

*/

//Headers
#include <stdio.h>
#include <windows.h>
#include "GL/glew.h"
#include <stdlib.h>
#include "vmath.h"
#include "Sphere.h"

#pragma comment (lib,"opengl32.lib")
#pragma comment (lib,"gdi32.lib")
#pragma comment (lib,"user32.lib")
#pragma comment (lib,"glew32.lib")
#pragma comment (lib,"Sphere.lib")
#pragma comment(lib,"Winmm.lib")


#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

// Globals
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
FILE *gpFile = NULL;

bool gbActiveWindow = false;
bool gbIsFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
GLenum Result;

//new
static int g_Width,g_Height;

//For Shaders
GLuint gVertex_Shader_Object;
GLuint gFragment_Shader_Object;
GLuint gShader_Program_Object;

GLint iShaderCompileStatus = 0;
GLint iInfoLogLength = 0;
GLchar *szInfoLog = NULL;
GLint iProgrmLinkStatus = 0;

// New
GLuint mvpUniform;
mat4 Perspective_Projection_Matrix;
mat4 View_Matrix;

GLuint gVao_sphere, gVbo_sphere_position, gVbo_sphere_normal, gVbo_sphere_element;
GLuint gNumElements, gNumVertices;

//for sphere
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

//for Lights
bool bLighting = false;
bool bAnimate = false;
GLfloat AngleTri = 0.0f;
bool XPressed,YPressed , ZPressed;

//12 Uniforms
GLuint Model_Uniform;
GLuint View_Uniform;
GLuint Projection_Uniform;

GLuint LD_Uniform, LA_Uniform, LS_Uniform;
GLuint KD_Uniform, KA_Uniform, KS_Uniform;

GLuint u_Material_Shininess;
GLuint Light_Position_Uniform;
GLuint LisPressed_Uniform;

//Red
GLuint LD_Uniform_Red, LA_Uniform_Red, LS_Uniform_Red;
GLuint KD_Uniform_Red, KA_Uniform_Red, KS_Uniform_Red;
GLuint Light_Position_Uniform_Red;

//Green
GLuint LD_Uniform_Green, LA_Uniform_Green, LS_Uniform_Green;
GLuint KD_Uniform_Green, KA_Uniform_Green, KS_Uniform_Green;
GLuint Light_Position_Uniform_Green;


//ENUM
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

GLsizei shaderCount, shaderNumber;

// Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int Initialize(void);
void Display(void);
void UnInitialize(void);
void Update(void);
void ToggleFullScreen(void);
void Resize(int, int);
void UnInitialize(void);
void Draw24Spheres(void);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIns, LPSTR lpCmdLine, int iCmdShow)
{
	//code
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("PPShape");
	int iRet = 0;
	bool bDone = false;

	//File IO
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can not Created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	else
	{
		fprintf(gpFile, "Log File Successfully Created");
	}

	//initialize the class
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(hInstance, IDC_HAND);
	wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.cbWndExtra = 0;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("Lights on Materials By YSR"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100, 100, WIN_WIDTH, WIN_HEIGHT,
		NULL, NULL, hInstance, NULL);

	// For Update
	ghwnd = hwnd;
	iRet = Initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\n ChoosePixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -2)
	{
		fprintf(gpFile, "\n SetPixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -3)
	{
		fprintf(gpFile, "\n wglCreateContext Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -4)
	{
		fprintf(gpFile, "\n wglMakeCurrent Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == 0)
	{
		fprintf(gpFile, "\n Initialization Successful");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd); //UpdateWindow
	SetFocus(hwnd);

	// Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else if (gbActiveWindow == true)
		{
			Update();
		}

		Display();
	}

	return((int)msg.wParam);

}//Main Ends


 //WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_CREATE:
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		g_Width = LOWORD(lParam);
		g_Height = HIWORD(lParam);
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;

		case 'L':
		case 'l':
			if (!bLighting)
			{
				bLighting = true;
			}
			else
			{
				bLighting = false;
			}
			PlaySound(TEXT("01Aa.wav"), NULL, SND_ASYNC | SND_LOOP);			
			break;
		
		case 'A':
		case 'a':
			if (!bAnimate)
			{
				bAnimate = true;
			}
			else
			{
				bAnimate = false;
			}
			break;
		
		case 'x':
		case 'X': 
			XPressed = true;
			YPressed = false;
			ZPressed = false;
			break;
	
		case 'y':
		case 'Y': 
			YPressed = true;
			ZPressed = false;
			XPressed = false;
			break;

		case 'z':
		case 'Z': 
			XPressed = false;
			YPressed = false;
			ZPressed = true;
			break;
		}
		break;

	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}//WndProc Ends


 //init()
int Initialize(void)
{
	
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;

	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits   = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits  = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	else if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	//Bridging API
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	else if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}
		

	//*****************************

	Result = glewInit();

	if (Result != GLEW_OK)
	{
		UnInitialize();
	}


	/*
			Add 12 Uniforms for Light
 	*/

	//For Shaders : create 
	gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

	//Define VS code
	GLchar *VertexShaderCode =
		"#version 450 core"							\
		"\n"										\
		"in vec4 vPosition;"						\
		"in vec3 vNormal;"							\
		"out vec3 T_Norm;"							\
		"out vec3 Light_Direction;"					\
		"out vec3 Light_Direction_R;"				\
		"out vec3 Light_Direction_G;"				\
		"out vec3 Viewer_Vector;"					\
		"\n"										\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform int u_LKeyPressed;"				\
		"uniform vec4 u_Light_Position;"			\
		"uniform vec4 u_Light_Position_R;"			\
		"uniform vec4 u_Light_Position_G;"			\
		"\n"										\
		"void main(void)"							\
		"{"											\
		"if(u_LKeyPressed == 1)"					\
		"{"											\
		"vec4 Eye_Coordinates = (u_view_matrix * u_model_matrix * vPosition);"	\
		"T_Norm = mat3(u_view_matrix * u_model_matrix) * vNormal;"			\
		"Light_Direction = vec3(u_Light_Position) - (Eye_Coordinates.xyz);"	\
		"Light_Direction_R = vec3(u_Light_Position_R) - (Eye_Coordinates.xyz);"	\
		"Light_Direction_G = vec3(u_Light_Position_G) - (Eye_Coordinates.xyz);"	\
		"Viewer_Vector = vec3(-Eye_Coordinates.xyz);"	\
		"}"		\
		"gl_Position = (u_projection_matrix * u_view_matrix * u_model_matrix * vPosition);"	\
		"}" ;

	//Specify code to Obj
	glShaderSource(gVertex_Shader_Object,
					1,
				(const GLchar**)&VertexShaderCode,
				NULL);

	//Compile the shader
	glCompileShader(gVertex_Shader_Object);

	//Error Code for VS
	glGetShaderiv(gVertex_Shader_Object,
				GL_COMPILE_STATUS,
				&iShaderCompileStatus);


	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertex_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gVertex_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n %s Failed in Vertex Shader",szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//2 - FS
	gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

	//Define FS code : "precision highp float;"	
	GLchar *FragmentShaderCode =
		"#version 450 core"			\
		"\n"						\
		"in vec3 T_Norm;"			\
		"in vec3 Light_Direction;"	\
		"in vec3 Light_Direction_R;"	\
		"in vec3 Light_Direction_G;"	\
		"in vec3 Viewer_Vector;"	\
		"out vec4 FragColor;"		\
		"\n"						\
		"uniform vec3 u_LA;"				\
		"uniform vec3 u_LD;"				\
		"uniform vec3 u_LS;"				\
		"uniform vec3 u_KA;"				\
		"uniform vec3 u_KD;"				\
		"uniform vec3 u_KS;"				\
		"uniform vec3 u_LA_R;"				\
		"uniform vec3 u_LD_R;"				\
		"uniform vec3 u_LS_R;"				\
		"uniform vec3 u_KA_R;"				\
		"uniform vec3 u_KD_R;"				\
		"uniform vec3 u_KS_R;"				\
		"uniform vec3 u_LA_G;"				\
		"uniform vec3 u_LD_G;"				\
		"uniform vec3 u_LS_G;"				\
		"uniform vec3 u_KA_G;"				\
		"uniform vec3 u_KD_G;"				\
		"uniform vec3 u_KS_G;"				\
		"uniform float u_Shininess;"		\
		"uniform int u_LKeyPressed;"		\
		"uniform vec4 u_Light_Position;"	\
		"uniform vec4 u_Light_Position_R;"	\
		"uniform vec4 u_Light_Position_G;"	\
		"\n"								\
		"void main(void)"					\
		"{"						\
		"if(u_LKeyPressed == 1)"\
		"{"						\
		"vec3 Normalized_View_Vector = normalize(Viewer_Vector);"	\
		"vec3 Normalized_Light_Direction = normalize(Light_Direction);"\
		"vec3 Normalized_Light_Direction_R = normalize(Light_Direction_R);"\
		"vec3 Normalized_Light_Direction_G = normalize(Light_Direction_G);"\
		"\n"\
		"vec3 Normalized_TNorm = normalize(T_Norm);"\
		"float TN_Dot_LD = max(dot(Normalized_Light_Direction,Normalized_TNorm), 0.0);"	\
		"vec3 Reflection_Vector = reflect(-Normalized_Light_Direction, Normalized_TNorm);"\
		"float TN_Dot_LD_R = max(dot(Normalized_Light_Direction_R,Normalized_TNorm), 0.0);"	\
		"vec3 Reflection_Vector_R = reflect(-Normalized_Light_Direction_R, Normalized_TNorm);"\
		"float TN_Dot_LD_G = max(dot(Normalized_Light_Direction_G,Normalized_TNorm), 0.0);"	\
		"vec3 Reflection_Vector_G = reflect(-Normalized_Light_Direction_G, Normalized_TNorm);"\
		"\n" \
		"vec3 Ambient = vec3(u_LA * u_KA);"	\
		"vec3 Diffuse = vec3(u_LD * u_KD * TN_Dot_LD);"	\
		"vec3 Specular = vec3(u_LS * u_KS * pow(max(dot(Reflection_Vector, Normalized_View_Vector), 0.0), u_Shininess));" \
		"vec3 Ambient_R = vec3(u_LA_R * u_KA_R);"	\
		"vec3 Diffuse_R = vec3(u_LD_R * u_KD_R * TN_Dot_LD_R);"	\
		"vec3 Specular_R = vec3(u_LS_R * u_KS_R * pow(max(dot(Reflection_Vector_R, Normalized_View_Vector), 0.0), u_Shininess));" \
		"vec3 Ambient_G  = vec3(u_LA_G * u_KA_G);"	\
		"vec3 Diffuse_G  = vec3(u_LD_G * u_KD_G * TN_Dot_LD_G);"	\
		"vec3 Specular_G = vec3(u_LS_G * u_KS_G * pow(max(dot(Reflection_Vector_G, Normalized_View_Vector), 0.0), u_Shininess));" \
		"\n" \
		"vec3 Phong_ADS_Light = Ambient + Diffuse + Specular;"	\
		"vec3 Phong_ADS_Light_R = Ambient_R + Diffuse_R+ Specular_R;"	\
		"vec3 Phong_ADS_Light_G = Ambient_G + Diffuse_G + Specular_G;"	\
		"FragColor = vec4(Phong_ADS_Light+Phong_ADS_Light_R+Phong_ADS_Light_G,1.0);" \
		"}"							\
		"else"						\
		"{"							\
		"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		"}"							\
		"}" ;


	//Specify code to Obj
	glShaderSource(gFragment_Shader_Object,1,
				  ( GLchar**)&FragmentShaderCode,NULL);

	//Compile the shader
	glCompileShader(gFragment_Shader_Object);

	//Error check for FS
	glGetShaderiv(gFragment_Shader_Object,
		GL_COMPILE_STATUS,
		&iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragment_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gFragment_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Fragment Shader : %s",szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//3 - Create Shader Program
	gShader_Program_Object = glCreateProgram();

	//attach VS & FS
	glAttachShader(gShader_Program_Object, gVertex_Shader_Object);

	glAttachShader(gShader_Program_Object, gFragment_Shader_Object);

	//prelink
	glBindAttribLocation(gShader_Program_Object,AMC_ATTRIBUTE_POSITION,"vPosition");
	glBindAttribLocation(gShader_Program_Object, AMC_ATTRIBUTE_NORMAL, "vNormal");
	
	//Link SP
	glLinkProgram(gShader_Program_Object);

	// Error check for SP
	glGetProgramiv(gShader_Program_Object, GL_LINK_STATUS, &iProgrmLinkStatus);


	if (iProgrmLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShader_Program_Object, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetProgramInfoLog(gShader_Program_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Shader Program Linking , %s",szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//postlink
	mvpUniform = glGetUniformLocation(gShader_Program_Object,"u_model_matrix");
	Projection_Uniform = glGetUniformLocation(gShader_Program_Object, "u_projection_matrix");
	View_Uniform = glGetUniformLocation(gShader_Program_Object, "u_view_matrix");

	LD_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LD");
	KD_Uniform = glGetUniformLocation(gShader_Program_Object, "u_KD");

	LA_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LA");
	KA_Uniform = glGetUniformLocation(gShader_Program_Object, "u_KA");

	LS_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LS");
	KS_Uniform = glGetUniformLocation(gShader_Program_Object, "u_KS");

	LisPressed_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LKeyPressed");
	Light_Position_Uniform = glGetUniformLocation(gShader_Program_Object, "u_Light_Position");
	u_Material_Shininess = glGetUniformLocation(gShader_Program_Object, "u_Shininess");
	
	
	//Red
	
	LD_Uniform_Red = glGetUniformLocation(gShader_Program_Object, "u_LD_R");
	KD_Uniform_Red = glGetUniformLocation(gShader_Program_Object, "u_KD_R");

	LA_Uniform_Red = glGetUniformLocation(gShader_Program_Object, "u_LA_R");
	KA_Uniform_Red = glGetUniformLocation(gShader_Program_Object, "u_KA_R");

	LS_Uniform_Red = glGetUniformLocation(gShader_Program_Object, "u_LS_R");
	KS_Uniform_Red = glGetUniformLocation(gShader_Program_Object, "u_KS_R");

	LisPressed_Uniform = glGetUniformLocation(gShader_Program_Object, "u_LKeyPressed");
	Light_Position_Uniform_Red = glGetUniformLocation(gShader_Program_Object, "u_Light_Position_R");


	//Green
		
	LD_Uniform_Green = glGetUniformLocation(gShader_Program_Object, "u_LD_G");
	KD_Uniform_Green = glGetUniformLocation(gShader_Program_Object, "u_KD_G");

	LA_Uniform_Green = glGetUniformLocation(gShader_Program_Object, "u_LA_G");
	KA_Uniform_Green = glGetUniformLocation(gShader_Program_Object, "u_KA_G");

	LS_Uniform_Green = glGetUniformLocation(gShader_Program_Object, "u_LS_G");
	KS_Uniform_Green = glGetUniformLocation(gShader_Program_Object, "u_KS_G");

	Light_Position_Uniform_Green = glGetUniformLocation(gShader_Program_Object, "u_Light_Position_G");



	//drawing
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// vao
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// position vbo 
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);


	//Depth 
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	Perspective_Projection_Matrix = mat4::identity();

	//Warm up call 
	Resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}


//Resize
void Resize(int Width, int Height)
{
	if (Height == 0)
	{
		Height = 1;
	}

	glViewport(0, 0, GLsizei(Width), GLsizei(Height));
	Perspective_Projection_Matrix = perspective(45.0f, (GLfloat)(Width) / (GLfloat)(Height), 1.0f, 100.0f);

}


//Display
void Display(void)
{
	//1
	mat4 Model_Matrix;
	mat4 View_Matrix;
	mat4 Projection_Matrix;
	mat4 Translation_Matrix;
	mat4 Rotation_Matrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShader_Program_Object);

	//code : 9 steps

	//2 : I[]
	Model_Matrix = mat4::identity();
	View_Matrix = mat4::identity();
	Rotation_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//4 : Do Mat mul
	Translation_Matrix = translate(0.0f, 0.0f,-2.5f);

	Model_Matrix = Translation_Matrix * Rotation_Matrix;

	// 5	: send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, Model_Matrix);

	glUniformMatrix4fv(View_Uniform, 1, GL_FALSE, View_Matrix);

	glUniformMatrix4fv(Projection_Uniform, 1, GL_FALSE, Perspective_Projection_Matrix);
	
	if (bLighting == true)
	{
		//1st
		glUniform1i(LisPressed_Uniform, 1);

		glUniform3f(LD_Uniform, 1.0f, 1.0f, 1.0f);

		glUniform3f(LS_Uniform, 1.0f, 1.0f, 1.0f);
		
		glUniform3f(LA_Uniform, 0.0f, 0.0f, 0.0f);
		

		//2nd Light

		glUniform3f(LD_Uniform_Red, 1.0f, 1.0f, 1.0f);
		
		glUniform3f(LS_Uniform_Red, 1.0f, 1.0f, 1.0f);

		glUniform3f(LA_Uniform_Red, 0.0f, 0.0f, 0.0f);
		
	
		//3rd Light

		glUniform3f(LD_Uniform_Green, 1.0f, 1.0f, 1.0f);

		glUniform3f(LS_Uniform_Green, 1.0f, 1.0f, 1.0f);
	
		glUniform3f(LA_Uniform_Green, 0.0f, 0.0f, 0.0f);
		

		//Position

		 if(XPressed)
		 {
			glUniform4f(Light_Position_Uniform_Red, 0.0f, sinf(AngleTri)*3, cosf(AngleTri)*3 , 1.0f); 
			glUniform4f(Light_Position_Uniform_Green, 0.0f, sinf(AngleTri)*3, cosf(AngleTri)*3 , 1.0f); 
			glUniform4f(Light_Position_Uniform, 0.0f, sinf(AngleTri)*3, cosf(AngleTri)*3 , 1.0f); 

		 }

		if(YPressed)
		{
			glUniform4f(Light_Position_Uniform_Green,sinf(AngleTri)*3, 0.0f, cosf(AngleTri)*3, 1.0f);
			glUniform4f(Light_Position_Uniform,sinf(AngleTri)*3, 0.0f, cosf(AngleTri)*3, 1.0f);
			glUniform4f(Light_Position_Uniform_Red,sinf(AngleTri)*3, 0.0f, cosf(AngleTri)*3, 1.0f);
		}

		 if(ZPressed)
		 {
			glUniform4f(Light_Position_Uniform,sinf(AngleTri)*3 ,cosf(AngleTri)*3 , 0.0f, 1.0f);
			glUniform4f(Light_Position_Uniform_Red,sinf(AngleTri)*3 ,cosf(AngleTri)*3 , 0.0f, 1.0f);
			glUniform4f(Light_Position_Uniform_Green,sinf(AngleTri)*3 ,cosf(AngleTri)*3 , 0.0f, 1.0f);
		 }

	}

	else
	{
		glUniform1i(LisPressed_Uniform, 0);
	}

	//Draw24Spheres(); : idx = struct Materials - i*6+j

	GLfloat  Material_Ambient[72]; // {0.0215f,0.1745f,0.0215f,1.0f};
	GLfloat  Material_Diffuse[72]; // {0.0215f,0.1745f,0.0215f,1.0f};
	GLfloat  Material_Specular[72]; //  {1.0f,1.0f,1.0f,1.0f};
	GLfloat  Material_Shininess[24]; // {128.0f};
	

	//24 Spheres
// 1 : Emerald
	Material_Ambient[0] = 0.0215f;
	Material_Ambient[1] = 0.1745f;
	Material_Ambient[2] = 0.0215f;

	Material_Diffuse[0] = 0.0215f;
	Material_Diffuse[1] = 0.1745f;
	Material_Diffuse[2] = 0.0215f;
    //
    
	Material_Specular[0] = 0.633f;
	Material_Specular[1] = 0.727811f;
	Material_Specular[2] = 0.633f;

	Material_Shininess[0] = 0.6f * 128.0f;


// 2 = Jade
	Material_Ambient[3] = 0.135f;
	Material_Ambient[4] = 0.2225f;
	Material_Ambient[5] = 0.1575f;
	//
	///

	Material_Diffuse[3] = 0.54f;
	Material_Diffuse[4] = 0.89f;
	Material_Diffuse[5] = 0.63f;
    //
	//
    
	Material_Specular[3] = 0.316228f;
	Material_Specular[4] = 0.316228f;
	Material_Specular[5] = 0.316228f;
	//

	Material_Shininess[1] = 0.1f * 128.0f;


// 3 = obisidian
	Material_Ambient[6] = 0.05375f;
	Material_Ambient[7] = 0.05f;
	Material_Ambient[8] = 0.06625f;
	//

	Material_Diffuse[6] = 0.18275f;
	Material_Diffuse[7] = 0.17f;
	Material_Diffuse[8] = 0.22525f;
    //
	//
    
	Material_Specular[6] = 0.332741f;
	Material_Specular[7] = 0.328634f;
	Material_Specular[8] = 0.346435f;
	//
    //

	Material_Shininess[2] = 0.3f * 128.0f;
    //   

// 4= Pearl
	Material_Ambient[9] = 0.25f;
	Material_Ambient[10] = 0.20725f;
	Material_Ambient[11] = 0.20725f;
	//
	///

	Material_Diffuse[9] = 1.0f;
	Material_Diffuse[10] = 0.829f;
	Material_Diffuse[11] = 0.829f;
    //
	//
    
	Material_Specular[9] = 0.296648f;
	Material_Specular[10] = 0.296648f;
	Material_Specular[11] = 0.296648f;
	//
    //

	Material_Shininess[3] = 0.088f * 128.0f;
    //

// 5 = Ruby
	Material_Ambient[12] = 0.1745f;
	Material_Ambient[13] = 0.01175f;
	Material_Ambient[14] = 0.01175f;
	//
	///

	Material_Diffuse[12] = 0.61424f;
	Material_Diffuse[13] = 0.04136f;
	Material_Diffuse[14] = 0.04136f;
    //
    
	Material_Specular[12] = 0.727811f;
	Material_Specular[13] = 0.626959f;
	Material_Specular[14] = 0.626959f;
	//

	Material_Shininess[4] = 0.6f * 128.0f;
    //

// 6 = Turquoise
	Material_Ambient[15] = 0.1f;
	Material_Ambient[16] = 0.18275f;
	Material_Ambient[17] = 0.1745f;
	//
	///

	Material_Diffuse[15] = 0.396f;
	Material_Diffuse[16] = 0.74151f;
	Material_Diffuse[17] = 0.69102f;
    //
	//
    
	Material_Specular[15] = 0.297524f;
	Material_Specular[16] = 0.30829f;
	Material_Specular[17] = 0.306678f;
	//
    //

	Material_Shininess[5] = 0.1f * 128.0f;
    //
  

// Metals   ---------------------

// 1 : Brass
	Material_Ambient[18] = 0.349412f;
	Material_Ambient[19] = 0.30829f;
	Material_Ambient[20] = 0.306678f;
	///

	Material_Diffuse[18] = 0.780392f;
	Material_Diffuse[19] = 0.223529f;
	Material_Diffuse[20] = 0.027451f;
    //
	//
    
	Material_Specular[18] = 0.992157f;
	Material_Specular[19] = 0.941176f;
	Material_Specular[20] = 0.807843f;
	//
    //

	Material_Shininess[6] = 0.21794872f * 128.0f;
    //

// 2 = Bronze
	Material_Ambient[21] = 0.2125f;
	Material_Ambient[22] = 0.1275f;
	Material_Ambient[23] = 0.054f;
	//
	///

	Material_Diffuse[21] = 0.714f;
	Material_Diffuse[22] = 0.4284f;
	Material_Diffuse[23] = 0.18144f;
    //
	//
    
	Material_Specular[21] = 0.393548f;
	Material_Specular[22] = 0.271906f;
	Material_Specular[23] = 0.166721f;
	//
    //

	Material_Shininess[7] = 0.2f * 128.0f;
    //

// 3 = Chrome
	Material_Ambient[24] = 0.25f;
	Material_Ambient[25] = 0.25f;
	Material_Ambient[26] = 0.25f;
	//
	///

	Material_Diffuse[24] = 0.4f;
	Material_Diffuse[25] = 0.4f;
	Material_Diffuse[26] = 0.4f;
    //
	//
    
	Material_Specular[24] = 0.774597f;
	Material_Specular[25] = 0.774597f;
	Material_Specular[26] = 0.774597f;
	//
    //

	Material_Shininess[8] = 0.6f * 128.0f;
    //

	
// 4= copper
	Material_Ambient[27] = 0.19125f;
	Material_Ambient[28] = 0.0735f;
	Material_Ambient[29] = 0.02025f;
	//
	///

	Material_Diffuse[27] = 0.7038f;
	Material_Diffuse[28] = 0.27048f;
	Material_Diffuse[29] = 0.0828f;
    //
	//
    
	Material_Specular[27] = 0.25677f;
	Material_Specular[28] = 0.137622f;
	Material_Specular[29] = 0.086014f;
	//
    //

	Material_Shininess[9] = 0.1f * 128.0f;


// 5 = Gold
	Material_Ambient[30] = 0.24725f;
	Material_Ambient[31] = 0.1995f;
	Material_Ambient[32] = 0.0745f;
	//
	///

	Material_Diffuse[30] = 0.7517f;
	Material_Diffuse[31] = 0.6065f;
	Material_Diffuse[32] = 0.2265f;
    //
    
	Material_Specular[30] = 0.6283f;
	Material_Specular[31] = 0.55580f;
	Material_Specular[32] = 0.36606f;
	//

	Material_Shininess[10] = 0.4f * 128.0f;
    //

// 6 = Silver
	Material_Ambient[33] = 0.19225f;
	Material_Ambient[34] = 0.19225f;
	Material_Ambient[35] = 0.19225f;
	//

	Material_Diffuse[33] = 0.5075f;
	Material_Diffuse[34] = 0.5075f;
	Material_Diffuse[35] = 0.5075f;
    //
	//
    
	Material_Specular[33] = 0.50828f;
	Material_Specular[34] = 0.50828f;
	Material_Specular[35] = 0.50828f;
	//
    //

	Material_Shininess[11] = 0.4f * 128.0f;
    //


// Plastic ---------------------
// 1 - Black
	Material_Ambient[36] = 0.0f;
	Material_Ambient[37] = 0.0f;
	Material_Ambient[38] = 0.0f;
	//
	///

	Material_Diffuse[36] = 0.01f;
	Material_Diffuse[37] = 0.01f;
	Material_Diffuse[38] = 0.01f;
    //
	//
    
	Material_Specular[36] = 0.5f;
	Material_Specular[37] = 0.5f;
	Material_Specular[38] = 0.5f;
	//
    //

	Material_Shininess[12] = 0.25f * 128.0f;
    //


// 2 - Cyan
	Material_Ambient[39] = 0.0f;
	Material_Ambient[40] = 0.1f;
	Material_Ambient[41] = 0.06f;
	//
	///

	Material_Diffuse[39] = 0.0f;
	Material_Diffuse[40] = 0.5098039f;
	Material_Diffuse[41] = 0.5098039f;
    //
	//
    
	Material_Specular[39] = 0.0f;
	Material_Specular[40] = 0.501960f;
	Material_Specular[41] = 0.501960f;
	//

	Material_Shininess[13] = 0.25f * 128.0f;
    //

//3 - Green
	Material_Ambient[42] = 0.0f;
	Material_Ambient[43] = 0.1f;
	Material_Ambient[44] = 0.06f;
	//
	///

	Material_Diffuse[42] = 0.1f;
	Material_Diffuse[43] = 0.35f;
	Material_Diffuse[44] = 0.1f;
    //
	//
    
	Material_Specular[42] = 0.45f;
	Material_Specular[43] = 0.55f;
	Material_Specular[44] = 0.45f;
	//
    //

	Material_Shininess[14] = 0.25f * 128.0f;
    //


// Red

	Material_Ambient[45] = 0.0f;
	Material_Ambient[46] = 0.0f;
	Material_Ambient[47] = 0.0f;
	//
	///

	Material_Diffuse[45] = 0.5f;
	Material_Diffuse[46] = 0.0f;
	Material_Diffuse[47] = 0.0f;
    //
	//
    
	Material_Specular[45] = 0.7f;
	Material_Specular[46] = 0.6f;
	Material_Specular[47] = 0.6f;
	//
    //

	Material_Shininess[15] = 0.25f * 128.0f;


// White
	Material_Ambient[48] = 0.0f;
	Material_Ambient[49] = 0.0f;
	Material_Ambient[50] = 0.0f;
	//
	///

	Material_Diffuse[48] = 0.55f;
	Material_Diffuse[49] = 0.55f;
	Material_Diffuse[50] = 0.55f;
    //
	//
    
	Material_Specular[48] = 0.7f;
	Material_Specular[49] = 0.7f;
	Material_Specular[50] = 0.7f;
	//
    //

	Material_Shininess[16] = 0.25f * 128.0f;
    //


// Yellow
	Material_Ambient[51] = 0.0f;
	Material_Ambient[52] = 0.0f;
	Material_Ambient[53] = 0.0f;
	//
	///

	Material_Diffuse[51] = 0.5f;
	Material_Diffuse[52] = 0.5f;
	Material_Diffuse[53] = 0.0f;
    
	Material_Specular[51] = 0.6f;
	Material_Specular[52] = 0.6f;
	Material_Specular[53] = 0.5f;

	Material_Shininess[17] = 0.25f * 128.0f;


// -------- Rubber
// 1 - Black
	Material_Ambient[54] = 0.02f;
	Material_Ambient[55] = 0.02f;
	Material_Ambient[56] = 0.02f;
	//
	///

	Material_Diffuse[54] = 0.01f;
	Material_Diffuse[55] = 0.01f;
	Material_Diffuse[56] = 0.01f;
    //
	//
    
	Material_Specular[54] = 0.4f;
	Material_Specular[55] = 0.4f;
	Material_Specular[56] = 0.4f;
	//
    //

	Material_Shininess[18] = 0.07813f * 128.0f;
    //


// 2 - Cyan
	Material_Ambient[57] = 0.0f;
	Material_Ambient[58] = 0.05f;
	Material_Ambient[59] = 0.05f;
	//
	///

	Material_Diffuse[57] = 0.4f;
	Material_Diffuse[58] = 0.5098039f;
	Material_Diffuse[59] = 0.5098039f;
    //
	//
    
	Material_Specular[57] = 0.0f;
	Material_Specular[58] = 0.501960f;
	Material_Specular[59] = 0.501960f;
	//
    //

	Material_Shininess[19] = 0.07813f * 128.0f;
    //

	//
	
//3 - Green
	Material_Ambient[60] = 0.0f;
	Material_Ambient[61] = 0.1f;
	Material_Ambient[62] = 0.06f;
	//
	///

	Material_Diffuse[60] = 0.1f;
	Material_Diffuse[61] = 0.35f;
	Material_Diffuse[62] = 0.1f;
    //
	//
    
	Material_Specular[60] = 0.45f;
	Material_Specular[61] = 0.55f;
	Material_Specular[62] = 0.45f;
	//
    //

	Material_Shininess[20] = 0.07813f * 128.0f;
    //

// 4- Red

	Material_Ambient[63] = 0.0f;
	Material_Ambient[64] = 0.0f;
	Material_Ambient[65] = 0.0f;
	//
	///

	Material_Diffuse[63] = 0.5f;
	Material_Diffuse[64] = 0.0f;
	Material_Diffuse[65] = 0.0f;
    //
	//
    
	Material_Specular[63] = 0.7f;
	Material_Specular[64] = 0.6f;
	Material_Specular[65] = 0.6f;
	//
    //

	Material_Shininess[21] = 0.07813f * 128.0f;
    //

// 5- White
	Material_Ambient[66] = 0.0f;
	Material_Ambient[67] = 0.0f;
	Material_Ambient[68] = 0.0f;
	//
	///

	Material_Diffuse[66] = 0.55f;
	Material_Diffuse[67] = 0.55f;
	Material_Diffuse[68] = 0.55f;
    //
	//
    
	Material_Specular[66] = 0.7f;
	Material_Specular[67] = 0.7f;
	Material_Specular[68] = 0.7f;
	//
    //

	Material_Shininess[22] = 0.07813f * 128.0f;

// 6- Yellow
	Material_Ambient[69] = 0.0f;
	Material_Ambient[70] = 0.0f;
	Material_Ambient[71] = 0.0f;

	Material_Diffuse[69] = 0.5f;
	Material_Diffuse[70] = 0.5f;
	Material_Diffuse[71] = 0.0f;
    
	Material_Specular[69] = 0.6f;
	Material_Specular[70] = 0.6f;
	Material_Specular[71] = 0.5f;

	Material_Shininess[23] = 0.07813f * 128.0f;


// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	//  glDrawArrays() 
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);

	//imp
	int width = (GLint)(g_Width / 6);
	int height = (GLint)(g_Height / 4);

  	int i,j,k;

  // material : i*6+j
  for(i = 0; i <= 6; i++) //x
   {
    for(j = 0; j <= 4; j++) //y
	{
		//1
		glUniform1f(u_Material_Shininess,Material_Shininess[i*j]);
		
		glUniform3f(KS_Uniform,Material_Specular[3*i+j],Material_Specular[3*i+j+1],Material_Specular[3*i+j+2]);		
		glUniform3f(KD_Uniform,Material_Diffuse[3*i+j],Material_Diffuse[3*i+j+1],Material_Diffuse[3*i+j+2]);
		glUniform3f(KA_Uniform,Material_Ambient[3*i+j],Material_Ambient[3*i+j+1],Material_Ambient[3*i+j+2]);

		glUniform3f(KS_Uniform_Red,Material_Specular[3*i+j],Material_Specular[3*i+j+1],Material_Specular[3*i+j+2]);
		glUniform3f(KD_Uniform_Red, Material_Diffuse[3*i+j],Material_Diffuse[3*i+j+1],Material_Diffuse[3*i+j+2]);
		glUniform3f(KA_Uniform_Red, Material_Ambient[3*i+j],Material_Ambient[3*i+j+1],Material_Ambient[3*i+j+2]);

		glUniform3f(KS_Uniform_Green,Material_Specular[3*i+j],Material_Specular[3*i+j+1],Material_Specular[3*i+j+2]);
		glUniform3f(KD_Uniform_Green,Material_Diffuse[3*i+j],Material_Diffuse[3*i+j+1],Material_Diffuse[3*i+j+2]);
		glUniform3f(KA_Uniform_Green, Material_Ambient[3*i+j],Material_Ambient[3*i+j+1],Material_Ambient[3*i+j+2]);

		//2
		glViewport(width*i, height*j, width, height);	

		Perspective_Projection_Matrix = perspective(45.0f, (GLfloat)(width) / (GLfloat)(height), 0.1f, 100.0f);

		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	}

 }


	// *** unbind vao ***
	glBindVertexArray(0);


	//Common
	glUseProgram(0);

	SwapBuffers(ghdc);
}


void Update(void)
{
	AngleTri += 0.01f;

	if (AngleTri >= 360.0f)
		AngleTri -= 360.0f;
}



//ToggleFullScreen
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	mi.cbSize = sizeof(MONITORINFO);

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//mi = { sizeof(MONITORINFO) };// not supported in VS2008

		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			if ((GetWindowPlacement(ghwnd, &wpPrev)) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),// WD
					(mi.rcMonitor.bottom - mi.rcMonitor.top), // HT
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}

	else
		if (gbIsFullScreen == true)
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

			SetWindowPlacement(ghwnd, &wpPrev);

			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
				SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

			ShowCursor(TRUE);
			gbIsFullScreen = false;

		}
}


//UnInitialize
void UnInitialize(void)
{
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	//for Shaders

	glUseProgram(gShader_Program_Object);
	glDetachShader(gShader_Program_Object, gFragment_Shader_Object);
	glDetachShader(gShader_Program_Object, gVertex_Shader_Object);

	glDeleteShader(gFragment_Shader_Object);
	gFragment_Shader_Object = 0;

	glDeleteShader(gVertex_Shader_Object);
	gVertex_Shader_Object = 0;

	glDeleteProgram(gShader_Program_Object);
	glUseProgram(0);

	
	//Break Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "\n Application Closed ");
	fclose(gpFile);
	gpFile = NULL;
}

