
/*

	Assignment : Curve using Shaders

*/

//Headers
#include<stdio.h>
#include<windows.h>
#include<stdlib.h>
#include "GL\glew.h"
#include "vmath.h"

#pragma comment (lib,"opengl32.lib")
#pragma comment (lib,"gdi32.lib")
#pragma comment (lib,"user32.lib")
#pragma comment (lib,"glew32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

// Globals
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
FILE *gpFile = NULL;

bool gbActiveWindow = false;
bool gbIsFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
GLenum Result;

//For Shaders
GLuint gVertex_Shader_Object;
GLuint gFragment_Shader_Object;
GLuint gShader_Program_Object;

GLint iShaderCompileStatus = 0;
GLint iInfoLogLength = 0;
GLchar *szInfoLog = NULL;
GLint iProgrmLinkStatus = 0;

//for VS ,FS
GLuint vao;
GLuint vbo;
GLuint mvpUniform;
mat4 Perspective_Projection_Matrix;

//for TCS
GLuint gTessellationControl_Shader_Object;
GLuint gTessellationEvaluation_Shader_Object;

unsigned int gNumOfLineSegments; // Binding
GLuint gNumberOfSegUniform;	// in one line

GLuint gNumberOfStripsUniform; // Group of Lines
GLuint gLineColorUniform; // imp : Set Color via uniform


//ENUM
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

GLsizei shaderCount, shaderNumber;

// Function Declarations

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int Initialize(void);
void Resize(int, int); // imp
void Display(void);
void UnInitialize(void);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIns, LPSTR lpCmdLine, int iCmdShow)
{
	
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Shape");
	int iRet = 0;
	bool bDone = false;

	//File IO
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can not Created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	else
	{
		fprintf(gpFile, "Log File Successfully Created");
	}

	//initialize the class
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(hInstance, IDC_HAND);
	wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.cbWndExtra = 0;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("TCS & TES : Beziear Curve "),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100, 100, WIN_WIDTH, WIN_HEIGHT,
		NULL, NULL, hInstance, NULL);

	// For Update
	ghwnd = hwnd;
	iRet = Initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\n ChoosePixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -2)
	{
		fprintf(gpFile, "\n SetPixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -3)
	{
		fprintf(gpFile, "\n wglCreateContext Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -4)
	{
		fprintf(gpFile, "\n wglMakeCurrent Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == 0)
	{
		fprintf(gpFile, "\n Initialization Successful");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd); //UpdateWindow
	SetFocus(hwnd);

	// Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else if (gbActiveWindow == true)
		{

		}

		Display();
	}

	return((int)msg.wParam);

}//Main Ends

 //WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function Declarations
	void ToggleFullScreen(void);
	void Resize(int, int);
	void UnInitialize(void);


	switch (iMsg)
	{
	case WM_CREATE:
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case VK_UP:
			gNumOfLineSegments++;
			if (gNumOfLineSegments >= 50)
				gNumOfLineSegments = 50;
			break;
		
		case VK_DOWN:
			gNumOfLineSegments--;
			if (gNumOfLineSegments <= 0)
				gNumOfLineSegments = 1;
			break;

		case 0x46:
			ToggleFullScreen();
			break;

		}
		break;

	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}//WndProc Ends


 //init()
int Initialize(void)
{

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;

	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	pfd.cDepthBits = 32; //1

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	else if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	//Bridging API
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	else if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}
		

	//*****************************

	Result = glewInit();

	if (Result != GLEW_OK)
	{
		UnInitialize();
	}


	//For Shaders : create 
	gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

	//Define VS code
	GLchar *VertexShaderCode =
		"#version 450 core"							\
		"\n"										\
		"in vec2 vPosition;"						\
		"\n"										\
		"void main(void)"							\
		"{"											\
		"gl_Position = vec4(vPosition,0.0,1.0);"	\
		"}" ;

	//Specify code to Obj
	glShaderSource(gVertex_Shader_Object,
					1,
				(const GLchar**)&VertexShaderCode,
				NULL);

	//Compile the shader
	glCompileShader(gVertex_Shader_Object);

	//Error Code for VS
	glGetShaderiv(gVertex_Shader_Object,
				GL_COMPILE_STATUS,
				&iShaderCompileStatus);


	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertex_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gVertex_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Vertex Shader");
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//TCS
	gTessellationControl_Shader_Object = glCreateShader(GL_TESS_CONTROL_SHADER);

	//code
	const GLchar *TessellationControlShaderCode =
		"#version 450 core"				\
		"\n"						\
		"layout(vertices = 4)out;"  \
		"uniform int numberOfSegments;"\
		"uniform int numberOfStrips;"  \
		"\n" \
		"void main()"\
		"{" \
		"gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;"  \
		"gl_TessLevelOuter[0] = float(numberOfStrips);  " \
		"gl_TessLevelOuter[1] = float(numberOfSegments);" \
		"}";

	//Specify code to Obj
	glShaderSource(gTessellationControl_Shader_Object,
					1,
				(const GLchar**)&TessellationControlShaderCode,
				NULL);

	//Compile the shader
	glCompileShader(gTessellationControl_Shader_Object);

	//Error Code
	glGetShaderiv(gTessellationControl_Shader_Object,
				GL_COMPILE_STATUS,
				&iShaderCompileStatus);


	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertex_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gTessellationControl_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in TC Shader : %s",szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//TES
	gTessellationEvaluation_Shader_Object = glCreateShader(GL_TESS_EVALUATION_SHADER);

	//code
	const GLchar *TessellationEvalShaderCode =
		"#version 450 core"  \
		"\n"			\
		"layout(isolines)in;" \
		"uniform mat4 u_mvp_matrix;"	\
		"\n"	\
		"void main ()" \
		"{" \
		"float u = gl_TessCoord.x;" \
		"vec3 p0 = gl_in[0].gl_Position.xyz;" \
		"vec3 p1 = gl_in[1].gl_Position.xyz;" \
		"vec3 p2 = gl_in[2].gl_Position.xyz;" \
		"vec3 p3 = gl_in[3].gl_Position.xyz;" \
		"float u1 = (1.0 - u);" \
		"float u2 = u * u;" \
		"float b3 = u2 * u;" \
		"float b2 = 3.0 * u2 * u1;" \
		"float b1 = 3.0 * u * u1 * u1;" \
		"float b0 = u1 * u1 * u1;" \
		"vec3 p = p0*b0 + p1*b1 + p2*b2 + p3*b3;" \
		"gl_Position = u_mvp_matrix * vec4(p,1.0);" \
		"}";
		
		
	//Specify code to Obj
	glShaderSource(gTessellationEvaluation_Shader_Object,1,
				(const GLchar**)&TessellationEvalShaderCode,NULL);

	//Compile the shader
	glCompileShader(gTessellationEvaluation_Shader_Object);

	//Error Code
	glGetShaderiv(gTessellationEvaluation_Shader_Object, GL_COMPILE_STATUS,
				  &iShaderCompileStatus);


	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gTessellationEvaluation_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gTessellationEvaluation_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in TE Shader : %s", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

		
		
	//4 - FS
	gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

	//Define FS code
	GLchar *FragmentShaderCode = 
		"#version 450 core" \
		"\n" \
		"uniform vec4 lineColor;" \
		"out vec4 FragColor;"
		"void main(void)" \
		" { " \
		"FragColor = lineColor;"
		" } " ;

	//Specify code to Obj
	glShaderSource(gFragment_Shader_Object,1,
		( GLchar**)&FragmentShaderCode,NULL);

	//Compile the shader
	glCompileShader(gFragment_Shader_Object);

	//Error check for FS
	glGetShaderiv(gFragment_Shader_Object,
		GL_COMPILE_STATUS,
		&iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragment_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gFragment_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Fragment Shader");
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//3 - Create Shader Program
	gShader_Program_Object = glCreateProgram();

	//attach VS & FS
	glAttachShader(gShader_Program_Object, gVertex_Shader_Object);
	
	//new
	glAttachShader(gShader_Program_Object, gTessellationControl_Shader_Object);
	glAttachShader(gShader_Program_Object, gTessellationEvaluation_Shader_Object);
	
	glAttachShader(gShader_Program_Object, gFragment_Shader_Object);

	//prelink
	glBindAttribLocation(gShader_Program_Object,AMC_ATTRIBUTE_POSITION,"vPosition");

	//Link SP
	glLinkProgram(gShader_Program_Object);


	// Error check for SP
	glGetProgramiv(gShader_Program_Object, GL_LINK_STATUS, &iProgrmLinkStatus);


	if (iProgrmLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShader_Program_Object, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetProgramInfoLog(gShader_Program_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Shader Program Linking");
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//postlink
	mvpUniform = glGetUniformLocation(gShader_Program_Object,"u_mvp_matrix");

//for TS
	//get seg-uniform
	gNumberOfSegUniform = glGetUniformLocation(gShader_Program_Object,"numberOfSegments");
	
	//get strips uniform
	gNumberOfStripsUniform = glGetUniformLocation(gShader_Program_Object,"numberOfStrips");
	
	//Line Color Unifrom
	gLineColorUniform = glGetUniformLocation(gShader_Program_Object,"lineColor");
	
	//Vertices
	float vertices[] = {-1.0f, -1.0f, -0.5f, 1.0f,
						0.5f,-1.0f,1.0f,1.0f };
	
	//create vao
	glGenVertexArrays(1, &vao);

	//Binding
	glBindVertexArray(vao);

	//Buffer
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER,vbo);

	// 8*sizeof(float) : use 
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices),vertices,GL_STATIC_DRAW); 

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0); //vao

	//Depth 
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	glLineWidth(3.0f);
	gNumOfLineSegments = 1;
	
	Perspective_Projection_Matrix = mat4::identity();

	//Warm up call 
	Resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}


//ToggleFullScreen
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	mi.cbSize = sizeof(MONITORINFO);

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//mi = { sizeof(MONITORINFO) };// not supported in VS2008

		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			if ((GetWindowPlacement(ghwnd, &wpPrev)) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),// WD
					(mi.rcMonitor.bottom - mi.rcMonitor.top), // HT
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}

		}
		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}

	else
		if (gbIsFullScreen == true)
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

			SetWindowPlacement(ghwnd, &wpPrev);

			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
				SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

			ShowCursor(TRUE);
			gbIsFullScreen = false;

		}
}


//UnInitialize
void UnInitialize(void)
{
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	//for Shaders
	if (vbo)
	{
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}

	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	glUseProgram(gShader_Program_Object);
	glDetachShader(gShader_Program_Object, gFragment_Shader_Object);
	glDetachShader(gShader_Program_Object, gVertex_Shader_Object);

	glDeleteShader(gFragment_Shader_Object);
	gFragment_Shader_Object = 0;

	glDeleteShader(gVertex_Shader_Object);
	gVertex_Shader_Object = 0;

	glDeleteProgram(gShader_Program_Object);
	glUseProgram(0);

	

	//Break Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "\n Application Closed ");
	fclose(gpFile);
	gpFile = NULL;
}


//Resize
void Resize(int Width, int Height)
{
	if (Height == 0)
	{
		Height = 1;
	}

	glViewport(0, 0, GLsizei(Width), GLsizei(Height));
	Perspective_Projection_Matrix = perspective(45.0f, ((GLfloat)Width / (GLfloat)Height), 0.1f, 100.0f);
	
}


//Display
void Display(void)
{
	//1
	mat4 ModelView_Matrix;
	mat4 ModelView_Projection_Matrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShader_Program_Object);

	//code : 9 steps
	//2 : I[]
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();

	//3 : Transforms
	ModelView_Matrix = translate(0.25f, 0.25f, -4.0f);

	//4 : Do Mat mul
	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;

	// 5 : send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	//new : TCS
	glUniform1i(gNumberOfSegUniform, gNumOfLineSegments);
	
	//show
	TCHAR str[255];
	wsprintf(str, TEXT("OpenGL Bezier Curve with Segments = %d"),gNumOfLineSegments);
	SetWindowText(ghwnd, str);

	glUniform1i(gNumberOfStripsUniform, 1); //Single

	//color
	glUniform4fv(gLineColorUniform, 1, vec4(1.0f,1.0f,0.0f,1.0f));

	// 6 : bind to vao
	glBindVertexArray(vao);

	// 7: TCS
	glPatchParameteri(GL_PATCH_VERTICES, 4); // imp

	//8 : draw scene
	glDrawArrays(GL_PATCHES, 0, 4);

	//9 : Unbind vao
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);
}

