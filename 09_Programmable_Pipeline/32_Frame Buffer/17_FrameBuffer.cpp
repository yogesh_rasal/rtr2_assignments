
/*

	Assignment :
	1. Create Framebuffers
	2. Bind it
	3. GenRenderBuffer
	4. Bind to Renderbuffer
	5. Get Drawable
	6. Put in FrameBuffer

*/

//Headers
#include <stdio.h>
#include <windows.h>
#include "GL\glew.h"
#include <stdlib.h>
#include "vmath.h"
#include "resource.h"

#pragma comment (lib,"gdi32.lib")
#pragma comment (lib,"opengl32.lib")
#pragma comment (lib,"user32.lib")
#pragma comment (lib,"glew32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

// Globals
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
FILE* gpFile = NULL;

bool gbActiveWindow = false;
bool gbIsFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
GLenum Result;

//imp
int gWidth, gHeight;

//For Shaders
GLuint gVertex_Shader_Object;
GLuint gFragment_Shader_Object;
GLuint gShader_Program_Object;

GLint iShaderCompileStatus = 0;
GLint iInfoLogLength = 0;
GLchar* szInfoLog = NULL;
GLint iProgrmLinkStatus = 0;

GLuint mvpUniform;
GLuint Texture_Sampler_Uniform;

bool bAnimate = false;
GLfloat AngleTri = 0.0f;

//mat
mat4 Perspective_Projection_Matrix;

//Cube
GLuint Vao_cube;
GLuint Vbo_cube_position;
GLuint Vbo_cube_texture; // most imp

//Texture
GLuint Texture_Marble;


// FBO variables
GLuint Fbo_VertexShaderObject;
GLuint Fbo_FragmentShaderObject;
GLuint Fbo_ShaderProgramObject;

GLuint Vao_Innercube; // vao
GLuint Vbo_Innercube; //  vbo
GLuint Vbo_texture_cube;

//RT
GLuint fbo;
GLuint fbo_depth;
GLuint fbo_texture;

//Uniforms
GLuint fbo_mvpUniform;
GLuint fbo_samplerUniform;

//ENUM
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

GLsizei shaderCount, shaderNumber;

// Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int Initialize(void);
void Display(void);
void UnInitialize(void);
void Update(void);
void ToggleFullScreen(void);
void Resize(int, int);
void UnInitialize(void);

//imp
BOOL LoadTexture(GLuint*, TCHAR[]);



//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIns, LPSTR lpCmdLine, int iCmdShow)
{
	//code
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("PPShape");
	int iRet = 0;
	bool bDone = false;

	//File IO
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can not Created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	else
	{
		fprintf_s(gpFile, "Log File Successfully Created");
	}

	//initialize the class
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(hInstance, IDC_HAND);
	wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.cbWndExtra = 0;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("PP Framebuffer By YSR"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100, 100, WIN_WIDTH, WIN_HEIGHT,
		NULL, NULL, hInstance, NULL);

	// For Update
	ghwnd = hwnd;
	iRet = Initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\n ChoosePixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -2)
	{
		fprintf(gpFile, "\n SetPixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -3)
	{
		fprintf(gpFile, "\n wglCreateContext Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -4)
	{
		fprintf(gpFile, "\n wglMakeCurrent Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == 0)
	{
		fprintf(gpFile, "\n Initialization Successful");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd); //UpdateWindow
	SetFocus(hwnd);

	// Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else if (gbActiveWindow == true)
		{
			Update();
		}

		Display();
	}

	return((int)msg.wParam);

}//Main Ends


 //WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_CREATE:
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		gWidth = LOWORD(lParam);
		gHeight = HIWORD(lParam);

		Resize(LOWORD(lParam), HIWORD(lParam));

		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;

		case 'A':
		case 'a':
			if (!bAnimate)
			{
				bAnimate = true;
			}
			else
			{
				bAnimate = false;
			}
			break;
		}
		break;

	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}//WndProc Ends


 //init()
int Initialize(void)
{

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;

	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	else if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	//Bridging API
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	else if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}


	//*****************************

	Result = glewInit();

	if (Result != GLEW_OK)
	{
		UnInitialize();
	}


	// Inner Shape
	Fbo_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Define VS code
	const GLchar* FBO_VertexShaderCode =
		"#version 450 core"				\
		"\n"							\
		"in vec4 vPosition;"			\
		"in vec2 vTexture_Coord;"		\
		"uniform mat4 u_mvp_matrix;"	\
		"out vec2 out_texture_coord;"	\
		"\n"							\
		"void main (void)"				\
		"{"								\
		"gl_Position = u_mvp_matrix*vPosition;"	\
		"out_texture_coord = vTexture_Coord;"	\
		"}";


	//Specify code to Obj
	glShaderSource(Fbo_VertexShaderObject,
		1,
		(const GLchar**)&FBO_VertexShaderCode,
		NULL);

	//Compile the shader
	glCompileShader(Fbo_VertexShaderObject);

	//Error Code for VS
	glGetShaderiv(Fbo_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);


	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(Fbo_VertexShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(Fbo_VertexShaderObject, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in FBO Vertex Shader");
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//2 - FS
	Fbo_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Define FS code
	const GLchar* FBO_FragmentShaderCode =
		"#version 450 core"										 \
		"\n"													 \
		"in vec2 out_texture_coord;"							 \
		"out vec4 FragColor;"									 \
		"uniform sampler2D u_texture_sampler;"					 \
		"\n"													 \
		"void main (void)"										 \
		"{ "		\
		"FragColor = texture(u_texture_sampler,out_texture_coord);" \
		"}";



	//Specify code to Obj
	glShaderSource(Fbo_FragmentShaderObject, 1,
		(GLchar**)&FBO_FragmentShaderCode, NULL);

	//Compile the shader
	glCompileShader(Fbo_FragmentShaderObject);

	//Error check for FS
	glGetShaderiv(Fbo_FragmentShaderObject,
		GL_COMPILE_STATUS,
		&iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(Fbo_FragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(Fbo_FragmentShaderObject, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in FBO Fragment Shader");
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//3 - Create Shader Program
	Fbo_ShaderProgramObject = glCreateProgram();

	//attach VS & FS
	glAttachShader(Fbo_ShaderProgramObject, Fbo_VertexShaderObject);

	glAttachShader(Fbo_ShaderProgramObject, Fbo_FragmentShaderObject);

	//prelink
	glBindAttribLocation(Fbo_ShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(Fbo_ShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD, "vTexture_Coord");

	//Link SP
	glLinkProgram(Fbo_ShaderProgramObject);


	// Error check for SP
	glGetProgramiv(Fbo_ShaderProgramObject, GL_LINK_STATUS, &iProgrmLinkStatus);


	if (iProgrmLinkStatus == GL_FALSE)
	{
		glGetProgramiv(Fbo_ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetProgramInfoLog(Fbo_ShaderProgramObject, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in FBO Shader Program Linking");
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//FBO uniforms
	fbo_mvpUniform = glGetUniformLocation(Fbo_ShaderProgramObject, "u_mvp_matrix");

	fbo_samplerUniform = glGetUniformLocation(Fbo_ShaderProgramObject, "u_texture_sampler");


	// Outer Shapes

		//For Shaders : create 
	gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

	//Define VS code
	const GLchar* VertexShaderCode =
		"#version 450 core"				\
		"\n"							\
		"in vec4 vPosition;"			\
		"in vec2 vTexture_Coord;"		\
		"uniform mat4 u_mvp_matrix;"	\
		"out vec2 out_texture_coord;"	\
		"\n"							\
		"void main (void)"				\
		"{"								\
		"gl_Position = u_mvp_matrix*vPosition;"	\
		"out_texture_coord = vTexture_Coord;"	\
		"}";


	//Specify code to Obj
	glShaderSource(gVertex_Shader_Object,
		1,
		(const GLchar**)&VertexShaderCode,
		NULL);

	//Compile the shader
	glCompileShader(gVertex_Shader_Object);

	//Error Code for VS
	glGetShaderiv(gVertex_Shader_Object,
		GL_COMPILE_STATUS,
		&iShaderCompileStatus);


	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertex_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gVertex_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Vertex Shader");
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//2 - FS
	gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

	//Define FS code
	const GLchar* FragmentShaderCode =
		"#version 450 core"										 \
		"\n"													 \
		"in vec2 out_texture_coord;"							 \
		"out vec4 FragColor;"									 \
		"uniform sampler2D u_texture_sampler;"					 \
		"\n"													 \
		"void main (void)"										 \
		"{ "		\
		"FragColor = texture(u_texture_sampler,out_texture_coord);" \
		"}";

	//Specify code to Obj
	glShaderSource(gFragment_Shader_Object, 1,
		(GLchar**)&FragmentShaderCode, NULL);

	//Compile the shader
	glCompileShader(gFragment_Shader_Object);

	//Error check for FS
	glGetShaderiv(gFragment_Shader_Object,
		GL_COMPILE_STATUS,
		&iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragment_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gFragment_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Fragment Shader");
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//3 - Create Shader Program
	gShader_Program_Object = glCreateProgram();

	//attach VS & FS
	glAttachShader(gShader_Program_Object, gVertex_Shader_Object);

	glAttachShader(gShader_Program_Object, gFragment_Shader_Object);

	//prelink
	glBindAttribLocation(gShader_Program_Object, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShader_Program_Object, AMC_ATTRIBUTE_TEXCOORD, "vTexture_Coord");

	//Link SP
	glLinkProgram(gShader_Program_Object);


	// Error check for SP
	glGetProgramiv(gShader_Program_Object, GL_LINK_STATUS, &iProgrmLinkStatus);


	if (iProgrmLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShader_Program_Object, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetProgramInfoLog(gShader_Program_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Shader Program Linking");
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//postlink : Uniforms

	mvpUniform = glGetUniformLocation(gShader_Program_Object, "u_mvp_matrix");

	Texture_Sampler_Uniform = glGetUniformLocation(gShader_Program_Object, "u_texture_sampler");


	// Cube

	const GLfloat cubeVertices[] =
	{
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,

		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,

		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f
	};


	//cube texture coordinates
	const GLfloat cubeTextCoords[] =
	{
		0.0f,0.0f,
		1.0f,0.0f,
		1.0f,1.0f,
		0.0f,1.0f,

		1.0f,0.0f,
		1.0f,1.0f,
		0.0f,1.0f,
		0.0f,0.0f,

		1.0f,0.0f,
		1.0f,1.0f,
		0.0f,1.0f,
		0.0f,0.0f,

		0.0f,0.0f,
		1.0f,0.0f,
		1.0f,1.0f,
		0.0f,1.0f,

		0.0f,1.0f,
		0.0f,0.0f,
		1.0f,0.0f,
		1.0f,1.0f,

		1.0f,1.0f,
		0.0f,1.0f,
		0.0f,0.0f,
		1.0f,0.0f
	};


	//FBO

		// VAO
	glGenVertexArrays(1, &Vao_Innercube);
	glBindVertexArray(Vao_Innercube);

	glGenBuffers(1, &Vbo_Innercube);

	glBindBuffer(GL_ARRAY_BUFFER, Vbo_Innercube);

	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);


	// Texture
	glGenBuffers(1, &Vbo_texture_cube);

	glBindBuffer(GL_ARRAY_BUFFER, Vbo_texture_cube);

	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTextCoords), cubeTextCoords, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, // Type 
		GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD);

	//Unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	// Outer Cube

		//Vao
	glGenVertexArrays(1, &Vao_cube);
	glBindVertexArray(Vao_cube);

	glGenBuffers(1, &Vbo_cube_position);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_cube_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//Texture
	glGenBuffers(1, &Vbo_cube_texture); //VIMP
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_cube_texture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTextCoords), cubeTextCoords, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


	//Actual FBO
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	glGenTextures(1, &fbo_texture);
	glBindTexture(GL_TEXTURE_2D, fbo_texture);

	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, 1024, 1024);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, fbo_texture, 0);

	//Depth
	glGenRenderbuffers(1, &fbo_depth);
	glBindRenderbuffer(GL_RENDERBUFFER, fbo_depth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, 1024, 1024);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, fbo_depth);

	//imp
	static const GLenum draw_buffer[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, draw_buffer);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	//texture
	glEnable(GL_TEXTURE_2D);

	//Depth 
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//imp
	LoadTexture(&Texture_Marble, MAKEINTRESOURCE(IDBITMAP_MARBLE));

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	Perspective_Projection_Matrix = mat4::identity();

	//Warm up call 
	Resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}


//Resize
void Resize(int Width, int Height)
{
	if (Height == 0)
	{
		Height = 1;
	}

	glViewport(0, 0, GLsizei(Width), GLsizei(Height));

	Perspective_Projection_Matrix = perspective(45.0f, (GLfloat)(Width) / (GLfloat)(Height), 1.0f, 100.0f);

}


//Texture Loading
BOOL LoadTexture(GLuint* texture, TCHAR imageResourceId[])
{
	HBITMAP hBitmap = NULL;
	BITMAP bmp;
	BOOL bStatus = FALSE;

	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

	if (hBitmap)
	{
		bStatus = TRUE;

		GetObject(hBitmap, sizeof(BITMAP), &bmp);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

		glGenTextures(1, texture);

		glBindTexture(GL_TEXTURE_2D, *texture);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		//new
		glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGB,
			bmp.bmWidth,
			bmp.bmHeight,
			0,
			GL_BGR_EXT,
			GL_UNSIGNED_BYTE,
			bmp.bmBits);

		glGenerateMipmap(GL_TEXTURE_2D); //new
		glBindTexture(GL_TEXTURE_2D, 0);

		DeleteObject(hBitmap);
	}
	return(bStatus);
}


//Display
void Display(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_TEXTURE_2D);

	//code : 9 steps

	//1
	mat4 Model_Matrix;
	mat4 Projection_Matrix;
	mat4 Translation_Matrix;
	mat4 Rotation_Matrix;
	mat4 ModelView_Projection_Matrix;

	Model_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();
	Rotation_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();

	//fbo
	glUseProgram(gShader_Program_Object);

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	glViewport(0, 0, 1080, 1080);

	glClearColor(0.80f, 0.40f, 0.10f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	//Matrices
	Translation_Matrix = translate(0.0f, 0.0f, -6.5f);

	Rotation_Matrix = rotate(AngleTri*2, 1.0f, 0.0f, 0.0f);

	Model_Matrix = Translation_Matrix * Rotation_Matrix;

	ModelView_Projection_Matrix = Perspective_Projection_Matrix * Model_Matrix;

	glUniformMatrix4fv(fbo_mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);


	// texture bind code
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, Texture_Marble);
	glUniform1i(Texture_Sampler_Uniform, 0);

//inner cube with texture
	glBindVertexArray(Vao_Innercube);

	//Scene
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

	glBindVertexArray(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glUseProgram(0);


//Outer Cube
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Very imp
	glViewport(0, 0, gWidth, gHeight);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(Fbo_ShaderProgramObject);


	//2 : I[]
	Model_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	Rotation_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//4 : Do Mat mul

	Translation_Matrix = translate(0.0f, 0.0f, -6.5f);

	Rotation_Matrix = rotate(AngleTri, 1.0f, 0.0f, 0.0f);
	Rotation_Matrix *= rotate(AngleTri, 0.0f, 1.0f, 0.0f);
	Rotation_Matrix *= rotate(AngleTri, 0.0f, 0.0f, 1.0f);

	Model_Matrix = scale(0.75f,0.75f,0.75f);

	Model_Matrix *= Translation_Matrix * Rotation_Matrix;

	//imp
	ModelView_Projection_Matrix = Perspective_Projection_Matrix * Model_Matrix;

	//VIMP
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	//ABU
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, fbo_texture);
	glUniform1i(fbo_samplerUniform, 0); //try

	// 6 : bind to vao
	glEnable(GL_TEXTURE_2D);

	glBindVertexArray(Vao_cube);

	//8 : draw scene
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

	// *** unbind vao ***
	glBindVertexArray(0);

	glBindTexture(GL_TEXTURE_2D, 0);

	glUseProgram(0);

	SwapBuffers(ghdc);
}


void Update(void)
{
	AngleTri += 0.2f;

	if (AngleTri >= 360.0f)
		AngleTri -= 350.0f;

}


//ToggleFullScreen
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	mi.cbSize = sizeof(MONITORINFO);

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//mi = { sizeof(MONITORINFO) };// not supported in VS2008

		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			if ((GetWindowPlacement(ghwnd, &wpPrev)) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),// WD
					(mi.rcMonitor.bottom - mi.rcMonitor.top), // HT
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}

	else
		if (gbIsFullScreen == true)
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

			SetWindowPlacement(ghwnd, &wpPrev);

			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
				SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

			ShowCursor(TRUE);
			gbIsFullScreen = false;

		}
}


//UnInitialize	
void UnInitialize(void)
{
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}


	//for Shaders

	if (Vao_cube)
	{
		glDeleteBuffers(1, &Vao_cube);
		Vao_cube = 0;
	}


	glUseProgram(gShader_Program_Object);
	glDetachShader(gShader_Program_Object, gFragment_Shader_Object);
	glDetachShader(gShader_Program_Object, gVertex_Shader_Object);

	glDeleteShader(gFragment_Shader_Object);
	gFragment_Shader_Object = 0;

	glDeleteShader(gVertex_Shader_Object);
	gVertex_Shader_Object = 0;

	glDeleteProgram(gShader_Program_Object);
	glUseProgram(0);


	//Break Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "\n Application Closed ");
	fclose(gpFile);
	gpFile = NULL;
}

