
/*

	Assignment :  Triangle Rectangle using Shaders

*/

//Headers
#include <stdio.h>
#include <windows.h>
#include "GL\glew.h"
#include <stdlib.h>
#include "vmath.h"

#pragma comment (lib,"opengl32.lib")
#pragma comment (lib,"gdi32.lib")
#pragma comment (lib,"user32.lib")
#pragma comment (lib,"glew32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define SLICES 20

using namespace vmath;

// Globals
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
FILE *gpFile = NULL;
GLfloat AngleTri = 0.0f, AngleRect = 0.0f;

bool gbActiveWindow = false;
bool gbIsFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
GLenum Result;

//For Shaders
GLuint gVertex_Shader_Object;
GLuint gFragment_Shader_Object;
GLuint gShader_Program_Object;

GLint iShaderCompileStatus = 0;
GLint iInfoLogLength = 0;
GLchar *szInfoLog = NULL;
GLint iProgrmLinkStatus = 0;

//new
GLuint vao_Triangle,vao_Rectagle;
GLuint vbo_Position_Triangle;
GLuint vbo_Position_Rectangle;
GLuint vbo_Color_Triangle;
GLuint vbo_Color_Rectangle;

//Graph
GLuint vbo_Position_Graph , vao_Graph ,	vbo_Color_Graph; //for old

//for new
GLuint vao_XAxis, vao_YAxis; 
GLuint vbo_XPosition,vbo_XColor;
GLuint vbo_YPosition, vbo_YColor;
GLuint vao_Lines, vao_HLines;
GLuint vbo_PositionHLines, vbo_PositionVLines;
GLuint vbo_ColorHLines, vbo_ColorVLines;

GLuint mvpUniform;
mat4 Perspective_Projection_Matrix;


//ENUM
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

GLsizei shaderCount, shaderNumber;


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Display(void);
void UnInitialize(void);
void Update(void);
int Initialize(void);

void showGraph();

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIns, LPSTR lpCmdLine, int iCmdShow)
{
	// Function Declarations

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Shape");
	int iRet = 0;
	bool bDone = false;

	//File IO
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can not Created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	else
	{
		fprintf(gpFile, "Log File Successfully Created");
	}

	//initialize the class
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(hInstance, IDC_HAND);
	wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.cbWndExtra = 0;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("All PP Shapes  : Perspective"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100, 100, WIN_WIDTH, WIN_HEIGHT,
		NULL, NULL, hInstance, NULL);

	// For Update
	ghwnd = hwnd;
	iRet = Initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\n ChoosePixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -2)
	{
		fprintf(gpFile, "\n SetPixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -3)
	{
		fprintf(gpFile, "\n wglCreateContext Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -4)
	{
		fprintf(gpFile, "\n wglMakeCurrent Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == 0)
	{
		fprintf(gpFile, "\n Initialization Successful");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd); //UpdateWindow
	SetFocus(hwnd);

	// Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else if (gbActiveWindow == true)
		{
			Update();
		}

		Display();
	}

	return((int)msg.wParam);

}//Main Ends

 //WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function Declarations
	void ToggleFullScreen(void);
	void Resize(int, int);
	void UnInitialize(void);


	switch (iMsg)
	{
	case WM_CREATE:

		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:

		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;

		}
		break;

	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}//WndProc Ends

 //init()
int Initialize(void)
{
	void Resize(int, int); // imp
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;

	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	pfd.cDepthBits = 32; //1

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	else if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	//Bridging API
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	else if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}
		

	//*****************************

	Result = glewInit();

	if (Result != GLEW_OK)
	{
		UnInitialize();
	}


	//For Shaders : create 
	gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

	//Define VS code
	GLchar *VertexShaderCode =
		"#version 450 core"							\
		"\n"										\
		"in vec4 vPosition;"						\
		"in vec4 vColor;"							\
		"out vec4 Out_Color;"						\
		"uniform mat4 u_mvp_matrix;"				\
		"void main(void)"							\
		"{"											\
		"gl_Position = u_mvp_matrix * vPosition;"	\
		"Out_Color = vColor;"						\
		"}" ;

	//Specify code to Obj
	glShaderSource(gVertex_Shader_Object,
					1,
				(const GLchar**)&VertexShaderCode,
				NULL);

	//Compile the shader
	glCompileShader(gVertex_Shader_Object);

	//Error Code for VS
	glGetShaderiv(gVertex_Shader_Object,
				GL_COMPILE_STATUS,
				&iShaderCompileStatus);


	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertex_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gVertex_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Vertex Shader");
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//2 - FS
	gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

	//Define FS code
	GLchar *FragmentShaderCode =
		"#version 450 core"		\
		"\n"					\
		"in vec4 Out_Color;"	\
		"out vec4 FragColor;"	\
		"void main(void)"		\
		" { "					\
		"FragColor = Out_Color;" \
		" } " ;

	//Specify code to Obj
	glShaderSource(gFragment_Shader_Object,1,
		( GLchar**)&FragmentShaderCode,NULL);

	//Compile the shader
	glCompileShader(gFragment_Shader_Object);

	//Error check for FS
	glGetShaderiv(gFragment_Shader_Object,
		GL_COMPILE_STATUS,
		&iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragment_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gFragment_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Fragment Shader");
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//3 - Create Shader Program
	gShader_Program_Object = glCreateProgram();

	//attach VS & FS
	glAttachShader(gShader_Program_Object, gVertex_Shader_Object);

	glAttachShader(gShader_Program_Object, gFragment_Shader_Object);

	//prelink
	glBindAttribLocation(gShader_Program_Object,AMC_ATTRIBUTE_POSITION,"vPosition");

	//Link SP
	glLinkProgram(gShader_Program_Object);


	// Error check for SP
	glGetProgramiv(gShader_Program_Object, GL_LINK_STATUS, &iProgrmLinkStatus);


	if (iProgrmLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShader_Program_Object, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetProgramInfoLog(gShader_Program_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Shader Program Linking");
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//postlink
	mvpUniform = glGetUniformLocation(gShader_Program_Object,"u_mvp_matrix");

	//showGraph
	showGraph();

	//use Triangle vertices
	const GLfloat Triangle_Vertices[] = { 0.0f,1.0f,0.0f,
										-1.0f,-1.0f,0.0f,
										1.0f,-1.0f,0.0f,
										0.0f,1.0f,0.0f,
										1.0f,-1.0f,0.0f,
									   -1.0f,-1.0f,0.0f };

	const GLfloat Triangle_Colors[] = { 0.0f,0.50f,0.50f,
										0.0f,0.50f,0.50f,
										0.0f,0.50f,0.50f,
										0.0f,0.50f,0.50f,
										0.0f,0.50f,0.50f,
										0.0f,0.50f,0.50f };

	//create vao for Triangle
	glGenVertexArrays(1, &vao_Triangle);

	//Binding
	glBindVertexArray(vao_Triangle);

	//Buffer
	glGenBuffers(1, &vbo_Position_Triangle);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_Position_Triangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Triangle_Vertices),Triangle_Vertices,GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	//vao

	//Color
	//Buffer
	glGenBuffers(1, &vbo_Color_Triangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Triangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Triangle_Colors),
		Triangle_Colors, GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	glBindVertexArray(0); //vao

	//**********
	const GLfloat Rectangle_Vertices[] = { 
		1.0f,1.0f,0.0f,
		-1.0f,1.0f,0.0f,
		-1.0f,1.0f,0.0f,
		-1.0f,-1.0f,0.0f,
		1.0f,-1.0f,0.0f,
		-1.0f,-1.0f,0.0f,
		1.0f,-1.0f,0.0f,
		1.0f,1.0f,0.0f
	};

	const GLfloat Rectangle_Colors[] = { 
		1.0f,0.0f,0.10f,
		1.0f,0.0f,0.10f,
		1.0f,0.0f,0.10f,
		1.0f,0.0f,0.10f,
		1.0f,0.0f,0.10f,
		1.0f,0.0f,0.10f,
		1.0f,0.0f,0.10f,
		1.0f,0.0f,0.10f };

	//create vao for Rectangle
	glGenVertexArrays(1, &vao_Rectagle);

	//Binding
	glBindVertexArray(vao_Rectagle);

	//Buffer
	glGenBuffers(1, &vbo_Position_Rectangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Position_Rectangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Rectangle_Vertices),Rectangle_Vertices, GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Color
	//Buffer
	glGenBuffers(1, &vbo_Color_Rectangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Color_Rectangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Rectangle_Colors),	Rectangle_Colors, GL_STATIC_DRAW);

	//most imp
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Set Position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); //vao
	

/*	
	//InCircle
	GLfloat Radi, smPerimeter, Area, points=100;

	Area = (0.5f * (1.0f + 1.0f) *2.0f); // 1/2 (Base * Height)

	smPerimeter = (2.0f + sqrt(5.0f) + sqrt(5.0f)) / 2; //Perimeter = sum(all sides)

	Radi = (Area / smPerimeter); // Radius

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, -0.38f, -4.2f);

	glBegin(GL_LINE_LOOP);

	for (int iPts = 0; iPts < points; iPts++)
	{
		angle = (GLfloat)(2.0f*3.14*iPts) / points;

		glVertex2f((cos(angle))*Radi, (sin(angle))*Radi);
	}
	glEnd();
*/
	

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Depth 

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	Perspective_Projection_Matrix = mat4::identity();

	//Warm up call 
	Resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}


//Graph
void showGraph()
{
	//Graph Lines 
	GLfloat hLinesVertices[SLICES * 12];
	GLfloat hLinesColor[SLICES * 12];

	//Vert
	GLfloat vLinesVertices[SLICES * 12];
	GLfloat vLinesColor[SLICES * 12];

	float fGraphLines = 0.0f;
	float fPerQuad = 1.0f / SLICES;

	GLint totalVertices = SLICES * 4;

	//like FFP
	fGraphLines = fPerQuad;

	//Quadrants
	for (int i = 0; i < totalVertices; i++)
	{
		if (fGraphLines <= 1.01f)
		{
		//1
			hLinesVertices[i * 3 + 0] = 1.0f;
			hLinesVertices[i * 3 + 1] = fGraphLines;
			hLinesVertices[i * 3 + 2] = 0.0f;

			hLinesColor[i * 3 + 0] = 0.0f;
			hLinesColor[i * 3 + 1] = 1.0f;
			hLinesColor[i * 3 + 2] = 0.0f;

			//vert

			vLinesVertices[i * 3 + 0] = fGraphLines;
			vLinesVertices[i * 3 + 1] = 1.0f;
			vLinesVertices[i * 3 + 2] = 0.0f;

			vLinesColor[i * 3 + 0] = 0.0f;
			vLinesColor[i * 3 + 1] = 1.0f;
			vLinesColor[i * 3 + 2] = 0.0f;

			i++;
		
		//2

			hLinesVertices[i * 3 + 0] = -1.0f;
			hLinesVertices[i * 3 + 1] = fGraphLines;
			hLinesVertices[i * 3 + 2] = 0.0f;

			hLinesColor[i * 3 + 0] = 0.0f;
			hLinesColor[i * 3 + 1] = 1.0f;
			hLinesColor[i * 3 + 2] = 0.0f;

			//vert
			vLinesVertices[i * 3 + 0] = fGraphLines;
			vLinesVertices[i * 3 + 1] = -1.0f;
			vLinesVertices[i * 3 + 2] = 0.0f;

			vLinesColor[i * 3 + 0] = 0.0f;
			vLinesColor[i * 3 + 1] = 1.0f;
			vLinesColor[i * 3 + 2] = 0.0f;

			i++;
    
		//3

			hLinesVertices[i * 3 + 0] = 1.0f;
			hLinesVertices[i * 3 + 1] = -fGraphLines;
			hLinesVertices[i * 3 + 2] = 0.0f;

			hLinesColor[i * 3 + 0] = 0.0f;
			hLinesColor[i * 3 + 1] = 1.0f;
			hLinesColor[i * 3 + 2] = 0.0f;

			//vert
			vLinesVertices[i * 3 + 0] = -fGraphLines;
			vLinesVertices[i * 3 + 1] = 1.0f;
			vLinesVertices[i * 3 + 2] = 0.0f;

			vLinesColor[i * 3 + 0] = 0.0f;
			vLinesColor[i * 3 + 1] = 1.0f;
			vLinesColor[i * 3 + 2] = 0.0f;

			i++;

		//4
			
			hLinesVertices[i * 3 + 0] = -1.0f;
			hLinesVertices[i * 3 + 1] = -fGraphLines;
			hLinesVertices[i * 3 + 2] = 0.0f;

			hLinesColor[i * 3 + 0] = 0.0f;
			hLinesColor[i * 3 + 1] = 1.0f;
			hLinesColor[i * 3 + 2] = 0.0f;

			//vert
			vLinesVertices[i * 3 + 0] = -fGraphLines;
			vLinesVertices[i * 3 + 1] = -1.0f;
			vLinesVertices[i * 3 + 2] = 0.0f;

			vLinesColor[i * 3 + 0] = 0.0f;
			vLinesColor[i * 3 + 1] = 1.0f;
			vLinesColor[i * 3 + 2] = 0.0f;
		}

	  fGraphLines = fGraphLines + fPerQuad; //imp
	}

 //Axes
	GLfloat xAxis[] = { 1.0f, 0.0f, 0.0f, 
					   -1.0f, 0.0f, 0.0f };

	GLfloat AxisColor[] = { 0.0f,1.0f,0.0f,
							0.0f,1.0f,0.0f };
	
	GLfloat YAxis[] = { 0.0f, 1.0f, 0.0f,
						0.0f, -1.0f, 0.0f };

	// Create vao
	glGenVertexArrays(1, &vao_XAxis);
	glBindVertexArray(vao_XAxis);

	glGenBuffers(1, &vbo_XPosition);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_XPosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6, xAxis, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	// Unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Bind color
	glGenBuffers(1, &vbo_XColor);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_XColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(AxisColor), AxisColor, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	// Unbind color
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Unbind vao
	glBindVertexArray(0);

 //Y-Axis
	
	// Create vao
	
	glGenVertexArrays(1, &vao_YAxis);
	glBindVertexArray(vao_YAxis);

	glGenBuffers(1, &vbo_YPosition);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_YPosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*6, YAxis, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	// Unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Bind color
	glGenBuffers(1, &vbo_YColor);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_YColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(AxisColor), AxisColor, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	// Unbind color
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Unbind vao
	glBindVertexArray(0);


	//Lines
	// Create vao
	glGenVertexArrays(1, &vao_HLines);
	glBindVertexArray(vao_HLines);

	glGenBuffers(1, &vbo_PositionHLines);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_PositionHLines);

	glBufferData(GL_ARRAY_BUFFER, sizeof(hLinesVertices), hLinesVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	// Unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Bind color
	glGenBuffers(1, &vbo_ColorHLines);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_ColorHLines);

	glBufferData(GL_ARRAY_BUFFER, sizeof(hLinesColor), hLinesColor, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	// Unbind color
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Unbind vao
	glBindVertexArray(0);

	
	//*****Vert

	// Create Lines vao
	glGenVertexArrays(1, &vao_Lines);
	glBindVertexArray(vao_Lines);

	glGenBuffers(1, &vbo_PositionVLines);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_PositionVLines);

	glBufferData(GL_ARRAY_BUFFER, sizeof(vLinesVertices), vLinesVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	// Unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Bind color
	glGenBuffers(1, &vbo_ColorVLines);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_ColorVLines);

	glBufferData(GL_ARRAY_BUFFER, sizeof(vLinesColor), vLinesColor, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	// Unbind 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Unbind vao Lines
	glBindVertexArray(0);

}


//Display
void Display(void)
{
	//1
	mat4 ModelView_Matrix;
	mat4 ModelView_Projection_Matrix;
	mat4 Translation_Matrix;
	mat4 Rotation_Matrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShader_Program_Object);

	//code : 9 steps

	//** for Graph  

	// X-Axis

	//2 : I[]
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//3 : Transforms
	ModelView_Matrix = translate(0.0f, 0.0f, -1.25f);

	//  Matrix Multiplication
	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;

	// 5 : send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	// Bind with vao
	glBindVertexArray(vao_XAxis);

	glLineWidth(3.0f);

	glDrawArrays(GL_LINES, 0, 2);

	// Unbind vao
	glBindVertexArray(0);


	// Y-AXIS
	// 2 : I[]
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();

	//  Transformation
	ModelView_Matrix = translate(0.0f, 0.0f, -1.25f);

	//  Matrix Multiplication
	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;

	//5 :  Send matrices to shader 
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	// Bind vao
	glBindVertexArray(vao_YAxis);

	glLineWidth(3.0f);
	glDrawArrays(GL_LINES, 0, 2);

	// Unbind vao
	glBindVertexArray(0);


	// HORIZONTAL
	//2: I[]
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();

	// Transformation
	ModelView_Matrix = translate(0.0f, 0.0f, -1.25f);

	// Matrix Multiplication
	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;

	// 5: Send matrices to shader 
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	// Bind with vao 
	glBindVertexArray(vao_HLines);

	// Draw 
	glLineWidth(1.0f);
	glDrawArrays(GL_LINES, 0, SLICES * 4);

	// Unbind vao 
	glBindVertexArray(0);


	// VERTICAL 

	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();

	//Transform
	ModelView_Matrix = translate(0.0f, 0.0f, -1.25f);

	// Matrix Multiplication
	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;

	// Send matrices to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	// Bind with vao 
	glBindVertexArray(vao_Lines);

	// Draw 
	glLineWidth(1.0f);
	glDrawArrays(GL_LINES, 0, SLICES * 4);

	// Unbind vao
	glBindVertexArray(0);


	//2 : I[]
	Rotation_Matrix = mat4::identity();
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//Triangle

	//3 : Transforms

	Translation_Matrix = translate(0.0f, 0.0f, -5.20f);

	//4 : Do Mat mul

	ModelView_Matrix = Translation_Matrix * Rotation_Matrix;

	//new
	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;


	// 5 : send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	glBindVertexArray(vao_Triangle);

	//8 : draw scene
	glLineWidth(3.0f);
	glDrawArrays(GL_LINES, 0, 6);

	//9 : Unbind vao
	glBindVertexArray(0);


	//Rectangle
	//2 : I[]
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	Rotation_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//4 : Do Mat mul
	Translation_Matrix = translate(0.0f, 0.0f, -5.0f);

	ModelView_Matrix = Translation_Matrix * Rotation_Matrix;

	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;


	// 5 : send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	// 6 : bind to vao
	glBindVertexArray(vao_Rectagle);

	//8 : draw scene
	glLineWidth(3.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glDrawArrays(GL_LINES, 2, 2);
	glDrawArrays(GL_LINES, 4, 2);
	glDrawArrays(GL_LINES, 6, 2);

	//9 : Unbind vao
	glBindVertexArray(0);

	//Common
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void Update(void)
{

}


//ToggleFullScreen
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	mi.cbSize = sizeof(MONITORINFO);

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//mi = { sizeof(MONITORINFO) };// not supported in VS2008

		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			if ((GetWindowPlacement(ghwnd, &wpPrev)) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),// WD
					(mi.rcMonitor.bottom - mi.rcMonitor.top), // HT
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}

		}
		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}

	else
		if (gbIsFullScreen == true)
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

			SetWindowPlacement(ghwnd, &wpPrev);

			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
				SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

			ShowCursor(TRUE);
			gbIsFullScreen = false;

		}
}


//UnInitialize
void UnInitialize(void)
{
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	//for Shaders
	if (vbo_Position_Rectangle)
	{
		glDeleteBuffers(1, &vbo_Position_Rectangle);
		vbo_Position_Rectangle = 0;
	}

	if (vao_Rectagle)
	{
		glDeleteVertexArrays(1, &vao_Rectagle);
		vao_Rectagle = 0;
	}

	// Tri
	if (vbo_Position_Triangle)
	{
		glDeleteBuffers(1, &vbo_Position_Triangle);
		vbo_Position_Rectangle = 0;
	}

	if (vao_Triangle)
	{
		glDeleteVertexArrays(1, &vao_Triangle);
		vao_Triangle = 0;
	}

	if (vbo_ColorVLines)
	{
		glDeleteBuffers(1, &vbo_ColorVLines);
		vbo_ColorVLines = 0;
	}

	if (vbo_ColorHLines)
	{
		glDeleteBuffers(1, &vbo_ColorHLines);
		vbo_ColorHLines = 0;
	}


	glUseProgram(gShader_Program_Object);
	glDetachShader(gShader_Program_Object, gFragment_Shader_Object);
	glDetachShader(gShader_Program_Object, gVertex_Shader_Object);

	glDeleteShader(gFragment_Shader_Object);
	gFragment_Shader_Object = 0;

	glDeleteShader(gVertex_Shader_Object);
	gVertex_Shader_Object = 0;

	glDeleteProgram(gShader_Program_Object);
	glUseProgram(0);

	

	//Break Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "\n Application Closed ");
	fclose(gpFile);
	gpFile = NULL;
}


//Resize
void Resize(int Width, int Height)
{
	if (Height == 0)
	{
		Height = 1;
	}

	glViewport(0, 0, GLsizei(Width), GLsizei(Height));
	Perspective_Projection_Matrix = perspective(45.0f, (GLfloat)(Width) / (GLfloat)(Height), 0.1f, 100.0f);
	
}

