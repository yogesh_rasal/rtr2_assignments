
/*

	Assignment :  Cube Pyramid

*/

//Headers
#include <stdio.h>
#include <windows.h>
#include "GL\glew.h"
#include <stdlib.h>
#include "vmath.h"

#pragma comment (lib,"opengl32.lib")
#pragma comment (lib,"gdi32.lib")
#pragma comment (lib,"user32.lib")
#pragma comment (lib,"glew32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

// Globals
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
FILE *gpFile = NULL;
GLfloat AngleTri = 0.0f, AngleRect = 0.0f;

bool gbActiveWindow = false;
bool gbIsFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
GLenum Result;

//For Shaders
GLuint gVertex_Shader_Object;
GLuint gFragment_Shader_Object;
GLuint gShader_Program_Object;

GLint iShaderCompileStatus = 0;
GLint iInfoLogLength = 0;
GLchar *szInfoLog = NULL;
GLint iProgrmLinkStatus = 0;

//new
GLuint Vao_pyramid;
GLuint Vao_cube;

GLuint Vbo_pyramid_position;
GLuint Vbo_pyramid_color;

GLuint Vbo_cube_position;
GLuint Vbo_cube_color;

GLuint mvpUniform;
mat4 Perspective_Projection_Matrix;


//ENUM
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

GLsizei shaderCount, shaderNumber;

// Function Declarations
int Initialize(void);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Display(void);
void UnInitialize(void);
void Update(void);
void ToggleFullScreen(void);
void Resize(int, int);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIns, LPSTR lpCmdLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Shape");
	int iRet = 0;
	bool bDone = false;

	//File IO
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can not Created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	else
	{
		fprintf(gpFile, "Log File Successfully Created");
	}

	//initialize the class
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(hInstance, IDC_HAND);
	wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.cbWndExtra = 0;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("3D Shapes using Perspective"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100, 100, WIN_WIDTH, WIN_HEIGHT,
		NULL, NULL, hInstance, NULL);

	// For Update
	ghwnd = hwnd;
	iRet = Initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\n ChoosePixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -2)
	{
		fprintf(gpFile, "\n SetPixelFormat Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -3)
	{
		fprintf(gpFile, "\n wglCreateContext Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == -4)
	{
		fprintf(gpFile, "\n wglMakeCurrent Failed");
		DestroyWindow(hwnd);
	}

	else if (iRet == 0)
	{
		fprintf(gpFile, "\n Initialization Successful");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd); //UpdateWindow
	SetFocus(hwnd);

	// Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else if (gbActiveWindow == true)
		{
			Update();
		}

		Display();
	}

	return((int)msg.wParam);

}//Main Ends


 //WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function Declarations
	
	switch (iMsg)
	{
	case WM_CREATE:

		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;

		}
		break;

	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}//WndProc Ends

 //init()
int Initialize(void)
{
	void Resize(int, int); // imp
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;

	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32; //1

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	else if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	//Bridging API
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	else if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}
		

	//*****************************

	Result = glewInit();

	if (Result != GLEW_OK)
	{
		UnInitialize();
	}


	/*
			
 	*/

	//For Shaders : create 
	gVertex_Shader_Object = glCreateShader(GL_VERTEX_SHADER);

	//Define VS code
	GLchar *VertexShaderCode =
		"#version 450 core"							\
		"\n"										\
		"in vec4 vPosition;"						\
		"in vec4 vColor;"							\
		"out vec4 Out_Color;"						\
		"uniform mat4 u_mvp_matrix;"				\
		"void main(void)"							\
		"{"											\
		"gl_Position = u_mvp_matrix * vPosition;"	\
		"Out_Color = vColor;"						\
		"}" ;

	//Specify code to Obj
	glShaderSource(gVertex_Shader_Object,
					1,
				(const GLchar**)&VertexShaderCode,
				NULL);

	//Compile the shader
	glCompileShader(gVertex_Shader_Object);

	//Error Code for VS
	glGetShaderiv(gVertex_Shader_Object,
				GL_COMPILE_STATUS,
				&iShaderCompileStatus);


	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertex_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gVertex_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Vertex Shader");
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//2 - FS
	gFragment_Shader_Object = glCreateShader(GL_FRAGMENT_SHADER);

	//Define FS code
	GLchar *FragmentShaderCode =
		"#version 450 core"		\
		"\n"					\
		"in vec4 Out_Color;"	\
		"out vec4 FragColor;"	\
		"void main(void)"		\
		" { "					\
		"FragColor = Out_Color;" \
		" } " ;

	//Specify code to Obj
	glShaderSource(gFragment_Shader_Object,1,
		( GLchar**)&FragmentShaderCode,NULL);

	//Compile the shader
	glCompileShader(gFragment_Shader_Object);

	//Error check for FS
	glGetShaderiv(gFragment_Shader_Object,
		GL_COMPILE_STATUS,
		&iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragment_Shader_Object,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetShaderInfoLog(gFragment_Shader_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Fragment Shader");
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//3 - Create Shader Program
	gShader_Program_Object = glCreateProgram();

	//attach VS & FS
	glAttachShader(gShader_Program_Object, gVertex_Shader_Object);

	glAttachShader(gShader_Program_Object, gFragment_Shader_Object);

	//prelink
	glBindAttribLocation(gShader_Program_Object,AMC_ATTRIBUTE_POSITION,"vPosition");
	glBindAttribLocation(gShader_Program_Object, AMC_ATTRIBUTE_COLOR, "vColor");
	
	//Link SP
	glLinkProgram(gShader_Program_Object);


	// Error check for SP
	glGetProgramiv(gShader_Program_Object, GL_LINK_STATUS, &iProgrmLinkStatus);


	if (iProgrmLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShader_Program_Object, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0) //Error found
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei Written;
				glGetProgramInfoLog(gShader_Program_Object, iInfoLogLength, &Written, szInfoLog);

				fprintf(gpFile, "\n  Failed in Shader Program Linking");
				free(szInfoLog);
				szInfoLog = NULL;
				UnInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//postlink
	mvpUniform = glGetUniformLocation(gShader_Program_Object,"u_mvp_matrix");


	//pyramid
	const GLfloat pyramidVertices[] = 
	{
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f
	};

	glGenVertexArrays(1, &Vao_pyramid);
	glBindVertexArray(Vao_pyramid);
	
	//for vertices
	glGenBuffers(1, &Vbo_pyramid_position);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_pyramid_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//for colors
	const GLfloat pyramidColors[] =
	{
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f
	};

	glGenBuffers(1,&Vbo_pyramid_color);
	glBindBuffer(GL_ARRAY_BUFFER,Vbo_pyramid_color);
	glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidColors),pyramidColors,GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);

	//cube
	const GLfloat cubeVertices[] = 
	{
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f	
	};

	glGenVertexArrays(1, &Vao_cube);
	glBindVertexArray(Vao_cube);

	glGenBuffers(1, &Vbo_cube_position);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_cube_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	//cube colors
	const GLfloat cubeColors[] = 
	{
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f	
	};

	glGenBuffers(1, &Vbo_cube_color);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_cube_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeColors), cubeColors, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);//vbo

	glBindVertexArray(0); //vao


	//Depth 
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	Perspective_Projection_Matrix = mat4::identity();

	//Warm up call 
	Resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}


//ToggleFullScreen
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	mi.cbSize = sizeof(MONITORINFO);

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//mi = { sizeof(MONITORINFO) };// not supported in VS2008

		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			if ((GetWindowPlacement(ghwnd, &wpPrev)) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),// WD
					(mi.rcMonitor.bottom - mi.rcMonitor.top), // HT
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}

		}
		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}

	else
		if (gbIsFullScreen == true)
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

			SetWindowPlacement(ghwnd, &wpPrev);

			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
				SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

			ShowCursor(TRUE);
			gbIsFullScreen = false;

		}
}


//UnInitialize
void UnInitialize(void)
{
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	//for Shaders
	if (Vbo_cube_position)
	{
		glDeleteBuffers(1, &Vbo_cube_position);
		Vbo_cube_position = 0;
	}

	if (Vao_cube)
	{
		glDeleteVertexArrays(1, &Vao_cube);
		Vao_cube = 0;
	}


	if (Vbo_pyramid_position)
	{
		glDeleteBuffers(1, &Vbo_pyramid_position);
		Vbo_pyramid_position = 0;
	}

	if (Vbo_cube_color)
	{
		glDeleteVertexArrays(1, &Vbo_cube_color);
		Vbo_cube_color = 0;
	}


	if (Vbo_pyramid_color)
	{
		glDeleteVertexArrays(1, &Vbo_pyramid_color);
		Vbo_pyramid_color = 0;
	}

	if (Vbo_pyramid_color)
	{
		glDeleteBuffers(1, &Vbo_pyramid_color);
		Vbo_pyramid_color = 0;
	}

	glUseProgram(gShader_Program_Object);
	glDetachShader(gShader_Program_Object, gFragment_Shader_Object);
	glDetachShader(gShader_Program_Object, gVertex_Shader_Object);

	glDeleteShader(gFragment_Shader_Object);
	gFragment_Shader_Object = 0;

	glDeleteShader(gVertex_Shader_Object);
	gVertex_Shader_Object = 0;

	glDeleteProgram(gShader_Program_Object);
	glUseProgram(0);

	

	//Break Current Context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "\n Application Closed ");
	fclose(gpFile);
	gpFile = NULL;
}


//Resize
void Resize(int Width, int Height)
{
	if (Height == 0)
	{
		Height = 1;
	}

	glViewport(0, 0, GLsizei(Width), GLsizei(Height));
	Perspective_Projection_Matrix = perspective(90.0f, (GLfloat)(Width) /(GLfloat) (Height), 1.0f, 100.0f);
	
}


//Display
void Display(void)
{
	//1
	mat4 ModelView_Matrix;
	mat4 ModelView_Projection_Matrix;
	mat4 Translation_Matrix;
	mat4 Rotation_Matrix;


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShader_Program_Object);

	//code : 9 steps
	//2 : I[]
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	Rotation_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//3 : Transforms

	//4 : Do Mat mul
	Translation_Matrix = translate(-1.6f, 0.0f, -4.0f);
	
	Rotation_Matrix = rotate(AngleTri,  0.0f, 1.0f, 0.0f);
	
	ModelView_Matrix = Translation_Matrix * Rotation_Matrix;

	//new
	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;

	
	// 5 : send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	// 6 : bind to vao
	glBindVertexArray(Vao_pyramid);


	//8 : draw scene
	glDrawArrays(GL_TRIANGLES, 0, 12);

	//9 : Unbind vao
	glBindVertexArray(0);


	//Rectangle
	//2 : I[]
	ModelView_Matrix = mat4::identity();
	ModelView_Projection_Matrix = mat4::identity();
	Rotation_Matrix = mat4::identity();
	Translation_Matrix = mat4::identity();

	//4 : Do Mat mul
	Translation_Matrix = translate(1.5f, 0.0f, -4.0f);

	Rotation_Matrix = rotate(AngleRect, 1.0f, 0.0f, 0.0f);

	ModelView_Matrix = Translation_Matrix * Rotation_Matrix;

	ModelView_Projection_Matrix = Perspective_Projection_Matrix * ModelView_Matrix;


	// 5 : send it to shader
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, ModelView_Projection_Matrix);

	// 6 : bind to vao
	glBindVertexArray(Vao_cube);

	//8 : draw scene
	
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);


	//9 : Unbind vao
	glBindVertexArray(0);

	//Common
	glUseProgram(0);
	
	SwapBuffers(ghdc);
}

void Update(void)
{
	AngleTri += 0.1f;

	if (AngleTri >= 360.0f)
		AngleTri -= 360.0f;

	AngleRect += 0.1f;

	if (AngleRect >= 360.0f)
		AngleRect -= 360.0f;

}

