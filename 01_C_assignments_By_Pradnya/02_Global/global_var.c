/*
1. GLOBAL VARIABLE 

take one global "count" variable.
call two functions from main, 
a.change_count_by_one -> 
	
	i)increase count variable by one.
	ii)print variable count in it.

b.change_count_buy_two -> 
	i)increase count variable by two.
	ii)print variable count in it.
	
	print count variable before and after these two function call from main.

*/

#include<stdio.h>

int count_var = 0;


 void increase_by_one() 
{
 count_var+= 1;
 printf("1st Value %d  \n", count_var);
}

 void increase_by_two() 
{ 
 count_var += 2;
 printf("2nd Value %d \n", count_var);

}

int main()
{
	increase_by_one();
	increase_by_two();  
	printf("Final Value %d  ", count_var);
}
