// Keep Static Call inside main

#include<stdio.h>

int main(void)
{
	int num =10;
	void inc_by_one(void);
	void inc_by_two(void);
	
	printf("Value of num in main : %d \n",num);
	
	// Calls
	inc_by_one();
	inc_by_one();

	inc_by_two();
	inc_by_two();

	return (0);
}


void inc_by_one(void)
{
	static int num = 5;
	printf("Value of num : %d \n",num);	
	++num;

}


void inc_by_two(void)
{
	int num = 5;
	printf("Value of num in Non-Static : %d \n",num);	
	num+=2;

}