// Use of Static in same file & outside main

#include<stdio.h>

 void change_count();
 
int main() 
{
 static int num = 5;
 printf("\n Value of Number in main : %d",num);
 change_count();
 printf("\n Value of Number after change : %d",num);
 
return 0;
}

 void change_count() 
{
 int num= 10;
 printf("\n Value of Number in change : %d",num);
}

