// Use of Extern in same file

#include<stdio.h>

// Global Extern Variable 
int Global_var = 10;

// Function tobe call
void print_extern(void);

int main(void)
{
	printf("Value of Global_var : %d \n",Global_var);
	// change 
	Global_var = 20;	
	printf("Value of Global_var after change : %d \n",Global_var);

	print_extern();
	
	printf("Value of Global_var after print_extern : %d \n",Global_var);

	return 0;
}


void print_extern(void)
{
	printf("Value of Global_var inside print_extern : %d \n",Global_var);
	Global_var = 30;	
}
 