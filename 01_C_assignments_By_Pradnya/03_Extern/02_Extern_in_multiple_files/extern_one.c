#include<stdio.h>

// print Global Extern Variable 
	
void print_extern(void)
{	 
	extern int Global_var;
	printf("\n Value of Global_var in print_extern : %d",Global_var);
}

// Note : extern members cannot change inside any Block{} scope.