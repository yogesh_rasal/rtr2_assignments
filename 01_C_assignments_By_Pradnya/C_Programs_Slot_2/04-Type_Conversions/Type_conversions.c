// Slot 2 Assignments : 04- Type Concversion

 
#include <stdio.h>

int main(void)
{
	//variables
	int i, j;
	char ch_01, ch_02;
	
	int a, result_int;
	float f, result_float;

	int num_explicit;
	float f_explicit;

	//code
	
	// Char to int Casting
	i = 70;
	ch_01 = i;
	printf("Value of i = %d\n", i);
	printf("Charater 1 (after ch_01 = i) = %c\n\n", ch_01);

	ch_02 = 'Q';
	j = ch_02;
	printf("Charater 2 = %c\n", ch_02);
	printf("Value of j (after j = ch_02) = %d\n\n", j);

	// int to float Casting
	a = 10;
	f = 7.8f;
	result_float = a + f;
	printf("Int a = %d & float f %f Added & Floating-Point Sum = %f\n\n", a, f, result_float);

	result_int = a + f;
	printf("Int a = %d And float %f Added & Integer Sum = %d\n\n", a, f, result_int);

	// Corecion
	f_explicit = 20.121995f;
	num_explicit = (int)f_explicit;
	printf("Float Number tobe Type Casted Explicitly = %f\n", f_explicit);
	printf("Int output After Explicit Type Casting Of %f into %d \n", f_explicit, num_explicit);

	return(0);
}
