/*
	Assignment : Relational Opeartors
*/

#include <stdio.h>

int main(void)
{
	int a;
	int b;
	int result;

	printf("\n Enter 1st Number : \t ");
	scanf("%d",&a);
	printf("\n Enter 2nd Number : \t ");
	scanf("%d",&b);
	
	printf("\n If Answer = 0 , It is 'FALSE'.\n");
	printf("\n If Answer = 1 , It is 'TRUE'.\n\n ");

	// Code
	result = (a < b);
	printf("(a < b)  a = %d is Less Than b = %d  		      \t Answer = %d\n", a, b, result);
	
	result = (a > b);
	printf("(a > b)  a = %d is Greater Than b = %d  		  \t Answer = %d\n", a, b, result);

	result = (a <= b);
	printf("(a <= b) a = %d is Less Than Or Equal To b = %d    \t Answer = %d\n", a, b, result);

	result = (a >= b);
	printf("(a >= b) a = %d is Greater Than Or Equal To b = %d \t Answer = %d\n", a, b, result);

	result = (a == b);
	printf("(a == b) a = %d is Equals to b = %d                 \t Answer = %d\n", a, b, result);

	result = (a != b);
	printf("(a != b) a = %d is NOT Equal to b = %d             \t Answer = %d\n", a, b, result);

	return(0);
}// main ends
