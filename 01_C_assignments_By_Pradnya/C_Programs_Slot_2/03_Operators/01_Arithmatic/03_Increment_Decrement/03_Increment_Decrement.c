// Assignment 03 : Increment , Decrement Operators

#include <stdio.h>

int main(void)
{
	//variables
	int a = 25;
	int b = 20;

	//code
	printf("\n ");
	printf("Value of a = %d\n", a);
	printf("Value of a = %d\n", a++);
	printf("Value of a = %d\n", a);
	printf("Value of a = %d\n", ++a);

	printf("\n Value of b = %d\n", b);
	printf("Value of b = %d\n", b--);
	printf("Value of b  = %d\n", b);
	printf("Value of b = %d\n\n", --b);

	return(0);
}
