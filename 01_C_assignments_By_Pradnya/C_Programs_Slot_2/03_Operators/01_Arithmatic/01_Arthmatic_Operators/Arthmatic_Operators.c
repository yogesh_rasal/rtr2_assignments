// Assignment for Arthmatic Operators 

#include "stdio.h"

int main(void)
{
	// Variables 
	int num1,num2,result;
	
	// Accept the i/p
	printf("\n Enter Numbers num1 & num2 : \t");
	scanf("%d  %d ",&num1 , &num2);
	
	// Operations tobe Perform : +,-,*,/ & %.
	result = num1 + num2;
	printf(" \n Sum of num1 = %d & num2 = %d is %d \n ",num1,num2,result);
	
	result = num1 - num2;
	printf(" \n Difference of num1 = %d & num2 = %d is %d \n ",num1,num2,result);
	
	result = num1 * num2;
	printf(" \n Multiplication of num1 = %d & num2 = %d is %d \n ",num1,num2,result);
	
	result = num1 / num2;
	printf(" \n Division of num1 = %d & num2 = %d gives Quotient %d \n ",num1,num2,result);
	
	result = num1 % num2;
	printf(" \n Division of num1 = %d & num2 = %d gives Remainder %d \n ",num1,num2,result);
	
	printf("\n\n");

	return(0);
	
}// main() ends
