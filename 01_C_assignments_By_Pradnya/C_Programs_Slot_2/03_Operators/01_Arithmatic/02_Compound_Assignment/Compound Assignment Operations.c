// Assignment : Compound Assignment Operations

#include <stdio.h>


int main(void)
{
	// Variables 
	int num1,num2,res;
	
	// Accept the i/p
	printf("\n Enter The Numbers num1 & num2");
	scanf("%d \t %d \n",&num1 , &num2);
	
	// Compound Assignment Operators +=, -=, *=, /= and %=
	res = num1;
	num1 += num2; // num1 = num1 + num2;
	printf(" \n Sum of num1 = %d & num2 = %d is %d \n ",res,num2,num1);

	res = num1;
	num1 -= num2; // num1 = num1 - num2;
	printf(" \n Difference of num1 = %d & num2 = %d is %d \n ",res,num2,num1);
	
	
	res = num1;
	num1 *= num2; // num1 = num1 * num2;
	printf(" \n Product of num1 = %d & num2 = %d is %d \n ",res,num2,num1);
	
	
	res = num1;
	num1 /= num2; // num1 = num1 / num2;
	printf(" \n division of num1 = %d & num2 = %d gives Quotient %d \n ",res,num2,num1);
	
	res = num1;
	num1 /= num2; // num1 = num1 % num2;
	printf(" \n division of num1 = %d & num2 = %d gives Reminder %d \n ",res,num2,num1);
	
	printf("\n\n");

	return(0);
	
}
