// Assignment : Bitwise Operators
	

#include <stdio.h>

int main(void)
{
	//function 
	void PrintBinaryFormOfNumber(unsigned int);

	//variables
	unsigned int yog_num1;
	unsigned int yog_num2;
	unsigned int right_shift_A, right_shift_B;
	unsigned int left_shift_A, left_shift_B;
	unsigned int result;

	//code
	printf("\n\n");
	printf("Enter An Integer = ");
	scanf("%u", &yog_num1);

	printf("\n\n");
	printf("Enter Another Integer = ");
	scanf("%u", &yog_num2);

	printf("\n\n");
	printf("By How Many Bits Do You Want To Shift 1st Number = %d To The Right ? ", yog_num1);
	scanf("%u", &right_shift_A);

	printf("\n\n");
	printf("By How Many Bits Do You Want To Shift 2nd Number = %d To The Right ? ", yog_num2);
	scanf("%u", &right_shift_B);

	printf("\n\n");
	printf("By How Many Bits Do You Want To Shift 1st Number = %d To The Left ? ", yog_num1);
	scanf("%u", &left_shift_A);

	printf("\n\n");
	printf("By How Many Bits Do You Want To Shift 2nd Number = %d To The Left ? ", yog_num2);
	scanf("%u", &left_shift_B);

	printf("\n\n\n\n");
	result = yog_num1 & yog_num2;
	printf("Bitwise AND-ing Of \n %d (Decimal), %o (Octal), %X (Hexadecimal) and \nB = %d (Decimal), %o (Octal), %X (Hexadecimal) \nGives The Result = %d (Decimal), %o (Octal), %X (Hexadecimal).\n\n", yog_num1, yog_num1, yog_num1, yog_num2, yog_num2, yog_num2, result, result, result);
	PrintBinaryFormOfNumber(yog_num1);
	PrintBinaryFormOfNumber(yog_num2);
	PrintBinaryFormOfNumber(result);

	printf("\n\n\n\n");
	result = yog_num1 | yog_num2;
	printf("Bitwise OR-ing Of \n 1st Number = %d (Decimal), %o (Octal), %X (Hexadecimal) and \n 2nd Number = %d (Decimal), %o (Octal), %X (Hexadecimal) Gives The \nResult = %d (Decimal), %o (Octal), %X (Hexadecimal).\n\n", yog_num1, yog_num1, yog_num1, yog_num2, yog_num2, yog_num2, result, result, result);
	PrintBinaryFormOfNumber(yog_num1);
	PrintBinaryFormOfNumber(yog_num2);
	PrintBinaryFormOfNumber(result);

	printf("\n\n\n\n");
	result = yog_num1 ^ yog_num2;
	printf("Bitwise XOR-ing Of \n 1st = %d (Decimal), %o (Octal), %X (Hexadecimal) and \nB = %d (Decimal), %o (Octal), %X (Hexadecimal) Gives The \nResult = %d (Decimal), %o (Octal), %X (Hexadecimal).\n\n", yog_num1, yog_num1, yog_num1, yog_num2, yog_num2, yog_num2, result, result, result);
	PrintBinaryFormOfNumber(yog_num1);
	PrintBinaryFormOfNumber(yog_num2);
	PrintBinaryFormOfNumber(result);

	printf("\n\n\n\n");
	result = ~yog_num1;
	printf("Bitwise COMPLEMENT Of \n 1st Number = %d (Decimal), %o (Octal), %X (Hexadecimal) \nGives The \nResult = %d (Decimal), %o (Octal), %X (Hexadecimal).\n\n", yog_num1, yog_num1, yog_num1, result, result, result);
	PrintBinaryFormOfNumber(yog_num1);
	PrintBinaryFormOfNumber(result);

	printf("\n\n\n\n");
	result = ~yog_num2;
	printf("Bitwise COMPLEMENT Of \n 2nd Number = %d (Decimal), %o (Octal), %X (Hexadecimal) \nGives The \nResult = %d (Decimal), %o (Octal), %X (Hexadecimal).\n\n", yog_num2, yog_num2, yog_num2, result, result, result);
	PrintBinaryFormOfNumber(yog_num2);
	PrintBinaryFormOfNumber(result);

	printf("\n\n\n\n");
	result = yog_num1 >> right_shift_A;
	printf("Bitwise RIGHT-SHIFT By %d Bits Of \n 1st Number = %d (Decimal), %o (Octal), %X (Hexadecimal) \nGives The \nResult = %d (Decimal), %o (Octal), %X (Hexadecimal).\n\n", right_shift_A, yog_num1, yog_num1, yog_num1, result, result, result);
	PrintBinaryFormOfNumber(yog_num1);
	PrintBinaryFormOfNumber(result);

	printf("\n\n\n\n");
	result = yog_num2 >> right_shift_B;
	printf("Bitwise RIGHT-SHIFT By %d Bits Of \n 2nd Number = %d (Decimal), %o (Octal), %X (Hexadecimal) \nGives The \nResult = %d (Decimal), %o (Octal), %X (Hexadecimal).\n\n", right_shift_B, yog_num2, yog_num2, yog_num2, result, result, result);
	PrintBinaryFormOfNumber(yog_num2);
	PrintBinaryFormOfNumber(result);

	printf("\n\n\n\n");
	result = yog_num1 << left_shift_A;
	printf("Bitwise LEFT-SHIFT By %d Bits Of \n 1st Number = %d (Decimal), %o (Octal), %X (Hexadecimal) \nGives The \nResult = %d (Decimal), %o (Octal), %X (Hexadecimal).\n\n", left_shift_A, yog_num1, yog_num1, yog_num1, result, result, result);
	PrintBinaryFormOfNumber(yog_num1);
	PrintBinaryFormOfNumber(result);

	printf("\n\n\n\n");
	result = yog_num2 << left_shift_B;
	printf("Bitwise LEFT-SHIFT By %d Bits Of \n 2nd Number  = %d (Decimal), %o (Octal), %X (Hexadecimal) \nGives The \nResult = %d (Decimal), %o (Octal), %X (Hexadecimal).\n\n", left_shift_B, yog_num2, yog_num2, yog_num2, result, result, result);
	PrintBinaryFormOfNumber(yog_num2);
	PrintBinaryFormOfNumber(result);

	return(0);
}

// Function
void PrintBinaryFormOfNumber(unsigned int decimal_number)
{
	//variable declarations
	unsigned int quotient, remainder;
	unsigned int num;
	unsigned int binary_array[8];
	int i;

	//code
	for (i = 0; i < 8; i++)
		binary_array[i] = 0;

	printf("The Binary Form Of The Decimal Integer %d is\t=\t", decimal_number);
	num = decimal_number;
	i = 7;
	
	while (num != 0)
	{
		quotient = num / 2;
		remainder = num % 2;
		binary_array[i] = remainder;
		num = quotient;
		i--;
	}

	for (i = 0; i < 8; i++)
	{
		printf("%u", binary_array[i]);
	}
	printf("\n\n");
}

