/*

	Assignment : Logical Operators

*/

#include <stdio.h>

int main(void)
{
	int num1,num2,num3;
	int result = 0;

	printf("\n");
	printf("\n Enter First Number : ");
	scanf("%d", &num1);

	printf("\n");
	printf("Enter Second Number : ");
	scanf("%d", &num2);

	printf("\n");
	printf("Enter Third Number : ");
	scanf("%d", &num3);

	printf("\n Note : in Answer,  1 = True & 0 = False \n \n");

	//code

	result = ((num1 <= num2) && (num2 != num3));	
	printf("Exp1(&&)Exp2 gives answer = True, if Exp1 & Exp2 are True \n");
	printf("%d is Less than %d AND %d Not Equals %d : \t Answer is %d \n \n",num1,num2,num2,num3,result);
	
	result = ((num1 >= num2) || (num2 == num3));	
	printf("Exp1(||)Exp2 gives answer = True, if anyone of Exp1 or Exp2 are True \n");
	printf("%d is Greater than %d OR %d is Equals %d : \t Answer is %d \n\n",num1,num2,num2,num3,result);
	
	result = !num1;
	printf("Negation of %d is %d \n",num1,result);
	
	result = !num2;
	printf("Negation of %d is %d \n",num2,result);
	
	result = !num3;
	printf("Negation of %d is %d \n",num3,result);
	
	result = !((num3 <= num2) && (num1 != num3));	
	printf("\n using Logical Not(!), Negation of [(%d <= %d) && (%d != %d)] is %d \n\n",num3,num2,num1,num3,result);
	
return(0);
}//main ends

	
