// Slot 2 Assignments : 01-Escape_Sequences_in_C

#include "stdio.h"

// code 
int main(void)
{
	// \n : For New Line
		printf("\n \n \n ");
		printf("Moving To Next Line...using \\n Escape Sequence \n");
		
	// \t : For tab
		printf("Demo of Horizontal \t Tab \t with \\t Escape Sequence \n\n");
	
	// \r : For Carriage Return , \b : For Back space & \' , \" : For Quotes
	printf("\"This is Double Quoted Output\" Done Using \\\" \\\" Escape Sequence\n");
	printf("\'This is A Single Quoted Output\' Done Using \\\' \\\' Escape Sequence\n");
	printf("BACKSPACE Turned To BACKSPACE\b Using Escape Sequence \\b   \n\n");
	printf("\r Demonstrating Carriage Return Using \\r Escape Sequence \n");
	
	// Use of Hex Vales
	printf("Demonstrating \x42 Using \\xhh Escape Sequence \n"); 
	
	// Use of Octal Vales
	printf("Demonstrating \102 Using \\ooo Escape Sequence\n\n"); 

	return 0;
}



