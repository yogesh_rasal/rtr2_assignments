/*
	Assignment : Enum & Constants in C
*/

#include <stdio.h>

// Macro : Textual Replacement
#define MY_PI 3.1415926535897932


// Un-named Enums
enum
{
	SUNDAY,
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY,
};

// Initialized Enums
enum
{
	JANUARY = 1,
	FEBRUARY,
	MARCH,
	APRIL,
	MAY,
	JUNE,
	JULY,
	AUGUST,
	SEPTEMBER,
	OCTOBER,
	NOVEMBER,
	DECEMBER
};

//Named enums
enum Numbers
{
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE = 5,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN
};

enum boolean
{
	TRUE = 1,
	FALSE = 0
};

// Code
int main(void)
{
	//local constant
	const double leps = 0.000001;

	printf("\n Local Constant Epsilon = %lf\n\n", leps);

	printf("Sunday is Day Number = %d\n", SUNDAY);
	printf("Monday is Day Number = %d\n", MONDAY);
	printf("Tuesday is Day Number = %d\n", TUESDAY);
	printf("Friday is Day Number = %d\n", FRIDAY);
	printf("Saturday is Day Number = %d\n\n", SATURDAY);

	printf("One is Enum Number = %d\n", ONE);
	printf("Two is Enum Number = %d\n", TWO);
	printf("Three is Enum Number = %d\n", THREE);
	printf("Four is Enum Number = %d\n", FOUR);
	printf("Five is Enum Number = %d\n", FIVE);
	printf("Nine is Enum Number = %d\n", NINE);
	printf("Ten is Enum Number = %d\n\n", TEN);
	
	
	printf("January is Month Number = %d\n", JANUARY);
	printf("February is Month Number = %d\n", FEBRUARY);
	printf("March is Month Number = %d\n", MARCH);
	printf("April is Month Number = %d\n", APRIL);
	printf("May is Month Number = %d\n", MAY);
	printf("June is Month Number = %d\n", JUNE);
	printf("July is Month Number = %d\n", JULY);
	printf("August is Month Number = %d\n", AUGUST);
	printf("November is Month Number = %d\n", NOVEMBER);
	printf("December is Month Number = %d\n\n", DECEMBER);

	printf("Value Of TRUE is = %d\n", TRUE);
	printf("Value Of FALSE is = %d\n\n", FALSE);

	printf("MY_PI Macro value = %.10lf\n\n", MY_PI);
	printf("Area Of Circle Of Radius = 4 is %f\n\n", (MY_PI * 4.0f * 4.0f)); //pi * r * r = area of circle of radius 'r'

	return(0);
}// main ends

