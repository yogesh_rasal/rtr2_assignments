// Ordinary Local Variable

#include<stdio.h>

void change_count(); 
int main()
{
  int local_num = 10;
 printf("\n Value before change_count : %d",local_num);
	
 change_count(); 
 printf("\n Value after change_count : %d",local_num);

 return(0);
}

void change_count(void)
{
	int local_num = 20;
	printf("\n Value inside change_count : %d",local_num);	
}