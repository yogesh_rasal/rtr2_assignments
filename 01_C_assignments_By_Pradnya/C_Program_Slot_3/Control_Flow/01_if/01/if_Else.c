/*                             

		C Assignment : Conditional if 
		
*/

#include <stdio.h>
int main(void)
{
	int num;

	num = 5;
	if (num) // Non-zero Positive Value
	{
		printf("if-block 1 : 'num' Exists And Has Value = %d !!!\n\n", num);
	}

	num = -5;
	if (num) // Non-zero Negative Value
	{
		printf("if-block 2 : 'num' Exists And Has Value = %d !!!\n\n", num);
	}

	num = 0;
	if (num) // Zero Value
	{
		printf("if-block 3 : 'num' Exists And Has Value = %d !!!\n\n", num);
	}

	return(0);
}
