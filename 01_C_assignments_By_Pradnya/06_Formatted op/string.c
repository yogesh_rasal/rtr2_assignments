/* 
 Problem 6. Format String
*/

#include<stdio.h>

int main()
{
int number = 13;
char ch = 'P';
float f_num = 3.0121995f;
double d_pi = 3.14159265358979323846;
char str[] = "Astromedicomp";
unsigned int b = 7;
long num = 30121995L;

printf("\n");
printf(" Decimal Value of number : %d \n", number);

printf(" Octal Value of number: %o \n", number);

printf(" Hex Value of number in Upper : %X \n", number);

printf(" Hex Value of number in Lower : %x \n", number);

//printf(" Char Value of number: %c \n", number);

printf("\n char Value of ch : %c \n", ch);

printf(" Hex Value of ch : %x \n", ch);

printf("\n Unsigned Value of b: %u \n", b);

printf("\n String Value of str is : %s \n", str);

printf("\n Values of f_num in float are %f \t with Less Precision %4.2f \t  with More Precision %2.5f \t \n \n",f_num ,f_num ,f_num );

printf(" Values of d_pi in Double are %g \t %e \t %E %a \t %A \t \n \n",d_pi ,d_pi ,d_pi,d_pi,d_pi);


printf(" Long Value of num: %ld \n", num);

return 0;

}
