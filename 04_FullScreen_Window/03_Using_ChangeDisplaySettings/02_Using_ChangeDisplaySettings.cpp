/*
		Assignment : Create & Paint Window using ChangeDisplaySettings

		LONG ChangeDisplaySettings(  LPDEVMODE lpDevMode,  // graphics mode
									 DWORD dwflags         // graphics mode options);

		The ChangeDisplaySettings function changes the settings of the default display device to the specified graphics mode.


*/

#include<windows.h>


#pragma comment (lib,"opengl32.lib")
#pragma comment (lib,"gdi32.lib")
#pragma comment (lib,"user32.lib")
#pragma comment (lib,"glu32.lib")


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
bool bIsFullScreen = false;
#define WIN_WIDTH 1366
#define WIN_HEIGHT 866

//Code
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR AppName[] = TEXT("My Window");

	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(hInstance, IDC_HAND);
	wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = AppName;
	wndclass.lpszMenuName = NULL;
	wndclass.cbWndExtra = 0;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(AppName, TEXT(" Fullscreen Window with ChangeDisplaySettings by Yogeshwar"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
		NULL, NULL, hInstance, NULL);

	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);

}//main ends


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Variables
	RECT rc;
	HDC hdc;
	TCHAR str[] = TEXT("Hello World !!!");
	
	void FullScreen(void);

	switch (iMsg)
	{
	case WM_CREATE:
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_RBUTTONDOWN:
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			FullScreen();
			break;
	
		}
		break;

	case WM_PAINT:
		PAINTSTRUCT ps; //specialist
		GetClientRect(hwnd, &rc);
		hdc = BeginPaint(hwnd, &ps);

		SetBkColor(hdc, RGB(0, 0, 0)); // it sets Background of Text
		SetTextColor(hdc, RGB(255, 255, 0)); 
		DrawText(hdc, str, -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

		EndPaint(hwnd, &ps);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

// Function
void FullScreen(void)
{	
	//Structure 
	DEVMODE devMode;
	EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &devMode);

	if(bIsFullScreen)
	{
		//devMode.dmBitsPerPel = 4;
		devMode.dmPelsWidth = WIN_WIDTH;
		devMode.dmPelsHeight = WIN_HEIGHT;
		devMode.dmSize = sizeof(devMode);
		devMode.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

		ChangeDisplaySettings(&devMode, CDS_FULLSCREEN);
		bIsFullScreen = true;
	}

	else 
	if (bIsFullScreen == true)
	{
		ChangeDisplaySettings(NULL, 0);
		bIsFullScreen = false;
	}
	
}
