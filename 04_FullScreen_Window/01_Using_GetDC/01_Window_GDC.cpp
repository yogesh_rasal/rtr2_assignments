/*
	Assignment : Create & Paint Window using GetDC
*/

#include<windows.h>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Code
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR AppName[] = TEXT("My Window");

	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(hInstance, IDC_HAND);
	wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = AppName;
	wndclass.lpszMenuName = NULL;
	wndclass.cbWndExtra = 0;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(AppName, TEXT(" Window with Generic DC by Yogeshwar"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
		NULL, NULL, hInstance, NULL);


	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);

}//main ends


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Variables
	RECT rc;
	HDC hdc;
	TCHAR str[] = TEXT("Hello Matrix World !!!");

	switch (iMsg)
	{
	case WM_CREATE:
	/* GetClientRect(hwnd, &rc);
	     hdc = GetDC(hwnd);
		
	    SetBkColor(hdc, RGB(0,0,0)); // it sets Background of Text
		SetTextColor(hdc,RGB(0,255,0)); // Green
		DrawText(hdc,str,22,&rc,DT_SINGLELINE | DT_CENTER | DT_VCENTER);

	    ReleaseDC(hwnd, hdc);
	*/	
	break;

	case WM_LBUTTONDOWN:
	   GetClientRect(hwnd, &rc);
	   hdc = GetDC(hwnd);
		
	    SetBkColor(hdc, RGB(0,0,0)); // it sets Background of Text
		SetTextColor(hdc,RGB(0,255,0)); // Green
		DrawText(hdc,str,22,&rc,DT_SINGLELINE | DT_CENTER | DT_VCENTER);

	   ReleaseDC(hwnd, hdc);
	break;

	case WM_RBUTTONDOWN:
		MessageBox(hwnd, TEXT("This is Right Mouse Click"), TEXT("Message"), MB_OK);
		break;

	case WM_KEYDOWN:

		switch (wParam)
		{
		case VK_ESCAPE:
			MessageBox(hwnd, TEXT("This is Escape Key"), TEXT("Message"), MB_OK);
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
